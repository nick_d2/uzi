#!/bin/sh

echo ""
echo -e "\\x1b[01;37mInstalling application program\\x1b[00m"
echo ""

rm -f /wnd/*.scr
cd /usr/boot/inst/wnd
cp -v *.scr /wnd

rm -f /bin/grlogin /bin/test /bin/db
cd /usr/boot/inst/bin
cp -v grlogin test db /bin
align -v /bin/grlogin /bin/test /bin/db

echo ""
echo -e "\\x1b[01;37mFinished installing application\\x1b[00m"
echo ""

