


GREP(1)             UNIX Programmer's Manual              GREP(1)



NAME
     grep, egrep, fgrep - search a file for a pattern

SYNOPSIS
     grep [ option ] ...  expression [ file ] ...

     egrep [ option ] ...  [ expression ] [ file ] ...

     fgrep [ option ] ...  [ strings ] [ file ]

DESCRIPTION
     Commands of the _g_r_e_p family search the input _f_i_l_e_s (standard
     input default) for lines matching a pattern.  Normally, each
     line found is copied to the standard output; unless the -h
     flag is used, the file name is shown if there is more than
     one input file.

     _G_r_e_p patterns are limited regular expressions in the style
     of _e_d(1); it uses a compact nondeterministic algorithm.
     _E_g_r_e_p patterns are full regular expressions; it uses a fast
     deterministic algorithm that sometimes needs exponential
     space.  _F_g_r_e_p patterns are fixed strings; it is fast and
     compact.

     The following options are recognized.

     -v   All lines but those matching are printed.

     -c   Only a count of matching lines is printed.

     -l   The names of files with matching lines are listed
          (once) separated by newlines.

     -n   Each line is preceded by its line number in the file.

     -b   Each line is preceded by the block number on which it
          was found.  This is sometimes useful in locating disk
          block numbers by context.

     -s   No output is produced, only status.

     -h   Do not print filename headers with output lines.

     -y   Lower case letters in the pattern will also match upper
          case letters in the input (_g_r_e_p only).

     -e _e_x_p_r_e_s_s_i_o_n
          Same as a simple _e_x_p_r_e_s_s_i_o_n argument, but useful when
          the _e_x_p_r_e_s_s_i_o_n begins with a -.

     -f _f_i_l_e
          The regular expression (_e_g_r_e_p) or string list (_f_g_r_e_p)



Printed 9/5/2003                                                1






GREP(1)             UNIX Programmer's Manual              GREP(1)



          is taken from the _f_i_l_e.

     -x   (Exact) only lines matched in their entirety are
          printed (_f_g_r_e_p only).

     Care should be taken when using the characters $ * [ ^ | ? '
     " ( ) and \ in the _e_x_p_r_e_s_s_i_o_n as they are also meaningful to
     the Shell.  It is safest to enclose the entire _e_x_p_r_e_s_s_i_o_n
     argument in single quotes ' '.

     _F_g_r_e_p searches for lines that contain one of the (newline-
     separated) _s_t_r_i_n_g_s.

     _E_g_r_e_p accepts extended regular expressions.  In the follow-
     ing description `character' excludes newline:

          A \ followed by a single character matches that charac-
          ter.

          The character ^ ($) matches the beginning (end) of a
          line.

          A . matches any character.

          A single character not otherwise endowed with special
          meaning matches that character.

          A string enclosed in brackets [] matches any single
          character from the string.  Ranges of ASCII character
          codes may be abbreviated as in `a-z0-9'.  A ] may occur
          only as the first character of the string.  A literal -
          must be placed where it can't be mistaken as a range
          indicator.

          A regular expression followed by * (+, ?) matches a
          sequence of 0 or more (1 or more, 0 or 1) matches of
          the regular expression.

          Two regular expressions concatenated match a match of
          the first followed by a match of the second.

          Two regular expressions separated by | or newline match
          either a match for the first or a match for the second.

          A regular expression enclosed in parentheses matches a
          match for the regular expression.

     The order of precedence of operators at the same parenthesis
     level is [] then *+? then concatenation then | and newline.

SEE ALSO
     ed(1), sed(1), sh(1)



Printed 9/5/2003                                                2






GREP(1)             UNIX Programmer's Manual              GREP(1)



DIAGNOSTICS
     Exit status is 0 if any matches are found, 1 if none, 2 for
     syntax errors or inaccessible files.

BUGS
     Ideally there should be only one _g_r_e_p, but we don't know a
     single algorithm that spans a wide enough range of space-
     time tradeoffs.

     Lines are limited to 256 characters; longer lines are trun-
     cated.












































Printed 9/5/2003                                                3



