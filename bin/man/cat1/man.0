


MAN(1)              UNIX Programmer's Manual               MAN(1)



NAME
     man - print sections of this manual

SYNOPSIS
     man [ option ... ] [ chapter ] title ...

DESCRIPTION
     _M_a_n locates and prints the section of this manual named
     _t_i_t_l_e in the specified _c_h_a_p_t_e_r.  (In this context, the word
     `page' is often used as a synonym for `section'.)  The _t_i_t_l_e
     is entered in lower case.  The _c_h_a_p_t_e_r number does not need
     a letter suffix.  If no _c_h_a_p_t_e_r is specified, the whole
     manual is searched for _t_i_t_l_e and all occurrences of it are
     printed.

     _O_p_t_i_o_n_s and their meanings are:

     -t   Phototypeset the section using _t_r_o_f_f(1).

     -n   Print the section on the standard output using
          _n_r_o_f_f(1).

     -k   Display the output on a Tektronix 4014 terminal using
          _t_r_o_f_f(1) and _t_c(1).

     -e   Appended or prefixed to any of the above causes the
          manual section to be preprocessed by _n_e_q_n or _e_q_n(1); -e
          alone means -te.

     -w   Print the path names of the manual sections, but do not
          print the sections themselves.

     (default)
          Copy an already formatted manual section to the termi-
          nal, or, if none is available, act as -n.  It may be
          necessary to use a filter to adapt the output to the
          particular terminal's characteristics.

     Further _o_p_t_i_o_n_s, e.g. to specify the kind of terminal you
     have, are passed on to _t_r_o_f_f(1) or _n_r_o_f_f.  _O_p_t_i_o_n_s and
     _c_h_a_p_t_e_r may be changed before each _t_i_t_l_e.

     For example:

          man man

     would reproduce this section, as well as any other sections
     named _m_a_n that may exist in other chapters of the manual,
     e.g. _m_a_n(7).

FILES
     /usr/man/man?/*



Printed 9/5/2003                                                1






MAN(1)              UNIX Programmer's Manual               MAN(1)



     /usr/man/cat?/*

SEE ALSO
     nroff(1), eqn(1), tc(1), man(7)

BUGS
     The manual is supposed to be reproducible either on a photo-
     typesetter or on a terminal.  However, on a terminal some
     information is necessarily lost.














































Printed 9/5/2003                                                2



