#!/bin/sh

PATH='.:/bin:/usr/bin'
PS1='\x1b[01;31m\u@\h \x1b[01;34m\w \x1b[01;32m\# \x1b[00m'

umask 022

