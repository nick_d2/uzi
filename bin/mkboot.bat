del uzidisk.dat
touch uzidisk.dat
@if errorlevel 1 goto failure
mkfs -l -q -v -y uzidisk.dat 1536 30
@if errorlevel 1 goto failure
ucp < mkboot.ucp
@if errorlevel 1 goto failure
fsck -y uzidisk.dat
@if errorlevel 1 goto failure

gzip --best < uzidisk.dat > uzboot.gz
copy/b uzboot.ldr + uzboot.gz uzboot.bin

md nlddl
copy checksum.dat nlddl
copy checksum.dat nlddl\crc.dat
copy boot.bin nlddl
copy uzboot.bin nlddl
del nlddl\checksum
crc nlddl
copy nlddl\checksum .

@echo SUCCESS
@goto done
:failure
@echo FAILURE
:done

