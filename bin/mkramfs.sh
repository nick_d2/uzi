#!/bin/sh
# fixme: must be <= 256 bytes

cd /boot/skel
mkfs -l -q -v -y /dev/hd0 1536 30
mount /dev/hd0 /mnt
cp -r -v * /mnt
align -s -v /mnt/bin/* /mnt/boot/kernel.bin
umount /dev/hd0
reboot

