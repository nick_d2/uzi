#!/bin/sh

echo ""
echo -e "\\x1b[01;37mInstalling LCD character sets\\x1b[00m"
echo ""

cp -v /usr/lib/font/lcd0.cmd /dev/lcd0
cp -v /usr/lib/font/lcd1.cmd /dev/lcd1

umask 022
mkdir /wnd
exec /usr/boot/appinst.sh

