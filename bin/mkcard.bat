del uzidisk.dat
touch uzidisk.dat
@if errorlevel 1 goto failure
mkfs -f -q -v -y uzidisk.dat 8192 163
@if errorlevel 1 goto failure
ucp < mkcard.ucp
@if errorlevel 1 goto failure
fsck -y uzidisk.dat
@if errorlevel 1 goto failure

@echo SUCCESS
@goto done
:failure
@echo FAILURE
:done

