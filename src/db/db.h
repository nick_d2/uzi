#ifndef _DB_H
#define _DB_H

#include <stdio.h>
/*#define INTEL removed by Nick - please see n.bat */
/*#define EXPR_DISPLAY*/
/*#define FULLSQL*/

typedef unsigned char BYTE;
#ifdef ZILOG
typedef int WORD;
typedef long LONG;
typedef float FLOAT;
#else
typedef short WORD;
typedef int LONG;
typedef float FLOAT;
#endif

typedef struct DbListEntry{
	struct DbListEntry* previous;
	struct DbListEntry* next;
	BYTE* data;
} DbListEntry;	

typedef struct DbTreeListEntry{
	struct DbTreeListEntry* previous;
	struct DbTreeListEntry* next;
	BYTE* data;
	struct DbTreeListEntry* firstchild;
	BYTE operation;
} DbTreeListEntry;

typedef struct DbStatement {
	BYTE type;
	DbListEntry* fields;
	DbListEntry* tables;
	DbListEntry* wheres;
	DbListEntry* auxlist;
} DbStatement;

typedef struct DbField {
	char name[9];
	BYTE datatype;
	BYTE size;
	BYTE index;
} DbField;

typedef struct DbTable {
	WORD fields;
	DbField* fieldspecs;
	WORD numrecs;
	WORD unusedrecs;
	FILE* datafile;
	BYTE open;
	char name[9];
} DbTable;

typedef struct DbResultSet {
	WORD fields;
	DbField* fieldspecs;
	WORD rows;
	WORD rowptr;
	FILE* datafile;
} DbResultSet;

typedef union DbRowSet{
	DbTable astable;
	DbResultSet asresult;
} DbRowSet;

typedef struct DbRow {
	DbResultSet* source;
	WORD rownr;
	BYTE* data;
} DbRow;

typedef struct DbExpression {
	BYTE type;
	BYTE valuetype;
	BYTE oper;
	BYTE eval;
	char value[32];
	LONG result;
	char sresult[32];
	struct DbExpression* expr[2];
	struct DbExpression* parent;
} DbExpression;

void dbDeleteList(DbListEntry* lst);
WORD dbListIndex(DbListEntry* lst);
WORD dbListSize(DbListEntry* lst);
void dbSkipLiteral(char ch, char* str, WORD* src, WORD* dst);

void dbTrimString(char* str);
DbStatement* dbParseStatement(char* str);
DbStatement* dbParseSelect(char* str);
DbStatement* dbParseInsert(char* str);
DbStatement* dbParseUpdate(char* str);
DbStatement* dbParseDelete(char* str);
DbListEntry* dbParseList(char* str,char eoe,char eol,WORD* ptr);
DbResultSet* dbExecuteStatement(char* str);
DbResultSet* dbExecuteQuery(DbStatement* stat);
DbResultSet* dbExecuteInsert(DbStatement* stat);

void dbTableOpen(DbTable* tbl);
void dbTableClose(DbTable* tbl);
void dbStatementClose(DbStatement* stat);
void dbResultSetClose(DbResultSet* rs);
void dbDeleteRow(DbRow* row);
DbRow* dbNewRow();

WORD dbFieldOffset(DbResultSet* tbl, WORD field);
WORD dbRecordSize(DbResultSet* tbl);
WORD dbFieldSearch(DbResultSet* tbl,char* str);

void dbNextRow(DbResultSet* rs, DbRow* row);
void dbGetString(DbRow* row,WORD field, char* dest);
WORD dbGetInt(DbRow* row, WORD field);
LONG dbGetLong(DbRow* row, WORD field);

BYTE dbOperatorPrecedence(BYTE oper);
DbExpression* dbParseExpression(char* str, WORD* strptr);
void dbDeleteExpression(DbExpression* expr);
void dbEvaluateExpression(DbExpression* expr, DbRow* row);
#ifdef EXPR_DISPLAY
void DisplayExpression(DbExpression* expr);
void DisplayExpressionTree(char* str);
#endif

#define STMT_SELECT		's'
#define STMT_INSERT		'i'
#define STMT_UPDATE		'u'
#define STMT_DELETE		'd'
#ifdef ZILOG
#define DB_DIRECTORY	"/usr/db/"
#else
#define DB_DIRECTORY	".\\"
#endif

#define EXPR_VALUE		1
#define EXPR_FIELD		2	/*Unused*/
#define	EXPR_EXPR		3

#define VALTYPE_INT		1
#define VALTYPE_FLOAT	2	/*Unused*/
#define VALTYPE_STRING	3
#define VALTYPE_BOOLEAN	4

#define OPERATOR_LOGIC	5	/* Operator precedence base-nr. for logical operators*/

#endif