iccz80 -mb -v1 -s9 -A -I..\..\include\ -DZILOG db
@if errorlevel 1 goto failure
del db.r01
as-z80 -l -o db.s01
@if errorlevel 1 goto failure

iccz80 -mb -v1 -s9 -A -I..\..\include\ -DZILOG main
@if errorlevel 1 goto failure
del main.r01
as-z80 -l -o main.s01
@if errorlevel 1 goto failure

link-z80 -f db
@if errorlevel 1 goto failure
ihex2bin -l db.i86 ..\..\bin\banked\db
@if errorlevel 1 goto failure

copy test.DAT ..\..\bin\db
copy test.TBL ..\..\bin\db

cd ..\..\bin
call appinst.bat
cd ..\src\db

@echo SUCCESS
@goto done
:failure
@echo FAILURE
:done

