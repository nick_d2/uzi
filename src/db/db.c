#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include "db.h"

void dbDeleteList(DbListEntry* lst)
{
	if(lst!=NULL){
		if(lst->data!=NULL) free(lst->data);
		if(lst->next!=NULL) dbDeleteList(lst->next);
		free(lst);
	}
}

WORD dbListIndex(DbListEntry* lst)
{
	DbListEntry* tmp=lst;
	WORD i=0;
	while(tmp->previous!=NULL){
		tmp=tmp->previous;
		i++;
	}
	return i;
}

WORD dbListSize(DbListEntry* lst)
{
	WORD i;
	DbListEntry* tmp=lst;
	i=dbListIndex(tmp)+1;
	while(tmp->next!=NULL){
		tmp=tmp->next;
		i++;
	}
	return i;
}

void dbSkipLiteral(char ch, char* str, WORD* src, WORD* dst)
{
	str[*dst]=str[*src];
	while(str[++(*src)]!=ch)
		str[++(*dst)]=str[*src];
	str[++*(dst)]=str[*src];
}

void dbTrimString(char* str)
{
	WORD src=0,dst=0;
	char last=' ';

	while(str[src]!=0){
		if(str[src]=='\'' || str[src]=='"')
			dbSkipLiteral(str[src],str,&src,&dst);
		if(last==' ' &&  (str[src]==',' || str[src]==')' || str[src]=='('
			|| str[src]=='+' || str[src]=='-' || str[src]=='*' || str[src]=='/'
			|| str[src]=='&' || str[src]=='|' || str[src]=='=' || str[src]=='<'
			|| str[src]=='>'))
			dst--;

		str[dst]=str[src];

		if((last!=',' && last!=' ' && last!='(' && last!='+' && last!='-' &&
			last!='*' && last!='/' && last!='&' && last!='|' && last!='=' &&
			last!='>' && last!='<')
			|| str[src]!=' ')
			dst++;

		last=str[src];
		src++;
	}
	if(last==' ')
		str[--dst]=0;
	else
		str[dst]=0;
}

char* dbReplaceChar(char* str, char fnd, char rep){
	WORD i=0;
	while(str[i]!=fnd && str[i]!=0x00)
		i++;
	if(str[i]!=0x00) str[i]=rep;
	return str+i;
}

DbListEntry* dbParseList(char* str, char eoe, char eol, WORD* ptr)
{
	WORD eptr=0;
	char element[32];
	BYTE instr=(0==1);
	BYTE inbrace=0;
	DbListEntry* prev=NULL;
	DbListEntry* first;
	DbListEntry* current=malloc(sizeof(DbListEntry));
	first=current;
	current->previous=NULL;
	while((str[*ptr]!=eol || instr) && str[*ptr]!=0){
		switch(str[*ptr]){
		case '\'':
		case '\"':
			instr=!instr;
			break;
		case '(':
			if(!instr)
				inbrace++;
			break;
		case ')':
			if(!instr)
				inbrace--;
		}

		if(str[*ptr]!=eoe || instr || inbrace>0)
			element[eptr]=str[*ptr];
		else {
			element[eptr]=0;
			current->data=malloc(strlen(element)+1);
			strcpy(current->data,element);
			eptr=-1;
			prev=current;
			current->next=malloc(sizeof(DbListEntry));
			current=current->next;
			current->previous=prev;
		}
		(*ptr)++;
		eptr++;
	}
	element[eptr]=0;
	current->data=malloc(strlen(element)+1);
	strcpy(current->data,element);
	current->next=NULL;
	return first;
}

DbStatement* dbParseStatement(char* str)
{
	char* stat=malloc(strlen(str)+1);
	strcpy(stat,str);
	dbTrimString(stat);
#ifdef FULLSQL
	if(strncmp(stat,"SELECT",6)==0)
		return dbParseSelect(stat);
	if(strncmp(stat,"INSERT",6)==0)
		return dbParseInsert(stat);
	if(strncmp(stat,"UPDATE",6)==0)
		return dbParseUpdate(stat);
	if(strncmp(stat,"DELETE",6)==0)
		return dbParseDelete(stat);
#else
	if(strncmp(stat,"SEL",3)==0)
		return dbParseSelect(stat);
	if(strncmp(stat,"INS",3)==0)
		return dbParseInsert(stat);
	if(strncmp(stat,"UPD",3)==0)
		return dbParseUpdate(stat);
	if(strncmp(stat,"DEL",3)==0)
		return dbParseDelete(stat);
#endif
	return NULL;
}

DbStatement* dbParseSelect(char* str)
{
	WORD strptr;
	DbStatement* stat=malloc(sizeof(DbStatement));
	stat->type=STMT_SELECT;
	stat->wheres=NULL;
	stat->auxlist=NULL;
#ifdef FULLSQL
	strptr=6;
#else
	strptr=3;
#endif
	if(str[strptr]==' ') strptr++;

	stat->fields=dbParseList(str,',',' ',&strptr);
#ifdef FULLSQL
	if(strncmp(str+strptr," FROM ",6)==0)
		strptr+=6;
#else
	if(strncmp(str+strptr," FR ",4)==0)
		strptr+=4;
#endif
	else
		return NULL;
	stat->tables=dbParseList(str,',',' ',&strptr);


#ifdef FULLSQL
	if(strncmp(str+strptr," WHERE",6)==0){
		strptr+=6;
#else
	if(strncmp(str+strptr," WH",3)==0){
		strptr+=3;
#endif
		if(str[strptr]==' ') strptr++;
		stat->wheres=dbParseList(str,',',' ',&strptr);
	}
#ifdef FULLSQL
	if(strncmp(str+strptr," ORDER BY ",10)==0){
		strptr+=10;
#else
	if(strncmp(str+strptr," ORD ",5)==0){
		strptr+=5;
#endif
		stat->auxlist=dbParseList(str,',',' ',&strptr);
	}

	return stat;
}

DbStatement* dbParseInsert(char* str)
{
	WORD strptr;
	DbStatement* stat=malloc(sizeof(DbStatement));
	stat->type=STMT_INSERT;
	stat->wheres=NULL;
#ifdef FULLSQL
	strptr=7;
#else
	strptr=4;
#endif
	
	stat->tables=dbParseList(str,',','(',&strptr);
	if(strncmp(str+strptr,"(",1)==0)
		strptr+=1;
	else
		return NULL;
	stat->fields=dbParseList(str,',',')',&strptr);
#ifdef FULLSQL
	if(strncmp(str+strptr,") VALUES(",9)==0){
		strptr+=9;
#else
	if(strncmp(str+strptr,") VAL(",6)==0){
		strptr+=6;
#endif
		stat->auxlist=dbParseList(str,',',')',&strptr);
	} else
		return NULL;
	return stat;
}

DbStatement* dbParseUpdate(char* str)
{
	WORD strptr;
	DbStatement* stat=malloc(sizeof(DbStatement));
	stat->type=STMT_UPDATE;
	stat->wheres=NULL;
	stat->auxlist=NULL;
#ifdef FULLSQL
	strptr=7;
#else
	strptr=4;
#endif

	stat->tables=dbParseList(str,',',' ',&strptr);
#ifdef FULLSQL
	if(strncmp(str+strptr," SET ",5)==0)
		strptr+=5;
#else
	if(strncmp(str+strptr," ST ",4)==0)
		strptr+=4;
#endif
	else
		return NULL;
	stat->fields=dbParseList(str,',',' ',&strptr);

#ifdef FULLSQL
	if(strncmp(str+strptr," WHERE",6)==0){\
		strptr+=6;
#else
	if(strncmp(str+strptr," WH",3)==0){
		strptr+=3;
#endif	
		if(str[strptr]==' ') strptr++;
		stat->wheres=dbParseList(str,',',' ',&strptr);

	}

	return stat;
}

DbStatement* dbParseDelete(char* str)
{
	WORD strptr;
	DbStatement* stat=malloc(sizeof(DbStatement));
	stat->type=STMT_DELETE;
	stat->wheres=NULL;
	stat->auxlist=NULL;
	stat->fields=NULL;
#ifdef FULLSQL
	strptr=7;
#else
	strptr=4;
#endif

	stat->tables=dbParseList(str,',',' ',&strptr);
#ifdef FULLSQL
	if(strncmp(str+strptr," WHERE",6)==0){
		strptr+=6;
		if(str[strptr]==' ') strptr++;
		stat->wheres=dbParseList(str,',',' ',&strptr);

	}
#else
	if(strncmp(str+strptr," WH",3)==0){
		strptr+=3;
		if(str[strptr]==' ') strptr++;
		stat->wheres=dbParseList(str,',',' ',&strptr);
	}
#endif

	return stat;
}

void dbTableOpen(DbTable* tbl)
{
	FILE *ff;
	WORD nfield,i;
	BYTE* buf=malloc(64);
	strcpy(buf,DB_DIRECTORY);
	strcat(buf,tbl->name);
	strcat(buf,".TBL");
	tbl->open=0;
	
	ff=fopen(buf,"rb");
	fread(buf,4,1,ff);
	if(buf[0]!='T' || buf[1]!='B' || buf[2]!='L' || buf[3]!=' ')
		goto einde;
	fread(buf,4,1,ff);
	nfield=buf[0];
	tbl->fields=nfield;
	tbl->fieldspecs=(DbField*) malloc(sizeof(DbField)*nfield);
	for(i=0; i<nfield; i++){
		fread(tbl->fieldspecs[i].name,8,1,ff);
		tbl->fieldspecs[i].name[8]=0;
		fread(&(tbl->fieldspecs[i].datatype),3,1,ff);
		fread(buf,1,1,ff);
	}
	strcpy(buf,DB_DIRECTORY);
	strcat(buf,tbl->name);
	strcat(buf,".DAT");
	tbl->datafile=fopen(buf,"r+b");
	tbl->open=1;
	fread(buf,4,1,tbl->datafile);
	if(buf[0]!='D' || buf[1]!='A' || buf[2]!='T' || buf[3]!=' '){
		tbl->open=0;
		fclose(tbl->datafile);
	}else {
		fread(&(tbl->numrecs),2,1,tbl->datafile);
		fread(&(tbl->unusedrecs),2,1,tbl->datafile);
		fread(buf,2,1,tbl->datafile);
	}
einde:
	fclose(ff);
	free(buf);
}

void dbTableClose(DbTable* tbl)
{
	fclose(tbl->datafile);
	free(tbl->fieldspecs);
	free(tbl);
	tbl=NULL;
}

void dbStatementClose(DbStatement* stat)
{
	dbDeleteList(stat->fields);
	dbDeleteList(stat->tables);
	dbDeleteList(stat->wheres);
	dbDeleteList(stat->auxlist);
	free(stat);
	stat=NULL;
}

void dbResultSetClose(DbResultSet* rs)
{
	fclose(rs->datafile);
	free(rs->fieldspecs);
	free(rs);
	rs=NULL;
}

WORD dbFieldOffset(DbResultSet* tbl, WORD field)
{
	WORD i,s=0;
	for(i=0; i<field; i++)
		s+=tbl->fieldspecs[i].size;
	s+=2;
	return s;
}

WORD dbRecordSize(DbResultSet* tbl)
{
	return dbFieldOffset(tbl,tbl->fields);
}

WORD dbFieldSearch(DbResultSet* tbl,char* str)
{
	WORD i;
	for(i=0; i<tbl->fields; i++)
		if(strcmp(tbl->fieldspecs[i].name,str)==0)
			return i;
	return -1;
}

DbResultSet* dbExecuteQuery(DbStatement* stat)
{
	DbResultSet* rs=malloc(sizeof(DbResultSet));
	DbTable* tbl=malloc(sizeof(DbTable));
	DbField* field;
	DbListEntry* current;
	WORD* offset;
	WORD* size;
	BYTE *src,*dst;
	WORD ptr;
	WORD i,j,rowsize,recsize;
	DbExpression* where;
	WORD sp=0;
	DbRow* dummy=dbNewRow();

	strcpy(tbl->name,stat->tables->data);
	dbTableOpen(tbl);

	rs->rows=0;
	rs->rowptr=0;

	if(strcmp(stat->fields[0].data,"\"*\"")==0){
		rs->fields=tbl->fields;
		offset=malloc(rs->fields*2);
		size=malloc(rs->fields*2);
		rs->fieldspecs=malloc(sizeof(DbField)*rs->fields);
		for(i=0; i<rs->fields; i++){
			field=&(tbl->fieldspecs[i]);
			offset[i]=dbFieldOffset((DbResultSet*) tbl,i);
			size[i]=field->size;
			memcpy(&(rs->fieldspecs[i]),&(tbl->fieldspecs[i]),sizeof(DbField));
		}		
	}else{
		rs->fields=dbListSize(stat->fields);
		offset=malloc(rs->fields*2);
		size=malloc(rs->fields*2);
		rs->fieldspecs=malloc(sizeof(DbField)*rs->fields);
		current=stat->fields;
		for(i=0; i<rs->fields; i++){
			WORD j=dbFieldSearch((DbResultSet*) tbl,current->data);
			field=&(tbl->fieldspecs[j]);
			offset[i]=dbFieldOffset((DbResultSet*) tbl,j);
			size[i]=field->size;
			memcpy(&(rs->fieldspecs[i]),&(tbl->fieldspecs[j]),sizeof(DbField));
			current=current->next;
		}
	}
	recsize=dbRecordSize((DbResultSet*) tbl);
	src=malloc(recsize);
	rowsize=dbRecordSize(rs);
	dst=malloc(rowsize);
	dummy->source=(DbResultSet*) tbl;
	dummy->rownr=0;
	dummy->data=src;

	rs->datafile=fopen("result","wb");
	if(stat->wheres!=NULL)
		where=dbParseExpression(stat->wheres->data,&sp);
	else
		where=dbParseExpression(NULL,&sp);

	for(i=0; i<tbl->numrecs; i++){
		fread(src,recsize,1,tbl->datafile);
		if(src[0]==0x01){
			dst[0]=1;
			dst[1]=0;
			ptr=2;
			for(j=0; j<rs->fields; j++){
				memcpy(dst+ptr,src+offset[j],size[j]);
				ptr+=size[j];
			}
			dbEvaluateExpression(where,dummy);
			if(where->result==1){
				fwrite(dst,rowsize,1,rs->datafile);
				rs->rows++;
			}
		}
	}
	free(src);
	free(offset);
	free(size);
	free(dummy);
	dbDeleteExpression(where);

	dbTableClose(tbl);
	fclose(rs->datafile);
	rs->datafile=fopen("result","r+b");
	return rs;
}

DbResultSet* dbExecuteInsert(DbStatement* stat)
{
	DbTable *tbl=malloc(sizeof(DbTable));
	BYTE* record;
	BYTE* buf;
	DbListEntry *currentf,*currentv;
	DbField* field;
	WORD fields;
	WORD i;
	WORD offset,size;
	WORD recsize;
	strcpy(tbl->name,stat->tables->data);
	dbTableOpen(tbl);

	recsize=dbRecordSize((DbResultSet*) tbl);
	record=malloc(recsize);
	for(i=0; i<recsize; i++)
		record[i]=0;
	buf=malloc(recsize);
	((WORD*)(record))[0]=0x0001;
	fields=dbListSize(stat->fields);
	currentf=stat->fields; 
	currentv=stat->auxlist;
	for(i=0; i<fields; i++){
		WORD j=dbFieldSearch((DbResultSet*) tbl,currentf->data);
		field=&(tbl->fieldspecs[j]);
		offset=dbFieldOffset((DbResultSet*) tbl,j);
		size=field->size;
		if(currentv->data[0]=='\''){
			currentv->data[strlen(currentv->data)-1]=0;
			switch(field->datatype){
			case 'S':
				memcpy(record+offset,currentv->data+1,size);
				break;
			case 'I':
				if(field->size==2)
					*((WORD*) (record+offset))=atoi(currentv->data+1);
				else if(field->size==4)
					*((LONG*) (record+offset))=atol(currentv->data+1);
			}
		}else{
			switch(field->datatype){
			case 'I':
				if(field->size==2)
					*((WORD*) (record+offset))=atoi(currentv->data);
				else if(field->size==4)
					*((LONG*) (record+offset))=atol(currentv->data);
			}
		}
		currentf=currentf->next;
		currentv=currentv->next;
	}

	if(tbl->unusedrecs==0){
		fseek(tbl->datafile,4,SEEK_SET);
		*((WORD*) buf)=tbl->numrecs+1;
		fwrite(buf,2,1,tbl->datafile);
		fseek(tbl->datafile,0L,SEEK_END);
	} else {
		fseek(tbl->datafile,6,SEEK_SET);
		*((WORD*) buf)=tbl->unusedrecs-1;
		fwrite(buf,2,1,tbl->datafile);
		fseek(tbl->datafile,0x0a,SEEK_SET);
		((WORD*) buf)[0]=(WORD) 0xffff;
		while(buf[0]!=0x00)
			fread(buf,recsize,1,tbl->datafile);
		fseek(tbl->datafile,-recsize,SEEK_CUR);
	}
	fwrite(record,recsize,1,tbl->datafile);

	free(record);
	free(buf);

	dbTableClose(tbl);

	return NULL;
}

DbResultSet* dbExecuteDelete(DbStatement* stat)
{
	DbTable *tbl=malloc(sizeof(DbTable));
	BYTE* record;
	WORD i;
	WORD recsize;
	WORD deletes;
	DbExpression* expr;
	DbRow* row;
	strcpy(tbl->name,stat->tables->data);
	dbTableOpen(tbl);
	
	recsize=dbRecordSize((DbResultSet*) tbl);
	record=malloc(recsize);
	i=0;
	if(stat->wheres!=NULL)
		expr=dbParseExpression(stat->wheres->data,&i);
	else
		expr=dbParseExpression(NULL,&i);
	row=dbNewRow();
	row->source=(DbResultSet*) tbl;
	row->data=record;
	row->rownr=0;
	deletes=0;
	for(i=0; i<tbl->numrecs; i++)
	{
		fread(row->data,recsize,1,tbl->datafile);
		if(record[0]==0x01){
			dbEvaluateExpression(expr,row);
			if(expr->result==1){
				record[0]=0;
				fseek(tbl->datafile,-recsize,SEEK_CUR);
				fwrite(row->data,recsize,1,tbl->datafile);
				fseek(tbl->datafile,0,SEEK_CUR);
				deletes++;
			}
		}
	}
	dbDeleteRow(row);
	fseek(tbl->datafile,6,SEEK_SET);
	deletes+=tbl->unusedrecs;
	fwrite(&deletes,2,1,tbl->datafile);

	dbDeleteExpression(expr);
	dbTableClose(tbl);
	return NULL;
}

DbResultSet* dbExecuteUpdate(DbStatement* stat){
	DbTable *tbl=malloc(sizeof(DbTable));
	BYTE* record;
	WORD i,j,k,l;
	WORD recsize;
	WORD nfields;
	DbExpression* expr;
	DbExpression** fieldexpr;
	DbRow* row;
	DbListEntry* current;
	char* str;
	strcpy(tbl->name,stat->tables->data);
	dbTableOpen(tbl);
	
	recsize=dbRecordSize((DbResultSet*) tbl);
	record=malloc(recsize);
	i=0;
	if(stat->wheres!=NULL)
		expr=dbParseExpression(stat->wheres->data,&i);
	else
		expr=dbParseExpression(NULL,&i);
	row=dbNewRow();
	row->source=(DbResultSet*) tbl;
	row->data=record;
	row->rownr=0;
	nfields=dbListSize(stat->fields);
	fieldexpr=malloc(nfields*sizeof(DbExpression*));
	current=stat->fields;

	for(j=0; j<nfields; j++){
		str=dbReplaceChar(current->data,'=',(BYTE) 0x00);
		l=0;
		fieldexpr[j]=dbParseExpression(str+1,&l);
		current=current->next;
	}
	
	for(i=0; i<tbl->numrecs; i++)
	{
		fread(row->data,recsize,1,tbl->datafile);
		if(record[0]==0x01){
			dbEvaluateExpression(expr,row);
			if(expr->result==1){				
				fseek(tbl->datafile,-recsize,SEEK_CUR);
				for(j=0; j<nfields; j++)
					dbEvaluateExpression(fieldexpr[j],row);
				current=stat->fields;
				for(j=0; j<nfields; j++){
					k=dbFieldSearch((DbResultSet*) tbl,current->data);
					l=dbFieldOffset((DbResultSet*) tbl,k);
					switch(tbl->fieldspecs[k].datatype){
					case 'S':
						memcpy(row->data+l,fieldexpr[j]->sresult,tbl->fieldspecs[k].size);
						break;
					case 'I':
						memcpy(row->data+l,&(fieldexpr[j]->result),tbl->fieldspecs[k].size);
						break;
					}
					current=current->next;
				}

				fwrite(row->data,recsize,1,tbl->datafile);
				fseek(tbl->datafile,0,SEEK_CUR);
			}
		}
	}
	dbDeleteRow(row);
	for(j=0; j<nfields; j++)
		dbDeleteExpression(fieldexpr[j]);
	free(fieldexpr);

	dbTableClose(tbl);
	return NULL;
}

DbResultSet* dbExecuteStatement(char* str)
{
	DbStatement* stat;
	DbResultSet* rs=NULL;

	stat=dbParseStatement(str);
	if(stat==NULL)
		return NULL; 
	switch(stat->type){
	case STMT_SELECT:
		rs=dbExecuteQuery(stat); break;
	case STMT_INSERT:
		rs=dbExecuteInsert(stat); break;
	case STMT_DELETE:
		rs=dbExecuteDelete(stat); break;
	case STMT_UPDATE:
		rs=dbExecuteUpdate(stat); break;
	}
	dbStatementClose(stat);
	return rs;
}

void dbNextRow(DbResultSet* rs, DbRow* row)
{
	WORD s;

	s=dbRecordSize(rs);

	if(row->data==NULL){
		row->data=malloc(s);
	} 
	if(rs->rowptr>=rs->rows){
		row->rownr=-1;
		free(row->data);
		row->data=NULL;
	}else{
		row->rownr=rs->rowptr++;
		row->source=rs;
		fread(row->data,s,1,rs->datafile);
	}
}

void dbDeleteRow(DbRow* row)
{
	if(row!=NULL){
		if(row->data!=NULL)
			free(row->data);
		free(row);
	}
}

DbRow* dbNewRow()
{
	DbRow* row=malloc(sizeof(DbRow));
	row->data=NULL;
	row->source=NULL;
	row->rownr=-1;
	return row;
}


void dbGetString(DbRow* row, WORD field, char* dest)
{
	WORD o,i;
	BYTE* dat=(BYTE*)(row->data);
	o=dbFieldOffset(row->source,field);
	i=0;
	switch(row->source->fieldspecs[field].datatype){
	case 'S':
		strcpy(dest,dat+o);
		break;
	case 'I':
		if(row->source->fieldspecs[field].size==2)
			itoa(*((WORD*) (dat+o)),dest,10);
		else if (row->source->fieldspecs[field].size==4)
			ltoa(*((LONG*) (dat+o)),dest,10);
		break;
	}
}

WORD dbGetInt(DbRow* row, WORD field)
{
	WORD o;
	BYTE* dat=(BYTE*)(row->data);
	o=dbFieldOffset(row->source,field);
	switch(row->source->fieldspecs[field].datatype){
	case 'S':
		return atoi(dat+o);
	case 'I':
		return *((WORD*) (dat+o));
	default:
		return 0;
	}
}

LONG dbGetLong(DbRow* row, WORD field)
{
	WORD o;
	BYTE* dat=(BYTE*)(row->data);
	o=dbFieldOffset(row->source,field);
	switch(row->source->fieldspecs[field].datatype){
	case 'S':
		return atol(dat+o);
	case 'I':
		if(row->source->fieldspecs[field].size==2)
			return (LONG) (*((WORD*) (dat+o)));
		else
			return *((LONG*) (dat+o));
	default:
		return 0;
	}
}

BYTE dbOperatorPrecedence(BYTE oper){
	switch(oper){
	case '(':
		return 1;
	case '^':
		return 2;
	case '*':
	case '/':
		return 3;
	case '+':
	case '-':
		return 4;
	case '=':			/* 5 = OPERATOR_LOGIC */
	case '>':
	case '<':
	case ']':
	case '[':
		return 5;
	case '&':
	case '|':
		return 6;
	case ';':
	case ')':
	case 0x00:
		return 63;
	default:
		return 0;
	}
}

DbExpression* dbNewExpression(DbExpression* p, BYTE t){
	DbExpression* r=malloc(sizeof(DbExpression));
	r->eval=0;
	r->expr[0]=NULL;
	r->expr[1]=NULL;
	r->oper=0;
	r->parent=p;
	r->type=t;
	r->result=0;
	return r;
}

DbExpression* dbParseExpression(char* str, WORD* strptr){
	DbExpression *ret,*fetch;
	DbExpression* expr=NULL;
	char value[64];
	WORD prec;
	WORD i;
	WORD valptr=0;
	if(str==NULL){
		ret=dbNewExpression(NULL,EXPR_VALUE);
		ret->value[0]='1';
		ret->value[1]=0;
		return ret;
	}

	for(i=0; i<64; i++)
		value[i]=0;
#ifdef EXPR_DISPLAY
	printf("Parsing: '%s'\n",str+(*strptr));
#endif
	ret=dbNewExpression(NULL,EXPR_EXPR);
	fetch=ret;
#ifdef EXPR_DISPLAY
	DisplayExpression(ret);printf("\n");
#endif
	do{
		expr=NULL;
terug:
		prec=dbOperatorPrecedence(str[*strptr]);
		if(prec==0)
			value[valptr]=str[*strptr];
		else if(prec==1){
			if(str[*strptr]==')')
				prec=63;	
			if(str[*strptr]=='('){
				valptr=0;
				(*strptr)++;
				expr=dbParseExpression(str,strptr);
				goto terug;
			}
		} else {
			value[valptr]=0;
			if(fetch->expr[0]==NULL){
				if(expr==NULL){
					fetch->expr[0]=dbNewExpression(fetch,EXPR_VALUE);
					strcpy(fetch->expr[0]->value,value);
				}else
					fetch->expr[0]=expr;
				fetch->oper=str[*strptr];
			} else {
				if(dbOperatorPrecedence(fetch->oper)>prec){
					fetch->expr[1]=dbNewExpression(fetch,EXPR_EXPR);
					if(expr==NULL){
						fetch->expr[1]->expr[0]=dbNewExpression(fetch->expr[1],EXPR_VALUE);
						strcpy(fetch->expr[1]->expr[0]->value,value);
					}else
						fetch->expr[1]->expr[0]=expr;
					fetch->expr[1]->oper=str[*strptr];
					fetch=fetch->expr[1];
				} else if(prec==63){
					if(expr==NULL){
						fetch->expr[1]=dbNewExpression(fetch,EXPR_VALUE);
						strcpy(fetch->expr[1]->value,value);
					}else
						fetch->expr[1]=expr;
				} else if(dbOperatorPrecedence(fetch->oper)<=prec){
					if(expr==NULL){
						fetch->expr[1]=dbNewExpression(fetch,EXPR_VALUE);
						strcpy(fetch->expr[1]->value,value);
					}else
						fetch->expr[1]=expr;
					while (dbOperatorPrecedence(fetch->oper)<=prec && fetch->parent!=NULL)
					{
						fetch=fetch->parent;
					} 
					if(fetch->parent==NULL && dbOperatorPrecedence(fetch->oper)<=prec){
						ret=dbNewExpression(NULL,EXPR_EXPR);
						ret->expr[0]=fetch;
						ret->oper=str[*strptr];
						fetch=ret;				
					}else if(fetch->parent==NULL && dbOperatorPrecedence(fetch->oper)>prec){						
						DbExpression* dummy;
						dummy=dbNewExpression(fetch,EXPR_EXPR);
						dummy->expr[0]=fetch->expr[1];
						dummy->oper=str[*strptr];
						dummy->expr[0]->parent=dummy;
						fetch->expr[1]=dummy;
						fetch=dummy;
					}else{
						DbExpression* par=fetch->parent;
						par->expr[1]=dbNewExpression(par,EXPR_EXPR);
						par->expr[1]->expr[0]=fetch;
						par->expr[1]->oper=str[*strptr];
						fetch=par->expr[1];
					}
				}
			}	
			valptr=-1;
#ifdef EXPR_DISPLAY
			DisplayExpression(ret);printf("\n");
#endif
		}
		valptr++; (*strptr)++;
	} while(prec<63);
	while(ret->expr[1]==NULL && ret->type==EXPR_EXPR){
		fetch=ret;
		ret=ret->expr[0];
		free(fetch);
	}
	return ret;
}

void dbDeleteExpression(DbExpression* expr){
	if(expr->type==EXPR_EXPR){
		dbDeleteExpression(expr->expr[0]);
		dbDeleteExpression(expr->expr[1]);
	} 
	free(expr);
	
}

#ifdef EXPR_DISPLAY

void DisplayExpression(DbExpression* expr){
	if(expr==NULL)
		printf("..");
	else{
		if(expr->type==EXPR_EXPR){
			printf("(");
			DisplayExpression(expr->expr[0]);
			printf("%c",expr->oper);
			DisplayExpression(expr->expr[1]);
			printf(")");
		} else
			printf("%s",expr->value);
	}
}

void DisplayExpressionTree(char* str){
	DbExpression* expr;
	WORD ptr=0;
	expr=dbParseExpression(str,&ptr);
	DisplayExpression(expr);
	dbDeleteExpression(expr);
	printf("\n");
}

#endif

void dbEvaluateExpression(DbExpression* expr, DbRow* row){
	if(expr->type==EXPR_EXPR){
		dbEvaluateExpression(expr->expr[0],row);
		dbEvaluateExpression(expr->expr[1],row);
		if(dbOperatorPrecedence(expr->oper)<OPERATOR_LOGIC){
			if(expr->expr[0]->valuetype==VALTYPE_INT && expr->expr[1]->valuetype==VALTYPE_INT){
				LONG res;
				expr->valuetype=VALTYPE_INT;
				switch(expr->oper){
				case '+':
					res=expr->expr[0]->result + expr->expr[1]->result;
					break;
				case '-':
					res=expr->expr[0]->result - expr->expr[1]->result;
					break;
				case '*':
					res=expr->expr[0]->result * expr->expr[1]->result;
					break;
				case '/':
					res=expr->expr[0]->result / expr->expr[1]->result;
					break;
				}
				expr->result=res;
			}
		} else {
			if(expr->expr[0]->valuetype!=VALTYPE_STRING && expr->expr[1]->valuetype!=VALTYPE_STRING){
				expr->valuetype=VALTYPE_BOOLEAN;
				switch(expr->oper){
				case '=':
					expr->result=expr->expr[0]->result == expr->expr[1]->result?1:0;
					break;
				case '<':
					expr->result=expr->expr[0]->result < expr->expr[1]->result?1:0;
					break;
				case '>':
					expr->result=expr->expr[0]->result > expr->expr[1]->result?1:0;
					break;
				case '[':
					expr->result=expr->expr[0]->result <= expr->expr[1]->result?1:0;
					break;
				case ']':
					expr->result=expr->expr[0]->result >= expr->expr[1]->result?1:0;
					break;
				case '&':
					expr->result=expr->expr[0]->result & expr->expr[1]->result;
					break;
				case '|':
					expr->result=expr->expr[0]->result | expr->expr[1]->result;
					break;
				}
			}

			if(expr->expr[0]->valuetype==VALTYPE_STRING && expr->expr[1]->valuetype==VALTYPE_STRING){
				WORD cmp=strcmp(expr->expr[0]->sresult,expr->expr[1]->sresult);
				switch(expr->oper){
				case '=':
					expr->valuetype=VALTYPE_BOOLEAN;
					expr->result=(cmp==0)?1:0;
					break;
				case '<':
					expr->valuetype=VALTYPE_BOOLEAN;
					expr->result=(cmp<0)?1:0;
					break;
				case '>':
					expr->valuetype=VALTYPE_BOOLEAN;
					expr->result=(cmp>0)?1:0;
					break;
				case '[':
					expr->valuetype=VALTYPE_BOOLEAN;
					expr->result=(cmp<=0)?1:0;
					break;
				case ']':
					expr->valuetype=VALTYPE_BOOLEAN;
					expr->result=(cmp>=0)?1:0;
					break;
				case '+':
					expr->valuetype=VALTYPE_STRING;
					strcpy(expr->sresult,expr->expr[0]->sresult);
					strcat(expr->sresult,expr->expr[1]->sresult);
					break;
				}
			}
			if(expr->expr[0]->valuetype==VALTYPE_BOOLEAN && expr->expr[1]->valuetype==VALTYPE_BOOLEAN){
				expr->valuetype=VALTYPE_BOOLEAN;
				switch(expr->oper){
				case '&':
					expr->result=expr->expr[0]->result & expr->expr[1]->result;
					break;
				case '|':
					expr->result=expr->expr[0]->result | expr->expr[1]->result;
					break;
				}
			}
		}
		
	} else {	
		WORD i,n,f=-1;
		if(row!=NULL){
			n=row->source->fields;
			for(i=0; i<n; i++){
				if(strcmp(row->source->fieldspecs[i].name,expr->value)==0)
					f=i;
				/*expr->type=EXPR_FIELD;*/
			}
		}
		if(f<0 || row==NULL){
			if(expr->value[0]=='\''){
				expr->valuetype=VALTYPE_STRING;
				strcpy(expr->sresult,expr->value+1);
				expr->sresult[strlen(expr->sresult)-1]=0;
			} else {
				expr->valuetype=VALTYPE_INT;
				expr->result=atol(expr->value);
			}
		}else{
			if(row->source->fieldspecs[f].datatype=='I'){
				expr->valuetype=VALTYPE_INT;
				expr->result=dbGetLong(row,f);
			} else if (row->source->fieldspecs[f].datatype=='S'){
				expr->valuetype=VALTYPE_STRING;
				dbGetString(row,f,expr->sresult);
			}
		}
	}
#ifdef EXPR_DISPLAY
	DisplayExpression(expr);printf("=%ld\n",expr->result);
#endif
}
