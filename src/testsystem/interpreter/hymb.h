// hymb.h

#ifndef _INC_HYMB
#define _INC_HYMB

// ----------------------------------------------------------------------------

typedef struct
     {
     char *pcBase;                      // also the 'handle' for malloc & free
     char *pcLimit;                     // similar to Microsoft's _msize func.
     } MBHANDLE;

// ----------------------------------------------------------------------------

#ifdef _INC_JLHANDLE
// the optional registration of allocated memory blocks is based on a handle
// system identical to JLHANDLE.CPP, see comments for atJLHandle, patJLH etc

extern ARRAYTAG atMBHandle;

extern int hiMBHandleFree; // to head of most recently used free handles list
extern ARRAYTAG *patMBH; // patMBH is slightly more readable than &atMBHandle
#endif

// ----------------------------------------------------------------------------

void MBEntrySetup(void);
void MBExitCleanup(void);

int fiMBAlloc(MBHANDLE *pmbh, int iBytes);
int fiMBRealloc(MBHANDLE *pmbh, int iBytes);
int fiMBFree(MBHANDLE *pmbh);

// ----------------------------------------------------------------------------

inline int iMBSize(MBHANDLE *pmbh)
     {
     return pmbh->pcLimit - pmbh->pcBase;
     }

// ----------------------------------------------------------------------------
// handy functions for the client, not important to the interface:

#ifdef _INC_STDIO
inline void MBAlloc(MBHANDLE *pmbh, int iBytes)
     {
     if (fiMBAlloc(pmbh, iBytes) == FALSE)
          {
          printf("MBAlloc: "
                 "Could not allocate 0x%08x bytes, exiting\n", iBytes);
          exit(1);
          }

#if DEBUG
     printf("MBAlloc: "
            "Allocated memory block 0x%08x size 0x%08x\n", pmbh, iBytes);
#endif
     }

inline void MBRealloc(MBHANDLE *pmbh, int iBytes)
     {
     if (fiMBRealloc(pmbh, iBytes) == FALSE)
          {
          printf("MBRealloc: "
                 "Could not reallocate 0x%08x bytes, exiting\n", iBytes);
          exit(1);
          }

#if DEBUG
     printf("MBRealloc: "
            "Reallocated memory block 0x%08x size 0x%08x\n", pmbh, iBytes);
#endif
     }

inline void MBFree(MBHANDLE *pmbh)
     {
     if (fiMBFree(pmbh) == FALSE)
          {
          printf("MBFree: "
                 "Could not free memory block 0x%08x, exiting\n", pmbh);
          exit(1);
          }

#if DEBUG
     printf("MBFree: "
            "Freed memory block 0x%08x\n", pmbh);
#endif
     }
#endif

// ----------------------------------------------------------------------------

#endif

