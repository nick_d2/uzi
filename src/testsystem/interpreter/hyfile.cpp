// hyfile.cpp

// ----------------------------------------------------------------------------

#include <fcntl.h>
#include <sys/stat.h>
#include <stdio.h>
#ifdef _MSC_VER
#include <io.h>
#include <windows.h>
#pragma hdrstop
#else
#include <unistd.h>
#endif
#include "hymb.h"
#include "hyfs.h"
#include "hyfile.h"

// ----------------------------------------------------------------------------

int fiFileAllocate(FILETAG *pft, int iSize)
     {
     // zero the entire file tag item data area, avoiding leaks
     memset(pft, 0, sizeof(FILETAG));

     // allocate via the lower level wrapper, relying on the fact
     // that the FILETAG structure is compatible with MBHANDLE !!
     if (fiMBAlloc((MBHANDLE *) pft, iSize) == 0)
          {
          return FALSE; // indicates failure to allocate block
          }

     // set position to start and file to null
     pft->pcPosition = pft->pcBase;
     pft->pcAppend = pft->pcBase;

     // say it is now allocated
     return TRUE;
     }

int fiFileFree(FILETAG *pft)
     {
     // free via the lower level wrapper, relying on the fact
     // that the FILETAG structure is compatible with MBHANDLE !!
     if (fiMBFree((MBHANDLE *) pft) == 0)
          {
          return FALSE; // indicates failure to free block
          }

     // zero the entire file tag item data area, avoiding leaks
     memset(pft, 0, sizeof(FILETAG));

     return TRUE; // indicates block freed and tag item zeroed
     }

// ----------------------------------------------------------------------------

int fiFileReadIn(FILETAG *pft, char *pszName)
     {
     int iSize;
     FSHANDLE fsh;

     // zero the entire file tag item data area, avoiding leaks
     memset(pft, 0, sizeof(FILETAG));

     // attempt to open file for reading, get size
     if (fiFSOpen(&fsh, pszName) == FALSE)
          {
          return FALSE; // probably file not found
          }
     iSize = filelength(fsh.iHandle); // assume can get size

#if 0
     // sanity check for file much too large
     if (iSize > 0x100000) // 1 megabyte
          {
          fiFSClose(&fsh);
          return FALSE;
          }
#endif

     // allocate heap to hold entire file
     // note: simpler than the array or pool implementation,
     // because the original limit, position, append info lost
     if (fiFileAllocate(pft, iSize) == FALSE)
          {
          fiFSClose(&fsh);
          return FALSE;
          }

     // attempt read the exact amount of data
     if (iFSRead(&fsh, pft->pcBase, iSize) != iSize)
          {
          fiFileFree(pft);
          fiFSClose(&fsh);
          return FALSE;
          }

     fiFSClose(&fsh);

     // set position to start and length to length read
     pft->pcPosition = pft->pcBase;
     pft->pcAppend = pft->pcBase + iSize;

     return TRUE; // says we read the file
     }

int fiFileWriteOut(FILETAG *pft, char *pszName)
     {
     int iSize;
     FSHANDLE fsh;

     // attempt to create or truncate file for writing
     if (fiFSCreate(&fsh, pszName) == FALSE)
          {
          return FALSE; // probably directory not found
          }

     // attempt to write the exact amount of data
     iSize = iFileSize(pft);
     if (iFSWrite(&fsh, pft->pcBase, iSize) != iSize)
          {
          fiFSClose(&fsh);
          return FALSE;
          }

     fiFSClose(&fsh);
     return TRUE; // says we wrote the file
     }

// ----------------------------------------------------------------------------

int fiFileCopy(FILETAG *pft, FILETAG *pftIn)
     {
     int i, iSize;

     // zero the entire file tag item data area, avoiding leaks
     memset(pft, 0, sizeof(FILETAG));

     // allocate via the lower level wrapper, relying on the fact
     // that the FILETAG structure is compatible with MBHANDLE !!
     iSize = iFileSize(pftIn);
     if (fiMBAlloc((MBHANDLE *) pft, iSize) == FALSE)
          {
          return FALSE; // indicates failure to allocate block
          }

     // calculate the required adjustment, before clobbering pft!
     i = pft->pcBase - pftIn->pcBase;

     // copy the entire file tag item data area, avoiding leaks
     memcpy(pft, pftIn, sizeof(FILETAG));

     // apply the required adjustment to all fields of filetag
     pft->pcBase += i;
     //pft->pcLimit += i;
     pft->pcPosition += i;
     pft->pcAppend += i;

     // another slight adjustment because the copy trims to size
     pft->pcLimit = pft->pcAppend;

     // copy the data area attached to the input file tag
     memcpy(pft->pcBase, pftIn->pcBase, iSize);

     return TRUE; // indicates copied and new tag item filled out
     }

int fiFileCopyPadded(FILETAG *pft, FILETAG *pftIn, int iLeft, int iRight)
     {
     int i, iSize;

     // zero the entire file tag item data area, avoiding leaks
     memset(pft, 0, sizeof(FILETAG));

     // allocate via the lower level wrapper, relying on the fact
     // that the FILETAG structure is compatible with MBHANDLE !!
     iSize = iFileSize(pftIn);
     if (fiMBAlloc((MBHANDLE *) pft, iLeft + iSize + iRight) == FALSE)
          {
          return FALSE; // indicates failure to allocate block
          }

     // calculate the required adjustment, before clobbering pft!
     i = pft->pcBase - pftIn->pcBase;

     // copy the entire file tag item data area, avoiding leaks
     memcpy(pft, pftIn, sizeof(FILETAG));

     // apply the required adjustment to all fields of filetag
     pft->pcBase += i;
     //pft->pcLimit += i;
     pft->pcPosition += iLeft + i;
     pft->pcAppend += iLeft + i + iRight;

     // another slight adjustment because the copy trims to size
     pft->pcLimit = pft->pcAppend;

     // copy the actual data, with padding as specified by caller
     memset(pft->pcBase, 0, iLeft);
     memcpy(pft->pcBase + iLeft, pftIn->pcBase, iSize);
     memset(pft->pcBase + iLeft + iSize, 0, iRight);

     return TRUE; // indicates copied and new tag item filled out
     }

// ----------------------------------------------------------------------------

int fiFileGetLine(FILETAG *pft, char *pszLine, int iLimit, int *piCount)
     {
     int i;

     // indicate eof to caller
     if (pft->pcPosition >= pft->pcAppend)
          {
          // if caller requested the length of the result, return null
          if (piCount != NULL)
               {
               *piCount = 0;
               }

          // the return value from the function indicates eof or not
          return FALSE;
          }

     for (i = 0; i < iLimit; i++)
          {
          // terminate the loop for eof
          if (pft->pcPosition >= pft->pcAppend)
               {
               break;
               }

          // terminate the loop for lf by itself
          if (*pft->pcPosition == 0x0a)
               {
               (pft->pcPosition)++;
               break;
               }

          // terminate the loop for cr optionally skipping lf
          if (*pft->pcPosition == 0x0d)
               {
               (pft->pcPosition)++;
               if (*pft->pcPosition == 0x0a)
                    {
                    pft->pcPosition++;
                    }
               break;
               }

          // copy character to caller buffer
          if (pszLine != NULL)
               {
               pszLine[i] = *pft->pcPosition; // formerly ++
               }
          pft->pcPosition++;
          }

     // null terminate string for caller
     if (pszLine != NULL)
          {
          // see hyprintf.cpp, but i decided to do this differently here!!
          //// sentinel is not always placed, so use the length instead !!
          //// callers wishing to bodgy can use count-1 and poke their own 0
          //if (i < iCount)
          //     {
               pszLine[i] = 0;
          //     }
          }

     // if caller requested the length of the result, return it
     if (piCount != NULL)
          {
          *piCount = i;
          }

     // the return value from the function indicates eof or not
     return TRUE;
     }

int fiFilePutLine(FILETAG *pft, char *pszLine)
     {
     int iTemp;

     // calculate length once
     iTemp = 0;
     if (pszLine != NULL)
          {
          iTemp = strlen(pszLine);
          }

     // do not go past end of memory
     if ((pft->pcPosition + iTemp + 2) > pft->pcLimit)
          {
          return FALSE;
          }

     // copy line and crlf terminator out to heap
     if (pszLine != NULL)
          {
          memcpy(pft->pcPosition, pszLine, iTemp);
          }
     memcpy(pft->pcPosition + iTemp, "\r\n", 2);
     pft->pcPosition += iTemp + 2;

     // set file length possibly truncating
     pft->pcAppend = pft->pcPosition;

     // say we stored a line
     return TRUE;
     }

// ----------------------------------------------------------------------------

