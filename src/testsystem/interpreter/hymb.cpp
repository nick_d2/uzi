// hymb.cpp

// ----------------------------------------------------------------------------

#include <stdio.h>
#include <windows.h>
#pragma hdrstop
#include "hymb.h"

// ----------------------------------------------------------------------------

void MBEntrySetup(void)
     {
     // for future expansion
     }

void MBExitCleanup(void)
     {
     // for future expansion
     }

// ----------------------------------------------------------------------------

int fiMBAlloc(MBHANDLE *pmbh, int iBytes)
     {
     // zero the entire memory block handle area, avoiding leaks
     memset(pmbh, 0, sizeof(MBHANDLE));

     pmbh->pcBase = (char *) malloc(iBytes);
     if (pmbh->pcBase == NULL)
          {
          return FALSE; // indicates failure to allocate block
          }

     pmbh->pcLimit = pmbh->pcBase + iBytes;

     return TRUE; // indicates success and handle set up
     }

int fiMBRealloc(MBHANDLE *pmbh, int iBytes)
     {
     char *pc;

     pc = (char *) realloc(pmbh->pcBase, iBytes);
     if (pc == NULL)
          {
          return FALSE; // indicates failure to reallocate block
          }

     pmbh->pcBase = pc;
     pmbh->pcLimit = pmbh->pcBase + iBytes;

     return TRUE; // indicates success and handle updated
     }

int fiMBFree(MBHANDLE *pmbh)
     {
     if (pmbh->pcBase == NULL)
          {
          return FALSE; // indicates no such block allocated
          }

     free(pmbh->pcBase); // this can't fail, apparently

     // zero the entire memory block handle area, avoiding leaks
     memset(pmbh, 0, sizeof(MBHANDLE));

     return TRUE; // indicates success, and handle zeroed
     }

// ----------------------------------------------------------------------------

#ifdef _INC_JLHANDLE
int hiMBNew(int iBytes)
     {
     int hi;
     MBHANDLE *pmbh;

     // take a new handle, before bothering with memory
     hi = hiJLHandleNew(patMBH);
     pmbh = (MBHANDLE *) pcArrayElement(patMBH, 1, hi);

     // use inline wrapper to allocate the required bytes
     MBAlloc(pmbh, iBytes); // this will bomb on failure

     return hi; // caller's reference is the opaque handle
     }

void MBResize(int hi, int iBytes)
     {
     MBHANDLE *pmbh;

     // find supplied handle, before bothering with memory
     pmbh = (MBHANDLE *) pcArrayElement(patMBH, 1, hi);

     // use inline wrapper to reallocate the required bytes
     MBRealloc(pmbh, iBytes); // this will bomb on failure
     }

void MBDelete(int hi)
     {
     // find supplied handle, before bothering with memory
     pmbh = (MBHANDLE *) pcArrayElement(patMBH, 1, hi);

     // use inline wrapper to free up the allocated memory
     MBFree(pmbh); // this will bomb on failure (null pcBase)
     }

int hiMBDelete(int hi)
     {
     MBDelete(hi);
     return 0; // handy way for caller to zero out its copy of handle
     }
#endif

// ----------------------------------------------------------------------------

