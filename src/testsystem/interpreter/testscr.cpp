// hfmtool.cpp by Nick for Hytech Font Metrics system

// ----------------------------------------------------------------------------

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>
#pragma hdrstop

#ifndef DEBUG
#define DEBUG 0
#endif

#include "hymb.h"
#include "hyfs.h"
#include "hyfile.h"
#include "hystring.h"

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

// ----------------------------------------------------------------------------
// local preprocessor definitions

#define LINE_MAX	0x200
//#define PATH_MAX	0x200

#define OUTPUT_BUFFER	0x10000 // to create output files of up to 64 kbytes

#define ARGUMENT_MAX	0x100 // the maximum number of arguments per command

// ----------------------------------------------------------------------------
// local struct and type definitions

typedef struct
	{
	char *pszVerb;
	int (*pfiHandler)(int argc, char **argv);
	} VERBTAG;

// ----------------------------------------------------------------------------
// local function prototypes

int main(int argc, char **argv);

static int fiExecute(FILETAG *ftIn);

static int iExampleVerb(int argc, char **argv);

// ----------------------------------------------------------------------------
// global variables

VERBTAG vtVerb[] =
	{
	{ "example_verb", iExampleVerb }
	};

#define VERBS (sizeof(vtVerb) / sizeof(VERBTAG))

#if DEBUG
int fiDebug; // this one is really accessible to all modules
#endif

// ----------------------------------------------------------------------------

int main(int argc, char **argv)
	{
	int i;

        FILETAG ftIn;
	char *pszInFileName;

        FILETAG ftOut;
	char *pszOutFileName;
	char szOutFileName[LINE_MAX+1];

	pszInFileName = NULL;
	pszOutFileName = NULL;

	if (argc > 1)
		{
		pszInFileName = argv[1];
		}

#if DEBUG
	fiDebug = 0;
#endif
	if (argc > 2)
		{
#if DEBUG
		i = atoi(argv[2]);
		if (i < 1 || i > 0xff)
			{
#endif
			pszOutFileName = argv[2];
#if DEBUG
			if (argc > 3)
				{
				fiDebug = atoi(argv[3]);
				}
			}
		else
			{
			fiDebug = i;
			}
#endif
		}

	if (pszInFileName == NULL)
		{
		printf("usage: testscr infile.txt [outfile.log]"
#if DEBUG
		       " [debuglevel]"
#endif
		       "\n");
		exit(1);
		}

	if (pszOutFileName == NULL)
		{
		pszOutFileName = szOutFileName;
		strcpy(pszOutFileName, pszInFileName);

		i = strlen(pszOutFileName);
		while (i--)
			{
			if (pszOutFileName[i] == '\\')
				{
				break; /* no extension, so don't strip it */
				}
			if (pszOutFileName[i] == '.')
				{
				pszOutFileName[i] = 0; /* strip dot and extension */
				break; /* ready to concatenate our extension */
				}
			}

		strcat(pszOutFileName, ".log");
		}

	if (!strcmp(pszInFileName, pszOutFileName))
		{
		printf("Input and output filenames identical\n");
		exit(1);
		}

	// read the input txt file entirely to a malloc'd block
	FileReadIn(&ftIn, pszInFileName);

	// interpret commands and construct intermediate files
	if (fiExecute(&ftIn) == FALSE)
		{
		exit(1);
		}

	fiFilePutLine(&ftOut, "this is some sample data");

	// ready to write the output we found
	FileWriteOut(&ftOut, pszOutFileName);

	return 0;
	}

// ----------------------------------------------------------------------------

static int fiExecute(FILETAG *pftIn)
	{
	int i;
	char sz[LINE_MAX+1];
	int argc;
	char *argv[ARGUMENT_MAX];
	int iSize, iSizeArg;
	char *pcArg;

	// execute the user's script file (similar to unix shell)
	while (fiFileGetLine(pftIn, sz, sizeof(sz) - 1, &iSize))
		{
		argc = 0;

		i = iStringIsolate(0, sz, iSize, &pcArg, &iSizeArg);
		while (iSizeArg && argc < ARGUMENT_MAX)
			{
			argv[argc++] = pcArg; // stash reference to string
			pcArg[iSizeArg] = 0; // add null terminator

			i = iStringIsolate(i + 1, sz, iSize,
					          &pcArg, &iSizeArg);
			}

		if (argc < 1 || argv[0][0] == '#')
			{
			continue; // comments and blank file lines ignored
			}

#if DEBUG
		for (i = 0; i < argc; i++)
			{
			printf("Argument %d = \"%s\"\n", i, argv[i]);
			}
#endif

		for (i = 0; i < VERBS; i++)
			{
			if (strcmp(argv[0], vtVerb[i].pszVerb) == 0)
				{
				break;
				}
			}

		if (i >= VERBS)
			{
			printf("Unrecognised command \"%s\"\n", argv[0]);
			return FALSE;
			}

		i = (vtVerb[i].pfiHandler)(argc, argv);
#if DEBUG
		printf("Exitcode = %d\n", i);
#endif
		if (i)
			{
			printf("Command \"%s\" reports failure\n", argv[0]);
			return FALSE;
			}
		}

	return TRUE;
	}

// ----------------------------------------------------------------------------

int iExampleVerb(int argc, char **argv)
	{
	if (argc < 2)
		{
		printf("usage: blah blah\n");
		return 1;
		}

	printf("your argument was %s\n", argv[1]);
	return 0;
	}

// ----------------------------------------------------------------------------

