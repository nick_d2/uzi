// hyfile.h

#ifndef _INC_HYFILE
#define _INC_HYFILE

// ----------------------------------------------------------------------------

typedef struct
{
     char *pcBase;                 // also the handle for the malloc'd buffer
     char *pcLimit;                // offset just after end of malloc'd buffer
     char *pcPosition;             // offset of the next char to be clobbered
     char *pcAppend;               // offset just after end of valid char data
} FILETAG;

// ----------------------------------------------------------------------------

int fiFileAllocate(FILETAG *pft, int iSize);
int fiFileFree(FILETAG *pft);

int fiFileReadIn(FILETAG *pft, char *pszName);
int fiFileWriteOut(FILETAG *pft, char *pszName);

int fiFileCopy(FILETAG *pft, FILETAG *pftIn);
int fiFileCopyPadded(FILETAG *pft, FILETAG *pftIn, int iLeft, int iRight);

int fiFileGetLine(FILETAG *pft, char *pszLine, int iLimit, int *piCount);
int fiFilePutLine(FILETAG *pft, char *pszLine);

// ----------------------------------------------------------------------------

inline int iFileSize(FILETAG *pft)
     {
     // return size in bytes, for use by FileReadIn etc family
     return pft->pcAppend - pft->pcBase; // note: the trimmed size
     }

// ----------------------------------------------------------------------------
// handy functions for the client, not important to the interface:

#ifdef _INC_STDIO
inline void FileAllocate(FILETAG *pft, int iSize)
     {
     if (fiFileAllocate(pft, iSize) == FALSE)
          {
          printf("FileAllocate: Could not allocate 0x%08x bytes\n", iSize);
          exit(1);
          }

#if DEBUG
     printf("FileAllocate: "
            "Allocated file 0x%08x, 0x%08x bytes\n", pft, iSize);
#endif
     }

inline void FileFree(FILETAG *pft)
     {
     if (fiFileFree(pft) == FALSE)
          {
          printf("FileFree: Could not free file 0x%08x\n", pft);
          exit(1);
          }

#if DEBUG
     printf("FileFree: Freed file 0x%08x\n", pft);
#endif
     }

inline void FileCopy(FILETAG *pft, FILETAG *pftIn)
     {
     if (fiFileCopy(pft, pftIn) == FALSE)
          {
          printf("FileCopy: Could not copy 0x%08x bytes\n", iFileSize(pftIn));
          exit(1);
          }
     }

inline void FileCopyPadded(FILETAG *pft, FILETAG *pftIn, int iLeft, int iRight)
     {
     if (fiFileCopyPadded(pft, pftIn, iLeft, iRight) == FALSE)
          {
          printf("FileCopyPadded: Could not copy 0x%08x bytes\n",
                 iFileSize(pftIn) + iLeft + iRight);
          exit(1);
          }
     }

inline void FileReadIn(FILETAG *pft, char *pszName)
     {
     if (fiFileReadIn(pft, pszName) == FALSE)
          {
          printf("FileReadIn: Could not read %s\n", pszName);
          exit(1);
          }

     printf(
#if DEBUG
            "FileReadIn: "
#endif
	    "Loaded %s, 0x%08x bytes\n", pszName, iFileSize(pft));
     }

inline void FileWriteOut(FILETAG *pft, char *pszName)
     {
     if (fiFileWriteOut(pft, pszName) == FALSE)
          {
          printf("FileWriteOut: Could not write %s\n", pszName);
          exit(1);
          }

     printf(
#if DEBUG
            "FileWriteOut: "
#endif
            "Created %s, 0x%08x bytes\n", pszName, iFileSize(pft));
     }
#endif

// ----------------------------------------------------------------------------

#endif

