/*
 * tter.c for uzi180 by Nick & Rob
 *
 * Allows recording and playback of binary data received from/to a serial port
 */
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>

#ifdef POSIX_TERMIOS
#include <termios.h>
#else
#include <sgtty.h>
#endif

#include <unistd.h>

#include <sys/time.h>
#include <sys/select.h>

/* ------------------------------------------------------------------------- */
/* definitions */

#define DEF_BAUDRATE	38400
#define DEF_DELAY	100  /* ms */

#define USAGE	"usage: %s <-r>|<-p [-d <delay ms>]> [-s <baudrate>] <devicename> <filename>"
#define BUFSIZE 0x80
#define P_FLAG	1
#define	R_FLAG	2
#define	S_FLAG	4
#define	D_FLAG	8

typedef struct
{
	int flags;	/* command line options flags */
	int speed;	/* terminal baudrate */
	int delay;	/* delay between characters during playback */
	char *tname;	/* terminal device name */
	char *pname;	/* this program's name */
	char *fname;	/* filename for recording/playback */
} cmdlineT;


/* ------------------------------------------------------------------------- */
/* prototypes */

void process_esc(char *buf, int *count);
void exit_error(char *mess1, char *mess2);
void exit_restore(void);
void entry_setup(void);
int get_baudrate(int speed);
int parse_cmd_line(int argc, char **argv);
int main(int argc, char **argv);

/* ------------------------------------------------------------------------- */
/* global variables */

int fd_in, fd_out, fd_ter;
FILE *file;
cmdlineT cmdline;
char buf[BUFSIZE];
char usage[80];


#ifdef POSIX_TERMIOS
struct termios state_in, state_ter;
#else
int raw_in, raw_ter;
#endif

/* ------------------------------------------------------------------------- */

void exit_error(char *mess1, char *mess2)
{
        char header[20];
	exit_restore();
        sprintf(header, "%s: ", cmdline.pname);
	write(fileno(stderr), header, strlen(header));
	write(fileno(stderr), mess1, strlen(mess1));
	perror(mess2);
	exit(1);
}

void exit_restore(void)
{
#ifdef POSIX_TERMIOS
	struct termios state;
#else
	struct sgttyb state;
#endif

	if (fd_in >= 0 && isatty(fd_in))
	{
#ifdef POSIX_TERMIOS
		tcsetattr(fd_in, TCSADRAIN, &state_in);
#else
		gtty(fd_in, &state);
		state.sg_flags = raw_in;
		stty(fd_in, &state);
#endif
	}

	if (fd_ter >= 0 && isatty(fd_ter))
	{
#ifdef POSIX_TERMIOS
		tcsetattr(fd_ter, TCSADRAIN, &state_ter);
#else
		gtty(fd_ter, &state);
		state.sg_flags = raw_ter;
		stty(fd_ter, &state);
#endif
	}

	close(fd_ter);
	fclose(file);
}

void entry_setup(void)
{
#ifdef POSIX_TERMIOS
	struct termios state;
#else
	struct sgttyb state;
#endif

	fd_in = fileno(stdin);
	fd_out = fileno(stdout);

	if (isatty(fd_in))
	{
#ifdef POSIX_TERMIOS
		tcgetattr(fd_in, &state);
		memcpy(&state_in, &state, sizeof(struct termios));

		state.c_iflag &= (IGNBRK | IGNCR | INLCR | ICRNL | IUCLC |
				IXANY | IXON | IXOFF | INPCK | ISTRIP);
		state.c_iflag |= (BRKINT | IGNPAR);
		state.c_oflag &= ~OPOST;

#ifdef XCASE	/* Actually non-POSIX */
		state.c_lflag &= ~(XCASE | ECHONL | NOFLSH |
				 ICANON | ISIG | ECHO);
#else
		state.c_lflag &= ~(ECHONL | NOFLSH |
				 ICANON | ISIG | ECHO);
#endif

		state.c_cflag |= CREAD;
		state.c_cc[VTIME] = 5;
		state.c_cc[VMIN] = 1;

		tcsetattr(fd_in, TCSADRAIN, &state);
#else
		gtty(fd_in, &state);
		raw_in = state.sg_flags;
		state.sg_flags = RAW | UNBUFF;
		stty(fd_in, &state);
#endif
	}

	if (isatty(fd_ter))
	{
#ifdef POSIX_TERMIOS
		/* Flush any unread & unwritten data */
		tcflush(fd_ter, TCIOFLUSH);

		tcgetattr(fd_ter, &state);
		memcpy(&state_ter, &state, sizeof(struct termios));

#if 0
		cfmakeraw(&state); /* Not always supported (e.g. Cygwin) */
#else
		/* Alternative for cfmakeraw() */
		state.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL|IXON);
		state.c_oflag &= ~OPOST;
		state.c_lflag &= ~(ECHO|ECHONL|ICANON|ISIG|IEXTEN);
		state.c_cflag &= ~(CSIZE|PARENB);
		state.c_cflag |= CS8;
#endif

		if (!(cmdline.flags & S_FLAG))
		{
			/* Set default baudrate */
			cmdline.speed = DEF_BAUDRATE;
			fprintf(stderr, "%s: baudrate = %d\n", cmdline.pname, cmdline.speed);
		}

		state.c_ispeed = translate_speed(cmdline.speed);
		state.c_ospeed = translate_speed(cmdline.speed);

		tcsetattr(fd_ter, TCSANOW, &state);

#else
		gtty(fd_ter, &state);
		raw_ter = state.sg_flags;
		state.sg_flags = RAW | UNBUFF;
		stty(fd_ter, &state);
#endif

	}
}

void process_esc(char *buf, int *count)
{
	switch(buf[0])
	{
	case 'x':
	case 'X':
		exit_restore();
		fprintf(stderr, "\n%s: exiting\n", cmdline.pname);
		exit(0);

	case 0x1b:
		return; /* with count = 1, so as to write it */

	default:
		*count = 0;
		return;
	}
}

int translate_speed(long speed)
{
	int val;

	switch(speed)
	{
	case 1200:
		val = B1200;
		break;
	case 1800:
		val = B1800;
		break;
	case 2400:
		val = B2400;
		break;
	case 4800:
		val = B4800;
		break;
	case 9600:
		val = B9600;
		break;
	case 19200:
		val = B19200;
		break;
	case 38400:
		val = B38400;
		break;
	case 57600:
		val = B57600;
		break;
	case 115200:
		val = B115200;
		break;
	case 128000:
		val = B128000;
		break;
	default:
		val = translate_speed(DEF_BAUDRATE);
		break;
	}

	return(val);
}

int parse_cmd_line(int argc, char **argv)
{
	int c, errflg = 0;
	extern char *optarg;
	extern int optind;

	cmdline.flags = 0;
	cmdline.speed = 0;
	cmdline.delay = 0;
	cmdline.tname = NULL;
	cmdline.fname = NULL;

	while ((c = getopt(argc, argv, "rps:d:")) != EOF)
	{
		switch(c)
		{
		/* 'r' and 'p' opts are mutually exclusive */
		case 'r':
			if (cmdline.flags & P_FLAG)
				errflg++;
			else
				cmdline.flags |= R_FLAG;
			break;
		case 'p':
			if (cmdline.flags & R_FLAG)
				errflg++;
			else
				cmdline.flags |= P_FLAG;
			break;
		case 's':
			cmdline.speed = atoi(optarg);
			cmdline.flags |= S_FLAG;
			break;
		case 'd':
			if (cmdline.flags & P_FLAG)
			{
				cmdline.delay = atoi(optarg);
				cmdline.flags |= D_FLAG;
			}
			else
				errflg++;
			break;
		default:
			errflg++;
		}

		if (errflg) return(-1);
	}

	if (argc - optind != 2) return(-1);

	cmdline.tname = argv[optind];
	cmdline.fname = argv[++optind];

	return 0;
}

int main(int argc, char **argv)
{
	int escflag, count;
	char *mode;

	cmdline.pname = argv[0];
	sprintf(usage, USAGE, cmdline.pname);

	if (parse_cmd_line(argc, argv) < 0)
	{
		(void)fprintf(stderr, "%s\n", usage);
		exit(1);
	}

	fd_in = -1;
	fd_out = -1;

	fd_ter = open(cmdline.tname, O_RDWR);

	if (fd_ter < 0)
	{
		exit_error("can't open ", cmdline.tname);
	}

	entry_setup();

	escflag = 0;

	if (cmdline.flags & P_FLAG)
	{
		/*
		 * PLAYBACK
		 */
		mode = "rb";
		file = fopen(cmdline.fname, mode);

		if (file == NULL)
		{
			exit_error("can't open ", cmdline.fname);
		}

		count = fread(buf, 1, BUFSIZE, file);

		while (count)
		{
			int i;

			if (ferror(file) != 0)
			{
				exit_error("can't read ", cmdline.fname);
			}

			if (feof(file))
			{
				if (buf[count-1] == 0x0a)
					if (buf[count-2] == 0x0d)
						/* DOS: skip 0x0d 0x0a */
						count -= 2;
					else
						/* UNIX: skip 0x0a */
						count -= 1;
			}

			for (i = 0; i < count; i++)
			{
				struct timeval t;

				t.tv_sec = 0;

				if (cmdline.flags & D_FLAG)
					t.tv_usec = cmdline.delay*1000; 
				else
					t.tv_usec = DEF_DELAY*1000;

				if (write(fd_ter, &buf[i], 1) != 1)
				{
					exit_error("can't write ", cmdline.tname);
				}

#ifdef POSIX_TERMIOS
				/* Wait for output to complete */
				tcdrain(fd_ter);
#endif

				/* User selected or default delay */
				(void)select(0, NULL, NULL, NULL, &t);
			}

			count = fread(buf, 1, BUFSIZE, file);
		}

		fprintf(stderr, "%s: ready\n", cmdline.pname);
	}
	else if (cmdline.flags & R_FLAG)
	{
		/*
		 * RECORDING
		 */
		fprintf(stderr, "%s: recording; hit any key to stop\n", cmdline.pname);
		fflush(stderr);

		/*
		 * RPB: The umask() doesn't appear to work correctly under
		 * Cygwin hence the weird "wb+" mode to specify a file which
		 * permits reading & writing. Should also work under Linux and
		 * other UNIX versions.
		 */
		//umask(0133); /* == 644 file permission */
		mode = "wb+";
		file = fopen(cmdline.fname, mode);

		if (file == NULL)
		{
			exit_error("can't open ", cmdline.fname);
		}

		while (1)
		{
			fd_set readfds;
			int fdmax, fdrdy;

			FD_ZERO(&readfds);
			FD_SET(fd_in, &readfds);
			FD_SET(fd_ter, &readfds);

			fdmax = fd_in > fd_ter ? fd_in : fd_ter;
			fdrdy = select(fdmax+1, &readfds, NULL, NULL, NULL);

			if (fdrdy < 0)
			{
				exit_error("can't select on read ", "select");
			}

			if (FD_ISSET(fd_in, &readfds))
			{
				fprintf(stderr, "\n%s: stopped\n", cmdline.pname);
				break;
			}

 			if (FD_ISSET(fd_ter, &readfds))
			{
				count = read(fd_ter, buf, BUFSIZE);
				if (count < 0)
				{
					exit_error("can't read ", cmdline.tname);
				}

				if (count)
				{
					if (write(fd_out, buf, count) != count)
					{
						exit_error("can't write ", "stdout");
					}

					if ((fwrite(buf, 1, count, file) != count) ||
					    (ferror(file) != 0))
					{
						exit_error("can't write ", cmdline.fname);
					}
				}
			}
		}
        }
        else
	{
		exit_restore();
		fprintf(stderr, "%s\n", usage);
		exit(1);
	}

	exit_restore();
	exit(0);
}

/* ------------------------------------------------------------------------- */
