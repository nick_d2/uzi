# apibus.txt

# bmp2txt infile.bmp [outfile.txt]
bmp2txt sans07r.bmp
bmp2txt sans07b.bmp
bmp2txt sans09r.bmp
bmp2txt sans12r.bmp
bmp2txt sans16r.bmp
bmp2txt sans07c.bmp

# txt2chs infile.txt [outfile.chs] x_space y_space
txt2chs sans07r.txt 1 3
txt2chs sans07b.txt 1 3
txt2chs sans09r.txt 1 4
txt2chs sans12r.txt 2 5
txt2chs sans16r.txt 2 7
txt2chs sans07c.txt 1 3

# chs2cmd infile.chs [outfile.cmd] slot
chs2cmd sans07r.chs lcd0.cmd 1
chs2cmd sans07b.chs lcd0.cmd 2
chs2cmd sans09r.chs lcd0.cmd 3
chs2cmd sans12r.chs lcd0.cmd 4
chs2cmd sans16r.chs lcd0.cmd 5
chs2cmd sans07c.chs lcd0.cmd 6
chs2cmd sans07r.chs lcd1.cmd 1
chs2cmd sans07b.chs lcd1.cmd 2
chs2cmd sans09r.chs lcd1.cmd 3
chs2cmd sans12r.chs lcd1.cmd 4
chs2cmd sans16r.chs lcd1.cmd 5

# chs2hfm infile.chs device slot family style
chs2hfm sans07r.chs 0 1 sans regular
chs2hfm sans07b.chs 0 2 sans bold
chs2hfm sans09r.chs 0 3 sans regular
chs2hfm sans12r.chs 0 4 sans regular
chs2hfm sans16r.chs 0 5 sans regular
chs2hfm sans07c.chs 0 6 sans condensed
chs2hfm sans07r.chs 1 1 sans regular
chs2hfm sans07b.chs 1 2 sans bold
chs2hfm sans09r.chs 1 3 sans regular
chs2hfm sans12r.chs 1 4 sans regular
chs2hfm sans16r.chs 1 5 sans regular

