/* wid-demo.c by Nick for Hytech Font Manager system */

#include <stdio.h>

signed char widths[5][0x100] =
	{
		{
#include "set00nld.c"
		},
		{
#include "set01nld.c"
		},
		{
#include "set02nld.c"
		},
		{
#include "set03nld.c"
		},
		{
#include "set04nld.c"
		}
	};

int main(int argc, char **argv)
	{
	int slot, width;
	char *text, *p;

	if (argc < 3)
		{
		printf("usage: wid-demo slot string\n");
		return 1;
		}

	slot = atoi(argv[1]);
	text = argv[2];

	if (slot < 0 || slot > 4)
		{
		printf("Slot must be in the range 0 thru 4\n");
		return 1;
		}

	p = text;
	width = 0;
	while (*p)
		{
		width += widths[slot][*p++];
		}

	printf("Width in character set %d of \"%s\" is %d\n",
	       slot, text, width);
	return 0;
	}

