hfmtool apibus.txt
@if errorlevel 1 goto failure

copy lcd0.cmd ..\..\bin\font
copy lcd1.cmd ..\..\bin\font
copy apibus.hfm ..\..\bin\font

hfmwidth apibus.hfm set00nld.c 0 0
@if errorlevel 1 goto failure

hfmwidth apibus.hfm set01nld.c 0 1
@if errorlevel 1 goto failure

hfmwidth apibus.hfm set02nld.c 0 2
@if errorlevel 1 goto failure

hfmwidth apibus.hfm set03nld.c 0 3
@if errorlevel 1 goto failure

hfmwidth apibus.hfm set04nld.c 0 4
@if errorlevel 1 goto failure

cl -Zi -I. wid-demo.c
@if errorlevel 1 goto failure

@echo SUCCESS
@goto done
:failure
@echo FAILURE
:done

