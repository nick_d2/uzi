/*   ADVENTURE -- Jim Gillogly, Jul 1977
 * This program is a re-write of ADVENT, written in FORTRAN mostly by
 * Don Woods of SAIL.  In most places it is as nearly identical to the
 * original as possible given the language and word-size differences.
 * A few places, such as the message arrays and travel arrays were changed
 * to reflect the smaller core size and word size.  The labels of the
 * original are reflected in this version, so that the comments of the
 * fortran are still applicable here.
 *
 * The data file distributed with the fortran source is assumed to be called
 * "glorkz" in the directory where the program is first run.
 */

/* static char hdr_sccsid[] = "	hdr.h	4.1	82/05/11	"; */

#ifndef VAX /* Nick */
#define read _read /* Nick, very temporary */
#define write _write /* Nick, very temporary */
int ioctl(int fd, int request, ...); /* IAR don't use regparams! */
int open(char *name, unsigned int flag, ...); /* IAR don't use regparams! */

#include <pwd.h>
#include <unistd.h> /* Nick, for execl() to execve() translation prototype */
#include <sys/wait.h> /* Nick, for wait() to waitpid() translation macro */

#define PWD_BUFFER_SIZE 256
#endif

#include <stdio.h> /* Nick */
#include <malloc.h> /* Nick */
#include <string.h> /* Nick */

/* hdr.h: included by c advent files */

#if 1 /* Nick */
extern char username[PWD_BUFFER_SIZE];
#endif

extern int setup;                       /* changed by savec & init      */
extern int datfd;                              /* message file descriptor      */
extern int delhit;
extern int yea;

#define TAB     011
#define LF      012
#define FLUSHLINE while (getchar()!='\n')
#define FLUSHLF   while (next()!=LF)

extern int loc,newloc,oldloc,oldlc2,wzdark,SHORT,gaveup,kq,k,k2;
extern char *wd1,*wd2;                         /* the complete words           */
extern int verb,obj,spk;
extern int blklin;
extern int saved,savet,mxscor,latncy;

#define MAXSTR  20                      /* max length of user's words   */

#define HTSIZE  512                     /* max number of vocab words    */
extern struct hashtab                          /* hash table for vocabulary    */
{       int val;                        /* word type &index (ktab)      */
	char *atab;                     /* pointer to actual string     */
} voc[HTSIZE];

#define DATFILE "glorkz"                /* all the original msgs        */
#define TMPFILE "tmp.foo.baz"           /* just the text msgs           */
#define LIBFILE "/lib/adventure.dat" /* Nick core image (NOT an executable!) */

struct text
{       int seekadr;                    /* DATFILE must be < 2**16      */
	int txtlen;                     /* length of msg starting here  */
};

#define RTXSIZ  205
extern struct text rtext[RTXSIZ];              /* random text messages         */

#define MAGSIZ  35
extern struct text mtext[MAGSIZ];              /* magic messages               */

extern int clsses;
#define CLSMAX  12
extern struct text ctext[CLSMAX];              /* classes of adventurer        */
extern int cval[CLSMAX];

extern struct text ptext[101];                 /* object descriptions          */

#define LOCSIZ  141                     /* number of locations          */
extern struct text ltext[LOCSIZ];              /* long loc description         */
extern struct text stext[LOCSIZ];              /* short loc descriptions       */

extern struct travlist                         /* direcs & conditions of travel*/
{       struct travlist *next;          /* ptr to next list entry       */
	int conditions;                 /* m in writeup (newloc / 1000) */
	int tloc;                       /* n in writeup (newloc % 1000) */
	int tverb;                      /* the verb that takes you there*/
} *travel[LOCSIZ],*tkk;                 /* travel is closer to keys(...)*/

extern int atloc[LOCSIZ];

extern int  plac[101];                         /* initial object placement     */
extern int  fixd[101],fixed[101];              /* location fixed?              */

extern int actspk[35];                         /* rtext msg for verb <n>       */

extern int cond[LOCSIZ];                       /* various condition bits       */

extern int setbit[16];                  /* bit defn masks 1,2,4,...     */

extern int hntmax;
extern int hints[20][5];                       /* info on hints                */
extern int hinted[20],hintlc[20];

extern int place[101], prop[101], linkx[201];
extern int abb[LOCSIZ];

extern int maxtrs,tally,tally2;                /* treasure values              */

#define FALSE   0
#define TRUE    1

extern int keys,lamp,grate,cage,rod,rod2,steps,/* mnemonics                    */
	bird,door,pillow,snake,fissur,tablet,clam,oyster,magzin,
	dwarf,knife,food,bottle,water,oil,plant,plant2,axe,mirror,dragon,
	chasm,troll,troll2,bear,messag,vend,batter,
	nugget,coins,chest,eggs,tridnt,vase,emrald,pyram,pearl,rug,chain,
	spices,
	back,look,cave,null,entrnc,dprssn,
	say,lock,throw,find,invent;

extern int chloc,chloc2,dseen[7],dloc[7],      /* dwarf stuff                  */
	odloc[7],dflag,daltlc;

extern int tk[21],stick,dtotal,attack;
extern int turns,lmwarn,iwest,knfloc,detail,   /* various flags & counters     */
	abbnum,maxdie,numdie,holdng,dkill,foobar,bonus,clock1,clock2,
	/*saved,*/closng,panic,closed,scorng;

extern int demo,/*newloc,*/limit;

