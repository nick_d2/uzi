iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ data
@if errorlevel 1 goto failure
del data.r01
as-z80 -l -o data.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ done
@if errorlevel 1 goto failure
del done.r01
as-z80 -l -o done.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ init
@if errorlevel 1 goto failure
del init.r01
as-z80 -l -o init.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ io
@if errorlevel 1 goto failure
del io.r01
as-z80 -l -o io.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ main
@if errorlevel 1 goto failure
del main.r01
as-z80 -l -o main.s01
@if errorlevel 1 goto failure

rem iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ okplay
rem @if errorlevel 1 goto failure
rem del okplay.r01
rem as-z80 -l -o okplay.s01
rem @if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ save
@if errorlevel 1 goto failure
del save.r01
as-z80 -l -o save.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ subr
@if errorlevel 1 goto failure
del subr.r01
as-z80 -l -o subr.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ vocab
@if errorlevel 1 goto failure
del vocab.r01
as-z80 -l -o vocab.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ wizard
@if errorlevel 1 goto failure
del wizard.r01
as-z80 -l -o wizard.s01
@if errorlevel 1 goto failure

link-z80 -f advent
@if errorlevel 1 goto failure
ihex2bin -l advent.i86 advent
@if errorlevel 1 goto failure

@echo SUCCESS
@goto done
:failure
@echo FAILURE
:done

