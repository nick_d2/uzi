/* save (III)   J. Gillogly
 * save user core image for restarting
 * usage: save(<command file (argv[0] from main)>,<output file>)
 * bugs
 *   -  impure code (i.e. changes in instructions) is not handled
 *      (but people that do that get what they deserve)
 */

static char sccsid[] = "	save.c	4.1	82/05/11	";

#if 1 /* Nick */
#include "hdr.h"
#else
#include <a.out.h>
#endif

#if 1 /* Nick */
unsigned int coresize, heapsize, filesize; /* accessible to caller */
extern char s_IDATA0, s_CSTACK, e_CSTACK; /* violently system dependent */
extern char s_savegame, e_savegame; /* quite system dependent */

int save(char *outfile, int mode);
int rest(char *infile, int mode);
unsigned int savearea(int fd, char *start, unsigned int count);
unsigned int restarea(int fd, char *start, unsigned int count);
#else
int filesize;                    /* accessible to caller         */

char *sbrk();
#endif

#if 1 /* Nick */
int save(char *outfile, int mode)
	{
	int fd;
	unsigned int count;

#if 0
	/* this restriction is so that we don't try */
	/* to write over the commnd file */
	fd = open(outfile, 0);
	if (fd != -1)
		{
		printf("Can't use an existing file\n");
		close(fd);
		return -1;
		}
#endif

	fd = creat(outfile, 0644);
	if (fd == -1)
		{
		/* printf("Cannot create %s\n", outfile); */
		return -1;
		}

	if (mode)
		{
		count = &e_savegame - &s_savegame;
		if (savearea(fd, &s_savegame, count) == -1)
			{
			return -1; /* fd has been closed by savearea() */
			}

		close(fd);
		return 0; /* for mode == 1, we close fd before returning */
		}

	coresize = &s_CSTACK - &s_IDATA0;
	heapsize = (char *)sbrk(0) - &e_CSTACK;

	count = savearea(fd, &s_IDATA0, coresize); /* write core */
	if (count == -1)
		{
		return -1; /* fd has been closed by savearea() */
		}
	filesize = count;

	count = savearea(fd, &e_CSTACK, heapsize); /* write heap */
	if (count == -1)
		{
		return -1; /* fd has been closed by savearea() */
		}
	filesize += count;

	return fd; /* new: caller needs to close it */
	}

int rest(char *infile, int mode)
	{
	int fd;
	unsigned int count;

	fd = open(infile, 0);
	if (fd < 0)
		{
		/* printf("Cannot open %s\n", infile); */
		return -1;
		}

	if (mode)
		{
		count = &e_savegame - &s_savegame;
		if (restarea(fd, &s_savegame, count) == -1)
			{
			return -1; /* fd has been closed by restarea() */
			}

		close(fd);
		return 0; /* for mode == 1, we close fd before returning */
		}

	coresize = &s_CSTACK - &s_IDATA0;

	count = restarea(fd, &s_IDATA0, coresize); /* write core */
	if (count == -1)
		{
		return -1; /* fd has been closed by restarea() */
		}
	filesize = count;

	if (brk(&e_CSTACK + heapsize) == -1)
		{
		/* printf("Not enough memory\n"); */
		close(fd);
		return -1;
		}

	count = restarea(fd, &e_CSTACK, heapsize); /* write heap */
	if (count == -1)
		{
		return -1; /* fd has been closed by restarea() */
		}
	filesize += count;

	return fd; /* new: caller needs to close it */
}

unsigned int savearea(int fd, char *start, unsigned int count)
	{
	if (write(fd, &count, sizeof(count)) != sizeof(count) ||
			write(fd, start, count) != count)
		{
		close(fd);
		return -1;
		}

	return sizeof(unsigned int) + count;
	}

unsigned int restarea(int fd, char *start, unsigned int count)
	{
	unsigned int temp;

	if (read(fd, &temp, sizeof(temp)) != sizeof(temp) ||
			temp != count ||
			read(fd, start, count) != count)
		{
		close(fd);
		return -1;
		}

	return sizeof(unsigned int) + count;
	}
#else
save(cmdfile,outfile)                   /* save core image              */
char *cmdfile,*outfile;
{
	register char *c;
	register int i,fd;
	int fdaout;
	struct exec header;
	int counter;
	char buff[512],pwbuf[120];

	fdaout=getcmd(cmdfile);         /* open command wherever it is  */
	if (fdaout<0) return(-1);       /* can do nothing without text  */
	if ((fd=open(outfile,0))>0)     /* this restriction is so that  */
	{       printf("Can't use an existing file\n"); /* we don't try */
		close(fd);              /* to write over the commnd file*/
		return(-1);
	}
	if ((fd=creat(outfile,0755))== -1)
	{       printf("Cannot create %s\n",outfile);
		return(-1);
	}
	/* can get the text segment from the command that we were
	 * called with, and change all data from uninitialized to
	 * initialized.  It will start at the top again, so the user
	 * is responsible for checking whether it was restarted
	 * could ignore sbrks and breaks for the first pass
	 */
	read(fdaout,&header,sizeof header);/* get the header       */
	header.a_bss = 0;                  /* no data uninitialized        */
	header.a_syms = 0;                 /* throw away symbol table      */
	switch (header.a_magic)            /* find data segment            */
	{   case 0407:                     /* non sharable code            */
		c = (char *) header.a_text;/* data starts right after text */
		header.a_data=sbrk(0)-c;   /* current size (incl allocs)   */
		break;
	    case 0410:                     /* sharable code                */
		c = (char *)
#ifdef pdp11
		    (header.a_text	   /* starts after text            */
		    & 0160000)             /* on an 8K boundary            */
		    +  020000;             /* i.e. the next one up         */
#endif
#ifdef vax
		    (header.a_text	   /* starts after text            */
		    & 037777776000)        /* on an 1K boundary            */
		    +        02000;        /* i.e. the next one up         */
#endif
#ifdef z8000
		    (header.a_text	   /* starts after text            */
		    & 0174000)             /* on an 2K boundary            */
		    +  004000;             /* i.e. the next one up         */
#endif
		header.a_data=sbrk(0)-c;   /* current size (incl allocs)   */
		break;
	    case 0411:                     /* sharable with split i/d      */
		c = 0;                     /* can't reach text             */
		header.a_data=(int)sbrk(0);/* current size (incl allocs)   */
		break;
	    case 0413:
		c = (char *) header.a_text;/* starts after text            */
		lseek(fdaout, 1024L, 0);   /* skip unused part of 1st block*/
	}
	if (header.a_data<0)               /* data area very big           */
		return(-1);                /* fail for now                 */

	filesize=sizeof header+header.a_text+header.a_data;
	write(fd,&header,sizeof header);   /* make the new header          */
	if (header.a_magic==0413)
		lseek(fd, 1024L, 0);       /* Start on 1K boundary	   */
	counter=header.a_text;             /* size of text                 */
	while (counter>512)                /* copy 512-byte blocks         */
	{       read(fdaout,buff,512);     /* as long as possible          */
		write(fd,buff,512);
		counter -= 512;
	}
	read(fdaout,buff,counter);         /* then pick up the rest        */
	write(fd,buff,counter);
	write(fd,c,header.a_data);         /* write all data in 1 glob     */
	close(fd);
}

#define	NULL	0

char	*execat(), *getenv();

getcmd(command)         /* get command name (wherever it is) like shell */
char *command;
{
	char *pathstr;
	register char *cp;
	char fname[128];
	int fd;

	if ((pathstr = getenv("PATH")) == NULL)
		pathstr = ":/bin:/usr/bin";
#if 1 /* Nick */
	cp = strchr(command, '/') ? "" : pathstr;
#else
	cp = index(command, '/')? "": pathstr;
#endif

	do {
		cp = execat(cp, command, fname);
		if ((fd=open(fname,0))>0)
			return(fd);
	} while (cp);

	printf("Couldn't open %s\n",command);
	return(-1);
}

static char *
execat(s1, s2, si)
register char *s1, *s2;
char *si;
{
	register char *s;

	s = si;
	while (*s1 && *s1 != ':' && *s1 != '-')
		*s++ = *s1++;
	if (si != s)
		*s++ = '/';
	while (*s2)
		*s++ = *s2++;
	*s = '\0';
	return(*s1? ++s1: 0);
}
#endif

