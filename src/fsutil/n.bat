cl -Zi -I. -I..\kernel\uzi -DVAX -DUTIL -DDEBUG=1 -c utils.c
@if errorlevel 1 goto failure

rem cl -Zi -I. -I..\kernel\uzi -DVAX -DUTIL -DDEBUG=1 -c ..\kernel\uzi\devflop.c
rem @if errorlevel 1 goto failure
cl -Zi -I. -I..\kernel\uzi -DVAX -DUTIL -DDEBUG=1 -c ..\kernel\uzi\devhd.c
@if errorlevel 1 goto failure
cl -Zi -I. -I..\kernel\uzi -DVAX -DUTIL -DDEBUG=1 -c ..\kernel\uzi\devio.c
@if errorlevel 1 goto failure
cl -Zi -I. -I..\kernel\uzi -DVAX -DUTIL -DDEBUG=1 -c ..\kernel\uzi\devmisc.c
@if errorlevel 1 goto failure
rem cl -Zi -I. -I..\kernel\uzi -DVAX -DUTIL -DDEBUG=1 -c ..\kernel\uzi\devswap.c
rem @if errorlevel 1 goto failure
cl -Zi -I. -I..\kernel\uzi -DVAX -DUTIL -DDEBUG=1 -c ..\kernel\uzi\devtty.c
@if errorlevel 1 goto failure
cl -Zi -I. -I..\kernel\uzi -DVAX -DUTIL -DDEBUG=1 -c ..\kernel\uzi\filesys.c
@if errorlevel 1 goto failure
cl -Zi -I. -I..\kernel\uzi -DVAX -DUTIL -DDEBUG=1 -c ..\kernel\uzi\machdep.c
@if errorlevel 1 goto failure
cl -Zi -I. -I..\kernel\uzi -DVAX -DUTIL -DDEBUG=1 -c ..\kernel\uzi\process.c
@if errorlevel 1 goto failure
cl -Zi -I. -I..\kernel\uzi -DVAX -DUTIL -DDEBUG=1 -c ..\kernel\uzi\scall1.c
@if errorlevel 1 goto failure
cl -Zi -I. -I..\kernel\uzi -DVAX -DUTIL -DDEBUG=1 -c ..\kernel\uzi\scall2.c
@if errorlevel 1 goto failure
cl -Zi -I. -I..\kernel\uzi -DVAX -DUTIL -DDEBUG=1 -c ..\kernel\uzi\xip.c
@if errorlevel 1 goto failure
cl -Zi -I. -I..\kernel\uzi -DVAX -DUTIL -DDEBUG=1 -c main.c
@if errorlevel 1 goto failure
cl -Zi -I. -I..\kernel\uzi -DVAX -DUTIL -DDEBUG=1 -c xfs.c
@if errorlevel 1 goto failure
cl -Zi -I. -I..\kernel\uzi -DVAX -DUTIL -DDEBUG=1 -c hdasm.c
@if errorlevel 1 goto failure
rem zas -j -n xflopasm.asz
rem @if errorlevel 1 goto failure

cl -Zi -I. -I..\kernel\uzi -DVAX -DUTIL -DDEBUG=1 -c ucp.c
@if errorlevel 1 goto failure
cl -Zi -I. -I..\kernel\uzi -DVAX -DUTIL -DDEBUG=1 -c ucpsub.c
@if errorlevel 1 goto failure
link @ucp.w32
@if errorlevel 1 goto failure
copy ucp.exe ..\bin

cl -Zi -I. -I..\kernel\uzi -DVAX -DUTIL -DNATIVE -DDEBUG=1 -c mkfs.c
@if errorlevel 1 goto failure
link @mkfs.w32
@if errorlevel 1 goto failure
copy mkfs.exe ..\bin

cl -Zi -I. -I..\kernel\uzi -DVAX -DUTIL -DNATIVE -DDEBUG=1 -c fsck.c
@if errorlevel 1 goto failure
link @fsck.w32
@if errorlevel 1 goto failure
copy fsck.exe ..\bin

cl -Zi -I. -I..\kernel\uzi -DVAX -DUTIL -DNATIVE -DDEBUG=1 -c bd.c
@if errorlevel 1 goto failure
link @bd.w32
@if errorlevel 1 goto failure
copy bd.exe ..\bin

rem compile utils in large mode, once, for large-mode mkfs and fsck
rem 
rem iccz80 -S -w -ml -v1 -z -A -I..\..\include\ -I..\kernel\uzi\ -DUTIL -DNATIVE -DDEBUG=1 utils
rem @if errorlevel 1 goto failure
rem del utils.r01
rem as-z80 -l -o utils.s01
rem @if errorlevel 1 goto failure
rem 
rem call mklink-l mkfs
rem echo utils >> mkfs.lnk
rem 
rem iccz80 -S -w -ml -v1 -z -A -I..\..\include\ -I..\kernel\uzi\ -DUTIL -DNATIVE -DDEBUG=1 mkfs
rem @if errorlevel 1 goto failure
rem del mkfs.r01
rem as-z80 -l -o mkfs.s01
rem @if errorlevel 1 goto failure
rem 
rem link-z80 -f mkfs
rem @if errorlevel 1 goto failure
rem ihex2bin -l mkfs.i86 ..\..\bin\large\mkfs
rem @if errorlevel 1 goto failure
rem 
rem call mklink-l fsck
rem echo utils >> fsck.lnk
rem 
rem iccz80 -S -w -ml -v1 -z -A -I..\..\include\ -I..\kernel\uzi\ -DUTIL -DNATIVE -DDEBUG=1 fsck
rem @if errorlevel 1 goto failure
rem del fsck.r01
rem as-z80 -l -o fsck.s01
rem @if errorlevel 1 goto failure
rem 
rem link-z80 -f fsck
rem @if errorlevel 1 goto failure
rem ihex2bin -l fsck.i86 ..\..\bin\large\fsck
rem @if errorlevel 1 goto failure

rem compile utils in banked mode, once, for banked-mode mkfs and fsck
rem 
rem iccz80 -S -w -mb -v1 -z -A -I..\..\include\ -I..\kernel\uzi\ -DUTIL -DNATIVE -DDEBUG=1 utils
rem @if errorlevel 1 goto failure
rem del utils.r01
rem as-z80 -l -o utils.s01
rem @if errorlevel 1 goto failure
rem 
rem call mklink-b mkfs
rem echo utils >> mkfs.lnk
rem 
rem iccz80 -S -w -mb -v1 -z -A -I..\..\include\ -I..\kernel\uzi\ -DUTIL -DNATIVE -DDEBUG=1 mkfs
rem @if errorlevel 1 goto failure
rem del mkfs.r01
rem as-z80 -l -o mkfs.s01
rem @if errorlevel 1 goto failure
rem 
rem link-z80 -f mkfs
rem @if errorlevel 1 goto failure
rem ihex2bin -l mkfs.i86 ..\..\bin\banked\mkfs
rem @if errorlevel 1 goto failure
rem 
rem call mklink-b fsck
rem echo utils >> fsck.lnk
rem 
rem iccz80 -S -w -mb -v1 -z -A -I..\..\include\ -I..\kernel\uzi\ -DUTIL -DNATIVE -DDEBUG=1 fsck
rem @if errorlevel 1 goto failure
rem del fsck.r01
rem as-z80 -l -o fsck.s01
rem @if errorlevel 1 goto failure
rem 
rem link-z80 -f fsck
rem @if errorlevel 1 goto failure
rem ihex2bin -l fsck.i86 ..\..\bin\banked\fsck
rem @if errorlevel 1 goto failure
rem 
rem iccz80 -S -w -ml -v1 -z -A -I..\..\include\ -I..\kernel\uzi\ -DUTIL -DNATIVE -DDEBUG=1 bd -l bd
rem @if errorlevel 1 goto failure
rem del bd.r01
rem as-z80 -l -o bd.s01
rem @if errorlevel 1 goto failure
rem 
rem link-z80 -f bd
rem @if errorlevel 1 goto failure
rem ihex2bin -l bd.i86 ..\..\bin\large\bd
rem @if errorlevel 1 goto failure

@echo SUCCESS
@goto done
:failure
@echo FAILURE
:done

