/*
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 UZIX utilities:

 Block dump: to examine disk.
 Usage:	bd d: blkstart [blkend]
**********************************************************/

/* The device is given as drive letter.
 * Major device is always 0 (FDD), minor device is 0..7 (drives A..H)
 */

#define __MAIN__COMPILATION
#define NEED__DEVIO
#define SKIP_TIME_T /* Nick */

#include "utildos.h"

#if 1 /* Nick */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#ifdef NATIVE /* Nick */
#include <fcntl.h>
#ifdef VAX
#include <io.h>
#else
#include <syscalls.h>
#endif
#include "utils.h"
#endif
#else
#ifdef SEPH
#include "sys\ioctl.h"
#endif
#ifdef _MSX_DOS
#include "..\include\stdio.h"
#include "..\include\stdlib.h"
#include "..\include\ctype.h"
#else
#include <c:\tc\include\stdio.h>
#include <c:\tc\include\stdlib.h>
#include <c:\tc\include\ctype.h>
#endif
#endif

#ifdef NATIVE /* Nick */
int device_handle;
uchar device_name[0x200]; /* 16]; */
#endif

char buf[BUFSIZE];

void dread(int dev, uint blk, char *addr);

void dread(int dev, uint blk, char *addr)
{
#ifdef NATIVE /* Nick */
/* printf("seeking to %ld\n", ((long)blk) << BUFSIZELOG); */
/* fflush(stdout); */
	if (lseek(device_handle, ((long)blk) << BUFSIZELOG, SEEK_SET) < 0)
		{
		printf("can't seek: ");
		fflush(stdout);
		perror(device_name);
		exit(1);
		}

	if (read(device_handle, addr, BUFSIZE) != BUFSIZE)
		{
		printf("can't read: ", dev);
		fflush(stdout);
		perror(device_name);
		exit(1);
		}
#else
	char *buf = bread(dev, blk, 0);
	if (buf == NULL) {
		printf("bd: disk is not MSXDOS compatible, device or drive error\n");
		xexit(1);
	}
	bcopy(buf, addr, BUFSIZE);
	bfree((bufptr)buf, 0);
#endif
}

void main(int argc, char *argv[])
{
	int i, j, k, dev;
	unsigned blkno, blkend;
	long addr;

#ifdef _MSX_DOS
	initenv();
#endif
#ifdef NATIVE /* Nick */
	dev = 0;
	if (argc < 3)
		{
		fprintf(stderr, "usage: bd device blkstart [blkend|-blkno]\n");
		exit(1);
		}
	strcpy(device_name, argv[1]);
	device_handle = open(device_name, O_RDWR | O_BINARY);
	if (device_handle < 0)
		{
		printf("can't open: ");
		fflush(stdout);
		perror(device_name);
		exit(1);
		}
#else
	if (argc < 3 || !isalpha(argv[1][0]) || (argv[1][1] != ':')) {
		fprintf(stderr, "usage: bd d: blkstart [blkend|-blkno]\n");
		xexit(1);
	}
	dev = ((argv[1][0] & 223) - 65);
#endif
	blkno = (unsigned)atol(argv[2]);
	if (argc < 4)
		blkend = blkno;
	else {
		if (argv[3][0] == '-')
			blkend = blkno + (unsigned)atol(argv[3]+1) - 1;
		else	blkend = (unsigned)atol(argv[3]);
	}
#ifdef _MSX_DOS
	printf("Insert disk and press RETURN: ");
	while (getchar() != '\n')
		;
	cursor(0);
#endif
#ifdef NATIVE /* Nick */
	printf("%s, blocks %u..%u (0x%X..0x%X)\n\n",
		device_name,blkno,blkend,blkno,blkend);
#else
	bufinit();
	d_init();
	d_open(dev);
	printf("Device %x, blocks %u..%u (0x%X..0x%X)\n\n",
		dev,blkno,blkend,blkno,blkend);
#endif
	addr = (long)blkno * BUFSIZE;	/* starting address */
	while (blkno <= blkend) {
		printf("Block %u (0x%X)\n", blkno, blkno);
		dread(dev, blkno++, buf);
		for (i = 0; i < BUFSIZE / 16; ++i, addr += 16) {
			printf("%02x%04x\t", (uint)(addr >> 16),
					     (uint)(addr & 0xffff));
			for (j = 0; j < 16; ++j) {
				k = buf[(i<<4)+j] & 0xFF;
				printf("%02X ", k);
			}
			printf(" |");
			for (j = 0; j < 16; ++j) {
				k = buf[(i<<4)+j] & 0xFF;
#if 1 /* Nick temporary */
				if (k < 0x20 || k >= 0x7f)
#else
				if (k < ' ' || k == 0177 || k == 0377)
#endif
					k = '.';
				printf("%c", k);
			}
			printf("|\n");
		}
		printf("\n");
		fflush(stdout);
	}
#ifdef _MSX_DOS
	cursor(255);
#endif
#ifdef NATIVE /* Nick */
	close(device_handle);
#else
	/* bufsync(); */
	d_close(dev);
#endif
	exit(0);
}

