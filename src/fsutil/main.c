/* main.c for uzi utils by Nick - a few scrappy publics missed elsewhere */

#include <stdio.h>
#include <time.h>

#define SKIP_TIME_T
#include <unix.h>

char UZIX[14]="UZI180"; /* Nick "UZIX"; should be 13 chars or fewer */
char HOST[14]="sn00000"; /* Nick kdata.k_host should be 13 chars or fewer */

char recv_bufs[13][256]; /* getting a bit pernickety here */

/* the following variables are the wrong size and would crash if accessed: */
int osBank, disp_tab, ncalls;

void abort(int exitcode)
	{
	fflush(stdout);
	exit(exitcode);
	}

void rdtime(uzitime_t *tloc)
	{
	time_t now;
	struct tm *tmbuf;

	time(&now);
	tmbuf = localtime(&now); /* gmtime(&now); */

	tloc->t_time = (tmbuf->tm_sec >> 1) |
		       (tmbuf->tm_min << 5) |
		       (tmbuf->tm_hour << 11);

	tloc->t_date = tmbuf->tm_mday |
		       ((tmbuf->tm_mon + 1) << 5) |
		       ((tmbuf->tm_year - 80) << 9);
	}

