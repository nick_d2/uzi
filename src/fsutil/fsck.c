/*
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 UZIX utilities:

 Filesystem Check: to check an UZIX disk.
 Usage:	fsck [-y] d:
**********************************************************/

/* The device is given as drive letter.
 * Major device is always 0 (FDD), minor device is 0..7 (drives A..H)
 */

#define __MAIN__COMPILATION
#define NEED__DEVIO
#define SKIP_TIME_T /* Nick */

#include "utildos.h"

#if 1 /* Nick */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#ifdef NATIVE /* Nick */
#include <fcntl.h>
#ifdef VAX
#include <io.h>
#else
#include <syscalls.h>
#endif
#include "utils.h"
#endif
#else
#ifdef SEPH
#include "sys\ioctl.h"
#include "sys\stat.h"
#endif
#ifdef _MSX_DOS
#include ".\include\stdio.h"
#include ".\include\stdlib.h"
#include ".\include\ctype.h"
#include ".\include\string.h"
#else
#include <c:\tc\include\stdio.h>
#include <c:\tc\include\stdlib.h>
#include <c:\tc\include\ctype.h>
#include <c:\tc\include\string.h>
#define xexit(n) (exit(n)) /* Nick */
#endif
#endif

#ifdef NATIVE /* Nick */
int device_handle;
uchar device_name[0x200]; /* 16]; */

char zeroed[BUFSIZE]; /* actually a misnomer */
#endif

#define MAXDEPTH 33	/* Maximum depth of directory tree to search */
uint depth;

int _yes = 0;

dev_t	dev;
filesys_t filsys;
uint	ninodes;
blkno_t finode;

#if 1 /* Nick free bitmap */
off_t	bitmap_inode;
off_t	bitmap_block;
off_t	bitmap_immov;

char	*inode_bitmap;
#endif
char	*block_bitmap;
uchar	*link_count;

void ckdir(uint inum, uint pnum, char *name);
void mkentry(uint inum);
blkno_t getblkno(dinode_t *ino, blkno_t num);
void setblkno(dinode_t *ino, blkno_t num, blkno_t dnum);
blkno_t _blk_alloc(filesys_t *filsys);
char *daread(uint blk);
void dwrite(uint blk, void *addr);
void iread(uint ino, dinode_t *buf);
void iwrite(uint ino, dinode_t *buf);
void dirread(dinode_t *ino, uint j, direct_t *dentry);
void dirwrite(dinode_t *ino, uint j, direct_t *dentry);
int da_read(dev_t,uint,void *);
int da_write(dev_t,uint,void *);
int yes(void);
#if 1 /* Nick free bitmap */
void bitmap_dump(char *bitmap, off_t start, off_t final);
blkno_t bitmap_find(blkno_t start, blkno_t final);
#endif
#ifdef NATIVE /* Nick */
void mypanic(char *message);
#endif

int da_read(dev, blk, addr)
	dev_t dev;
	uint blk;
	void *addr;
{
#ifdef NATIVE /* Nick */
	if (lseek(device_handle, ((long)blk) << BUFSIZELOG, SEEK_SET) < 0)
		{
		printf("can't seek: ");
		fflush(stdout);
		perror(device_name);
		exit(1);
		}

	if (read(device_handle, addr, BUFSIZE) != BUFSIZE)
		{
		printf("can't read: ");
		fflush(stdout);
		perror(device_name);
		exit(1);
		}
#else
	bufptr buf = bread(dev, blk, 0);

	if (buf == NULL)
		return 0;
	bcopy(buf, addr, BUFSIZE);
	bfree(buf, 0);
#endif
	return BUFSIZE;
}

int da_write(dev, blk, addr)
	dev_t dev;
	uint blk;
	void *addr;
{
#ifdef NATIVE /* Nick */
	if (lseek(device_handle, ((long)blk) << BUFSIZELOG, SEEK_SET) < 0)
		{
		printf("can't seek: ");
		fflush(stdout);
		perror(device_name);
		exit(1);
		}

	if (write(device_handle, addr, BUFSIZE) != BUFSIZE)
		{
		printf("can't write: ");
		fflush(stdout);
		perror(device_name);
		exit(1);
		}
#else
	bufptr buf = bread(dev, blk, 1);

	if (buf == NULL)
		return 0;
	bcopy(addr, buf, BUFSIZE);
	bfree(buf, 1);
#endif
	return BUFSIZE;
}

/* Pass 1 checks each inode independently for validity, zaps bad block
 * numbers in the inodes, and builds the block allocation map.
 */
void pass1(void) {
	uint n, mode, icount = 0;
	blkno_t b, bn, bno, *buf;
	dinode_t ino;

	n = ROOTINODE;
	while (n < ninodes) {
		iread(n, &ino);
		link_count[n] = -1;
		if (ino.i_mode == 0)
			goto Cont;
		mode = ino.i_mode & S_IFMT;
		/* Check mode */
		if (mode != S_IFALIGN && /* Nick */
		    mode != S_IFREG &&
		    mode != S_IFDIR &&
		    mode != S_IFLNK &&
		    mode != S_IFBLK &&
		    mode != S_IFCHR) {
			PF("Inode %d with mode 0%o is not of correct type. Zap? ",
			       n, ino.i_mode);
			if (yes()) {
				ino.i_mode = 0;
				ino.i_nlink = 0;
				iwrite(n, &ino);
				goto Cont;
			}
		}
		link_count[n] = 0;
		++icount;
		/* Check blocks and build free block map */
		if (mode == S_IFALIGN || /* Nick */
		    mode == S_IFREG || mode == S_IFDIR || mode == S_IFLNK) {
			/* Check singly indirect blocks */
			b = DIRECTBLOCKS;
			while (b <= DIRECTBLOCKS+INDIRECTBLOCKS) {
				bno = ino.i_addr[b];
				if (bno != 0 &&
				    (bno < finode ||
				     bno >= filsys.s_fsize)) {
					PF("Inode %d singly indirect "
					       "block %d is out of range "
					       "with value of %u. Zap? ",
					       n, b, bno);
					if (yes()) {
						ino.i_addr[b] = 0;
						iwrite(n, &ino);
						bno = 0; /* Nick oops */
					}
				}
				if (bno != 0 &&
				    (uint)(ino.i_size >> BUFSIZELOG) < DIRECTBLOCKS) {
					PF("Inode %d singly indirect block "
					       "%d is past end of file with "
					       "value of %u. Zap? ",
					       n, b, bno);
					if (yes()) {
						ino.i_addr[b] = 0;
						iwrite(n, &ino);
						bno = 0; /* Nick oops */
					}
				}
				if (bno != 0)
					block_bitmap[bno >> 3] |= 1 << (bno & 7);
				++b;
			}
			/* Check the double indirect blocks */
			if (ino.i_addr[DIRECTBLOCKS+INDIRECTBLOCKS] != 0) {
				buf = (blkno_t *) daread(ino.i_addr[19]);
				b = 0;
				while (b < BUFSIZE/sizeof(blkno_t)) {
					bno = buf[b]; /* Nick [n]; oops */
					if (bno != 0 &&
					    (bno < finode ||
					     bno >= filsys.s_fsize)) {
						PF("Inode %d doubly indirect "
						       "block %d is out of range "
						       "with value of %u. Zap? ",
						       n, b, bno);
						if (yes()) {
							buf[b] = 0;
							dwrite(b, (char *) buf);
							bno = 0; /* Nick oops */
						}
					}
					if (bno != 0)
						block_bitmap[bno >> 3] |= 1 << (bno & 7);
					++b;
				}
			}
			/* Check the rest */
			b = 0;
			bn = (uint)(ino.i_size >> BUFSIZELOG);
			while (b <= bn) {
				bno = getblkno(&ino, b);
				if (bno != 0 &&
				    (bno < finode || bno >= filsys.s_fsize)) {
					PF("Inode %d block %d is out of "
					       "range with value of %u. Zap? ",
					       n, b, bno);
					if (yes()) {
						setblkno(&ino, b, 0);
						iwrite(n, &ino);
					}
				}
				if (bno != 0)
					block_bitmap[bno >> 3] |= 1 << (bno & 7);
				++b;
			}
		}
Cont:		++n;
	}
	/* Fix free inode count in super block */
	b = ninodes - ROOTINODE - icount;
	if (filsys.s_tinode != b) {
		PF("Free inode count in super block is %u should be %u. Fix? ",
			filsys.s_tinode, b);
		if (yes()) {
			filsys.s_tinode = b;
			dwrite((blkno_t)1, (char *) &filsys);
		}
	}
}

/* Clear inode free list, rebuild block free list using bit map. */
void pass2(void) {
#if 1 /* Nick free bitmap */
	char *buf;
	blkno_t j, lm, oldtfree, oldtinode;
	blkno_t bitmap_inode, bitmap_block, bitmap_immov;

	oldtfree = filsys.s_tfree;
	oldtinode = filsys.s_tinode;

#if 0 /* remove support for the original UZI filesystem */
	if (filsys.s_bitmap_inode)
		{
#endif
		printf("        Rebuild free bitmaps? ");
#if 0 /* remove support for the original UZI filesystem */
		}
	else
		{
		printf("        Rebuild free list? ");
		}
#endif
#else
	blkno_t j, oldtfree = filsys.s_tfree;

	PF("        Rebuild free list? ");
#endif
	if (!yes())
		return;
	/* Initialize the super-block */
#if 0 /* remove support for the original UZI filesystem */
	filsys.s_ninode = 0;
#endif
#if 1 /* Nick free bitmap */
#if 0 /* remove support for the original UZI filesystem */
	if (filsys.s_bitmap_inode)
		{
#endif
		/* grab fields of superblock, for clarity and extra speed */
		bitmap_inode = filsys.s_bitmap_inode;
		bitmap_block = filsys.s_bitmap_block;
		bitmap_immov = filsys.s_bitmap_immov;

		/* mark all system reserved blocks as used in the bitmap */
		for (j = 0; j < finode+filsys.s_isize; j++)
			{
			block_bitmap[j >> 3] |= 1 << (j & 7);
			}

		/* mark all out-of-range blocks as used in the bitmap */
		lm = (bitmap_immov - bitmap_block) << 3; /* may wrap to 0 */
		for (j = filsys.s_fsize; j != lm; j++)
			{
			block_bitmap[j >> 3] |= 1 << (j & 7);
			}

		/* count free blocks in the already constructed bitmap */
		filsys.s_tfree = 0;
		for (j = finode+filsys.s_isize; j < filsys.s_fsize; j++)
			{
			if ((block_bitmap[j >> 3] & (1 << (j & 7))) == 0)
				{
				++filsys.s_tfree;
				}
			}

		/* dump bitmap, preserving unused bytes of reserved blocks */
		bitmap_dump(block_bitmap, bitmap_block, bitmap_immov);

		/* mark all system reserved inodes as used in the bitmap */
		inode_bitmap[0] |= 1;
		inode_bitmap[ROOTINODE >> 3] |= 1 << (ROOTINODE & 7);

		/* mark inodes as used in the bitmap, using link_count[] */
		for (j = ROOTINODE + 1; j < ninodes; j++)
			{
			if (link_count[j] != (uchar)-1)
				{
				inode_bitmap[j >> 3] |= 1 << (j & 7);
				}
			}

		/* mark all out-of-range inodes as used in the bitmap */
		lm = (bitmap_block - bitmap_inode) << 3; /* may wrap to 0 */
		for (j = DINODESPERBLOCK * filsys.s_isize; j != lm; j++)
			{
			inode_bitmap[j >> 3] |= 1 << (j & 7);
			}

		/* dump bitmap, preserving unused bytes of reserved blocks */
		bitmap_dump(inode_bitmap, bitmap_inode, bitmap_block);
#if 0 /* remove support for the original UZI filesystem */
		}
	else
		{
		filsys.s_nfree = 1;
		filsys.s_free[0] = 0;
		filsys.s_tfree = 0;

		/* Free each block, building the free list */
		j = filsys.s_fsize - 1;
		while (j >= finode+filsys.s_isize)
			{
			if ((block_bitmap[j >> 3] & (1 << (j & 7))) == 0)
				{
				if (filsys.s_nfree == FSFREEBLOCKS)
					{
					dwrite(j, (char *) &filsys.s_nfree);
					filsys.s_nfree = 0;
					}
				filsys.s_free[filsys.s_nfree] = j;
				++filsys.s_nfree;
				++filsys.s_tfree;
				}
			--j;
			}
		}
#endif
#else
	filsys.s_nfree = 1;
	filsys.s_free[0] = 0;
	filsys.s_tfree = 0;

	/* Free each block, building the free list */
	j = filsys.s_fsize - 1;
	while (j >= finode+filsys.s_isize) {
		if ((block_bitmap[j >> 3] & (1 << (j & 7))) == 0) {
			if (filsys.s_nfree == FSFREEBLOCKS) {
				dwrite(j, (char *) &filsys.s_nfree);
				filsys.s_nfree = 0;
			}
			filsys.s_free[filsys.s_nfree] = j;
			++filsys.s_nfree;
			++filsys.s_tfree;
		}
		--j;
	}
#endif
#if 1 /* Nick free bitmap */
	buf = daread(SUPERBLOCK);
	bcopy((char *)&filsys, buf, sizeof(filsys));
	dwrite(SUPERBLOCK, buf);
#else
	dwrite(SUPERBLOCK, (char *) &filsys);
#endif
	if (oldtfree != filsys.s_tfree) {
		PF("During free list regeneration s_tfree "
		       "was changed to %d from %d.\n",
			filsys.s_tfree, oldtfree);
		FF;
	}
}

/* Pass 3 finds and fixes multiply allocated blocks. */
void pass3(void) {
	uint n, mode;
	dinode_t ino;
	blkno_t b, bn, bno,newno;

	b = finode;
	while (b < filsys.s_fsize) {
		block_bitmap[b >> 3] &= ~(1 << (b & 7));
		b++;
	}
	n = ROOTINODE;
	while (n < ninodes) {
		iread(n, &ino);
		mode = ino.i_mode & S_IFMT;
		if (mode != S_IFALIGN && /* Nick */
		    mode != S_IFREG && mode != S_IFDIR && mode != S_IFLNK)
			goto Cont;
		/* Check singly indirect blocks */
		b = DIRECTBLOCKS;
		while (b <= DIRECTBLOCKS+INDIRECTBLOCKS) {
			bno = ino.i_addr[b];
			if (bno != 0) {
				if ((block_bitmap[bno >> 3] & (1 << (bno & 7))) != 0) {
					PF("Indirect block %d in inode %u "
					       "value %u multiply allocated. "
					       "Fix? ", b, n, bno);
					if (yes()) {
						newno = _blk_alloc(&filsys);
						if (newno == 0) {
							PF("Sorry... "
							 "No more free blocks.\n");
							FF;
						}
						else {
							dwrite(newno,daread(bno));
							ino.i_addr[b] = newno;
							iwrite(n, &ino);
						}
					}
				}
				else	block_bitmap[bno >> 3] |= 1 << (bno & 7);
			}
			++b;
		}
		/* Check the rest */
		b = 0;
		bn = (uint)(ino.i_size >> BUFSIZELOG);
		while (b <= bn) {
			bno = getblkno(&ino, b);
			if (bno == 0)
				goto Cont1;
			if ((block_bitmap[bno >> 3] & (1 << (bno & 7))) != 0) {
				PF("Block %d in inode %u value %u "
				       "multiply allocated. Fix? ",
				       b, n, bno);
				if (yes()) {
					newno = _blk_alloc(&filsys);
					if (newno == 0) {
						PF("Sorry... "
						  "No more free blocks.\n");
						FF;
					}
					else {
						dwrite(newno, daread(bno));
						setblkno(&ino, b, newno);
						iwrite(n, &ino);
					}
				}
			}
			else	block_bitmap[bno >> 3] |= 1 << (bno & 7);
Cont1:			++b;
		}
Cont:		++n;
	}
}

/* This recursively checks the directories */
void ckdir(inum, pnum, name)
	uint inum;
	uint pnum;
	char *name;
{
	dinode_t ino;
	direct_t dentry;
	uint j, i, nentries;
	char ename[150];

	iread(inum, &ino);
	if ((ino.i_mode & S_IFMT) != S_IFDIR)
		return;
	++depth;
	i = (uint)(ino.i_size) % sizeof(direct_t);
	if (i != 0) {
		PF("Directory inode %d has improper length (%d extra). Fix? ",
			inum, i);
		if (yes()) {
			ino.i_size &= ~(sizeof(direct_t) - 1);
			iwrite(inum, &ino);
		}
	}
	nentries = (uint)(ino.i_size / sizeof(direct_t));
	j = 0;
	while (j < nentries) {
		dirread(&ino, j, &dentry);
		if (dentry.d_ino == 0)
			goto Cont;
		if (dentry.d_ino < ROOTINODE ||
		    dentry.d_ino >= 8 * filsys.s_isize) {
			PF("Directory entry %s%-1.14s has out-of-range "
				"inode %u. Zap? ",
				name, dentry.d_name, dentry.d_ino);
			if (yes()) {
				dentry.d_ino = 0;
				dentry.d_name[0] = '\0';
				dirwrite(&ino, j, &dentry);
				goto Cont;
			}
		}
		if (dentry.d_ino && link_count[dentry.d_ino] == (uchar)-1) {
			PF("Directory entry %s%-1.14s points to "
				"bogus inode %u. Zap? ",
				name, dentry.d_name, dentry.d_ino);
			if (yes()) {
				dentry.d_ino = 0;
				dentry.d_name[0] = '\0';
				dirwrite(&ino, j, &dentry);
				goto Cont;
			}
		}
		if (link_count[dentry.d_ino]++ == 254) {
			fprintf(stderr, "More than 254 links to inode %u.\n"
				"Not enough memory. FSCK aborted.\n",
				dentry.d_ino);
#ifdef NATIVE /* Nick */
			exit(1);
#else
			xexit(1);
#endif
		}
		i = 0;
		while (i < DIRNAMELEN && dentry.d_name[i]) {
			if (dentry.d_name[i] == '/') {
				PF("Directory entry %s%-1.14s "
					"contains slash. Fix? ",
					name, dentry.d_name);
				if (yes()) {
					dentry.d_name[i] = '~';
					dirwrite(&ino, j, &dentry);
				}
			}
			++i;
		}
		if (strcmp((char *)dentry.d_name, ".") == 0 &&
		    dentry.d_ino != inum) {
			PF("'.' entry %s%-1.*s points to wrong place. Fix? ",
			       name, DIRNAMELEN, dentry.d_name);
			if (yes()) {
				dentry.d_ino = inum;
				dirwrite(&ino, j, &dentry);
			}
		}
		if (strcmp((char *)dentry.d_name, "..") == 0 &&
		    dentry.d_ino != pnum) {
			PF("'..' entry %s%-1.*s points to wrong place. Fix? ",
			       name, DIRNAMELEN, dentry.d_name);
			if (yes()) {
				dentry.d_ino = pnum;
				dirwrite(&ino, j, &dentry);
			}
		}
		if (dentry.d_ino != pnum &&
		    dentry.d_ino != inum &&
		    depth < MAXDEPTH) {
			strcpy(ename, name);
			strcat(ename, (char *)dentry.d_name);
			strcat(ename, "/");
			ckdir(dentry.d_ino, inum, ename);
		}
Cont:		++j;
	}
	--depth;
}

/* Pass 4 traverses the directory tree, fixing bad directory entries
 * and finding the actual number of references to each inode.
 */
void pass4(void) {
	depth = 0;
	link_count[ROOTINODE] = 1;
	ckdir(ROOTINODE, ROOTINODE, "/");
	if (depth != 0)
#ifdef NATIVE /* Nick */
		mypanic("Inconsistent depth");
#else
		panic("Inconsistent depth");
#endif
}

/* This makes an entry in "lost+found" for inode n */
void mkentry(inum)
	uint inum;
{
	dinode_t rootino;
	direct_t dentry;
	uint d, ne;

	iread(ROOTINODE, &rootino);
	d = 0;
	ne = (uint)(rootino.i_size / sizeof(direct_t));
	while (d < ne) {
		dirread(&rootino, d, &dentry);
		if (dentry.d_ino == 0 && dentry.d_name[0] == '\0') {
			dentry.d_ino = inum;
			sprintf(dentry.d_name, "l_f%d", inum);
			dirwrite(&rootino, d, &dentry);
			return;
		}
		++d;
	}
	PF("Sorry... No empty slots in root directory.\n"); FF;
}

/* Pass 5 compares the link counts found in pass 4 with the inodes. */
void pass5(void) {
#if 1 /* Nick free bitmap */
	char *buf;
#endif
	uint n;
	dinode_t ino;

	n = ROOTINODE;
	while (n < ninodes) {
		iread(n, &ino);
		if (ino.i_mode == 0) {
			if (link_count[n] != (uchar)-1)
#ifdef NATIVE /* Nick */
				mypanic("Inconsistent link count");
#else
				panic("Inconsistent link count");
#endif
			goto Cont;
		}
		if (link_count[n] == (uchar)-1 && ino.i_mode != 0)
#ifdef NATIVE /* Nick */
			mypanic("Inconsistent link count");
#else
			panic("Inconsistent link count");
#endif
		if (link_count[n] != (uchar)-1 && ino.i_nlink != link_count[n]) {
			PF("Inode %d has link count %d should be %d. Fix? ",
			       n, ino.i_nlink, link_count[n]);
			if (yes()) {
				ino.i_nlink = link_count[n];
				iwrite(n, &ino);
			}
		}
		if (link_count[n] == 0) {
			if (S_ISBLK(ino.i_mode) ||
			    S_ISCHR(ino.i_mode) ||
			    ino.i_size == 0) {
				PF("Useless inode %d with mode 0%o has "
				       "become detached. Link count is %d. Zap? ",
				       n, ino.i_mode, ino.i_nlink);
				if (yes()) {
					ino.i_nlink = 0;
					ino.i_mode = 0;
					iwrite(n, &ino);
					++filsys.s_tinode;
#if 1 /* Nick free bitmap */
					buf = daread(SUPERBLOCK);
					bcopy((char *)&filsys,
					      buf, sizeof(filsys));
					dwrite(SUPERBLOCK, buf);
#else
					dwrite(SUPERBLOCK, (char *) &filsys);
#endif
				}
			}
			else {
				PF("Inode %d has become detached. "
					"Link count is %d. Fix? ",
					n, ino.i_nlink);
				if (yes()) {
					ino.i_nlink = 1;
					iwrite(n, &ino);
					mkentry(n);
				}
			}
		}
Cont:		++n;
	}
}

/* Getblkno gets a pointer index, and a number of a block in the file.
 * It returns the number of the block on the disk.  A value of zero
 * means an unallocated block.
 */
blkno_t getblkno(ino, num)
	dinode_t *ino;
	blkno_t num;
{
	blkno_t indb;
	blkno_t dindb;
	blkno_t *buf;

	if (num < DIRECTBLOCKS) /* Direct block */
		return (ino->i_addr[num]);
	if (num < DIRECTBLOCKS + BUFSIZE/sizeof(blkno_t)) {	/* Single indirect */
		indb = ino->i_addr[DIRECTBLOCKS];
		if (indb == 0)
			return (0);
		buf = (blkno_t *) daread(indb);
		return (buf[num - DIRECTBLOCKS]);
	}
	/* Double indirect */
	indb = ino->i_addr[DIRECTBLOCKS+INDIRECTBLOCKS];
	if (indb == 0)
		return (0);
	buf = (blkno_t *) daread(indb);
#if 1 /* Nick, this must have been an oversight */
	indb = num - (DIRECTBLOCKS + BUFSIZE/sizeof(blkno_t));
#else
	indb = (num - DIRECTPERBLOCK + BUFSIZE/sizeof(blkno_t));
#endif
	dindb = buf[indb >> 8];
	buf = (blkno_t *) daread(dindb);
	return (buf[indb & 0xFF]);
}

/* Setblkno sets the given block number of the given file to the given
 * disk block number, possibly creating or modifiying the indirect blocks.
 * A return of zero means there were no blocks available to create an
 * indirect block. This should never happen in fsck.
 */
void setblkno(ino, num, dnum)
	dinode_t *ino;
	blkno_t num;
	blkno_t dnum;
{
	blkno_t indb;
	blkno_t dindb;
	blkno_t *buf;

	if (num < DIRECTBLOCKS) /* Direct block */
		ino->i_addr[num] = dnum;
	else if (num < DIRECTBLOCKS + BUFSIZE/sizeof(blkno_t)) {	/* Single indirect */
		indb = ino->i_addr[DIRECTBLOCKS];
		if (indb == 0)
#ifdef NATIVE /* Nick */
			mypanic("Missing indirect block");
#else
			panic("Missing indirect block");
#endif
		buf = (blkno_t *) daread(indb);
		buf[num - DIRECTBLOCKS] = dnum;
		dwrite(indb, (char *) buf);
	}
	else {			/* Double indirect */
		indb = ino->i_addr[DIRECTBLOCKS+INDIRECTBLOCKS];
		if (indb == 0)
#ifdef NATIVE /* Nick */
			mypanic("Missing indirect block");
#else
			panic("Missing indirect block");
#endif
		buf = (blkno_t *) daread(indb);
		num -= DIRECTBLOCKS + BUFSIZE/sizeof(blkno_t);
		dindb = buf[num >> 8];
		if (dindb == 0)
#ifdef NATIVE /* Nick */
			mypanic("Missing indirect block");
#else
			panic("Missing indirect block");
#endif
		buf = (blkno_t *) daread(dindb);
		buf[num & 0xFF] = num;
		dwrite(indb, buf);
	}
}

/* Blk_alloc allocates an unused block.
 * A returned block number of zero means no more blocks.
 */
blkno_t _blk_alloc(filsys)
	filesys_t *filsys;
{
	uint j;
	char *buf; /* blkno_t *buf; */
	blkno_t newno;
#if 1 /* Nick free bitmap */
	blkno_t bitmap_block, bitmap_immov;

#if 0 /* remove support for the original UZI filesystem */
	if (filsys->s_bitmap_inode)
		{
#endif
		/* grab fields of superblock, for clarity and extra speed */
		bitmap_block = filsys->s_bitmap_block;
		bitmap_immov = filsys->s_bitmap_immov;

		/* read bitmap, skipping unused bytes of reserved blocks */
		newno = bitmap_find(bitmap_block, bitmap_immov);
		if (newno == (blkno_t)-1)
			{
			/* no '0' bit found within the bitmap limits */
			return 0; /* return an ENOSPC error (really full) */
			}

#if 0 /* remove support for the original UZI filesystem */
		goto found_block; /* crude way to skip free list processing */
		}
#endif
#endif
#if 0 /* remove support for the original UZI filesystem */
	newno = filsys->s_free[--filsys->s_nfree];
	if (newno == 0) {
		++filsys->s_nfree;
		return (0);
	}
	/* See if we must refill the s_free array */
	if (filsys->s_nfree == 0) {
		buf = daread(newno);
		filsys->s_nfree = ((blkno_t *)buf)[0];
		j = 0;
		while (j < FSFREEBLOCKS) {
			filsys->s_free[j] = ((blkno_t *)buf)[j + 1];
			++j;
		}
	}
#if 1 /* Nick free bitmap */
found_block:
#endif
#endif
	--filsys->s_tfree;
	if (newno < finode || newno >= filsys->s_fsize) {
		PF("Free list is corrupt.  Did you rebuild it?\n");
		return (0);
	}
#if 1 /* Nick free bitmap */
	buf = daread(SUPERBLOCK);
	bcopy((char *)&filsys, buf, sizeof(filsys));
	dwrite(SUPERBLOCK, buf);
#else
	dwrite(SUPERBLOCK, filsys);
#endif
	return (newno);
}

char *daread(blk)
	uint blk;
{
	static char buf[BUFSIZE];

	if (da_read(dev, blk, buf) != BUFSIZE) {
		PF("Read of block %d failed.\n", blk);
		abort();
	}
	return (buf);
}

void dwrite(blk, addr)
	uint blk;
	void *addr;
{
	if (da_write(dev, blk, addr) != BUFSIZE) {
		PF("Write of block %d failed.\n", blk);
		abort();
	}
}

void iread(ino, buf)
	uint ino;
	dinode_t *buf;
{
	dinode_t *addr = (dinode_t *)daread((ino>>DINODESPERBLOCKLOG)+finode);

	bcopy((char *)&addr[ino & (DINODESPERBLOCK-1)], (char *)buf,
	      sizeof(dinode_t)); /* Nick added casts */
}

void iwrite(ino, buf)
	uint	ino;
	dinode_t *buf;
{
	dinode_t *addr = (dinode_t *)daread((ino>>DINODESPERBLOCKLOG)+finode);

	bcopy((char *)buf, (char *)&addr[ino & (DINODESPERBLOCK-1)],
	      sizeof(dinode_t)); /* Nick added casts */
	dwrite((ino >> DINODESPERBLOCKLOG) + finode, addr);
}

void dirread(ino, j, dentry)
	dinode_t *ino;
	uint j;
	direct_t *dentry;
{
	direct_t *buf;
	blkno_t blkno = getblkno(ino, (blkno_t) j / DIRECTPERBLOCK);

	if (blkno == 0)
#ifdef NATIVE /* Nick */
		mypanic("Missing block in directory");
#else
		panic("Missing block in directory");
#endif
	buf = (direct_t *)daread(blkno);
	bcopy((char *)(buf+j % DIRECTPERBLOCK), (char *)dentry,
	      sizeof(direct_t)); /* Nick added casts */
}

void dirwrite(ino, j, dentry)
	dinode_t *ino;
	uint j;
	direct_t *dentry;
{
	direct_t *buf;
	blkno_t blkno = getblkno(ino, (blkno_t) j / DIRECTPERBLOCK);

	if (blkno == 0)
#ifdef NATIVE /* Nick */
		mypanic("Missing block in directory");
#else
		panic("Missing block in directory");
#endif
	buf = (direct_t *)daread(blkno);
	bcopy((char *)dentry, (char *)(buf+j % DIRECTPERBLOCK),
	      sizeof(direct_t)); /* Nick added casts */
	dwrite(blkno, buf);
}

int yes(void) {
	char line[20];

	FF;
	if (_yes)
		PF("Y\n");
	else if (!fgets(line, sizeof(line), stdin) ||
		 (*line != 'y' && *line != 'Y'))
		return (0);
	return (1);
}

void main(argc, argv)
	int argc;
	char *argv[];
{
	char *buf, *p;
	int ap = 1, ac = argc;

#ifdef _MSX_DOS
	initenv();
#endif
	if (--ac > 0 && *(p = argv[ap]) == '-') {
		if (p[1] != 'y' && p[1] != 'Y') {
			fprintf(stderr, "Illegal switch %s\n", p);
#ifdef NATIVE /* Nick */
			exit(1);
#else
			xexit(-1);
#endif
		}
		++_yes;
		++ap; /* p = argv[2]; */
		--ac;
	}
#ifdef NATIVE /* Nick */
	if (ac != 1)
		{
		fprintf(stderr,"usage: fsck [-y] device\n");
		exit(1);
		}
	strcpy(device_name, argv[ap]);
	device_handle = open(device_name, O_RDWR | O_BINARY);
	if (device_handle < 0)
		{
		printf("can't open: ");
		fflush(stdout);
		perror(device_name);
		exit(1);
		}
#else
	if ((ac != 1) || ((p = argv[ap])[1] != ':') || !isalpha(*p)) {
		fprintf(stderr,"usage: fsck [-y] d:\n");
		xexit(-1);
	}
	dev = ((*p & 223) - 65);
	if (dev >= 8) {
		fprintf(stderr,"Invalid drive %c.\n",dev+65);
		xexit(-1);
	}
#endif
#ifdef _MSX_DOS
	if (!_yes) {
		PF("Insert disk and press RETURN: "); FF;
		getchar();
	}
#endif
#ifndef NATIVE /* Nick */
	bufinit();
	if (d_init() || d_open(dev)) {
		fprintf(stderr,"Can't open device number %x\n", dev);
		xexit(-1);
	}
#endif
	/* Read in the super block. */
	buf = daread(SUPERBLOCK);
	bcopy(buf, (char *)&filsys, sizeof(filesys_t)); /* Nick added cast */
	/* Verify the fsize and isize parameters */
	if (filsys.s_mounted != SMOUNTED) {
		PF("Device %x has invalid magic number %d. Fix? ",
		       dev, filsys.s_mounted);
		if (!yes())
#ifdef NATIVE /* Nick */
			exit(1);
#else
			xexit(-1);
#endif
		filsys.s_mounted = SMOUNTED;
#if 1 /* Nick free bitmap */
		/* buf = daread(SUPERBLOCK); */
		bcopy((char *)&filsys, buf, sizeof(filsys));
		dwrite(SUPERBLOCK, buf);
#else
		dwrite(SUPERBLOCK, &filsys);
#endif
	}
#ifdef NATIVE /* Nick */
	PF("\nChecking %s with fsize %u, isize %u, rsize %u. Confirm? ",
	       device_name, filsys.s_fsize, filsys.s_isize,
	       filsys.s_reserv-1-SUPERBLOCK);
#else
	PF("Checking drive %c with fsize %u, isize %u, rsize %u. Confirm? ",
	       dev+65, filsys.s_fsize, filsys.s_isize,
	       filsys.s_reserv-1-SUPERBLOCK);
#endif
	if (!yes())
#ifdef NATIVE /* Nick */
		exit(1);
#else
		xexit(-1);
#endif

#if 1 /* Nick free bitmap */
#if 0 /* remove support for the original UZI filesystem */
	if (filsys.s_bitmap_inode)
		{
#endif
		inode_bitmap = malloc(filsys.s_bitmap_block -
				      filsys.s_bitmap_inode);
		if (inode_bitmap == NULL)
			{
			p = "inode bitmap";
			goto nomem;
			}
		bfill(inode_bitmap, 0, filsys.s_bitmap_block -
				       filsys.s_bitmap_inode);

		block_bitmap = malloc(filsys.s_bitmap_immov -
				      filsys.s_bitmap_block);
		if (block_bitmap == NULL)
			{
			p = "block bitmap";
			goto nomem;
			}
		bfill(block_bitmap, 0, filsys.s_bitmap_immov -
				       filsys.s_bitmap_block);
#if 0 /* remove support for the original UZI filesystem */
		}
	else
		{
		block_bitmap = malloc(((filsys.s_fsize - 1) >> 3) + 1);
		if (block_bitmap == NULL)
			{
			p = "block bitmap";
			goto nomem;
			}
		bfill(block_bitmap, 0, ((filsys.s_fsize - 1) >> 3) + 1);
		}
#endif
#else
	block_bitmap = malloc(((filsys.s_fsize - 1) >> 3) + 1);
	if (block_bitmap == NULL) {
		p="block bitmap";
		goto nomem;
	}
	bfill(block_bitmap, 0, ((filsys.s_fsize - 1) >> 3) + 1);
#endif

	ninodes = DINODESPERBLOCK * filsys.s_isize;
	finode = filsys.s_reserv;

	link_count = (uchar *) malloc(ninodes);
	if (!link_count) {
		p="link counts";
nomem:		fprintf(stderr,"Not enough memory for %s.\n", p);
#ifdef NATIVE /* Nick */
		exit(1);
#else
		xexit(-1);
#endif
	}
	bfill(link_count, 0, ninodes); /* * sizeof(char)); */

	PF("\nPass 1: Checking inodes.\n");		FF; pass1();
#if 1 /* Nick free bitmap */
#if 0 /* remove support for the original UZI filesystem */
	if (filsys.s_bitmap_inode)
		{
#endif
		printf("Pass 2: Rebuilding free bitmaps.\n");
		fflush(stdout);
		pass2();
#if 0 /* remove support for the original UZI filesystem */
		}
	else
		{
		printf("Pass 2: Rebuilding free list.\n");
		fflush(stdout);
		pass2();
		}
#endif
#else
	PF("Pass 2: Rebuilding free list.\n");		FF; pass2();
#endif
	PF("Pass 3: Checking block allocation.\n");	FF; pass3();
	PF("Pass 4: Checking directory entries.\n");	FF; pass4();
	PF("Pass 5: Checking link counts.\n");		FF; pass5();
#ifdef NATIVE /* Nick */
	close(device_handle);
	PF("Done.\n\n");
#else
	bufsync();
	d_close(dev);
	PF("Done.\n");
#endif
	exit(0);
}

#if 1 /* Nick free bitmap */
void bitmap_dump(char *bitmap, off_t start, off_t final)
	{
	char *buf;
	blkno_t j, lm;

	j = start;
	while (j < final)
		{
		/* calculate ending position after writing block */
		lm = int_min((j + BUFSIZE) & ~(BUFSIZE - 1), final);

		/* optimisation if we are writing the entire block */
		if ((lm - j) < BUFSIZE)
			{
			buf = daread(j >> BUFSIZELOG);
			}
		else
			{
			buf = zeroed; /* no need to read the block */
			}

		/* ready to copy data into the block and write it */
		bcopy(bitmap + (j - start), buf + (j & (BUFSIZE - 1)), lm - j);
		dwrite(j >> BUFSIZELOG, buf);

		/* bump counter to continue after data just written */
		j = lm;
		}
	}

blkno_t bitmap_find(blkno_t start, blkno_t final)
	{
	char *buf, *p;
	blkno_t i, j, lm, newno;

	j = start;
	while (j < final)
		{
		/* calculate ending position for bits in this block */
		lm = int_min((j + BUFSIZE) & ~(BUFSIZE - 1), final);

		/* read block and calculate starting relevant byte */
		buf = daread(j >> BUFSIZELOG);
		p = buf + (j & (BUFSIZE - 1));

		/* ready to scan each byte looking for a bit of '0' */
		for (i = j; i < lm; i++)
			{
			if (*p != (char)0xff)
				{
				/* at least one of the 8 blocks is available */

				/* calculate which disk block it refers to */
				newno = (i - start) << 3;
				for (i = 0; i < 7; i++) /* don't check bit 7 */
					{
					if ((*p & (1 << i)) == 0)
						{
						break;
						}
					}
				newno += i;

				/* ready to turn on this bit and write back */
				*p |= 1 << i;
				dwrite(j >> BUFSIZELOG, buf);

				return newno;
				}
			p++;
			}

		/* nothing found this time, release block and loop */

		/* bump counter to continue after data just read */
		j = lm;
		}

	/* no '0' bit found within the bitmap limits */
	return (blkno_t)-1;
	}
#endif

#ifdef NATIVE /* Nick */
void mypanic(char *message)
	{
	printf("PANIC: %s", message);
	fflush(stdout);
	exit(1);
	}
#endif

