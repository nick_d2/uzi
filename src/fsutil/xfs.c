/*
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 UZIX system calls wrappers, used by UZIX utilities
**********************************************************/

#define NEED__SCALL
#define NEED__DEVIO
#define NEED__DEVFLOP
#define NEED__FILESYS

#if 1 /* Nick */
#define MAIN /* creates the public data structures */
#include "xfs.h" /* this will include unix.h and extern.h */
#else
#include "uzix.h"
#ifdef _MSX_DOS
#include "..\include\stdio.h"
#include "..\include\stdlib.h"
#include "signal.h"
#include "fcntl.h"
#else
#include <c:\tc\include\stdio.h>
#include <c:\tc\include\stdlib.h>
#endif
#include "xfs.h"
#endif

char *stringerr[] = {
	"0",
	"1 Operation not permitted",
	"2 No such file or directory",
	"3 No such process",
	"4 Interrupted system call",
	"5 I/O error",
	"6 No such device or address",
	"7 Arg list too long",
	"8 Exec format error",
	"9 Bad file number",
	"10 No child processes",
	"11 Try again",
	"12 Out of memory",
	"13 Permission denied",
	"14 Bad address",
	"15 Block device required",
	"16 Device or resource busy",
	"17 File exists",
	"18 Cross-device link",
	"19 No such device",
	"20 Not a directory",
	"21 Is a directory",
	"22 Invalid argument",
	"23 File table overflow",
	"24 Too many open files",
	"25 Not a typewriter",
	"26 Text file busy",
	"27 File too large",
	"28 No space left on device",
	"29 Illegal seek",
	"30 Read-only file system",
	"31 Too many links",
	"32 Broken pipe",
	"33 Math argument out of domain of func",
	"34 Math result not representable",
	"35 Resource deadlock would occur",
	"36 File name too long",
	"37 No record locks available",
	"38 Function not implemented",
	"39 Directory not empty",
	"40 Too many symbolic links encountered",
	"41 It's a shell script"
};

#if 1 /* Nick */
ptptr ptab_alloc(void);
void newproc(ptptr *p);
#endif

void xfs_init(bootdev, waitfordisk)
	dev_t bootdev;
	uchar waitfordisk;
{
#if 1 /* Nick */
	char *j;
	ptptr initproc;

	inint = 0;
	udata.u_euid = 0;
	udata.u_insys = 1;
	udata.u_mask = 022;

	newproc (udata.u_ptab = initproc = ptab_alloc());
	initproc->p_status = P_RUNNING;
	initproc->p_fork_inf = udata.u_page;       /* set for use in execve */

	/* User's file table
	 */
	for (j=udata.u_files; j < (udata.u_files+UFTSIZE); ++j)
		*j = -1;
#else
	arch_init();
#endif
	bufinit();
	d_init();
	/* User's file table */
	bfill(udata.u_files, -1, sizeof(udata.u_files));
	/* Open the console tty device */
	if (d_open(TTYDEV) != 0)
		panic("no tty");
	if ((root_dev = bootdev) != (NULLDEV)) {
#ifdef _MSX_DOS
		if (waitfordisk) {
			printf("Insert disk and press RETURN: ");
			while (getchar() != '\n')
				;
		}
#endif
		/* Mount the root device */
		if (fmount(root_dev, NULL, 0))
			panic("no filesys");
		if ((root_ino = i_open(root_dev, ROOTINODE)) == 0)
			panic("no root");
		udata.u_cwd = root_ino;
		udata.u_root = root_ino;
		i_ref(root_ino);
	}
	rdtime(&udata.u_time);
}

void xfs_end(void)
{
	register int j;

	for (j = 0; j < UFTSIZE; ++j) {
		if ((udata.u_files[j] & 0x80) == 0)
			doclose(j);
	}
	UZIXsync();		/* Not necessary, but a good idea. */
}

int UZIXopen(name, flag)
	char *name;
	int flag;
{
	udata.u_argn0 = (uint)name;
	udata.u_argn1 = (uint)flag;
	udata.u_argn2 = (uint)0;
	return sys_open();
}

int UZIXclose(uindex)
	int uindex;
{
	udata.u_argn0 = uindex;
	return sys_close();
}

int UZIXcreat(name, mode)
	char *name;
	mode_t mode;
{
	udata.u_argn0 = (uint)name;
	udata.u_argn1 = O_CREAT|O_WRONLY|O_TRUNC;
	udata.u_argn2 = (uint)mode;
	return sys_open();
}

int UZIXlink(name1, name2)
	char *name1;
	char *name2;
{
	udata.u_argn0 = (uint)name1;
	udata.u_argn1 = (uint)name2;
	return sys_link();
}

int UZIXsymlink(name1, name2)
	char *name1;
	char *name2;
{
	udata.u_argn0 = (uint)name1;
	udata.u_argn1 = (uint)name2;
	return sys_symlink();
}

int UZIXunlink(path)
	char   *path;
{
	udata.u_argn0 = (uint)path;
	return sys_unlink();
}

int UZIXread(d, buf, nbytes)
	int d;
	char *buf;
	uint nbytes;
{
	udata.u_argn0 = (uint)d;
	udata.u_argn1 = (uint)buf;
	udata.u_argn2 = (uint)nbytes;
	udata.u_callno = 23;
	return sys_readwrite();
}

int UZIXwrite(d, buf, nbytes)
	int d;
	char *buf;
	uint nbytes;
{
	udata.u_argn0 = (uint)d;
	udata.u_argn1 = (uint)buf;
	udata.u_argn2 = (uint)nbytes;
	udata.u_callno = 36;
	return sys_readwrite();
}

off_t UZIXlseek(file, offset, flag)
	int file;
	off_t offset;
	int flag;
{
	udata.u_argn0 = (uint)file;
	udata.u_argn1 = (uint)offset;
	udata.u_argn2 = (uint)(offset >> 16);
	udata.u_argn3 = (uint)flag;
	return sys_lseek();
}

int UZIXchdir(dir)
	char *dir;
{
	udata.u_argn0 = (uint)dir;
	return sys_chdir();
}

int UZIXmknod(name, mode, dev)
	char *name;
	mode_t mode;
	int dev;
{
	udata.u_argn0 = (uint)name;
	udata.u_argn1 = (uint)mode;
	udata.u_argn2 = (uint)dev;
	return sys_mknod();
}

void UZIXsync()
{
	sys_sync();
}

int UZIXaccess(path, mode)
	char *path;
	int mode;
{
	udata.u_argn0 = (uint)path;
	udata.u_argn1 = (uint)mode;
	return sys_access();
}

int UZIXchmod(path, mode)
	char *path;
	mode_t mode;
{
	udata.u_argn0 = (uint)path;
	udata.u_argn1 = (uint)mode;
	return sys_chmod();
}

int UZIXchown(path, owner, group)
	char *path;
	int owner;
	int group;
{
	udata.u_argn0 = (uint)path;
	udata.u_argn1 = (uint)owner;
	udata.u_argn2 = (uint)group;
	return sys_chown();
}

int UZIXstat(path, buf)
	char *path;
	void *buf;
{
	udata.u_argn0 = (uint)path;
	udata.u_argn1 = (uint)buf;
	udata.u_callno = 27;
	return sys_statfstat();
}

int UZIXfstat(fd, buf)
	int fd;
	void *buf;
{
	udata.u_argn0 = (uint)fd;
	udata.u_argn1 = (uint)buf;
	udata.u_callno = 13;
	return sys_statfstat();
}

#if 1 /* Nick free bitmap */
int UZIXfalign(fd, parm)
	int fd;
	int parm;
{
	udata.u_argn0 = (uint)fd;
	udata.u_argn1 = (uint)parm;
	return sys_falign();
}
#endif

int UZIXdup(oldd)
	int oldd;
{
	udata.u_argn0 = (uint)oldd;
	return sys_dup();
}

int UZIXdup2(oldd, newd)
	int oldd;
	int newd;
{
	udata.u_argn0 = (uint)oldd;
	udata.u_argn1 = (uint)newd;
	return sys_dup2();
}

int UZIXumask(mask)
	int mask;
{
	udata.u_argn0 = (uint)SET_UMASK;
	udata.u_argn1 = (uint)mask;
	return sys_getset();
}

int UZIXgetfsys(dev, buf)
	dev_t dev;
	void *buf;
{
	udata.u_argn0 = (uint)dev;
	udata.u_argn1 = (uint)buf;
	return sys_getfsys();
}

int UZIXioctl(fd, request, data)
	int fd;
	int request;
	void *data;
{
	udata.u_argn0 = (uint)fd;
	udata.u_argn1 = (uint)request;
	udata.u_argn2 = (uint)data;
	return sys_ioctl();
}

int UZIXmount(spec, dir, rwflag)
	char *spec;
	char *dir;
	int rwflag;
{
	udata.u_argn0 = (uint)spec;
	udata.u_argn1 = (uint)dir;
	udata.u_argn2 = (uint)rwflag;
	udata.u_callno = 19;
	return sys_mountumount();
}

int UZIXumount(spec)
	char *spec;
{
	udata.u_argn0 = (uint)spec;
	udata.u_callno = 32;
	return sys_mountumount();
}

int UZIXtime(tvec)
	void *tvec;
{
	udata.u_argn0 = (uint)tvec;
	return sys_time();
}
