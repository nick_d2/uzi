/* utils.c for uzi180 utils by Nick - subroutines based on utils.asz source */

#include "utils.h"

#define TRUE 1
#define FALSE 0

#if 0
/* convert an integer to a string in any base (2-36) */
char *itob (int n, char *s, int base)
    {
    register unsigned int u;
    register char *p, *q;
    register negative, c;
    if ((n < 0) && (base == -10)) {
        negative = TRUE;
        u = -n;
        }
    else {
        negative = FALSE;
        u = n;
        }
    if (base == -10)            /* Signals signed conversion */
         base = 10;
    p = q = s;
    do {                        /* Generate digits in reverse order */
        if ((*p = u % base + '0') > '9')
            *p += ('A' - ('9' + 1));
        ++p;
        u = u / base;
        } while (u > 0);
    if (negative)
        *p++ = '-';
    *p = '\0';                  /* Terminate the string */
    while (q < --p) {           /* Reverse the digits */
        c = *q;
        *q++ = *p;
        *p = c;
        }
    return s;
    }
#endif

void bzero(char *ptr, int count)
	{
	while (count--)
		{
		*ptr++ = 0;
		}
	}

void bfill(char *ptr, char val, int count)
	{
	while (count--)
		*ptr++ = val;
	}

void bcopy(char *src, char *dest, int count)
	{
	while (count--)
		{
		*dest++ = *src++;
		}
	}

int int_min(int a, int b)
	{
	return (b < a ? b : a);
	}

int int_max(int a, int b)
	{
	return (b > a ? b : a);
	}

