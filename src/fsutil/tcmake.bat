tcc -ms -I..\include -I..\kernel -DPC_UTILS_TARGET -c ..\kernel\devflop.c
@if errorlevel 1 goto failure
tcc -ms -I..\include -I..\kernel -DPC_UTILS_TARGET -c ..\kernel\devio.c
@if errorlevel 1 goto failure
tcc -ms -I..\include -I..\kernel -DPC_UTILS_TARGET -c ..\kernel\devmisc.c
@if errorlevel 1 goto failure
tcc -ms -I..\include -I..\kernel -DPC_UTILS_TARGET -c ..\kernel\devswap.c
@if errorlevel 1 goto failure
tcc -ms -I..\include -I..\kernel -DPC_UTILS_TARGET -c ..\kernel\devtty.c
@if errorlevel 1 goto failure
tcc -ms -I..\include -I..\kernel -DPC_UTILS_TARGET -c ..\kernel\filesys.c
@if errorlevel 1 goto failure
tcc -ms -I..\include -I..\kernel -DPC_UTILS_TARGET -c ..\kernel\machdep.c
@if errorlevel 1 goto failure
tcc -ms -I..\include -I..\kernel -DPC_UTILS_TARGET -c ..\kernel\scall1.c
@if errorlevel 1 goto failure
tcc -ms -I..\include -I..\kernel -DPC_UTILS_TARGET -c ..\kernel\scall2.c
@if errorlevel 1 goto failure

tcc -ms -I..\include -I..\kernel -DPC_UTILS_TARGET -c mkfs.c
@if errorlevel 1 goto failure
tlink @mkfs.lnk
@if errorlevel 1 goto failure

tcc -ms -I..\include -I..\kernel -DPC_UTILS_TARGET -c ucp.c
@if errorlevel 1 goto failure
tcc -ms -I..\include -I..\kernel -DPC_UTILS_TARGET -c ucpsub.c
@if errorlevel 1 goto failure
tcc -ms -I..\include -I..\kernel -DPC_UTILS_TARGET -c xfs.c
@if errorlevel 1 goto failure
tlink @ucp.lnk
@if errorlevel 1 goto failure

@echo SUCCESS
@goto done
:failure
@echo FAILURE
:done

