/*
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 UZIX utilities:

 Make Filesystem: to make an UZIX disk.
 Usage:	mkfs [-yfqv] d: fsize isize [rsize]
**********************************************************/

/* The device is given as drive letter.
 * Major device is always 0 (FDD), minor device is 0..7 (drives A..H)
 */

#define __MAIN__COMPILATION
#define NEED__DEVIO
#define SKIP_TIME_T /* Nick */

#include "utildos.h"

#if 1 /* Nick */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#ifdef NATIVE /* Nick */
#include <fcntl.h>
#ifdef VAX
#include <io.h>
#else
#include <syscalls.h>
#endif
#include "utils.h"
#endif
#else
#ifdef SEPH
#include "sys\ioctl.h"
#include "sys\stat.h"
#endif
#ifdef _MSX_DOS
#include ".\include\stdio.h"
#include ".\include\stdlib.h"
#include ".\include\ctype.h"
#else
#include <c:\tc\include\stdio.h>
#include <c:\tc\include\stdlib.h>
#include <c:\tc\include\ctype.h>
#endif
#endif

#ifdef NATIVE /* Nick */
int device_handle;
uchar device_name[0x200]; /* 16]; */

char zeroed[BUFSIZE];
#endif

char bootblock[] = {
#if 1
#include "boot.c" /* see ..\gboot\gboot.asm and ..\..\bin\boot.bin */
#else
	0xEB, 0xFE, 0x90, 'U',	'Z',  'I',  'X',  'd',
	'i',  's',  'k',  0x00, 0x02, 0x02, 0x01, 0x00,
	0x00, 0x00, 0x00, 0xA0, 0x05, 0xF9, 0x00, 0x00,
	0x09, 0x00, 0x02, 0x00, 0x00, 0x00, 0xD0, 0x36,
	0x56, 0x23, 0x36, 0xC0, 0x31, 0x1F, 0xF5, 0x11,
	0x4A, 0xC0, 0x0E, 0x09, 0xCD, 0x7D, 0xF3, 0x0E,
	0x08, 0xCD, 0x7D, 0xF3, 0xFE, 0x1B, 0xCA, 0x22,
	0x40, 0xF3, 0xDB, 0xA8, 0xE6, 0xFC, 0xD3, 0xA8,
	0x3A, 0xFF, 0xFF, 0x2F, 0xE6, 0xFC, 0x32, 0xFF,
	0xFF, 0xC7, 0x57, 0x41, 0x52, 0x4E, 0x49, 0x4E,
	0x47, 0x21, 0x07, 0x0D, 0x0A, 0x0A, 0x54, 0x68,
	0x69, 0x73, 0x20, 0x69, 0x73, 0x20, 0x61, 0x6E,
	0x20, 0x55, 0x5A, 0x49, 0x58, 0x20, 0x64, 0x69,
	0x73, 0x6B, 0x2C, 0x20, 0x6E, 0x6F, 0x6E, 0x20,
	0x62, 0x6F, 0x6F, 0x74, 0x61, 0x62, 0x6C, 0x65,
	0x2E, 0x0D, 0x0A, 0x55, 0x73, 0x69, 0x6E, 0x67,
	0x20, 0x69, 0x74, 0x20, 0x75, 0x6E, 0x64, 0x65,
	0x72, 0x20, 0x4D, 0x53, 0x58, 0x44, 0x4F, 0x53,
	0x20, 0x63, 0x61, 0x6E, 0x20, 0x64, 0x61, 0x6D,
	0x61, 0x67, 0x65, 0x20, 0x69, 0x74, 0x2E, 0x0D,
	0x0A, 0x0A, 0x48, 0x69, 0x74, 0x20, 0x45, 0x53,
	0x43, 0x20, 0x66, 0x6F, 0x72, 0x20, 0x42, 0x41,
	0x53, 0x49, 0x43, 0x20, 0x6F, 0x72, 0x20, 0x61,
	0x6E, 0x79, 0x20, 0x6B, 0x65, 0x79, 0x20, 0x74,
	0x6F, 0x20, 0x72, 0x65, 0x62, 0x6F, 0x6F, 0x74,
	0x2E, 0x24, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
#endif
};

/* This makes a filesystem */
dev_t dev;
direct_t dirbuf[DIRECTPERBLOCK] = {
	{ ROOTINODE, "." },
	{ ROOTINODE, ".."}
};
dinode_t inode[DINODESPERBLOCK];

#if 1 /* Nick free bitmap */
off_t	bitmap_inode;
off_t	bitmap_block;
off_t	bitmap_immov;
off_t	bitmap_final;

char	*inode_bitmap;
char	*block_bitmap;
char	*immov_bitmap;
#endif

uchar _fmt = 0; 	/* do low level formatting */
uchar _yes = 0; 	/* yes to all questions */
uchar _quick = 0;	/* quick initialization */
uchar _verb = 0;	/* be verbose */
#if 1 /* Nick free bitmap */
#if 0 /* remove support for the original UZI filesystem */
uchar _bitmap = 0;	/* use a free bitmap rather than a free list */
#endif
uchar _immov = 0;	/* create a locking bitmap as well as free bitmaps */
#endif

#if 1 /* Nick fsck compatible */
char *daread(uint blk);
void dwrite(uint blk, void *addr);
int da_read(dev_t,uint,void *);
int da_write(dev_t,uint,void *);
#else
void dwrite(uint, void *);
#endif
int yes(char *);
void mkfs(uint, uint, uint);
void doformatting(uint fsize);
#if 1 /* Nick free bitmap */
void bitmap_dump(char *bitmap, off_t start, off_t final);
#endif

#if 1 /* Nick fsck compatible */
char *daread(blk)
	uint blk;
{
	static char buf[BUFSIZE];

	if (da_read(dev, blk, buf) != BUFSIZE) {
		PF("Read of block %d failed.\n", blk);
		abort();
	}
	return (buf);
}

void dwrite(blk, addr)
	uint blk;
	void *addr;
{
	if (da_write(dev, blk, addr) != BUFSIZE) {
		PF("Write of block %d failed.\n", blk);
		abort();
	}
}

int da_read(dev, blk, addr)
	dev_t dev;
	uint blk;
	void *addr;
{
#ifdef NATIVE /* Nick */
	if (lseek(device_handle, ((long)blk) << BUFSIZELOG, SEEK_SET) < 0)
		{
		printf("can't seek: ");
		fflush(stdout);
		perror(device_name);
		exit(1);
		}

	if (read(device_handle, addr, BUFSIZE) != BUFSIZE)
		{
 if (errno == 0)
  {
  bfill(addr, 0xaa, BUFSIZE); /* for creation of unpadded initrd images */
  }
 else
  {
		printf("can't read: ");
		fflush(stdout);
		perror(device_name);
		exit(1);
  }
		}
#else
	bufptr buf = bread(dev, blk, 0);

	if (buf == NULL)
		return 0;
	bcopy(buf, addr, BUFSIZE);
	bfree(buf, 0);
#endif
	return BUFSIZE;
}

int da_write(dev, blk, addr)
	dev_t dev;
	uint blk;
	void *addr;
{
#ifdef NATIVE /* Nick */
	if (lseek(device_handle, ((long)blk) << BUFSIZELOG, SEEK_SET) < 0)
		{
		printf("can't seek: ");
		fflush(stdout);
		perror(device_name);
		exit(1);
		}

	if (write(device_handle, addr, BUFSIZE) != BUFSIZE)
		{
		printf("can't write: ");
		fflush(stdout);
		perror(device_name);
		exit(1);
		}
#else
	bufptr buf = bread(dev, blk, 1);

	if (buf == NULL)
		return 0;
	bcopy(addr, buf, BUFSIZE);
	bfree(buf, 1);
#endif
	return BUFSIZE;
}
#else
void dwrite(blk, addr)
	uint blk;
	void *addr;
{
#ifdef NATIVE /* Nick */
	if (lseek(device_handle, ((long)blk) << BUFSIZELOG, SEEK_SET) < 0)
		{
		printf("can't seek: ");
		fflush(stdout);
		perror(device_name);
		exit(1);
		}

	if (write(device_handle, addr, BUFSIZE) != BUFSIZE)
		{
		printf("can't write: ");
		fflush(stdout);
		perror(device_name);
		exit(1);
		}
#else
	char *buf = bread(dev, blk, 2);

	if (buf == NULL) {
		printf("mkfs: disk is not MS(X)DOS compatible, device or drive error\n");
		xexit(1);
	}
	bcopy(addr, buf, BUFSIZE);
	bfree((bufptr)buf, 2);
#endif
}
#endif

int yes(p)
	char *p;
{
	char line[20];

	fputs(p,stdout);
	FF;
	if (_yes)
		fputs("Y\n", stdout);
	else if (fgets(line, sizeof(line), stdin) == NULL ||
		 (*line != 'y' && *line != 'Y'))
		return (0);
	return (1);
}

void mkfs(fsize, isize, rsize)
	uint fsize, isize, rsize;
{
	uint j, lm;
	filesys_t fs;
#ifdef NATIVE /* Nick */
#if 1 /* Nick fsck compatible */
	char *buf;

	if (_verb)
		{
		PF("Creating boot block...\n");	FF;
		}
#if 1 /* Nick generic boot routine */
	for (j = 0; j < sizeof(bootblock); j += BUFSIZE)
		{
		/* writing a complete block, or the last partial block? */
		lm = int_min(sizeof(bootblock) - j, BUFSIZE);

		if (lm < BUFSIZE)
			{
			buf = daread(j >> BUFSIZELOG);
			}
		else
			{
			buf = zeroed;
			}

		bcopy(&bootblock[j], buf, lm);
		dwrite(j >> BUFSIZELOG, buf);
		}
#else
	buf = daread(0);
	/* Preserve disk data (number of sectors, format type, etc) */
	for (j = 11; j < 30; j++) bootblock[j] = buf[j];
	/* Preserve other relevant data (we just use first 256 bytes) */
	for (j = 256; j < 512; j++) bootblock[j] = buf[j];
	/* Write new boot block */
	bootblock[0x10] = rsize;
	dwrite(0, bootblock);
#endif
#else
	char buf[BUFSIZE];

	if (_verb)
		{
		PF("Creating boot block...\n");	FF;
		}

	if (lseek(device_handle, 0L, SEEK_SET) < 0)
		{
		printf("can't seek: ");
		fflush(stdout);
		perror(device_name);
		exit(1);
		}

	if (read(device_handle, buf, BUFSIZE) != BUFSIZE)
		{
		printf("can't read: ");
		fflush(stdout);
		perror(device_name);
		exit(1);
		}

	/* Preserve disk data (number of sectors, format type, etc) */
	for (j = 11; j < 30; j++) bootblock[j] = buf[j];
	/* Preserve other relevant data (we just use first 256 bytes) */
	for (j = 256; j < 512; j++) bootblock[j] = buf[j];
	/* Write new boot block */
	bootblock[0x10] = rsize;
	dwrite(0, bootblock);
#endif
#else
	char zeroed[BUFSIZE];
	char *buf = bread(dev, 0, 0);

	if (buf == NULL) {
		printf("mkfs: disk is not MS(X)DOS compatible, device or drive error\n");
		xexit(1);
	}
	if (_verb) {
		PF("Creating boot block...\n");	FF;
	}
	/* Preserve disk data (number of sectors, format type, etc) */
	for (j = 11; j < 30; j++) bootblock[j] = buf[j];
	/* Preserve other relevant data (we just use first 256 bytes) */
	for (j = 256; j < 512; j++) bootblock[j] = buf[j];
	/* Write new boot block */
	bfree((bufptr)buf, 0);
	bootblock[0x10] = rsize;
	dwrite(0, bootblock);
#endif

	/* Zero out the blocks */
	bzero(zeroed, BUFSIZE);
	if (_verb) {
		PF("Initializing inodes, be patient...\n"); FF;
	}
	if (_quick) {
#if 0 /* Nick, don't write last block, done by my doformatting() routine */
		dwrite(fsize-1,zeroed); 	/* Last block */
#endif
		lm = SUPERBLOCK+1+rsize+isize;	/* Super+Reserv+Inodes */
	}
	else	lm = fsize;			/* All blocks of filesys */
#if 1 /* Nick, don't write reserved area, done by my doformatting() routine */
	j = SUPERBLOCK+1+rsize;
#else
	j = 1;
#endif
	while (j < lm) {
		if (_verb && j % 9 == 0) {
			PF("Blk #%d\r", j); FF;
		}
		dwrite(j++, zeroed);
	}
	if (_verb) {
		PF("Blk #%d\n",--j); FF;
	}
	/* Initialize the super-block */
	if (_verb) {
#ifdef NATIVE
		PF("Creating super block...\n"); FF;
#else
		PF("Creating super-block...\n"); FF;
#endif
	}
	bzero((char *)&fs,sizeof(fs)); /* Nick added cast */
	fs.s_mounted = SMOUNTED;	/* Magic number */
	fs.s_reserv = SUPERBLOCK+1+rsize;
	fs.s_isize = isize;
	fs.s_fsize = fsize;
	fs.s_tinode = DINODESPERBLOCK * isize - 2;
#if 1 /* Nick free bitmap */
	fs.s_tfree = fsize - (SUPERBLOCK+1+rsize+isize+1); /* +1 block */
	fs.s_bitmap_inode = bitmap_inode;
	fs.s_bitmap_block = bitmap_block;
	fs.s_bitmap_immov = bitmap_immov;
	fs.s_bitmap_final = bitmap_final;

#if 0 /* remove support for the original UZI filesystem */
	if (_bitmap)
		{
#endif
		/* mark all system reserved blocks as used in the bitmap */
		for (j = 0; j < SUPERBLOCK+1+rsize+isize+1; j++) /* +1 block */
			{
			block_bitmap[j >> 3] |= 1 << (j & 7);
			}

		/* mark all out-of-range blocks as used in the bitmap */
		lm = (bitmap_immov - bitmap_block) << 3; /* may wrap to 0 */
		for (j = fsize; j != lm; j++)
			{
			block_bitmap[j >> 3] |= 1 << (j & 7);
			}

		/* dump bitmap, preserving unused bytes of reserved blocks */
		bitmap_dump(block_bitmap, bitmap_block, bitmap_immov);
#if 0 /* remove support for the original UZI filesystem */
		}
	else
		{
		/* Free each block, building the free list */
		j = fsize - 1;
		while (j >= SUPERBLOCK+1+rsize+isize+1) /* +1 block root dir */
			{
			if (fs.s_nfree == FSFREEBLOCKS)
				{
				dwrite(j, (char *) &fs.s_nfree);
				fs.s_nfree = 0;
				bzero((char *)fs.s_free,sizeof(fs.s_free)); /* Nick */
				}
			fs.s_free[fs.s_nfree++] = j--;
			}
		}
#endif

	if (_immov)
		{
		/* mark all system reserved blocks as used in the bitmap */
		for (j = 0; j < SUPERBLOCK+1+rsize+isize+1; j++) /* +1 block */
			{
			immov_bitmap[j >> 3] |= 1 << (j & 7);
			}

		/* mark all out-of-range blocks as used in the bitmap */
		lm = (bitmap_final - bitmap_immov) << 3; /* may wrap to 0 */
		for (j = fsize; j != lm; j++)
			{
			immov_bitmap[j >> 3] |= 1 << (j & 7);
			}

		/* dump bitmap, preserving unused bytes of reserved blocks */
		bitmap_dump(immov_bitmap, bitmap_immov, bitmap_final);
		}
#else
	/* Free each block, building the free list */
	j = fsize - 1;
	while (j > SUPERBLOCK+1+rsize+isize) { /* +1 block root dir */
		if (fs.s_nfree == FSFREEBLOCKS) {
			dwrite(j, (char *) &fs.s_nfree);
			fs.s_nfree = 0;
			bzero((char *)fs.s_free,sizeof(fs.s_free)); /* Nick */
		}
		/* fs.s_tfree++; */
		fs.s_free[fs.s_nfree++] = j--;
	}
#endif
	/* The inodes are already zeroed out */
	/* create the root dir */
	inode[ROOTINODE].i_mode = S_IFDIR | 0755;
	inode[ROOTINODE].i_nlink = 3;
	inode[ROOTINODE].i_size = sizeof(direct_t)*2;
	inode[ROOTINODE].i_addr[0] = SUPERBLOCK+1+rsize+isize;
	/* Reserve reserved inode */
	inode[0].i_nlink = 1;
	inode[0].i_mode = ~0;
#if 1 /* Nick free bitmap */
#if 0 /* remove support for the original UZI filesystem */
	if (_bitmap)
		{
#endif
		/* mark all system reserved inodes as used in the bitmap */
		inode_bitmap[0] |= 1;
		inode_bitmap[ROOTINODE >> 3] |= 1 << (ROOTINODE & 7);

		/* mark all out-of-range inodess as used in the bitmap */
		lm = (bitmap_block - bitmap_inode) << 3; /* may wrap to 0 */
		for (j = DINODESPERBLOCK * isize; j != lm; j++)
			{
			inode_bitmap[j >> 3] |= 1 << (j & 7);
			}

		/* dump bitmap, preserving unused bytes of reserved blocks */
		bitmap_dump(inode_bitmap, bitmap_inode, bitmap_block);
#if 0 /* remove support for the original UZI filesystem */
		}
	else
		{
		/* Free inodes in first inode block */
		j = ROOTINODE+1;
		while (j < DINODESPERBLOCK)
			{
			if (fs.s_ninode == FSFREEINODES)
				{
				break;
				}
			fs.s_inode[fs.s_ninode++] = j++;
			}
		}
#endif
#else
	/* Free inodes in first inode block */
	j = ROOTINODE+1;
	while (j < DINODESPERBLOCK) {
		if (fs.s_ninode == FSFREEINODES)
			break;
		fs.s_inode[fs.s_ninode++] = j++;
	}
#endif
	dwrite(SUPERBLOCK+1+rsize, inode);
	dwrite(SUPERBLOCK+1+rsize+isize, dirbuf);
	/* Write out super block */
#if 1 /* Nick fsck compatible */
	buf = daread(SUPERBLOCK);
	bcopy((char *)&fs, buf, sizeof(fs));
	dwrite(SUPERBLOCK, buf);
#else
	dwrite(SUPERBLOCK, &fs);
#endif
}

void doformatting(uint fsize)
{
#ifdef _MSX_DOS
	if (_verb) {
		PF("Low level formatting:\n"); FF;
	}
	asm("	push ix");
	asm("	push iy");
	asm("	ld ix,0147h");		/* FORMAT function (it asks user) */
	asm("	ld iy,(0fcc0h)");	/* ROM BIOS slot */
	asm("	call 001Ch");
	asm("	pop iy");
	asm("	pop ix");
#else
#ifdef __ONFILE
	uint j;
	char zeroed[BUFSIZE];

	/* apply minimum size, in case never formatted before */
	fdinfo[MINOR(dev)].size = min(fsize, fdinfo[MINOR(dev)].size);

	/* write the entire disk, growing the file if necessary */
	bfill(zeroed, 0xaa, BUFSIZE);
	for (j = 0; j < fsize; j++)
		{
		dwrite(j, zeroed);
		}
#else
#if 1 /* Nick */
	uint j;
	char zeroed[BUFSIZE];

	if (_verb) {
		PF("Low level formatting...\n"); FF;
	}

	/* write the entire disk, growing the file if necessary */
	bfill(zeroed, 0xaa, BUFSIZE);
	for (j = 0; j < fsize; j++)
		{
		dwrite(j, zeroed);
		}
#else
	fprintf(stderr, "Use FORMAT for low level formatting!\n");
#endif
#endif
#endif
}

void main(argc, argv)
	int argc;
	char *argv[];
{
	uint fsize, isize, rsize = 0;
	int ap = 1, ac = argc;
	uchar *p;

#ifdef _MSX_DOS
	initenv();
#endif
	while (ap < argc && *(p = (uchar *)argv[ap]) == '-') {
		++p;
		++ap;
		--ac;
		while (*p) {
			switch (*p++) {
			case 'y': case 'Y':	_yes = 1;	break;
			case 'f': case 'F':	_fmt = 1;	break;
			case 'q': case 'Q':	_quick = 1;	break;
			case 'v': case 'V':	_verb = 1;	break;
#if 1 /* Nick free bitmap */
			case 'l': case 'L':	_immov = 1;	/* fallthru */
#if 1 /* remove support for the original UZI filesystem */
								break;
#else
			case 'b': case 'B':	_bitmap = 1;	break;
#endif
#endif
			default:
				fprintf(stderr, "Illegal switch %s\n", p);
#ifdef NATIVE /* Nick */
				exit(1);
#else
				xexit(-1);
#endif
			}
		}
	}
#ifdef NATIVE /* Nick */
	if (ac < 4) {
		/* parameters order agree with FSCK */
#if 1 /* Nick free bitmap */
		fprintf(stderr, "usage: mkfs [-yfqvb] device fsize isize [rsize]\n");
#else
		fprintf(stderr, "usage: mkfs [-yfqv] device fsize isize [rsize]\n");
#endif
		exit(1);
#else
	if ((ac < 4) || (argv[ap+0][1] != ':') || !isalpha(argv[ap+0][0])) {
		/* parameters order agree with FSCK */
		fprintf(stderr, "usage: mkfs [-yfqv] d: fsize isize [rsize]\n");
		xexit(-1);
#endif
	}
#ifdef NATIVE /* Nick */
	strcpy(device_name, argv[ap]);
	device_handle = open(device_name, O_RDWR | O_BINARY);
	if (device_handle < 0)
		{
		printf("can't open: ");
		fflush(stdout);
		perror(device_name);
		exit(1);
		}
#else
	dev = ((argv[ap+0][0] & 223) - 65);
#endif
	fsize = (uint) atoi(argv[ap+1]);
	isize = (uint) atoi(argv[ap+2]);
	if (ac > 4)
		rsize = (uint) atoi(argv[ap+3]);
#ifndef NATIVE
	if (dev >= 8) {
		fprintf(stderr,"Invalid drive %c:.\n",dev+65);
		xexit(-1);
	}
#endif
	if (fsize < 100 || isize >= fsize/30 || rsize > 100) {
		fprintf(stderr,"Bad parameter values\n");
#ifdef NATIVE /* Nick */
		exit(1);
#else
		xexit(-1);
#endif
	}
#if 1 /* Nick free bitmap */
#if 0 /* remove support for the original UZI filesystem */
	if (_bitmap)
		{
#endif
		bitmap_inode = SUPERBLOCK * BUFSIZE + sizeof(filesys_t);
		bitmap_inode = (bitmap_inode + 3) & ~3;

		bitmap_block = bitmap_inode +
			       ((DINODESPERBLOCK * isize - 1) >> 3) + 1;
		bitmap_block = (bitmap_block + 3) & ~3;

		bitmap_immov = bitmap_block + ((fsize - 1) >> 3) + 1;
		bitmap_immov = (bitmap_immov + 3) & ~3;

		bitmap_final = bitmap_immov;
		rsize = ((bitmap_final + (BUFSIZE - 1)) >> BUFSIZELOG) -
			(SUPERBLOCK + 1);
#if 0 /* remove support for the original UZI filesystem */
		}
#endif

	if (_immov)
		{
		bitmap_final = bitmap_immov + ((fsize - 1) >> 3) + 1;
		bitmap_final = (bitmap_final + 3) & ~3;

		rsize = ((bitmap_final + (BUFSIZE - 1)) >> BUFSIZELOG) -
			(SUPERBLOCK + 1);
		}
#endif
#ifdef NATIVE /* Nick */
	PF("\nMaking filesystem on %s, fsize %u, isize %u, rsize %u. ",
	       device_name, fsize, isize, rsize); FF;
#else
	PF("\nMaking filesystem on drive %c, fsize %u, isize %u, rsize %u. ",
	       dev+65, fsize, isize, rsize); FF;
#endif
	if (!yes("Confirm? "))
#ifdef NATIVE /* Nick */
		exit(1);
#else
		xexit(-1);
#endif
#ifdef _MSX_DOS
	if (!_yes) {
		PF("Insert disk and press RETURN: ");	FF;
		getchar();
	}
#endif
	PF("\n");
	if (_fmt)
		doformatting(fsize);
#if 1 /* Nick free bitmap */
#if 0 /* remove support for the original UZI filesystem */
	if (_bitmap)
		{
#endif
		inode_bitmap = malloc(bitmap_block - bitmap_inode);
		if (inode_bitmap == NULL)
			{
			p = "inode bitmap";
			goto nomem;
			}
		bfill(inode_bitmap, 0, bitmap_block - bitmap_inode);

		block_bitmap = malloc(bitmap_final - bitmap_block);
		if (block_bitmap == NULL)
			{
			p = "block bitmap";
			goto nomem;
			}
		bfill(block_bitmap, 0, bitmap_final - bitmap_block);
#if 0 /* remove support for the original UZI filesystem */
		}
#endif

	if (_immov)
		{
		immov_bitmap = malloc(bitmap_final - bitmap_immov);
		if (immov_bitmap == NULL)
			{
			p = "lock bitmap";
nomem:			fprintf(stderr,"Not enough memory for %s.\n", p);
			exit(1);
			}
		bfill(immov_bitmap, 0, bitmap_final - bitmap_immov);
		}
#endif
#ifndef NATIVE /* Nick */
	bufinit();
	if (d_init() || d_open(dev)) {
		fprintf(stderr, "Can't open device %x",dev);
		xexit(-1);
	}
#endif
	mkfs(fsize, isize, rsize);
#ifdef NATIVE /* Nick */
	close(device_handle);
#else
	d_close(dev);
	bufsync();
#endif
	if (_verb)
#ifdef NATIVE /* Nick */
		PF("Filesystem on %s successfully initialized!\n\n",
		   device_name);
#else
		PF("Filesystem on drive %c successfully initialized!\n",
		dev+65);
#endif
	exit(0);
}

#if 1 /* Nick free bitmap */
void bitmap_dump(char *bitmap, off_t start, off_t final)
	{
	char *buf;
	blkno_t j, lm;

	/* dump bitmap, preserving unused bytes of reserved blocks */
	j = start;
	while (j < final)
		{
		/* calculate ending position after writing block */
		lm = int_min((j + BUFSIZE) & ~(BUFSIZE - 1), final);

		/* optimisation if we are writing the entire block */
		if ((lm - j) < BUFSIZE)
			{
			buf = daread(j >> BUFSIZELOG);
			}
		else
			{
			buf = zeroed; /* no need to read the block */
			}

		/* ready to copy data into the block and write it */
		bcopy(bitmap + (j - start), buf + (j & (BUFSIZE - 1)), lm - j);
		dwrite(j >> BUFSIZELOG, buf);

		/* bump counter to continue after data just written */
		j = lm;
		}
	}
#endif

