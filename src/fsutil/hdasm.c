/* hdasm.c for uzi180 utils by Nick - implements a simple disk emulator */

#include <io.h>
#include <fcntl.h>
#include <sys/stat.h>

extern char cmdblk[8]; /* for scsiop(), defined in devhd.c */
extern long hd_offset;
extern long hd_sector;
extern char *dptr;
extern int dlen;
extern char *cptr;
extern int busid;

int scsiop(void);

int scsiop(void)
	{
	int count;
	long position;
	static int drive_handle = -1;
	static char *drive_name = "uzidisk.dat";

#if DEBUG > 1
	printf("    %s sector %08x count %02x\n",
			cmdblk[1] == 'W' ? "Writing" : "Reading",
			hd_sector, cmdblk[7]);
#endif

	if (drive_handle < 0)
		{
		drive_handle = open(drive_name, O_RDWR | O_BINARY);
		if (drive_handle < 0)
			{
			return 1; /* says we failed the operation */
			}
		}

	count = cmdblk[7] * 0x200;
	position = hd_sector * 0x200;

	if (lseek(drive_handle, position, 0) != position)
		{
		return 2; /* says we failed the operation */
		}

	switch (cmdblk[1])
		{

	case 'R':
		if (read(drive_handle, dptr, count) != count)
			{
			return 3; /* says we failed the operation */
			}
		break;

	case 'W':
		if (write(drive_handle, dptr, count) != count)
			{
			return 4; /* says we failed the operation */
			}
		break;

		}

	return 0; /* says the operation was successful */
	}

