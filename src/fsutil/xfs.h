/*
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 UZIX system calls wrappers, used by UZIX utilities
**********************************************************/

#ifndef _XFS_H
#define _XFS_H

#if 1 /* Nick */
#include <vendor.h>
#include <unix.h>
#include <config.h>
#include <extern.h>
#else
#include "uzix.h"
#include "unix.h"
#include "extern.h"
#endif

extern char *stringerr[];

extern void xfs_init(dev_t bootdev, uchar waitfordisk);
extern void xfs_end(void);
extern int UZIXopen(char *name, int flag/*, ...*/);
extern int UZIXclose(int uindex);
extern int UZIXcreat(char *name, mode_t mode);
extern int UZIXlink(char *name1, char *name2);
extern int UZIXsymlink(char *name1, char *name2);
extern int UZIXunlink(char *path);
extern int UZIXread(int d, char *buf, uint nbytes);
extern int UZIXwrite(int d, char *buf, uint nbytes);
extern int UZIXseek(int file, uint offset, int flag);
extern int UZIXchdir(char *dir);
extern int UZIXmknod(char *name, mode_t mode, int dev);
extern void UZIXsync(void);
extern int UZIXaccess(char *path, int mode);
extern int UZIXchmod(char *path, mode_t mode);
extern int UZIXchown(char *path, int owner, int group);
extern int UZIXstat(char *path, void *buf);
extern int UZIXfstat(int fd, void *buf);
#if 1 /* Nick free bitmap */
extern int UZIXfalign(int fd, int parm);
#endif
extern int UZIXdup(int oldd);
extern int UZIXdup2(int oldd, int newd);
extern int UZIXumask(int mask);
extern int UZIXioctl(int fd, int request, void *data);
extern int UZIXmount(char *spec, char *dir, int rwflag);
extern int UZIXumount(char *spec);
extern int UZIXtime(void *tvec);

extern int UZIXgetfsys(dev_t dev, void *buf);
#endif
