// hymem.cpp

// ----------------------------------------------------------------------------

#include <stdio.h>
#include <windows.h>
#pragma hdrstop
#include "hymem.h"

// ----------------------------------------------------------------------------

void MemEntrySetup(void)
     {
     // for future expansion
     }

void MemExitCleanup(void)
     {
     // for future expansion
     }

// ----------------------------------------------------------------------------

int fiMemAlloc(MEMTAG *pmt, int iBytes)
     {
     // zero the entire memory block handle area, avoiding leaks
     memset(pmt, 0, sizeof(MEMTAG));

     pmt->pcBase = (char *) malloc(iBytes);
     if (pmt->pcBase == NULL)
          {
          return FALSE; // indicates failure to allocate block
          }

     pmt->pcLimit = pmt->pcBase + iBytes;

     return TRUE; // indicates success and handle set up
     }

int fiMemResize(MEMTAG *pmt, int iBytes)
     {
     char *pc;
     int iBytesIn;

     if (iBytes == 0)
          {
          pc = NULL;
          free(pmt->pcBase);
          }
     else
          {
          pc = (char *) realloc(pmt->pcBase, iBytes);
          if (pc == NULL)
               {
               return FALSE; // indicates failure to reallocate block
               }
          }

     iBytesIn = iMemSize(pmt);
     if (iBytes > iBytesIn)
          {
          memset(pc + iBytesIn, 0, iBytes - iBytesIn);
          }

     pmt->pcBase = pc;
     pmt->pcLimit = pmt->pcBase + iBytes;

     return TRUE; // indicates success and handle updated
     }

int fiMemFree(MEMTAG *pmt)
     {
#if 0
     if (pmt->pcBase == NULL)
          {
          return FALSE; // indicates no such block allocated
          }
#endif

     free(pmt->pcBase); // this can't fail, apparently

     // zero the entire memory block handle area, avoiding leaks
     memset(pmt, 0, sizeof(MEMTAG));

     return TRUE; // indicates success, and handle zeroed
     }

// ----------------------------------------------------------------------------

#ifdef _INC_JLHANDLE
int hiMemNew(int iBytes)
     {
     int hi;
     MEMTAG *pmt;

     // take a new handle, before bothering with memory
     hi = hiJLHandleNew(patMem);
     pmt = (MEMTAG *) pcArrayElement(patMem, 1, hi);

     // use inline wrapper to allocate the required bytes
     MemAlloc(pmt, iBytes); // this will bomb on failure

     return hi; // caller's reference is the opaque handle
     }

void MemResize(int hi, int iBytes)
     {
     MEMTAG *pmt;

     // find supplied handle, before bothering with memory
     pmt = (MEMTAG *) pcArrayElement(patMem, 1, hi);

     // use inline wrapper to reallocate the required bytes
     MemRealloc(pmt, iBytes); // this will bomb on failure
     }

void MemDelete(int hi)
     {
     // find supplied handle, before bothering with memory
     pmt = (MEMTAG *) pcArrayElement(patMem, 1, hi);

     // use inline wrapper to free up the allocated memory
     MemFree(pmt); // this will bomb on failure (null pcBase)
     }

int hiMemDelete(int hi)
     {
     MemDelete(hi);
     return 0; // handy way for caller to zero out its copy of handle
     }
#endif

// ----------------------------------------------------------------------------

