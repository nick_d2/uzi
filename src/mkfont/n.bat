cl -Zi -I. -DDEBUG=0 -Tp hfmdump.c -Tp hfmlib.c
@if errorlevel 1 goto failure
copy hfmdump.exe ..\bin

cl -Zi -I. -DDEBUG=0 -Tp hfmwidth.c hfmlib.obj
@if errorlevel 1 goto failure
copy hfmwidth.exe ..\bin

cl -Zi -I. -DDEBUG=0 hfmtool.cpp hfmlib.obj bmp2txt.cpp txt2chs.cpp chs2cmd.cpp hystring.cpp hycache.cpp hylist.cpp hyimage.cpp hyfile.cpp hyfs.cpp hymem.cpp
@if errorlevel 1 goto failure
copy hfmtool.exe ..\bin

cl -Zi -I. -DDEBUG=0 restool.cpp po2c.cpp hystring.obj hycache.obj hylist.obj hyimage.obj hyfile.obj hyfs.obj hymem.obj
@if errorlevel 1 goto failure
copy restool.exe ..\bin

cl -Zi -I. -DDEBUG=0 -DSTANDALONE po2c.cpp hystring.obj hycache.obj hylist.obj hyimage.obj hyfile.obj hyfs.obj hymem.obj
@if errorlevel 1 goto failure
copy po2c.exe ..\bin

cl -Zi -I. -DDEBUG=0 -DSTANDALONE bmp2txt.cpp hycache.obj hylist.obj hyimage.obj hyfile.obj hyfs.obj hymem.obj
@if errorlevel 1 goto failure
copy bmp2txt.exe ..\bin

cl -Zi -I. -DDEBUG=0 -DSTANDALONE txt2chs.cpp hycache.obj hylist.obj hyfile.obj hyfs.obj hymem.obj
@if errorlevel 1 goto failure
copy txt2chs.exe ..\bin

cl -Zi -I. -DDEBUG=0 -DSTANDALONE chs2cmd.cpp hycache.obj hylist.obj hyfile.obj hyfs.obj hymem.obj
@if errorlevel 1 goto failure
copy chs2cmd.exe ..\bin

@echo SUCCESS
@goto done
:failure
@echo FAILURE
:done

