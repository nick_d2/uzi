// bmp2txt.cpp

// ----------------------------------------------------------------------------

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <windows.h>
#pragma hdrstop

#ifndef DEBUG
#define DEBUG 0
#endif

#include "hymem.h"
#include "hyfs.h"
#include "hyfile.h"
#include "hyimage.h"
#include "hylist.h"
#include "hycache.h"
#include "bmp2txt.h"

// ----------------------------------------------------------------------------
// local preprocessor definitions

#define LINE_MAX	0x200
#define PATH_MAX	0x200

#define CACHE_MAX	0x10 // note:  this is smaller than hfmtool's setting

#define OUTPUT_BUFFER	0x100000 // to create output files of up to 1 Megabyte

// ----------------------------------------------------------------------------

#ifdef STANDALONE
int main(int argc, char **argv);
#endif
static int fiProcess(FILETAG *pftOut, BMPTAG *pbtIn);

// ----------------------------------------------------------------------------

#ifdef STANDALONE
int main(int argc, char **argv)
	{
	int i;
        LISTTAG ltCache;

	// set up the cache to track intermediate memory files
        CacheEntrySetup(&ltCache, CACHE_MAX);

	// run the real program, passing in the cache descriptor
	i = iBmp2Txt(&ltCache, argc, argv);

	// it just goes on and on my friend
	CacheExitFlush(&ltCache, 3); // force writing of intermediate files
	CacheExitCleanup(&ltCache); // deallocate memory, though it's redundant

	return i;
	}
#endif

// ----------------------------------------------------------------------------

int iBmp2Txt(LISTTAG *pltCache, int argc, char **argv)
	{
	int i;

	BMPTAG btIn;
	char *pszInFileName;

	int iCacheOut;
	FILETAG *pftOut;
	char *pszOutFileName;
	char szOutFileName[PATH_MAX];

	pszInFileName = NULL;
	pszOutFileName = NULL;

	if (argc > 1)
		{
		pszInFileName = argv[1];
		}

	if (argc > 2)
		{
		pszOutFileName = argv[2];
		}

	if (pszInFileName == NULL)
		{
		printf("usage: bmp2txt infile.bmp [outfile.txt]\n");
		exit(1);
		}

	if (pszOutFileName == NULL)
		{
		pszOutFileName = szOutFileName;
		strcpy(pszOutFileName, pszInFileName);

		i = strlen(pszOutFileName);
		while (i--)
			{
			if (pszOutFileName[i] == '\\')
				{
				break; /* no extension, so don't strip it */
				}
			if (pszOutFileName[i] == '.')
				{
				pszOutFileName[i] = 0; /* strip dot and extension */
				break; /* ready to concatenate our extension */
				}
			}

		strcat(pszOutFileName, ".txt");
		}

	if (!strcmp(pszInFileName, pszOutFileName))
		{
		printf("Input and output filenames identical\n");
		exit(1);
		}

	// read the input bmp file entirely to a malloc'd block
	if (fiBmpReadIn(&btIn, pszInFileName) == FALSE)
		{
		printf("Could not read %s\n", pszInFileName);
		exit(1);
		}

	printf("Loaded %s, 0x%08x samples\n", pszInFileName,
	       btIn.iSampleAppend);

	// prepare an output buffer for character set data
	CacheAllocate(pltCache, &iCacheOut, OUTPUT_BUFFER);
	pftOut = pftCacheItem(pltCache, iCacheOut);

	// search for characters and write character set files
	if (fiProcess(pftOut, &btIn) == FALSE)
		{
		exit(1);
		}

	// ready to write the output we found
	CacheWriteOut(pltCache, iCacheOut, pszOutFileName);

	// some people started singing it not knowing what it was
	BmpFree(&btIn);

	return 0;
	}

// ----------------------------------------------------------------------------

static int fiProcess(FILETAG *pftOut, BMPTAG *pbtIn)
	{
	int m, x, y, t;
	int i, j, k, l;
	int n, o, p, q /*, u*/;
	int r, s;
	int cx, cy;
	int iCurrent, iState, iSample, iOffset;
	int *piTotal, *piRow, *piColumn;
	MEMTAG mtTotal, mtRow, mtColumn;
	char *pc;

	cx = pbtIn->pbih->biWidth;
	cy = pbtIn->pbih->biHeight;

#if 0
	if (pbtIn->iChannels != 1)
		{
		printf("fiProcess: Channels %d (must be 1)\n",
		       pbtIn->iChannels);
		return FALSE;
		}
#endif

	// prepare to find total of black values per pixel row
	MemAlloc(&mtTotal, cy * sizeof(int));
	piTotal = (int *)mtTotal.pcBase;

	// prepare to find row indices of the character lines
	MemAlloc(&mtRow, (cy + 1) * sizeof(int));
	piRow = (int *)mtRow.pcBase;

	// find blackness of each row and divide into character lines
	r = 0;
	iState = 0;
	iCurrent = cy * pbtIn->iSamplesLine;
	for (y = cy - 1; y >= 0; y--)
		{
		iCurrent -= pbtIn->iSamplesLine;
		iSample = iCurrent;

		t = 0;
		for (x = 0; x < cx; x++)
			{
			for (m = 0; m < pbtIn->iChannels; m++)
				{
				t += pbtIn->pucSample[iSample++] ^ 0xff;
				}
			}

		piTotal[y] = t; // total of dark pixels for row

		m = (t != 0); // 0 = all white, 1 = something found
		if (iState != m)
			{
			iState = m;
			piRow[r++] = y + 1; // record change of state
			}
		}

	if (iState)
		{
		piRow[r++] = y + 1; // record change of state to white
		}

	// prepare to find column indices of individual characters
	MemAlloc(&mtColumn, (cx + 1) * sizeof(int));
	piColumn = (int *)mtColumn.pcBase;

	// loop through the character lines found
	pc = pftOut->pcBase;
	for (i = 0; i < r; i += 2)
		{
		j = piRow[i + 1]; // start line of region
		k = piRow[i]; // line after end of region

		// optional feature to find baseline ignoring descenders
		t = 0;
		for (y = j; y < k; y++)
			{
			t += piTotal[y]; // total black counts for region
			}

		l = j; // assume line after bottom is baseline
		t = t * 2 / (3 * (k - j)); // 2/3 of the average black count
		for (y = j; y < k; y++)
			{
			if (piTotal[y] >= t) // is line blacker than this?
				{
				l = y; // found the line after baseline
				break;
				}
			}

		// find blackness of each column and divide into characters
		s = 0;
		iState = 0;
		iCurrent = j * pbtIn->iSamplesLine;
		iOffset = pbtIn->iSamplesLine - pbtIn->iChannels;
		for (x = 0; x < cx; x++)
			{
			iSample = iCurrent;
			iCurrent += pbtIn->iChannels;

			t = 0;
			for (y = j; y < k; y++)
				{
				for (m = 0; m < pbtIn->iChannels; m++)
					{
					t += pbtIn->pucSample[iSample++] ^
					     0xff;
					}
				iSample += iOffset;
				}

			m = (t != 0); // 0 = all white, 1 = something found
			if (iState != m)
				{
				iState = m;
				piColumn[s++] = x; // record change of state
				}
			}

		if (iState)
			{
			piColumn[s++] = x; // record change of state to white
			}

		// loop through the individual characters found
		for (n = 0; n < s; n += 2)
			{
			o = piColumn[n]; // start column of character
			p = piColumn[n + 1]; // column after end of character

			pc += sprintf(pc, "at %d,%d size %d,%d\r\n",
				      o, cy - j, p + 1 - o, k - j);

			iCurrent = k * pbtIn->iSamplesLine;
			iOffset = o * pbtIn->iChannels;
			for (y = k - 1; y >= j; y--)
				{
				iCurrent -= pbtIn->iSamplesLine;
				iSample = iCurrent + iOffset;

				q = (y == l) ? '_' : ' ';
				/*u = '|';*/
				for (x = o; x < p; x++)
					{
					t = 0;
					for (m = 0; m < pbtIn->iChannels; m++)
						{
						if
						  ((pbtIn->pucSample[iSample++]
						  ^ 0xff) >= 0x80)
							{
							t = 1;
							}
						}
					if (t)
						{
						*pc++ = '#';
						*pc++ = '#';
						}
					else
						{
						*pc++ = q; /*u;*/
						*pc++ = q;
						}
					/*u = q;*/
					}

				*pc++ = q;
				*pc++ = q;
				*pc++ = '\r';
				*pc++ = '\n';
				}

#if 0
			/*u = '|';*/
			for (x = o; x < p; x++)
				{
				*pc++ = ' '; /*u;*/
				*pc++ = ' ';
				/*u = ' ';*/
				}

			*pc++ = ' ';
			*pc++ = ' ';
			*pc++ = '\r';
			*pc++ = '\n';
#endif
			}

		}

	MemFree(&mtColumn);
	MemFree(&mtRow);
	MemFree(&mtTotal);

	pftOut->pcAppend = pc;

	return TRUE;
	}

// ----------------------------------------------------------------------------

