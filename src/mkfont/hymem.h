// hymem.h

#ifndef _INC_HYMEM
#define _INC_HYMEM

// ----------------------------------------------------------------------------

typedef struct
     {
     char *pcBase;                      // also the 'handle' for malloc & free
     char *pcLimit;                     // similar to Microsoft's _msize func.
     } MEMTAG;

// ----------------------------------------------------------------------------

#ifdef _INC_JLHANDLE
// the optional registration of allocated memory blocks is based on a handle
// system identical to JLHANDLE.CPP, see comments for atJLHandle, patJLH etc

extern ARRAYTAG atMemHandle;

extern int hiMemHandleFree; // to head of most recently used free handles list
extern ARRAYTAG *patMemH; // patMemH is slightly more readable than &atMemHandle
#endif

// ----------------------------------------------------------------------------

void MemEntrySetup(void);
void MemExitCleanup(void);

int fiMemAlloc(MEMTAG *pmt, int iBytes);
int fiMemResize(MEMTAG *pmt, int iBytes);
int fiMemFree(MEMTAG *pmt);

// ----------------------------------------------------------------------------

inline int iMemSize(MEMTAG *pmt)
     {
     return pmt->pcLimit - pmt->pcBase;
     }

// ----------------------------------------------------------------------------
// handy functions for the client, not important to the interface:

#ifdef _INC_STDIO
inline void MemAlloc(MEMTAG *pmt, int iBytes)
     {
     if (fiMemAlloc(pmt, iBytes) == FALSE)
          {
          printf("MemAlloc: "
                 "Could not allocate 0x%08x bytes, exiting\n", iBytes);
          exit(1);
          }

#if DEBUG
     printf("MemAlloc: "
            "Allocated memory 0x%08x size 0x%08x\n", pmt, iBytes);
#endif
     }

inline void MemResize(MEMTAG *pmt, int iBytes)
     {
     if (fiMemResize(pmt, iBytes) == FALSE)
          {
          printf("MemResize: "
                 "Could not resize to 0x%08x bytes, exiting\n", iBytes);
          exit(1);
          }

#if DEBUG
     printf("MemResize: "
            "Resized memory 0x%08x size 0x%08x\n", pmt, iBytes);
#endif
     }

inline void MemFree(MEMTAG *pmt)
     {
     if (fiMemFree(pmt) == FALSE)
          {
          printf("MemFree: "
                 "Could not free memory 0x%08x, exiting\n", pmt);
          exit(1);
          }

#if DEBUG
     printf("MemFree: "
            "Freed memory 0x%08x\n", pmt);
#endif
     }
#endif

// ----------------------------------------------------------------------------

#endif

