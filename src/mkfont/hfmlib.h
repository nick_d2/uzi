/* hfmlib.h by Nick for Hytech Font Manager system */

/* ------------------------------------------------------------------------- */
/* preprocessor definitions */

#define HFM_MAGIC 0x6668

#define	HFM_FORMAT 1

#define HFMF_SANS 0
#define HFMF_SERIF 1

#define HFMST_CONDENSED 0
#define HFMST_REGULAR 1
#define HFMST_BOLD 2
#define HFMST_ITALIC 3
#define HFMST_UNDERLINE 4

#define HFMSTR_SANS "sans"
#define HFMSTR_SERIF "serif"
#define HFMSTR_CONDENSED "condensed"
#define HFMSTR_REGULAR "regular"
#define HFMSTR_BOLD "bold"
#define HFMSTR_ITALIC "italic"
#define HFMSTR_UNDERLINE "underline"

#define	HFM_PATH "/lib/apibus.hfm"

#define HFM_REF(base, pointer) ((char *)pointer - (char *)(base))
#define HFM_DEREF(base, offset) ((char *)(base) + (int)(offset))

/* ------------------------------------------------------------------------- */
/* struct and type definitions */

typedef struct s_hfm
	{
	unsigned short hfm_magic;		/* int = HFM_MAGIC */
	unsigned short hfm_format;		/* int = HFM_FORMAT */
	unsigned short hfm_file_size;		/* int = char count for file */
	unsigned short hfm_device_count;	/* int = hfm_device_t count */
	unsigned short hfm_device_table;	/* ptr -> hfm_device_t array */
	} hfm_t;

typedef struct s_hfm_device
	{
	unsigned short hfmd_slot_count;		/* int = hfm_slot_t count */
	unsigned short hfmd_slot_table;		/* ptr -> hfm_slot_t array */
	unsigned short hfmd_family_count;	/* int = hfm_family_t count */
	unsigned short hfmd_family_table;	/* ptr -> hfm_family_t array */
	} hfm_device_t;

typedef struct s_hfm_slot
	{
	unsigned short hfmsl_char_first;	/* int = first char (ascii) */
	unsigned short hfmsl_char_count;	/* int = count (from first) */
	unsigned short hfmsl_width_table;	/* ptr -> char array */
	unsigned short hfmsl_bitmap_table;	/* ptr -> ptr array */

	unsigned short hfmsl_x_total;		/* int = cell width pixels */
	unsigned short hfmsl_y_total;		/* int = cell height pixels */
	unsigned short hfmsl_x_space;		/* int = interchar space pix */
	unsigned short hfmsl_y_space;		/* int = interline space pix */
	} hfm_slot_t;

typedef struct s_hfm_family
	{
	unsigned short hfmf_style_count;	/* int = hfm_style_t count */
	unsigned short hfmf_style_table;	/* ptr -> hfm_style_t array */
	} hfm_family_t;

typedef struct s_hfm_style
	{
	unsigned short hfmst_height;		/* int = font height pixels */
	unsigned short hfmst_style;		/* int = font style HFMST_? */
	unsigned short hfmst_slot;		/* int = device's font index */
	} hfm_style_t;

/* ------------------------------------------------------------------------- */
/* exported function prototypes */

int hfm_load(char *hfm_path);
void hfm_free(void);

int hfm_search_family(int device, int family,
		      int height_min, int height_max,
		      hfm_family_t *hfmf_ret);
int hfm_search_family_auto(int device, int family,
			   int height_min, int height_max,
			   hfm_family_t *hfmf_ret);

int hfm_search_style(hfm_family_t *hfmf, int style, int *slot_ret);
int hfm_search_style_auto(hfm_family_t *hfmf, int style, int *slot_ret);

hfm_slot_t *hfm_locate(int device, int slot);

int hfm_width(int device, int slot, char *string);
int hfm_count_in_field(int device, int slot, char *string,
		       int field_width, int *width_ret);

#ifdef _MSC_VER
char *hfm_family_int_to_string(int family);
int hfm_family_string_to_int(char *family);
char *hfm_style_int_to_string(int style);
int hfm_style_string_to_int(char *style);
#endif

/* ------------------------------------------------------------------------- */
/* exported global variables */

extern hfm_t *hfm;

/* ------------------------------------------------------------------------- */

