/* hdmlib.h by Nick for Hytech Device Manager system */

/* ------------------------------------------------------------------------- */
/* preprocessor definitions */

#define HDM_DEVICES 3

#define HDM_DEVICE_LCD0 0
#define HDM_DEVICE_LCD1 1
#define HDM_DEVICE_LPR0 2

#define HDM_PATH_LCD0 "/dev/lcd0"
#define HDM_PATH_LCD1 "/dev/lcd1"
#define HDM_PATH_LPR0 "/dev/lpr0"

/* ------------------------------------------------------------------------- */
/* struct and type definitions */

typedef struct s_hdm
	{
	FILE *hdm_stream[HDM_DEVICES];
	} hdm_t;

/* ------------------------------------------------------------------------- */
/* exported function prototypes */

int hdm_open(void);
void hdm_close(void);

void hdm_select(int device, int slot);
int hdm_print_in_field(int device, int slot, char *string,
		       int field_width, int *width_ret);

/* ------------------------------------------------------------------------- */
/* exported inline functions */

#define hdm_getc(device) \
	fgetc(hdm->hdm_stream[(device)])

#define hdm_putc(device, value) \
	fputc((value), hdm->hdm_stream[(device)])

#define hdm_read(device, buffer, count) \
	fread((buffer), (count), sizeof(char), hdm->hdm_stream[(device)])

#define hdm_write(device, buffer, count) \
	fwrite((buffer), (count), sizeof(char), hdm->hdm_stream[(device)])

#define hdm_print(device, buffer) \
	fputs((buffer), hdm->hdm_stream[(device)])

#define hdm_flush(device) \
	fflush(hdm->hdm_stream[(device)])

/* ------------------------------------------------------------------------- */
/* exported global variables */

extern hdm_t *hdm;

/* ------------------------------------------------------------------------- */

