// tolc.h by Nick for Hytech Resource Manager system

#ifndef _INC_TOLC
#define _INC_TOLC

#if 0
typedef struct
	{
	FILETAG ftIn;
	FILETAG ftOut;
	int iPrefix; // affects operation of fiMessCompare
	int iSuffix; // affects operation of fiMessCompare
	} MESSTAG;

extern LISTTAG ltMess; // list of type MESSTAG

#if 0
int fiUnescAll(void);
#endif
int fiSort(void);
int iMessCompare(const void *pvLeft, const void *pvRight);
int fiUniq(void);
int fiMessEqual(const void *pvLeft, const void *pvRight);

int fiSuck(FILETAG *pft, FILETAG *pftIn, char *pcTerm, int iSizeTerm);
int fiUnesc(FILETAG *pft, FILETAG *pftIn, char *pcTerm, int iSizeTerm);
#endif

int iTolc(LISTTAG *pltCache, int argc, char **argv);
int fiProcess(int (*pffiHandler)(FILETAG *pftWork), FILETAG *pftWork);

#endif

