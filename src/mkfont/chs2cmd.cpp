// chs2cmd.cpp

// ----------------------------------------------------------------------------

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>
#pragma hdrstop

#ifndef DEBUG
#define DEBUG 0
#endif

#include "hymem.h"
#include "hyfs.h"
#include "hyfile.h"
#include "hylist.h"
#include "hycache.h"
#include "chs2cmd.h"

// ----------------------------------------------------------------------------
// local preprocessor definitions

#define LINE_MAX	0x200
#define PATH_MAX	0x200

#define CACHE_MAX	0x10 // note:  this is smaller than hfmtool's setting

#define OUTPUT_BUFFER	0x10000 // to create output files of up to 64 kbytes

// ----------------------------------------------------------------------------

#ifdef STANDALONE
int main(int argc, char **argv);
#endif
static int fiProcess(FILETAG *pftOut, FILETAG *pftIn, int iSlot);

// ----------------------------------------------------------------------------

#ifdef STANDALONE
int main(int argc, char **argv)
	{
	int i;
        LISTTAG ltCache;

	// set up the cache to track intermediate memory files
        CacheEntrySetup(&ltCache, CACHE_MAX);

	// run the real program, passing in the cache descriptor
	i = iChs2Cmd(&ltCache, argc, argv);

	// it just goes on and on my friend
	CacheExitFlush(&ltCache, 3); // force writing of intermediate files
	CacheExitCleanup(&ltCache); // deallocate memory, though it's redundant

	return i;
	}
#endif

// ----------------------------------------------------------------------------

int iChs2Cmd(LISTTAG *pltCache, int argc, char **argv)
	{
	int i, iSlot;
#if 0 // modification so that we will append to files cached earlier
	CACHETAG *pct;
#endif

	int iCacheIn;
	FILETAG *pftIn;
	char *pszInFileName;

	int iCacheOut;
	FILETAG *pftOut;
	char *pszOutFileName;
	char szOutFileName[PATH_MAX];

	pszInFileName = NULL;
	pszOutFileName = NULL;

	if (argc > 1)
		{
		pszInFileName = argv[1];
		}

	iSlot = 0;
	if (argc > 2)
		{
		iSlot = atoi(argv[2]);
		if (iSlot < 1 || iSlot > 0xff)
			{
			pszOutFileName = argv[2];
			if (argc > 3)
				{
				iSlot = atoi(argv[3]);
				}
			}
		}

	if (pszInFileName == NULL)
		{
		printf("usage: chs2cmd infile.txt [outfile.cmd] [slot]\n");

		exit(1);
		}

	if (pszOutFileName == NULL)
		{
		pszOutFileName = szOutFileName;
		strcpy(pszOutFileName, pszInFileName);

		i = strlen(pszOutFileName);
		while (i--)
			{
			if (pszOutFileName[i] == '\\')
				{
				break; /* no extension, so don't strip it */
				}
			if (pszOutFileName[i] == '.')
				{
				pszOutFileName[i] = 0; /* strip dot and extension */
				break; /* ready to concatenate our extension */
				}
			}

		strcat(pszOutFileName, ".cmd");
		}

	if (strcmp(pszInFileName, pszOutFileName) == 0)
		{
		printf("Input and output filenames identical\n");
		exit(1);
		}

	// read the input chs file entirely to a malloc'd block
	CacheReadIn(pltCache, &iCacheIn, pszInFileName);
	pftIn = pftCacheItem(pltCache, iCacheIn);

#if 1 // modification so that we will append to files cached earlier
	if (fiCacheSearch(pltCache, &iCacheOut, pszOutFileName))
		{
		// the file was found in the cache, assume block is oversized
		}
	else
		{
		CacheAllocate(pltCache, &iCacheOut, OUTPUT_BUFFER);
		}
	pftOut = pftCacheItem(pltCache, iCacheOut);

	// append esc plus character set data to output buffer
	if (fiProcess(pftOut, pftIn, iSlot) == FALSE)
		{
		exit(1);
		}
#else
	// grab an unallocated cache entry for FileCopyPadded()
	iCacheOut = pltCache->iItemAppend; // this must be done first
	pct = (CACHETAG *) pcListPut(pltCache); // this will bomb if full
	pct->iDirty = 1; // emulate the action of CacheAllocate()
	pftOut = &pct->ft; // emulate the action of pftCacheItem()

	// create output buffer with esc plus character set data
	FileCopyPadded(pftOut, pftIn, 5, 0);
	pftOut->pcBase[0] = 0x1b;
	pftOut->pcBase[1] = '.';
	pftOut->pcBase[2] = iSlot + '0';
	*(short *)&pftOut->pcBase[3] = iFileSize(pftIn);
#endif

	// ready to write the output we found
	CacheWriteOut(pltCache, iCacheOut, pszOutFileName);
	pctCacheItem(pltCache, iCacheOut)->iDirty = 4; // extra dirty

	// it's the song that doesn't end
	return 0;
	}

// ----------------------------------------------------------------------------

static int fiProcess(FILETAG *pftOut, FILETAG *pftIn, int iSlot)
	{
	char *pc;
	int iSizeIn, iSizeOut;

	// append esc plus character set data to output buffer
	iSizeIn = iFileSize(pftIn);
	iSizeOut = 5 + iSizeIn;

	if (pftOut->pcAppend + iSizeOut > pftOut->pcLimit)
		{
		printf("No room left to append output file\n");
		return FALSE;
		}

	pc = pftOut->pcAppend;

	pc[0] = 0x1b;
	pc[1] = '.';
	pc[2] = iSlot + '0';
	*(short *)&pc[3] = iSizeIn;

	memcpy(&pc[5], pftIn->pcBase, iSizeIn);

	pftOut->pcAppend += iSizeOut;
	return TRUE;
	}

// ----------------------------------------------------------------------------

