/* hfmwidth.c by Nick for Hytech Font Manager system */

/* ------------------------------------------------------------------------- */
/* header and preprocessor directives */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef DEBUG
#define DEBUG 0
#endif

#include "hfmlib.h"

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#define LINE_MAX 0x200

/* ------------------------------------------------------------------------- */
/* function prototypes */

int main(int argc, char **argv);

/* ------------------------------------------------------------------------- */
/* global variables */

char buffer[0x100];

/* ------------------------------------------------------------------------- */

int main(int argc, char **argv)
	{
	int i, j;
	char *infile, *outfile;
	FILE *fileout;
	int device, slot;
	hfm_slot_t *hfmsl;
	char line[LINE_MAX];

	if (argc < 5)
		{
		printf("usage: hfmwidth infile.hfm outfile.c device slot\n");
		return 1;
		}

	infile = argv[1];
	outfile = argv[2];
	device = atoi(argv[3]);
	slot = atoi(argv[4]);

	if (hfm_load(infile) == FALSE)
		{
		return 1;
		}

	fileout = fopen(outfile, "w");
	if (fileout == NULL)
		{
		hfm_free();
		fprintf(stderr, "can't create: ");
		perror(outfile);
		return 1;
		}

	hfmsl = hfm_locate(device, slot);
	if (hfmsl == NULL)
		{
		hfm_free();
		fprintf(stderr, "hfm_locate(%d, %d) returns NULL\n",
				device, slot);
		return 1;
		}

	printf("Device %d slot %d: "
	       " range=%d,%d"
	       " width=0x%04x bitmap=0x%04x"
	       " total=%d,%d space=%d,%d\n",
	       device, slot,
	       hfmsl->hfmsl_char_first,
	       hfmsl->hfmsl_char_count,
	       hfmsl->hfmsl_width_table,
	       hfmsl->hfmsl_bitmap_table,
	       hfmsl->hfmsl_x_total,
	       hfmsl->hfmsl_y_total,
	       hfmsl->hfmsl_x_space,
	       hfmsl->hfmsl_y_space);

	/* fill out the table with the standard cell width */
	memset(buffer, hfmsl->hfmsl_x_total, 0x100);

	/* copy in those widths which are not the standard */
	memcpy(buffer + hfmsl->hfmsl_char_first,
	       HFM_DEREF(hfm, hfmsl->hfmsl_width_table),
	       hfmsl->hfmsl_char_count);

	strcpy(line, "/* ");
	strcat(line, outfile);
	strcat(line, " generated from ");
	strcat(line, infile);
	strcat(line, ", do not edit! */\n");
	fputs(line, fileout);

	*line = 0; /* causes a blank line to be written at start */

	j = 0;
	while (j < 0x100)
		{
		strcat(line, "\n");
		fputs(line, fileout);

		*line = 0;
		for (i = 0; i < 8; i++)
			{
			sprintf(&line[i*5], "0x%02x,", buffer[j++] & 0xff);
			}
		}

	if (*line)
		{
		line[strlen(line)-1] = 0; /* kill trailing comma */
		strcat(line, "\n");
		fputs(line, fileout);

		*line = 0; /* causes a blank line to be written at end */
		}

	strcat(line, "\n");
	fputs(line, fileout);

	fclose(fileout);

	hfm_free();
	return 0;
	}

/* ------------------------------------------------------------------------- */

