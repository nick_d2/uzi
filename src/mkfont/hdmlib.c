/* hdmlib.c by Nick for Hytech Device Manager system */

/* ------------------------------------------------------------------------- */
/* header and preprocessor directives */

#include <stdio.h>
#include <malloc.h>

#include "hfmlib.h"
#include "hdmlib.h"

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

/* ------------------------------------------------------------------------- */
/* global variables */

/* note:  the library is allowed exactly 1 global variable, which must be of */
/* pointer type.  if there is a need for more, we must implement a global_t */

hdm_t *hdm;

/* ------------------------------------------------------------------------- */
/* exported functions */

int hdm_open(void)
	{
	FILE *stream;

	if (hdm)
		{
		fprintf(stderr, HDM_PATH_LCD0 ": already open\n");
		return FALSE;
		}

	stream = fopen(HDM_PATH_LCD0, "rw");
	if (stream == NULL)
		{
		perror(HDM_PATH_LCD0);
		return FALSE;
		}

	hdm = malloc(sizeof(hdm_t));
	if (hdm == NULL)
		{
		fprintf(stderr, HDM_PATH_LCD0 ": out of memory\n");
		return FALSE;
		}

	hdm->hdm_stream[HDM_DEVICE_LCD0] = stream;
	hdm->hdm_stream[HDM_DEVICE_LCD1] = fopen(HDM_PATH_LCD1, "rw");
	hdm->hdm_stream[HDM_DEVICE_LPR0] = fopen(HDM_PATH_LPR0, "rw");
	return TRUE;
	}

void hdm_close(void)
	{
	if (hdm)
		{
		fclose(hdm->hdm_stream[HDM_DEVICE_LCD0]);
		fclose(hdm->hdm_stream[HDM_DEVICE_LCD1]);
		fclose(hdm->hdm_stream[HDM_DEVICE_LPR0]);

		free(hdm);
		hdm = NULL;
		}
	}

/* ------------------------------------------------------------------------- */

void hdm_select(int device, int slot)
	{
	hdm_putc(device, 0x1b);
	hdm_putc(device, '0' + slot);
	}

int hdm_print_in_field(int device, int slot, char *string,
		       int field_width, int *width_ret)
	{
	int count;

	count = hfm_count_in_field(device, slot, string,
				   field_width, width_ret);
	return hdm_write(device, string, count);
	}

/* ------------------------------------------------------------------------- */

