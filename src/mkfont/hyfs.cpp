// hyfs.cpp

// ----------------------------------------------------------------------------

#include <io.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdio.h>
#include <windows.h>
#include <string.h>
#pragma hdrstop
#include "hyfs.h"

// ----------------------------------------------------------------------------

void FSEntrySetup(void)
     {
     // for future expansion, including GetModuleFileName
     }

void FSExitCleanup(void)
     {
     // for future expansion
     }

// ----------------------------------------------------------------------------

int fiFSOpen(FSHANDLE *pfsh, char *pszName)
     {
#if 1 /* munging to accept unix style paths */
 int i;

 pszName = strdup(pszName);
 if (pszName == NULL)
  {
  perror("strdup()");
  exit(1);
  }
 for (i = strlen(pszName) - 1; i >= 0; i--)
  {
  if (pszName[i] == '/')
   {
   pszName[i] = '\\';
   }
  }
#endif

     // clear filestreamtag, although we should also free name and error if
     // not null, so calling convention needs to be revised (zero it for now)
     memset(pfsh, 0, sizeof(FSHANDLE));

     // set up fields to print a report on closure
     //pfsh->iMode = 0;
     //pfsh->iCount = 0;

     // attempt to open file or device for reading
     pfsh->pszName = pszName;
     pfsh->iHandle = open(pszName, O_RDONLY | O_BINARY);
     if (pfsh->iHandle < 0)
          {
          pfsh->iError = IDS_ERROR_FS_OPEN;
          return FALSE;
          }

     // indicate successful operation
     //pfsh->iError = 0;
     return TRUE;
     }

int fiFSCreate(FSHANDLE *pfsh, char *pszName)
     {
#if 1 /* munging to accept unix style paths */
 int i;

 pszName = strdup(pszName);
 if (pszName == NULL)
  {
  perror("strdup()");
  exit(1);
  }
 for (i = strlen(pszName) - 1; i >= 0; i--)
  {
  if (pszName[i] == '/')
   {
   pszName[i] = '\\';
   }
  }
#endif

     // clear filestreamtag, although we should also free name and error if
     // not null, so calling convention needs to be revised (zero it for now)
     memset(pfsh, 0, sizeof(FSHANDLE));

     // set up fields to print a report on closure
     pfsh->iMode = 1;
     //pfsh->iCount = 0;

     // attempt to create file or open device for writing
     pfsh->pszName = pszName;
     pfsh->iHandle = open(pszName, O_RDWR | O_CREAT | O_TRUNC | O_BINARY,
                                   S_IREAD | S_IWRITE);
     if (pfsh->iHandle < 0)
          {
          pfsh->iError = IDS_ERROR_FS_CREATE;
          return FALSE;
          }

     // indicate successful operation
     //pfsh->iError = 0;
     return TRUE;
     }

int fiFSClose(FSHANDLE *pfsh)
     {
     int i;

     // attempt to close file, noting success
     i = close(pfsh->iHandle);
     if (i < 0) // check msdn
          {
          pfsh->iError = IDS_ERROR_FS_CLOSE;
          return FALSE;
          }
     pfsh->iHandle = -1; // for no good reason

     // indicate successful operation
     pfsh->iError = 0;
     return TRUE;
     }

// ----------------------------------------------------------------------------

int iFSRead(FSHANDLE *pfsh, char *pcOut, int iLimit)
     {
     int i;

     // attempt to open file or device for reading
     i = read(pfsh->iHandle, pcOut, iLimit);
     if (i < 0) // check msdn
          {
          pfsh->iError = IDS_ERROR_FS_READ;
          return 0;
          }

     // maintain fields to print a report on closure
     pfsh->iCount += i;

     // indicate successful operation
     pfsh->iError = 0;
     return i;
     }

int iFSWrite(FSHANDLE *pfsh, char *pcIn, int iLimit)
     {
     int i;

     // attempt to open file or device for reading
     i = write(pfsh->iHandle, pcIn, iLimit);
     if (i < 0) // check msdn
          {
          pfsh->iError = IDS_ERROR_FS_WRITE;
          return 0;
          }

     // maintain fields to print a report on closure
     pfsh->iCount += i;

     // indicate successful operation
     pfsh->iError = 0;
     return i;
     }

// ----------------------------------------------------------------------------

