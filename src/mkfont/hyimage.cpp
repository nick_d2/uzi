// hyimage.cpp

// ----------------------------------------------------------------------------

#include <stdio.h>
#include <math.h>
#include <windows.h>
#pragma hdrstop
#include "hyfile.h"
#include "hyimage.h"

// ----------------------------------------------------------------------------

BITMAPFILEHEADER bfhDefault =
     {
     0x4D42, // bfType = 'BM'
     0,      // bfSize
     0,      // bfReserved1
     0,      // bfReserved2
     0,      // bfOffBits
     };

/*
BITMAPINFOHEADER bihDefault =
     {
     0x46465952, // iFileId = 'RIFF'
     0,          // iFileSize
     0x45564157, // iTypeId = 'BMPE'
     0x20746d66, // iFormatId = 'fmt_'
     0x10,       // iFormatSize
     1,          // siFormatTag
     2,          // siChannels
     44100,      // iPixelRate
     176400,     // iByteRate
     4,          // siAlignment
     16,         // siBitsPixel
     0x61746164, // iDataId = 'data'
     0           // iDataSize
     };
*/

// ----------------------------------------------------------------------------

int fiBmpAllocate(BMPTAG *pbt, BITMAPINFOHEADER *pbihIn,
                  int cxPixels, int cyPixels)
     {
     int iPalette;
     int iBitsPixel, iBitsLine;
     int iPixelsLine, iBytes;

     // fill important bmptag fields from the user's bmp header
     pbt->iChannels = max(1, pbihIn->biBitCount / 8);
     pbt->iBitsChannel = pbihIn->biBitCount / pbt->iChannels;

     // calculate overall image size and required storage
     iBitsPixel = pbt->iChannels * pbt->iBitsChannel;
     iBitsLine = (cxPixels * iBitsPixel + 0x1f) & 0xffffffe0;

     pbt->iSamplesLine = iBitsLine / pbt->iBitsChannel;

     iBytes = cyPixels * (iBitsLine / 8);

     iPalette = 0;
     if (pbt->iChannels == 1)
          {
          iPalette = sizeof(RGBQUAD) << pbt->iBitsChannel;
          }

     // allocate and initialise memory block for header + data
     if (fiFileAllocate(&pbt->ft, sizeof(BITMAPFILEHEADER) +
                                  sizeof(BITMAPINFOHEADER) +
                                  iPalette + iBytes) == FALSE)
          {
          return FALSE;
          }

     memset(pbt->ft.pcBase, 0, pbt->ft.pcLimit - pbt->ft.pcBase);

     // fill bmptag fields which refer to the allocated memory block
     pbt->pbfh = (BITMAPFILEHEADER *) (pbt->ft.pcBase);
     pbt->pbih = (BITMAPINFOHEADER *) (pbt->ft.pcBase +
                                       sizeof(BITMAPFILEHEADER));
     pbt->prgbq = (RGBQUAD *) (pbt->ft.pcBase +
                               sizeof(BITMAPFILEHEADER) +
                               sizeof(BITMAPINFOHEADER));
     pbt->pucSample = (unsigned char *) (pbt->ft.pcBase +
                                        sizeof(BITMAPFILEHEADER) +
                                        sizeof(BITMAPINFOHEADER));
     if (pbt->iChannels == 1)
          {
          pbt->pucSample += sizeof(RGBQUAD) << pbt->iBitsChannel;
          }

     // initialise bitmap header within the allocated block
     memcpy(pbt->pbfh, &bfhDefault, sizeof(BITMAPFILEHEADER));
     pbt->pbfh->bfOffBits = (char *) (pbt->pucSample) - pbt->ft.pcBase;
     pbt->pbfh->bfSize = pbt->pbfh->bfOffBits + iBytes;

     memcpy(pbt->pbih, pbihIn, sizeof(BITMAPINFOHEADER));
     pbt->pbih->biWidth = cxPixels;
     pbt->pbih->biHeight = cyPixels;

     // initialise the remainder of the bmptag for client
     pbt->iSampleBase = 0;
     pbt->iSampleLimit = pbt->pbih->biHeight * pbt->iSamplesLine;
     pbt->iSamplePosition = 0;
     pbt->iSampleAppend = 0;

     //pbt->rPixelWindow.left = 0;
     //pbt->rPixelWindow.right = pbt->pbih->biWidth;
     //pbt->rPixelWindow.bottom = 0;
     //pbt->rPixelWindow.top = pbt->pbih->biHeight;

     //pbt->rPixelSurface.left = 0;
     //pbt->rPixelSurface.right = iBitsLine / iBitsPixel;
     //pbt->rPixelSurface.bottom = 0;
     //pbt->rPixelSurface.top = pbt->pbih->biHeight;

     // say we met with success
     return TRUE;
     }

// ----------------------------------------------------------------------------

int fiBmpReadIn(BMPTAG *pbt, char *pszFileName)
     {
     int iBitsPixel, iBitsLine, iPixelsLine;

     // attempt to read the client's requested file fully
     if (fiFileReadIn(&pbt->ft, pszFileName) == FALSE)
          {
          return FALSE;
          }

     // fill bmptag fields which refer to the allocated memory block
     pbt->pbfh = (BITMAPFILEHEADER *) (pbt->ft.pcBase);
     pbt->pbih = (BITMAPINFOHEADER *) (pbt->ft.pcBase +
                                       sizeof(BITMAPFILEHEADER));
     pbt->prgbq = (RGBQUAD *) (pbt->ft.pcBase +
                               sizeof(BITMAPFILEHEADER) +
                               sizeof(BITMAPINFOHEADER));

     // check the file format in place before using header fields
     if ((pbt->pbih->biPlanes != 1) ||
         ((pbt->pbih->biBitCount != 1) &&
          (pbt->pbih->biBitCount != 8) &&
          (pbt->pbih->biBitCount != 24) &&
          (pbt->pbih->biBitCount != 32)))
         {
         printf("fiBmpReadIn: Invalid BMP file %s\n", pszFileName);
         FileFree(&pbt->ft);
         return FALSE;
         }
// printf("%d %d\n", pbt->pbih->biWidth, pbt->pbih->biHeight);

     // fill important bmptag fields from the user's bmp header
     pbt->iChannels = max(1, pbt->pbih->biBitCount / 8);
     pbt->iBitsChannel = pbt->pbih->biBitCount / pbt->iChannels;
// printf("%d %d\n", pbt->iChannels, pbt->iBitsChannel);

     pbt->pucSample = (unsigned char *) (pbt->ft.pcBase +
                                        sizeof(BITMAPFILEHEADER) +
                                        sizeof(BITMAPINFOHEADER));
     if (pbt->iChannels == 1)
          {
          pbt->pucSample += sizeof(RGBQUAD) << pbt->iBitsChannel;
          }

     iBitsPixel = pbt->iChannels * pbt->iBitsChannel;
     iBitsLine = (pbt->pbih->biWidth * iBitsPixel + 0x1f) & 0xffffffe0;

     pbt->iSamplesLine = iBitsLine / pbt->iBitsChannel;

     // initialise the remainder of the bmptag for client
     pbt->iSampleBase = 0;
     pbt->iSamplePosition = 0;
     pbt->iSampleAppend = pbt->pbih->biHeight * pbt->iSamplesLine;
     pbt->iSampleLimit = pbt->iSampleAppend;

     //pbt->rPixelWindow.left = 0;
     //pbt->rPixelWindow.right = pbt->pbih->biWidth;
     //pbt->rPixelWindow.bottom = 0;
     //pbt->rPixelWindow.top = pbt->pbih->biHeight;

     //pbt->rPixelSurface.left = 0;
     //pbt->rPixelSurface.right = iBitsLine / iBitsPixel;
     //pbt->rPixelSurface.bottom = 0;
     //pbt->rPixelSurface.top = pbt->pbih->biHeight;

     // say we met with success
     return TRUE;
     }

// ----------------------------------------------------------------------------

int fiBmpWriteOut(BMPTAG *pbt, char *pszFileName)
     {
     int i, j, k;
     int iBitsPixel, iBitsLine;
     int iBytesLine, iBytes;

     // update bmp file and info header lengths from length generated
     iBitsPixel = pbt->iChannels * pbt->iBitsChannel;
     iBitsLine = (pbt->pbih->biWidth * iBitsPixel + 0x1f) & 0xffffffe0;

     iBytesLine = iBitsLine / 8;
     iBytes = pbt->pbih->biHeight * iBytesLine;

     pbt->pbfh->bfOffBits = (char *) (pbt->pucSample) - pbt->ft.pcBase;
     pbt->pbfh->bfSize = pbt->pbfh->bfOffBits + iBytes;

     // refresh palette within file data if any palette is required
     if (pbt->iChannels == 1)
          {
          j = 0;
          k = 1 << pbt->iBitsChannel;
          for (i = 0; i < k; i++)
               {
               ((int *) (pbt->prgbq))[i] = j;
               j += 0x00010101;
               }
          }

     // update bmp info header information for client selected format
     pbt->pbih->biPlanes = 1; // the red baron
     pbt->pbih->biBitCount = pbt->iChannels * pbt->iBitsChannel;

     // update length in output filetag then write out
     pbt->ft.pcAppend = (char *) (pbt->pucSample) + iBytes;
     return fiFileWriteOut(&pbt->ft, pszFileName);
     }

// ----------------------------------------------------------------------------

void BmpFree(BMPTAG *pbt)
     {
     FileFree(&pbt->ft);
     }

// ----------------------------------------------------------------------------

int fiBmpCopy(BMPTAG *pbt, BMPTAG *pbtIn)
     {
     // copy the tag item, which will then be modified
     memcpy(pbt, pbtIn, sizeof(BMPTAG));

     // redirect the new tag item to a new malloc'd buffer
     pbt->ft.pcBase = (char *) malloc(pbt->ft.pcLimit - pbt->ft.pcBase);
     if (pbt->ft.pcBase == NULL)
          {
          printf("fiBmpCopy: "
                 "Could not allocate copy buffer of %08x bytes\n",
                 pbt->ft.pcLimit - pbt->ft.pcBase);
          return FALSE;
          }

     // copy the original sample data into the new malloc'd buffer
     memcpy(pbt->ft.pcBase, pbtIn->ft.pcBase,
                            pbtIn->ft.pcLimit - pbtIn->ft.pcBase);

     // fill bmptag fields which refer to the allocated memory block
     pbt->pbfh = (BITMAPFILEHEADER *) (pbt->ft.pcBase);
     pbt->pbih = (BITMAPINFOHEADER *) (pbt->ft.pcBase +
                                       sizeof(BITMAPFILEHEADER));
     pbt->prgbq = (RGBQUAD *) (pbt->ft.pcBase +
                               sizeof(BITMAPFILEHEADER) +
                               sizeof(BITMAPINFOHEADER));
     pbt->pucSample = (unsigned char *) (pbt->ft.pcBase +
                                        sizeof(BITMAPFILEHEADER) +
                                        sizeof(BITMAPINFOHEADER));
     if (pbt->iChannels == 1)
          {
          pbt->pucSample += sizeof(RGBQUAD) << pbt->iBitsChannel;
          }

     // sneakily nudge the remaining fields of the new tag item
     pbt->ft.pcLimit += pbt->ft.pcBase - pbtIn->ft.pcBase;
     pbt->ft.pcPosition += pbt->ft.pcBase - pbtIn->ft.pcBase;
     pbt->ft.pcAppend += pbt->ft.pcBase - pbtIn->ft.pcBase;

     // say we met with success
     return TRUE;
     }

// ----------------------------------------------------------------------------

int fiLinearExpand2D(BMPTAG *pbtOut, BMPTAG *pbtIn,
                     int cxFactor, int cyFactor)
     {
     int i, j, k, l, m, x, y, t;
     int cxIn, cyIn, cxOut, cyOut;
     int cxCentre, cyCentre;
     int cxLeftFraction, cxRightFraction;
     int cyTopFraction, cyBottomFraction;
     int iLine, iPixel, iSample;
     int *pi, *piThis, *piPrev, *piTemp;
     int iThis[4], iPrev[4], iNormal;

     cxIn = pbtIn->pbih->biWidth;
     cyIn = pbtIn->pbih->biHeight;

     cxOut = cxIn * cxFactor;
     cyOut = cyIn * cyFactor;

     if (fiBmpAllocate(pbtOut, pbtIn->pbih, cxOut, cyOut) == FALSE)
          {
          printf("fiLinearExpand2D: "
                 "Failed to allocate output buffer of %08x pixels\n",
                 cxOut * cyOut);
          return FALSE;
          }

     pi = (int *) malloc(cxOut * pbtIn->iChannels * 2 * sizeof(int));
     if (pi == NULL)
          {
          printf("fiLinearExpand2D: "
                 "Failed to allocate temporary buffer of %08x samples\n",
                 cxOut * pbtIn->iChannels * 2);
          BmpFree(pbtOut);
          return FALSE;
          }

     piThis = pi;
     piPrev = pi + cxOut * pbtIn->iChannels;

     cxCentre = cxFactor / 2;
     cyCentre = cyFactor / 2;

     cyBottomFraction = cyCentre;
     cyTopFraction = cyFactor;
     memset(piThis, 0, cxOut * pbtIn->iChannels * sizeof(int));

     iNormal = cxFactor * cyFactor;

     i = 0;
     iLine = 0;
     for (y = 0; y <= cyIn; y++)
          {
          iPixel = iLine;
          iLine += pbtIn->iSamplesLine;

          piTemp = piPrev;
          piPrev = piThis;
          piThis = piTemp;

          if (y < cyIn)
               {
               cxLeftFraction = cxCentre;
               cxRightFraction = cxFactor;
               memset(iThis, 0, pbtIn->iChannels * sizeof(int));

               t = 0;
               iSample = iPixel;
               for (x = 0; x <= cxIn; x++)
                    {
                    memcpy(iPrev, iThis, pbtIn->iChannels * sizeof(int));

                    if (x < cxIn)
                         {
                         for (m = 0; m < pbtIn->iChannels; m++)
                              {
                              iThis[m] = pbtIn->pucSample[iSample++];
                              }
                         }
                    else
                         {
                         cxRightFraction = cxCentre;
                         memset(iThis, 0, pbtIn->iChannels * sizeof(int));
                         }

                    for (k = cxLeftFraction; k < cxRightFraction; k++)
                         {
                         for (m = 0; m < pbtIn->iChannels; m++)
                              {
                              piThis[t++] =
                              k * iThis[m] + (cxFactor - k) * iPrev[m];
                              }
                         }

                    cxLeftFraction = 0;
                    }
               }
          else
               {
               cyTopFraction = cyCentre;
               memset(piThis, 0, cxOut * pbtIn->iChannels * sizeof(int));
               }

          for (l = cyBottomFraction; l < cyTopFraction; l++)
               {
               j = i;
               i += pbtOut->iSamplesLine;

               t = 0;
               for (x = 0; x < cxOut; x++)
                    {

#if 1
                    if (j >= pbtOut->iSampleLimit)
                         {
                         printf("fiLinearExpand2D: "
                         "Overflowed output buffer at sample %08x\n", j);
                         free(pi);
                         BmpFree(pbtOut);
                         return FALSE; // should never happen
                         }
#endif

                    for (m = 0; m < pbtIn->iChannels; m++)
                         {
                         pbtOut->pucSample[j++] =
                         (l * piThis[t] + (cyFactor - l) * piPrev[t]) /
                         iNormal;

                         t++;
                         }

                    }
               }

          cyBottomFraction = 0;
          }

     free(pi);

#if 1
     if (i < pbtOut->iSampleLimit)
          {
          printf("fiLinearExpand2D: "
                 "Partially filled output buffer to sample %08x\n", i);
          BmpFree(pbtOut);
          return FALSE; // should never happen
          }
#endif

     pbtOut->iSampleAppend = i;
     return TRUE;
     }

// ----------------------------------------------------------------------------

int fiLinearShrink2D(BMPTAG *pbtOut, BMPTAG *pbtIn,
                     int cxFactor, int cyFactor)
     {
     int i, j, k, l, m, x, y, t, u;
     int cxIn, cyIn, cxOut, cyOut;
     int iLine, iPixel, iSample;
     int *pi, iNormal;

     cxIn = pbtIn->pbih->biWidth;
     cyIn = pbtIn->pbih->biHeight;

     cxOut = cxIn / cxFactor;
     cyOut = cyIn / cyFactor;

     if (fiBmpAllocate(pbtOut, pbtIn->pbih, cxOut, cyOut) == FALSE)
          {
          printf("fiLinearShrink2D: "
                 "Failed to allocate output buffer of %08x samples\n",
                 cxOut * cyOut * pbtIn->iChannels);
          return FALSE;
          }

     pi = (int *) malloc(cxOut * pbtIn->iChannels * sizeof(int));
     if (pi == NULL)
          {
          printf("fiLinearShrink2D: "
                 "Failed to allocate temporary buffer of %08x pixels\n",
                 cxOut);
          BmpFree(pbtOut);
          return FALSE;
          }

     iNormal = cxFactor * cyFactor;

     i = 0;
     iLine = 0;
     for (y = 0; y < cyOut; y++)
          {
          j = i;
          i += pbtOut->iSamplesLine;

          memset(pi, 0, cxOut * pbtIn->iChannels * sizeof(int));

          for (l = 0; l < cyFactor; l++)
               {
               iPixel = iLine;
               iLine += pbtIn->iSamplesLine;

               t = 0;
               iSample = iPixel;
               for (x = 0; x < cxOut; x++)
                    {
                    for (k = 0; k < cxFactor; k++)
                         {
                         u = t;
                         for (m = 0; m < pbtIn->iChannels; m++)
                              {
                              pi[u++] += pbtIn->pucSample[iSample++];
                              }
                         }

                    t += pbtIn->iChannels;
                    }
               }

          t = 0;
          for (x = 0; x < cxOut; x++)
               {
#if 1
               if (j >= pbtOut->iSampleLimit)
                    {
                    printf("fiLinearShrink2D: "
                    "Overflowed output buffer at sample %08x\n", j);
                    free(pi);
                    BmpFree(pbtOut);
                    return FALSE; // should never happen
                    }
#endif

               for (m = 0; m < pbtIn->iChannels; m++)
                    {
                    pbtOut->pucSample[j++] = pi[t++] / iNormal;
                    }
               }
          }

     free(pi);

#if 1
     if (i < pbtOut->iSampleLimit)
          {
          printf("fiLinearShrink2D: "
                 "Partially filled output buffer to sample %08x\n", i);
          BmpFree(pbtOut);
          return FALSE; // should never happen
          }
#endif

     pbtOut->iSampleAppend = i;
     return TRUE;
     }

// ----------------------------------------------------------------------------

#if 1
int fiSincInterpolate2D(BMPTAG *pbtOut, BMPTAG *pbtIn,
                        int cxInterpolate, int cyInterpolate)
     {
     BMPTAG btFilter;
     int i, j, k, l, m, x, y, t, u;
     int iThis, iShift, iNormal;
     int cxSize, cySize;
     int cxPixelsIn, cyPixelsIn;
     int cxPixelsOut, cyPixelsOut;
     int cxFraction, cyFraction;
     int cxLeft, cyBottom;
     int cxCentre, cyCentre;
     int cxLeftClipped, cxRightClipped;
     int cxLeftFraction, cxRightFraction;
     int cyBottomClipped, cyTopClipped;
     int cyBottomFraction, cyTopFraction;
     int cxLeftSample, cxRightSample;
     int cxCoefficient, cyCoefficient;
     int cxCoefficientSave, cyCoefficientSave;
     int cxCoefficientStep, cyCoefficientStep;
     int fiCoefficientStep;

     cxSize = cxInterpolate * FILTER_OVERLAP;
     cySize = cyInterpolate * FILTER_OVERLAP;

     cxPixelsIn = pbtIn->pbih->biWidth;
     cyPixelsIn = pbtIn->pbih->biHeight;

     cxPixelsOut = cxPixelsIn * cxInterpolate;
     cyPixelsOut = cyPixelsIn * cyInterpolate;

     if (fiBmpAllocate(pbtOut, pbtIn->pbih,
                       cxPixelsOut, cyPixelsOut) == FALSE)
          {
          printf("fiSincInterpolate2D: "
                 "Failed to allocate output buffer of %08x samples\n",
                 cxPixelsOut * cyPixelsOut * pbtIn->iChannels);
          return FALSE;
          }

     if (fiMakeSincFilter2D(&btFilter, pbtIn, cxSize, cySize,
                          FILTER_CUTOFF, FILTER_CUTOFF) == FALSE)
          {
          BmpFree(pbtOut);
          return FALSE;
          }

 fiBmpWriteOut(&btFilter, "filtint.bmp");
 printf("Created filtint.bmp, 0x%08x samples\n", btFilter.iSampleAppend);

#if 0
     iShift = 0;
     for (x = cxInterpolate; x < cxSize; x <<= 1)
          {
          iShift++;
          }
     for (y = cyInterpolate; y < cySize; y <<= 1)
          {
          iShift++;
          }
 printf("shift %d\n", iShift);
#endif

     i = 0;
     iNormal = 0;
     for (y = 0; y < cySize; y++)
          {
          j = i;
          i += btFilter.iSamplesLine;

          for (x = 0; x < cxSize; x++)
               {
               iNormal += (int) (btFilter.pucSample[j]) - 0x30;
               j += btFilter.iChannels; // only 1st channel !
               }
          }
#if 0
 printf("normal %d\n", iNormal);
     iNormal >>= iShift;
#endif
     iNormal = (iNormal << 8) / (cxInterpolate * cyInterpolate);

     if (iNormal == 0)
          {
          printf("fiSincInterpolate2D: "
                 "Inadequate precision to normalise filter response\n");
          BmpFree(&btFilter);
          BmpFree(pbtOut);
          return FALSE;
          }

     cxCentre = FILTER_OVERLAP / 2;
     cyCentre = FILTER_OVERLAP / 2;

     cxCoefficientStep = cxInterpolate * btFilter.iChannels;
     cyCoefficientStep = cyInterpolate * btFilter.iSamplesLine;

     fiCoefficientStep = (btFilter.iChannels >= pbtIn->iChannels);

     cyBottomFraction = cyInterpolate / 2;
     cyTopFraction = cyInterpolate;

     i = 0;
     for (y = 0; y <= cyPixelsIn; y++)
          {
          if (y >= cyPixelsIn)
               {
               cyTopFraction = cyInterpolate / 2;
               }

          cyBottom = y - cyCentre;

          cyBottomClipped = max(cyBottom, 0) * pbtIn->iSamplesLine;
          cyTopClipped = min(cyBottom + FILTER_OVERLAP, cyPixelsIn) *
                         pbtIn->iSamplesLine;

          // the (cyInterpolate - 1) term below is completed when we also
          // subtract iFraction, making (cyInterpolate - cyFraction - 1).
          // same algorithm applies to cxCoefficientSave inside the loop.

          // this reversal is necessary because the window moves in the
          // opposite direction, from the viewpoint of the coefficients,
          // than our normal viewpoint which is relative to input data.

          cyCoefficientSave = (cyInterpolate - 1) +
                              max(0, -cyBottom) * cyInterpolate;

          for (cyFraction = cyBottomFraction; cyFraction < cyTopFraction;
                                              cyFraction++)
               {
               j = i;
               i += pbtOut->iSamplesLine;

               // fraction here ensures that we move through the
               // available coefficients for smooth interpolation,
               // compensation for clipping is done using quantised
               // values especially to avoid disturbing this effect
               cyCoefficient = (cyCoefficientSave - cyFraction) *
                               btFilter.iSamplesLine;

               cxLeftFraction = cxInterpolate / 2;
               cxRightFraction = cxInterpolate;

               for (x = 0; x <= cxPixelsIn; x++)
                    {
                    if (x >= cxPixelsIn)
                         {
                         cxRightFraction = cxInterpolate / 2;
                         }

                    cxLeft = x - cxCentre;

                    cxLeftClipped = max(cxLeft, 0) * pbtIn->iChannels;
                    cxRightClipped = min(cxLeft + FILTER_OVERLAP, cxPixelsIn) *
                                     pbtIn->iChannels;

                    cxCoefficientSave = (cxInterpolate - 1) +
                                        max(0, -cxLeft) * cxInterpolate;

                    for (cxFraction = cxLeftFraction;
                         cxFraction < cxRightFraction; cxFraction++)
                         {
#if 1
                         if (j >= pbtOut->iSampleLimit)
                              {
                              printf("fiSincInterpolate2D: "
                              "Overflowed output buffer at sample %08x\n", j);
                              BmpFree(&btFilter);
                              BmpFree(pbtOut);
                              return FALSE; // should never happen
                              }
#endif

                         cxLeftSample = cxLeftClipped;
                         cxRightSample = cxRightClipped;

                         // fraction here ensures that we move through the
                         // available coefficients for smooth interpolation,
                         // compensation for clipping is done using quantised
                         // values especially to avoid disturbing this effect
                         cxCoefficient = (cxCoefficientSave - cxFraction) *
                                         btFilter.iChannels;

                         for (m = 0; m < pbtIn->iChannels; m++)
                              {
                              iThis = 0;

                              t = cyCoefficient + cxCoefficient;

                              for (k = cyBottomClipped; k < cyTopClipped;
                                                      k += pbtIn->iSamplesLine)
                                   {
                                   u = t;
                                   t += cyCoefficientStep;

                                   for (l = cxLeftSample; l < cxRightSample;
                                                         l += pbtIn->iChannels)
                                        {
                                        iThis += (pbtIn->pucSample[k + l] *
                                                  ((int)
                                                   (btFilter.pucSample[u]) -
                                                   0x30)); //>> iShift;
                                        u += cxCoefficientStep;
                                        }
                                   }

                              cxLeftSample++;
                              cxRightSample++;
                              cxCoefficient += fiCoefficientStep;

                              iThis = (iThis << 8) / iNormal;
                              if (iThis > 0xff)
                                   {
                                   iThis = 0xff;
                                   }
                              if (iThis < 0)
                                   {
                                   iThis = 0;
                                   }
                              pbtOut->pucSample[j++] = iThis;
                              }
                         }

                    cxLeftFraction = 0;
                    }
               }

          cyBottomFraction = 0;
          }

     BmpFree(&btFilter);

#if 0 //1
     if (j < pbtOut->iSampleLimit)
          {
          printf("fiSincInterpolate2D: "
                 "Partially filled output buffer to sample %08x\n", j);
          BmpFree(pbtOut);
          return FALSE; // should never happen
          }
#endif

     pbtOut->iSampleAppend = j;
     return TRUE;
     }
#endif

// ----------------------------------------------------------------------------

#if 0
int fiSincDecimate2D(BMPTAG *pbtOut, BMPTAG *pbtIn,
                     int cxDecimate, int cyDecimate)
     {
     BMPTAG btFilter;
     int i, j, k, l, m, x, y, t, u;
     int cxSize, cySize, iThis;
     int iShift, iNormal;
     int fiCoefficientStep;
     int cxPixelsIn, cyPixelsIn;
     int cxPixelsOut, cyPixelsOut;
     int iCoefficient, iCentre, iLeft;
     int iLeftClipped, iRightClipped;

     cxSize = cxDecimate * FILTER_OVERLAP;
     cySize = cyDecimate * FILTER_OVERLAP;

     cxPixelsIn = pbtIn->pbih->biWidth;
     cyPixelsIn = pbtIn->pbih->biHeight;

     cxPixelsOut = cxPixelsIn / cxDecimate;
     cyPixelsOut = cyPixelsIn / cyDecimate;

     if (fiBmpAllocate(pbtOut, pbtIn->pbih,
                       cxPixelsOut, cyPixelsOut) == FALSE)
          {
          printf("fiSincDecimate2D: "
                 "Failed to allocate output buffer of %08x samples\n",
                 cxPixelsOut * cyPixelsOut * pbtIn->iChannels);
          return FALSE;
          }

     if (fiMakeSincFilter2D(&btFilter, pbtIn, cxSize, cySize,
                          FILTER_CUTOFF, FILTER_CUTOFF) == FALSE)
          {
          BmpFree(pbtOut);
          return FALSE;
          }

 fiBmpWriteOut(&btFilter, "filtdec.bmp");
 printf("Created filtdec.bmp, 0x%08x samples\n", btFilter.iSampleAppend);

     iShift = 0;
     for (i = 1; i < iSize; i <<= 1)
          {
          iShift++;
          }

     iNormal = 0;
     for (i = 0; i < iSize; i++)
          {
          iNormal += btFilter.pucSample[i * btFilter.iChannels];
          }
     iNormal >>= iShift;

     iCentre = iSize / 2;
     fiCoefficientStep = (btFilter.iChannels >= pbtIn->iChannels);

     j = 0;
     for (i = 0; i < iPixelsIn; i += iDecimate)
          {
#if 1
          if (j >= pbtOut->iSampleLimit)
               {
               printf("fiSincDecimate2D: "
                      "Overflowed output buffer at sample %08x\n", j);
               BmpFree(&btFilter);
               BmpFree(pbtOut);
               return FALSE; // should never happen
               }
#endif

          iLeft = i - iCentre;

          iLeftClipped = max(iLeft, 0) * pbtIn->iChannels;
          iRightClipped = min(iLeft + iSize, pbtIn->iSampleAppend) *
                          pbtIn->iChannels;

          iCoefficient = max(0, -iLeft) * btFilter.iChannels;

          for (m = 0; m < pbtIn->iChannels; m++)
               {
               iThis = 0;

               l = iCoefficient;
               iCoefficient += fiCoefficientStep;

               for (k = iLeftClipped++; k < iRightClipped;
                                        k += pbtIn->iChannels)
                    {
                    iThis += (pbtIn->pucSample[k] * btFilter.pucSample[l]) >>
                             iShift;
                    l += btFilter.iChannels;
                    }

               iRightClipped++;

               iThis /= iNormal;
               if (iThis > 0xff)
                    {
                    iThis = 0xff;
                    }
               if (iThis < 0)
                    {
                    iThis = 0;
                    }

               pbtOut->pucSample[j++] = iThis;
               }
          }

     BmpFree(&btFilter);

#if 1
     if (j < pbtOut->iSampleLimit)
          {
          printf("fiSincDecimate2D: "
                 "Partially filled output buffer to sample %08x\n", j);
          BmpFree(pbtOut);
          return FALSE; // should never happen
          }
#endif

     pbtOut->iSampleAppend = j;
     return TRUE;
     }
#endif

// ----------------------------------------------------------------------------

#if 0
int fiSincResample2D(BMPTAG *pbtOut, BMPTAG *pbtIn,
                     int cxInterpolate, int cyInterpolate,
                     int cxDecimate, int cyDecimate)
     {
     BMPTAG btFilter;
     int i, j, k, l, m, x, y, t, u;
     int cxSize, cySize;
     int cxSizeQuantised, cySizeQuantised;
     int iShift, iNormal, iThis;
     int iCoefficientStep, fiCoefficientStep;
     int cxPixelsIn, cyPixelsIn;
     int cxPixelsQuantised, cyPixelsQuantised;
     int iCoefficient;
     int iIntermediate, iInteger, iFraction;
     int iLeftQuantised, iCentreQuantised;
     int iLeftClipped, iRightClipped;

     // I think it is best to quantise the input data window size once,
     // at the beginning, so all convolutions are done with the same width,
     // avoiding normalisation issues.  Therefore one coefficient will
     // always skipped, on either side of the coefficient bank depending
     // on the interpolation status, but it will be of low amplitude.
     //
     // window_size_quantised = window_size / output_rate
     //                       = max(t, t * input_rate / output_rate)

     cxSizeQuantised = max(FILTER_OVERLAP,
                           (FILTER_OVERLAP * cxDecimate) / cxInterpolate);
     cySizeQuantised = max(FILTER_OVERLAP,
                           (FILTER_OVERLAP * cyDecimate) / cyInterpolate);

     // So how many possible interpolation states are there?  The
     // interpolation has to be reset each time the base coefficient
     // pointer overflows to a value => the input data rate unit
     // (output_rate intermediate units).  Thus, the base coefficient
     // pointer is the state counter for the interpolation and it
     // can have values 0 thru output_rate-1.
     //
     // Having calculated window_size_quantised we can then multiply
     // by the number of possible interpolation states, thus:
     //
     // window_size_coefficient = window_size_quantised * output_rate
     //                         = max(t * output_rate, t * input_rate)
     //
     // Note:  The second derivation undoes the beneficial effect of
     // the quantisation, meaning the window is now slightly larger
     // than necessary, but this is necessary, because otherwise our
     // use of s * t to set the cutoff would introduce a freq. error.

     cxSize = FILTER_OVERLAP * max(cxInterpolate, cxDecimate);
     cySize = FILTER_OVERLAP * max(cyInterpolate, cyDecimate);

     cxPixelsIn = pbtIn->pbih->biWidth;
     cyPixelsIn = pbtIn->pbih->biHeight;

     cxPixelsQuantised = (cxPixelsIn * cxInterpolate) / cxDecimate;
     cyPixelsQuantised = (cyPixelsIn * cyInterpolate) / cyDecimate;

     if (fiBmpAllocate(pbtOut, pbtIn->pbih,
                       cxPixelsQuantised, cyPixelsQuantised) == FALSE)
          {
          printf("fiSincResample2D: "
                 "Failed to allocate output buffer of %08x samples\n",
                 cxPixelsQuantised * cyPixelsQuantised * pbtIn->iChannels);
          return FALSE;
          }

     if (fiMakeSincFilter2D(&btFilter, pbtIn, cxSize, cySize,
                          FILTER_CUTOFF, FILTER_CUTOFF) == FALSE)
          {
          BmpFree(pbtOut);
          return FALSE;
          }

 if (iInterpolate > iDecimate)
  {
  fiBmpWriteOut(&btFilter, "filtint.bmp");
  printf("Created filtint.bmp, 0x%08x samples\n", btFilter.iSampleAppend);
  }
 else

  {
  fiBmpWriteOut(&btFilter, "filtdec.bmp");
  printf("Created filtdec.bmp, 0x%08x samples\n", btFilter.iSampleAppend);
  }

     iShift = 0;
     for (i = 1; i < iSizeQuantised; i <<= 1)
          {
          iShift++;
          }

     iNormal = 0;
     for (i = 0; i < iSizeQuantised; i++)
          {
          iNormal += btFilter.pucSample[i * iInterpolate * btFilter.iChannels];
          }
     iNormal >>= iShift;

     // centre should also be compensated for the rounding down of
     // (iPixelsIn * iInterpolate) / iDecimate, so must convert back
     iCentreQuantised = iSizeQuantised / 2;

     iCoefficientStep = iInterpolate * btFilter.iChannels;
     fiCoefficientStep = (btFilter.iChannels >= pbtIn->iChannels);

     j = 0;
     iIntermediate = 0;
     for (i = 0; i < iPixelsQuantised; i++)
          {
#if 1
          if (j >= pbtOut->iSampleLimit)
               {
               printf("fiSincResample2D: "
                      "Overflowed output buffer at sample %08x\n", j);
               BmpFree(&btFilter);
               BmpFree(pbtOut);
               return FALSE; // should never happen
               }
#endif

          // integer and fraction variables are used for clarity,
          // and could be optimised with modulo arithmetic, but
          // there is little point optimising this part of the
          // loop since we are doing vector multiplication later
          iInteger = iIntermediate / iInterpolate;
          iFraction = iIntermediate % iInterpolate;
          iIntermediate += iDecimate;

          // left side of window is recalculated each time through,
          // to avoid issues with the rounding of negative numbers
          iLeftQuantised = iInteger - iCentreQuantised;

          // input data window size is constant, so the quantised
          // value calculated in the beginning is used for clipping
          iLeftClipped = max(iLeftQuantised, 0) * pbtIn->iChannels;
          iRightClipped = min(iLeftQuantised + iSizeQuantised, iPixelsIn) *
                          pbtIn->iChannels;

          // fraction here ensures that we move through the
          // available coefficients for smooth interpolation, the
          // compensation for clipping is done using quantised
          // values especially to avoid disturbing this effect
          iCoefficient = ((iInterpolate - iFraction - 1) +
                         max(0, -iLeftQuantised) * iInterpolate) *
                         btFilter.iChannels;

          for (m = 0; m < pbtIn->iChannels; m++)
               {
               iThis = 0;

               l = iCoefficient;
               iCoefficient += fiCoefficientStep;

               for (k = iLeftClipped++; k < iRightClipped;
                                        k += pbtIn->iChannels)
                    {
                    iThis += (pbtIn->pucSample[k] * btFilter.pucSample[l]) >>
                             iShift;
                    l += iCoefficientStep;
                    }

               iRightClipped++;

               iThis /= iNormal;
               if (iThis > 0xff)
                    {
                    iThis = 0xff;
                    }
               if (iThis < 0)
                    {
                    iThis = 0;
                    }

               pbtOut->pucSample[j++] = iThis;
               }
          }

     BmpFree(&btFilter);

#if 1
     if (j < pbtOut->iSampleLimit)
          {
          printf("fiSincResample2D: "
                 "Partially filled output buffer to sample %08x\n", j);
          BmpFree(pbtOut);
          return FALSE; // should never happen
          }
#endif

     pbtOut->iSampleAppend = j;
     return TRUE;
     }
#endif

// ----------------------------------------------------------------------------

int fiMakeSincFilter2D(BMPTAG *pbtOut, BMPTAG *pbtIn,
                       int cxSize, int cySize, int cxCutoff, int cyCutoff)
     {
     BITMAPINFOHEADER bih;
     int i, j, m, x, y, iThis;
     float fxScale, fyScale;
     float fxCentre, fyCentre;
     float fxWindow, fyWindow;
     float fxThis, fyThis;

     // the sinc filter will have the same file format as the input data,
     // except that channels is set to 1 because the same filter will be
     // used on all colour channels, and bits per sample is set to 8
     // to reduce the number of combinations to be handled when filtering.
     //
     // note:  the sinc filter will adopt the same sample rate as the input,
     // which is not exactly correct if overall interpolation will be used.

     memcpy(&bih, pbtIn->pbih, sizeof(BITMAPINFOHEADER));

     bih.biBitCount = 8;

     if (fiBmpAllocate(pbtOut, &bih, cxSize, cySize) == FALSE)
          {
          printf("fiMakeSincFilter2D: "
                 "Failed to allocate output buffer of %08x samples\n",
                 cxSize, cySize);
          return FALSE;
          }

     fxCentre = (cxSize - 1) / 2.0;
     fyCentre = (cySize - 1) / 2.0;

     fxScale = (6.283185307 * cxCutoff) / cxSize;
     fyScale = (6.283185307 * cyCutoff) / cySize;

     i = 0;
     for (y = 0; y < cySize; y++)
          {
          j = i;
          i += pbtOut->iSamplesLine;

          fyWindow = 1 - min(y + 1, cySize - y) / fyCentre;
          fyWindow = 1 - fyWindow * fyWindow;

          fyThis = fyScale * (y - fyCentre);
          if (fyThis)
               {
               fyThis = sin(fyThis) / fyThis;
               }
          else
               {
               fyThis = 1;
               }

          for (x = 0; x < cxSize; x++)
               {
               fxWindow = 1 - min(x + 1, cxSize - x) / fxCentre;
               fxWindow = 1 - fxWindow * fxWindow;

               fxThis = fxScale * (x - fxCentre);
               if (fxThis)
                    {
                    fxThis = sin(fxThis) / fxThis;
                    }
               else
                    {
                    fxThis = 1;
                    }

               // the filter gain is fixed here, but will later be adjusted by
               // normalisation (only the caller knows the interpolation in use)
               iThis = fxThis * fyThis * fxWindow * fyWindow * 0xcf + 0x30;

#if 1
               if (j >= pbtOut->iSampleLimit)
                    {
                    printf("fiMakeSincFilter2D: "
                           "Overflowed output buffer at sample %08x\n", j);
                    BmpFree(pbtOut);
                    return FALSE; // should never happen
                    }
#endif

               //for (m = 0; m < pbtOut->iChannels; m++) // always 1 for now
                    {
                    pbtOut->pucSample[j++] = iThis;
                    }
               }
          }

#if 1
     if (j < pbtOut->iSampleLimit)
          {
          printf("fiMakeSincFilter2D: "
                 "Partially filled output buffer to sample %08x\n", j);
          BmpFree(pbtOut);
          return FALSE; // should never happen
          }
#endif

     pbtOut->iSampleAppend = j;
     return TRUE;
     }

// ----------------------------------------------------------------------------

