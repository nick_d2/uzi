// hyfs.h

#define _INC_HYFS

// ----------------------------------------------------------------------------

#define IDS_ERROR_FS_OPEN	12000
#define IDS_ERROR_FS_CREATE	12001
#define IDS_ERROR_FS_CLOSE	12002
#define IDS_ERROR_FS_READ	12003
#define IDS_ERROR_FS_WRITE	12004

// ----------------------------------------------------------------------------

typedef struct
     {
     int iHandle;
     char *pszName;

     int iMode;
     int iCount;

     int iError;
     char *pszError;
     } FSHANDLE;

// ----------------------------------------------------------------------------

void FSEntrySetup(void);
void FSExitCleanup(void);

int fiFSOpen(FSHANDLE *pfsh, char *pszName);
int fiFSCreate(FSHANDLE *pfsh, char *pszName);
int fiFSClose(FSHANDLE *pfsh);

int iFSRead(FSHANDLE *pfsh, char *pcOut, int iLimit);
int iFSWrite(FSHANDLE *pfsh, char *pcIn, int iLimit);

// ----------------------------------------------------------------------------

inline void FSOpen(FSHANDLE *pfsh, char *pszName)
     {
     if (fiFSOpen(pfsh, pszName) == 0)
          {
          printf("FSOpen: "
                 "Could not open file %s for reading, exiting\n", pszName);
          exit(1);
          }

     printf("FSOpen: "
            "Handle 0x%08x file %s opened, read mode\n", pfsh, pszName);
     }

inline void FSCreate(FSHANDLE *pfsh, char *pszName)
     {
     if (fiFSCreate(pfsh, pszName) == 0)
          {
          printf("FSCreate: "
                 "Could not create or truncate file %s, exiting\n", pszName);
          exit(1);
          }

     printf("FSCreate: "
            "Handle 0x%08x file %s opened, write mode\n", pfsh, pszName);
     }

inline void FSClose(FSHANDLE *pfsh)
     {
     if (fiFSClose(pfsh) == 0)
          {
          printf("FSClose: "
                 "Could not close handle 0x%08x, exiting\n", pfsh);
          exit(1);
          }

     printf("FSClose: "
            "Handle 0x%08x file %s closed, %s mode\n",
            pfsh, pfsh->pszName, pfsh->iMode ? "write" : "read");

     printf("Loaded file %s, size %d bytes\n",
            pfsh->pszName, pfsh->iCount);
     }

inline void FSRead(FSHANDLE *pfsh, char *pcOut, int iLimit)
     {
     int iCount;

     iCount = iFSRead(pfsh, pcOut, iLimit);
     if (iCount != iLimit)
          {
          printf("FSRead: "
                 "End of file, handle 0x%08x, request %d, return %d\n",
                 pfsh, iLimit, iCount);
          exit(1);
          }

     printf("FSRead: "
            "Handle 0x%08x file %s read, request %d, return %d\n",
            pfsh, pfsh->pszName, iLimit, iCount);
     }

inline void FSWrite(FSHANDLE *pfsh, char *pcIn, int iLimit)
     {
     int iCount;

     iCount = iFSWrite(pfsh, pcIn, iLimit);
     if (iCount != iLimit)
          {
          printf("FSWrite: "
                 "Disk full, handle 0x%08x, request %d, return %d\n",
                 pfsh, iLimit, iCount);
          exit(1);
          }

     printf("FSWrite: "
            "Handle 0x%08x file %s written, request %d, return %d\n",
            pfsh, pfsh->pszName, iLimit, iCount);
     }

// ----------------------------------------------------------------------------

