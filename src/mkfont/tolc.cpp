// tolc.cpp by Nick for Hytech Resource Manager system

// ----------------------------------------------------------------------------

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>
#pragma hdrstop

#ifndef DEBUG
#define DEBUG 0
#endif

#include "hymem.h"
#include "hyfs.h"
#include "hyfile.h"
#include "hylist.h"
#include "hycache.h"
#include "hystring.h"
#include "tolc.h"

//#include "reslib.h"

// ----------------------------------------------------------------------------
// local preprocessor definitions

#define LINE_MAX	0x200
#define PATH_MAX	0x200

#define OUTPUT_BUFFER	0x100000 // to create output files of up to 1 Megabyte
#define CACHE_MAX	0x200 //0 // the maximum number of files to operate on

#define ARGUMENT_MAX	0x100 // the maximum number of arguments per command
#define MESS_MAX	0x2000 // the maximum number of msgs in the output file
#define MESS_SIZE	0x2000 // the maximum size of a "po" formatted message

#define SYM_SIZE	0x1f // the assumed maximum symbol length for linking

// ----------------------------------------------------------------------------
// local function prototypes

#ifdef STANDALONE
int main(int argc, char **argv);
#endif

int iTolc(LISTTAG *pltCache, int argc, char **argv);
static int fiProcessTolc(FILETAG *pftWork);
static int iStringTokeniseBasic(int iIndex, char *psz, int iLimit,
                    char **ppsz, int *piCount);

// ----------------------------------------------------------------------------
// global variables

#if 0
LISTTAG ltMess; // list of type MESSTAG
#endif
static FILETAG *pftWork; // to modify 'C' source files in place

// ----------------------------------------------------------------------------

#ifdef STANDALONE
int main(int argc, char **argv)
	{
	int i;
        LISTTAG ltCache;

	// set up the cache to track intermediate memory files
        CacheEntrySetup(&ltCache, CACHE_MAX);

#if 0
	// allocate message list to collect information about messages
	ListAllocate(&ltMess, MESS_MAX, sizeof(MESSTAG));
#endif

	// run the real program, passing in the cache descriptor
	i = iTolc(&ltCache, argc, argv);

#if 0
	// junk the internal message list buffer
	ListFree(&ltMess);
#endif

	// it just goes on and on my friend
	CacheExitFlush(&ltCache, 3); // force writing of intermediate files
	CacheExitCleanup(&ltCache); // deallocate memory, though it's redundant

	return i;
	}
#endif

// ----------------------------------------------------------------------------

#if 0
#if 0
int fiUnescAll(void)
	{
	MESSTAG *pmt;
	FILETAG *pft;

	// set up one item of lookahead in message table
	ltMess.iPosition = 0;
	pmt = (MESSTAG *) pcListCanGet(&ltMess);

	// tag one message at a time (including any dupes)
	while (pmt)
		{
		pft = &pmt->ftOut;
		FileAllocate(pft, MESS_SIZE);

		pmt->ftIn.pcPosition = pmt->ftIn.pcBase;
		if (fiUnesc(pft, &pmt->ftIn, NULL, 0) == FALSE)
			{
			return FALSE;
			}
		FileResize(pft, iFileSize(pft));

		pmt = (MESSTAG *) pcListCanGet(&ltMess);
		}

	return TRUE;
	}
#endif

int fiSort(void)
	{
	// sort the message information collected
	qsort(ltMess.pcBase, ltMess.iAppend, sizeof(MESSTAG), iMessCompare);

	return TRUE;
	}

static int iMessCompare(const void *pvLeft, const void *pvRight)
	{
	int i, j;

	// apply an artifical prefix to the sort order for merging
	i = ((MESSTAG *)pvLeft)->iPrefix;
	j = ((MESSTAG *)pvRight)->iPrefix;
	if (i < j)
		{
		return -1;
		}
	if (i > j)
		{
		return 1;
		}

	// both have same artifical prefix, need to compare them
	i = iStringCompare(((MESSTAG *) pvLeft)->ftOut.pcBase,
			      iFileSize(&((MESSTAG *) pvLeft)->ftOut),
			      ((MESSTAG *) pvRight)->ftOut.pcBase,
			      iFileSize(&((MESSTAG *) pvRight)->ftOut));
	if (i)
		{
		return i;
		}

	// apply an artifical suffix to the sort order for merging
	i = ((MESSTAG *)pvLeft)->iSuffix;
	j = ((MESSTAG *)pvRight)->iSuffix;
	if (i < j)
		{
		return -1;
		}
	if (i > j)
		{
		return 1;
		}

	// both prefixes, both strings, and both suffixes identical
	return 0;
	}

int fiUniq(void)
	{
	int iAppend;
	MESSTAG *pmt, *pmtNext, *pmtOut;

	// set up one item of lookahead in message table
	ltMess.iPosition = 0;
	pmt = (MESSTAG *) pcListCanGet(&ltMess);

	// advance one message at a time, squashing duplicates
	iAppend = 0;
	pmtOut = pmt; // the message list will be manipulated in-place
	while (pmt)
		{
		iAppend++;
		memcpy(pmtOut++, pmt, sizeof(MESSTAG));

		do
			{
			pmtNext = (MESSTAG *) pcListCanGet(&ltMess);
			}
		while (pmtNext && fiMessEqual(pmt, pmtNext));

		pmt = pmtNext;
		}

	// indicate how many list items survived
	ltMess.iAppend = iAppend;

	return TRUE;
	}

static int fiMessEqual(const void *pvLeft, const void *pvRight)
	{
	// note, this doesn't use the iPrefix or iSuffix fields,
	// this is intentional - it means we can use fiSort() to
	// put the messages in some strange order, then fiUniq()
	// to delete duplicates, then fiSort() to original order

	return fiStringEqual(((MESSTAG *) pvLeft)->ftOut.pcBase,
			     iFileSize(&((MESSTAG *) pvLeft)->ftOut),
			     ((MESSTAG *) pvRight)->ftOut.pcBase,
			     iFileSize(&((MESSTAG *) pvRight)->ftOut));
	}

// ----------------------------------------------------------------------------

int fiSuck(FILETAG *pft, FILETAG *pftIn, char *pcTerm, int iSizeTerm)
	{
	int i, j;
	char *pc;
	char *pcArg;
	int iSizeArg;

	// convert pointer to keyword into file position
	i = pftIn->pcPosition - pftIn->pcBase;
	j = i;

	pc = pft->pcAppend;
	i = iStringTokeniseBasic(i, pftIn->pcBase, iFileSize(pftIn),
			       &pcArg, &iSizeArg);
	while (iSizeArg)
		{
		if (iSizeTerm && fiStringEqual(pcArg, iSizeArg,
					       pcTerm, iSizeTerm))
			{
			break;
			}

		if (pc + iSizeArg + 2 > pft->pcLimit) /* 1 for UNIX! */
			{
			pftIn->pcPosition = pftIn->pcBase + j;
			return FALSE;
			}

		memcpy(pc, pcArg, iSizeArg);
		pc += iSizeArg;
		*pc++ = '\r'; /* remove this for UNIX! */
		*pc++ = '\n';

		j = i;
		i = iStringTokeniseBasic(i, pftIn->pcBase, iFileSize(pftIn),
				       &pcArg, &iSizeArg);
		}

	pftIn->pcPosition = pftIn->pcBase + j;
	pft->pcAppend = pc;

	return TRUE;
	}

int fiUnesc(FILETAG *pft, FILETAG *pftIn, char *pcTerm, int iSizeTerm)
	{
	int i, j, k, ch;
	char *pc;
	char *pcArg;
	int iSizeArg;

	// convert pointer to keyword into file position
	i = pftIn->pcPosition - pftIn->pcBase;
	j = i;

	pc = pft->pcAppend;
	i = iStringTokeniseBasic(i, pftIn->pcBase, iFileSize(pftIn),
			       &pcArg, &iSizeArg);
	while (iSizeArg)
		{
// fputs("--> ", stdout);
// fwrite(pcArg, iSizeArg, 1, stdout);
// fputs("\n", stdout);
// fflush(stdout);

		if (iSizeTerm && fiStringEqual(pcArg, iSizeArg,
					       pcTerm, iSizeTerm))
			{
			break;
			}

		if (pcArg[0] != '\"')
			{
			pftIn->pcPosition = pftIn->pcBase + j;
			return FALSE;
			}

		for (k = 1; k < iSizeArg; k++)
			{
			ch = pcArg[k];
			if (ch == '\"')
				{
				break; // unescaped closing quote
				}
			if (ch == '\\' && (k + 1) < iSizeArg)
				{
				ch = pcArg[++k];
				switch (ch)
					{
				case 'b':
					ch = '\b';
					break;
				case 'n':
					ch = '\n';
					break;
				case 'r':
					ch = '\r';
					break;
				case 't':
					ch = '\t';
					break;
					}
				}

			if (pc + 1 > pft->pcLimit)
				{
				pftIn->pcPosition = pftIn->pcBase + j;
				return FALSE;
				}

			*pc++ = ch;
			}

		j = i;
		i = iStringTokeniseBasic(i, pftIn->pcBase, iFileSize(pftIn),
				       &pcArg, &iSizeArg);
		}

	pftIn->pcPosition = pftIn->pcBase + j;
	pft->pcAppend = pc;

	return TRUE;
	}
#endif

// ----------------------------------------------------------------------------

int iTolc(LISTTAG *pltCache, int argc, char **argv)
	{
	int i;

	int iCacheWork;
	FILETAG *pftWork;
	char *pszInFileName;

	char *pszOutFileName;
	char szOutFileName[PATH_MAX];

	pszInFileName = NULL;
	pszOutFileName = NULL;

	if (argc > 1)
		{
		pszInFileName = argv[1];
		}

	if (argc > 2)
		{
		pszOutFileName = argv[2];
		}

	if (pszInFileName == NULL)
		{
		printf("usage: tolc infile.bas [outfile.b]\n");
		exit(1);
		}

	if (pszOutFileName == NULL)
		{
		pszOutFileName = szOutFileName;
		strcpy(pszOutFileName, pszInFileName);

		i = strlen(pszOutFileName);
		while (i--)
			{
			if (pszOutFileName[i] == ':' ||
					pszOutFileName[i] == '/' ||
					pszOutFileName[i] == '\\')
				{
				break; /* no extension, so don't strip it */
				}
			if (pszOutFileName[i] == '.')
				{
				pszOutFileName[i] = 0; /* strip dot and extension */
				break; /* ready to concatenate our extension */
				}
			}

		strcat(pszOutFileName, ".b");
		}

	if (strcmp(pszInFileName, pszOutFileName) == 0)
		{
		printf("Input and output filenames identical\n");
		exit(1);
		}

	// read the input c file entirely to a malloc'd block
	CacheReadIn(pltCache, &iCacheWork, pszInFileName);
	pftWork = pftCacheItem(pltCache, iCacheWork);

	// append esc plus character set data to output buffer
	if (fiProcess(fiProcessTolc, pftWork) == FALSE)
		{
		exit(1);
		}

	// ready to write the output we found
	CacheWriteOut(pltCache, iCacheWork, pszOutFileName);
	pctCacheItem(pltCache, iCacheWork)->iDirty = 4; // extra dirty

	// it's the song that doesn't end
	return 0;
	}

// ----------------------------------------------------------------------------

int fiProcess(int (*pffiHandler)(FILETAG *pftWork), FILETAG *pftWork)
	{
	int i, j;
	int iSizeArg;
	char *pcArg, *pcComma, *pcLine; //, *pcKeyword;

	// parse the user's 'C' source code file using tokens
	pcComma = NULL;
	pcLine = pftWork->pcBase;
	//pcKeyword = NULL;
	i = iStringTokeniseBasic(0, pftWork->pcBase, iFileSize(pftWork),
			       &pcArg, &iSizeArg);
	while (iSizeArg)
		{
// fwrite(pcArg, iSizeArg, 1, stdout);
// fputs("\n", stdout);
// fflush(stdout);
#if 0
		if (pcKeyword)
			{
			if (fiStringEqual(pcArg, iSizeArg, "(", 1))
				{
				pftWork->pcPosition = pcKeyword;
				if (pffiHandler(pftWork))
					{
					pcKeyword = NULL;
					i = pftWork->pcPosition -
					    pftWork->pcBase;
					goto mycontinue;
					}
				}
			pcKeyword = NULL;
			}

		if (fiStringEqual(pcArg, iSizeArg, "_", 1) ||
		    fiStringEqual(pcArg, iSizeArg, "N_", 2))
			{
			pcKeyword = pcArg;
			}

	mycontinue:
#endif
		if (pcComma == pcArg)
			{
			if (pftWork->pcAppend + 1 > pftWork->pcLimit)
				{
				// need to expand beyond current limit
				FileResize(pftWork,
					   iFileSize(pftWork) + 1);
				}

			// need to use reverse copy if expanding
			memmove(pcArg + 1, pcArg, pftWork->pcAppend - pcArg);
			pftWork->pcAppend += 1;

			*pcArg++ = ' ';
			i++;
			}
		pcComma = (*pcArg == ',') ? pcArg + 1 : NULL;

		if (*pcArg == 0x0a)
			{
			pcLine = pcArg + 1;
			}

		if (*pcArg != '"')
			{
			for (j = 0; j < iSizeArg; j++)
				{
				pcArg[j] = tolower(pcArg[j]);
				if (pcArg[j] == '.' &&
						(*pcArg < '0' ||
						*pcArg >= '9'))
					{
					pcArg[j] = '_';
					}
				}
			}

		i = iStringTokeniseBasic(i,
					 pftWork->pcBase, iFileSize(pftWork),
					 &pcArg, &iSizeArg);
		}

	return TRUE;
	}

// ----------------------------------------------------------------------------

static int fiProcessTolc(FILETAG *pftWork)
	{
#if 0
	int i, j;
	char *pc;
	int iSize, iDelta;
	char *pcArg;
	int iSizeArg;
	MESSTAG *pmt;
	FILETAG ft;

	// convert pointer to keyword into file position
	i = pftWork->pcPosition - pftWork->pcBase;
	j = i;

	// skip the tokens _ and (, we know they will match
	i = iStringTokeniseBasic(i, pftWork->pcBase, iFileSize(pftWork),
			       &pcArg, &iSizeArg);
	i = iStringTokeniseBasic(i, pftWork->pcBase, iFileSize(pftWork),
			       &pcArg, &iSizeArg);

	// parse and concatenate the individual 'C' strings
	FileAllocate(&ft, MESS_SIZE);
	pftWork->pcPosition = pftWork->pcBase + i;
	if (fiUnesc(&ft, pftWork, ")", 1) == FALSE)
		{
// fputs("FALSE\n", stdout);
// fflush(stdout);
		FileFree(&ft);
		return FALSE;
		}
// fputs("result \"", stdout);
// fwrite(ft.pcBase, iFileSize(&ft), 1, stdout);
// fputs("\"\nTRUE\n", stdout);
// fflush(stdout);
	i = pftWork->pcPosition - pftWork->pcBase;
	FileResize(&ft, iFileSize(&ft));

	// check for the ) keyword closing the expression
	i = iStringTokeniseBasic(i, pftWork->pcBase, iFileSize(pftWork),
			       &pcArg, &iSizeArg);
	if (fiStringEqual(pcArg, iSizeArg, ")", 1) == FALSE)
		{
		FileFree(&ft);
		return FALSE;
		}

	// set up one item of lookahead in message table
	ltMess.iPosition = 0;
	pmt = (MESSTAG *) pcListCanGet(&ltMess);

	// perform a linear search against the cached messages
	pc = ft.pcBase; // reuse a variable to speed things up a bit
	iSize = iFileSize(&ft); // reuse a variable to speed things up a bit
	while (pmt)
		{
// fputs("trying \"", stdout);
// fwrite(pmt->ftOut.pcBase, iFileSize(&pmt->ftOut), 1, stdout);
// fputs("\"\n", stdout);
// fflush(stdout);
		if (fiStringEqual(pc, iSize,
				  pmt->ftOut.pcBase, iFileSize(&pmt->ftOut)))
			{
// fputs("BINGO! \"", stdout);
// fwrite(pmt->ftIn.pcBase, iFileSize(&pmt->ftIn), 1, stdout);
// fputs("\"\n", stdout);
// fflush(stdout);
			FileFree(&ft); // ft was only for comparison

			// bingo!  replace the entire 'C' expression
			iSize = iFileSize(&pmt->ftIn) - 2; // trim (UNIX 1!)
			iDelta = iSize + j - i;

			if (pftWork->pcAppend + iDelta > pftWork->pcLimit)
				{
				// need to expand beyond current limit
				FileResize(pftWork,
					   iFileSize(pftWork) + iDelta);
				}

			if (iDelta)
				{
				// need to use reverse copy if expanding
				pc = pftWork->pcBase + i;
				memmove(pftWork->pcBase + j + iSize,
					pc, pftWork->pcAppend - pc);
				pftWork->pcAppend += iDelta;
				}

			memcpy(pftWork->pcBase + j, pmt->ftIn.pcBase, iSize);
			pftWork->pcPosition = pftWork->pcBase + j + iSize;
			return TRUE;
			}

		pmt = (MESSTAG *) pcListCanGet(&ltMess);
		}

	// not a valid message, don't replace it - the 'C' compiler
	// will notice the _(something) sequence and hopefully barf
	FileFree(&ft);
// fputs("returning FALSE\n", stdout);
// fflush(stdout);
#endif
	return FALSE;
	}

// ----------------------------------------------------------------------------

static int iStringTokeniseBasic(int iIndex, char *psz, int iLimit,
                    char **ppsz, int *piCount)
     {
     int i, j, iCount, ch, qu;

     iCount = iLimit - iIndex;
     if (iCount < 1)
          {
          *ppsz = NULL;
          *piCount = 0;
          return iIndex;
          }

     // scan past any leading whitespace
     for (i = 0; i < iCount; i++)
          {
          ch = psz[iIndex + i];
          switch (ch)
               {
          case 0x09:
//          case 0x0a:
          case 0x0c:
          case 0x0d:
          case 0x20:
               continue;
          case '/':
               if ((i + 1) >= iCount)
                    {
                    break;
                    }
               switch (psz[iIndex + i + 1])
                    {
               case '*':
                    for (i += 2; (i + 1) < iCount; i++)
                         {
                         if (psz[iIndex + i] == '*' &&
                                   psz[iIndex + i + 1] == '/')
                              {
                              i++;
                              break;
                              }
                         }
                    i++;
                    continue;
               case '/':
                    for (i += 2; i < iCount; i++)
                         {
                         switch (psz[iIndex + i])
                              {
                         case 0x00:
                         case 0x0a:
                         case 0x0d:
                              continue;
                              }
                         }
                    continue;
                    }
               }
          break;
          }

     if (i >= iCount)
          {
          *ppsz = NULL;
          *piCount = 0;
          return iIndex;
          }

     j = i;
     ch = ((unsigned char *)psz)[iIndex + i++];

     // quoted strings are handled differently
     if (ch == '"') // || ch == '\'')
          {
          qu = ch; // take note of the closing quote style

          // scan past quoted string, skipping escapes
          for (; i < iCount; i++)
               {
               ch = ((unsigned char *)psz)[iIndex + i];
               if (ch == qu)
                    {
                    i++; // skip the closing quote
                    break; // end of string encountered
                    }
#if 0
               if (ch == '\\')
                    {
                    i++; // skip a possible quote if escaped
                    if (i >= iCount)
                         {
                         break; // backslash-newline encountered
                         }
                    }
#endif
               }

          // string (maybe incomplete), so return start and length to caller
          *ppsz = psz + iIndex + j;
          *piCount = i - j;
          return iIndex + i;
          }

     if ((ch < '0' || ch > '9') &&
               (ch < 'A' || ch > 'Z') &&
               (ch < 'a' || ch > 'z') &&
               ch != '_')
          {
          // single char operator, so return start and length to caller
          *ppsz = psz + iIndex + j;
          *piCount = 1;
          return iIndex + i;
          }

     // scan past as much symbol text as possible
     for (; i < iCount; i++)
          {
          ch = ((unsigned char *)psz)[iIndex + i];
          if ((ch < '0' || ch > '9') &&
                    (ch < 'A' || ch > 'Z') &&
                    (ch < 'a' || ch > 'z') &&
                    ch != '_' && ch != '.')
               {
               break;
               }
          }

     // valid symbol, so return start and length to caller
     *ppsz = psz + iIndex + j;
     *piCount = i - j;
     return iIndex + i;
     }

// ----------------------------------------------------------------------------

