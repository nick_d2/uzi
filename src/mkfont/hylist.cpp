// hylist.cpp

// ----------------------------------------------------------------------------

#include <io.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdio.h>
#include <windows.h>
#pragma hdrstop
#include "hymem.h"
#include "hyfs.h"
#include "hylist.h"

// ----------------------------------------------------------------------------

int fiListAllocate(LISTTAG *plt, int iItemCount, int iItemSize)
     {
     // zero the entire list tag item data area, avoiding leaks
     memset(plt, 0, sizeof(LISTTAG));

     // allocate via the lower level wrapper, relying on the fact
     // that the LISTTAG structure is compatible with MEMTAG !!
     if (fiMemAlloc((MEMTAG *) plt, iItemCount * iItemSize) == 0)
          {
          return FALSE; // indicates failure to allocate block
          }

     // set position to start and list to null
     plt->iBase = 0;
     plt->iLimit = iItemCount;
     plt->iPosition = 0;
     plt->iAppend = 0;

     // save the item size for use in addressing calcuations
     plt->iItemSize = iItemSize;

     // say it is now allocated
     return TRUE;
     }

int fiListFree(LISTTAG *plt)
     {
     // free via the lower level wrapper, relying on the fact
     // that the LISTTAG structure is compatible with MEMTAG !!
     if (fiMemFree((MEMTAG *) plt) == 0)
          {
          return FALSE; // indicates failure to free block
          }

     // zero the entire list tag item data area, avoiding leaks
     memset(plt, 0, sizeof(LISTTAG));

     return TRUE; // indicates block freed and tag item zeroed
     }

// ----------------------------------------------------------------------------

int fiListReadIn(LISTTAG *plt, char *pszName, int iItemSize)
     {
     int iSize;
     FSHANDLE fsh;

     // zero the entire list tag item data area, avoiding leaks
     memset(plt, 0, sizeof(LISTTAG));

     // attempt to open list for reading, get size
     if (fiFSOpen(&fsh, pszName) == FALSE)
          {
          return FALSE; // probably file not found
          }
     iSize = filelength(fsh.iHandle); // assume can get size

#if 0
     // sanity check for list much too large
     if (iSize > 0x100000) // 1 megabyte
          {
          fiFSClose(&fsh);
          return FALSE;
          }
#endif

     // allocate heap to hold entire list
     // note: simpler than the array or pool implementation,
     // because the original limit, position, append info lost
     if (fiListAllocate(plt, (iSize + iItemSize - 1) / iItemSize,
                             iItemSize) == FALSE)
          {
          fiFSClose(&fsh);
          return FALSE;
          }

     // attempt read the exact amount of data
     if (iFSRead(&fsh, plt->pcBase, iSize) != iSize)
          {
          fiListFree(plt);
          fiFSClose(&fsh);
          return FALSE;
          }

     fiFSClose(&fsh);

     // set position to start and length to length read
     plt->iBase = 0;
     plt->iLimit = (iSize + iItemSize - 1) / iItemSize;
     plt->iPosition = 0;
     plt->iAppend = iSize / iItemSize;

     // save the item size for use in addressing calcuations
     plt->iItemSize = iItemSize;

     return TRUE; // says we read the list
     }

int fiListWriteOut(LISTTAG *plt, char *pszName)
     {
     int iSize;
     FSHANDLE fsh;

     // attempt to create or truncate list for writing
     if (fiFSCreate(&fsh, pszName) == FALSE)
          {
          return FALSE; // probably directory not found
          }

     // attempt to write the exact amount of data
     iSize = iListSize(plt) * plt->iItemSize;
     if (iFSWrite(&fsh, plt->pcBase, iSize) != iSize)
          {
          fiFSClose(&fsh);
          return FALSE;
          }

     fiFSClose(&fsh);
     return TRUE; // says we wrote the list
     }

// ----------------------------------------------------------------------------

int fiListCopy(LISTTAG *plt, LISTTAG *pltIn, int iItemSize)
     {
     int i, iSize;

     // zero the entire list tag item data area, avoiding leaks
     memset(plt, 0, sizeof(LISTTAG));

     // allocate via the lower level wrapper, relying on the fact
     // that the LISTTAG structure is compatible with MEMTAG !!
     iSize = iListSize(pltIn) * pltIn->iItemSize;
     if (fiMemAlloc((MEMTAG *) plt, iSize) == FALSE)
          {
          return FALSE; // indicates failure to allocate block
          }

     // calculate the required adjustment, before clobbering plt!
     i = plt->pcBase - pltIn->pcBase;

     // copy the entire list tag item data area, avoiding leaks
     memcpy(plt, pltIn, sizeof(LISTTAG));

     // apply the required adjustment to all fields of listtag
     plt->pcBase += i;

     // another slight adjustment because the copy trims to size
     plt->pcLimit = plt->pcBase + iSize;
     plt->iLimit = iSize;

     // copy the data area attached to the input list tag
     memcpy(plt->pcBase, pltIn->pcBase, iSize);

     return TRUE; // indicates copied and new tag item filled out
     }

// ----------------------------------------------------------------------------

char *pcListCanGet(LISTTAG *plt)
     {
     if (plt->iPosition >= plt->iAppend)
          {
          return NULL; // indicates no more items available in list
          }

     return pcListItem(plt, plt->iPosition++);
     }

char *pcListCanPut(LISTTAG *plt)
     {
     char *pc;

     if (plt->iAppend >= plt->iLimit)
          {
          return NULL; // indicates no room to append a new item
          }

     pc = pcListItem(plt, plt->iAppend++);
     memset(pc, 0, plt->iItemSize);
     return pc;
     }

// ----------------------------------------------------------------------------

