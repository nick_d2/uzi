/* hfmlib.c by Nick for Hytech Font Manager system */

/* ------------------------------------------------------------------------- */
/* header and preprocessor directives */

#include <stdio.h>
#ifdef _MSC_VER
#include <windows.h>
#include <io.h>
#else
#include <unistd.h>
#endif
#include <fcntl.h>
#include <string.h>
#include <malloc.h>

#include "hfmlib.h"

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

/* ------------------------------------------------------------------------- */
/* global variables */

/* note:  the library is allowed exactly 1 global variable, which must be of */
/* pointer type.  if there is a need for more, we must implement a global_t */

hfm_t *hfm;

/* ------------------------------------------------------------------------- */
/* exported functions */

int hfm_load(char *hfm_path)
	{
	int fd;
	unsigned int u, v;
	hfm_t hdr;

	if (hfm)
		{
		fprintf(stderr, "%s: already loaded\n", hfm_path);
		return FALSE;
		}

	fd = open(hfm_path, O_RDONLY | O_BINARY);
	if (fd < 0)
		{
		goto hfm_load_perror; /* use a goto to save code space */
		}

	if (read(fd, &hdr, sizeof(hfm_t)) < sizeof(hfm_t) ||
			hdr.hfm_magic != HFM_MAGIC ||
			hdr.hfm_format != HFM_FORMAT ||
			hdr.hfm_file_size < sizeof(hfm_t))
		{
		fprintf(stderr, "%s: bad header\n", hfm_path);
		return FALSE;
		}

	hfm = (hfm_t *)malloc(hdr.hfm_file_size);
	if (hfm == NULL)
		{
		fprintf(stderr, "%s: out of memory\n", hfm_path);
		return FALSE;
		}
	memcpy(hfm, (char *)&hdr, sizeof(hfm_t));

	u = hdr.hfm_file_size - sizeof(hfm_t);
	v = read(fd, (char *)hfm + sizeof(hdr), u);

	close(fd);
	if (v == u)
		{
		return TRUE; /* success, access the block via hfm_t *hfm */
		}

	free(hfm); /* had an error, so don't leak the temporary buffer */
	hfm = NULL; /* politely inform everyone there's really no file */

	if (v < u)
		{
		fprintf(stderr, "%s: too short\n", hfm_path);
		return FALSE;
		}

hfm_load_perror:
	perror(hfm_path);
	return FALSE;
	}

void hfm_free(void)
	{
	if (hfm)
		{
		free(hfm);
		hfm = NULL;
		}
	}

/* ------------------------------------------------------------------------- */

int hfm_search_family(int device, int family,
		      int height_min, int height_max,
		      hfm_family_t *hfmf_ret)
	{
	int i, j, k;
	hfm_device_t *hfmd;
	hfm_family_t *hfmf;
	hfm_style_t *p, *q;

	/* find the specified device */

	if (hfm == NULL || device < 0 || device >= hfm->hfm_device_count)
		{
		return FALSE; /* no file yet or device index too large */
		}

	hfmd = (hfm_device_t *)HFM_DEREF(hfm, hfm->hfm_device_table) +
	       device;

	/* in the device description, find the specified family */

	if (family < 0 || family >= hfmd->hfmd_family_count)
		{
		return FALSE; /* device not in use or family index too large */
		}

	hfmf = (hfm_family_t *)HFM_DEREF(hfm, hfmd->hfmd_family_table) +
	       family;

	/* in the family description, find all styles with height in range */

	k = hfmf->hfmf_style_count;

	/* step 1:  find the first font which satisfies the height minimum */

	p = (hfm_style_t *)HFM_DEREF(hfm, hfmf->hfmf_style_table);
	for (i = 0; i < k; i++)
		{
		if (p->hfmst_height >= height_min)
			{
			break;
			}
		p++;
		}

	/* step 2:  find the first font which violates the height maximum */

	q = p;
	for (j = i; j < k; j++)
		{
		if (q->hfmst_height >= height_max)
			{
			break;
			}
		q++;
		}

	/* found at least one font for the device, family and height range? */

	if (j > i)
		{
		hfmf_ret->hfmf_style_count = j - i;
		hfmf_ret->hfmf_style_table = HFM_REF(hfm, p);
		return TRUE; /* yes, return a family descriptor to the user */
		}

	return FALSE; /* no, don't bother to fill in the user's descriptor */
	}

int hfm_search_family_auto(int device, int family,
			   int height_min, int height_max,
			   hfm_family_t *hfmf_ret)
	{
	/* step 1:  see if any fonts in the user's preferred family will */
	/* satisfy the given height requirement (at least 1 style available) */

	if (hfm_search_family(device, family,
			      height_min, height_max, hfmf_ret))
		{
		return TRUE; /* the user's preferred family is available */
		}

	/* step 2:  try the standard system font (sans serif) */
	/* unless we already tried this in the initial search */

	if (family != HFMF_SANS && hfm_search_family(device, HFMF_SANS,
						     height_min, height_max,
						     hfmf_ret))
		{
		return TRUE; /* standard (sans serif) family is available */
		}

	/* step 3:  try the alternate system font (seriffed) */
	/* unless we already tried this in the initial search */

	if (family != HFMF_SERIF && hfm_search_family(device, HFMF_SERIF,
						      height_min, height_max,
						      hfmf_ret))
		{
		return TRUE; /* alternate (seriffed) family is available */
		}

	return FALSE; /* no automatic font-change available in height range */
	}

/* ------------------------------------------------------------------------- */

int hfm_search_style(hfm_family_t *hfmf, int style, int *slot_ret)
	{
	int k;
	hfm_style_t *p;

	/* using the given family description, find a particular style */

	k = hfmf->hfmf_style_count;

	/* search backwards for the tallest font with the requested style */

	p = (hfm_style_t *) HFM_DEREF(hfm, hfmf->hfmf_style_table) + k;
	while (k--)
		{
		p--;
		if (p->hfmst_style == style)
			{
			*slot_ret = p->hfmst_slot;
			return TRUE; /* user's requested style is available */
			}
		}

	return FALSE; /* user's requested style is not available */
	}

int hfm_search_style_auto(hfm_family_t *hfmf, int style, int *slot_ret)
	{
	/* step 1:  see if the user's preferred style is available */

	if (hfm_search_style(hfmf, style, slot_ret))
		{
		return TRUE; /* the user's preferred style is available */
		}

	/* step 2:  try the regular style unless we already tried this */

	if (style != HFMST_REGULAR && hfm_search_style(hfmf, HFMST_REGULAR,
						       slot_ret))
		{
		return TRUE; /* standard (regular) style is available */
		}

	/* step 3:  try the condensed style unless we already tried this */

	if (style != HFMST_CONDENSED && hfm_search_style(hfmf, HFMST_CONDENSED,
							 slot_ret))
		{
		return TRUE; /* alternate (condensed) style is available */
		}

	return FALSE; /* no automatic style-change available in family */
	}

/* ------------------------------------------------------------------------- */

hfm_slot_t *hfm_locate(int device, int slot)
	{
	hfm_device_t *hfmd;

	/* find the specified device */

	if (hfm == NULL || device < 0 || device >= hfm->hfm_device_count)
		{
		return FALSE; /* no file yet or device index too large */
		}

	hfmd = (hfm_device_t *)HFM_DEREF(hfm, hfm->hfm_device_table) +
	       device;

	/* in the device description, find the specified slot */

	if (slot < 0 || slot >= hfmd->hfmd_slot_count)
		{
		return FALSE; /* device not in use or slot index too large */
		}

	return (hfm_slot_t *)HFM_DEREF(hfm, hfmd->hfmd_slot_table) +
	       slot;
	}

/* ------------------------------------------------------------------------- */

int hfm_width(int device, int slot, char *string)
	{
	hfm_slot_t *hfmsl;
	char *hfmw;
	unsigned int u, char_first, char_count;
	int width;

	hfmsl = hfm_locate(device, slot);
	char_first = hfmsl->hfmsl_char_first;
	char_count = hfmsl->hfmsl_char_count;

	hfmw = (char *)HFM_DEREF(hfm, hfmsl->hfmsl_width_table);
	width = 0;

	u = *(unsigned char *)string; /* get first char, unsigned!! */
	while (u)
		{
		u -= char_first; /* offset it for start of table */
		if (u < char_count) /* does character exist in table? */
			{
			width += hfmw[u]; /* yes, proportional width */
			}
		else
			{
			width += hfmsl->hfmsl_x_total; /* no, standard width */
			}

		u = *(unsigned char *)++string; /* get next char, unsigned!! */
		}

	return width; /* note:  we do not return the count here!! */
	}

int hfm_count_in_field(int device, int slot, char *string,
		       int field_width, int *width_ret)
	{
	hfm_slot_t *hfmsl;
	char *hfmw;
	unsigned int u, char_first, char_count;
	int w, width, count;

	hfmsl = hfm_locate(device, slot);
	char_first = hfmsl->hfmsl_char_first;
	char_count = hfmsl->hfmsl_char_count;

	hfmw = (char *)HFM_DEREF(hfm, hfmsl->hfmsl_width_table);
	width = 0;
	count = 0;

	u = *(unsigned char *)string; /* get first char, unsigned!! */
	while (u)
		{
		u -= char_first; /* offset it for start of table */
		if (u < char_count) /* does character exist in table? */
			{
			w = hfmw[u]; /* yes, proportional width */
			}
		else
			{
			w = hfmsl->hfmsl_x_total; /* no, standard width */
			}

		width += w;
		if (width > field_width)
			{
			width -= w; /* restore width so far */
			break; /* too wide, truncate before this ch */
			}

		count++;

		u = *(unsigned char *)++string; /* get next char, unsigned!! */
		}

	if (width_ret != NULL)
		{
		*width_ret = width;
		}

	return count;
	}

/* ------------------------------------------------------------------------- */

#ifdef _MSC_VER
char *hfm_family_int_to_string(int family)
	{
	switch (family)
		{
	case HFMF_SANS:
		return HFMSTR_SANS;
	case HFMF_SERIF:
		return HFMSTR_SERIF;
		}
	return "";
	}

int hfm_family_string_to_int(char *family)
	{
	if (strcmp(family, HFMSTR_SANS) == 0)
		{
		return HFMF_SANS;
		}
	if (strcmp(family, HFMSTR_SERIF) == 0)
		{
		return HFMF_SERIF;
		}
	return -1;
	}

char *hfm_style_int_to_string(int style)
	{
	switch (style)
		{
	case HFMST_CONDENSED:
		return HFMSTR_CONDENSED;
	case HFMST_REGULAR:
		return HFMSTR_REGULAR;
	case HFMST_BOLD:
		return HFMSTR_BOLD;
	case HFMST_ITALIC:
		return HFMSTR_ITALIC;
	case HFMST_UNDERLINE:
		return HFMSTR_UNDERLINE;
		}
	return "";
	}

int hfm_style_string_to_int(char *style)
	{
	if (strcmp(style, HFMSTR_CONDENSED) == 0)
		{
		return HFMST_CONDENSED;
		}
	if (strcmp(style, HFMSTR_REGULAR) == 0)
		{
		return HFMST_REGULAR;
		}
	if (strcmp(style, HFMSTR_BOLD) == 0)
		{
		return HFMST_BOLD;
		}
	if (strcmp(style, HFMSTR_ITALIC) == 0)
		{
		return HFMST_ITALIC;
		}
	if (strcmp(style, HFMSTR_UNDERLINE) == 0)
		{
		return HFMST_UNDERLINE;
		}
	return -1;
	}
#endif

/* ------------------------------------------------------------------------- */

