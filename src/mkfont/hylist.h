// hylist.h

#ifndef _INC_HYLIST
#define _INC_HYLIST

// ----------------------------------------------------------------------------

typedef struct
{
     char *pcBase;                 // also the handle for the malloc'd buffer
     char *pcLimit;                // offset just after end of malloc'd buffer

     int iBase; // usually 0 unless client wants to do advanced looping
     int iLimit; // iAppend or larger if block is of arbitrary size
     int iPosition; // for general use, initialised to 0 for client
     int iAppend; // calculated for client, pbih->biHeight * iSamplesLine

     int iItemSize; // bytes per item, to convert above into char pointers
} LISTTAG;

// ----------------------------------------------------------------------------

int fiListAllocate(LISTTAG *plt, int iItemCount, int iItemSize);
int fiListFree(LISTTAG *plt);

int fiListReadIn(LISTTAG *plt, char *pszName, int iItemSize);
int fiListWriteOut(LISTTAG *plt, char *pszName);

int fiListCopy(LISTTAG *plt, LISTTAG *pltIn);

char *pcListCanGet(LISTTAG *plt);
char *pcListCanPut(LISTTAG *plt);

// ----------------------------------------------------------------------------

inline int iListSize(LISTTAG *plt)
     {
     // return size in bytes, for use by ListReadIn etc family
     return plt->iAppend /*- plt->iBase*/; // note: the trimmed size
     }

inline char *pcListItem(LISTTAG *plt, int iItem)
     {
     // return pointer to start of item, caller has to cast it
     return plt->pcBase + iItem * plt->iItemSize;
     }

inline char *pcListGet(LISTTAG *plt)
     {
     char *pc;

     pc = pcListCanGet(plt);
     if (pc == NULL)
          {
          printf("pcListGet: Could not read from list 0x%08x\n", plt);
          exit(1);
          }
     return pc;
     }

inline char *pcListPut(LISTTAG *plt)
     {
     char *pc;

     pc = pcListCanPut(plt);
     if (pc == NULL)
          {
          printf("pcListPut: Could not append to list 0x%08x\n", plt);
          exit(1);
          }
     return pc;
     }

// ----------------------------------------------------------------------------
// handy functions for the client, not important to the interface:

#ifdef _INC_STDIO
inline void ListAllocate(LISTTAG *plt, int iItemCount, int iItemSize)
     {
     if (fiListAllocate(plt, iItemCount, iItemSize) == FALSE)
          {
          printf("ListAllocate: "
                 "Could not allocate 0x%08x items of 0x%08x bytes\n",
                 iItemCount, iItemSize);
          exit(1);
          }

#if DEBUG
     printf("ListAllocate: "
            "Allocated list 0x%08x, 0x%08x items of 0x%08x bytes\n",
            plt, iItemCount, iItemSize);
#endif
     }

inline void ListFree(LISTTAG *plt)
     {
     if (fiListFree(plt) == FALSE)
          {
          printf("ListFree: Could not free list 0x%08x\n", plt);
          exit(1);
          }

#if DEBUG
     printf("ListFree: Freed list 0x%08x\n", plt);
#endif
     }

inline void ListCopy(LISTTAG *plt, LISTTAG *pltIn)
     {
     if (fiListCopy(plt, pltIn) == FALSE)
          {
          printf("ListCopy: Could not copy 0x08x items of 0x%08x bytes\n",
                 iListSize(pltIn), pltIn->iItemSize);
          exit(1);
          }
     }

inline void ListReadIn(LISTTAG *plt, char *pszName, int iItemSize)
     {
     if (fiListReadIn(plt, pszName, iItemSize) == FALSE)
          {
          printf("ListReadIn: Could not read %s\n", pszName);
          exit(1);
          }

     printf(
#if DEBUG
            "ListReadIn: "
#endif
	    "Loaded %s, 0x%08x items of 0x%08x bytes\n",
            pszName, iListSize(plt), iItemSize);
     }

inline void ListWriteOut(LISTTAG *plt, char *pszName)
     {
     if (fiListWriteOut(plt, pszName) == FALSE)
          {
          printf("ListWriteOut: Could not write %s\n", pszName);
          exit(1);
          }

     printf(
#if DEBUG
            "ListWriteOut: "
#endif
            "Created %s, 0x%08x items of 0x%08x bytes\n",
            pszName, iListSize(plt), plt->iItemSize);
     }
#endif

// ----------------------------------------------------------------------------

#endif

