// hystring.h

// ----------------------------------------------------------------------------
// prototypes for hystring.cpp

int fiStringEqual(char *psz0, int iCount0, char *psz1, int iCount1);
int iStringCompare(char *psz0, int iCount0, char *psz1, int iCount1);

void StringCopy(char *psz, char *pszIn, int iCount);
int iStringAppend(int iIndex, char *psz, int iLimit, char *pszIn, int iCount);

void StringToLower(char *psz, char *pszIn, int iCount);
void StringToUpper(char *psz, char *pszIn, int iCount);

int iStringIsolate(int iIndex, char *psz, int iLimit,
                   char **ppsz, int *piCount);
int iStringTokenise(int iIndex, char *psz, int iLimit,
                    char **ppsz, int *piCount);

// ----------------------------------------------------------------------------

