// hystring.cpp

// ----------------------------------------------------------------------------

#include <windows.h>
#pragma hdrstop
#include "hystring.h"

// ----------------------------------------------------------------------------

int fiStringEqual(char *psz0, int iCount0, char *psz1, int iCount1)
     {
     if (iCount0 != iCount1)
          {
          return FALSE;
          }

#if 0
     if (iCount0 < 1)
          {
          return TRUE; // matching null strings
          }
#endif

     return memcmp(psz0, psz1, iCount0) == 0;
     }

int iStringCompare(char *psz0, int iCount0, char *psz1, int iCount1)
     {
     if (iCount0 < iCount1)
          {
#if 0
          if (iCount0 < 1)
               {
               return -1; // psz0 null, psz1 non-null
               }
#endif

          return memcmp(psz0, psz1, iCount0) > 0 ? 1 : -1;
          }

     if (iCount0 > iCount1)
          {
#if 0
          if (iCount1 < 1)
               {
               return 1; // psz0 non-null, psz1 null
               }
#endif

          return memcmp(psz0, psz1, iCount1) < 0 ? -1 : 1;
          }

#if 0
     if (iCount0 < 1)
          {
          return TRUE; // matching null strings
          }
#endif

     return memcmp(psz0, psz1, iCount0);
     }

// ----------------------------------------------------------------------------

void StringCopy(char *psz, char *pszIn, int iCount)
     {
     int i;

     // copy as much as we can
     for (i = 0; i < iCount; i++)
          {
          if (pszIn[i] == 0)
               {
               break;
               }

          psz[i] = pszIn[i];
          }

     // terminate the string politely
     psz[i] = 0;
     }

int iStringAppend(int iIndex, char *psz, int iLimit, char *pszIn, int iCount)
     {
     int i;

     if (iIndex >= iLimit)
          {
          return iIndex; // in this case no terminator can be saved
          }

     iCount = min(iCount, iLimit - iIndex);
     if (iCount < 1)
          {
          psz[iIndex] = 0;
          return iIndex;
          }

     // copy as much as we can
     for (i = 0; i < iCount; i++)
          {
          if (pszIn[iIndex + i] == 0)
               {
               break;
               }

          psz[iIndex + i] = pszIn[iIndex + i];
          }

     // terminate the string politely
     iIndex += iCount;
     if (iIndex < iLimit)
          {
          psz[iIndex] = 0;
          }

     return iIndex;
     }

// ----------------------------------------------------------------------------

void StringToLower(char *psz, char *pszIn, int iCount)
     {
     int i;

     // copy as much as we can
     for (i = 0; i < iCount; i++)
          {
          if (pszIn[i] == 0)
               {
               break;
               }

          // convert to lower case in the process
          psz[i] = tolower(pszIn[i]);
          }

     // terminate the string politely
     psz[i] = 0;
     }

void StringToUpper(char *psz, char *pszIn, int iCount)
     {
     int i;

     // copy as much as we can
     for (i = 0; i < iCount; i++)
          {
          if (pszIn[i] == 0)
               {
               break;
               }

          // convert to upper case in the process
          psz[i] = toupper(pszIn[i]);
          }

     // terminate the string politely
     psz[i] = 0;
     }

// ----------------------------------------------------------------------------

int iStringIsolate(int iIndex, char *psz, int iLimit,
                   char **ppsz, int *piCount)
     {
     int i, j, iCount;

     iCount = iLimit - iIndex;
     if (iCount < 1)
          {
          *ppsz = NULL;
          *piCount = 0;
          return iIndex;
          }

     // scan past any leading whitespace
     for (i = 0; i < iCount; i++)
          {
          switch (psz[iIndex + i])
               {
          case 0x09:
          case 0x0a:
          case 0x0c:
          case 0x0d:
          case 0x20:
               continue;
               }
          break;
          }

     // scan past as much argument text as possible
     j = i;
     for (; i < iCount; i++)
          {
          switch (psz[iIndex + i])
               {
          case 0x00: // in this case terminator is also a condition
          case 0x09:
          case 0x0a:
          case 0x0c:
          case 0x0d:
          case 0x20:
               goto mybreak;
               }
          }
mybreak:

     // validate length, as argument must have length to exist
     if (i <= j)
          {
          *ppsz = NULL;
          *piCount = 0;
          return iIndex + i;
          }

     // valid argument, so return start and length to caller
     *ppsz = psz + iIndex + j;
     *piCount = i - j;
     return iIndex + i;
     }

// ----------------------------------------------------------------------------

int iStringTokenise(int iIndex, char *psz, int iLimit,
                    char **ppsz, int *piCount)
     {
     int i, j, iCount, ch, qu;

     iCount = iLimit - iIndex;
     if (iCount < 1)
          {
          *ppsz = NULL;
          *piCount = 0;
          return iIndex;
          }

     // scan past any leading whitespace
     for (i = 0; i < iCount; i++)
          {
          ch = psz[iIndex + i];
          switch (ch)
               {
          case 0x09:
          case 0x0a:
          case 0x0c:
          case 0x0d:
          case 0x20:
               continue;
          case '/':
               if ((i + 1) >= iCount)
                    {
                    break;
                    }
               switch (psz[iIndex + i + 1])
                    {
               case '*':
                    for (i += 2; (i + 1) < iCount; i++)
                         {
                         if (psz[iIndex + i] == '*' &&
                                   psz[iIndex + i + 1] == '/')
                              {
                              i++;
                              break;
                              }
                         }
                    i++;
                    continue;
               case '/':
                    for (i += 2; i < iCount; i++)
                         {
                         switch (psz[iIndex + i])
                              {
                         case 0x00:
                         case 0x0a:
                         case 0x0d:
                              continue;
                              }
                         }
                    continue;
                    }
               }
          break;
          }

     if (i >= iCount)
          {
          *ppsz = NULL;
          *piCount = 0;
          return iIndex;
          }

     j = i;
     ch = ((unsigned char *)psz)[iIndex + i++];

     // quoted strings are handled differently
     if (ch == '"' || ch == '\'')
          {
          qu = ch; // take note of the closing quote style

          // scan past quoted string, skipping escapes
          for (; i < iCount; i++)
               {
               ch = ((unsigned char *)psz)[iIndex + i];
               if (ch == qu)
                    {
                    i++; // skip the closing quote
                    break; // end of string encountered
                    }
               if (ch == '\\')
                    {
                    i++; // skip a possible quote if escaped
                    if (i >= iCount)
                         {
                         break; // backslash-newline encountered
                         }
                    }
               }

          // string (maybe incomplete), so return start and length to caller
          *ppsz = psz + iIndex + j;
          *piCount = i - j;
          return iIndex + i;
          }

     if ((ch < '0' || ch > '9') &&
               (ch < 'A' || ch > 'Z') &&
               (ch < 'a' || ch > 'z') &&
               ch != '_')
          {
          // single char operator, so return start and length to caller
          *ppsz = psz + iIndex + j;
          *piCount = 1;
          return iIndex + i;
          }

     // scan past as much symbol text as possible
     for (; i < iCount; i++)
          {
          ch = ((unsigned char *)psz)[iIndex + i];
          if ((ch < '0' || ch > '9') &&
                    (ch < 'A' || ch > 'Z') &&
                    (ch < 'a' || ch > 'z') &&
                    ch != '_')
               {
               break;
               }
          }

     // valid symbol, so return start and length to caller
     *ppsz = psz + iIndex + j;
     *piCount = i - j;
     return iIndex + i;
     }

// ----------------------------------------------------------------------------

