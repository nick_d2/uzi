// hyimage.h

// ----------------------------------------------------------------------------

#define FILTER_CUTOFF 2 // cutoff/overlap should be 1/2 for nyquist frequency
#define FILTER_OVERLAP 4 // history size, in slow-samples, for interp/decimate

// ----------------------------------------------------------------------------

/*
typedef struct tagBITMAPFILEHEADER {
        WORD    bfType;
        DWORD   bfSize;
        WORD    bfReserved1;
        WORD    bfReserved2;
        DWORD   bfOffBits;
} BITMAPFILEHEADER, FAR *LPBITMAPFILEHEADER, *PBITMAPFILEHEADER;

typedef struct tagBITMAPINFOHEADER{
        DWORD      biSize;
        LONG       biWidth;
        LONG       biHeight;
        WORD       biPlanes;
        WORD       biBitCount;
        DWORD      biCompression;
        DWORD      biSizeImage;
        LONG       biXPelsPerMeter;
        LONG       biYPelsPerMeter;
        DWORD      biClrUsed;
        DWORD      biClrImportant;
} BITMAPINFOHEADER, FAR *LPBITMAPINFOHEADER, *PBITMAPINFOHEADER;

typedef struct tagRGBQUAD {
        BYTE    rgbBlue;
        BYTE    rgbGreen;
        BYTE    rgbRed;
        BYTE    rgbReserved;
} RGBQUAD;
typedef RGBQUAD FAR* LPRGBQUAD;

typedef struct tagRECT
{
    LONG    left;
    LONG    top;
    LONG    right;
    LONG    bottom;
} RECT, *PRECT, NEAR *NPRECT, FAR *LPRECT;
*/

typedef struct
     {
     FILETAG ft; // ft.pcBase points to WAV_FILE_HEADER followed by data

     BITMAPFILEHEADER *pbfh; // easier than (BITMAPINFOHEADER *) ft.pcBase
     BITMAPINFOHEADER *pbih; // easier than (BITMAPINFOHEADER *) (ft.pcBase+x)
     RGBQUAD *prgbq; // easier than (RGBQUAD *) (ft.pcBase+y), base of palette
     unsigned char *pucSample; // points after BITMAPINFOHEADER and palette

     int iChannels; // max(biBitCount / 8, 1), because usoft is so very lame
     int iBitsChannel; // always biBitcount / iChannels, because usoft is lame
     int iSamplesLine; // generally in bytes, this offset includes wasted part

     int iSampleBase; // usually 0 unless client wants to do advanced looping
     int iSampleLimit; // iSampleAppend or larger if block is of arbitrary size
     int iSamplePosition; // for general use, initialised to 0 for client
     int iSampleAppend; // calculated for client, pbih->biHeight * iSamplesLine

     int iBackground; // dc bias per channel, to be removed for calculations
     int cxHot, cyHot; // reference point on image for client use, or centre

     //RECT rPixelWindow; // set up from the exact pixel image size via pbih
     //RECT rPixelSurface; // same as rPixelWindow, except rRight includes waste
     } BMPTAG;

// ----------------------------------------------------------------------------

int fiBmpAllocate(BMPTAG *pbt, BITMAPINFOHEADER *pbihIn,
                  int cxPixels, int cyPixels);
int fiBmpReadIn(BMPTAG *pbt, char *pszFileName);
int fiBmpWriteOut(BMPTAG *pbt, char *pszFileName);
void BmpFree(BMPTAG *pbt);
int fiBmpCopy(BMPTAG *pbt, BMPTAG *pbtIn);

int fiLinearExpand2D(BMPTAG *pbtOut, BMPTAG *pbtIn,
                     int cxFactor, int cyFactor);

int fiLinearShrink2D(BMPTAG *pbtOut, BMPTAG *pbtIn,
                     int cxFactor, int cyFactor);

int fiSincInterpolate2D(BMPTAG *pbtOut, BMPTAG *pbtIn,
                        int cxInterpolate, int cyInterpolate);

int fiSincDecimate2D(BMPTAG *pbtOut, BMPTAG *pbtIn,
                     int cxDecimate, int cyDecimate);

int fiSincResample2D(BMPTAG *pbtOut, BMPTAG *pbtIn,
                     int cxInterpolate, int cyInterpolate,
                     int cxDecimate, int cyDecimate);

int fiMakeSincFilter2D(BMPTAG *pbtOut, BMPTAG *pbtIn,
                       int cxSize, int cySize, int cxCutoff, int cyCutoff);

// ----------------------------------------------------------------------------

