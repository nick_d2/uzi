// hycache.h

#ifndef _INC_HYCACHE
#define _INC_HYCACHE

// ----------------------------------------------------------------------------

typedef struct
{
     int iDirty; // 0 = free, 1 = unnamed, 2 = clean, 3 = dirty
     MEMTAG mt; // smallish memory block containing the filename
     FILETAG ft; // potentially large memory block containing the data
} CACHETAG;

// ----------------------------------------------------------------------------

void CacheEntrySetup(LISTTAG *plt, int iItemCount);
void CacheExitCleanup(LISTTAG *plt);

void CacheExitFlush(LISTTAG *plt, int iDirtiness);
int fiCacheFlush(LISTTAG *plt, int iItem, int iDirtiness);

int fiCacheAllocate(LISTTAG *plt, int *piItem, int iSize);
int fiCacheFree(LISTTAG *plt, int iItem);

int fiCacheSearch(LISTTAG *plt, int *piItem, char *pszName);
int fiCacheReadIn(LISTTAG *plt, int *piItem, char *pszName);
int fiCacheWriteOut(LISTTAG *plt, int iItem, char *pszName);

// ----------------------------------------------------------------------------

inline CACHETAG *pctCacheItem(LISTTAG *plt, int iItem)
     {
     // return pointer to start of item, casted to cache item
     return (CACHETAG *) pcListItem(plt, iItem);
     }

inline MEMTAG *pmtCacheItem(LISTTAG *plt, int iItem)
     {
     // return pointer to memory block handle within item, no casting needed
     return &pctCacheItem(plt, iItem)->mt;
     }

inline FILETAG *pftCacheItem(LISTTAG *plt, int iItem)
     {
     // return pointer to file tag within item, no casting needed
     return &pctCacheItem(plt, iItem)->ft;
     }

// ----------------------------------------------------------------------------
// handy functions for the client, not important to the interface:

#ifdef _INC_STDIO
inline void CacheAllocate(LISTTAG *plt, int *piItem, int iSize)
     {
     if (fiCacheAllocate(plt, piItem, iSize) == FALSE)
          {
          printf("CacheAllocate: "
                 "Could not allocate cache 0x%08x size 0x%08x\n", plt, iSize);
          exit(1);
          }

#if DEBUG
     printf("CacheAllocate: "
            "Allocated cache 0x%08x item %d size 0x%08x\n",
            plt, *piItem, iSize);
#endif
     }

inline void CacheFree(LISTTAG *plt, int iItem)
     {
     if (fiCacheFree(plt, iItem) == FALSE)
          {
          printf("CacheFree: Could not free cache 0x%08x item %d\n",
                 plt, iItem);
          exit(1);
          }

#if DEBUG
     printf("CacheFree: Freed cache 0x%08x item %d\n", plt, iItem);
#endif
     }

inline void CacheFlush(LISTTAG *plt, int iItem, int iDirtiness)
     {
     if (fiCacheFlush(plt, iItem, iDirtiness) == FALSE)
          {
          printf("CacheFlush: Could not flush cache 0x%08x item %d\n",
                 plt, iItem);
          exit(1);
          }

//     printf(
//#if DEBUG
//            "CacheFlush: "
//#endif
//            "Flushed "
//#if DEBUG
//            "cache 0x%08x "
//#endif
//            "item %d, file %s size 0x%08x\n",
//#if DEBUG
//            plt,
//#endif
//            iItem,
//            pmtCacheItem(plt, iItem)->pcBase,
//            iFileSize(pftCacheItem(plt, iItem)));

#if DEBUG
     printf("CacheFlush: Flushed cache 0x%08x item %d\n", plt, iItem);
#endif
     }

inline void CacheSearch(LISTTAG *plt, int *piItem, char *pszName)
     {
     if (fiCacheSearch(plt, piItem, pszName) == FALSE)
          {
          printf("CacheSearch: Could not find cache 0x%08x, file %s\n",
                 plt, pszName);
          exit(1);
          }

#if DEBUG
     printf(
#if 0 /*DEBUG*/
            "CacheSearch: "
#endif
	    "Found "
#if 0 /*DEBUG*/
            "cache 0x%08x "
#endif
            "item %d, file %s size 0x%08x\n",
#if 0 /*DEBUG*/
            plt,
#endif
            *piItem, pszName, iFileSize(pftCacheItem(plt, *piItem)));
#endif
     }

inline void CacheReadIn(LISTTAG *plt, int *piItem, char *pszName)
     {
     if (fiCacheReadIn(plt, piItem, pszName) == FALSE)
          {
          printf("CacheReadIn: Could not read cache 0x%08x, file %s\n",
                 plt, pszName);
          exit(1);
          }

#if DEBUG
     printf(
#if 0 /*DEBUG*/
            "CacheReadIn: "
#endif
	    "Loaded "
#if 0 /*DEBUG*/
            "cache 0x%08x "
#endif
            "item %d, file %s size 0x%08x\n",
#if 0 /*DEBUG*/
            plt,
#endif
            *piItem, pszName, iFileSize(pftCacheItem(plt, *piItem)));
#endif
     }

inline void CacheWriteOut(LISTTAG *plt, int iItem, char *pszName)
     {
     if (fiCacheWriteOut(plt, iItem, pszName) == FALSE)
          {
          printf("CacheWriteOut: "
                 "Could not write cache 0x%08x item %d, file %s\n",
                 plt, iItem, pszName);
          exit(1);
          }

#if DEBUG
     printf(
#if 0 /*DEBUG*/
            "CacheWriteOut: "
#endif
            "Cached "
#if 0 /*DEBUG*/
            "cache 0x%08x "
#endif
            "item %d, file %s size 0x%08x\n",
#if 0 /*DEBUG*/
            plt,
#endif
            iItem, pszName, iFileSize(pftCacheItem(plt, iItem)));
#endif
     }
#endif

// ----------------------------------------------------------------------------

#endif

