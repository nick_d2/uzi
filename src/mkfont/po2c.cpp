// po2c.cpp by Nick for Hytech Resource Manager system

// ----------------------------------------------------------------------------

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>
#pragma hdrstop

#ifndef DEBUG
#define DEBUG 0
#endif

#include "hymem.h"
#include "hyfs.h"
#include "hyfile.h"
#include "hylist.h"
#include "hycache.h"
#include "hystring.h"
#include "po2c.h"

//#include "reslib.h"

// ----------------------------------------------------------------------------
// local preprocessor definitions

#define LINE_MAX	0x200
#define PATH_MAX	0x200

#define OUTPUT_BUFFER	0x100000 // to create output files of up to 1 Megabyte
#define CACHE_MAX	0x200 //0 // the maximum number of files to operate on

#define ARGUMENT_MAX	0x100 // the maximum number of arguments per command
#define MESS_MAX	0x2000 // the maximum number of msgs in the output file
#define MESS_SIZE	0x2000 // the maximum size of a "po" formatted message

#define SYM_SIZE	0x1f // the assumed maximum symbol length for linking

// ----------------------------------------------------------------------------
// local function prototypes

#ifdef STANDALONE
int main(int argc, char **argv);
#endif

int iPo2C(LISTTAG *pltCache, int argc, char **argv);
static int fiInterpretPo2C(LISTTAG *plt, FILETAG *pftIn);
static int fiProcessPo2C(FILETAG *pftWork);

// ----------------------------------------------------------------------------
// global variables

LISTTAG ltMess; // list of type MESSTAG
static FILETAG *pftWork; // to modify 'C' source files in place

// ----------------------------------------------------------------------------

#ifdef STANDALONE
int main(int argc, char **argv)
	{
	int i;
        LISTTAG ltCache;

	// set up the cache to track intermediate memory files
        CacheEntrySetup(&ltCache, CACHE_MAX);

	// allocate message list to collect information about messages
	ListAllocate(&ltMess, MESS_MAX, sizeof(MESSTAG));

	// run the real program, passing in the cache descriptor
	i = iPo2C(&ltCache, argc, argv);

	// junk the internal message list buffer
	ListFree(&ltMess);

	// it just goes on and on my friend
	CacheExitFlush(&ltCache, 3); // force writing of intermediate files
	CacheExitCleanup(&ltCache); // deallocate memory, though it's redundant

	return i;
	}
#endif

// ----------------------------------------------------------------------------

#if 0
int fiUnescAll(void)
	{
	MESSTAG *pmt;
	FILETAG *pft;

	// set up one item of lookahead in message table
	ltMess.iPosition = 0;
	pmt = (MESSTAG *) pcListCanGet(&ltMess);

	// tag one message at a time (including any dupes)
	while (pmt)
		{
		pft = &pmt->ftOut;
		FileAllocate(pft, MESS_SIZE);

		pmt->ftIn.pcPosition = pmt->ftIn.pcBase;
		if (fiUnesc(pft, &pmt->ftIn, NULL, 0) == FALSE)
			{
			return FALSE;
			}
		FileResize(pft, iFileSize(pft));

		pmt = (MESSTAG *) pcListCanGet(&ltMess);
		}

	return TRUE;
	}
#endif

int fiSort(void)
	{
	// sort the message information collected
	qsort(ltMess.pcBase, ltMess.iAppend, sizeof(MESSTAG), iMessCompare);

	return TRUE;
	}

static int iMessCompare(const void *pvLeft, const void *pvRight)
	{
	int i, j;

	// apply an artifical prefix to the sort order for merging
	i = ((MESSTAG *)pvLeft)->iPrefix;
	j = ((MESSTAG *)pvRight)->iPrefix;
	if (i < j)
		{
		return -1;
		}
	if (i > j)
		{
		return 1;
		}

	// both have same artifical prefix, need to compare them
	i = iStringCompare(((MESSTAG *) pvLeft)->ftOut.pcBase,
			      iFileSize(&((MESSTAG *) pvLeft)->ftOut),
			      ((MESSTAG *) pvRight)->ftOut.pcBase,
			      iFileSize(&((MESSTAG *) pvRight)->ftOut));
	if (i)
		{
		return i;
		}

	// apply an artifical suffix to the sort order for merging
	i = ((MESSTAG *)pvLeft)->iSuffix;
	j = ((MESSTAG *)pvRight)->iSuffix;
	if (i < j)
		{
		return -1;
		}
	if (i > j)
		{
		return 1;
		}

	// both prefixes, both strings, and both suffixes identical
	return 0;
	}

int fiUniq(void)
	{
	int iAppend;
	MESSTAG *pmt, *pmtNext, *pmtOut;

	// set up one item of lookahead in message table
	ltMess.iPosition = 0;
	pmt = (MESSTAG *) pcListCanGet(&ltMess);

	// advance one message at a time, squashing duplicates
	iAppend = 0;
	pmtOut = pmt; // the message list will be manipulated in-place
	while (pmt)
		{
		iAppend++;
		memcpy(pmtOut++, pmt, sizeof(MESSTAG));

		do
			{
			pmtNext = (MESSTAG *) pcListCanGet(&ltMess);
			}
		while (pmtNext && fiMessEqual(pmt, pmtNext));

		pmt = pmtNext;
		}

	// indicate how many list items survived
	ltMess.iAppend = iAppend;

	return TRUE;
	}

static int fiMessEqual(const void *pvLeft, const void *pvRight)
	{
	// note, this doesn't use the iPrefix or iSuffix fields,
	// this is intentional - it means we can use fiSort() to
	// put the messages in some strange order, then fiUniq()
	// to delete duplicates, then fiSort() to original order

	return fiStringEqual(((MESSTAG *) pvLeft)->ftOut.pcBase,
			     iFileSize(&((MESSTAG *) pvLeft)->ftOut),
			     ((MESSTAG *) pvRight)->ftOut.pcBase,
			     iFileSize(&((MESSTAG *) pvRight)->ftOut));
	}

// ----------------------------------------------------------------------------

int fiSuck(FILETAG *pft, FILETAG *pftIn, char *pcTerm, int iSizeTerm)
	{
	int i, j;
	char *pc;
	char *pcArg;
	int iSizeArg;

	// convert pointer to keyword into file position
	i = pftIn->pcPosition - pftIn->pcBase;
	j = i;

	pc = pft->pcAppend;
	i = iStringTokenise(i, pftIn->pcBase, iFileSize(pftIn),
			       &pcArg, &iSizeArg);
	while (iSizeArg)
		{
		if (iSizeTerm && fiStringEqual(pcArg, iSizeArg,
					       pcTerm, iSizeTerm))
			{
			break;
			}

		if (pc + iSizeArg + 2 > pft->pcLimit) /* 1 for UNIX! */
			{
			pftIn->pcPosition = pftIn->pcBase + j;
			return FALSE;
			}

		memcpy(pc, pcArg, iSizeArg);
		pc += iSizeArg;
		*pc++ = '\r'; /* remove this for UNIX! */
		*pc++ = '\n';

		j = i;
		i = iStringTokenise(i, pftIn->pcBase, iFileSize(pftIn),
				       &pcArg, &iSizeArg);
		}

	pftIn->pcPosition = pftIn->pcBase + j;
	pft->pcAppend = pc;

	return TRUE;
	}

int fiUnesc(FILETAG *pft, FILETAG *pftIn, char *pcTerm, int iSizeTerm)
	{
	int i, j, k, l, ch, dig;
	char *pc;
	char *pcArg;
	int iSizeArg;

	// convert pointer to keyword into file position
	i = pftIn->pcPosition - pftIn->pcBase;
	j = i;

	pc = pft->pcAppend;
	i = iStringTokenise(i, pftIn->pcBase, iFileSize(pftIn),
			       &pcArg, &iSizeArg);
	while (iSizeArg)
		{
// fputs("--> ", stdout);
// fwrite(pcArg, iSizeArg, 1, stdout);
// fputs("\n", stdout);
// fflush(stdout);

		if (iSizeTerm && fiStringEqual(pcArg, iSizeArg,
					       pcTerm, iSizeTerm))
			{
			break;
			}

		if (pcArg[0] != '\"')
			{
			pftIn->pcPosition = pftIn->pcBase + j;
			return FALSE;
			}

		for (k = 1; k < iSizeArg; k++)
			{
			ch = pcArg[k];
			if (ch == '\"')
				{
				break; // unescaped closing quote
				}
			if (ch == '\\' && (k + 1) < iSizeArg)
				{
				ch = pcArg[++k];
				switch (ch)
					{
				case 'b':
					ch = '\b';
					break;
				case 'n':
					ch = '\n';
					break;
				case 'r':
					ch = '\r';
					break;
				case 't':
					ch = '\t';
					break;
				case 'x':
					ch = 0;
					for (l = 0; l < 2 && (k + 1) < iSizeArg; l++)
						{
						switch (pcArg[k + 1])
							{
						case '0':
						case '1':
						case '2':
						case '3':
						case '4':
						case '5':
						case '6':
						case '7':
						case '8':
						case '9':
							ch = (ch << 4) | (pcArg[++k] - '0');
							break;
						case 'A':
						case 'B':
						case 'C':
						case 'D':
						case 'E':
						case 'F':
							ch = (ch << 4) | (pcArg[++k] + 0xa - 'A');
							break;
						case 'a':
						case 'b':
						case 'c':
						case 'd':
						case 'e':
						case 'f':
							ch = (ch << 4) | (pcArg[++k] + 0xa - 'a');
							break;
							}
						}
					break;
					}
				}

			if (pc + 1 > pft->pcLimit)
				{
				pftIn->pcPosition = pftIn->pcBase + j;
				return FALSE;
				}

			*pc++ = ch;
			}

		j = i;
		i = iStringTokenise(i, pftIn->pcBase, iFileSize(pftIn),
				       &pcArg, &iSizeArg);
		}

	pftIn->pcPosition = pftIn->pcBase + j;
	pft->pcAppend = pc;

	return TRUE;
	}

// ----------------------------------------------------------------------------

int iPo2C(LISTTAG *pltCache, int argc, char **argv)
	{
	int i;

	int iCachePo;
	FILETAG *pftPo;
	char *pszPoFileName;

	int iCacheWork;
	FILETAG *pftWork;
	char *pszInFileName;

	char *pszOutFileName;
	char szOutFileName[PATH_MAX];

	pszPoFileName = NULL;
	pszInFileName = NULL;
	pszOutFileName = NULL;

	if (argc > 1)
		{
		pszPoFileName = argv[1];
		}

	if (argc > 2)
		{
		pszInFileName = argv[2];
		}

	if (argc > 3)
		{
		pszOutFileName = argv[3];
		}

	if (pszPoFileName == NULL || pszInFileName == NULL)
		{
		printf("usage: po2c infile.po infile.c [outfile.l]\n");
		exit(1);
		}

	if (pszOutFileName == NULL)
		{
		pszOutFileName = szOutFileName;
		strcpy(pszOutFileName, pszInFileName);

		i = strlen(pszOutFileName);
		while (i--)
			{
			if (pszOutFileName[i] == ':' ||
					pszOutFileName[i] == '/' ||
					pszOutFileName[i] == '\\')
				{
				break; /* no extension, so don't strip it */
				}
			if (pszOutFileName[i] == '.')
				{
				pszOutFileName[i] = 0; /* strip dot and extension */
				break; /* ready to concatenate our extension */
				}
			}

		strcat(pszOutFileName, ".l");
		}

	if (strcmp(pszInFileName, pszOutFileName) == 0)
		{
		printf("Input and output filenames identical\n");
		exit(1);
		}

	// read the input po file entirely to a malloc'd block
	CacheReadIn(pltCache, &iCachePo, pszPoFileName);
	pftPo = pftCacheItem(pltCache, iCachePo);

	// interpret the "msgstr...msgid" syntax to message list
	if (fiInterpretPo2C(&ltMess, pftPo) == FALSE)
		{
		exit(1);
		}

	// do some conversion steps on the information collected,
	// not really necessary, but would allow a binary search
	if (fiSort() == FALSE)
		{
		exit(1);
		}

	if (fiUniq() == FALSE)
		{
		exit(1);
		}

	// read the input c file entirely to a malloc'd block
	CacheReadIn(pltCache, &iCacheWork, pszInFileName);
	pftWork = pftCacheItem(pltCache, iCacheWork);

	// append esc plus character set data to output buffer
	if (fiProcess(fiProcessPo2C, pftWork) == FALSE)
		{
		exit(1);
		}

	// ready to write the output we found
	CacheWriteOut(pltCache, iCacheWork, pszOutFileName);
#if 0
	pctCacheItem(pltCache, iCacheWork)->iDirty = 4; // extra dirty
#else
	CacheFlush(pltCache, iCacheWork, 3); // return its memory to pool
// note: this defeats the point of caching completely, revisit later:
	CacheFree(pltCache, iCacheWork); // return its memory to pool
#endif

	// it's the song that doesn't end
	return 0;
	}

// ----------------------------------------------------------------------------

static int fiInterpretPo2C(LISTTAG *plt, FILETAG *pftIn)
	{
	int i, iSize, iSizeArg;
	char *pcArg, *pcKeyword;
	MESSTAG *pmt;

	// interpret input, looking for "msgid" and "msgstr" tokens
	i = iStringTokenise(0, pftIn->pcBase, iFileSize(pftIn),
			       &pcArg, &iSizeArg);
	while (iSizeArg)
		{
		// create a new zeroed list entry to hold 2 strings
		pmt = (MESSTAG *) pcListPut(plt);

		// check for the keyword introducing the 1st string
		if (fiStringEqual(pcArg, iSizeArg, "msgid", 5) == FALSE)
			{
			fputs("Syntax error at token:\n", stderr);
			fwrite(pcArg, iSizeArg, 1, stderr);
			fputs("\n", stderr);
			return FALSE;
			}

		// parse and concatenate the individual 'C' strings
		FileAllocate(&pmt->ftOut, MESS_SIZE);
		pftIn->pcPosition = pftIn->pcBase + i;
		if (fiUnesc(&pmt->ftOut, pftIn, "msgstr", 6) == FALSE)
			{
			i = iStringTokenise(i, pftIn->pcBase, iFileSize(pftIn),
					       &pcArg, &iSizeArg);
			fputs("Msgid too long at token:\n", stderr);
			fwrite(pcArg, iSizeArg, 1, stderr);
			fputs("\n", stderr);
			return FALSE;
			}
		i = pftIn->pcPosition - pftIn->pcBase;
		FileResize(&pmt->ftOut, iFileSize(&pmt->ftOut));

		// check for the keyword introducing the 2nd string
		i = iStringTokenise(i, pftIn->pcBase, iFileSize(pftIn),
				       &pcArg, &iSizeArg);
		if (fiStringEqual(pcArg, iSizeArg, "msgstr", 6) == FALSE)
			{
			fputs("Syntax error at token:\n", stderr);
			fwrite(pcArg, iSizeArg, 1, stderr);
			fputs("\n", stderr);
			return FALSE;
			}

		// collect (without parsing) the replacement text
		FileAllocate(&pmt->ftIn, MESS_SIZE);
		pftIn->pcPosition = pftIn->pcBase + i;
		if (fiSuck(&pmt->ftIn, pftIn, "msgid", 5) == FALSE)
			{
			i = iStringTokenise(i, pftIn->pcBase, iFileSize(pftIn),
					       &pcArg, &iSizeArg);
			fputs("Msgstr too long at token:\n", stderr);
			fwrite(pcArg, iSizeArg, 1, stderr);
			fputs("\n", stderr);
			return FALSE;
			}
		i = pftIn->pcPosition - pftIn->pcBase;
		FileResize(&pmt->ftIn, iFileSize(&pmt->ftIn));

		// get the next token, it must be "msgid" or EOF
		i = iStringTokenise(i, pftIn->pcBase, iFileSize(pftIn),
				       &pcArg, &iSizeArg);
		}

	return TRUE;
	}

// ----------------------------------------------------------------------------

int fiProcess(int (*pffiHandler)(FILETAG *pftWork), FILETAG *pftWork)
	{
	int i;
	int iSize, iSizeArg;
	char *pcArg, *pcKeyword;

	// parse the user's 'C' source code file using tokens
	pcKeyword = NULL;
	i = iStringTokenise(0, pftWork->pcBase, iFileSize(pftWork),
			       &pcArg, &iSizeArg);
	while (iSizeArg)
		{
// fwrite(pcArg, iSizeArg, 1, stdout);
// fputs("\n", stdout);
// fflush(stdout);
		if (pcKeyword)
			{
			if (fiStringEqual(pcArg, iSizeArg, "(", 1))
				{
				pftWork->pcPosition = pcKeyword;
				if (pffiHandler(pftWork))
					{
					pcKeyword = NULL;
					i = pftWork->pcPosition -
					    pftWork->pcBase;
					goto mycontinue;
					}
				}
			pcKeyword = NULL;
			}

		if (fiStringEqual(pcArg, iSizeArg, "_", 1) ||
		    fiStringEqual(pcArg, iSizeArg, "N_", 2))
			{
			pcKeyword = pcArg;
			}

	mycontinue:
		i = iStringTokenise(i, pftWork->pcBase, iFileSize(pftWork),
				       &pcArg, &iSizeArg);
		}

	return TRUE;
	}

// ----------------------------------------------------------------------------

static int fiProcessPo2C(FILETAG *pftWork)
	{
	int i, j;
	char *pc;
	int iSize, iDelta;
	char *pcArg;
	int iSizeArg;
	MESSTAG *pmt;
	FILETAG ft;

	// convert pointer to keyword into file position
	i = pftWork->pcPosition - pftWork->pcBase;
	j = i;

	// skip the tokens _ and (, we know they will match
	i = iStringTokenise(i, pftWork->pcBase, iFileSize(pftWork),
			       &pcArg, &iSizeArg);
	i = iStringTokenise(i, pftWork->pcBase, iFileSize(pftWork),
			       &pcArg, &iSizeArg);

	// parse and concatenate the individual 'C' strings
	FileAllocate(&ft, MESS_SIZE);
	pftWork->pcPosition = pftWork->pcBase + i;
	if (fiUnesc(&ft, pftWork, ")", 1) == FALSE)
		{
// fputs("FALSE\n", stdout);
// fflush(stdout);
		FileFree(&ft);
		return FALSE;
		}
// fputs("result \"", stdout);
// fwrite(ft.pcBase, iFileSize(&ft), 1, stdout);
// fputs("\"\nTRUE\n", stdout);
// fflush(stdout);
	i = pftWork->pcPosition - pftWork->pcBase;
	FileResize(&ft, iFileSize(&ft));

	// check for the ) keyword closing the expression
	i = iStringTokenise(i, pftWork->pcBase, iFileSize(pftWork),
			       &pcArg, &iSizeArg);
	if (fiStringEqual(pcArg, iSizeArg, ")", 1) == FALSE)
		{
		FileFree(&ft);
		return FALSE;
		}

	// set up one item of lookahead in message table
	ltMess.iPosition = 0;
	pmt = (MESSTAG *) pcListCanGet(&ltMess);

	// perform a linear search against the cached messages
	pc = ft.pcBase; // reuse a variable to speed things up a bit
	iSize = iFileSize(&ft); // reuse a variable to speed things up a bit
	while (pmt)
		{
// fputs("trying \"", stdout);
// fwrite(pmt->ftOut.pcBase, iFileSize(&pmt->ftOut), 1, stdout);
// fputs("\"\n", stdout);
// fflush(stdout);
		if (fiStringEqual(pc, iSize,
				  pmt->ftOut.pcBase, iFileSize(&pmt->ftOut)))
			{
// fputs("BINGO! \"", stdout);
// fwrite(pmt->ftIn.pcBase, iFileSize(&pmt->ftIn), 1, stdout);
// fputs("\"\n", stdout);
// fflush(stdout);
			FileFree(&ft); // ft was only for comparison

			// bingo!  replace the entire 'C' expression
			iSize = iFileSize(&pmt->ftIn) - 2; // trim (UNIX 1!)
			iDelta = iSize + j - i;

			if (pftWork->pcAppend + iDelta > pftWork->pcLimit)
				{
				// need to expand beyond current limit
				FileResize(pftWork,
					   iFileSize(pftWork) + iDelta);
				}

			if (iDelta)
				{
				// need to use reverse copy if expanding
				pc = pftWork->pcBase + i;
				memmove(pftWork->pcBase + j + iSize,
					pc, pftWork->pcAppend - pc);
				pftWork->pcAppend += iDelta;
				}

			memcpy(pftWork->pcBase + j, pmt->ftIn.pcBase, iSize);
			pftWork->pcPosition = pftWork->pcBase + j + iSize;
			return TRUE;
			}

		pmt = (MESSTAG *) pcListCanGet(&ltMess);
		}

	// not a valid message, don't replace it - the 'C' compiler
	// will notice the _(something) sequence and hopefully barf
	FileFree(&ft);
// fputs("returning FALSE\n", stdout);
// fflush(stdout);
	return FALSE;
	}

// ----------------------------------------------------------------------------

