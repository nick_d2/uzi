// hfmtool.cpp by Nick for Hytech Font Metrics system

// ----------------------------------------------------------------------------

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>
#pragma hdrstop

#ifndef DEBUG
#define DEBUG 0
#endif

#include "hymem.h"
#include "hyfs.h"
#include "hyfile.h"
#include "hylist.h"
#include "hycache.h"
#include "hystring.h"
#include "bmp2txt.h"
#include "txt2chs.h"
#include "chs2cmd.h"

#include "hfmlib.h"

// ----------------------------------------------------------------------------
// local preprocessor definitions

#define LINE_MAX	0x200
//#define PATH_MAX	0x200

#define OUTPUT_BUFFER	0x10000 // to create output files of up to 64 kbytes
#define CACHE_MAX	0x100 // the maximum number of files to operate on

#define ARGUMENT_MAX	0x100 // the maximum number of arguments per command
#define FONT_MAX	0x100 // the maximum number of fonts in the output file

// ----------------------------------------------------------------------------
// local struct and type definitions

typedef struct
	{
	int iDevice;
	int iSlot;

	int ixTotal;
	int iyTotal;
	int ixSpace;
	int iySpace;

	char cWidth[0x100];
	} SLOTTAG;

typedef struct
	{
	int iDevice;
	int iFamily;
	int iHeight;
	int iStyle;

	int iSlot;
	} INFOTAG;

typedef struct
	{
	char *pszVerb;
	int (*pfiHandler)(LISTTAG *pltCache, int argc, char **argv);
	} VERBTAG;

// ----------------------------------------------------------------------------
// local function prototypes

int main(int argc, char **argv);

static int fiExecute(LISTTAG *pltCache, FILETAG *ftIn);
static int fiConstruct(LISTTAG *pltCache, FILETAG *ftOut);
static int iCompareInfo(const void *pvLeft, const void *pvRight);
static int iCompareSlot(const void *pvLeft, const void *pvRight);

static int iChs2Hfm(LISTTAG *pltCache, int argc, char **argv);
static int fiProcess(SLOTTAG *pst, INFOTAG *pit, FILETAG *pftIn);

// ----------------------------------------------------------------------------
// global variables

LISTTAG ltSlot; // list of type SLOTTAG
LISTTAG ltInfo; // list of type INFOTAG

VERBTAG vtVerb[] =
	{
	{ "bmp2txt", iBmp2Txt },
	{ "txt2chs", iTxt2Chs },
	{ "chs2cmd", iChs2Cmd },
	{ "chs2hfm", iChs2Hfm }
	};

#define VERBS (sizeof(vtVerb) / sizeof(VERBTAG))

#if DEBUG
int fiDebug; // this one is really accessible to all modules
#endif

// ----------------------------------------------------------------------------

int main(int argc, char **argv)
	{
	int i;
        LISTTAG ltCache;

	int iCacheIn;
        FILETAG *pftIn;
	char *pszInFileName;

	int iCacheOut;
        FILETAG *pftOut;
	char *pszOutFileName;
	char szOutFileName[LINE_MAX+1];

	pszInFileName = NULL;
	pszOutFileName = NULL;

	if (argc > 1)
		{
		pszInFileName = argv[1];
		}

#if DEBUG
	fiDebug = 0;
#endif
	if (argc > 2)
		{
#if DEBUG
		i = atoi(argv[2]);
		if (i < 1 || i > 0xff)
			{
#endif
			pszOutFileName = argv[2];
#if DEBUG
			if (argc > 3)
				{
				fiDebug = atoi(argv[3]);
				}
			}
		else
			{
			fiDebug = i;
			}
#endif
		}

	if (pszInFileName == NULL)
		{
		printf("usage: hfmtool infile.txt [outfile.hfm]"
#if DEBUG
		       " [debuglevel]"
#endif
		       "\n");
		exit(1);
		}

	if (pszOutFileName == NULL)
		{
		pszOutFileName = szOutFileName;
		strcpy(pszOutFileName, pszInFileName);

		i = strlen(pszOutFileName);
		while (i--)
			{
			if (pszOutFileName[i] == '\\')
				{
				break; /* no extension, so don't strip it */
				}
			if (pszOutFileName[i] == '.')
				{
				pszOutFileName[i] = 0; /* strip dot and extension */
				break; /* ready to concatenate our extension */
				}
			}

		strcat(pszOutFileName, ".hfm");
		}

	if (!strcmp(pszInFileName, pszOutFileName))
		{
		printf("Input and output filenames identical\n");
		exit(1);
		}

	// set up the cache to track intermediate memory files
        CacheEntrySetup(&ltCache, CACHE_MAX); // to handle CACHE_MAX files

	// allocate font list to collect information about fonts
	ListAllocate(&ltSlot, FONT_MAX, sizeof(SLOTTAG));
	ListAllocate(&ltInfo, FONT_MAX, sizeof(INFOTAG));

	// read the input txt file entirely to a malloc'd block
	CacheReadIn(&ltCache, &iCacheIn, pszInFileName);
        pftIn = pftCacheItem(&ltCache, iCacheIn);

	// interpret commands and construct intermediate files
	if (fiExecute(&ltCache, pftIn) == FALSE)
		{
		exit(1);
		}

	// prepare an output buffer for font metric data
	CacheAllocate(&ltCache, &iCacheOut, OUTPUT_BUFFER);
        pftOut = pftCacheItem(&ltCache, iCacheOut);

	// construct output based on the font information collected
	if (fiConstruct(&ltCache, pftOut) == FALSE)
		{
		exit(1);
		}

	// ready to write the output we found
	CacheWriteOut(&ltCache, iCacheOut, pszOutFileName);
#if 1
	pctCacheItem(&ltCache, iCacheOut)->iDirty = 4; // extra dirty
#else
	CacheFlush(&ltCache, iCacheOut, 3); // force immediate writing
#endif

	// junk the internal font list buffer
	ListFree(&ltInfo);
	ListFree(&ltSlot);

	// it just goes on and on my friend
#if DEBUG
	CacheExitFlush(&ltCache, 3); // force writing of intermediate files
#else
	CacheExitFlush(&ltCache, 4); // force writing of output files only
#endif
	CacheExitCleanup(&ltCache); // deallocate memory, though it's redundant

	return 0;
	}

// ----------------------------------------------------------------------------

static int fiExecute(LISTTAG *pltCache, FILETAG *pftIn)
	{
	int i;
	char sz[LINE_MAX+1];
	int argc;
	char *argv[ARGUMENT_MAX];
	hfm_t *hfm;
	int iSize, iSizeArg;
	char *pcArg;

	// execute the user's script file (similar to unix shell)
	while (fiFileGetLine(pftIn, sz, sizeof(sz) - 1, &iSize))
		{
		argc = 0;

		i = iStringIsolate(0, sz, iSize, &pcArg, &iSizeArg);
		while (iSizeArg && argc < ARGUMENT_MAX)
			{
			argv[argc++] = pcArg; // stash reference to string
			pcArg[iSizeArg] = 0; // add null terminator

			i = iStringIsolate(i + 1, sz, iSize,
					          &pcArg, &iSizeArg);
			}

		if (argc < 1 || argv[0][0] == '#')
			{
			continue; // comments and blank file lines ignored
			}

#if DEBUG
		for (i = 0; i < argc; i++)
			{
			printf("Argument %d = \"%s\"\n", i, argv[i]);
			}
#endif

		for (i = 0; i < VERBS; i++)
			{
			if (strcmp(argv[0], vtVerb[i].pszVerb) == 0)
				{
				break;
				}
			}

		if (i >= VERBS)
			{
			printf("Unrecognised command \"%s\"\n", argv[0]);
			return FALSE;
			}

		i = (vtVerb[i].pfiHandler)(pltCache, argc, argv);
#if DEBUG
		printf("Exitcode = %d\n", i);
#endif
		if (i)
			{
			printf("Command \"%s\" reports failure\n", argv[0]);
			return FALSE;
			}
		}

	return TRUE;
	}

// ----------------------------------------------------------------------------

static int fiConstruct(LISTTAG *pltCache, FILETAG *pftOut)
	{
	char *pc;
	int i, j, iDevice, iSlot, iFamily;
	LISTTAG lthfmd; // list of type hfm_device_t
	LISTTAG lthfmsl; // list of type hfm_slot_t
	LISTTAG lthfmf; // list of type hfm_family_t
	LISTTAG lthfmst; // list of type hfm_style_t
	hfm_t *phfm;
	hfm_device_t *phfmd;
	hfm_slot_t *phfmsl;
	hfm_family_t *phfmf;
	hfm_style_t *phfmst;
	SLOTTAG *pst;
	INFOTAG *pit;

	// sort the font information collected
	qsort(ltSlot.pcBase, ltSlot.iAppend, sizeof(SLOTTAG), iCompareSlot);
	qsort(ltInfo.pcBase, ltInfo.iAppend, sizeof(INFOTAG), iCompareInfo);

	// create the Hytech Font Manager header
	pc = pftOut->pcBase;

	phfm = (hfm_t *)pc;
	memset(pc, 0, sizeof(hfm_t));
	pc += sizeof(hfm_t);

	phfm->hfm_magic = HFM_MAGIC;
	phfm->hfm_format = HFM_FORMAT;

	// allocate temporary buffers to be reused several times
	ListAllocate(&lthfmd, FONT_MAX, sizeof(hfm_device_t));
	ListAllocate(&lthfmsl, FONT_MAX, sizeof(hfm_slot_t));
	ListAllocate(&lthfmf, FONT_MAX, sizeof(hfm_family_t));
	ListAllocate(&lthfmst, FONT_MAX, sizeof(hfm_style_t));

	// set up one item of lookahead in slot table
	ltSlot.iPosition = 0;
	pst = (SLOTTAG *) pcListCanGet(&ltSlot);

	// set up one item of lookahead in info table
	ltInfo.iPosition = 0;
	pit = (INFOTAG *) pcListCanGet(&ltInfo);

	// write one device at a time, zeroing missing devices
	iDevice = 0;
	lthfmd.iAppend = 0;
	while (pst || pit)
		{
		phfmd = (hfm_device_t *) pcListPut(&lthfmd);

		// collect one slot at a time, zeroing missing slots
		iSlot = 0;
		lthfmsl.iAppend = 0;
		while (pst && pst->iDevice == iDevice)
			{
			phfmsl = (hfm_slot_t *) pcListPut(&lthfmsl);

			if (pst->iSlot < iSlot)
				{
				printf("Duplicate slot %d\n", iSlot);
				return FALSE;
				}

			if (pst->iSlot == iSlot)
				{
				for (i = 0; i < 0x100; i++)
					{
					if (pst->cWidth[i] != pst->ixTotal)
						{
						break;
						}
					}

				for (j = 0xff; j > i; j--)
					{
					if (pst->cWidth[j] != pst->ixTotal)
						{
						j++;
						break;
						}
					}

				if (j > i)
					{
					j -= i;
					phfmsl->hfmsl_char_first = i;
					phfmsl->hfmsl_char_count = j;
					phfmsl->hfmsl_width_table =
							pc - pftOut->pcBase;
					memcpy(pc, pst->cWidth + i, j);
					pc += j;
					}

				phfmsl->hfmsl_x_total = pst->ixTotal;
				phfmsl->hfmsl_y_total = pst->iyTotal;
				phfmsl->hfmsl_x_space = pst->ixSpace;
				phfmsl->hfmsl_y_space = pst->iySpace;

				pst = (SLOTTAG *) pcListCanGet(&ltSlot);
				}

			iSlot++;
			}

		// write the collected slot information for the device
		if (lthfmsl.iAppend)
			{
			phfmd->hfmd_slot_count = lthfmsl.iAppend;
			phfmd->hfmd_slot_table = pc - pftOut->pcBase;
			j = lthfmsl.iAppend * sizeof(hfm_slot_t);
			memcpy(pc, lthfmsl.pcBase, j);
			pc += j;
			}

		// collect one family at a time, zeroing missing families
		iFamily = 0;
		lthfmf.iAppend = 0;
		while (pit && pit->iDevice == iDevice)
			{
			phfmf = (hfm_family_t *) pcListPut(&lthfmf);

			// collect one height/style at a time, with no gaps
			lthfmst.iAppend = 0;
			while (pit && pit->iDevice == iDevice &&
				      pit->iFamily == iFamily)
				{
				phfmst = (hfm_style_t *) pcListPut(&lthfmst);

				phfmst->hfmst_height = pit->iHeight;
				phfmst->hfmst_style = pit->iStyle;
				phfmst->hfmst_slot = pit->iSlot;

				pit = (INFOTAG *) pcListCanGet(&ltInfo);
				}

			if (lthfmst.iAppend)
				{
				phfmf->hfmf_style_count = lthfmst.iAppend;
				phfmf->hfmf_style_table = pc - pftOut->pcBase;
				j = lthfmst.iAppend * sizeof(hfm_style_t);
				memcpy(pc, lthfmst.pcBase, j);
				pc += j;
				}

			iFamily++;
			}

		// write the collected family information for the device
		if (lthfmf.iAppend)
			{
			phfmd->hfmd_family_count = lthfmf.iAppend;
			phfmd->hfmd_family_table = pc - pftOut->pcBase;
			j = lthfmf.iAppend * sizeof(hfm_family_t);
			memcpy(pc, lthfmf.pcBase, j);
			pc += j;
			}

		iDevice++;
		}

	// write the collected device information
	if (lthfmd.iAppend)
		{
		phfm->hfm_device_count = lthfmd.iAppend;
		phfm->hfm_device_table = pc - pftOut->pcBase;
		j = lthfmd.iAppend * sizeof(hfm_device_t);
		memcpy(pc, lthfmd.pcBase, j);
		pc += j;
		}

	// free temporary buffers
	ListFree(&lthfmd);
	ListFree(&lthfmsl);
	ListFree(&lthfmf);
	ListFree(&lthfmst);

	// set the file length in the header
	phfm->hfm_file_size = pc - pftOut->pcBase;

	// set the file length to be written
	pftOut->pcAppend = pc;

	return TRUE;
	}

static int iCompareSlot(const void *pvLeft, const void *pvRight)
	{
	if (((SLOTTAG *) pvLeft)->iDevice < ((SLOTTAG *) pvRight)->iDevice)
		{
		return -1;
		}

	if (((SLOTTAG *) pvLeft)->iDevice > ((SLOTTAG *) pvRight)->iDevice)
		{
		return 1;
		}

	if (((SLOTTAG *) pvLeft)->iSlot < ((SLOTTAG *) pvRight)->iSlot)
		{
		return -1;
		}

	if (((SLOTTAG *) pvLeft)->iSlot > ((SLOTTAG *) pvRight)->iSlot)
		{
		return 1;
		}

	return 0;
	}

static int iCompareInfo(const void *pvLeft, const void *pvRight)
	{
	if (((INFOTAG *) pvLeft)->iDevice < ((INFOTAG *) pvRight)->iDevice)
		{
		return -1;
		}

	if (((INFOTAG *) pvLeft)->iDevice > ((INFOTAG *) pvRight)->iDevice)
		{
		return 1;
		}

	if (((INFOTAG *) pvLeft)->iFamily < ((INFOTAG *) pvRight)->iFamily)
		{
		return -1;
		}

	if (((INFOTAG *) pvLeft)->iFamily > ((INFOTAG *) pvRight)->iFamily)
		{
		return 1;
		}

	if (((INFOTAG *) pvLeft)->iHeight < ((INFOTAG *) pvRight)->iHeight)
		{
		return -1;
		}

	if (((INFOTAG *) pvLeft)->iHeight > ((INFOTAG *) pvRight)->iHeight)
		{
		return 1;
		}

	if (((INFOTAG *) pvLeft)->iStyle < ((INFOTAG *) pvRight)->iStyle)
		{
		return -1;
		}

	if (((INFOTAG *) pvLeft)->iStyle > ((INFOTAG *) pvRight)->iStyle)
		{
		return 1;
		}

	return 0;
	}

// ----------------------------------------------------------------------------

int iChs2Hfm(LISTTAG *pltCache, int argc, char **argv)
	{
	int i, iDevice, iSlot;
	char *pszFamily, *pszStyle;
	INFOTAG *pit;
	SLOTTAG *pst;

	int iCacheIn;
	FILETAG *pftIn;
	char *pszInFileName;

	if (argc < 6)
		{
		printf("usage: infile.chs device slot family style\n");
		exit(1);
		}

	pszInFileName = argv[1];
	iDevice = atoi(argv[2]);
	iSlot = atoi(argv[3]);
	pszFamily = argv[4];
	pszStyle = argv[5];

	pit = (INFOTAG *) pcListPut(&ltInfo);
	pit->iDevice = iDevice;
	pit->iSlot = iSlot;

	pit->iFamily = hfm_family_string_to_int(pszFamily);
	if (pit->iFamily < 0)
		{
		printf("Family must be one of:\n"
		       "\t" HFMSTR_SANS "\n"
		       "\t" HFMSTR_SERIF "\n");
		exit(1);
		}

	pit->iStyle = hfm_style_string_to_int(pszStyle);
	if (pit->iStyle < 0)
		{
		printf("Style must be one of:\n"
		       "\t" HFMSTR_CONDENSED "\n"
		       "\t" HFMSTR_REGULAR "\n"
		       "\t" HFMSTR_BOLD "\n"
		       "\t" HFMSTR_ITALIC "\n"
		       "\t" HFMSTR_UNDERLINE "\n");
		exit(1);
		}

	pst = (SLOTTAG *) pcListPut(&ltSlot);
	pst->iDevice = iDevice;
	pst->iSlot = iSlot;

	// read the input chs file entirely to a malloc'd block
	CacheReadIn(pltCache, &iCacheIn, pszInFileName);
	pftIn = pftCacheItem(pltCache, iCacheIn);

	// extract font width and height info, and character widths
	if (fiProcess(pst, pit, pftIn) == FALSE)
		{
		return 1;
		}

	return 0;
	}

// ----------------------------------------------------------------------------

static int fiProcess(SLOTTAG *pst, INFOTAG *pit, FILETAG *pftIn)
	{
	unsigned short *pus;
	int i;
	char *pc;

	if (iFileSize(pftIn) < 0x214)
		{
		printf("Input chs file is unexpectedly short\n");
		return FALSE;
		}

	pc = &pftIn->pcBase[0x210];
	pst->ixTotal = *pc++;
	pst->iyTotal = *pc++;
	pst->ixSpace = *pc++;
	pst->iySpace = *pc++;
	memset(pst->cWidth, pst->ixTotal, 0x100);
	pit->iHeight = pst->iyTotal - pst->iySpace;

	pus = (unsigned short *) &pftIn->pcBase[0x10];
	for (i = 0; i < 0x100; i++)
		{
		pc = &pftIn->pcBase[0x10 + *pus++];
		if (*pc & 0x20) // got width for character?
			{
			pst->cWidth[i] = *(pc - 1); // yes, save it
			}
		}

	return TRUE;
	}

// ----------------------------------------------------------------------------

