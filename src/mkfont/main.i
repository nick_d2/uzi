









































































typedef unsigned char uchar;
typedef unsigned int uint;


typedef uchar bool_t;	
typedef uint count_t;	


typedef uint size_t;



typedef int ssize_t;


typedef unsigned int mode_t;






typedef long off_t;



typedef struct s_time {
	uint	t_time;
	uint	t_date;
} dostime_t;

typedef unsigned long time_t;


struct tms {
	time_t	tms_utime;
	time_t	tms_stime;
	time_t	tms_cutime;
	time_t	tms_cstime;
	time_t	tms_etime;	
};




struct utimbuf {
	time_t	actime;
	time_t	modtime;
};



struct stat {			
	uint	st_dev; 	
	uint	st_ino; 	
	mode_t	st_mode;	
	uint	st_nlink;	
	uint	st_uid; 	
	uint	st_gid; 	
	uint	st_rdev;	
	off_t	st_size;	
	time_t	st_atime;	
	time_t	st_mtime;	
	time_t	st_ctime;	
};




typedef struct direct {
	unsigned int d_ino;		
	uchar	d_name[14];	
} direct_t;

























typedef void *va_list;










































struct __stdio_file {
	uchar	*bufpos;	
	uchar	*bufread;	
	uchar	*bufwrite;	
	uchar	*bufstart;	
	uchar	*bufend;	

	int	fd;		
	int	mode;
	char	unbuf[8];	
	struct __stdio_file * next;
};






typedef struct __stdio_file FILE;

extern FILE stdin[1];
extern FILE stdout[1];
extern FILE stderr[1];









extern char *gets (char *);

extern int _putchar (int);
extern int _getchar (void);







extern int setvbuf (FILE*, char*, int, size_t);



extern void setbuffer (FILE*, char*, size_t);


extern int fgetc (FILE*);
extern int fputc (int, FILE*);
extern int ungetc (int, FILE*);

extern int fclose (FILE*);
extern int fflush (FILE*);

extern char *fgets (char*, size_t, FILE*);
extern FILE *__fopen (char*, int, FILE*, char*);











extern int fputs (void *, FILE*);
extern int puts (void *);

extern int fread (void *, size_t, size_t, FILE *);
extern int fwrite (void *, size_t, size_t, FILE *);

extern int fseek (FILE *fp, long offset, int whence);
extern long ftell (FILE *fp);

extern int printf (char*, ...);
extern int fprintf (FILE*, char*, ...);
extern int sprintf (char*, char*, ...);

extern int vprintf (char*, va_list);
extern int vfprintf (FILE*, char*, va_list);
extern int vsprintf (char*, char*, va_list);







int __vprinter (unsigned (*pp)(void *, unsigned, FILE *), FILE *op,
		    char *fmt, va_list ap);
unsigned int __fputter (void *buffer, unsigned int count, FILE *op);
unsigned int __sputter (void *buffer, unsigned int count, FILE *op);


extern int scanf (char*, ...);
extern int fscanf (FILE*, char*, ...);
extern int sscanf (char*, char*, ...);

extern int vscanf (char*, va_list);
extern int vfscanf (FILE*, char*, va_list);
extern int vsscanf (char*, char*, va_list);

extern void perror (char *__s);
extern char *strerror (int __errno);

extern char *tmpnam (char *buf);

extern int rename (char *oldname, char *newname);
extern void rewind (FILE *fp);
extern FILE *popen (char *, char *);
extern int pclose (FILE *);































typedef long clock_t;


struct tm {
	int tm_sec;
	int tm_min;
	int tm_hour;
	int tm_mday;
	int tm_mon;
	int tm_year;
	int tm_wday;
	int tm_yday;
	int tm_isdst;
};

struct timezone {
	int tz_minuteswest;	
	int tz_dsttime; 	
};




extern char *tzname[2];
extern int daylight;
extern long timezone;

extern long clock (void);
extern time_t mktime (struct tm * __tp);
extern long difftime (time_t *__time2, time_t *__time1);

extern time_t time (time_t *tvec);

extern void __tm_conv (struct tm *tmbuf, time_t *t, int offset);
extern void __asctime (char *, struct tm *);
extern char *asctime (struct tm * __tp);
extern char *ctime (time_t * __tp);
extern void tzset (void);

extern struct tm *gmtime (time_t *__tp);
extern struct tm *localtime (time_t * __tp);
extern unsigned long convtime (time_t *time_field);











extern unsigned char __ctype[];












extern int toupper (int);
extern int tolower (int);















































extern void free (void *);
extern void *malloc (size_t);
extern void *realloc (void *, size_t);
extern void *calloc (size_t, size_t);
extern void *alloca (size_t);









typedef struct {
	int	quot;		
	int	rem;		
} div_t;


typedef struct {
	long int quot;		
	long int rem;		
} ldiv_t;





extern void _exit (int);
extern void exit (int);
extern void abort (void);

extern unsigned int sleep (unsigned int seconds);

extern int rand (void);
extern void srand (uint seed);

extern char *__longtoa (unsigned long, char *, int, char, char);
extern char *itoa (int value, char *strP, int radix);
extern char *ultoa (unsigned long value, char *strP, int radix);
extern char *ltoa (long value, char *strP, int radix);

extern int atoi (char *str);
extern long atol (char *strP);

extern char *_itoa (int value);
extern char *_ltoa (long value);
extern char *_ultoa (unsigned long value);

extern char *ultostr (unsigned long value, int radix);
extern char *ltostr (long value, int radix);

extern long strtol (char * nptr, char ** endptr, int base);
extern unsigned long strtoul (char * nptr,
				   char ** endptr, int base);

extern double strtod (char * nptr, char ** endptr);


extern char **environ;
extern char *getenv (char *);
extern int putenv (char *);
extern int setenv (char *, char *, int);
extern void unsetenv (char *);

typedef void (*atexit_t) (int);
typedef void (*onexit_t) (int, void *);
extern int atexit (atexit_t);
extern int on_exit (onexit_t, void *arg);
extern onexit_t __cleanup;

extern char *crypt (char *__key, char *__salt);

typedef int (*cmp_func_t) (void *, void *);

extern int _bsearch;
extern void *bsearch (void *key, void *base, size_t num, size_t size, cmp_func_t cmp);
extern void *lfind (void *key, void *base, size_t *num, size_t size, cmp_func_t cmp);
extern void *lsearch (void *key, void *base, size_t *num, size_t size, cmp_func_t cmp);
extern void *_qbuf;
extern void qsort (void *base, size_t num, size_t size, cmp_func_t cmp);

extern int opterr;
extern int optind;
extern char *optarg;
extern int getopt (int argc, char *argv[], char *optstring);

extern char *getpass(char *prompt);

extern int _argc;
extern char **_argv;












extern size_t strlen (const char * __str);

extern char * strcat (char *, const char *);
extern char * strcpy (char *, const char *);
extern int strcmp (const char *, const char *);

extern char * strncat (char *, const char *, size_t);
extern char * strncpy (char *, const char *, size_t);
extern int strncmp (const char *, const char *, size_t);

extern int stricmp (const char *, const char *);
extern strnicmp (const char *, const char *, size_t);

extern char * strchr (const char *, int);
extern char * strrchr (const char *, int);
extern char * strdup (const char *);


extern void * memcpy (void *, const void *, size_t);
extern void * memccpy (void *, const void *, int, size_t);
extern void * memchr (const void *, int, size_t);
extern void * memset (void *, int, size_t);
extern int memcmp (const void *, const void *, size_t);

extern void * memmove (void *, const void *, size_t);








char *strpbrk (const char *, const char *);
char *strsep (char **, const char *);
char *strstr (const char *, const char *);
char *strtok (char *, const char *);
size_t strcspn (const char *, const char *);
size_t strspn (const char *, const char *);


































typedef long int32;		
typedef unsigned long uint32;	
typedef unsigned short uint16;	
typedef unsigned char byte_t;	
typedef unsigned char uint8;	





































































































int availmem(void);
void *callocw(unsigned nelem,unsigned size);
int32 clock();
int dirps(void);
void free(void *);

int getopt(int argc,char *argv[],char *opts);
void getrand(unsigned char *buf,int len);
int htob(char c);
int htoi(char *);
int readhex(uint8 *,char *,int);
long htol(char *);
char *inbuf(uint16 port,char *buf,uint16 cnt);
uint16 hash_ip(int32 addr);
int istate(void);
void logmsg(int s,char *fmt, ...);
int ilog2(uint16 x);
void *ltop(long);
void *malloc(size_t nb);
void *mallocw(size_t nb);
int memcnt(uint8 *buf,uint8 c,int size);
void memxor(uint8 *,uint8 *,unsigned int);
char *outbuf(uint16 port,char *buf,uint16 cnt);
int32 rdclock(void);
void restore(int);
void rip(char *);
char *smsg(char *msgs[],unsigned nmsgs,unsigned n);
void stktrace(unsigned short dummy_parameter); 
 
char *strdupw(const char *);

int urandom(unsigned int n);
int wildmat(char *s,char *p,char **argv);


























extern int optind;
extern char *optarg;


extern int32 Memthresh;


extern int32 Clock;


extern char Badhost[];
extern char Nospace[];
extern char Notval[];
extern char *Hostname;
extern char Version[];
extern char Whitespace[];


extern char Eol[];


extern char System[];


extern char *Tmpdir;

extern unsigned Nfiles;	
extern unsigned Nsock;	

extern void (*Gcollect[])();



unsigned char inportb(unsigned short port);
void outportb(unsigned short port, unsigned char value);
void enable(void);
void disable(void);
void abyte(char data);
void acrlf(void);
void ahexw(int data);
void amess(char *ptr);
int diag_open(int dev);
int diag_close(int dev);
int diag_write(int dev, const char *buffer, size_t count);









































































































extern unsigned Ibufsize;	
extern int Nibufs;		
extern long Pushdowns;		
extern long Pushalloc;		


struct mbuf {
	struct mbuf *next;	
	struct mbuf *anext;	
	uint16 size;		
	int refcnt;		
	struct mbuf *dup;	
	uint8 *data;		
	uint16 cnt;
};












struct mbuf *alloc_mbuf(uint16 size);
struct mbuf *free_mbuf(struct mbuf **bpp);

struct mbuf *ambufw(uint16 size);
struct mbuf *copy_p(struct mbuf *bp,uint16 cnt);
uint16 dup_p(struct mbuf **hp,struct mbuf *bp,uint16 offset,uint16 cnt);
uint16 extract(struct mbuf *bp,uint16 offset,void *buf,uint16 len);
struct mbuf *free_p(struct mbuf **bpp);
uint16 len_p(struct mbuf *bp);
void trim_mbuf(struct mbuf **bpp,uint16 length);
int write_p(FILE *fp,struct mbuf *bp);

struct mbuf *dequeue(struct mbuf **q);
void enqueue(struct mbuf **q,struct mbuf **bpp);
void free_q(struct mbuf **q);
uint16 len_q(struct mbuf *bp);

struct mbuf *qdata(void *data,uint16 cnt);
uint16 dqdata(struct mbuf *bp,void *buf,unsigned cnt);

void append(struct mbuf **bph,struct mbuf **bpp);
void pushdown(struct mbuf **bpp,void *buf,uint16 size);
uint16 pullup(struct mbuf **bph,void *buf,uint16 cnt);






int pull8(struct mbuf **bpp);       
long pull16(struct mbuf **bpp);	
int32 pull32(struct mbuf **bpp);	

uint16 get16(uint8 *cp);
int32 get32(uint8 *cp);
uint8 *put16(uint8 *cp,uint16 x);
uint8 *put32(uint8 *cp,int32 x);

void iqstat(void);
void refiq(void);
void mbuf_crunch(struct mbuf **bpp);

void mbufsizes(void);
void mbufstat(void);
void mbuf_garbage(int red);
























struct timer {
	struct timer *next;	
	int32 duration;		
	int32 expiration;	
	void (*func)(void *);	
	void *arg;		
	char state;		



};











extern int Tick;
extern void (*Cfunc[])();	


void kalarm(int32 ms);
int ppause(int32 ms);
int32 read_timer(struct timer *t);
void set_timer(struct timer *t,int32 x);
void start_timer(struct timer *t);
void stop_timer(struct timer *timer);
char *tformat(int32 t);


int32 msclock(void);
int32 secclock(void);
int32 usclock(void);




















typedef struct {
	unsigned int	j_sp;
	unsigned int	j_ss; 
	unsigned int	j_bc;
	unsigned int	j_ix;
	unsigned int	j_iy;
	unsigned int	j_ip;
	unsigned int	j_cs; 
}	jmp_buf[1];

void	 longjmp(jmp_buf jmpb, int retval);
int	 setjmp(jmp_buf jmpb);















struct proc {
	struct proc *prev;	
	struct proc *next;

	struct {
		unsigned int suspend:1;		
		unsigned int waiting:1;		
		unsigned int istate:1;		
		unsigned int sset:1;		
		unsigned int freeargs:1;	
	} flags;
	jmp_buf env;		
	jmp_buf sig;		
	int signo;		
	void *event;		
	uint16 *stack;		
	unsigned stksize;	
	char *name;		
	int retval;		
	struct timer alarm;	
	FILE *input;		
	FILE *output;		
	int iarg;		
	void *parg1;		
	void *parg2;		

	void (*startp)(int i, void * p, void *u); 

};
extern struct proc *Waittab[];	
extern struct proc *Rdytab;	
extern struct proc *Curproc;	
extern struct proc *Susptab;	
extern int Stkchk;		
extern int Kdebug;		

struct sigentry {
	void *event;
	int n;
};
struct ksig {
	struct sigentry entry[200	];
	struct sigentry *wp;
	struct sigentry *rp;
	volatile int nentries;	
	int maxentries;
	int32 duksigs;
	int lostsigs;
	int32 ksigs;		
	int32 ksigwakes;	
	int32 ksignops;		
	int32 ksigsqueued;	
	int32 kwaits;		
	int32 kwaitnops;	
	int32 kwaitints;	
};
extern struct ksig Ksig;













void alert(struct proc *pp,int val);
void chname(struct proc *pp,char *newname);
void killproc(struct proc *pp);
void killself(void);



struct proc *mainproc(char *name);

struct proc *newproc(char *name,unsigned int stksize,
	void (*pc)(int,void *,void *),
	int iarg,void *parg1,void *parg2,int freeargs);
void ksignal(void *event,int n);
int kwait(void *event);
void resume(struct proc *pp);
int setsig(int val);
void suspend(struct proc *pp);


void chkstk(void);
void kinit(void);
unsigned phash(void *event);

void psetup(struct proc *pp);



































struct iface;	
struct iftype {
	char *name;		
	int (*send)(struct mbuf **,struct iface *,int32,uint8);
				
	int (*output)(struct iface *,uint8 *,uint8 *,uint16,struct mbuf **);
				
	char *(*format)(char *,uint8 *);
				
	int (*scan)(uint8 *,char *);
				
	int type;		
	int hwalen;		
	void (*rcvf)(struct iface *,struct mbuf **);
				
	int (*addrtest)(struct iface *,struct mbuf *);
				
	void (*trace)(FILE *,struct mbuf **,int);
				
	int (*dinit)(struct iface *,int32,int,char **);
				
	int (*dstat)(struct iface *);
				
};
extern struct iftype Iftypes[];



struct iface {
	struct iface *next;	
	char *name;		

	int32 addr;		
	int32 broadcast;	
	int32 netmask;		

	uint16 mtu;		

	uint16 trace;		






	FILE *trfp;		

	struct iface *forw;	

	struct proc *rxproc;	
	struct proc *txproc;	
	struct proc *supv;	

	struct mbuf *outq;	
	int outlim;		
	int txbusy;		

	void *dstate;		
	int (*dtickle)(struct iface *);
				
	void (*dstatus)(struct iface *);	
				

	
	int dev;		
				
	int32 (*ioctl)(struct iface *,int cmd,int set,int32 val);
				
	int (*iostatus)(struct iface *,int cmd,int32 val);
				
	int (*stop)(struct iface *);
	uint8 *hwaddr;		

	
	void *edv;		
	int xdev;		
	struct iftype *iftype;	

				
	int (*send)(struct mbuf **,struct iface *,int32,uint8);
			
	int (*output)(struct iface *,uint8 *,uint8 *,uint16,struct mbuf **);
			
	int (*raw)(struct iface *,struct mbuf **);
			
	void (*show)(struct iface *);

	int (*discard)(struct iface *,struct mbuf **);
	int (*echo)(struct iface *,struct mbuf **);

	
	int32 ipsndcnt; 	
	int32 rawsndcnt;	
	int32 iprecvcnt;	
	int32 rawrecvcnt;	
	int32 lastsent;		
	int32 lastrecv;		
};
extern struct iface *Ifaces;	
extern struct iface  Loopback;	
extern struct iface  Encap;	


struct qhdr {
	uint8 tos;
	int32 gateway;
};

extern char Noipaddr[];
extern struct mbuf *Hopper;


int bitbucket(struct iface *ifp,struct mbuf **bp);
int if_detach(struct iface *ifp);
struct iface *if_lookup(char *name);
char *if_name(struct iface *ifp,char *comment);
void if_tx(int dev,void *arg1,void *unused);
struct iface *ismyaddr(int32 addr);
void network(int i,void *v1,void *v2);
int nu_send(struct mbuf **bpp,struct iface *ifp,int32 gateway,uint8 tos);
int nu_output(struct iface *,uint8 *,uint8 *,uint16,struct mbuf **);
int setencap(struct iface *ifp,char *mode);


int net_route(struct iface *ifp,struct mbuf **bpp);






































































struct pseudo_header {
	int32 source;		
	int32 dest;		
	char protocol;		
	uint16 length;		
};


struct mib_entry {
	char *name;
	union {
		int32 integer;
	} value;
};













extern char Hashtab[];	


extern struct mib_entry Ip_mib[];

























struct ip {
	int32 source;		
	int32 dest;		
	uint16 length;		
	uint16 id;		
	uint16 offset;		
	uint16 checksum;		

	struct {
		unsigned int congest:1;	
		unsigned int df:1;	
		unsigned int mf:1;	
	} flags;

	uint8 version;		
	uint8 tos;		
	uint8 ttl;		
	uint8 protocol;		
	uint8 optlen;		
	uint8 options[40	];
};





















struct route {
	struct route *prev;	
	struct route *next;
	int32 target;		
	unsigned int bits;	
	int32 gateway;		
	int32 metric;		
	struct iface *iface;	
	struct {
		unsigned int rtprivate:1; 
		unsigned int rttrig:1;	
	} flags;
	struct timer timer;	
	int32 uses;		
};
extern struct route *Routes[32][7		];	
extern struct route R_default;			




struct rt_cache {
	int32 target;
	struct route *route;
};
extern struct rt_cache Rt_cache[];
extern int32 Rtlookups;	
extern int32 Rtchits;		

extern uint16 Id_cntr;		


struct reasm {
	struct reasm *next;	
	struct timer timer;	
	struct frag *fraglist;	
	uint16 length;		
	int32 source;		
	int32 dest;
	uint16 id;
	char protocol;
};


struct frag {
	struct frag *prev;	
	struct frag *next;	
	struct mbuf *buf;	
	uint16 offset;		
	uint16 last;		
};

extern struct reasm *Reasmq;	


struct raw_ip {
	struct raw_ip *next;	

	struct mbuf *rcvq;	
	void (*r_upcall)(struct raw_ip *);
	int protocol;		
	int user;		
};

extern struct raw_ip *Raw_ip;


struct iplink {
	char proto;
	char *name;
	void (*funct)(struct iface *,struct ip *,struct mbuf **,int,int32);
	void (*dump)(FILE *,struct mbuf **,int32,int32,int);
};
extern struct iplink Iplink[];


extern int Tcp_interact[];

extern int Ip_trace;


void ip_garbage(int drastic);
void ip_recv(struct iface *iface,struct ip *ip,struct mbuf **bpp,
	int rxbroadcast, int32 said);
void ipip_recv(struct iface *iface,struct ip *ip,struct mbuf **bp,
	int rxbroadcast,int32 said);
int ip_send(int32 source,int32 dest,char protocol,char tos,char ttl,
	struct mbuf **bpp,uint16 length,uint16 id,char df);
struct raw_ip *raw_ip(int protocol,void (*r_upcall)(struct raw_ip *) );
void del_ip(struct raw_ip *rrp);
void rquench(struct iface *ifp,int drop);


void dumpip(struct iface *iface,struct ip *ip,struct mbuf *bp,int32 spi);


void ipinit(void);
uint16 ip_mtu(int32 addr);
void encap_tx(int dev,void *arg1,void *unused);
int ip_encap(struct mbuf **bpp,struct iface *iface,int32 gateway,uint8 tos);
void ip_proc(struct iface *iface,struct mbuf **bpp);
int ip_route(struct iface *i_iface,struct mbuf **bpp,int rxbroadcast);
int32 locaddr(int32 addr);
void rt_merge(int trace);
struct route *rt_add(int32 target,unsigned int bits,int32 gateway,
	struct iface *iface,int32 metric,int32 ttl,uint8 private);
int rt_drop(int32 target,unsigned int bits);
struct route *rt_lookup(int32 target);
struct route *rt_blookup(int32 target,unsigned int bits);


uint16 cksum(struct pseudo_header *ph,struct mbuf *m,uint16 len);
uint16 eac(int32 sum);
void htonip(struct ip *ip,struct mbuf **data,int cflag);
int ntohip(struct ip *ip,struct mbuf **bpp);


uint16 lcsum(uint16 *wp,uint16 len);


void net_sim(struct mbuf *bp);






































extern int32 Ip_addr;	
extern int Net_error;	
extern char Inet_eol[];



















struct socket {
	int32 address;		
	uint16 port;		
};


struct connection {
	struct socket local;
	struct socket remote;
};

int32 resolve(char *name);
int32 resolve_mx(char *name);
char *resolve_a(int32 ip_address, int shorten);


int32 aton(char *s);
char *inet_ntoa(int32 a);
char *pinet(struct socket *s);



































struct tcp {
	uint16 source;	
	uint16 dest;	
	int32 seq;	
	int32 ack;	
	uint16 wnd;			
	uint16 checksum;		
	uint16 up;			
	uint16 mss;			
	uint8 wsopt;			
	uint32 tsval;			
	uint32 tsecr;			
	struct {
		unsigned int congest:1;	
		unsigned int urg:1;
		unsigned int ack:1;
		unsigned int psh:1;
		unsigned int rst:1;
		unsigned int syn:1;
		unsigned int fin:1;
		unsigned int mss:1;	
		unsigned int wscale:1;	
		unsigned int tstamp:1;	
	} flags;
};











struct reseq {
	struct reseq *next;	
	struct tcp seg;		
	struct mbuf *bp;	
	uint16 length;		
	char tos;		
};

enum tcp_state {
	TCP_CLOSED=1,
	TCP_LISTEN,
	TCP_SYN_SENT,
	TCP_SYN_RECEIVED,
	TCP_ESTABLISHED,
	TCP_FINWAIT1,
	TCP_FINWAIT2,
	TCP_CLOSE_WAIT,
	TCP_LAST_ACK,
	TCP_CLOSING,
	TCP_TIME_WAIT 
};


struct tcb {
	struct tcb *next;	

	struct connection conn;

	enum tcp_state state;	

	char reason;		






	uint8 type;
	uint8 code;

	
	struct {
		int32 una;	
		int32 nxt;	
		int32 ptr;	
		int32 wl1;	
		int32 wl2;	
		int32 wnd;	
		uint16 up;	
		uint8 wind_scale;
	} snd;
	int32 iss;		
	int32 resent;		
	int32 cwind;		
	int32 ssthresh;		
	int dupacks;		

	
	struct {
		int32 nxt;	
		int32 wnd;	
		uint16 up;	
		uint8 wind_scale;
	} rcv;
	int32 last_ack_sent;	
	int32 ts_recent;	

	int32 irs;		
	int32 rerecv;		
	int32 mss;		

	int32 window;		
	int32 limit;		

	void (*r_upcall)(struct tcb *tcb,int32 cnt);
		
	void (*t_upcall)(struct tcb *tcb,int32 cnt);
		
	void (*s_upcall)(struct tcb *tcb,int old,int new);
		
	struct {	
		unsigned int force:1;	
		unsigned int clone:1;	
		unsigned int retran:1;	
		unsigned int active:1;	
		unsigned int synack:1;	
		unsigned int rtt_run:1;	
		unsigned int congest:1;	
		int ts_ok:1;	
		int ws_ok:1;		
	} flags;
	char tos;		
	int backoff;		

	struct mbuf *rcvq;	
	struct mbuf *sndq;	
	int32 rcvcnt;		
	int32 sndcnt;		




	struct reseq *reseq;	
	struct timer timer;	
	int32 rtt_time;		
	int32 rttseq;		
	int32 rttack;		
	int32 srtt;		
	int32 mdev;		
	int32 rtt;		

	int user;		


	int32 quench;		
	int32 unreach;		
	int32 timeouts;		
	int32 lastack;		
	int32 txbw;		
	int32 lastrx;		
	int32 rxbw;		
};

struct tcp_rtt {
	int32 addr;		
	int32 srtt;		
	int32 mdev;		
};
extern struct tcp_rtt Tcp_rtt[];
extern int (*Kicklist[])();


struct tcp_stat {
	uint16 runt;		
	uint16 checksum;		
	uint16 conout;		
	uint16 conin;		
	uint16 resets;		
	uint16 bdcsts;		
};
extern struct mib_entry Tcp_mib[];
















extern struct tcb *Tcbs;
extern char *Tcpstates[];
extern char *Tcpreasons[];


extern int Tcp_tstamps;
extern int32 Tcp_irtt;
extern uint16 Tcp_limit;
extern uint16 Tcp_mss;
extern int Tcp_syndata;
extern int Tcp_trace;
extern uint16 Tcp_window;

void st_tcp(struct tcb *tcb);


void htontcp(struct tcp *tcph,struct mbuf **data,
	int32 ipsrc,int32 ipdest);
int ntohtcp(struct tcp *tcph,struct mbuf **bpp);


void reset(struct ip *ip,struct tcp *seg);
void send_syn(struct tcb *tcb);
void tcp_input(struct iface *iface,struct ip *ip,struct mbuf **bpp,
	int rxbroadcast,int32 said);
void tcp_icmp(int32 icsource,int32 source,int32 dest,
	uint8 type,uint8 code,struct mbuf **bpp);


void close_self(struct tcb *tcb,int reason);
struct tcb *create_tcb(struct connection *conn);
struct tcb *lookup_tcb(struct connection *conn);
void rtt_add(int32 addr,int32 rtt);
struct tcp_rtt *rtt_get(int32 addr);
int seq_ge(int32 x,int32 y);
int seq_gt(int32 x,int32 y);
int seq_le(int32 x,int32 y);
int seq_lt(int32 x,int32 y);
int seq_within(int32 x,int32 low,int32 high);
void settcpstate(struct tcb *tcb,enum tcp_state newstate);
void tcp_garbage(int red);


void tcp_output(struct tcb *tcb);


int32 backoff(int n);
void tcp_timeout(void *p);


int close_tcp(struct tcb *tcb);
int del_tcp(struct tcb *tcb);
int kick(int32 addr);
int kick_tcp(struct tcb *tcb);
struct tcb *open_tcp(struct socket *lsocket,struct socket *fsocket,
	int mode,uint16 window,
	void (*r_upcall)(struct tcb *tcb,int32 cnt),
	void (*t_upcall)(struct tcb *tcb,int32 cnt),
	void (*s_upcall)(struct tcb *tcb,int old,int new),
	int tos,int user);
int32 recv_tcp(struct tcb *tcb,struct mbuf **bpp,int32 cnt);
void reset_all(void);
void reset_tcp(struct tcb *tcb);
long send_tcp(struct tcb *tcb,struct mbuf **bpp);
char *tcp_port(uint16 n);
int tcpval(struct tcb *tcb);






























extern struct mib_entry Udp_mib[];









struct udp {
	uint16 source;	
	uint16 dest;	
	uint16 length;	
	uint16 checksum;	
};






struct udp_cb {
	struct udp_cb *next;
	struct socket socket;	
	void (*r_upcall)(struct iface *iface,struct udp_cb *,int);
				
	struct mbuf *rcvq;	
	int rcvcnt;		
	int user;		
};
extern struct udp_cb *Udps;	




int del_udp(struct udp_cb *up);
struct udp_cb *open_udp(struct socket *lsocket,
	void (*r_upcall)(struct iface *iface,struct udp_cb *,int));
int recv_udp(struct udp_cb *up,struct socket *fsocket,struct mbuf **bp);
int send_udp(struct socket *lsocket,struct socket *fsocket,char tos,
	char ttl,struct mbuf **data,uint16 length,uint16 id,char df);
void udp_input(struct iface *iface,struct ip *ip,struct mbuf **bp,
	int rxbroadcast,int32 said);
void udp_garbage(int drastic);


void udp_icmp(int32 icsource, int32 ipsource,int32 ipdest,
	char ictype,char iccode,struct mbuf **bpp);

void hop_icmp(struct udp_cb *ucb, int32 icsource, int32 ipdest,
	uint16 udpdest, char ictype, char iccode);



int st_udp(struct udp_cb *udp,int n);


void htonudp(struct udp *udp,struct mbuf **data,struct pseudo_header *ph);
int ntohudp(struct udp *udp,struct mbuf **bpp);
uint16 udpcksum(struct mbuf *bp);






























struct sockaddr {
	short sa_family;
	char sa_data[14];
};


struct in_addr {
	unsigned long s_addr;
};


struct sockaddr_in {
	short sin_family;
	unsigned short sin_port;
	struct in_addr sin_addr;
	char sin_zero[8];
};







extern char Ax25_eol[];















extern uint8 Mycall[7	];


extern uint8 Ax25multi[][7	];

extern int Digipeat;
extern int Ax25mbox;







struct sockaddr_ax {
	short sax_family;		
	uint8 ax25_addr[7	];
	char iface[(sizeof(struct sockaddr) - sizeof(short) - 7	)];		
};


struct ax25 {
	uint8 dest[7	];		
	uint8 source[7	];		
	uint8 digis[7	][7	];	
	int ndigis;			
	int nextdigi;			
	enum {
		LAPB_UNKNOWN,
		LAPB_COMMAND,
		LAPB_RESPONSE
	} cmdrsp;			
};


struct ax_route {
	struct ax_route *next;		
	uint8 target[7	];
	uint8 digis[7	][7	];
	int ndigis;
	enum {
		AX_LOCAL,		
		AX_AUTO			
	} type;
};

extern struct ax_route *Ax_routes;
extern struct ax_route Ax_default;














struct lqhdr {
	uint16 version;		

	int32	ip_addr;	
};


struct lqentry {
	uint8 addr[7	];	
	int32 count;		
};





struct lq {
	struct lq *next;
	uint8 addr[7	];	
	struct iface *iface;	
	int32 time;		
	int32 currxcnt;	


};

extern struct lq *Lq;	


struct ld {
	struct ld *next;	
	uint8 addr[7	];
	struct iface *iface;	
	int32 time;		
	int32 currxcnt;	
};

extern struct ld *Ld;	


struct ax_route *ax_add(uint8 *,int,uint8 digis[][7	],int);
int ax_drop(uint8 *);
struct ax_route *ax_lookup(uint8 *);
void ax_recv(struct iface *,struct mbuf **);
int axui_send(struct mbuf **bp,struct iface *iface,int32 gateway,uint8 tos);
int axi_send(struct mbuf **bp,struct iface *iface,int32 gateway,uint8 tos);
int ax_output(struct iface *iface,uint8 *dest,uint8 *source,uint16 pid,
	struct mbuf **data);
int axsend(struct iface *iface,uint8 *dest,uint8 *source,
	int cmdrsp,int ctl,struct mbuf **data);


void htonax25(struct ax25 *hdr,struct mbuf **data);
int ntohax25(struct ax25 *hdr,struct mbuf **bpp);


void getlqentry(struct lqentry *ep,struct mbuf **bpp);
void getlqhdr(struct lqhdr *hp,struct mbuf **bpp);
void logsrc(struct iface *iface,uint8 *addr);
void logdest(struct iface *iface,uint8 *addr);
char *putlqentry(char *cp,uint8 *addr,int32 count);
char *putlqhdr(char *cp,uint16 version,int32 ip_addr);
struct lq *al_lookup(struct iface *ifp,uint8 *addr,int sort);


int addreq(uint8 *a,uint8 *b);
char *pax25(char *e,uint8 *addr);
int setcall(uint8 *out,char *call);

void beac_input(struct iface *iface,uint8 *src,struct mbuf **bpp);














int kiss_free(struct iface *ifp);
int kiss_raw(struct iface *iface,struct mbuf **data);
void kiss_recv(struct iface *iface,struct mbuf **bp);
int kiss_init(struct iface *ifp);
int32 kiss_ioctl(struct iface *iface,int cmd,int set,int32 val);
void kiss_recv(struct iface *iface,struct mbuf **bp);




















struct ether {
	uint8 dest[6];
	uint8 source[6];
	uint16 type;
};



extern uint8 Ether_bdcst[];












char *pether(char *out,uint8 *addr);
int gether(uint8 *out,char *cp);
int enet_send(struct mbuf **bpp,struct iface *iface,int32 gateway,uint8 tos);
int enet_output(struct iface *iface,uint8 dest[],uint8 source[],uint16 type,
	struct mbuf **bpp);
void eproc(struct iface *iface,struct mbuf **bpp);


void htonether(struct ether *ether,struct mbuf **data);
int ntohether(struct ether *ether,struct mbuf **bpp);










































































struct ax25_cb {
	struct ax25_cb *next;		

	struct iface *iface;		

	struct mbuf *txq;		
	struct mbuf *rxasm;		
	struct mbuf *rxq;		

	uint8 local[7	];		
	uint8 remote[7	];

	struct {
		unsigned int rejsent:1;		
		unsigned int remotebusy:1;	
		unsigned int rtt_run:1;		
		unsigned int retrans:1;		
		unsigned int clone:1;		
	} flags;

	uint8 reason;			




	uint8 response;			
	uint8 vs;			
	uint8 vr;			
	uint8 unack;			
	int maxframe;			
	uint16 paclen;			
	uint16 window;			
	enum {
		V1=1,			
		V2			
	} proto;			
	uint16 pthresh;			
	unsigned retries;		
	unsigned n2;			
	enum {
		LAPB_DISCONNECTED=1,
		LAPB_LISTEN,
		LAPB_SETUP,
		LAPB_DISCPENDING,
		LAPB_CONNECTED,
		LAPB_RECOVERY
	} state;			
	struct timer t1;		
	struct timer t3;		
	int32 rtt_time;			
	int rtt_seq;			
	int32 srt;			
	int32 mdev;			

	void (*r_upcall)(struct ax25_cb *,int);	
	void (*t_upcall)(struct ax25_cb *,int);	
	void (*s_upcall)(struct ax25_cb *,int,int);	

	int user;			

	int segremain;			
};

struct axlink {
	int pid;
	void (*funct)(struct iface *,struct ax25_cb *,uint8 *, uint8 *,
	 struct mbuf **,int);
};
extern struct axlink Axlink[];






extern struct ax25_cb Ax25default,*Ax25_cb;
extern char *Ax25states[],*Axreasons[];
extern int32 Axirtt,T3init,Blimit;
extern uint16 N2,Maxframe,Paclen,Pthresh,Axwindow,Axversion;


void st_ax25(struct ax25_cb *axp);


struct ax25_cb *cr_ax25(uint8 *addr);
void del_ax25(struct ax25_cb *axp);
struct ax25_cb *find_ax25(uint8 *);


int ax25val(struct ax25_cb *axp);
int disc_ax25(struct ax25_cb *axp);
int kick_ax25(struct ax25_cb *axp);
struct ax25_cb *open_ax25(struct iface *,uint8 *,uint8 *,
	int,uint16,
	void (*)(struct ax25_cb *,int),
	void (*)(struct ax25_cb *,int),
	void (*)(struct ax25_cb *,int,int),
	int user);
struct mbuf *recv_ax25(struct ax25_cb *axp,uint16 cnt);
int reset_ax25(struct ax25_cb *axp);
int send_ax25(struct ax25_cb *axp,struct mbuf **bp,int pid);


void est_link(struct ax25_cb *axp);
void lapbstate(struct ax25_cb *axp,int s);
int lapb_input(struct ax25_cb *axp,int cmdrsp,struct mbuf **bp);
int lapb_output(struct ax25_cb *axp);
struct mbuf *segmenter(struct mbuf **bp,uint16 ssize);
int sendctl(struct ax25_cb *axp,int cmdrsp,int cmd);
int sendframe(struct ax25_cb *axp,int cmdrsp,int ctl,struct mbuf **data);
void axnl3(struct iface *iface,struct ax25_cb *axp,uint8 *src,
	uint8 *dest,struct mbuf **bp,int mcast);


void pollthem(void *p);
void recover(void *p);


uint16 ftype(int control);
void lapb_garbage(int drastic);


void s_arcall(struct ax25_cb *axp,int cnt);
void s_ascall(struct ax25_cb *axp,int old,int new);
void s_atcall(struct ax25_cb *axp,int cnt);







































				






















struct nr4hdr {
	uint8 opcode ;		
	uint8 yourindex ;	
	uint8 yourid ;		

	union {

		struct {				
			uint8 family ;	
			uint8 proto ;	
		} pid ;

		struct {				
			uint8 myindex ;	
			uint8 myid ;	
			uint8 window ;	
			uint8 user[7	] ;	
			uint8 node[7	] ;	
		} conreq ;

		struct {				
			uint8 myindex ;	
			uint8 myid ;	
			uint8 window ; 	
		} conack ;

		struct {				
			uint8 txseq ;	
			uint8 rxseq ;	
		} info ;

		struct {				
			uint8 rxseq ;	
		} ack ;

	} u ;	

} ;



struct nr4txbuf {
	struct timer tretry ;		
	unsigned retries ;			
	struct mbuf *data ;			
} ;



struct nr4rxbuf {
	uint8 occupied ;	
	struct mbuf *data ; 		
} ;


struct nr4_addr {
	uint8 user[7	];
	uint8 node[7	];
};

struct sockaddr_nr {
	short nr_family;
	struct nr4_addr nr_addr;
};



struct nr4cb {
	unsigned mynum ;			
	unsigned myid ;				
	unsigned yournum ;			
	unsigned yourid ;			
	struct nr4_addr remote ;		
	struct nr4_addr local ;			

	unsigned window ;			

	

	long srtt ;					
	long mdev ;					
	unsigned blevel ;			
	unsigned txmax ;			
								
								
								
								

	

	char clone ;				
	char choked ;				
	char qfull ;				
								
	char naksent ;				

	

	struct nr4txbuf *txbufs ;	
	uint8 nextosend ;	
	uint8 ackxpected ;	
	unsigned nbuffered ;		
	struct mbuf *txq ;			

	

	struct nr4rxbuf *rxbufs ;	
	uint8 rxpected ;	
	uint8 rxpastwin ;	
	struct mbuf *rxq ;			

	

	int state ;					






	int dreason ;				






	

	struct timer tchoke ;		
	struct timer tack ;		

	struct timer tcd ;		
	unsigned cdtries ;		

	void (*r_upcall)(struct nr4cb *,uint16);
					
	void (*t_upcall)(struct nr4cb *,uint16);
					
	void (*s_upcall)(struct nr4cb *,int,int);
					
	int user ;			
} ;



struct nr4circp {
	uint8 cid ;			
						
	struct nr4cb *ccb ;		
						
} ;



extern struct nr4circp Nr4circuits[20		] ;



extern unsigned short Nr4window ;	
extern long Nr4irtt ;			
extern unsigned short Nr4retries ;	
extern long Nr4acktime ;		
extern char *Nr4states[] ;		
extern char *Nr4reasons[] ;		
extern unsigned short Nr4qlimit ;		
extern long Nr4choketime ;		
extern uint8 Nr4user[7	];	




int ntohnr4(struct nr4hdr *, struct mbuf **);
struct mbuf *htonnr4(struct nr4hdr *);


void free_n4circ(struct nr4cb *);
struct nr4cb *get_n4circ(int, int);
int init_nr4window(struct nr4cb *, unsigned);
int nr4between(unsigned, unsigned, unsigned);
struct nr4cb *match_n4circ(int, int,uint8 *,uint8 *);
struct nr4cb *new_n4circ(void);
void nr4defaults(struct nr4cb *);
int nr4valcb(struct nr4cb *);
void nr_garbage(int red);


void nr4input(struct nr4hdr *hdr,struct mbuf **bp);
int nr4output(struct nr4cb *);
void nr4sbuf(struct nr4cb *, unsigned);
void nr4sframe(uint8 *, struct nr4hdr *, struct mbuf **);
void nr4state(struct nr4cb *, int);


void nr4ackit(void *);
void nr4cdtimeout(void *);
void nr4txtimeout(void *);
void nr4unchoke(void *);


void disc_nr4(struct nr4cb *);
int kick_nr4(struct nr4cb *);
struct nr4cb *open_nr4(struct nr4_addr *, struct nr4_addr *, int,
  void (*)(struct nr4cb *,uint16),
  void (*)(struct nr4cb *,uint16),
  void (*)(struct nr4cb *,int,int),int);
struct mbuf *recv_nr4(struct nr4cb *, uint16);
void reset_nr4(struct nr4cb *);
int send_nr4(struct nr4cb *, struct mbuf **);


void nr4_state(struct nr4cb *, int, int);















				

				

				

				





struct nr3hdr {
	uint8 source[7	] ;	
	uint8 dest[7	] ;	
	unsigned ttl ;		
} ;



struct nr3dest {
	uint8 dest[7	] ;		
	char alias[7	] ;		
	uint8 neighbor[7	] ;	
	unsigned quality ;		
} ;



struct nriface {
	struct iface *iface ;		
	char alias[7	] ;		
					
	unsigned quality ;		
} ;


struct nrnbr_tab {
	struct nrnbr_tab *next ;	
	struct nrnbr_tab *prev ;
	uint8 call[7	] ;		
	unsigned iface ;		
					
	unsigned refcnt ;		
} ;






struct nr_bind {
	struct nr_bind *next ;		
	struct nr_bind *prev ;
	unsigned quality ;		
	unsigned obsocnt ;		
	unsigned flags ;


	struct nrnbr_tab *via ;		
} ;



struct nrroute_tab {
	struct nrroute_tab *next ;	
	struct nrroute_tab *prev ;
	char alias[7	] ;		
	uint8 call[7	] ;		
	unsigned num_routes ;		
	struct nr_bind *routes ;	

} ;


struct nrnf_tab {
	struct nrnf_tab *next ;		
	struct nrnf_tab *prev ;
	uint8 neighbor[7	] ;	
	unsigned iface ;		
} ;


struct raw_nr {
	struct raw_nr *prev;
	struct raw_nr *next;

	struct mbuf *rcvq;	
	uint8 protocol;		
};


extern struct nriface Nrifaces[10	] ;


extern unsigned Nr_numiface ;


extern struct nrnbr_tab *Nrnbr_tab[17	] ;


extern struct nrroute_tab *Nrroute_tab[17	] ;


extern struct nrnf_tab *Nrnf_tab[17	] ;

extern uint8 Nr_nodebc[7	];







extern unsigned Nr_nfmode ;


extern unsigned short Nr_ttl ;


extern unsigned Obso_init ;


extern unsigned Obso_minbc ;



extern unsigned Nr_autofloor ;




extern int Nr_verbose ;





extern unsigned Nr_maxroutes ;


extern struct iface *Nr_iface ;




void del_rnr(struct raw_nr *rpp);
uint8 *find_nralias(char *);
struct nrroute_tab *find_nrroute(uint8 *);
void nr_bcnodes(unsigned ifno);
void nr_nodercv(struct iface *iface,uint8 *source,struct mbuf **bpp);
int nr_nfadd(uint8 *, unsigned);
int nr_nfdrop(uint8 *, unsigned);
void nr_route(struct mbuf **bp,struct ax25_cb *iaxp);
int nr_routeadd(char *, uint8 *, unsigned,
	unsigned, uint8 *, unsigned, unsigned);
int nr_routedrop(uint8 *, uint8 *, unsigned);
int nr_send(struct mbuf **bp,struct iface *iface,int32 gateway,uint8 tos);
void nr_sendraw(uint8 *dest,unsigned family,unsigned proto,
	struct mbuf **data);
void nr3output(uint8 *dest,struct mbuf **data);
uint16 nrhash(uint8 *s);
struct raw_nr *raw_nr(uint8);


void donrdump(struct nr4cb *cb);
int doroutedump(void);
int dorouteinfo(int argc,char *argv[],void *p);
int putalias(char *to, char *from,int complain);


struct mbuf *htonnr3(struct nr3hdr *);
struct mbuf *htonnrdest(struct nr3dest *);
int ntohnr3(struct nr3hdr *, struct mbuf **);
int ntohnrdest(struct nr3dest *ds,struct mbuf **bpp);












enum ftp_type {
	ASCII_TYPE,
	IMAGE_TYPE,
	LOGICAL_TYPE
};


enum verb_level {
	V_QUIET,	
	V_SHORT,	
	V_NORMAL,	
	V_HASH,		
	V_STAT		
};

long sendfile(FILE *fp,FILE *network,enum ftp_type mode,enum verb_level verb);
long recvfile(FILE *fp,FILE *network,enum ftp_type mode,enum verb_level verb);
int isbinary(FILE *fp);
int md5hash(FILE *fp,uint8 hash[16],int ascii);








































































struct stopwatch {
	long calls;
	uint16 maxval;
	uint16 minval;
	int32 totval;
};
extern struct stopwatch Sw[];
extern uint16 Intstk[];	
extern uint16 Stktop[];	
extern void (*Shutdown[])();	
extern int Mtasker;	


void asytimer(void);


extern unsigned *Refcnt;
int _creat(const char *file,int mode);
int _open(const char *file,int mode);
int dup(int fd);
int _close(int fd);
int _read(int fd,void *buf,unsigned cnt);
int _write(int fd,void *buf,unsigned cnt); 
long _lseek(int fd,long offset,int whence);


unsigned long dma_map(void *p,unsigned short len,int copy);
void dma_unmap(void *p,int copy);
int dis_dmaxl(int chan);
int ena_dmaxl(int chan);
unsigned long dmalock(void *p,unsigned short len);
unsigned long dmaunlock(unsigned long physaddr,unsigned short len);
void *dma_malloc(int32 *physaddr,unsigned short len);


void rtype(uint16 c);


void escctimer(void);
void esccstop(void);


long bioscnt(void);
void clrbit(unsigned port,char bits);
void ctick(void);
int32 divrem(int32 dividend,uint16 divisor);
void  (*getirq(unsigned int))(void);
int getmask(unsigned irq);
int intcontext(void);
void ioinit(void);
void iostop(void);
void kbsave(int c);
int kbread(void);
int maskoff(unsigned irq);
int maskon(unsigned irq);
void pctick(void);
void setbit(unsigned port,char bits);
int setirq(unsigned irq,void (*handler)(void));
void sysreset(void);
void systick(void);
void writebit(unsigned port,char mask,int val);



void hwtick(void);




void chktasker(void);
void chtimer(void (*)());
int32 divrem(int32 dividend,uint16 divisor);
uint16 getss(void);
void giveup(void);
uint16 kbraw(void);
uint16 longdiv(uint16 divisor,int n,uint16 *dividend);
uint16 longmul(uint16 multiplier,int n,uint16 *multiplicand);
void nullvec(void);
void kbint(void);
void uchtimer(void);
uint16 clockbits(void);


void swstart(void);
uint16 stopval(void);


void swstop(int n);









































struct telnet {
	char local[6];	
	char remote[6];	
	struct session *session;	
	char eolmode;		
};

extern int Refuse_echo;
extern int Tn_cr_mode;


int tel_connect(struct session *sp,struct sockaddr *fsocket,int len);
void tel_output(int unused,void *p1,void *p2);
void tnrecv(struct telnet *tn);
void doopt(struct telnet *tn,int opt);
void dontopt(struct telnet *tn,int opt);
void willopt(struct telnet *tn,int opt);
void wontopt(struct telnet *tn,int opt);
void answer(struct telnet *tn,int r1,int r2);


void ttylhandle(int s,void *unused,void *p);




struct ttystate {
	uint8 *line;		
	uint8 *lp;		
	unsigned int echo:1;	
	unsigned int edit:1;	
	unsigned int crnl:1;	
};


struct session {
	unsigned strchr;
	enum {
		TELNET,
		FTP,
		AX25TNC,
		FINGER,
		PING,
		NRSESSION,
		COMMAND,
		VIEW,
		HOP,
		TIP,
		PPPPASS,
		DIAL,
		DQUERY,
		DCLIST,
		ITRACE,
		REPEAT,
		FAX
	} type;

	char *name;	
	union {
		struct ftpcli *ftp;
		struct telnet *telnet;
		void *p;
	} cb;
	struct proc *proc;	
	struct proc *proc1;	
	struct proc *proc2;	
	FILE *network;		
	FILE *record;		
	FILE *upload;		
	struct ttystate ttystate;
	FILE *input;		
	FILE *output;		
	int (*ctlproc)(int);	
	int (*inproc)(int);	
	struct session *parent;
	enum {
		SCROLL_INBAND,
		SCROLL_LOCAL
	} scrollmode;	

	FILE *keyboard;		

};
extern char *Sestypes[];
extern unsigned Nsessions;		
extern long Sfsize;			

extern struct session *Sessions[];	



extern struct session *Current;		
extern struct session *Lastcurr;	
extern struct session *Command;		
extern char *Cmdline;			


void freesession(struct session *sp);
int keywait(char *prompt,int flush);
struct session *sessptr(char *cp);
struct session *newsession(char *name,int type,int makecur);
void sesflush(void);
void upload(int unused,void *sp1,void *p);

extern uint16 Lport;








struct ftpcli {
	FILE *control;
	FILE *data;

	char state;




	uint16 verbose;		
	int batch;		
	int abort;		
	int update;		
	char type;		
	char typesent;		
	int logbsize;		
	FILE *fp;		

	char buf[256		];	
	char line[256		];	
	struct session *session;
};














int ttydriv(struct session *sp,uint8 c);




















struct zentry {
	uint16 code;	
	char data;	
};
struct zfast {		
	uint16 owncode;	
	uint16 code;	
	char data;	
};



struct lzw {
	uint16 codebits;		
	int maxbits;		

	int32 prefix;		
	char mode;		


	union {
		struct zentry **tbl;	

		struct mbuf **bpp;	

		void *p;	
	} tu;			
	int nextbit;		
	int version;		

	int32 cnt;		
	int32 code;		
	int32 next;		
	int flushbit;		
	
	struct mbuf *buf;	
};

struct usock;		
void lzwencode(int s,char c);
void lzwinit(int s,int bits,int mode);
void lzwfree(struct usock *up);
void lzwflush(struct usock *up);
int lzwdecode(struct usock *up);




























struct loc {
	struct usock *peer;
	struct mbuf *q;
	int hiwat;		
	int flags;

};




union sp {
        struct sockaddr *sa;
        struct sockaddr_in *in;
        struct sockaddr_ax *ax;
        struct sockaddr_nr *nr;
};
struct socklink {
	int type;		
	int (*socket)(struct usock *,int);
	int (*bind)(struct usock *);
	int (*listen)(struct usock *,int);
	int (*connect)(struct usock *);
	int accept;
	int (*recv)(struct usock *,struct mbuf **,struct sockaddr *,int *);
	int (*send)(struct usock *,struct mbuf **,struct sockaddr *);
	int (*qlen)(struct usock *,int);
	int (*kick)(struct usock *);
	int (*shut)(struct usock *,int);
	int (*close)(struct usock *);
	int (*check)(struct sockaddr *,int);
	char **error;
	char *(*state)(struct usock *);
	int (*status)(struct usock *);
	char *eol;
};
extern struct socklink Socklink[];
union cb {
	struct tcb *tcb;
	struct ax25_cb *ax25;
	struct udp_cb *udp;
	struct raw_ip *rip;
	struct raw_nr *rnr;
	struct nr4cb *nr4;
	struct loc *local;
	void *p;
};

struct usock {
	unsigned strchr;
	struct proc *owner;
	int refcnt;
	char noblock;
	enum {
		NOTUSED,
		TYPE_TCP,
		TYPE_UDP,
		TYPE_AX25I,
		TYPE_AX25UI,
		TYPE_RAW,
		TYPE_NETROML3,
		TYPE_NETROML4,
		TYPE_LOCAL_STREAM,
		TYPE_LOCAL_DGRAM 
	} type;
	struct socklink *sp;
	int rdysock;
	union cb cb;
	struct sockaddr *name;
	int namelen;
	struct sockaddr *peername;
	int peernamelen;
	uint8 errcodes[4];	
	uint8 tos;		
	int flag;		
};
extern char *(*Psock[])(struct sockaddr *);
extern char Badsocket[];
extern char *Socktypes[];
extern struct usock **Usock;
extern unsigned Nsock;
extern uint16 Lport;

struct usock *itop(int s);
void st_garbage(int red);


int so_ax_sock(struct usock *up,int protocol);
int so_ax_bind(struct usock *up);
int so_ax_listen(struct usock *up,int backlog);
int so_ax_conn(struct usock *up);
int so_ax_recv(struct usock *up,struct mbuf **bpp,struct sockaddr *from,
	int *fromlen);
int so_ax_send(struct usock *up,struct mbuf **bp,struct sockaddr *to);
int so_ax_qlen(struct usock *up,int rtx);
int so_ax_kick(struct usock *up);
int so_ax_shut(struct usock *up,int how);
int so_ax_close(struct usock *up);
int checkaxaddr(struct sockaddr *name,int namelen);
int so_axui_sock(struct usock *up,int protocol);
int so_axui_bind(struct usock *up);
int so_axui_conn(struct usock *up);
int so_axui_recv(struct usock *up,struct mbuf **bpp,struct sockaddr *from,
	int *fromlen);
int so_axui_send(struct usock *up,struct mbuf **bp,struct sockaddr *to);
int so_axui_qlen(struct usock *up,int rtx);
int so_axui_shut(struct usock *up,int how);
int so_axui_close(struct usock *up);
char *axpsocket(struct sockaddr *p);
char *axstate(struct usock *up);
int so_ax_stat(struct usock *up);



int so_ip_sock(struct usock *up,int protocol);
int so_ip_conn(struct usock *up);
int so_ip_recv(struct usock *up,struct mbuf **bpp,struct sockaddr *from,
	int *fromlen);
int so_ip_send(struct usock *up,struct mbuf **bp,struct sockaddr *to);
int so_ip_qlen(struct usock *up,int rtx);
int so_ip_close(struct usock *up);
int checkipaddr(struct sockaddr *name,int namelen);
char *ippsocket(struct sockaddr *p);


int so_los(struct usock *up,int protocol);
int so_lod(struct usock *up,int protocol);
int so_lo_recv(struct usock *up,struct mbuf **bpp,struct sockaddr *from,
	int *fromlen);
int so_los_send(struct usock *up,struct mbuf **bp,struct sockaddr *to);
int so_lod_send(struct usock *up,struct mbuf **bp,struct sockaddr *to);
int so_lod_qlen(struct usock *up,int rtx);
int so_los_qlen(struct usock *up,int rtx);
int so_loc_shut(struct usock *up,int how);
int so_loc_close(struct usock *up);
char *lopsocket(struct sockaddr *p);
int so_loc_stat(struct usock *up);


int so_n3_sock(struct usock *up,int protocol);
int so_n4_sock(struct usock *up,int protocol);
int so_n4_listen(struct usock *up,int backlog);
int so_n3_conn(struct usock *up);
int so_n4_conn(struct usock *up);
int so_n3_recv(struct usock *up,struct mbuf **bpp,struct sockaddr *from,
	int *fromlen);
int so_n4_recv(struct usock *up,struct mbuf **bpp,struct sockaddr *from,
	int *fromlen);
int so_n3_send(struct usock *up,struct mbuf **bp,struct sockaddr *to);
int so_n4_send(struct usock *up,struct mbuf **bp,struct sockaddr *to);
int so_n3_qlen(struct usock *up,int rtx);
int so_n4_qlen(struct usock *up,int rtx);
int so_n4_kick(struct usock *up);
int so_n4_shut(struct usock *up,int how);
int so_n3_close(struct usock *up);
int so_n4_close(struct usock *up);
int checknraddr(struct sockaddr *name,int namelen);
char *nrpsocket(struct sockaddr *p);
char *nrstate(struct usock *up);
int so_n4_stat(struct usock *up);


int so_tcp(struct usock *up,int protocol);
int so_tcp_listen(struct usock *up,int backlog);
int so_tcp_conn(struct usock *up);
int so_tcp_recv(struct usock *up,struct mbuf **bpp,struct sockaddr *from,
	int *fromlen);
int so_tcp_send(struct usock *up,struct mbuf **bp,struct sockaddr *to);
int so_tcp_qlen(struct usock *up,int rtx);
int so_tcp_kick(struct usock *up);
int so_tcp_shut(struct usock *up,int how);
int so_tcp_close(struct usock *up);
char *tcpstate(struct usock *up);
int so_tcp_stat(struct usock *up);


int so_udp(struct usock *up,int protocol);
int so_udp_bind(struct usock *up);
int so_udp_conn(struct usock *up);
int so_udp_recv(struct usock *up,struct mbuf **bpp,struct sockaddr *from,
	int *fromlen);
int so_udp_send(struct usock *up,struct mbuf **bp,struct sockaddr *to);
int so_udp_qlen(struct usock *up,int rtx);
int so_udp_shut(struct usock *up,int how);
int so_udp_close(struct usock *up);
int so_udp_stat(struct usock *up);

















































































extern char *Sock_errlist[];


extern int Axi_sock;	

int accept(int s,struct sockaddr *peername,int *peernamelen);
int bind(int s,struct sockaddr *name,int namelen);
int close_s(int s);
int connect(int s,struct sockaddr *peername,int peernamelen);
char *eolseq(int s);
void freesock(struct proc *pp);
int getpeername(int s,struct sockaddr *peername,int *peernamelen);
int getsockname(int s,struct sockaddr *name,int *namelen);
int listen(int s,int backlog);
int recv_mbuf(int s,struct mbuf **bpp,int flags,struct sockaddr *from,int *fromlen);
int send_mbuf(int s,struct mbuf **bp,int flags,struct sockaddr *to,int tolen);
int settos(int s,int tos);
int shutdown(int s,int how);
int socket(int af,int type,int protocol);
void sockinit(void);
int sockkick(int s);
int socklen(int s,int rtx);
struct proc *sockowner(int s,struct proc *newowner);
int usesock(int s);
int socketpair(int af,int type,int protocol,int sv[]);


void flushsocks(void);
int recv(int s,void *buf,int len,int flags);
int recvfrom(int s,void *buf,int len,int flags,struct sockaddr *from,int *fromlen);
int send(int s,void *buf,int len,int flags);
int sendto(int s,void *buf,int len,int flags,struct sockaddr *to,int tolen);


char *psocket(void *p);
char *sockerr(int s);
char *sockstate(int s);


int start_tcp(uint16 port,char *name,void (*task)(),int stack);
int stop_tcp(uint16 port);







struct cmds {
	char *name;		
	int (*func)(int argc,char *argv[],void *p);
				
	int stksize;		
	int  argcmin;		
	char *argc_errmsg;	
};
extern struct cmds Cmds[],Startcmds[],Stopcmds[],Attab[];


int cmdparse(struct cmds cmds[],char *line,void *p);
int subcmd(struct cmds tab[],int argc,char *argv[],void *p);
int setbool(int *var,char *label,int argc,char *argv[]);
int bit16cmd(uint16 *bits, uint16 mask, char *label, int argc, char *argv[]);
int setint(int *var,char *label,int argc,char *argv[]);
int setlong(int32 *var,char *label,int argc,char *argv[]);
int setshort(unsigned short *var,char *label,int argc,char *argv[]);
int setuns(unsigned *var,char *label,int argc,char *argv[]);






int doasystat(int argc,char *argv[],void *p);
int fp_attach (int argc,char *argv[],void *p);


int domem(int argc,char *argv[],void *p);


int doamiga(int argc,char *argv[],void *p);


int doarp(int argc,char *argv[],void *p);


int asy_attach(int argc,char *argv[],void *p);


int doax25(int argc,char *argv[],void *p);
int doaxheard(int argc,char *argv[],void *p);
int doaxdest(int argc,char *argv[],void *p);
int doconnect(int argc,char *argv[],void *p);


int dobootp(int argc,char *argv[],void *p);


int bootpdcmd(int argc,char *argv[],void *p);


int dodialer(int argc,char *argv[],void *p);


int docd(int argc,char *argv[],void *p);
int dodir(int argc,char *argv[],void *p);
int domkd(int argc,char *argv[],void *p);
int dormd(int argc,char *argv[],void *p);


int dodomain(int argc,char *argv[],void *p);


int dodrstat(int argc,char *argv[],void *p);
int dr_attach(int argc,char *argv[],void *p);
int dodr(int argc,char *argv[],void *p);


int eg_attach(int argc,char *argv[],void *p);
int doegstat(int argc,char *argv[],void *p);


int doetherstat(int argc,char *argv[],void *p);
int ec_attach(int argc,char *argv[],void *p);


int dofax(int argc,char *argv[],void *p);
int fax1(int argc,char *argv[],void *p);
int fax0(int argc,char *argv[],void *p);


int dofinger(int argc,char *argv[],void *p);


int finstart(int argc,char *argv[],void *p);
int fin0(int argc,char *argv[],void *p);


int doftp(int argc,char *argv[],void *p);
int doabort(int argc,char *argv[],void *p);


int ftpstart(int argc,char *argv[],void *p);
int ftp0(int argc,char *argv[],void *p);


int dohapnstat(int argc,char *argv[],void *p);
int hapn_attach(int argc,char *argv[],void *p);


int dohop(int argc,char *argv[],void *p);


int dohs(int argc,char *argv[],void *p);
int hs_attach(int argc,char *argv[],void *p);


int doicmp(int argc,char *argv[],void *p);


int doifconfig(int argc,char *argv[],void *p);
int dodetach(int argc,char *argv[],void *p);


int doip(int argc,char *argv[],void *p);
int doroute(int argc,char *argv[],void *p);


int dosec(int argc,char *argv[],void *p);


int doksp(int argc,char *argv[],void *p);


int ps(int argc,char *argv[],void *p);


int dolterm(int argc,char *argv[],void *p);


int dodelete(int argc,char *argv[],void *p);
int dorename(int argc,char *argv[],void *p);
int doexit(int argc,char *argv[],void *p);
int dohostname(int argc,char *argv[],void *p);
int dolog(int argc,char *argv[],void *p);
int dohelp(int argc,char *argv[],void *p);
int doattach(int argc,char *argv[],void *p);
int doparam(int argc,char *argv[],void *p);
int dopage(int argc,char *argv[],void *p);
int domode(int argc,char *argv[],void *p);
int donothing(int argc,char *argv[],void *p);
int donrstat(int argc,char *argv[],void *p);
int doescape(int argc,char *argv[],void *p);
int doremote(int argc,char *argv[],void *p);
int doboot(int argc,char *argv[],void *p);
int dorepeat(int argc,char *argv[],void *p);
int dodebug(int argc,char *argv[],void *p);
int dowipe(int argc,char *argv[],void *p);


int dombox(int argc,char *argv[],void *p);


int donntp(int argc,char *argv[],void *p);


int donetrom(int argc,char *argv[],void *p);
int nr_attach(int argc,char *argv[],void *p);


int doshell(int argc,char *argv[],void *p);
int doisat(int argc,char *argv[],void *p);


int pc_attach(int argc,char *argv[],void *p);


int pk_attach(int argc,char *argv[],void *p);


int pi_attach(int argc,char *argv[],void *p);
int dopistat(int argc,char *argv[],void *p);


int doping(int argc,char *argv[],void *p);


int dopop(int argc,char *argv[],void *p);


int pop1(int argc,char *argv[],void *p);
int pop0(int argc,char *argv[],void *p);


int doqtso(int argc,char *argv[],void *p);


int dorarp(int argc,char *argv[],void *p);


int dorip(int argc,char *argv[],void *p);


int doaddrefuse(int argc,char *argv[],void *p);
int dodroprefuse(int argc,char *argv[],void *p);
int dorip(int argc,char *argv[],void *p);
int doripadd(int argc,char *argv[],void *p);
int doripdrop(int argc,char *argv[],void *p);
int doripinit(int argc,char *argv[],void *p);
int doripmerge(int argc,char *argv[],void *p);
int doripreq(int argc,char *argv[],void *p);
int doripstat(int argc,char *argv[],void *p);
int doripstop(int argc,char *argv[],void *p);
int doriptrace(int argc,char *argv[],void *p);


int dosound(int argc,char *argv[],void *p);


int escc_attach(int argc,char *argv[],void *p);
int doesccstat(int argc,char *argv[],void *p);


int dosession(int argc,char *argv[],void *p);
int go(int argc,char *argv[],void *p);
int doclose(int argc,char *argv[],void *p);
int doreset(int argc,char *argv[],void *p);
int dokick(int argc,char *argv[],void *p);
int dorecord(int argc,char *argv[],void *p);
int dosfsize(int argc,char *argv[],void *p);
int doupload(int argc,char *argv[],void *p);


int dis1(int argc,char *argv[],void *p);
int dis0(int argc,char *argv[],void *p);
int echo1(int argc,char *argv[],void *p);
int echo0(int argc,char *argv[],void *p);
int rem1(int argc,char *argv[],void *p);
int rem0(int argc,char *argv[],void *p);
int term1(int argc,char *argv[],void *p);
int term0(int argc,char *argv[],void *p);
int bsr1(int argc,char *argv[],void *p);
int bsr0(int argc,char *argv[],void *p);


int dosmtp(int argc,char *argv[],void *p);


int smtp1(int argc,char *argv[],void *p);
int smtp0(int argc,char *argv[],void *p);


int dosock(int argc,char *argv[],void *p);


int dofiles(int argc,char *argv[],void *p);


int doswatch(int argc,char *argv[],void *p);


int dotcp(int argc,char *argv[],void *p);


int doecho(int argc,char *argv[],void *p);
int doeol(int argc,char *argv[],void *p);
int dotelnet(int argc,char *argv[],void *p);
int dotopt(int argc,char *argv[],void *p);


int dotip(int argc,char *argv[],void *p);


int ttylstart(int argc,char *argv[],void *p);
int ttyl0(int argc,char *argv[],void *p);


int dotrace(int argc,char *argv[],void *p);


int doudp(int argc,char *argv[],void *p);


int doview(int argc,char *argv[],void *p);
void view(int,void *,void *);





struct daemon {
	char *name;
	unsigned stksize;
	void (*fp)(int,void *,void *);
};
extern struct daemon Daemons[];


void gcollect(int,void*,void*);


void keyboard(int,void*,void*);
void network(int,void *,void *);
void display(int,void *,void *);


void killer(int,void*,void*);


void rand_init(int,void*,void*);


void timerproc(int,void*,void*);











enum devparam {
	PARAM_DATA,
	PARAM_TXDELAY,
	PARAM_PERSIST,
	PARAM_SLOTTIME,
	PARAM_TXTAIL,
	PARAM_FULLDUP,
	PARAM_HW,
	PARAM_MUTE,
	PARAM_DTR,
	PARAM_RTS,
	PARAM_SPEED,
	PARAM_ENDDELAY,
	PARAM_GROUP,
	PARAM_IDLE,
	PARAM_MIN,
	PARAM_MAXKEY,
	PARAM_WAIT,
	PARAM_DOWN=0x81,
	PARAM_UP=0x82,
	PARAM_BLIND=0x83,	
	PARAM_RETURN=0xff
};


int devparam(char *s);
char *parmname(int n);









































struct dserver {
	struct dserver *prev;	
	struct dserver *next;

	int32 address;		
	int32 timeout;		
	int32 srtt;		
	int32 mdev;		
	int32 queries;		
	int32 responses;	
};
extern struct dserver *Dlist;
extern int Dsocket;		








struct dhdr {
	uint16 id;		
	uint8 qr;		


	uint8 opcode;

	uint8 aa;		
	uint8 tc;		
	uint8 rd;		
	uint8 ra;		
	uint8 rcode;		






	uint16 qdcount;		
	uint16 ancount;		
	uint16 nscount;		
	uint16 arcount;		
	struct rr *questions;	
	struct rr *answers;	
	struct rr *authority;	
	struct rr *additional;	
};

struct mx {
	uint16 pref;
	char *exch;
};

struct hinfo {
	char *cpu;
	char *os;
};

struct soa {
	char *mname;
	char *rname;
	int32 serial;
	int32 refresh;
	int32 retry;
	int32 expire;
	int32 minimum;
};

struct rr {
	struct rr *last;
	struct rr *next;
	uint8 source;









	char *comment;		
	char *name;		
	int32 ttl;		

	uint16 class;		

	uint16 type;		

	uint16 rdlength;		
	union {
		int32 addr;		
		struct soa soa;		
		struct mx mx;		
		struct hinfo hinfo;	
		char *name;		
		char *data;		
	} rdata;
};
extern struct proc *Dfile_updater;


int add_nameserver(int32 address);
void free_rr(struct rr *rrlp);
struct rr *inverse_a(int32 ip_address);
struct rr *resolve_rr(char *dname,uint16 dtype);
char *resolve_a(int32 ip_address, int shorten);
struct rr *resolve_mailb(char *name);


int ntohdomain(struct dhdr *dhdr,struct mbuf **bpp);





















extern char *Startup;	
extern char *Userfile;	
extern char *Maillog;	
extern char *Mailspool;	
extern char *Mailqdir;	
extern char *Mailqueue;	
extern char *Routeqdir;	
extern char *Alias;	
extern char *Dfile;	
extern char *Fdir;	
extern char *Arealist;		
extern char *Helpdir;		
extern char *Rewritefile;	
extern char *Newsdir;		
extern char *Popusers;		
extern char *Signature;		
extern char *Forwardfile;	
extern char *Historyfile;	

void initroot(char *root);
char *rootdircat(char *filename);
int userlogin(char *name,char *pass,char **path,int len,int *pwdignore);
char *userlookup(char *username, char **password, char **directory,
			int *permission, int32 *ip_address);
void usercvt(void);









extern char Badhost[];
extern char *Hostname;
extern char Nospace[];			

extern struct proc *Cmdpp;
extern struct proc *Display;
extern int main_exit;			







extern char *Rempass;





















struct tracecmd {
	char *name;	
	int val;	
	int mask;	
};
extern struct tracecmd Tracecmd[];	




struct trace {
	int (*addrtest)(struct iface *iface,struct mbuf *bp);
	void (*tracef)(FILE *,struct mbuf **,int);
};

extern struct trace Tracef[];


void dump(struct iface *ifp,int direction,struct mbuf *bp);
void raw_dump(struct iface *ifp,int direction, struct mbuf *bp);
void trace_log(struct iface *ifp,char *fmt, ...);
void shuttrace(void);
int tprintf(struct iface *ifp,char *fmt,...);
void hex_dump(FILE *fp,struct mbuf **bpp);


void arc_dump(FILE *fp,struct mbuf **bpp,int check);
int arc_forus(struct iface *iface,struct mbuf *bp);


void arp_dump(FILE *fp,struct mbuf **bpp);


void ax25_dump(FILE *fp,struct mbuf **bpp,int check);
int ax_forus(struct iface *iface,struct mbuf *bp);


void ether_dump(FILE *fp,struct mbuf **bpp,int check);
int ether_forus(struct iface *iface,struct mbuf *bp);


void icmp_dump(FILE *fp,struct mbuf **bpp,int32 source,int32 dest,int check);


void ipip_dump(FILE *fp,struct mbuf **bpp,int32 source,int32 dest,int check);
void ip_dump(FILE *fp,struct mbuf **bpp,int check);


void ki_dump(FILE *fp,struct mbuf **bpp,int check);
int ki_forus(struct iface *iface,struct mbuf *bp);


void netrom_dump(FILE *fp,struct mbuf **bpp,int check);


void ppp_dump(FILE *fp,struct mbuf **bpp,int check);


void rip_dump(FILE *fp,struct mbuf **bpp);


void esp_dump(FILE *fp,struct mbuf **bpp,int32 source,int32 dest,int check);
void ah_dump(FILE *fp,struct mbuf **bpp,int32 source,int32 dest,int check);


void sl_dump(FILE *fp,struct mbuf **bpp,int check);
void vjcomp_dump(FILE *fp,struct mbuf **bpp,int unused);


void tcp_dump(FILE *fp,struct mbuf **bpp,int32 source,int32 dest,int check);


void udp_dump(FILE *fp,struct mbuf **bpp,int32 source,int32 dest,int check);











struct display {
	unsigned cookie;	

	uint8 cols;	
	uint8 col;	
	uint8 savcol;	

	uint8 rows;	
	uint8 row;	
	uint8 savrow;	

	uint8 slast;	
	int scroll;	

	int argi;	
	int arg[5];	

	uint8 attrib;	
	enum {
		DISP_NORMAL,	
		DISP_ESCAPE,	
		DISP_ARG	
	} state;	
	struct {
		unsigned int dirty_screen:1;	
		unsigned int dirty_cursor:1;	
		unsigned int no_line_wrap:1;	
		unsigned int no_scroll:1;	
		unsigned int scrollbk;		 
	} flags;	

	uint8 *buf;	

	




	struct dirty {
		uint8 lcol;	
		uint8 rcol;	
	} *dirty;		

	uint8 *tabstops;	

	FILE *sfile;	
	long sfseek;	
	long sfoffs;	
	long sfsize;	
	long sflimit;	
};

struct display *newdisplay(int rows,int cols,int noscrol,int sfsize);
void displaywrite(struct display *dp,void *buf,int cnt);
void dupdate(struct display *dp);
void closedisplay(struct display *dp);
void statwrite(struct display *dp,int col,void *buf,int cnt,int attrib);
void dscrollmode(struct display *dp,int flag);
void dhome(struct display *dp);
void dend(struct display *dp);
void dpgup(struct display *dp);
void dpgdown(struct display *dp);
void dcursup(struct display *dp);
void dcursdown(struct display *dp);
void debug(char *s);

















































































































































































typedef unsigned short ino_t;	
typedef unsigned short blkno_t;	
typedef unsigned short dev_t;	










typedef struct blkbuf_s blkbuf_t;


struct blkbuf_s
	{
	unsigned char bf_data[0x200		]; 
	dev_t bf_dev; 	
	blkno_t bf_blk; 	
	unsigned char bf_dirty;	
	unsigned char bf_busy;	
	unsigned char bf_prio;	
	unsigned int bf_time;	
	 
	};


typedef struct
	{
	unsigned char minors; 	
	int (*dev_init)(unsigned char minor);
	int (*dev_open)(unsigned char minor);
	int (*dev_close)(unsigned char minor);
	int (*dev_read)(unsigned char minor, unsigned char rawflag);
	int (*dev_write)(unsigned char minor, unsigned char rawflag);
	int (*dev_ioctl)(unsigned char minor, int cmd, void *data);
	} devsw_t;





extern blkbuf_t *Bufpool[3 ];
extern devsw_t *Devtab[3 ];

extern unsigned int bufclock;		

extern unsigned int buf_hits;		
extern unsigned int buf_miss;		
extern unsigned int buf_flsh;		

























void *bread(dev_t dev, blkno_t blk, unsigned char rewrite);
int bfree(blkbuf_t *bp, unsigned char dirty);
void *zerobuf(unsigned char waitfor);
void bufsync(void);
blkbuf_t *bfind(int dev, blkno_t blk);
blkbuf_t *freebuf(unsigned char waitfor);
void bufdump(void);
devsw_t *validdev(dev_t dev, char *msg);
int bdreadwrite(blkbuf_t *bp, unsigned char write);
int cdreadwrite(dev_t dev, unsigned char write);
int d_openclose(dev_t dev, unsigned char open);
int d_ioctl(dev_t dev, int request, void *data);
int d_init(void);
int ok(unsigned char minor);
int ok_rdwr(unsigned char minor, unsigned char rawflag);
int nogood(unsigned char minor);
int nogood_ioctl(unsigned char minor, int req, void *data);




























void xfs_init(dev_t boot_dev);
void xfs_end(void);
int xfs_open(char *name, int flag, ...);
int xfs_close(int uindex);
int xfs_creat(char *name, mode_t mode);
int xfs_link(char *name1, char *name2);
int xfs_symlink(char *name1, char *name2);
int xfs_unlink(char *path);
int xfs_read(int d, char *buf, unsigned int nbytes);
int xfs_write(int d, char *buf, unsigned int nbytes);
int xfs_seek(int file, unsigned int offset, int flag);
int xfs_chdir(char *dir);
int xfs_mknod(char *name, mode_t mode, int dev);
void xfs_sync(void);
int xfs_access(char *path, int mode);
int xfs_chmod(char *path, mode_t mode);
int xfs_chown(char *path, int owner, int group);
int xfs_stat(char *path, void *buf);
int xfs_fstat(int fd, void *buf);
int xfs_falign(int fd, int parm);
int xfs_dup(int oldd);
int xfs_dup2(int oldd, int newd);
int xfs_umask(int mask);
int xfs_ioctl(int fd, int request, ...);
int xfs_mount(char *spec, char *dir, int rwflag);
int xfs_umount(char *spec);
int xfs_time(void *tvec);
int xfs_getfsys(dev_t dev, void *buf);










const char *_(const char *);
int _fprintf(FILE *stream, const char *format, ...);
int _fputs(const char *string, FILE *stream);
int _printf(const char *format, ...);
int _puts(const char *string);
char *_strcpy(char *buffer, const char *string);
char *_strdup(const char *string);










void __msg0000__07Incoming_20_2(void);
void __msg0000__08_20_08(void);
void __msg0000__09_09_20MRU_09_(void);
void __msg0000__09_09_256u_20Lc(void);
void __msg0001__09_09_256u_20Lc(void);
void __msg0000__09_2510ld_20Cmp(void);
void __msg0001__09_2510ld_20Cmp(void);
void __msg0000__09_2510ld_20Sea(void);
void __msg0000__09_25ld(void);
void __msg0000__09_25p_09_25u_0(void);
void __msg0000__09_25s(void);
void __msg0000__09_25s_09_25s_0(void);
void __msg0001__09_25s_09_25s_0(void);
void __msg0000__09_25s_0a(void);
void __msg0000__09_25u_09_25s_0(void);
void __msg0000__09_28open_20for(void);
void __msg0000__09_3c_25u_3e(void);
void __msg0000__09IN(void);
void __msg0000__09Local_3a_09(void);
void __msg0000__09Remote_3a_09(void);
void __msg0000__09Urgent_20poin(void);
void __msg0000__09changes_3a_20(void);
void __msg0000__09connection_20(void);
void __msg0001__09connection_20(void);
void __msg0000__09delta_20ACK_2(void);
void __msg0000__09delta_20ACK_3(void);
void __msg0000__09delta_20ID_3a(void);
void __msg0000__09delta_20SEQ_2(void);
void __msg0000__09delta_20SEQ_3(void);
void __msg0000__09delta_20WINDO(void);
void __msg0000__09increment_20I(void);
void __msg0000__09local_20IP_20(void);
void __msg0000__0a(void);
void __msg0000__0a_20_20_20_20(void);
void __msg0000__0a_20_20_20_20_(void);
void __msg0000__0a_2520s(void);
void __msg0000__0a_25s_20_2d_20(void);
void __msg0001__0a_25s_20_2d_20(void);
void __msg0000__0a_2a_2a_2a_20L(void);
void __msg0000__0a_2a_2a_2a_20U(void);
void __msg0000__0a_2a_2a_2a_2a_(void);
void __msg0000__0aCurrent_20rem(void);
void __msg0000__0aReclaimation_(void);
void __msg0000__0aUsed_20addres(void);
void __msg0000__0apanic_3a_20(void);
void __msg0000__20_0aend_0asize(void);
void __msg0000__20_20(void);
void __msg0000__20_20_20(void);
void __msg0000__20_20_20_20(void);
void __msg0000__20_20_20_20_20(void);
void __msg0000__20_20_20_20_20_(void);
void __msg0001__20_20_20_20_20_(void);
void __msg0002__20_20_20_20_20_(void);
void __msg0003__20_20_20_20_20_(void);
void __msg0004__20_20_20_20_20_(void);
void __msg0005__20_20_20_20_20_(void);
void __msg0006__20_20_20_20_20_(void);
void __msg0007__20_20_20_20_20_(void);
void __msg0008__20_20_20_20_20_(void);
void __msg0009__20_20_20_20_20_(void);
void __msg000a__20_20_20_20_20_(void);
void __msg000b__20_20_20_20_20_(void);
void __msg000c__20_20_20_20_20_(void);
void __msg000d__20_20_20_20_20_(void);
void __msg000e__20_20_20_20_20_(void);
void __msg000f__20_20_20_20_20_(void);
void __msg0010__20_20_20_20_20_(void);
void __msg0011__20_20_20_20_20_(void);
void __msg0012__20_20_20_20_20_(void);
void __msg0013__20_20_20_20_20_(void);
void __msg0014__20_20_20_20_20_(void);
void __msg0015__20_20_20_20_20_(void);
void __msg0016__20_20_20_20_20_(void);
void __msg0000__20_20_20_20_20B(void);
void __msg0000__20_20_20_20_20D(void);
void __msg0000__20_20_20_20_20H(void);
void __msg0000__20_20_20_20_20I(void);
void __msg0001__20_20_20_20_20I(void);
void __msg0000__20_20_20_20_20N(void);
void __msg0000__20_20_20_20_20S(void);
void __msg0000__20_20_20_20_25_(void);
void __msg0000__20_20_20_20_251(void);
void __msg0000__20_20_20_20_253(void);
void __msg0000__20_20_20_20_25s(void);
void __msg0001__20_20_20_20_25s(void);
void __msg0002__20_20_20_20_25s(void);
void __msg0000__20_20_20_20_26U(void);
void __msg0000__20_20_20_20In_0(void);
void __msg0000__20_20_20_20Out_(void);
void __msg0000__20_20_20_20Reco(void);
void __msg0000__20_20_20_20Uplo(void);
void __msg0000__20_20_20_25s_0a(void);
void __msg0000__20_20_20PUSH(void);
void __msg0000__20_20_20TCP_20c(void);
void __msg0000__20_20_20connect(void);
void __msg0000__20_20_3d_3d_3d_(void);
void __msg0001__20_20_3d_3d_3d_(void);
void __msg0000__20_20SR_20_5bnu(void);
void __msg0000__20_20S_5bF_5d_2(void);
void __msg0000__20_20remote_20I(void);
void __msg0000__20_20seq_20x_25(void);
void __msg0000__20_21_3f(void);
void __msg0000__20_21A(void);
void __msg0000__20_21F(void);
void __msg0000__20_21H(void);
void __msg0000__20_21N(void);
void __msg0000__20_21P(void);
void __msg0000__20_21S(void);
void __msg0000__20_23_20_20S_23(void);
void __msg0000__20_25_2d15s(void);
void __msg0000__20_25_2d3d_20(void);
void __msg0000__20_2502u_2f_250(void);
void __msg0001__20_2502u_2f_250(void);
void __msg0000__20_2502x(void);
void __msg0000__20_2510lu_2510l(void);
void __msg0000__20_254d_20_254d(void);
void __msg0000__20_258s_20_20_2(void);
void __msg0000__20_25lu_20bps_0(void);
void __msg0000__20_25s(void);
void __msg0000__20_25s_0a(void);
void __msg0000__20_25s_20_25s_2(void);
void __msg0000__20_25s_25s(void);
void __msg0000__20_25s_2b_25x(void);
void __msg0000__20_25u_0a(void);
void __msg0000__20_25u_2d_3e_25(void);
void __msg0000__20_28_25ld_20ms(void);
void __msg0000__20_28_25s_20see(void);
void __msg0000__20_28_25s_29_0a(void);
void __msg0000__20_28_25x0_2d_2(void);
void __msg0000__20_28ASCII_20du(void);
void __msg0000__20_28Hex_2fASCI(void);
void __msg0000__20_28S_29(void);
void __msg0000__20_28headers_20(void);
void __msg0000__20_28published_(void);
void __msg0000__20_2a_2a_2a(void);
void __msg0000__20_2d_20(void);
void __msg0000__20_2d_20no_20br(void);
void __msg0000__20APND(void);
void __msg0000__20Ack_20x_25lx(void);
void __msg0000__20BUSY(void);
void __msg0000__20CE(void);
void __msg0000__20CHECKSUM_20ER(void);
void __msg0000__20CHOKE(void);
void __msg0000__20DF(void);
void __msg0000__20Data_20_25u(void);
void __msg0000__20EOF(void);
void __msg0000__20ERR(void);
void __msg0000__20IPaddr_20_25s(void);
void __msg0000__20Illegal_20I_2(void);
void __msg0000__20Invalid_20con(void);
void __msg0000__20Invalid_20seq(void);
void __msg0000__20MC_3a_20int_2(void);
void __msg0000__20MF(void);
void __msg0000__20MORE(void);
void __msg0000__20MSS_20_25u(void);
void __msg0000__20NAK(void);
void __msg0000__20NR_3d_25d(void);
void __msg0000__20NS_3d_25d(void);
void __msg0000__20RX_3a_20int_2(void);
void __msg0000__20Raw_20output(void);
void __msg0000__20Remote_3a_20_(void);
void __msg0000__20Retry(void);
void __msg0000__20Running_20und(void);
void __msg0000__20State_3a_20_2(void);
void __msg0000__20TMP(void);
void __msg0000__20TSTAMP_20_25l(void);
void __msg0000__20TX_3a_20int_2(void);
void __msg0000__20Too_2dlong_20(void);
void __msg0000__20UP_20x_25x(void);
void __msg0000__20Unack_3a_20_2(void);
void __msg0000__20Unknown_20pas(void);
void __msg0000__20Vr_20_3d_20_2(void);
void __msg0000__20WSCALE_20_25u(void);
void __msg0000__20Wnd_20_25u(void);
void __msg0000__20_5b_25s_5d_20(void);
void __msg0000__20_5bNS16550A_5(void);
void __msg0000__20_5bcts_20flow(void);
void __msg0000__20_5brlsd_20lin(void);
void __msg0000__20_5btrigger_20(void);
void __msg0000__20asy_20(void);
void __msg0000__20bad_20header_(void);
void __msg0000__20bad_20packet_(void);
void __msg0000__20cmd_20_25u(void);
void __msg0000__20code_20_25s(void);
void __msg0000__20dest_20_25s(void);
void __msg0000__20dest_20_25s_2(void);
void __msg0000__20diag(void);
void __msg0000__20disp(void);
void __msg0000__20fifo_20TO_20_(void);
void __msg0000__20full(void);
void __msg0000__20hwaddr_20_25s(void);
void __msg0000__20hwlen_20_25u(void);
void __msg0000__20hwtype_20_25s(void);
void __msg0000__20id_20_25u_20o(void);
void __msg0000__20id_20_25u_20p(void);
void __msg0000__20id_20_25u_20s(void);
void __msg0000__20idle_20timer_(void);
void __msg0000__20input(void);
void __msg0000__20len_20_25u(void);
void __msg0000__20line(void);
void __msg0000__20new_20gateway(void);
void __msg0001__20new_20gateway(void);
void __msg0000__20none(void);
void __msg0000__20offset_20_25u(void);
void __msg0000__20op_20_25u(void);
void __msg0000__20op_20REPLY(void);
void __msg0000__20op_20REQUEST(void);
void __msg0000__20op_20REVERSE_(void);
void __msg0001__20op_20REVERSE_(void);
void __msg0000__20output(void);
void __msg0000__20pid_3d0x_25x_(void);
void __msg0000__20pid_3dARP_0a(void);
void __msg0000__20pid_3dIP_0a(void);
void __msg0000__20pid_3dNET_2fR(void);
void __msg0000__20pid_3dTEXNET_(void);
void __msg0000__20pid_3dText_0a(void);
void __msg0000__20pid_3dX_2e25_(void);
void __msg0000__20pipe(void);
void __msg0000__20pointer_20_25(void);
void __msg0000__20prot_20_25s_0(void);
void __msg0000__20prot_20_25u_0(void);
void __msg0000__20prot_200x_25x(void);
void __msg0000__20prot_20IP(void);
void __msg0000__20receive_20_20(void);
void __msg0000__20sock(void);
void __msg0000__20spi_20_25lx(void);
void __msg0000__20sw_20over_20_(void);
void __msg0000__20tos_20_25u(void);
void __msg0000__20trace_20file_(void);
void __msg0000__20tracing_20off(void);
void __msg0000__20transmit_20_2(void);
void __msg0000__20ttl_20_25d_0a(void);
void __msg0000__20txseq_20_25d_(void);
void __msg0000__20type_200x_25x(void);
void __msg0000__20type_20ARP_0a(void);
void __msg0000__20type_20IP_0a(void);
void __msg0000__20type_20REVARP(void);
void __msg0000__20v(void);
void __msg0000__20vers_20_25u_2(void);
void __msg0000__20wnd_20_25d(void);
void __msg0000__20_7c_20(void);
void __msg0000__21_21_20_25s_20(void);
void __msg0000__21_21_20maximum(void);
void __msg0000__22_25s_22_20_2d(void);
void __msg0000__25_2d10_2e10s_2(void);
void __msg0000__25_2d10p_25_2d1(void);
void __msg0000__25_2d10s_20IP_2(void);
void __msg0000__25_2d10s_25_2d1(void);
void __msg0001__25_2d10s_25_2d1(void);
void __msg0000__25_2d10s_25_2d6(void);
void __msg0000__25_2d11s_25_2d9(void);
void __msg0000__25_2d13s(void);
void __msg0000__25_2d15s(void);
void __msg0000__25_2d16s(void);
void __msg0000__25_2d16s_20_20(void);
void __msg0000__25_2d16s_25_2d3(void);
void __msg0000__25_2d16s_25_2d9(void);
void __msg0000__25_2d17s(void);
void __msg0000__25_2d20s_258lu_(void);
void __msg0000__25_2d22s(void);
void __msg0000__25_2d22s_0a(void);
void __msg0000__25_2d23s(void);
void __msg0000__25_2d2u(void);
void __msg0000__25_2d32s_20_28_(void);
void __msg0000__25_2d3u(void);
void __msg0000__25_2d4d(void);
void __msg0000__25_2d4d_255d_20(void);
void __msg0000__25_2d4u(void);
void __msg0000__25_2d5ld(void);
void __msg0000__25_2d7lu(void);
void __msg0000__25_2d7s_20_25_2(void);
void __msg0000__25_2d8lu(void);
void __msg0000__25_2ds(void);
void __msg0000__2502x(void);
void __msg0000__2504x(void);
void __msg0000__2504x_20_20(void);
void __msg0000__2504x_3a_2504x(void);
void __msg0000__2510lu_20In_2c_(void);
void __msg0000__2510lu_20Out_2c(void);
void __msg0000__2510lu_2510lu_2(void);
void __msg0000__2515d_20_25s_0a(void);
void __msg0000__251s_20_253d_20(void);
void __msg0000__252d_20_25_2d6s(void);
void __msg0001__252d_20_25_2d6s(void);
void __msg0000__253d_3a(void);
void __msg0000__254d_20_25_2d8s(void);
void __msg0000__254s(void);
void __msg0000__258lu(void);
void __msg0000__258lu_0a(void);
void __msg0000__258s(void);
void __msg0000__258u_2510u_2511(void);
void __msg0000__259p_20_20_20_2(void);
void __msg0000__259p_20_25_2d8d(void);
void __msg0000__259p_20_25_2d9s(void);
void __msg0000__259p_256u_20_20(void);
void __msg0000__259p_256u_256u_(void);
void __msg0000__25c(void);
void __msg0000__25c_20(void);
void __msg0000__25c_20_257s_20_(void);
void __msg0000__25c_25c_25c(void);
void __msg0000__25c_25c_25c_0a(void);
void __msg0000__25c_25c_25c_253(void);
void __msg0000__25c_25s_0a(void);
void __msg0000__25c_5b2J(void);
void __msg0000__25d_0a(void);
void __msg0000__25d_20message_2(void);
void __msg0000__25ld(void);
void __msg0000__25lu(void);
void __msg0000__25lu_0a(void);
void __msg0000__25lu_2f_25lu_0a(void);
void __msg0000__25p_20(void);
void __msg0000__25p_20_256lu(void);
void __msg0000__25p_20_256lu_20(void);
void __msg0000__25s(void);
void __msg0000__25s_0a(void);
void __msg0000__25s_0a_25s_0a(void);
void __msg0000__25s_20(void);
void __msg0000__25s_20_2502d_25(void);
void __msg0000__25s_20_25p_0a(void);
void __msg0000__25s_20_25s_0a(void);
void __msg0000__25s_20_25s_3a_2(void);
void __msg0001__25s_20_25s_3a_2(void);
void __msg0000__25s_20_2d_20(void);
void __msg0000__25s_20_2d_20Dif(void);
void __msg0000__25s_20_2d_20Sam(void);
void __msg0000__25s_20errno_20_(void);
void __msg0000__25s_20file_25s_(void);
void __msg0000__25s_20is_20unre(void);
void __msg0000__25s_20remain_20(void);
void __msg0000__25s_20thru_20(void);
void __msg0000__25s_20type_20_2(void);
void __msg0000__25s_25_25_25s_2(void);
void __msg0000__25s_25c_0a(void);
void __msg0000__25s_25c_25c_25c(void);
void __msg0000__25s_25s(void);
void __msg0000__25s_25s_0a(void);
void __msg0000__25s_25s_0a_0a(void);
void __msg0000__25s_3a(void);
void __msg0000__25s_3a_0a(void);
void __msg0000__25s_3a_20(void);
void __msg0000__25s_3a_20_25d_2(void);
void __msg0000__25s_3a_20_25i_0(void);
void __msg0000__25s_3a_20_25ld_(void);
void __msg0000__25s_3a_20_25s_0(void);
void __msg0000__25s_3a_20_25s_2(void);
void __msg0000__25s_3a_20_25u_0(void);
void __msg0000__25s_3a_20Error_(void);
void __msg0000__25s_3a_20Interf(void);
void __msg0000__25s_3a_20PPP_2f(void);
void __msg0000__25s_3a_20errno_(void);
void __msg0000__25s_3a_20local_(void);
void __msg0000__25s_3a_20mode_2(void);
void __msg0000__25s_3a_20not_20(void);
void __msg0000__25s_3a_20releas(void);
void __msg0000__25s_3a_20rtt_20(void);
void __msg0000__25s_3a_20srtt_2(void);
void __msg0000__25s_3c_25ld_40_(void);
void __msg0000__25s_3e_20_25s_2(void);
void __msg0000__25s_3e_20Invali(void);
void __msg0000__25s_40_25s_0a(void);
void __msg0000__25s_40_25s_3a_0(void);
void __msg0000__25sMAILER_2dDAE(void);
void __msg0000__25sR_0a(void);
void __msg0000__25u_0a(void);
void __msg0000__25u_20additiona(void);
void __msg0000__25u_20answers_3(void);
void __msg0000__25u_20authority(void);
void __msg0000__25u_20questions(void);
void __msg0000__25u_3a_20calls_(void);
void __msg0000__25x(void);
void __msg0000__25x_20not_20a_2(void);
void __msg0000__25x_3a_25x_09_2(void);
void __msg0000__26AXB_20_20_20_(void);
void __msg0000__26CB_20_20_20_2(void);
void __msg0000__26TCB_20_20_20_(void);
void __msg0000__26UCB_20_20_20_(void);
void __msg0000__27_25c_27_0a(void);
void __msg0000__28(void);
void __msg0000__28_252u_29_25_2(void);
void __msg0000__28F_29(void);
void __msg0000__28P_29(void);
void __msg0000__28P_2fF_29(void);
void __msg0000__28compressed_20(void);
void __msg0000__28missing_20UI_(void);
void __msg0000__28not_20odd_21_(void);
void __msg0000__29_0a(void);
void __msg0000__2a_2a_20smtp_3a(void);
void __msg0000__2a_2a_2a_20Done(void);
void __msg0000__2a_2a_2a_20Unab(void);
void __msg0000__2a_2a_2a_2a_2a_(void);
void __msg0000__2c_20(void);
void __msg0000__2d_2d_2d_2d_2d_(void);
void __msg0001__2d_2d_2d_2d_2d_(void);
void __msg0002__2d_2d_2d_2d_2d_(void);
void __msg0003__2d_2d_2d_2d_2d_(void);
void __msg0004__2d_2d_2d_2d_2d_(void);
void __msg0000__2d_3e_25s(void);
void __msg0000__2d_3e_25s_20ihl(void);
void __msg0000__2e_0a(void);
void __msg0000__2f_25lu_20ms(void);
void __msg0000__2f_25lu_20ms_0a(void);
void __msg0000__2f_25lu_20ms_3b(void);
void __msg0000__2f_25u(void);
void __msg0000__2fEX(void);
void __msg0000_0x_2503x_20_20_2(void);
void __msg0000_0x_2504x_0a(void);
void __msg0000_0x_2508lx_0a(void);
void __msg0000_0x_25x_09_09_25l(void);
void __msg0001_0x_25x_09_09_25l(void);
void __msg0000_0x_25x_0a(void);
void __msg0000_200_20(void);
void __msg0000_250_20_25s_0a(void);
void __msg0000_250_2d_25s_0a(void);
void __msg0000_8530_20Int_20sta(void);
void __msg0000__3a_20_25s(void);
void __msg0000__3a_20_25s_0a(void);
void __msg0000__3b_20Tries_3a_2(void);
void __msg0000__3c_25ld_40_25s_(void);
void __msg0000__3c_25s_40_25s_2(void);
void __msg0000__3c_3d_3d_25s(void);
void __msg0000__3c_3d_3d_25s_0a(void);
void __msg0000__3d_3d_3eARTICLE(void);
void __msg0000__3d_3d_3eNEWNEWS(void);
void __msg0000__3d_3d_3eQUIT_0a(void);
void __msg0000__3d_3dNo_20respo(void);
void __msg0000__40_25s(void);
void __msg0000_ACKIT_20called_2(void);
void __msg0000_AHDLC_20ABORT_2c(void);
void __msg0000_AHDLC_20CRC_20ER(void);
void __msg0000_AHDLC_20RUNT_2c_(void);
void __msg0000_ARP_3a_20len_20_(void);
void __msg0000_ARTICLE_20_25s(void);
void __msg0000_AX25_3a_20(void);
void __msg0000_Abort_0d_0a(void);
void __msg0000_Aborted_2e(void);
void __msg0000_Aborted_2e_0a(void);
void __msg0000_Accept_0a(void);
void __msg0000_Active_20RIP_20o(void);
void __msg0000_All_20groups_20a(void);
void __msg0000_Already_20have_2(void);
void __msg0000_Arcnet_3a_20len_(void);
void __msg0000_Argument_20missi(void);
void __msg0000_Ascii_0a(void);
void __msg0000_Attach_20device_(void);
void __msg0000_Attach_20netrom_(void);
void __msg0000_Available_20area(void);
void __msg0000_Backoff_20Level_(void);
void __msg0000_Bad_20digipeater(void);
void __msg0000_Bad_20syntax_2e_(void);
void __msg0000_Bad_20target_20_(void);
void __msg0000_Bad_20user_20or_(void);
void __msg0000_Base_20Addr_09Rx(void);
void __msg0000_Base_20Addr_20_2(void);
void __msg0000_Buf_20hits_2fmis(void);
void __msg0000_CTRL_2d_25c_0a(void);
void __msg0000_CWD_20_25s_0a(void);
void __msg0000_Calls_20originat(void);
void __msg0000_Calls_20timed_20(void);
void __msg0000_Can_27t_20NLST_2(void);
void __msg0000_Can_27t_20add_20(void);
void __msg0000_Can_27t_20attach(void);
void __msg0000_Can_27t_20change(void);
void __msg0000_Can_27t_20create(void);
void __msg0001_Can_27t_20create(void);
void __msg0002_Can_27t_20create(void);
void __msg0003_Can_27t_20create(void);
void __msg0004_Can_27t_20create(void);
void __msg0000_Can_27t_20delete(void);
void __msg0000_Can_27t_20detach(void);
void __msg0000_Can_27t_20list_2(void);
void __msg0000_Can_27t_20open_2(void);
void __msg0001_Can_27t_20open_2(void);
void __msg0002_Can_27t_20open_2(void);
void __msg0003_Can_27t_20open_2(void);
void __msg0000_Can_27t_20read_2(void);
void __msg0001_Can_27t_20read_2(void);
void __msg0002_Can_27t_20read_2(void);
void __msg0003_Can_27t_20read_2(void);
void __msg0004_Can_27t_20read_2(void);
void __msg0005_Can_27t_20read_2(void);
void __msg0000_Can_27t_20rename(void);
void __msg0000_Can_27t_20rewrit(void);
void __msg0000_Can_27t_20set_20(void);
void __msg0000_Can_27t_20write_(void);
void __msg0001_Can_27t_20write_(void);
void __msg0002_Can_27t_20write_(void);
void __msg0000_Ch_20Iface_20_20(void);
void __msg0000_Channel_20_2d_20(void);
void __msg0000_Closed_3a_20_25s(void);
void __msg0000_Code_20base_20se(void);
void __msg0000_Connect_20failed(void);
void __msg0001_Connect_20failed(void);
void __msg0000_Connected_0a(void);
void __msg0000_Connected_20to_2(void);
void __msg0000_Connected_2e(void);
void __msg0000_Connection_20fai(void);
void __msg0000_Could_20not_20cr(void);
void __msg0000_Couldn_27t_20que(void);
void __msg0000_Current_20messag(void);
void __msg0000_Currently_20enab(void);
void __msg0000_DISASTER_21_20Ou(void);
void __msg0000_DRSI_20Board_20S(void);
void __msg0000_Data_0a(void);
void __msg0000_Data_20outside_2(void);
void __msg0000_Data_20pointer_2(void);
void __msg0001_Data_20pointer_2(void);
void __msg0000_Defaulting_20to_(void);
void __msg0000_Del_5fconst_20_3(void);
void __msg0000_Dest_20Addr_20_2(void);
void __msg0000_Dialing_20not_20(void);
void __msg0000_Different_0a(void);
void __msg0000_Directory_20tabl(void);
void __msg0000_Disconnected_20(void);
void __msg0000_Disconnecting_2e(void);
void __msg0000_Dup_20acks_20_20(void);
void __msg0000_Duplex_3a_20_25s(void);
void __msg0000_EAGLE_20Board_20(void);
void __msg0000_EGATTACH_3a_20No(void);
void __msg0000_ESCC_20channel_2(void);
void __msg0001_ESCC_20channel_2(void);
void __msg0000_ESCC_20driver_20(void);
void __msg0001_ESCC_20driver_20(void);
void __msg0000_Encapsulation_20(void);
void __msg0000_Enqued_20_20_2d_(void);
void __msg0000_Enter_20message_(void);
void __msg0000_Enter_20pattern_(void);
void __msg0000_Enter_20text_3a_(void);
void __msg0000_Error_20in_20res(void);
void __msg0000_Error_20updating(void);
void __msg0000_Error_20writing_(void);
void __msg0000_Escape_20charact(void);
void __msg0000_Ether_3a_20len_2(void);
void __msg0000_Extra_20blocks_3(void);
void __msg0000_F_3e(void);
void __msg0000_FRAME_20TOO_20LA(void);
void __msg0000_Failed_0a(void);
void __msg0000_First_20init_20E(void);
void __msg0000_Forwarding_20tim(void);
void __msg0000_Frame_20out_20of(void);
void __msg0000_Frame_20within_2(void);
void __msg0000_Free_20address_2(void);
void __msg0000_Free_20cache_3a_(void);
void __msg0000_From_20_25s_20_2(void);
void __msg0000_From_3a_20_25s_0(void);
void __msg0000_Garbage_20bp_20_(void);
void __msg0000_Group_20list_20t(void);
void __msg0000_HAPN_20_25d_3a_2(void);
void __msg0000_HS_3a_20Warning_(void);
void __msg0000_Hardware_20_25u_(void);
void __msg0000_Host_20_25s_20_2(void);
void __msg0000_Host_20_25s_20un(void);
void __msg0000_Hostname_20set_2(void);
void __msg0000_ICMP_20from_20_2(void);
void __msg0000_ICMP_3a_20type_2(void);
void __msg0000_IP_0a(void);
void __msg0000_IP_20addr_20_20_(void);
void __msg0000_IP_3a_20CHECKSUM(void);
void __msg0000_IP_3a_20bad_20he(void);
void __msg0000_IP_3a_20len_20_2(void);
void __msg0000_IPCP_0a(void);
void __msg0000_IPCP_20_25s_0a(void);
void __msg0000_IRQ_20_25u_20out(void);
void __msg0000_Image_0a(void);
void __msg0000_Initializing_20C(void);
void __msg0000_Inqlen_3a_20_25d(void);
void __msg0000_Insert_20disk_20(void);
void __msg0000_Insufficient_20a(void);
void __msg0000_Interface_20_20_(void);
void __msg0000_Interface_20_22_(void);
void __msg0001_Interface_20_22_(void);
void __msg0002_Interface_20_22_(void);
void __msg0000_Interface_20_25s(void);
void __msg0001_Interface_20_25s(void);
void __msg0002_Interface_20_25s(void);
void __msg0003_Interface_20_25s(void);
void __msg0004_Interface_20_25s(void);
void __msg0005_Interface_20_25s(void);
void __msg0006_Interface_20_25s(void);
void __msg0000_Interface_20addr(void);
void __msg0000_Interface_3a_20(void);
void __msg0000_Invalid_20magic_(void);
void __msg0000_Invalid_20range_(void);
void __msg0000_Invalid_20type_2(void);
void __msg0000_Invalid_20user_2(void);
void __msg0000_Job_20id_20_25s_(void);
void __msg0000_KISS_3a_20(void);
void __msg0000_Known_20users_20(void);
void __msg0000_LCP_0a(void);
void __msg0000_LCP_20_25s_0a(void);
void __msg0000_Last_20message_0(void);
void __msg0000_Loc_20(void);
void __msg0000_Local_3a_20_25s(void);
void __msg0000_Local_3a_20_25s_(void);
void __msg0000_Lock_20directory(void);
void __msg0000_Logging_20off_0a(void);
void __msg0000_Logging_20to_20_(void);
void __msg0000_Logical_20bytesi(void);
void __msg0000_Login_20incorrec(void);
void __msg0000_MRU_20_25s_20_28(void);
void __msg0000_Mail_20area_3a_2(void);
void __msg0000_Mail_20box_20ful(void);
void __msg0000_Main_20commands_(void);
void __msg0000_Mbuf_20sizes_3a_(void);
void __msg0000_Message_20_23_25(void);
void __msg0000_Mode_20_25s_20un(void);
void __msg0001_Mode_20_25s_20un(void);
void __msg0000_Msg_20_25d_20Kil(void);
void __msg0000_Must_20be_20_200(void);
void __msg0001_Must_20be_20_200(void);
void __msg0000_Must_20be_20_3e_(void);
void __msg0000_Must_20set_20_27(void);
void __msg0000_N_3e_3d_255u_3a_(void);
void __msg0000_NET_2fROM_20Rout(void);
void __msg0000_NET_2fROM_20not_(void);
void __msg0000_NET_2fROM_3a_20_(void);
void __msg0000_NEWNEWS_20_25s_2(void);
void __msg0000_NNTP_20_25s_20Ca(void);
void __msg0001_NNTP_20_25s_20Ca(void);
void __msg0000_NNTP_20_25s_20Co(void);
void __msg0001_NNTP_20_25s_20Co(void);
void __msg0002_NNTP_20_25s_20Co(void);
void __msg0003_NNTP_20_25s_20Co(void);
void __msg0004_NNTP_20_25s_20Co(void);
void __msg0000_NNTP_20_25s_20Gi(void);
void __msg0000_NNTP_20_25s_20ba(void);
void __msg0000_NNTP_20_25s_20gi(void);
void __msg0001_NNTP_20_25s_20gi(void);
void __msg0000_NNTP_20_25s_20pr(void);
void __msg0000_NNTP_20Cannot_20(void);
void __msg0000_NNTP_20can_27t_2(void);
void __msg0000_NNTP_20daemon_20(void);
void __msg0001_NNTP_20daemon_20(void);
void __msg0002_NNTP_20daemon_20(void);
void __msg0000_NNTP_20group_20_(void);
void __msg0000_NNTP_20receive_2(void);
void __msg0000_NNTP_20received_(void);
void __msg0000_NNTP_20window_20(void);
void __msg0000_NO_20_2d_20(void);
void __msg0000_NO_20_2d_20bad_2(void);
void __msg0000_NO_20_2d_20synta(void);
void __msg0000_NULL_20BUFFER_0a(void);
void __msg0000_Net_3e_20(void);
void __msg0000_Network_20_25s_2(void);
void __msg0000_New_20mail_20arr(void);
void __msg0001_New_20mail_20arr(void);
void __msg0000_New_20news_20arr(void);
void __msg0001_New_20news_20arr(void);
void __msg0000_No_20HAPN_20adap(void);
void __msg0000_No_20current_20s(void);
void __msg0000_No_20dialer_20ac(void);
void __msg0000_No_20finger_20in(void);
void __msg0000_No_20help_20avai(void);
void __msg0000_No_20packet_20dr(void);
void __msg0000_No_20such_20mess(void);
void __msg0000_No_20such_20serv(void);
void __msg0000_Nodetimer_20_25l(void);
void __msg0000_None(void);
void __msg0000_None_0a(void);
void __msg0000_Normal_20AHDLC_2(void);
void __msg0000_Not_20a_20valid_(void);
void __msg0000_Not_20found_0a(void);
void __msg0000_Not_20in_20table(void);
void __msg0000_Not_20supported_(void);
void __msg0000_Nothing_20to_20T(void);
void __msg0000_Obsotimer_20_25l(void);
void __msg0000_Oh_2c_20hello_20(void);
void __msg0000_Only_20_25d_20DR(void);
void __msg0000_Only_20_25d_20ne(void);
void __msg0000_Only_201_20EAGLE(void);
void __msg0000_Outqlen_3a_20_25(void);
void __msg0000_PANIC_3a_20Timer(void);
void __msg0000_PANIC_3a_20buffe(void);
void __msg0000_PAP_0a(void);
void __msg0000_PAP_20_25s_0a(void);
void __msg0000_PASS_20_25s(void);
void __msg0000_PI_20Board_20Sta(void);
void __msg0000_PI_3a_20DMA_20_2(void);
void __msg0000_PI_3a_20DMA_20ch(void);
void __msg0000_PI_3a_20IRQ_20_2(void);
void __msg0000_PI_3a_20Interfac(void);
void __msg0000_PI_3a_20Maximum_(void);
void __msg0000_PI_3a_20Mode_20_(void);
void __msg0000_PI_3a_20No_20IP_(void);
void __msg0000_PI_3a_20No_20mem(void);
void __msg0001_PI_3a_20No_20mem(void);
void __msg0002_PI_3a_20No_20mem(void);
void __msg0003_PI_3a_20No_20mem(void);
void __msg0000_PI_3a_20Set_20my(void);
void __msg0000_PID_20_20_20_20_(void);
void __msg0000_POP_20users_20fi(void);
void __msg0000_PORT_20_25u_2c_2(void);
void __msg0000_PPP_3a_20len_20_(void);
void __msg0000_Packet_20driver_(void);
void __msg0000_Pap_0a(void);
void __msg0000_Parameter_20_25s(void);
void __msg0000_Password_3a_20(void);
void __msg0000_Password_3a_20_2(void);
void __msg0000_Path_3a_20(void);
void __msg0000_Persistence_3a_2(void);
void __msg0000_Pi_3a_20Initiali(void);
void __msg0000_Pinging_20_25s_0(void);
void __msg0000_Pinging_20resume(void);
void __msg0000_Pinging_20suspen(void);
void __msg0000_Please_20hang_20(void);
void __msg0000_Pool_20count_20_(void);
void __msg0000_Processing_20rec(void);
void __msg0000_QUIT_0a(void);
void __msg0000_Quality_20cannot(void);
void __msg0000_R_3a_25s_20_40_2(void);
void __msg0000_REQUEST(void);
void __msg0000_RESPONSE(void);
void __msg0000_RETURN_0a(void);
void __msg0000_RFrames_20_2d_20(void);
void __msg0000_RIP_20refused_20(void);
void __msg0000_RIP_3a_20(void);
void __msg0000_RIP_3a_20sent_20(void);
void __msg0000_RIPCMD_3a_20Unkn(void);
void __msg0000_RIPCMD_5fREQUEST(void);
void __msg0000_RIPCMD_5fRESPONS(void);
void __msg0000_RIP_5frx_3a_20No(void);
void __msg0000_Range_20for_20in(void);
void __msg0000_Reassembly_20fra(void);
void __msg0000_Reassembly_20que(void);
void __msg0000_Received_3a_20(void);
void __msg0000_Recording_20into(void);
void __msg0000_Recording_20off_(void);
void __msg0000_Recv_3a_20_20_20(void);
void __msg0000_Refuse_0a(void);
void __msg0000_Refusing_20annou(void);
void __msg0000_Removing_20frame(void);
void __msg0000_Requesting_2e_2e(void);
void __msg0000_Resolver_20_25s_(void);
void __msg0000_Resolving_20_25s(void);
void __msg0001_Resolving_20_25s(void);
void __msg0000_Returned_20(void);
void __msg0000_Route_20add_20fa(void);
void __msg0000_Routing_20lookup(void);
void __msg0000_Rxints_20_20_2d_(void);
void __msg0000_S_20_20_20_20_20(void);
void __msg0000_S_20command_20sy(void);
void __msg0000_S_23_20_20_20Typ(void);
void __msg0000_SMTP_20client_20(void);
void __msg0000_SP_20_3d_20_25p_(void);
void __msg0000_STOR_20_25s_0a(void);
void __msg0000_STOR_20_25s_3a_2(void);
void __msg0001_STOR_20_25s_3a_2(void);
void __msg0000_SUBCMD_20_2d_20D(void);
void __msg0000_SYST_0a(void);
void __msg0000_Same_0a(void);
void __msg0000_Send_20file_2c_2(void);
void __msg0000_Send_3a_20_2508l(void);
void __msg0000_Sequence_20_23_2(void);
void __msg0000_Server_20address(void);
void __msg0000_Session_20_25s_2(void);
void __msg0000_Slot_20time_3a_2(void);
void __msg0000_Socket_20not_20i(void);
void __msg0000_Sorry_20_2d_20th(void);
void __msg0000_Stack_20violatio(void);
void __msg0000_State_20is_20_25(void);
void __msg0000_Station_20_20_20(void);
void __msg0001_Station_20_20_20(void);
void __msg0000_String_20_25d_20(void);
void __msg0000_Subject_3a_20_25(void);
void __msg0000_Symbols_20read_3(void);
void __msg0000_System_20is_20ov(void);
void __msg0000_T1_3a_20(void);
void __msg0000_T3_3a_20(void);
void __msg0000_TACK_3a_20(void);
void __msg0000_TCB_20_25p_20_25(void);
void __msg0000_TCD_3a_20(void);
void __msg0000_TCP_20header_20c(void);
void __msg0000_TCP_3a_20_25u_2d(void);
void __msg0000_TChoke_3a_20(void);
void __msg0000_TX_20Delay_3a_20(void);
void __msg0000_TX_20Tail_20time(void);
void __msg0000_TYPE_20A_0a(void);
void __msg0000_TYPE_20I_0a(void);
void __msg0000_TYPE_20L_20_25d_(void);
void __msg0000_Target_20_20_20_(void);
void __msg0000_Thank_20you_20_2(void);
void __msg0000_The_20escape_20c(void);
void __msg0000_Time_20for_20100(void);
void __msg0000_Timeout_20value_(void);
void __msg0000_Timer_20_20_20_2(void);
void __msg0000_Tip_20session_20(void);
void __msg0000_To_3a_20_25s_0a(void);
void __msg0000_Too_20many_204po(void);
void __msg0000_Too_20many_20HAP(void);
void __msg0000_Too_20many_20asy(void);
void __msg0000_Too_20many_20dig(void);
void __msg0000_Too_20many_20hs_(void);
void __msg0000_Too_20many_20mai(void);
void __msg0000_Too_20many_20nrs(void);
void __msg0000_Too_20many_20pac(void);
void __msg0000_Too_20many_20pc1(void);
void __msg0000_Too_20many_20ses(void);
void __msg0000_Too_20many_20sli(void);
void __msg0000_Try_20to_20bounc(void);
void __msg0000_Trying_20_25s_2e(void);
void __msg0001_Trying_20_25s_2e(void);
void __msg0000_Trying_20Connect(void);
void __msg0000_Tstate_20_3d_20_(void);
void __msg0000_Turn_20off_20loc(void);
void __msg0000_Tx_20state_20_20(void);
void __msg0001_Tx_20state_20_20(void);
void __msg0000_TxBuffers_3a_20_(void);
void __msg0000_Txdefer_20_2d_20(void);
void __msg0000_UDP_3a(void);
void __msg0000_USER_20_25s(void);
void __msg0000_Unknown_20comman(void);
void __msg0000_Unknown_20encaps(void);
void __msg0000_Unknown_20parame(void);
void __msg0000_Uploading_20_25s(void);
void __msg0000_Uploading_20off_(void);
void __msg0000_Upper_20layers_2(void);
void __msg0000_Uptime_20_25s_20(void);
void __msg0001_Uptime_20_25s_20(void);
void __msg0000_Usage_3a_20_25s_(void);
void __msg0001_Usage_3a_20_25s_(void);
void __msg0000_Usage_3a_20F_20u(void);
void __msg0000_Usage_3a_20ax25_(void);
void __msg0000_Usage_3a_20dial_(void);
void __msg0000_Usage_3a_20mbox_(void);
void __msg0000_Usage_3a_20pop_2(void);
void __msg0000_Usage_3a_20smtp_(void);
void __msg0000_Use_20the_20ax25(void);
void __msg0000_User_20_20_20_20(void);
void __msg0000_User_20_25s_20no(void);
void __msg0000_VJ_20Compressed_(void);
void __msg0000_VJ_20Uncompresse(void);
void __msg0000_Valid_20options_(void);
void __msg0000_Value_20_25s_20_(void);
void __msg0000_WARNING_3a_20Loc(void);
void __msg0000_Warning_21_20Int(void);
void __msg0000_Warning_3a_20typ(void);
void __msg0000_Window_3a_20_25_(void);
void __msg0000_Wink_20DTR_3f_20(void);
void __msg0000_Writing_20articl(void);
void __msg0000_XMD5_20_25s_0a(void);
void __msg0000_XMKD_20_25s_0a(void);
void __msg0000_XRMD_20_25s_0a(void);
void __msg0000_You_20have_20(void);
void __msg0000_You_20have_20new(void);
void __msg0000_You_27re_20using(void);
void __msg0000_Zap_20failed_3a_(void);
void __msg0000__5bNET_2dHMR_24_(void);
void __msg0000__5bunknown_5d(void);
void __msg0000__5eC_0a(void);
void __msg0000__5eR_0a(void);
void __msg0000__5funlink_3a_20b(void);
void __msg0000_accept_0a(void);
void __msg0000_allocs_20_25lu_2(void);
void __msg0000_allow_20pap_20no(void);
void __msg0000_allow_20tcp_20no(void);
void __msg0000_anext_20pointer_(void);
void __msg0000_answer_20script_(void);
void __msg0000_bad_20destinatio(void);
void __msg0000_bad_20internet_2(void);
void __msg0000_bad_20neighbor_2(void);
void __msg0000_begin_20_2503o_2(void);
void __msg0000_bootp_20_5bnet_5(void);
void __msg0000_bootp_3a_20Net_5(void);
void __msg0000_bootp_3a_20timed(void);
void __msg0000_bp_20_25lx_20tot(void);
void __msg0000_by_20_25s_20_28_(void);
void __msg0000_by_20_25s_20with(void);
void __msg0000_can_27t_20read_2(void);
void __msg0000_code_20_25u_20ar(void);
void __msg0000_completed_20inte(void);
void __msg0000_compressed_20(void);
void __msg0000_control_3a_20_25(void);
void __msg0000_dcache_5fsearch_(void);
void __msg0000_delay_20_25lu_0a(void);
void __msg0000_dfile_5fsearch_3(void);
void __msg0001_dfile_5fsearch_3(void);
void __msg0000_dfile_5fupdate_3(void);
void __msg0001_dfile_5fupdate_3(void);
void __msg0002_dfile_5fupdate_3(void);
void __msg0000_disable_28_29_20(void);
void __msg0001_disable_28_29_20(void);
void __msg0002_disable_28_29_20(void);
void __msg0000_dns_5fquery_3a_2(void);
void __msg0001_dns_5fquery_3a_2(void);
void __msg0000_down_20script_3a(void);
void __msg0000_drtx_5fdefer_20_(void);
void __msg0001_drtx_5fdefer_20_(void);
void __msg0002_drtx_5fdefer_20_(void);
void __msg0003_drtx_5fdefer_20_(void);
void __msg0004_drtx_5fdefer_20_(void);
void __msg0000_drtx_5frrts_20_2(void);
void __msg0000_empty_20packet_2(void);
void __msg0000_enable_28_29_20r(void);
void __msg0000_escc_25d_3a_20No(void);
void __msg0000_expired(void);
void __msg0000_filter_20mode_20(void);
void __msg0000_fp_20_20_20_20_2(void);
void __msg0000_free_3a_20WARNIN(void);
void __msg0000_from_20_25s_20(void);
void __msg0000_from_20_25s_2ebb(void);
void __msg0000_ftp_3e_20syst_0a(void);
void __msg0000_garbage_20collec(void);
void __msg0000_heap_20size_20_2(void);
void __msg0000_id_20_25p_20name(void);
void __msg0000_ignored_20_28hol(void);
void __msg0000_input_20line_3a_(void);
void __msg0000_ip_5frecv_28_25s(void);
void __msg0000_ip_5fsend_0a(void);
void __msg0000_ksigs_20_25lu_20(void);
void __msg0000_kwaits_20_25lu_2(void);
void __msg0000_login_3a_20(void);
void __msg0000_maibox_20name_20(void);
void __msg0000_mailbox_20name_2(void);
void __msg0000_mailhost_20not_2(void);
void __msg0000_maximum_20route_(void);
void __msg0000_mbuf_20allocs_20(void);
void __msg0000_merge_20_25s_20_(void);
void __msg0000_metric_20better_(void);
void __msg0000_metric_20change_(void);
void __msg0000_metric_20not_20b(void);
void __msg0000_modes_20are_3a_2(void);
void __msg0000_netrom_20interfa(void);
void __msg0000_next_20pointer_2(void);
void __msg0000_no_20such_20rout(void);
void __msg0000_none_0a(void);
void __msg0000_normal_20_28_25s(void);
void __msg0000_nr4output_3a_20_(void);
void __msg0000_null_0a(void);
void __msg0000_packet_20delayed(void);
void __msg0000_packet_20duped_0(void);
void __msg0000_packet_20lost_0a(void);
void __msg0000_performing_20int(void);
void __msg0000_port_20_25d_3a_2(void);
void __msg0000_protocol_3a_20(void);
void __msg0000_pushdown_20calls(void);
void __msg0000_queue_20job_20_2(void);
void __msg0000_received_20_25u_(void);
void __msg0000_recv_3a_20do_20(void);
void __msg0000_recv_3a_20dont_2(void);
void __msg0000_recv_3a_20will_2(void);
void __msg0000_recv_3a_20wont_2(void);
void __msg0000_reject_0a(void);
void __msg0000_response_20id_20(void);
void __msg0000_restore_280_29_2(void);
void __msg0000_restore_281_29_2(void);
void __msg0000_route_20add_20_5(void);
void __msg0000_route_20drop_20_(void);
void __msg0000_route_20to_20sel(void);
void __msg0000_running(void);
void __msg0000_sbuf_3a_20buffer(void);
void __msg0000_sender(void);
void __msg0000_sending_20job_20(void);
void __msg0000_sent_3a_20do_20(void);
void __msg0000_sent_3a_20dont_2(void);
void __msg0000_sent_3a_20will_2(void);
void __msg0000_sent_3a_20wont_2(void);
void __msg0000_serial_20line_20(void);
void __msg0001_serial_20line_20(void);
void __msg0002_serial_20line_20(void);
void __msg0000_set_20mycall_20f(void);
void __msg0000_smtp_20daemon_20(void);
void __msg0000_smtp_20daemon_3a(void);
void __msg0000_smtp_20job_20_25(void);
void __msg0000_smtp_20mode_3a_2(void);
void __msg0000_smtp_20recv_3a_2(void);
void __msg0000_smtp_20sent_3a_2(void);
void __msg0000_some_20strange_2(void);
void __msg0000_spool_3a_20_25s_(void);
void __msg0000_src_20_25s(void);
void __msg0000_srtt_20_3d_20_25(void);
void __msg0000_standard_0a(void);
void __msg0000_stktrace_20from_(void);
void __msg0000_stop(void);
void __msg0000_stopped(void);
void __msg0000_target(void);
void __msg0000_timer_3a_20ints_(void);
void __msg0000_tip_20or_20diale(void);
void __msg0000_tmp_20file_3a_20(void);
void __msg0000_traceroute_20don(void);
void __msg0000_unknown_0a(void);
void __msg0000_unknown_200x_250(void);
void __msg0000_unknown_20hardwa(void);
void __msg0000_up_20script_3a_2(void);
void __msg0000_user_20abort_0a(void);
void __msg0000_username_20not_2(void);
void __msg0000_valid_20subcomma(void);





extern struct cmds Cmds[],Startcmds[],Stopcmds[],Attab[];







































static char Escape = 0x1d;	




char Badhost[] = "Unknown host %s\n";


char *Hostname;


char Nospace[] = "No space!!\n";	


struct proc *Cmdpp;


struct proc *Display;


char *Cmdline;				


int main_exit = 0;			



static char Prompt[] = "net> ";


static FILE *Logfp;


static time_t StartTime;		


static int Verbose;


static int keychar(int c);
static void pass(char *,int len);
static void passchar(int c);


int
main(argc,argv)
int argc;
char *argv[];
{
	FILE *fp;
	struct daemon *tp;
	int c;
	char cmdbuf[256];

	long hinit = 2048	*2;












	amess("kernel: parameters:");
	if (argc < 2)
		{
		amess(" none");
		}
	else
		{
		for (c = 1; c < argc; c++)
			{
			abyte(' ');
			amess(argv[c]);
			}
		}
	acrlf();




	StartTime = time(&StartTime);

	while((c = getopt(argc,argv,"f:s:d:bvh:")) != (-1)){
		switch(c){
		case 'h':	
			hinit = atol(optarg);
			break;

		case 'm':	
			Memthresh = atol(optarg);
			break;

		case 'f':	
			Nfiles = atoi(optarg);
			break;
		case 's':	
			Nsock = atoi(optarg);
			break;
		case 'd':	
			initroot(optarg);
			break;





		case 'v':
			Verbose = 1;
			break;
		}
	}
	



	free(malloc(hinit)); 


	kinit();
	ipinit();
	ioinit();
	sockinit();



	Cmdpp = mainproc("main"); 




	
	stdin = diagopen(1, "rt"); 
	stdout = diagopen(1, "wt"); 
	_printf(_("\n")); 
	fflush(stdout); 








	Command = Lastcurr = newsession("console",COMMAND,1); 

	Display = newproc("display",350,display,0,0,0,0);

	_printf(_(

	       "Hytech "

	       "KA9Q NOS kernel\n"
	       "Version %s\n"

	       "Compiled for Zilog Z8S180 CPU\n"




	       "\n"
	       "Copyright 1986-1995 by Phil Karn, KA9Q\n"

	       "Copyright 2003 by Nick Downing, Hytech\n"

	       "\n"),
	       Version);
	fflush(stdout);

 abyte('F');
	xfs_init(0);

 abyte('G');
	usercvt();
	

	for(tp=Daemons;;tp++){

		if(tp->name == 0)
			break;
		newproc(tp->name,tp->stksize,tp->fp,0,0,0,0);
	}

	Encap.txproc = newproc("encap tx",512,if_tx,0,&Encap,0,0);

	if(optind < argc){
		
		if((fp = __fopen((argv[optind]), -1, (FILE*)0, ("r"))) == 0){
			_fprintf(stderr, _("Can't read config file "));
			perror(argv[optind]);
		}
	} else {
		fp = __fopen((Startup), -1, (FILE*)0, ("r"));
	}

	if(fp != 0){
		while(fgets(cmdbuf,sizeof(cmdbuf),fp) != 0){
			rip(cmdbuf);
			if(Cmdline != 0)
				free(Cmdline);
			Cmdline = strdupw(cmdbuf);
			if(Verbose)
				_printf(_("%s\n"),Cmdline);
			if(cmdparse(Cmds,cmdbuf,0) != 0){
				_printf(_("input line: %s\n"),Cmdline);
			}
		}
		fclose(fp);
	}
	

	for(;;){
		printf(Prompt);
		fflush(stdout);
		if(fgets(cmdbuf,sizeof(cmdbuf),stdin) != 0){
			rip(cmdbuf);
			if(Cmdline)
				free(Cmdline);
			Cmdline = strdupw(cmdbuf);
			(void)cmdparse(Cmds,cmdbuf,Lastcurr);
		}

 else
  {
  kwait(0); 
  }

	}
}




void
keyboard(i,v1,v2)
int i;
void *v1;
void *v2;
{
	int c;
	int j;

	
loop:

	c = fgetc(Current->keyboard);
	while (c == (-1))
		{
		kwait(0); 
		c = fgetc(Current->keyboard); 
		}




	if(c == Escape && Escape != 0 && Current != Command){
		
		Lastcurr = Current;
		Current = Command;
		alert(Display,1);
		goto loop;
	}

	passchar(c);
	goto loop;
}



static void
pass(s,len)
char *s;
int len;
{
	while(len-- != 0)
		passchar(*s++);
}



static void
passchar(c)
int c;
{
	int cnt;

	


	if(Current->inproc != 0 && (*Current->inproc)(c) == 0)
		return;

	
	if((cnt = ttydriv(Current,(char)c)) != 0){
		
		fwrite(Current->ttystate.line,1,cnt,Current->input);
		fflush(Current->input);
	}
}




int
dorepeat(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	int32 interval;
	int ret;
	struct session *sp;

	if((!!((__ctype[(unsigned char)(argv[1][0])])&0x08		))){
		interval = atol(argv[1]);
		argc--;
		argv++;
	} else {
		interval = 55		;
	}
	if((sp = newsession(Cmdline,REPEAT,1)) == 0){
		_printf(_("Too many sessions\n"));
		return 1;
	}
	sp->inproc = keychar;	
	


	setvbuf(sp->output,0,0x00	,2048);
	while(sp->inproc == keychar){	
		_printf(_("%c[2J"),0x1b);	
		ret = subcmd(Cmds,argc,argv,p);
		fflush(sp->output);
		if(ret != 0 || ppause(interval) == -1)
			break;
	}
	keywait(0,1);
	freesession(sp);
	return 0;
}



static int
keychar(c)
int c;
{
	if(c != 0x3)
		return 1;	

	_fprintf(Current->output, _("^C\n"));
	alert(Current->proc,107);
	Current->inproc = 0;
	return 0;
}



int
dodelete(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	int i;

	for(i=1;i < argc; i++){
		if(unlink(argv[i]) == -1){
			_printf(_("Can't delete %s"),argv[i]);
			perror("");
		}
	}
	return 0;
}



int
dorename(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	if(rename(argv[1],argv[2]) == -1){
		_printf(_("Can't rename %s"),argv[1]);
		perror("");
	}
	return 0;
}



int
doexit(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	int i;
	time_t StopTime;
	struct session *sp;

	StopTime = time(&StopTime);
	main_exit = 1;	
	
	for(i=0;i<Nsessions;i++){
		if((sp = Sessions[i]) == 0)
			continue;
		alert(sp->proc,107);
		alert(sp->proc1,107);
		alert(sp->proc2,107);
	}
	reset_all();
	if(Dfile_updater != 0)
		alert(Dfile_updater,0);	
	for(i=0;i<100;i++)
		kwait(0);	
	shuttrace();
	logmsg(-1,"NOS was stopped at %s", ctime(&StopTime));
	if(Logfp){
		fclose(Logfp);
		Logfp = 0;
	}

 abyte('X');
	xfs_end();
 abyte('Y');



	iostop();
	exit(0);
	return 0;	
}



int
dohostname(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	if(argc < 2)
		_printf(_("%s\n"),Hostname);
	else {
		struct iface *ifp;
		char *name;

		if((ifp = if_lookup(argv[1])) != 0){
			if((name = resolve_a(ifp->addr, 0)) == 0){
				_printf(_("Interface address not resolved\n"));
				return 1;
			} else {
				if(Hostname != 0)
					free(Hostname);
				Hostname = name;

				
				if ( Hostname[strlen(Hostname)] == '.' ) {
					Hostname[strlen(Hostname)] = '\0';
				}
				_printf(_("Hostname set to %s\n"), name );
			}
		} else {
			if(Hostname != 0)
				free(Hostname);
			Hostname = strdupw(argv[1]);
		}
	}
	return 0;
}



int
dolog(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	static char *logname;

	if(argc < 2){
		if(Logfp)
			_printf(_("Logging to %s\n"),logname);
		else
			_printf(_("Logging off\n"));
		return 0;
	}
	if(Logfp){
		logmsg(-1,"NOS log closed");
		fclose(Logfp);
		Logfp = 0;
		free(logname);
		logname = 0;
	}
	if(strcmp(argv[1],"stop") != 0){
		logname = strdupw(argv[1]);
		Logfp = __fopen((logname), -1, (FILE*)0, ("a+"));
		logmsg(-1,"NOS was started at %s", ctime(&StartTime));
	}
	return 0;
}



int
dohelp(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct cmds *cmdp;
	int i;
	char buf[66];

	_printf(_("Main commands:\n"));
	memset(buf,' ',sizeof(buf));
	buf[64] = '\n';
	buf[65] = '\0';
	for(i=0,cmdp = Cmds;cmdp->name != 0;cmdp++,i = (i+1)%4){
		strncpy(&buf[i*16],cmdp->name,strlen(cmdp->name));
		if(i == 3){
			printf(buf);
			memset(buf,' ',sizeof(buf));
			buf[64] = '\n';
			buf[65] = '\0';
		}
	}
	if(i != 0)
		printf(buf);
	return 0;
}







int
doattach(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return subcmd(Attab,argc,argv,p);
}




int
doparam(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct iface *ifp;
	int param;
	int32 val;

	if((ifp = if_lookup(argv[1])) == 0){
		_printf(_("Interface \"%s\" unknown\n"),argv[1]);
		return 1;
	}
	if(ifp->ioctl == 0){
		_printf(_("Not supported\n"));
		return 1;
	}
	if(argc < 3){
		for(param=1;param<=16;param++){
			val = (*ifp->ioctl)(ifp,param,0,0L);
			if(val != -1)
				_printf(_("%s: %ld\n"),parmname(param),val);
		}
		return 0;
	}
	if((param = devparam(argv[2])) == -1){
		_printf(_("Unknown parameter %s\n"),argv[2]);
		return 1;
	}
	if(argc < 4){
		
		val = (*ifp->ioctl)(ifp,param,0,0L);
		if(val == -1){
			_printf(_("Parameter %s not supported\n"),argv[2]);
		} else {
			_printf(_("%s: %ld\n"),parmname(param),val);
		}
		return 0;
	}
	
	(*ifp->ioctl)(ifp,param,1,atol(argv[3]));
	return 0;
}




int
doescape(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	if(argc < 2)
		_printf(_("0x%x\n"),Escape);
	else
		Escape = *argv[1];
	return 0;
}








int
doremote(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct sockaddr_in fsock;
	int s,c;
	uint8 *data,x;
	uint16 port,len;
	char *key = 0;
	int klen;
	int32 addr = 0;
	char *cmd,*host;

	port = 1234	;	
	optind = 1;		
	while((c = getopt(argc,argv,"a:p:k:s:")) != (-1)){
		switch(c){
		case 'a':
			addr = resolve(optarg);
			break;
		case 'p':
			port = atoi(optarg);
			break;
		case 'k':
			key = optarg;
			klen = strlen(key);
			break;





		}
	}
	if(optind > argc - 2){
		_printf(_("Insufficient args\n"));
		return -1;
	}
	host = argv[optind++];
	cmd = argv[optind];
	if((s = socket(0,1,0)) == -1){
		perror("socket failed");
		return 1;
	}
	len = 1;
	
	if(addr != 0)
		len += sizeof(int32);

	if(key != 0)
		len += klen;

	if(len == 1)
		data = &x;
	else
		data = mallocw(len);

	fsock.sin_family = 0;
	fsock.sin_addr.s_addr = resolve(host);
	fsock.sin_port = port;

	switch(cmd[0]){
	case 'r':
		data[0] = 1;
		if(key != 0)
			strncpy((char *)&data[1],key,klen);
		break;
	case 'e':
		data[0] = 2;
		if(key != 0)
			strncpy((char *)&data[1],key,klen);
		break;
	case 'k':
		data[0] = 3;
		if(addr != 0)
			put32(&data[1],addr);
		break;
	default:
		_printf(_("Unknown command %s\n"),cmd);
		goto cleanup;
	}
	
	if(sendto(s,data,len,0,(struct sockaddr *)&fsock,sizeof(fsock)) == -1){
		perror("sendto failed");
		goto cleanup;
	}
cleanup:
	if(data != &x)
		free(data);
	close_s(s);
	return 0;
}




int
dopage(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	FILE *fp;
	FILE *outsav;

	fp = tmpfile();
	outsav = stdout;
	stdout = fp;
	subcmd(Cmds,argc,argv,p);
	stdout = outsav;
	newproc("view",512,view,0,(void *)fp,0,0);	
	return 0;
}




int
dodebug(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	setbool(&Kdebug,"kernel debug",argc,argv);
	return 0;
}




int
dowipe(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	setbool(&_clrtmp,"tmp file wiping",argc,argv);
	return 0;
}




int
donothing(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return 0;
}






void
logmsg(int s,char *fmt, ...)
{
	va_list ap;
	char *cp;

	time_t t;



	int i;
	struct sockaddr fsocket;




	if(Logfp == 0)
		return;

	time(&t);
	cp = ctime(&t);
	rip(cp);
	i = (sizeof(struct sockaddr));
	_fprintf(Logfp, _("%s"),cp);
	if(getpeername(s,&fsocket,&i) != -1)
		_fprintf(Logfp, _(" %s"),psocket(&fsocket));

	_fprintf(Logfp, _(" - "));
	(ap = (char *) (&(fmt)+1));
	vfprintf(Logfp, fmt,ap);
	;
	_fprintf(Logfp, _("\n"));
	fflush(Logfp);






}

