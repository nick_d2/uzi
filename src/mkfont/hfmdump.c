/* hfmdump.c by Nick for Hytech Font Manager system */

/* ------------------------------------------------------------------------- */
/* header and preprocessor directives */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef DEBUG
#define DEBUG 0
#endif

#include "hfmlib.h"

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#define FONT_MAX	0x100

/* ------------------------------------------------------------------------- */
/* function prototypes */

int main(int argc, char **argv);
static int walk_tree(void);
static int try_query(void);

/* ------------------------------------------------------------------------- */
/* global variables */

int family_found[FONT_MAX];
int height_found[FONT_MAX];
int style_found[FONT_MAX]; /* note:  too many fonts in .hfm will cause crash */

int query, device, height_min, height_max, family, style;

/* ------------------------------------------------------------------------- */

int main(int argc, char **argv)
	{
	if (argc < 2)
		{
		printf("usage: hfmdump infile.hfm"
		       " [device height_min height_max family style]\n");
		return 1;
		}

	if (argc < 3)
		{
		query = 0;
		}
	else if (argc == 7)
		{
		query = 1;

		device = atoi(argv[2]);
		height_min = atoi(argv[3]);
		height_max = atoi(argv[4]);

		family = hfm_family_string_to_int(argv[5]);
		if (family < 0)
			{
			printf("Family must be one of:\n"
			       "\t" HFMSTR_SANS "\n"
			       "\t" HFMSTR_SERIF "\n");
			return 1;
			}

		style = hfm_style_string_to_int(argv[6]);
		if (style < 0)
			{
			printf("Style must be one of:\n"
			       "\t" HFMSTR_CONDENSED "\n"
			       "\t" HFMSTR_REGULAR "\n"
			       "\t" HFMSTR_BOLD "\n"
			       "\t" HFMSTR_ITALIC "\n"
			       "\t" HFMSTR_UNDERLINE "\n");
			return 1;
			}
		}
	else
		{
		printf("Invalid number of parameters\n");
		return 1;
		}

	if (hfm_load(argv[1]) == FALSE)
		{
		return 1;
		}

	if (walk_tree() == FALSE)
		{
		hfm_free();
		return 1;
		}

	if (query && try_query() == FALSE)
		{
		hfm_free();
		return 1;
		}

	hfm_free();
	return 0;
	}

/* ------------------------------------------------------------------------- */

static int walk_tree(void)
	{
	hfm_device_t *hfmd;
	int hfmd_index, hfmd_count;

	hfm_slot_t *hfmsl;
	int hfmsl_index, hfmsl_count;

	hfm_family_t *hfmf;
	int hfmf_index, hfmf_count;

	hfm_style_t *hfmst;
	int hfmst_index, hfmst_count;

	hfmd_count = hfm->hfm_device_count;
	hfmd = (hfm_device_t *) HFM_DEREF(hfm, hfm->hfm_device_table);
	for (hfmd_index = 0; hfmd_index < hfmd_count; hfmd_index++)
		{
		hfmsl_count = hfmd->hfmd_slot_count;
		hfmsl = (hfm_slot_t *)
			HFM_DEREF(hfm, hfmd->hfmd_slot_table);

		hfmf_count = hfmd->hfmd_family_count;
		hfmf = (hfm_family_t *)
		       HFM_DEREF(hfm, hfmd->hfmd_family_table);

		if (query == 0)
			{
			printf("Device %d (slots %d, families %d)\n",
			       hfmd_index, hfmsl_count, hfmf_count);

			for (hfmsl_index = 0; hfmsl_index < hfmsl_count; hfmsl_index++)
				{
				printf("\tSlot %d: "
				       " range=%d,%d"
				       " width=0x%04x bitmap=0x%04x"
				       " total=%d,%d space=%d,%d\n",
				       hfmsl_index,
				       hfmsl->hfmsl_char_first,
				       hfmsl->hfmsl_char_count,
				       hfmsl->hfmsl_width_table,
				       hfmsl->hfmsl_bitmap_table,
				       hfmsl->hfmsl_x_total,
				       hfmsl->hfmsl_y_total,
				       hfmsl->hfmsl_x_space,
				       hfmsl->hfmsl_y_space);

				hfmsl++;
				}
			}

		for (hfmf_index = 0; hfmf_index < hfmf_count; hfmf_index++)
			{
			hfmst_count = hfmf->hfmf_style_count;
			hfmst = (hfm_style_t *)
				HFM_DEREF(hfm, hfmf->hfmf_style_table);

			if (query == 0)
				{
				printf("\tFamily %d \"%s\" (styles %d)\n",
				       hfmf_index,
				       hfm_family_int_to_string(hfmf_index),
				       hfmst_count);
				}

			for (hfmst_index = 0; hfmst_index < hfmst_count;
					      hfmst_index++)
				{
				if (query == 0)
					{
					printf("\t\tStyle %d: "
					       " height=%d style=%d \"%s\""
					       " slot=%d\n",
					       hfmst_index,
					       hfmst->hfmst_height,
					       hfmst->hfmst_style,
					       hfm_style_int_to_string(
							hfmst->hfmst_style),
					       hfmst->hfmst_slot);
					}

				if (query && hfmd_index == device)
					{
					family_found[hfmst->hfmst_slot] =
							hfmf_index;
					height_found[hfmst->hfmst_slot] =
							hfmst->hfmst_height;
					style_found[hfmst->hfmst_slot] =
							hfmst->hfmst_style;
					}

				hfmst++;
				}

			hfmf++;
			}

		if (query == 0)
			{
			printf("\n");
			}

		hfmd++;
		}

	return TRUE;
	}

/* ------------------------------------------------------------------------- */

static int try_query(void)
	{
	hfm_style_t *hfmst;
	int hfmst_index, hfmst_count;

	hfm_family_t hfmf;

	int slot;
	hfm_slot_t *hfmsl;

	printf("Searching for"
	       " device=%d family=%d \"%s\""
	       " height_min=%d height_max=%d\n",
	       device, family, hfm_family_int_to_string(family),
	       height_min, height_max);

	if (hfm_search_family_auto(device, family,
				   height_min, height_max, &hfmf) == FALSE)
		{
		printf("Intersection of family and height is empty\n");
		return FALSE;
		}

	hfmst_count = hfmf.hfmf_style_count;
	hfmst = (hfm_style_t *) HFM_DEREF(hfm, hfmf.hfmf_style_table);

	printf("\tFound %d possibilities\n", hfmst_count);

	for (hfmst_index = 0; hfmst_index < hfmst_count; hfmst_index++)
		{
		printf("\t\tStyle %d: "
		       " height=%d style=%d \"%s\" slot=%d\n",
		       hfmst_index,
		       hfmst->hfmst_height,
		       hfmst->hfmst_style,
		       hfm_style_int_to_string(hfmst->hfmst_style),
		       hfmst->hfmst_slot);

		hfmst++;
		}

	printf("\n");

	printf("Searching within the returned results for style=%d \"%s\"\n",
	       style, hfm_style_int_to_string(style));

	if (hfm_search_style_auto(&hfmf, style, &slot) == FALSE)
		{
		printf("Style is not available for this family and height\n");
		return FALSE;
		}

	hfmsl = hfm_locate(device, slot);

	printf("\tSlot %d: "
	       " range=%d,%d"
	       " width=0x%04x bitmap=0x%04x"
	       " total=%d,%d space=%d,%d\n",
	       slot,
	       hfmsl->hfmsl_char_first,
	       hfmsl->hfmsl_char_count,
	       hfmsl->hfmsl_width_table,
	       hfmsl->hfmsl_bitmap_table,
	       hfmsl->hfmsl_x_total,
	       hfmsl->hfmsl_y_total,
	       hfmsl->hfmsl_x_space,
	       hfmsl->hfmsl_y_space);

	printf("\tReverse lookup on slot %d: "
	       " height=%d family=%d \"%s\" style=%d \"%s\"\n",
	       slot,
	       height_found[slot],
	       family_found[slot],
	       hfm_family_int_to_string(family_found[slot]),
	       style_found[slot],
	       hfm_style_int_to_string(style_found[slot]));

	printf("\n");

	return TRUE;
	}

/* ------------------------------------------------------------------------- */

