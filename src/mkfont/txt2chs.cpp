// txt2chs.cpp

// ----------------------------------------------------------------------------

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>
#pragma hdrstop

#ifndef DEBUG
#define DEBUG 0
#endif

#include "hymem.h"
#include "hyfs.h"
#include "hyfile.h"
#include "hylist.h"
#include "hycache.h"
#include "txt2chs.h"

// ----------------------------------------------------------------------------
// local preprocessor definitions

#define LINE_MAX	0x200
#define PATH_MAX	0x200

#define CACHE_MAX	0x10 // note:  this is smaller than hfmtool's setting

#define OUTPUT_BUFFER	0x10000 // to create output files of up to 64 kbytes

// ----------------------------------------------------------------------------

#ifdef STANDALONE
int main(int argc, char **argv);
#endif
static int fiProcess(FILETAG *pftOut, FILETAG *pftIn, int iICS, int iILS);
static int iCompare(const void *pvLeft, const void *pvRight);
static int iUpdateCRC(char *pcBase, int iCount, int iCRC);

// ----------------------------------------------------------------------------

#ifdef STANDALONE
int main(int argc, char **argv)
	{
	int i;
        LISTTAG ltCache;

	// set up the cache to track intermediate memory files
        CacheEntrySetup(&ltCache, CACHE_MAX);

	// run the real program, passing in the cache descriptor
	i = iTxt2Chs(&ltCache, argc, argv);

	// it just goes on and on my friend
	CacheExitFlush(&ltCache, 3); // force writing of intermediate files
	CacheExitCleanup(&ltCache); // deallocate memory, though it's redundant

	return i;
	}
#endif

// ----------------------------------------------------------------------------

int iTxt2Chs(LISTTAG *pltCache, int argc, char **argv)
	{
	int i, iICS, iILS;

	int iCacheIn;
	FILETAG *pftIn;
	char *pszInFileName;

	int iCacheOut;
	FILETAG *pftOut;
	char *pszOutFileName;
	char szOutFileName[PATH_MAX];

	pszInFileName = NULL;
	pszOutFileName = NULL;

	if (argc > 1)
		{
		pszInFileName = argv[1];
		}

	iICS = 0;
	iILS = 0;
	if (argc > 2)
		{
		i = atoi(argv[2]);
		if (i < 1 || i > 0xff)
			{
			pszOutFileName = argv[2];
			if (argc > 3)
				{
				iICS = atoi(argv[3]);
				if (argc > 4)
					{
					iILS = atoi(argv[4]);
					}
				}
			}
		else
			{
			iICS = i;
			if (argc > 3)
				{
				iILS = atoi(argv[3]);
				}
			}
		}

	if (pszInFileName == NULL)
		{
		printf("usage: txt2chs infile.txt [outfile.chs] [ICS] [ILS]\n");
		exit(1);
		}

	if (pszOutFileName == NULL)
		{
		pszOutFileName = szOutFileName;
		strcpy(pszOutFileName, pszInFileName);

		i = strlen(pszOutFileName);
		while (i--)
			{
			if (pszOutFileName[i] == '\\')
				{
				break; /* no extension, so don't strip it */
				}
			if (pszOutFileName[i] == '.')
				{
				pszOutFileName[i] = 0; /* strip dot and extension */
				break; /* ready to concatenate our extension */
				}
			}

		strcat(pszOutFileName, ".chs");
		}

	if (!strcmp(pszInFileName, pszOutFileName))
		{
		printf("Input and output filenames identical\n");
		exit(1);
		}

	// read the input txt file entirely to a malloc'd block
	CacheReadIn(pltCache, &iCacheIn, pszInFileName);
	pftIn = pftCacheItem(pltCache, iCacheIn);

	// prepare an output buffer for character set data
	CacheAllocate(pltCache, &iCacheOut, OUTPUT_BUFFER);
	pftOut = pftCacheItem(pltCache, iCacheOut);

	// search for characters and write character set files
	if (fiProcess(pftOut, pftIn, iICS, iILS) == FALSE)
		{
		exit(1);
		}

	// ready to write the output we found
	CacheWriteOut(pltCache, iCacheOut, pszOutFileName);

	// and now they'll all be singing it forever just because
	return 0;
	}

// ----------------------------------------------------------------------------

static int fiProcess(FILETAG *pftOut, FILETAG *pftIn, int iICS, int iILS)
	{
	char *pc;
	char sz[LINE_MAX+3];
	int i, j, c, d;
	int x, y, cx, cy;
	int tx, ty, bx, fx;
	int x0, x1, x2, y0, y1, y2;
	int mx, my;
	MEMTAG mt;
	unsigned short *pus;
	int iValue, iMask;
	int iWidth[0x100], iHeight[0x100];
	int iCRC;

	pc = pftOut->pcBase;

	strcpy(pc, "XXXXYYMMDDHHMMSS");
	pc += 0x10;

	pus = (unsigned short *)pc;
	memset(pc, 0, 0x208);
	pc += 0x208;

	// create the microspace characters
	*pc++ = 1; // inter character space should be ignored here
	pus[1] = pc - (char *)pus;
	*pc++ = 0x20; // indicates we have width but nothing else

	*pc++ = 2; // inter character space should be ignored here
	pus[2] = pc - (char *)pus;
	*pc++ = 0x20; // indicates we have width but nothing else

	*pc++ = 3; // inter character space should be ignored here
	pus[3] = pc - (char *)pus;
	*pc++ = 0x20; // indicates we have width but nothing else

	*pc++ = 4; // inter character space should be ignored here
	pus[4] = pc - (char *)pus;
	*pc++ = 0x20; // indicates we have width but nothing else

	*pc++ = -4; // inter character space should be ignored here
	pus[5] = pc - (char *)pus;
	*pc++ = 0x20; // indicates we have width but nothing else

	*pc++ = -3; // inter character space should be ignored here
	pus[6] = pc - (char *)pus;
	*pc++ = 0x20; // indicates we have width but nothing else

	*pc++ = -2; // inter character space should be ignored here
	pus[7] = pc - (char *)pus;
	*pc++ = 0x20; // indicates we have width but nothing else

	*pc++ = -1; // inter character space should be ignored here
	pus[8] = pc - (char *)pus;
	*pc++ = 0x20; // indicates we have width but nothing else

	*pc++ = 12; // inter character space should be ignored here
	pus[0x1c] = pc - (char *)pus;
	*pc++ = 0x20; // indicates we have width but nothing else

	*pc++ = -12; // inter character space should be ignored here
	pus[0x1d] = pc - (char *)pus;
	*pc++ = 0x20; // indicates we have width but nothing else

	if (iICS < 1 || iICS > 0xff)
		{
		iICS = 1;
		}

	if (iILS < 1 || iILS > 0xff)
		{
		iILS = 1;
		}

	c = '!';
	d = 0;
	while (fiFileGetLine(pftIn, sz, sizeof(sz) - 1, NULL))
		{
		if (sscanf(sz, "at %d,%d size %d,%d", &x, &y, &cx, &cy) == 4)
			{
			/*printf("at %d,%d size %d,%d\n", x, y, cx, cy);*/

			if (c >= 0x100 || d >= 0x100)
				{
				printf("Too many characters\n");
				break; /* exit(1); */
				}

			MemAlloc(&mt, cx * cy);
			i = 0;

			x0 = cx;
			x1 = 0;
			x2 = 0;
			y0 = cy;
			y1 = 0;
			y2 = cy;
			for (y = 0; y < cy; y++)
				{
				if (fiFileGetLine(pftIn, sz, sizeof(sz) - 1,
						  &j) == FALSE)
					{
					printf("Unexpected end of file\n");
					break; /* exit(1); */
					}

				if (j < (cx * 2))
					{
					printf("Line too short in file\n");
					break; /* exit(1); */
					}

				for (x = 0; x < cx; x++)
					{
					if (sz[x * 2] == '#')
						{
						mt.pcBase[i++] = 1;
						x0 = min(x0, x);
						x1 = max(x1, x + 1);
						y0 = min(y0, y);
						y1 = max(y1, y + 1);
						}
					else
						{
						mt.pcBase[i++] = 0;
						if (sz[x * 2] == '_')
							{
							y2 = y + 1;
							}
						}
					}
				}

			tx = max(0, x1 - x0); // trimmed width in pixels
			ty = max(0, y1 - y0); // trimmed height in pixels
#if DEBUG
			printf("at %d,%d trim %d,%d origin %d,%d\n",
			       x0, y0, tx, ty, x2, y2);
#endif

			iWidth[d] = tx; // save for median calculation
			iHeight[d] = ty; // save for median calculation

			bx = (tx + 7) >> 3; // width in bytes

			if (x2)
				{
				*pc++ = x0 - x2;
				*pc++ = y0 - y2;
				*pc++ = tx + iICS;
				pus[c] = pc - (char *)pus;
				*pc++ = bx | 0xe0;
				}
			else if (y2)
				{
				*pc++ = y0 - y2;
				*pc++ = tx + iICS;
				pus[c] = pc - (char *)pus;
				*pc++ = bx | 0x60;
				}
			else
				{
				*pc++ = tx + iICS;
				pus[c] = pc - (char *)pus;
				*pc++ = bx | 0x20;
				}

			if (bx)
				{
				*pc++ = ty;

				i = y0 * cx;
				for (y = y0; y < y1; y++)
					{
					j = i + x0;
					i += cx;

					iValue = 0;
					iMask = 0x80;
					for (x = x0; x < x1; x++)
						{
						if (iMask == 0)
							{
							*pc++ = iValue;

							iValue = 0;
							iMask = 0x80;
							}

						if (mt.pcBase[j++])
							{
							iValue |= iMask;
							}

						iMask >>= 1;
						}

					*pc++ = iValue;
					}
				}

			MemFree(&mt);

			c++; // character code for index updating
			d++; // character code for median calculation
			}
		}

	// post process widths/heights now we've had all characters
	mx = 0;
	my = 0;
	if (d)
		{
		qsort(iWidth, d, sizeof(int), iCompare);
		mx = iWidth[d / 2]; // find median width

		qsort(iHeight, d, sizeof(int), iCompare);
		my = iHeight[d / 2]; // find median height
		}

	printf("Success: "
	       " range=%d,%d median=%d,%d total=%d,%d space=%d,%d\n",
	       0x21, d, mx, my, mx + iICS, my + iILS, iICS, iILS);

	// create the space character (now that we have width)
#if 1 // unused chars must end up with their width = x cell width!!
	*pc++ = mx + iICS;
#else
	*pc++ = mx; // should be mx + iICS but we cheat a little
#endif
	pus[' '] = pc - (char *)pus;
	*pc++ = 0x20; // indicates we have width but nothing else

	// set file length, as all data has now been appended
	pftOut->pcAppend = pc;

	// fill out the table with minus signs for undefined chars
	for (c = 0; c < 0x100; c++)
		{
		if (pus[c] == 0)
			{
#if 1 // always fill out table with spaces, as width must = x cell width
			pus[c] = pus[' '];
#else
			pus[c] = pus['-'];
			if (pus[c] == 0)
				{
				pus[c] = pus[' ']; // or space if no minus
				}
#endif
			}
		}

	// fill in header fields that weren't known until now
	pc = (char *)(pus + 0x100);
	*pc++ = mx + iICS;
	*pc++ = my + iILS;
	*pc++ = iICS;
	*pc++ = iILS;
	*pc++ = 'O'; // character to use for uncrossed zero
	*pc++ = 0; // spare
	*(unsigned short *)pc = iFileSize(pftOut);

	// calculate and insert CRC using character set algorithm
	pc = pftOut->pcBase + 4;
	iCRC = iUpdateCRC(pc, pftOut->pcAppend - pc, 0xffff);

	pc = pftOut->pcBase;
	for (i = 0; i < 4; i++)
		{
		*pc++ = "0123456789ABCDEF"[(iCRC >> 12) & 0xf];
		iCRC <<= 4;
		}

	return TRUE;
	}

static int iCompare(const void *pvLeft, const void *pvRight)
	{
	if (*(int *)pvLeft < *(int *)pvRight)
		{
		return -1;
		}

	if (*(int *)pvLeft > *(int *)pvRight)
		{
		return 1;
		}

	return 0;
	}

static int iUpdateCRC(char *pcBase, int iCount, int iCRC)
	{
	int i, j;

	for (i = 0; i < iCount; i++)
		{
		iCRC ^= (pcBase[i] << 8);
		for (j = 0; j < 8; j++)
			{
			if (iCRC & 0x8000)
				{
				iCRC = (iCRC << 1) ^ 0x1021;
				}
			else
				{
				iCRC <<= 1;
				}
			}
		}

	return iCRC;
	}

// ----------------------------------------------------------------------------

