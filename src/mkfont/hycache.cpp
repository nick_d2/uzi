// hycache.cpp

// ----------------------------------------------------------------------------

#include <io.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdio.h>
#include <windows.h>
#pragma hdrstop
#include "hymem.h"
#include "hyfs.h"
#include "hyfile.h"
#include "hylist.h"
#include "hycache.h"

// ----------------------------------------------------------------------------

void CacheEntrySetup(LISTTAG *plt, int iItemCount)
     {
     // allocate the cache list via the lower level wrapper
     ListAllocate(plt, iItemCount, sizeof(CACHETAG));
     }

void CacheExitCleanup(LISTTAG *plt)
     {
     int i;

     // free all active cache tags in the cache
     for (i = 0; i < plt->iAppend; i++)
          {
          CacheFree(plt, i);
          }

     // free the cache list via the lower level wrapper
     ListFree(plt);
     }

// ----------------------------------------------------------------------------

void CacheExitFlush(LISTTAG *plt, int iDirtiness)
     {
     int i;

     // write out all dirty cache tags in the cache
     for (i = 0; i < plt->iAppend; i++)
          {
          CacheFlush(plt, i, iDirtiness);
          }
     }

int fiCacheFlush(LISTTAG *plt, int iItem, int iDirtiness)
     {
     CACHETAG *pct;

     plt->iPosition = iItem;
     pct = (CACHETAG *) pcListCanGet(plt);
     if (pct == NULL)
          {
          return FALSE; // the user passed an invalid token
          }

     if (pct->iDirty >= iDirtiness) // got name for the entry, and is it dirty?
          {
#if 1 // highly temporary, add a diagnostic (but error now bombs!!
 FileWriteOut(&pct->ft, pct->mt.pcBase);
#else
          if (fiFileWriteOut(&pct->ft, pct->mt.pcBase) == 0)
               {
               return FALSE;
               }
#endif

          pct->iDirty = 2; // preserve the entry, it is now clean
          }

     return TRUE;
     }

// ----------------------------------------------------------------------------

int fiCacheAllocate(LISTTAG *plt, int *piItem, int iSize)
     {
     CACHETAG *pct;

     plt->iPosition = plt->iAppend;

     pct = (CACHETAG *) pcListCanPut(plt);
     if (pct == NULL)
          {
          return FALSE; // no more entries in the cache
          }

     if (fiFileAllocate(&pct->ft, iSize) == 0)
          {
          plt->iAppend = plt->iPosition;
          return FALSE; // not enough memory for the data
          }

     pct->iDirty = 1; // unnamed
     *piItem = plt->iPosition; // return token to user
     return TRUE;
     }

int fiCacheFree(LISTTAG *plt, int iItem)
     {
     CACHETAG *pct;
     int fiSuccess;

     plt->iPosition = iItem;
     pct = (CACHETAG *) pcListCanGet(plt);
     if (pct == NULL)
          {
          return FALSE; // the user passed an invalid token
          }

     // if nothing is to be done, indicate success
     fiSuccess = TRUE;

     // unconditionally delete this entry from the cache
     if (pct->iDirty >= 2) // got a name for the entry?
          {
          fiSuccess &= fiMemFree(&pct->mt);
          }

     if (pct->iDirty >= 1) // got any data for the entry?
          {
          fiSuccess &= fiFileFree(&pct->ft);
          }

     pct->iDirty = 0; // return the cache entry to the pool
 // note: this defeats the point of caching completely, revisit later:
 if (plt->iAppend == iItem + 1)
  {
  plt->iAppend = iItem;
  }
     return fiSuccess; // if this is FALSE, a grievious error has occurred
     }

// ----------------------------------------------------------------------------

int fiCacheSearch(LISTTAG *plt, int *piItem, char *pszName)
     {
     CACHETAG *pct;

     plt->iPosition = 0;
     while (pct = (CACHETAG *) pcListCanGet(plt))
          {
          if (pct->iDirty >= 2 && // got a name for the entry?
                    strcmp(pszName, pct->mt.pcBase) == 0) // matching name?
               {
               plt->iPosition--; // it was advanced by pcListCanGet()

               *piItem = plt->iPosition; // return token to user
               return TRUE; // the file name was found in the cache
               }
          }

     // we will now rely on plt->iPosition == plt->iAppend
     return FALSE;
     }

int fiCacheReadIn(LISTTAG *plt, int *piItem, char *pszName)
     {
     CACHETAG *pct;

     if (fiCacheSearch(plt, piItem, pszName))
          {
          return TRUE; // the file name was found in the cache
          }

     // we will now rely on plt->iPosition == plt->iAppend
     pct = (CACHETAG *) pcListCanPut(plt);
     if (pct == NULL)
          {
          return FALSE; // no more entries in the cache
          }

     if (fiMemAlloc(&pct->mt, strlen(pszName) + 1) == 0)
          {
          plt->iAppend = plt->iPosition;
          return FALSE; // not enough memory for the file name
          }
     strcpy(pct->mt.pcBase, pszName); // stash the file name

     if (fiFileReadIn(&pct->ft, pszName) == 0)
          {
          fiMemFree(&pct->mt);
          plt->iAppend = plt->iPosition;
          return FALSE; // file not found or not enough memory
          }

     pct->iDirty = 2; // clean
     *piItem = plt->iPosition; // return token to user
     return TRUE; // the file was successfully read into the cache
     }

int fiCacheWriteOut(LISTTAG *plt, int iItem, char *pszName)
     {
     CACHETAG *pct;
     MEMTAG mt;

     plt->iPosition = iItem;
     pct = (CACHETAG *) pcListCanGet(plt);
     if (pct == NULL || pct->iDirty < 1)
          {
          return FALSE; // the user passed an invalid token
          }

     if (pct->iDirty >= 2 && // got a name for the entry?
               strcmp(pszName, pct->mt.pcBase) == 0) // matching name?
          {
          return TRUE; // no action needed, maybe pszName == pct->mt.pcBase
          }

     if (fiMemAlloc(&mt, strlen(pszName) + 1) == 0)
          {
          return FALSE; // not enough memory for the new file name
          }
     strcpy(mt.pcBase, pszName); // stash the file name

     if (pct->iDirty >= 2 && // got an existing name for the entry?
               fiMemFree(&pct->mt) == 0) // if so, it's now safe to free it
          {
          fiMemFree(&mt);
          return FALSE; // couldn't free the existing name storage
          }
     memcpy(&pct->mt, &mt, sizeof(MEMTAG)); // can now clobber the old mt

     pct->iDirty = 3; // dirty
     return TRUE; // the new name has been registered, flush the data later
     }

// ----------------------------------------------------------------------------

