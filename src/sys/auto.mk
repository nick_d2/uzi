# auto.mk by Nick for Hytech NOS/UZI project

# -----------------------------------------------------------------------------

SUBDIRS=

OUTPUTS=	sys_$(MODEL).$(LIBEXT)

sys_$(MODEL)_$(LIBEXT)_SOURCES= \
		unix_$(MODEL).S sys.c $(_sys_phony_SOURCES) \
		fork.S vfork.S _fork.S _vfork.S

#unix_$(MODEL)_S_MODULES= \
#		__unix

sys_c_MODULES=	_exit access alarm brk chdir chmod chown chroot close creat \
		dup dup2 execve falign fstat getegid geteuid getfsys \
		getgid getpid getppid getprio getuid ioctl kill link lseek \
		mkfifo mknod mount open pause pipe read reboot sbrk lseek \
		setgid setprio setuid signal stat stime symlink sync sysdebug \
		systrace time times umask umount unlink utime waitpid write \
#fork vfork

_sys_phony_SOURCES= \
		__exit.$(OBJEXT) _access.$(OBJEXT) _alarm.$(OBJEXT) \
		_brk.$(OBJEXT) _chdir.$(OBJEXT) _chmod.$(OBJEXT) \
		_chown.$(OBJEXT) _chroot.$(OBJEXT) _close.$(OBJEXT) \
		_creat.$(OBJEXT) _dup.$(OBJEXT) _dup2.$(OBJEXT) \
		_execve.$(OBJEXT) _falign.$(OBJEXT) \
		_fstat.$(OBJEXT) _getegid.$(OBJEXT) _geteuid.$(OBJEXT) \
		_getfsys.$(OBJEXT) _getgid.$(OBJEXT) _getpid.$(OBJEXT) \
		_getppid.$(OBJEXT) _getprio.$(OBJEXT) _getuid.$(OBJEXT) \
		_ioctl.$(OBJEXT) _kill.$(OBJEXT) _link.$(OBJEXT) \
		_lseek.$(OBJEXT) _mkfifo.$(OBJEXT) _mknod.$(OBJEXT) \
		_mount.$(OBJEXT) _open.$(OBJEXT) _pause.$(OBJEXT) \
		_pipe.$(OBJEXT) _read.$(OBJEXT) _reboot.$(OBJEXT) \
		_sbrk.$(OBJEXT) _lseek.$(OBJEXT) _setgid.$(OBJEXT) \
		_setprio.$(OBJEXT) _setuid.$(OBJEXT) _signal.$(OBJEXT) \
		_stat.$(OBJEXT) stime.$(OBJEXT) _symlink.$(OBJEXT) \
		_sync.$(OBJEXT) _sysdebug.$(OBJEXT) _systrace.$(OBJEXT) \
		_time.$(OBJEXT) _times.$(OBJEXT) _umask.$(OBJEXT) \
		_umount.$(OBJEXT) _unlink.$(OBJEXT) _utime.$(OBJEXT) \
		_waitpid.$(OBJEXT) _write.$(OBJEXT)
#_fork.$(OBJEXT) _vfork.$(OBJEXT)
_sys_phony_DEFINES= \
		$(sys_$(MODEL)_$(LIBEXT)_DEFINES)
__exit_$(OBJEXT)_SOURCES= \
		sys.c
__exit_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE__exit
_access_$(OBJEXT)_SOURCES= \
		sys.c
_access_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_access
_alarm_$(OBJEXT)_SOURCES= \
		sys.c
_alarm_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_alarm
_brk_$(OBJEXT)_SOURCES= \
		sys.c
_brk_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_brk
_chdir_$(OBJEXT)_SOURCES= \
		sys.c
_chdir_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_chdir
_chmod_$(OBJEXT)_SOURCES= \
		sys.c
_chmod_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_chmod
_chown_$(OBJEXT)_SOURCES= \
		sys.c
_chown_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_chown
_chroot_$(OBJEXT)_SOURCES= \
		sys.c
_chroot_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_chroot
_close_$(OBJEXT)_SOURCES= \
		sys.c
_close_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_close
_creat_$(OBJEXT)_SOURCES= \
		sys.c
_creat_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_creat
_dup_$(OBJEXT)_SOURCES= \
		sys.c
_dup_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_dup
_dup2_$(OBJEXT)_SOURCES= \
		sys.c
_dup2_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_dup2
_execve_$(OBJEXT)_SOURCES= \
		sys.c
_execve_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_execve
_falign_$(OBJEXT)_SOURCES= \
		sys.c
_falign_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_falign
#_fork_$(OBJEXT)_SOURCES= \
#		sys.c
#_fork_$(OBJEXT)_DEFINES= \
#		$(_sys_phony_DEFINES) _SYS MODULE MODULE_fork
_fstat_$(OBJEXT)_SOURCES= \
		sys.c
_fstat_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_fstat
_getegid_$(OBJEXT)_SOURCES= \
		sys.c
_getegid_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_getegid
_geteuid_$(OBJEXT)_SOURCES= \
		sys.c
_geteuid_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_geteuid
_getfsys_$(OBJEXT)_SOURCES= \
		sys.c
_getfsys_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_getfsys
_getgid_$(OBJEXT)_SOURCES= \
		sys.c
_getgid_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_getgid
_getpid_$(OBJEXT)_SOURCES= \
		sys.c
_getpid_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_getpid
_getppid_$(OBJEXT)_SOURCES= \
		sys.c
_getppid_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_getppid
_getprio_$(OBJEXT)_SOURCES= \
		sys.c
_getprio_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_getprio
_getuid_$(OBJEXT)_SOURCES= \
		sys.c
_getuid_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_getuid
_ioctl_$(OBJEXT)_SOURCES= \
		sys.c
_ioctl_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_ioctl
_kill_$(OBJEXT)_SOURCES= \
		sys.c
_kill_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_kill
_link_$(OBJEXT)_SOURCES= \
		sys.c
_link_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_link
_lseek_$(OBJEXT)_SOURCES= \
		sys.c
_lseek_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_lseek
_mkfifo_$(OBJEXT)_SOURCES= \
		sys.c
_mkfifo_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_mkfifo
_mknod_$(OBJEXT)_SOURCES= \
		sys.c
_mknod_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_mknod
_mount_$(OBJEXT)_SOURCES= \
		sys.c
_mount_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_mount
_open_$(OBJEXT)_SOURCES= \
		sys.c
_open_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_open
_pause_$(OBJEXT)_SOURCES= \
		sys.c
_pause_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_pause
_pipe_$(OBJEXT)_SOURCES= \
		sys.c
_pipe_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_pipe
_read_$(OBJEXT)_SOURCES= \
		sys.c
_read_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_read
_reboot_$(OBJEXT)_SOURCES= \
		sys.c
_reboot_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_reboot
_sbrk_$(OBJEXT)_SOURCES= \
		sys.c
_sbrk_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_sbrk
_lseek_$(OBJEXT)_SOURCES= \
		sys.c
_lseek_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_lseek
_setgid_$(OBJEXT)_SOURCES= \
		sys.c
_setgid_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_setgid
_setprio_$(OBJEXT)_SOURCES= \
		sys.c
_setprio_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_setprio
_setuid_$(OBJEXT)_SOURCES= \
		sys.c
_setuid_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_setuid
_signal_$(OBJEXT)_SOURCES= \
		sys.c
_signal_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_signal
_stat_$(OBJEXT)_SOURCES= \
		sys.c
_stat_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_stat
_stime_$(OBJEXT)_SOURCES= \
		sys.c
_stime_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_stime
_symlink_$(OBJEXT)_SOURCES= \
		sys.c
_symlink_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_symlink
_sync_$(OBJEXT)_SOURCES= \
		sys.c
_sync_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_sync
_sysdebug_$(OBJEXT)_SOURCES= \
		sys.c
_sysdebug_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_sysdebug
_systrace_$(OBJEXT)_SOURCES= \
		sys.c
_systrace_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_systrace
_time_$(OBJEXT)_SOURCES= \
		sys.c
_time_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_time
_times_$(OBJEXT)_SOURCES= \
		sys.c
_times_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_times
_umask_$(OBJEXT)_SOURCES= \
		sys.c
_umask_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_umask
_umount_$(OBJEXT)_SOURCES= \
		sys.c
_umount_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_umount
_unlink_$(OBJEXT)_SOURCES= \
		sys.c
_unlink_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_unlink
_utime_$(OBJEXT)_SOURCES= \
		sys.c
_utime_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_utime
_waitpid_$(OBJEXT)_SOURCES= \
		sys.c
_waitpid_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_waitpid
_write_$(OBJEXT)_SOURCES= \
		sys.c
_write_$(OBJEXT)_DEFINES= \
		$(_sys_phony_DEFINES) _SYS MODULE MODULE_write
#_vfork_$(OBJEXT)_SOURCES= \
#		sys.c
#_vfork_$(OBJEXT)_DEFINES= \
#		$(_sys_phony_DEFINES) _SYS MODULE MODULE_vfork

# -----------------------------------------------------------------------------

