#!/bin/sh

# compile (with -s9) the fork system calls (run n.pmm after this)
# needed so that a known number of items (incl ix/iy) are pushed

test -d build/z80/banked/ || mkdir -p build/z80/banked/
tradcpp -D__STDC__ -DNDEBUG -DPC_UZIX_TARGET -DMODULE -DMODULE_fork -I../../include/ -I../../include/z80/ sys.c build/z80/banked/fork.i
po2c ../po/messages.po build/z80/banked/fork.i
iccz80 -S -A -v1 -s9 -mb build/z80/banked/fork.l
rm -f build/z80/banked/fork.r01
mv -f build/z80/banked/fork.s01 fork.S

test -d build/z80/banked/ || mkdir -p build/z80/banked/
tradcpp -D__STDC__ -DNDEBUG -DPC_UZIX_TARGET -DMODULE -DMODULE_vfork -I../../include/ -I../../include/z80/ sys.c build/z80/banked/vfork.i
po2c ../po/messages.po build/z80/banked/vfork.i
iccz80 -S -A -v1 -s9 -mb build/z80/banked/vfork.l
rm -f build/z80/banked/vfork.r01
mv -f build/z80/banked/vfork.s01 vfork.S

test -d build/z80/banked/ || mkdir -p build/z80/banked/
tradcpp -D__STDC__ -DNDEBUG -DPC_UZIX_TARGET -D_SYS -DMODULE -DMODULE_fork -I../../include/ -I../../include/z80/ sys.c build/z80/banked/_fork.i
po2c ../po/messages.po build/z80/banked/_fork.i
iccz80 -S -A -v1 -s9 -mb build/z80/banked/_fork.l
rm -f build/z80/banked/_fork.r01
mv -f build/z80/banked/_fork.s01 _fork.S

test -d build/z80/banked/ || mkdir -p build/z80/banked/
tradcpp -D__STDC__ -DNDEBUG -DPC_UZIX_TARGET -D_SYS -DMODULE -DMODULE_vfork -I../../include/ -I../../include/z80/ sys.c build/z80/banked/_vfork.i
po2c ../po/messages.po build/z80/banked/_vfork.i
iccz80 -S -A -v1 -s9 -mb build/z80/banked/_vfork.l
rm -f build/z80/banked/_vfork.r01
mv -f build/z80/banked/_vfork.s01 _vfork.S

