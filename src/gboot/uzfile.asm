; uzfile.asm by Nick, gunzipper, based on zipfile.inc from Hytech EPROM 5.529+

; -----------------------------------------------------------------------------

$ io64180.inc

	module	_uzfile

	public	uzfgo
	public	gzhbuf
	public	gztbuf
	extern	abyte
	extern	ahexb
	extern	ahexw
	extern	acrlf
	extern	bomb
	extern	copyr
	extern	uzmess

	rseg	RCODE

; -----------------------------------------------------------------------------

bad_format:
	call	uzmess
	db	'not in gzip format',0dh,0ah,0
	jp	bomb

uzfgo:	; file is deflated, inflate
	;in0	d,(BBR)
	;push	de

	;ld	e,0
	;call	winehl			; e:hl -> input data
	;push	hl
	;pop	iy
	;out0	(BBR),e			; BBR:iy -> input data

 ld (outptr),hl
 ld hl,outseg
 ld (hl),e ; save absolute address -> output buffer for calls to copyr

 ld hl,virtbr
 ld (hl),d ; save virtual base address to use when incrementing virtbr:ix
 push iy
 pop ix ; now reading the input data from (ix) instead of (iy)

 ld l,d
 ld h,80h
 ld a,(hl) ; translate BBR value via virtual memory table for actual use
 out0 (BBR),a ; prepare to read the input data directly from (ix)

	sub	a			; inflation_routine
	ld	(lblock),a		; last block flag
	ld	(dbits),a		; dead bits in low dbuf
	call	nxtix			; initialise dbuf

 ld iy,gzhbuf
 ld b,10
 call pread

 ld hl,(gzhbuf)
 ld de,8b1fh
 or a
 sbc hl,de ; ensure magic number is gzip
 jr nz,bad_format

 ld hl,(gzhbuf+2)
 ld de,8
 or a
 sbc hl,de ; ensure compression method = 8 and no header options are present
 jr nz,bad_format

 ld iy,outbuf ; non-repeated data will be stored temporarily at (iy)

uzf0:	ld	a,(lblock)
	dec	a
 .if 0
 jp z,uzfret
 .else
	jr	z,uzfret		; inflate_done
 .endif

 .if 0
 call acrlf

 ld a,(virtbr)
 call ahexb
 ld a,':'
 call abyte
 push ix
 pop hl
 call ahexw

 ld a,'='
 call abyte

 extern zdata
 ld hl,LWRD zdata+10
 ld a,BYTE3 zdata+10
 add hl,hl
 add hl,hl
 rra
 rr h
 rr l
 rra
 rr h
 rr l
 ex de,hl
 ld c,a ; c:de -> absolute start of compressed data

 push ix
 pop hl
 ld a,(virtbr)
 add hl,hl
 add hl,hl
 rra
 rr h
 rr l
 rra
 rr h
 rr l ; a:hl -> absolute position in compressed data

 or a
 sbc hl,de
 sbc a,c ; a:hl = absolute offset within compressed data
 call ahexb
 call ahexw

 ld a,' '
 call abyte
 ld a,16
 ld hl,dbits
 sub (hl)
 call ahexb
 ld a,' '
 call abyte
 call peek
 ld a,c
 and 7
 call ahexb
 call acrlf
 .endif

	ld	a,1
	call	bits			; get_bit
	ld	(lblock),a

	call	tbl			; read_tables
	;jr	nz,uzfret		; bad table

uzf1:
 .if 0
 ld a,'a'
 call abyte
 .endif

	;ld	bc,wdog			; rwdog
	;in0	(c)

	ld	hl,plit			; lit_parse_table
	ld	de,llit			; lit_bitlen_table
	call	parse			; returns de = item
	dec	d
 .if 0
 jp z,uzf2
 .else
	jr	z,uzf2
 .endif

	ld	(iy),e
	inc	iy
 .if 0
 ld a,e
 call ahexb
 call acrlf
 .endif
 .if 0
 ld a,'b'
 call abyte
 .endif

 .if 1
	ld	hl,plit			; lit_parse_table
	ld	de,llit			; lit_bitlen_table
	call	parse			; returns de = item
	dec	d
	jr	z,uzf2

	ld	(iy),e
	inc	iy
 .if 0
 ld a,e
 call ahexb
 call acrlf
 .endif
 .if 0
 ld a,'c'
 call abyte
 .endif

	ld	hl,plit			; lit_parse_table
	ld	de,llit			; lit_bitlen_table
	call	parse			; returns de = item
	dec	d
	jr	z,uzf2

	ld	(iy),e
	inc	iy
 .if 0
 ld a,e
 call ahexb
 call acrlf
 .endif
 .if 0
 ld a,'d'
 call abyte
 .endif

	ld	hl,plit			; lit_parse_table
	ld	de,llit			; lit_bitlen_table
	call	parse			; returns de = item
	dec	d
	jr	z,uzf2

	ld	(iy),e
	inc	iy
 .if 0
 ld a,e
 call ahexb
 call acrlf
 .endif
 .if 0
 ld a,'e'
 call abyte
 .endif

	push	iy
	pop	hl
	ld	de,outbuf+100h
	or	a
	sbc	hl,de
	jr	c,uzf1			; output buffer still has room
 .if 0
 ld a,'f'
 call abyte
 .endif

	inc	h			; hl now = iy - outbuf (byte count)
	call	oflsh
 .endif
	jr	uzf1

uzfret:	; enter with z = success, nz = failure
 .if 0
 ld a,'g'
 call abyte
 .endif

	;ld	bc,(zdbuf+16h)		; uncompressed size, if success
	;pop	de
	;out0	(BBR),d
 push iy
 pop hl
 ld de,outbuf
 or a
 sbc hl,de
 call nz,oflsh ; leaves outptr/seg valid for possible checking by our caller

 ld iy,gztbuf
 ld b,8
 call pread ; retrieve crc/size, our caller is responsible for checking them

 ld hl,outseg
 ld e,(hl)
 ld hl,(outptr) ; so our caller can calculate the actual decompressed size
 ret

uzf2:
 .if 0
 ld a,'h'
 call abyte
 .endif

	ld	a,e
	or	a
 .if 1
 jp z,uzf0
 .else
	jr	z,uzf0			; new_table
 .endif

	ex	de,hl			; hl = basic length code

	cp	9
	jr	c,uzf4			; length 3-10 = hl+2, no extra bits
	cp	1dh
	jr	c,uzf3			; length 11-258, 1-5 extra bits

	ld	hl,258			; length 258 = hl, no extra bits
	jr	uzf5

uzf3:	sub	5			; a = 4-17h extra bits/4
	rrca
	rrca
	and	7
	ld	b,a			; b = 1-5 extra bits

	ld	a,l
	dec	a			; no need to sub 5
	and	3			; 4 possible lengths for current b
	or	4
	ld	l,a			; hl = 4-7 for algorithm

	ld	a,b
	add	hl,hl			; hl = base length, value 0-0e0h
	djnz	$-1

	ex	de,hl
	call	bitsb			; hl = 1-5 extra bits, value 0-1fh
	add	hl,de			; hl = 8-0ffh for lengths 11-258

	inc	hl
uzf4:	inc	hl
	inc	hl
uzf5:	push	hl
 .if 0
 ld a,'i'
 call abyte
 .endif

	ld	hl,pdist		; dist_parse_table
	ld	de,ldist		; dist_bitlen_table
	call	parse			; returns de = item

	;ld	a,e
 ex de,hl
 ld a,l
	cp	4
	jr	c,uzf6			; de = distance 0-3, no extra bits

	;ex	de,hl			; hl = basic distance code

	rrca
	and	0fh
	dec	a
	ld	b,a			; b = 1-0bh extra bits

	ld	a,l
	and	1			; 2 possible distances for current b
	or	2
	ld	l,a			; hl = 2-3 for algorithm

	ld	a,b
	add	hl,hl			; hl = base distance, value 0-6000h
	djnz	$-1

	ex	de,hl
	call	bitsw			; hl = 0-0bh extra bits, value 0-1fffh
	add	hl,de			; de = distance 4-7fffh

	;ex	de,hl
uzf6:	;inc	de			; real distance
 inc hl
 push hl
 .if 0
 ld a,'j'
 call abyte
 .endif

 push iy
 pop hl
 ld de,outbuf
 sub a ;or a
 sbc hl,de ; got anything in output buffer?
 ld c,l
 ld b,h ; zf, bc indicate how many bytes waiting

 ld iy,outbuf-4000h
 ld d,a ;ld d,0

 ld hl,outseg
 ld e,(hl)
 ld hl,(outptr)

 call nz,copyr ; flush anything still in output buffer
 .if 0
 ld a,'k'
 call abyte
 .endif

	;push	iy
	;pop	hl
	;push	hl
	;or	a
	;sbc	hl,de
	;pop	de
	;pop	bc
	;ldir
	;push	de
	;pop	iy
 ; e:hl = output location
 ; can't use outptr/seg until e:hl has been saved back
 .if 0
 ex (sp),hl
 call ahexw
 ex (sp),hl
 ld a,','
 call abyte
 .endif
 pop bc ; bc = distance code
 push hl
 or a
 sbc hl,bc ; go backwards as specified by distance code
 ex (sp),hl ; restore e:hl = output location
 pop iy
 ld a,e
 sbc a,0
 ld d,a ; d:iy = output location - distance code

 .if 0
 ex (sp),hl
 call ahexw
 ex (sp),hl
 call acrlf
 .endif
 pop bc ; bc = length code
 call copyr ; append bc bytes from d:iy to output buffer

 ld (outptr),hl
 ld hl,outseg
 ld (hl),e

 ld iy,outbuf ; restart output buffer

 .if 0
	ld	a,(zfprog)
	or	a
	jp	z,uzf1			; skip progress callouts if disabled

	push	ix
	pop	hl
	in0	a,(BBR)
	rlca
	rlca
	rlca
	rlca
	add	a,h			; minor absehl
	call	uzprog			; a = index = source address / 100h
 .endif
	jp	uzf1

; -----------------------------------------------------------------------------

tbl:	ld	a,2
	call	bits			; get_bits
	dec	a			; 1 = default tables
	jr	nz,tbldyn

	ld	hl,llit			; lit_bitlen_table
	ld	de,llit+1		; lit_bitlen_table+1
	ld	bc,90h
	ld	(hl),8
	ldir				; 90h * bit length 8
	ld	c,70h
	ld	(hl),9
	ldir				; 70h * bit length 9
	ld	c,18h
	ld	(hl),7
	ldir				; 18h * bit length 7
	ld	c,8-1
	ld	(hl),8			; 8 * bit length 8
	ldir

	ld	hl,ldist		; dist_bitlen_table
	ld	de,ldist+1
	ld	bc,20h-1
	ld	(hl),5
	ldir				; 20h * bit length 5

	ld	hl,120h
	ld	(nlit),hl		; num_lit_codes = 120h
	dec	h
	ld	(ndist),hl		; num_dist_codes = 20h
	jp	tblmak			; make_parse_tables

tbldyn:	dec	a			; 2 = dynamic tables
	;ret	nz			; else bad table
 jp nz,bad_block

	ld	a,5
	call	bitsb			; get_bits
	ld	bc,101h
	add	hl,bc
	ld	(nlit),hl		; num_lit_codes

	ld	a,5
	call	bitsb
	inc	hl
	ld	(ndist),hl		; num_dist_codes

	ld	hl,ltbl			; bitlen_bitlens
	ld	bc,13h
	call	zfill			; initialise bitlen bitlen table

	ld	a,4
	call	bits			; get_bits
	add	a,4
	ld	b,a			; count of bitlen bitlens
	ld	de,lxlat		; bitlen_order
tbl0:	push	bc

	ld	a,(de)
	inc	de
	ld	c,a
	ld	b,0
	ld	hl,ltbl			; bitlen_bitlens
	add	hl,bc

	ld	a,3
	call	bits			; get_bits
	ld	(hl),a

	pop	bc
	djnz	tbl0

	ld	bc,13h
	ld	de,ltbl			; bitlen_bitlens
	ld	hl,ptbl			; bitlen_parse_table
	call	make			; make_parse_table

	ld	de,llit			; lit_bitlen_table
	ld	hl,(nlit)		; num_lit_codes
	ld	bc,(ndist)		; num_dist_codes
	add	hl,bc
	add	hl,de
	ld	(lend),hl		; termination loop counter
tbl1:	ex	de,hl
	push	hl

	ld	hl,ptbl			; bitlen_parse_table
	ld	de,ltbl			; bitlen_bitlens
	call	parse

	ld	a,e
	cp	10h
	jr	c,tbl5			; single bitlen
	jr	z,tbl4			; 10h = repeated bitlen * 3-6
	cp	11h
	jr	z,tbl2			; 11h = repeated zeros * 3-10

	ld	a,7			; 12h = repeated zeros * 11-138
	call	bits			; get_bits
	add	a,11
	jr	tbl3

tbl2:	ld	a,3			; 11h = repeated zeros * 3-10
	call	bits			; get_bits
	add	a,3
tbl3:	ld	c,a

	pop	hl
	call	zfill
	jr	tbl6

tbl4:	ld	a,2			; 10h = repeated bitlen * 3-6
	call	bits			; get_bits
	add	a,3
	ld	c,a

	pop	hl
	dec	hl
	call	hlfill
	jr	tbl6

tbl5:	pop	hl
	ld	(hl),e			; single bitlen

tbl6:	inc	hl
	ld	e,l
	ld	d,h

	ld	bc,(lend)		; termination loop counter
	or	a
	sbc	hl,bc
	jr	c,tbl1

	ld	bc,(ndist)
	ld	hl,ldist-1
	add	hl,bc
	ex	de,hl
	ld	hl,(lend)
	dec	hl
	lddr				; move dist_bitlen_table to proper spot

tblmak:	ld	bc,(nlit)		; num_lit_codes
	ld	de,llit			; lit_bitlen_table
	ld	hl,plit			; lit_parse_table + lit_parse_tree
	call	make			; make_parse_table

	ld	bc,(ndist)		; num_dist_codes
	ld	de,ldist		; dist_bitlen_table
	ld	hl,pdist		; dist_parse_table + dist_parse_tree
	;call	make
	;ret

; call with:
; bc = items
; de -> bitlen table, 1 byte per item
; hl -> parse table, 100h words
; parse tree is at hl+200h

make:	ld	a,c
	or	b
	ret	z

	ld	(nmake),bc		; count of items
	ld	(lmake),de		; pointer to bitlen table
	ld	(pmake),hl		; pointer to parse table + tree

	; scan entries counting each bit length 0-15 to lcount

	ld	hl,lcount		; bitlen_counts
	ld	bc,10h*2
	call	zfill

	ld	de,(lmake)		; pointer to bitlen table
	ld	bc,(nmake)		; count of items
make0:	push	bc

	ld	a,(de)
	inc	de

	ld	c,a
	ld	b,0
	ld	hl,lcount		; bitlen_counts
	add	hl,bc
	add	hl,bc
	inc	(hl)
	jr	nz,$+4
	inc	hl
	inc	(hl)

	pop	bc
	dec	bc
	ld	a,c
	or	b
	jr	nz,make0

	; assign each bit length 1-15 a starting code to cmake

	ld	de,0
	ld	bc,2
make1:	push	bc

	ld	hl,cmake		; current_codes
	add	hl,bc
	ld	(hl),e
	inc	hl
	ld	(hl),d

	ld	hl,lcount		; bitlen_counts
	add	hl,bc
	ld	c,(hl)
	inc	hl
	ld	b,(hl)

	ex	de,hl
	add	hl,bc
	add	hl,hl
	ex	de,hl

	pop	bc
	inc	c
	inc	c
	ld	a,c
	cp	20h
	jr	c,make1

	ld	a,e
	or	d
	jr	z,make15

	; table is not full, allowed if sum lcount <= 1

	ld	hl,lcount+2		; ignore bit length 0
	ld	de,0
	ld	a,15			; bit lengths 1-15
make14:	ld	c,(hl)
	inc	hl
	ld	b,(hl)
	inc	hl
	ex	de,hl
	add	hl,bc
	ex	de,hl
	dec	a
	jr	nz,make14
	ld	hl,1
	or	a
	sbc	hl,de
	;ret	c			; bad table if 1 < sum lcount
 jp c,bad_table

make15:	; scan entries assigning codes to hmake from cmake

	ld	de,(lmake)		; pointer to bitlen table
	ld	bc,(nmake)		; count of items
	ld	hl,hmake		; huffman_table
make2:	push	bc

	ld	a,(de)
	inc	de
	or	a
	ld	bc,0
	jr	z,make3

	push	hl
	ld	c,a
	ld	hl,cmake		; current_codes
	add	hl,bc
	add	hl,bc

	ld	c,(hl)
	inc	(hl)
	inc	hl
	ld	b,(hl)
	jr	nz,$+3
	inc	(hl)
	pop	hl

make3:	ld	(hl),c
	inc	hl
	ld	(hl),b
	inc	hl

	pop	bc
	dec	bc
	ld	a,c
	or	b
	jr	nz,make2

	; scan entries bit reversing each code in hmake

	ld	de,(lmake)		; pointer to bitlen table
	ld	bc,(nmake)		; count of items
	ld	hl,hmake		; huffman_table
make4:	push	bc

	ld	a,(de)
	inc	de

	push	de
	push	hl

	ld	e,(hl)
	inc	hl
	ld	d,(hl)

	or	a
	jr	z,make6

	ld	hl,0
	ld	b,a
make5:	rr	d
	rr	e
	adc	hl,hl
	djnz	make5
	ex	de,hl

make6:	pop	hl
	ld	(hl),e
	inc	hl
	ld	(hl),d
	inc	hl
	pop	de

	pop	bc
	dec	bc
	ld	a,c
	or	b
	jr	nz,make4

	; construct parse table and tree

	ld	hl,(pmake)		; pointer to parse table + tree
	ld	bc,100h*2
	call	zfill
	inc	hl
	ld	(trptr),hl		; tree ptr

	ld	de,0			; index to lmake
make7:	ld	hl,hmake
	add	hl,de
	add	hl,de
	ld	c,(hl)
	inc	hl
	ld	b,(hl)

	ld	hl,(lmake)		; pointer to bitlen table
	add	hl,de
	ld	a,(hl)
	or	a
	jr	z,make13
	cp	9
	jr	nc,make10

	ld	b,a
	ld	hl,1
make8:	add	hl,hl
	djnz	make8			; b = 0 for <= 8 bit huffman code
	ld	(pinc),hl		; parse table increment

make9:	ld	hl,(pmake)		; pointer to parse table + tree
	add	hl,bc
	add	hl,bc			; huffman code

	ld	(hl),e
	inc	hl
	ld	(hl),d

	ld	hl,(pinc)		; parse table increment
	add	hl,bc
	ld	c,l
	ld	b,h

	ld	a,b
	or	a
	jr	z,make9
	jr	make13

make10:	push	de
	push	af

	ld	a,b
	ld	b,0
	ld	hl,(pmake)		; pointer to parse table + tree
	add	hl,bc
	add	hl,bc			; huffman code, lo 8 bits
	ld	c,a			; huffman code, hi 8 bits

	pop	af
	sub	8
	ld	b,a

make11:	ld	e,(hl)
	inc	hl
	ld	d,(hl)

	ld	a,e
	or	d
	jr	nz,make12		; existing branch

	ld	de,(trptr)		; tree ptr
	ld	(hl),d
	dec	hl
	ld	(hl),e

	push	de
;	rept	4
;	ld	(de),a
;	inc	de
;	endm
 ld (de),a
 inc de
 ld (de),a
 inc de
 ld (de),a
 inc de
 ld (de),a
 inc de
	ld	(trptr),de		; tree ptr
	pop	de

make12:	ex	de,hl

	rr	c
	jr	nc,$+4
	inc	hl
	inc	hl

	djnz	make11

	pop	de
	ld	(hl),e
	inc	hl
	ld	(hl),d

make13:	inc	de

	ld	hl,(nmake)
	or	a
	sbc	hl,de
	jr	nz,make7

	ret				; z for caller of tbl

bad_block:
	call	uzmess
	db	'invalid table type',0dh,0ah,0
	jp	bomb

bad_table:
	call	uzmess
	db	'file has bad table',0dh,0ah,0
	jp	bomb

; -----------------------------------------------------------------------------
; enter with:
; a = bits to read (1-10h)

; returns:
; de = preserved
; hl = value

bitsw:	cp	9
	jr	c,bitsb

	sub	8
	ld	h,a			; h = eventual bits in h
	call	peek
 .if 0
	inc	ix			; shortcut to flush 8 bits
 .else
	call	nxtix
 .endif
	ld	l,c			; l = bits 0-7 of result

	ld	a,h
	call	bits
	ld	h,a			; h = bits 8-n of result
	ret				; result in hl

bitsb:	call	bits
	ld	l,a
	ld	h,b			; 0
	ret

; enter with:
; a = bits to read (1-8)

; returns:
; de = preserved
; a = value

bits:	push	hl
	ld	l,a
	call	peek
	call	flush
	ld	a,c
	ld	c,l
	ld	hl,bmask-1
	add	hl,bc
	and	(hl)
	pop	hl
	ret				; result in hl and a

; -----------------------------------------------------------------------------
; enter with:
; hl -> parse table
; de -> bitlen table

; returns:
; de = next item from input

parse:	call	peek			; a = index for parse table lookup
	add	hl,bc
	add	hl,bc
	ld	a,(hl)
	inc	hl
	ld	h,(hl)
	ld	l,a			; hl = parse table entry
	ex	de,hl			; de = parse table entry, hl -> bitlens

 .if 0
	bit	4,d
 .else
	bit	7,d
 .endif
	jr	z,parse2		; parse from table
					; parse from tree
 .if 0
	inc	ix			; shortcut to flush 8 bits
 .else
	call	nxtix
 .endif
	call	peek			; c = bit buffer for tree lookup

	ld	a,(dbits)

parse0:	rr	c
	jr	nc,$+4
	inc	de
	inc	de

	inc	a
	cp	8			; dbits = 8?
	jr	c,parse1		; no, bit buffer ok

 .if 0
	inc	ix			; shortcut to flush 8 bits
	ld	c,(ix)			; refresh bit buffer
 .else
	call	nxtix
	ld	a,(dbuf)
	ld	c,a
 .endif

	sub	a			; new byte, no dead bits

parse1:	ex	de,hl
	ld	e,(hl)
	inc	hl
	ld	d,(hl)

 .if 0
	bit	4,d
 .else
	bit	7,d
 .endif
	jr	nz,parse0

	ld	(dbits),a
	ret

parse2:	add	hl,de
	ld	l,(hl)			; l = bitlen table entry
	;call	flush
	;ret

; enter with:
; l = bits to kill
; BBR:ix -> input data
; dbuf = 8 bits from input
; dbits = dead bits 0-7

; returns:
; bc = preserved
; de = preserved
; hl = preserved
; BBR:ix = advanced
; dbuf = updated
; dbits = advanced

flush:	ld	a,(dbits)
	add	a,l
	ld	(dbits),a
	sub	8
	ret	c
	ld	(dbits),a

nxtix:	ld	a,(ix)
	ld	(dbuf),a
	;push	bc
	;ld	bc,1
	;add	ix,bc
	;pop	bc
	;ret	nc			; still in same window
 inc ix
 push ix
 ex (sp),hl
 bit 7,h
 pop hl
 ret z ; still in same window (ix < 8000h)

 .if 0
	ld	a,(zfprog)
	or	a			; progress callouts enabled?
	jr	z,skprog		; skip if not

	in0	a,(BBR)
	push	af
	push	bc
	push	de
	push	hl
	push	iy

	add	a,0fh+1
	rlca
	rlca
	rlca
	rlca				; convert window to segment
	ld	h,a
	ld	l,0			; absolute 0:hl -> new source data
	call	uzprog			; call boot1

	pop	iy
	pop	hl
	pop	de
	pop	bc
	pop	af
	jr	dnprog
 .endif

skprog:	;in0	a,(BBR)
dnprog:	;inc	a
 push hl
 ld hl,virtbr
 inc (hl)
 ld l,(hl)
 ld h,80h
 ld a,(hl) ; translate BBR value via virtual memory table for actual use
 pop hl
	out0	(BBR),a			; wrapped into new window
 
	;ld	ix,0f000h		; BBR:ix -> new source data
 ld ix,4000h
	ret

; -----------------------------------------------------------------------------

; enter with:
; BBR:ix -> input data
; dbuf = 8 bits from input
; dbits = dead bits 0-7

; returns:
; bc = next 8 bits (b = 0)
; de = preserved
; hl = preserved

peek:	ld	bc,(dbuf)		; b = dbits
	ld	a,b
	or	a
	ret	z
	ld	a,(ix)
peeklp:	rra
	rr	c
	djnz	peeklp
	ret

; -----------------------------------------------------------------------------

zfill:	sub	a
afill:	ld	(hl),a
	dec	bc
hlfill:	ld	e,l
	ld	d,h
	inc	de
	ldir
	ret

; -----------------------------------------------------------------------------

pread:	; plain_read
	ld	hl,dbits
	ld	a,(hl)			; see if we were already byte aligned
	or	a
	jr	z,preade
	ld	(hl),0			; no, we need to flush up to 7 bits

preadl:	call	nxtix			; refresh bit buffer, then advance ix
preade:	ld	a,(dbuf)		; enter here with a valid bit buffer

	ld	(iy),a			; save in output (no shifting needed)
	inc	iy

	djnz	preadl			; copy up to 100h bytes
	jp	nxtix			; refresh bit buffer for our caller

oflsh:	; output_flush
	ld	c,l
	ld	b,h			; bc now = iy - outbuf (byte count)

	ld	iy,outbuf-4000h		; because IDATA0 is phys 4000, log 8000
	ld	d,0

	ld	hl,outseg
	ld	e,(hl)
	ld	hl,(outptr)

	call	copyr			; flush output buffer to output

	ld	(outptr),hl
	ld	hl,outseg
	ld	(hl),e

	ld	iy,outbuf		; restart output buffer
	ret

; -----------------------------------------------------------------------------

		rseg	IDATA0
bmask:		defs	8
		rseg	CDATA0
		defb	1,3,7,0fh,1fh,3fh,7fh,0ffh

		rseg	IDATA0
lxlat:		defs	19
		rseg	CDATA0
		defb	16,17,18,0,8,7,9,6,10,5,11,4,12,3,13,2,14,1,15

; -----------------------------------------------------------------------------

		rseg	UDATA0

; tbl data

nlit:		defs	2		; num_lit_codes
ndist:		defs	2		; num_dist_codes
lend:		defs	2		; llit + nlit + ndist, loop terminator

; make data

nmake:		defs	2		; count of items
lmake:		defs	2		; pointer to bitlen table
pmake:		defs	2		; pointer to parse table + tree
trptr:		defs	2		; tree ptr
pinc:		defs	2		; parse table increment

; peek + flush data

; retain ordering %
dbuf:		defs	1
dbits:		defs	1		; dead bits in low dbuf
; %

; uzfile data

lblock:		defs	1		; last_block_flag

virtbr:		defs	1		; virtual base register before xlat
gzhbuf:		defs	10		; gzip header (no extra fields allowed)
gztbuf:		defs	8		; gzip trailer (crc-32, orig size-32)

outptr:		defs	2		; absolute output offset
outseg:		defs	1		; absolute output segment
outbuf:		defs	103h		; minimises the need to call copyr
		; outbuf should be 100h, but we allow an extra 3 bytes, due to
		; the main loop for decompressing non-repeated data, which is
		; unrolled 4 times (we test against outbuf+100h every 4 chars)

; buffers

ltbl:		defs	13h		; bitlen_bitlens
lcount:		defs	10h*2		; bitlen_counts
cmake:		defs	10h*2		; current_codes
hmake:		defs	120h*2		; huffman_table

; llit must be followed by ldist because we unpack lit bitlens and
; dist bitlens together, then move dist bitlens to start at ldist

; retain ordering %
llit:		defs	120h		; lit_bitlen_table
ldist:		defs	20h		; dist_bitlen_table
; %

ptbl:		defs	100h*2		; bitlen_parse_table
plit:		defs	(100h+240h)*2	; lit_parse_table + lit_parse_tree
pdist:		defs	(100h+40h)*2	; dist_parse_table + dist_parse_tree

;ztop:		defs	1		; must be < 0e000h

; -----------------------------------------------------------------------------

