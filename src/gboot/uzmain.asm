; uzmain.asm by Nick, gunzipper, main program and error handling routines

; -----------------------------------------------------------------------------

$ io64180.inc

BUFSIZE		equ	200h		; how many bytes per disk block
BUFSIZELOG	equ	9		; shift count representing the above

HD0_START	equ	512
HD0_SIZE	equ	1536

; -----------------------------------------------------------------------------

	module	_uzmain

	public	init
	public	bomb
	public	uzmess
	extern	abyte
	extern	ahexb
	extern	ahexw
	extern	acrlf
	extern	amess
	extern	copyr
	extern	uzfgo
	extern	gztbuf
	extern	zdata

	extern	s_CODE
	extern	e_CODE

	rseg	ICODE

; -----------------------------------------------------------------------------

init::
; now there are the next stack structure:
;	+4 envp
;	+2 argv
; sp->	+0 argc

	call	uzmess
	defb	'unzipping root filesystem',0dh,0ah,0

	ld	hl,0
	ld	iy,LWRD zdata
	defb	11h ; ld de,
	defb	4 ; e:hl -> destination, absolute 4:0000 (CP/M A: and B:)
	defb	BYTE3 zdata ; d:iy -> source, virtual zdata (gzipped input)

	call	uzfgo			; we return here only if successful

	ld	a,e
	sub	4
	ld	e,a
	ld	d,0			; de:hl = 32-bit actual size unpacked
 .if 0
 push hl
 ex de,hl
 call ahexw
 ex de,hl
 call ahexw
 ld a,' '
 call abyte
 ld hl,(gztbuf+6)
 call ahexw
 ld hl,(gztbuf+4)
 call ahexw
 call acrlf
 pop hl
 .endif

	ld	bc,(gztbuf+4)
	or	a
	sbc	hl,bc
	jr	nz,bad_size		; check low order 16 bits of size

	ld	hl,(gztbuf+6)
	;or	a
	sbc	hl,de
	jr	nz,bad_size		; check high order 16 bits of size

 .if 0 ; intentionally corrupt inode 20 so that fsck won't run
 jr poox
poo:
 defw 0c1h
 defw 0c0h
poox:
 ld iy,poo-4000h
 ld hl,0f18h
 ld de,4
 ld bc,poox-poo
 call copyr
 .endif

 .if 0 ; check for accidental corruption of inode 18
 ; dump block 7
 ld a,40h-4
 out0 (BBR),a
 ld hl,4e00h
 ld c,20h
dline:
 ld b,10h
ditem:
 ld a,(hl)
 inc hl
 call ahexb
 ld a,' '
 call abyte
 djnz ditem
 call acrlf
 dec c
 jr nz,dline
 .endif

	call	uzmess
	defb	'starting kernel via reboot',0dh,0ah,0

	ld	hl,0
	; wait for last character to be sent @ 9600
reboot_delay:
	dec	hl
	ld	a,l
	or	h
	jr	nz,reboot_delay

	jr	success			; reboot and run ramdrive boot sector

bad_size:
	call	uzmess
	db	'bad uncompressed size',0dh,0ah,0
	;jr	bomb

bomb::
	; try not to surprise the user unnecessarily
	call	uzmess
	defb	'doing a clean boot',0dh,0ah,0

	; before rebooting, prepare to clobber CP/M drive A: directory
	ld	hl,block
	ld	de,block+1
	ld	bc,BUFSIZE-1
	ld	(hl),0aah		; clobbering pattern
	ldir				; initialise temporary buffer

	; perform the clobbering (this ensures a clean boot from EPROM)
 .if 1
	ld	de,0
	ld	b,4
clobber_loop:
	push	bc
	push	de

	ld	hl,block
	call	block_write

	pop	de
	pop	bc
	inc	de
	djnz	clobber_loop
 .else
	ld	de,304h
	ld	l,c
	ld	h,b ; ld hl,0		; e:hl -> destination 4:0000

	ld	b,4			; clobber until just before 4:0800
clobber_loop:
	push	bc

	ld	iy,block		; copy from d:iy -> destination
	ld	bc,BUFSIZE		; bytes to copy
	call	copyr			; copy them, one byte at a time

	pop	bc
	djnz	clobber_loop
 .endif

success:
	; ready to reboot, by instructing the WPO chip on motherboard
	ld	a,0aah
	out0	(TRDR),A		; command byte to reboot the system
	ld	a,13h
	out0	(CNTR),A		; TE=1, divisor = 3, start transmission

	; wait for the reboot to occur, or else we're rather stuck
	jr	$

block_write:
	push	hl
	call	block_xlate		; a:hl -> data in ramdrive

	pop	iy
	ld	d,3			; d:iy -> user's buffer in seg 3:

	ld	e,a			; e:hl -> data in ramdrive
	jp	copyr			; copy BUFSIZE bytes from d:iy to e:hl

block_xlate:
	;ld	hl,HD0_SIZE
	;scf
	;sbc	hl,de			; check whether block no is in range
	;jr	c,bad_block

	ld	bc,BUFSIZE		; bytes to be copied for 1 block

	ld	hl,HD0_START
	add	hl,de			; hl = block index (from start of ram)

	ld	a,h
	ld	h,l
	ld	l,c ;0			; a:hl = block index * 100h

	add	hl,hl
	adc	a,a			; a:hl = block index * BUFSIZE (200h!!)
	ret

; -----------------------------------------------------------------------------

uzmess::
	call	amess
	defb	'uzboot: ',0
	jp	amess

; -----------------------------------------------------------------------------

	rseg	UDATA0

block:	defs	BUFSIZE			; temporary buffer for clobbering disc

; -----------------------------------------------------------------------------

	END
