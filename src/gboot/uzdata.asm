; uzdata.asm by Nick, gunzipper, stub program providing label to access zdata

; -----------------------------------------------------------------------------

$ io64180.inc

; -----------------------------------------------------------------------------

	module	_uzdata

	public	zdata

	rseg	CODE

; -----------------------------------------------------------------------------

zdata::
	; the actual data is provided by concatenating it to the executable

; -----------------------------------------------------------------------------

	END
