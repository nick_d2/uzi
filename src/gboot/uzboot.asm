; uzboot.asm by Nick, based on c0k.asm for UZI180 (creates a kernel executable)

; -----------------------------------------------------------------------------

$ io64180.inc

E_MAGIC		equ	0a6c9h
E_FORMAT_KERNEL	equ	3
E_STACK_SIZE	equ	1000h

; -----------------------------------------------------------------------------

	module	_uzboot

	extern	init

	extern	e_RCODE			; e_hsize
	extern	s_ICODE			; e_idata
	extern	s_UDATA0		; e_udata
	extern	s_CSTACK		; e_stack
	extern	e_CSTACK		; e_break

; -----------------------------------------------------------------------------
; forward definitions of segments, to set the linkage order (c0k must be first)

	rseg	RCODE
	rseg	ICODE
;	rseg	TEMP
;	rseg	DATA0
;	rseg	WCSTR
	rseg	CONST
	rseg	CSTR
	rseg	IDATA0(NUL)
	rseg	CDATA0
	rseg	ECSTR(NUL)
	rseg	CCSTR
	rseg	CODE(NUL)		; needs to be at end for banked model
	rseg	UDATA0
	rseg	CSTACK

; -----------------------------------------------------------------------------

	rseg	_DEFAULT		; means header is discarded at loading

	defw	E_MAGIC			; e_magic
	defw	E_FORMAT_KERNEL		; e_format
	defd	12345678h		; e_size
	defw	e_RCODE+14h		; e_hsize (14h = l__DEFAULT)
	defw	s_ICODE			; e_idata
	defw	init			; e_entry
	defw	s_UDATA0		; e_udata
	defw	s_CSTACK		; e_stack
	defw	e_CSTACK		; e_break

; -----------------------------------------------------------------------------

$ diag.inc				; this will select RCODE segment
$ copyr.inc				; this will select RCODE segment

; -----------------------------------------------------------------------------

	rseg	CSTACK
	defs	E_STACK_SIZE		; the default stack size is set here

; -----------------------------------------------------------------------------

	END
