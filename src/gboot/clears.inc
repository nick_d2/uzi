; clears.s01
; Hard and fast system initialisation for built in Z180 devices
; Now modified for use as an include file when compiling the boot routines

; -----------------------------------------------------------------------------

;$io64180.s01

;	.area	_CODE
	rseg	RCODE

; -----------------------------------------------------------------------------

	public	clears

clears::
	; initialise z180 serial ports
	ld	a,01110100b		; enables, rts off, d8 pn s1, tend0
	out0	(CNTLA0),a		; rts0 = 1 keeps us alive, new v5
	out0	(CNTLA1),a		; cka1d = 1 enables tend0
 .if 1
	ld	a,22h			; 22h = 9600, 20h = 38400
	out0	(CNTLB0),a		; ps = 1 enables /30 mode
	out0	(CNTLB1),a		; ps = 1 enables /30 mode
	sub	a
	out0	(STAT0),a
	out0	(STAT1),a		; cts1e = 0 enables rxs
 .else
	sub	a
	out0	(STAT0),a
	out0	(STAT1),a		; cts1e = 0 enables rxs
	out0	(CNTLB0),a		; 115200, ps = 0 disables /30 mode
	out0	(CNTLB1),a		; 115200, ps = 0 disables /30 mode
 .endif
	in0	(RDR0)			; clear asci0 rx & irq
	in0	(RDR1)			; clear asci1 rx & irq

	; initialise dma controller
	out0	(BCR0H),a
	out0	(BCR1H),a
	out0	(MAR1H),a
	out0	(MAR1B),a
	out0	(IAR1L),a
	out0	(IAR1H),a

	ret

; -----------------------------------------------------------------------------

;	END
