; gboot.asm
; Generic Hytech boot loader program to occupy 4 sectors at start of each disk

$ io64180.inc

LDFILE		equ	0f037h		; BOOTLDR.BIN entry point

SUPERBLOCK	equ	4		; starting position of filesystem
SMOUNTED	equ	12742		; random number to specify mounted fs
ROOTINODE	equ	1		; inode number of / for all mounted fs

BUFSIZE		equ	200h		; how many bytes per disk block
BUFSIZELOG	equ	9		; shift count representing the above

SIZEOF_DINODE_T	equ	64		; how many bytes per disk inode
DINODESPERBLOCK	equ	8		; how many disk inodes per disk block
DINODESPERBLOCKLOG equ	3		; shift count representing the above

DIRECTBLOCKS	equ	18
INDIRECTBLOCKS	equ	1		; MUST BE 1!
DINDIRECTBLOCKS equ	1		; MUST BE 1!

SIZEOF_DIRECT_T	equ	16		; how many bytes per directory entry
DIRNAMELEN	equ	14		; how many bytes of these are dir name

REGION_LOG 	equ	14
REGION_BYTES	equ	(1<<REGION_LOG)
REGION_BLOCKS	equ	(REGION_BYTES/BUFSIZE)

PAGE_LOG	equ	12
PAGE_BYTES	equ	(1<<PAGE_LOG)
PAGE_BLOCKS	equ	(PAGE_BYTES/BUFSIZE)

HD0_START	equ	512
HD0_SIZE	equ	1536

E_MAGIC_FIXME	equ	0a6c9h
E_FORMAT_KERNEL	equ	3

	rseg	RCODE

; -----------------------------------------------------------------------------

start:	defm	'XXXXYYMMDDHHMMSS'	; BOOT.BIN CRC, date and time

	; hytech eprom will enter at base+10h
	ld	sp,8000h		; as we will later set CBAR = 80h
	call	clears
	call	acrlf
	call	initial_boot		; for compatibility (returns if not ok)
	jr	entry
	defb	0,0

	; need a cp/m directory entry at base+20h
	defb	0,'BOOT    BIN',0,0,0,(final+7fh-start)/80h
	defb	0
	defb	((400h-(final-start)) >> 14) & 1
	defb	((800h-(final-start)) >> 14) & 2
	defb	((0c00h-(final-start)) >> 14) & 3
	defb	0,0,0,0,0,0,0,0,0,0,0,0

; -----------------------------------------------------------------------------

entry:
	call	gmess
	defb	'loading /boot/kernel.bin',0dh,0ah,0

	call	virtual_init		; modifies CBR (can't call EPROM now)

	ld	de,SUPERBLOCK
	ld	hl,block
	call	block_read

	ld	hl,(block)
	ld	de,SMOUNTED
	or	a
	sbc	hl,de
	jp	nz,black_magic

	ld	hl,(block+2)
	ld	(reserv),hl
	ld	hl,(block+4)
	ld	(isize),hl
	ld	hl,(block+6)
	ld	(fsize),hl

	ld	de,ROOTINODE
	ld	hl,name_boot
	call	dir_search		; returns de = the found inode
	jr	nz,bad_file

	ld	hl,name_kernel_bin
	call	dir_search		; returns de = the found inode
	jr	nz,bad_file

	call	file_setup		; read inode and prepare to walk file

	ld	hl,exe_header_buf
	ld	bc,exe_header_size
	call	file_read		; read first block, copy header to buf
	jr	c,bad_length

	call	check_e_magic		; ensure e_magic = E_MAGIC
	call	check_e_format		; ensure e_format = E_FORMAT_KERNEL

	ld	a,0-8			; so that logical 8000 = phys 0:0000
	out0	(CBR),a

	call	get_ca0_size		; returns bc = size, de = start of copy
	ld	hl,8000h		; destination = start of CA0 region
	call	file_read		; read CA0 data to common data segment

	ld	a,4-8			; so that logical 8000 = phys 0:4000
	out0	(CBR),a

	call	get_ca1_size		; returns bc = size, de = start of copy
	ex	de,hl			; destination specified by file header
	call	file_read		; read CA1 data to kernel data segment

	call	get_udata_size_m1	; returns bc = size-1, de = start+1
	call	nc,clear_bc_p1_at_de_m1	; if bc => 0, clear bc+1 bytes at de-1

	call	file_scan		; scan file and construct block list

	call	say_starting		; tell the user we have liftoff

	ld	bc,8080h		; initial spot in virt memory table
	ld	hl,region_list		; -> block no. of 1st found region
	jr	translate_loope

translate_loop:
	add	hl,de			; restore hl value from comparison

	call	block_to_bbr		; calculates a from word at hl (bumped)
	ld	(bc),a			; a = required bbr value to access page
	inc	bc

translate_loope:
	ld	de,(region_ptr)		; indicates end of region list
	or	a
	sbc	hl,de			; see if we've gone past the end
	jr	c,translate_loop

kazumi:
	ld	sp,parameters-parameters_end ; stack just under copied params

	ld	hl,parameters		; pre-initialised parameter block
	ld	de,parameters-parameters_end ; to fit neatly at top of memory
	ld	bc,parameters_end-parameters ; size in bytes to copy
	ldir				; copy parameter block for kernel init
	; careful! b will be used again below!!

	ld	hl,runsys		; start of small stub program below
	ld	de,(e_stack)		; load into base of stack region
	push	de
	ld	c,runsys_end-runsys	; size of small stub program (b=0)
	ldir				; copy stub program
	ld	hl,(e_entry)		; information for the runsys stub
	ld	de,(e_break)		; information for the client program
	ret				; enter stub program (jp 8100h)

bad_file:
	call	gmess
	defb	'file not found',0dh,0ah,0
	jr	error_bomb

bad_length:
	call	gmess
	defb	'file too short',0dh,0ah,0
	jr	error_bomb

runsys:
	ld	a,84h
	out0	(CBAR),a		; CA0 = 16k, bank area = 16k, CA1 = 32k
	jp	(hl)			; jump to entry address from exe header
runsys_end:

; -----------------------------------------------------------------------------
; had some kind of fatal error, the entry points are to save code space

check_e_magic:
	ld	hl,(e_magic)
	ld	de,E_MAGIC_FIXME
	or	a
	sbc	hl,de
	ret	z

	call	gmess
	defb	'bad e_magic: ',0
	jr	error_magic

check_e_format:
	ld	hl,(e_format)
	ld	de,E_FORMAT_KERNEL
	or	a
	sbc	hl,de
	ret	z

	call	gmess
	defb	'bad e_format: ',0
	jr	error_magic

black_magic:
	call	gmess
	defb	'bad magic: ',0

error_magic:
	add	hl,de

error_ahexw:
	call	ahexw

error_acrlf:
	call	acrlf

error_bomb:
	; try not to surprise the user unnecessarily
	call	gmess
	defb	'doing a clean boot',0dh,0ah,0

	; before erroring, prepare to clobber CP/M drive A: directory
	ld	hl,block
	ld	de,block+1
	ld	bc,BUFSIZE-1
	ld	(hl),0aah		; clobbering pattern
	ldir				; initialise temporary buffer

	; perform the clobbering (this ensures a clean boot from EPROM)
 .if 1
	ld	de,0
	ld	b,4
clobber_loop:
	push	bc
	push	de

	ld	hl,block
	call	block_write

	pop	de
	pop	bc
	inc	de
	djnz	clobber_loop
 .else
	ld	de,304h
	ld	l,c
	ld	h,b ; ld hl,0		; e:hl -> destination 4:0000

	ld	b,4			; clobber until just before 4:0800
clobber_loop:
	push	bc

	ld	iy,block		; copy from d:iy -> destination
	ld	bc,BUFSIZE		; bytes to copy
	call	copyr			; copy them, one byte at a time

	pop	bc
	djnz	clobber_loop
 .endif

	; ready to error, by instructing the WPO chip on motherboard
	ld	a,0aah
	out0	(TRDR),A		; command byte to error the system
	ld	a,13h
	out0	(CNTR),A		; TE=1, divisor = 3, start transmission

	; wait for the error to occur, or else we're rather stuck
	jr	$

; -----------------------------------------------------------------------------

gmess:	call	amess
	defb	'gboot: ',0
	jp	amess

rsel1:
 .if 1 ; temporary only
 ld de,2 ; save eprom serial no to 2:fc81
 call copy_serial_no
 .endif
	ld	a,81h
	out0	(TRDR),A		; command byte to set RSEL=1
	ld	a,13h
	out0	(CNTR),A		; TE=1, divisor = 3, start transmission

	sub	a
	dec	a
	jr	nz,$-1
	dec	a
	jr	nz,$-1
	dec	a
	jr	nz,$-1
	dec	a
	jr	nz,$-1			; delay for command to be processed
 .if 1 ; temporary only
 ld de,200h ; restore eprom serial no from 2:fc81
copy_serial_no:
 ld hl,0fc81h
 push hl
 pop iy
 ld bc,5
 jp copyr
 .else
	ret
 .endif

say_starting:
	call	gmess
	defb	'starting operating system',0dh,0ah,0
	ret

virtual_init:
	call	rsel1

	ld	a,80h
	out0	(CBAR),a		; CA0 = 0k, bank area = 32k, CA1 = 32k

	ld	a,4-8 ;0 also works, see bstartup.s01 at label init
	out0	(CBR),a			; window onto 0:8000, virt memory table

	ld	de,08001h		; address of virtual memory table+1
	ld	bc,0ffh			; size of virtual memory table-1
	; fall into clear_bc_p1_at_de_m1

clear_bc_p1_at_de_m1:
	ld	l,e
	ld	h,d
	dec	hl
	ld	(hl),0

	ld	a,c
	or	b
	ret	z

	ldir
	ret

de_min_hl_de:
	; enter with values in hl and de, returns de = min(hl, de)
	or	a
	sbc	hl,de
	ret	nc			; de is lesser
	add	hl,de			; hl was lesser, restore its value
	ex	de,hl			; and return the original hl in de
	ret

get_ca0_size:
	ld	hl,(e_hsize)		; variable size of header
	ld	de,exe_header_size	; subtract fixed size of header
	jr	calculate_size

get_ca1_size:
	ld	hl,(e_udata)		; ending logical addr in CA1 region
	ld	de,(e_idata)		; starting logical addr in CA1 region
	jr	calculate_size

get_udata_size_m1:
	ld	hl,(e_break)		; ending logical addr to be cleared
	ld	de,(e_udata)		; starting logical addr to be cleared
	inc	de			; special algorithm for correct cf
	; fall into calculate_size

calculate_size:				; enter with hl = start addr, de = end
	or	a
	sbc	hl,de			; find the difference
	ld	c,l
	ld	b,h			; bc = bytes to load for region
	ret

; -----------------------------------------------------------------------------

dir_search:
	ld	(dir_name),hl

 .if 1 ; new kernel format
	call	file_setup		; read inode and prepare to walk file

dir_search_loop:
	ld	bc,SIZEOF_DIRECT_T
	ld	hl,directory_buf
	call	file_read		; returns hl = preserved, cf set up
	ret	c ; also implies nz	; directory search failed (not found)

	inc	hl

	ld	de,(dir_name)
	ld	b,DIRNAMELEN

dir_search_compare_loop:
	inc	hl

	ld	a,(de)
	cp	(hl)
	jr	nz,dir_search_loop	; didn't match, go and read another
	or	a
	jr	z,dir_search_compare_good ; matched, and sentinel was reached

	inc	de
	djnz	dir_search_compare_loop

	; matched, and directory filename was of maximal length
	ld	a,(de)
	or	a
	; if user's filename is exactly maximum, return zf=1 (found)
	; if user's filename is longer than maximum, return zf=0 (not found)

dir_search_compare_good:
	ld	de,(directory_buf)	; de = return value, inode number
	ret				; with zf=1 from the comparisons above
 .else
	ld	hl,inode
	call	inode_read		; read inode of the dir to be searched

	call	bmap_setup		; prepare counters to walk the file
	jr	dir_search_loope

dir_search_loop:
	push	hl			; h = count of entries in current block
	call	bmap_block		; get de = block no. of current block
					; and increment the block position
	ld	a,e
	or	d
	jr	z,bad_dir_block		; hole in directory

	ld	hl,block
	push	hl
	call	block_read
	pop	hl
	pop	bc			; b = count of entries in current block

dir_search_entry_loop:
	push	hl
	inc	hl

	ld	de,(dir_name)
	ld	c,DIRNAMELEN

dir_search_compare_loop:
	inc	hl

	ld	a,(de)
	cp	(hl)
	jr	nz,dir_search_compare_bad
	or	a
	jr	z,dir_search_compare_good

	inc	de
	dec	c
	jr	nz,dir_search_compare_loop

	; matched, and directory filename was of maximal length
	ld	a,(de)
	or	a
	; if user's filename is exactly maximum, return zf=1 (found)
	; if user's filename is longer than maximum, return zf=0 (not found)

dir_search_compare_good:
	pop	hl
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	ret

dir_search_compare_bad:
	pop	hl
	ld	de,SIZEOF_DIRECT_T
	add	hl,de
	djnz	dir_search_entry_loop

dir_search_loope:
	call	bmap_remain		; get hl = valid bytes in current block
					; and decrement the file size remaining
	add	hl,hl
	add	hl,hl
	add	hl,hl
	add	hl,hl			; assumes SIZEOF_DIRECT_T = 16!!

	ld	a,h			; a = valid bytes / SIZEOF_DIRECT_T
	or	a
	jr	nz,dir_search_loop

	inc	a			; nz
	ret

bad_dir_block:
	call	gmess
	defb	'hole in directory',0dh,0ah,0
	jp	error_bomb
 .endif

; -----------------------------------------------------------------------------

file_scan:
	ld	hl,region_list
	ld	(region_ptr),hl

 .if 0 ; new kernel format
	ld	hl,inode
	call	inode_read		; read inode of the target file
 .endif

	call	bmap_setup		; prepare counters to walk the file
	call	bmap_remain		; get hl = valid bytes in first block
					; and decrement the file size remaining
	ld	a,l
	or	h			; any bytes in file?
	jr	z,bad_file_size		; no, abort proceedings

file_scan_loop:
	call	bmap_block		; get de = block no. of current block
					; and increment the block position
	ld	a,e
	or	d
	jr	z,bad_file_block
; ld l,e
; ld h,d
; call ahexw

	ld	a,e
	and	PAGE_BLOCKS-1		; check it starts on a page boundary
	jr	nz,bad_alignment

	ld	hl,(region_ptr)
	ld	(hl),e
	inc	hl
	ld	(hl),d			; stash away the starting block no.
	inc	hl
	ld	(region_ptr),hl		; for the current region (bumped)

	ld	b,REGION_BLOCKS-1

file_region_loop:
	push	bc
	push	de

	call	bmap_remain		; get hl = valid bytes in current block
					; and decrement the file size remaining
	ld	a,l
	or	h
	jr	z,file_region_final

	call	bmap_block		; get de = block no. of current block
					; and increment the block position
	ld	a,e
	or	d
	jr	z,bad_file_block
; ld a,' '
; call abyte
; ld l,e
; ld h,d
; call ahexw

	ex	de,hl			; hl = the actual block no. from file
	pop	de
	inc	de			; de = expected, contiguous block no.

	or	a
	sbc	hl,de
	jr	nz,bad_alignment

	pop	bc
	djnz	file_region_loop
; call acrlf

	call	bmap_remain		; get hl = valid bytes in current block
					; and decrement the file size remaining
	ld	a,l
	or	h			; anything left to read?
	jr	nz,file_scan_loop	; yes (scan is limited by file size)
	ret				; last region is = REGION_BLOCKS blocks

file_region_final:
; call acrlf
	pop	hl			; last region is < REGION_BLOCKS blocks
	pop	hl			; clean up saved variables, and done
	ret

bad_file_size:
	call	gmess
	defb	'null file',0dh,0ah,0
	jp	error_bomb

bad_file_block:
	call	gmess
	defb	'hole in file',0dh,0ah,0
	jp	error_bomb

bad_alignment:
	call	gmess
	defb	'not aligned',0dh,0ah,0
	jp	error_bomb

; -----------------------------------------------------------------------------

 .if 1 ; new kernel format
file_read:
	; enter with hl -> user buffer, bc = bytes to read (must be nonzero!!)

	push	hl			; this will be preserved for caller
	push	bc			; this will be used to set cf on return

	ld	(read_user),bc
	ld	(read_user_ptr),hl

	ld	hl,0
	ld	(read_done),hl

	ld	hl,(read_remain)
	ld	a,l
	or	h			; got any leftover data in buffer?
	jr	nz,file_read_entry	; yes, use it first

file_read_loop:
	; read a new block into the buffer, recording no. of valid bytes read
	call	bmap_remain		; get hl = valid bytes in current block
					; and decrement the file size remaining
	ld	a,l
	or	h
	jr	z,file_read_done	; reached end of file

	ld	(read_remain),hl

	call	bmap_block		; get de = block no. of current block
					; and increment the block position
	ld	a,e
	or	d
	jr	z,bad_file_block	; hole in file

	ld	hl,block
	ld	(read_remain_ptr),hl
	call	block_read

file_read_entry:
	; satisfy as much as possible of user's request from current buffer
	ld	hl,(read_remain)	; bytes available
	ld	de,(read_user)		; bytes wanted
	call	de_min_hl_de		; de = amount to be processed this time

	ld	hl,(read_remain)
	or	a
	sbc	hl,de
	ld	(read_remain),hl	; reduce bytes available by amount

	ld	hl,(read_user)
	or	a
	sbc	hl,de
	ld	(read_user),hl		; reduce bytes wanted by amount

	ld	hl,(read_done)
	add	hl,de
	ld	(read_done),hl		; increase bytes copied by amount

	ld	c,e
	ld	b,d
	ld	de,(read_user_ptr)
	ld	hl,(read_remain_ptr)
	ldir
	ld	(read_remain_ptr),hl
	ld	(read_user_ptr),de

	; see if the user's entire request has now been processed
	ld	hl,(read_user)
	ld	a,l
	or	h
	jr	nz,file_read_loop	; no, get another block

file_read_done:
	ld	hl,(read_done)		; indicates how much was really read
	ld	c,l
	ld	b,h			; bc = return value, how much was read

	pop	de			; de = size of user's original request
	or	a
	sbc	hl,de			; set cf=1 if less than amount wanted

	pop	hl			; hl preserved for convenience of user
	ret

; -----------------------------------------------------------------------------

file_setup:
	; enter with de = inode number for file to be read

	ld	hl,inode
	call	inode_read		; read inode of the dir to be searched

	ld	hl,0
	ld	(read_remain),hl	; forces a new data block to be loaded
	; fall into bmap_setup
 .endif

bmap_setup:
	ld	hl,(inode+8)
	ld	(size_remain),hl
	ld	hl,(inode+10)
	ld	(size_remain+2),hl

	ld	hl,inode+24
	ld	(direct_ptr),hl
	ld	hl,indirect_block+BUFSIZE
	ld	(indirect_ptr),hl
	ld	hl,dindirect_block+BUFSIZE
	ld	(dindirect_ptr),hl
	ret

bmap_remain:
	ld	hl,(size_remain)
	ld	de,(size_remain+2)

	ld	bc,BUFSIZE
	or	a
	sbc	hl,bc
	ex	de,hl
	ld	b,0 ; ld bc,0
	sbc	hl,bc
	ex	de,hl
	jr	c,bmap_remain_final

	ld	(size_remain),hl
	ld	(size_remain+2),de

	ld	hl,BUFSIZE
	ret				; return a complete block of size hl

bmap_remain_final:
	ld	hl,(size_remain)
	ld	(size_remain),bc ; 0
	;ld	(size_remain+2),bc ; 0
	ret				; return a partial block of size hl

bmap_block:
	ld	hl,(direct_ptr)
	ld	de,inode+24+DIRECTBLOCKS*2
	or	a
	sbc	hl,de
	jr	nc,bmap_indirect

	ld	hl,(direct_ptr)
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	inc	hl
	ld	(direct_ptr),hl
	ret

bmap_indirect:
	ld	hl,(indirect_ptr)
	ld	de,indirect_block+BUFSIZE
	or	a
	sbc	hl,de
	jr	c,bmap_indirect_already

	ld	hl,(direct_ptr)
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	inc	hl
	ld	(direct_ptr),hl

	ld	hl,indirect_block
	ld	(indirect_ptr),hl
	call	block_read

bmap_indirect_already:
	ld	hl,(direct_ptr)
	ld	de,inode+24+DIRECTBLOCKS*2+INDIRECTBLOCKS*2+1 ; fudge!
	or	a
	sbc	hl,de
	jr	nc,bmap_dindirect

	ld	hl,(indirect_ptr)
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	inc	hl
	ld	(indirect_ptr),hl
	ret

bmap_dindirect:
	ld	hl,(dindirect_ptr)
	ld	de,dindirect_block+BUFSIZE
	or	a
	sbc	hl,de
	jr	c,bmap_dindirect_already

	ld	hl,(indirect_ptr)
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	inc	hl
	ld	(indirect_ptr),hl

	ld	hl,dindirect_block
	ld	(dindirect_ptr),hl
	call	block_read

bmap_dindirect_already:
	ld	hl,(dindirect_ptr)
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	inc	hl
	ld	(dindirect_ptr),hl
	ret

; -----------------------------------------------------------------------------
; enter with hl -> buffer, de = inode number

inode_read:
	push	hl

	ld	a,e
	and	DINODESPERBLOCK-1	; find inode position within disk block

	ld	b,DINODESPERBLOCKLOG	; shift count to find which disk block
inode_shift:
	srl	d
	rr	e
	djnz	inode_shift		; leaves de = block (relative to itab)

	ld	hl,(isize)
	scf
	sbc	hl,de			; check if block is beyond end of table
	jr	c,bad_inode

	ld	hl,(reserv)
	add	hl,de
	ex	de,hl			; adjust block for start of inode table

	ld	hl,block
	push	af
	push	hl
	call	block_read
	pop	hl
	pop	de			; d = inode position within block

	ld	bc,SIZEOF_DINODE_T
	ld	e,c
	mlt	de
	add	hl,de
	pop	de			; de -> user's buffer for disk inode
	ldir				; copy it, freeing up block buffer
	ret

bad_inode:
	call	gmess
	defb	'bad inode: ',0
	add	a,'0'
	call	abyte
	call	amess
	defb	', ',0
	ex	de,hl
	jp	error_ahexw

; -----------------------------------------------------------------------------
; enter with hl -> buffer, de = block number

block_read:
	push	hl
	call	block_xlate		; a:hl -> data in ramdrive

	push	hl
	pop	iy
	ld	d,a			; d:iy -> data in ramdrive

	pop	hl
	ld	e,3			; e:hl -> user's buffer in seg 3:
	jp	copyr			; copy BUFSIZE bytes from d:iy to e:hl

block_write:
	push	hl
	call	block_xlate		; a:hl -> data in ramdrive

	pop	iy
	ld	d,3			; d:iy -> user's buffer in seg 3:

	ld	e,a			; e:hl -> data in ramdrive
	jp	copyr			; copy BUFSIZE bytes from d:iy to e:hl

block_xlate:
	ld	hl,HD0_SIZE
	scf
	sbc	hl,de			; check whether block no is in range
	jr	c,bad_block

	ld	bc,BUFSIZE		; bytes to be copied for 1 block

	ld	hl,HD0_START
	add	hl,de			; hl = block index (from start of ram)

	ld	a,h
	ld	h,l
	ld	l,c ;0			; a:hl = block index * 100h

	add	hl,hl
	adc	a,a			; a:hl = block index * BUFSIZE (200h!!)
	ret

block_to_bbr:
	ld	a,(hl)
	inc	hl
	ld	d,(hl)			; d:a = block no. from start of disk
	inc	hl

	srl	d
	rra
	srl	d
	rra
	srl	d
	rra				; a = block / 8 (PAGE_BLOCKS = 8!!)

	add	a,HD0_START/PAGE_BLOCKS-4
	ret

bad_block:
	call	gmess
	defb	'bad block: ',0
	ex	de,hl
	jp	error_ahexw

; -----------------------------------------------------------------------------

initial_boot:
	ld	hl,UZINAM
	call	LDFILE			; copy entire KERNEL.BIN data to c:0000
	ret	z			; halt if the file could not be found

	call	gmess
	defb	'loading A:KERNEL.BIN',0dh,0ah,0

	ld	a,'1'
	ld	(argument_1+7),a	; so kernel will use /dev/hd1 as root

loop:	ld	de,(LDMAX)
	or	a
	sbc	hl,de			; did we load an entire extent?
	jr	nz,done			; no, assume the file is fully loaded

	add	hl,de			; restore hl = LDMAX
	ld	(LDPTR),hl		; load from where we finished last time

	ld	a,l
	or	h			; did we wrap around to a new segment?
	jr	nz,cont

	ld	hl,LDSEG
	inc	(hl)			; yes, need to advance segment counter

cont:	ld	hl,4000h
	add	hl,de
	ld	(LDMAX),hl		; advance LDMAX to load 4000h bytes

	ld	hl,LDEXT
	inc	(hl)			; advance extent counter to next extent

	ld	hl,UZINAM
	call	LDFILE			; copy more CMX.BIN data to LDSEG:LDPTR
	jr	nz,loop

	ld	hl,LDEXT
	dec	(hl)			; account for extent that wasn't found

done:
	call	virtual_init		; modifies CBR (can't call EPROM now)

	ld	iy,0
	ld	hl,exe_header_buf
	ld	de,0c03h		; copy from c: to 3:
	ld	bc,exe_header_size
	call	copyr			; read the executable's header

	call	check_e_magic		; ensure e_magic = E_MAGIC
	call	check_e_format		; ensure e_format = E_FORMAT_KERNEL

	call	get_ca0_size		; returns bc = size, de = start of copy
	push	de
	pop	iy

	ld	de,0c00h		; copy from c: to 0:
	ld	l,e
	ld	h,e			; destination = 0 (start of CA0 region)
	call	copyr			; returns iy advanced, d & e preserved
	push	de

	call	get_ca1_size		; returns bc = size, de = start of copy
	ld	hl,4000h-8000h
	add	hl,de			; correct for the intended CBR = 4-8

	pop	de			; copy from c: to 0:
	call	copyr

	call	get_udata_size_m1	; returns bc = size-1, de = start+1
	call	nc,clear_bc_p1_at_de_m1	; if bc => 0, clear bc+1 bytes at de-1

	call	say_starting		; tell the user we have liftoff

	ld	a,(LDEXT)
	inc	a			; do all pages, including the first
	ld	b,a			; how many extents were loaded ?  (-1)

	ld	hl,8080h		; initial spot in virt memory table
	ld	a,0c0h-4		; bbr value to access 1st loaded page

virtual_loop:
	ld	(hl),a			; map 16kbytes into virt memory space
	inc	hl
	add	a,4
	djnz	virtual_loop

	jp	kazumi

; -----------------------------------------------------------------------------

$ diag.inc
$ copyr.inc
$ clears.inc

; -----------------------------------------------------------------------------

parameters:	defw	2		; argc
		defw	arguments-parameters_end ; argv
		defw	environment-parameters_end ; envp
arguments:	defw	argument_0-parameters_end
		defw	argument_1-parameters_end
		defw	0		; terminates the argument list
argument_0:	defb	'/boot/kernel.bin',0
argument_1:	defb	'root=hd0',0
environment:	defw	environment_0-parameters_end
		defw	0		; termninates the environment list
environment_0:	defb	'SN=00000',0
parameters_end:	; this will equal address 0 after parameters are copied

name_boot:	defb	'boot',0
name_kernel_bin: defb	'kernel.bin',0

		; note: defb 1 is very important here, because if UZINAM ends
		; up on a 32 byte boundary, the KERNEL.BIN filename (extent 0)
		; may be seen by the initial_boot routine, preventing the
		; proper loading of /boot/kernel after formatting the ramdrive
UZINAM: 	defb	1,'KERNEL  BIN'		; LDNAM
LDEXT:		defb	0			; LDEXT
LDSEG:		defb	0ch			; LDSEG
LDPTR:		defw	0			; LDPTR
LDMAX:		defw	4000h			; LDMAX

; -----------------------------------------------------------------------------

final:		; initialised code and data ends here

reserv:
isize		equ	reserv+2
fsize		equ	isize+2

dir_name	equ	fsize+2
size_remain	equ	dir_name+2

direct_ptr	equ	size_remain+4
indirect_ptr	equ	direct_ptr+2
dindirect_ptr	equ	indirect_ptr+2

inode		equ	dindirect_ptr+2
block		equ	inode+SIZEOF_DINODE_T

indirect_block	equ	block+BUFSIZE
dindirect_block	equ	indirect_block+BUFSIZE

 .if 1 ; new kernel format
directory_buf	equ	dindirect_block+BUFSIZE

read_remain	equ	directory_buf+SIZEOF_DIRECT_T
read_remain_ptr	equ	read_remain+2
read_user	equ	read_remain_ptr+2
read_user_ptr	equ	read_user+2
read_done	equ	read_user_ptr+2

exe_header_buf	equ	read_done+2
e_magic		equ	exe_header_buf
e_format	equ	e_magic+2
e_size		equ	e_format+2
e_hsize		equ	e_size+4
e_idata		equ	e_hsize+2
e_entry		equ	e_idata+2
e_udata		equ	e_entry+2
e_stack		equ	e_udata+2
e_break		equ	e_stack+2
exe_header_size	equ	e_break+2-exe_header_buf

region_ptr	equ	exe_header_buf+exe_header_size
 .else
region_ptr	equ	dindirect_block+BUFSIZE
 .endif
region_list	equ	region_ptr+2

; -----------------------------------------------------------------------------

	END
