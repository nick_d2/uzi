;	NULLRJMP.ASM

; -----------------------------------------------------------------------------

.INCLUDE "4433DEF.INC"

; -----------------------------------------------------------------------------

.CSEG

RESET_ENTRY:
	RJMP	RESET_ENTRY

; -----------------------------------------------------------------------------

	; BODEN = 0 ENABLED
	; BODLEVEL = 1 FOR 2.7V

	; CKSEL = 010 FOR 64 MS R/C + 16K * 11 MHZ STARTUP DELAY

	.ORG	$1000

	.DW	$4106			; HARDWARE ADDRESS COMMAND
	.DW	$4207			; SOFTWARE ADDRESS COMMAND
	.DW	$ACB2			; WRITE FUSE BITS COMMAND

; -----------------------------------------------------------------------------

