; DOS.ASM

; -----------------------------------------------------------------------------

EXTRN	GETSTRLOC:FAR
EXTRN	GETSTRALLOC:FAR
EXTRN	RLSSTRALLOC:FAR

EXTRN	DOS_READY:WORD
EXTRN	DOS_ERROR:WORD
EXTRN	DOS_COUNT:WORD

CODE	SEGMENT

	ASSUME	CS:CODE,DS:CODE

B	EQU	BYTE PTR
W	EQU	WORD PTR
D	EQU	DWORD PTR

	JUMPS
	LOCALS

; -----------------------------------------------------------------------------

PUBLIC	DOS_PRINT

DOS_PRINT:				; DOS_DATA$
	PUSH	BP
	MOV	BP,SP
	PUSH	DS
	PUSH	SI
	PUSH	DI

	LDS	SI,[BP+6]		; DOS_DATA$
	CALL	STRING_PTR_DS_DX	; DS:DX -> CX BYTE STRING

	MOV	AH,40H
	MOV	BX,1			; STDOUT
	INT	21H

	CALL	DOS_DONE		; SET UP COUNT/ERROR/READY

	POP	DI
	POP	SI
	POP	DS
	POP	BP
	RETF	4

; -----------------------------------------------------------------------------

	PUBLIC	DOS_PRINT_LINE

DOS_PRINT_LINE:				; DOS_DATA$
	PUSH	BP
	MOV	BP,SP
	PUSH	DS
	PUSH	SI
	PUSH	DI

	LDS	SI,[BP+6]		; DOS_DATA$
	CALL	STRING_PTR_DS_SI
	PUSH	DS
	PUSH	SI
	PUSH	CX

	PUSH	CS
	POP	DS
	MOV	SI,OFFSET DOS_BUF
	MOV	AX,CX
	ADD	AX,2
	PUSH	AX
	CALL	STRING_ALLOC		; ALLOCATE LEN(DOS_DATA$)+2 BYTES

	CALL	STRING_PTR_ES_DI
	POP	AX
	MOV	DX,DI
	MOV	BX,ES

	POP	CX
	POP	SI
	POP	DS
	REP	MOVSB			; COPY USER STRING TO TEMPORARY BUFFER

	MOV	DS,BX			; DS:DX -> TEMPORARY BUFFER
	XCHG	CX,AX

	MOV	AX,0A0DH
	STOSW				; ADD CR/LF

	MOV	AH,40H
	MOV	BX,1			; STDOUT
	INT	21H

	CALL	DOS_DONE		; SET UP COUNT/ERROR/READY

	PUSH	CS
	POP	DS
	MOV	SI,OFFSET DOS_BUF
	CALL	STRING_FREE

	POP	DI
	POP	SI
	POP	DS
	POP	BP
	RETF	4

; -----------------------------------------------------------------------------

	PUBLIC	DOS_GET_DATE		; YEAR%, MONTH%, DAY%

DOS_GET_DATE:
	PUSH	BP
	MOV	BP,SP
	PUSH	DS
	PUSH	SI
	PUSH	DI

	MOV	AH,2AH
	INT	21H			; GET DATE

	LES	DI,[BP+14]		; YEAR%
	XCHG	AX,CX
	STOSW

	LES	DI,[BP+10]		; MONTH%
	MOV	AL,DH
	SUB	AH,AH
	STOSW

	LES	DI,[BP+6]		; DAY%
	MOV	AL,DL
	STOSW

	CALL	DOS_OK			; SET DOS_COUNT% DOS_READY% DOS_ERROR%

	POP	DI
	POP	SI
	POP	DS
	POP	BP
	RETF	12

; -----------------------------------------------------------------------------

	PUBLIC	DOS_GET_TIME		; HOUR%, MINUTE%, SECOND%, HSEC%

DOS_GET_TIME:
	PUSH	BP
	MOV	BP,SP
	PUSH	DS
	PUSH	SI
	PUSH	DI

	MOV	AH,2CH
	INT	21H			; GET TIME

	LES	DI,[BP+18]		; HOUR%
	MOV	AL,CH
	SUB	AH,AH
	STOSW

	LES	DI,[BP+14]		; MINUTE%
	MOV	AL,CL
	STOSW

	LES	DI,[BP+10]		; SECOND%
	MOV	AL,DH
	STOSW

	LES	DI,[BP+6]		; HSEC%
	MOV	AL,DL
	STOSW

	CALL	DOS_OK			; SET DOS_COUNT% DOS_READY% DOS_ERROR%

	POP	DI
	POP	SI
	POP	DS
	POP	BP
	RETF	16

; -----------------------------------------------------------------------------

	PUBLIC	BIOS_KEY

BIOS_KEY:				; K%
	PUSH	BP
	MOV	BP,SP
	PUSH	DS
	PUSH	SI
	PUSH	DI

	PUSH	[BP+8]
	PUSH	[BP+6]
	PUSH	CS
	CALL	BIOS_KEY_TEST		; BIOS_KEY_TEST K%

	LDS	SI,[BP+6]
	CMP	W [SI],0
	JZ	@@DONE			; IF K% THEN

	PUSH	[BP+8]
	PUSH	[BP+6]
	PUSH	CS
	CALL	BIOS_KEY_WAIT		; BIOS_KEY_WAIT K%

@@DONE:	POP	DI
	POP	SI
	POP	DS
	POP	BP
	RETF	4

; -----------------------------------------------------------------------------

	PUBLIC	BIOS_KEY_TEST

BIOS_KEY_TEST:				; K%
	PUSH	BP
	MOV	BP,SP
	PUSH	DS
	PUSH	SI
	PUSH	DI

	LES	DI,[BP+6]
	SUB	AX,AX
	STOSW				; K% = 0

	SUB	AX,AX
	MOV	DS,AX
	MOV	AX,DS:[41AH]
	CMP	AX,DS:[41CH]
	JZ	@@DONE			; DON'T GIVE UP A TIME SLICE

	MOV	AH,1
	INT	16H			; TEST FOR KEY
	JZ	@@DONE

	LES	DI,[BP+6]
	TEST	AL,AL
	JZ	@@EXTD
	SUB	AH,AH
@@EXTD:	STOSW				; K% = RETURNED AX

@@DONE:	POP	DI
	POP	SI
	POP	DS
	POP	BP
	RETF	4

; -----------------------------------------------------------------------------

	PUBLIC	BIOS_KEY_WAIT

BIOS_KEY_WAIT:				; K%
	PUSH	BP
	MOV	BP,SP
	PUSH	DS
	PUSH	SI
	PUSH	DI

	SUB	AH,AH
	INT	16H			; WAIT FOR KEY

	LES	DI,[BP+6]
	TEST	AL,AL
	JZ	@@EXTD
	SUB	AH,AH
@@EXTD:	STOSW				; K% = RETURNED AX

	POP	DI
	POP	SI
	POP	DS
	POP	BP
	RETF	4

; -----------------------------------------------------------------------------

DOS_DONE:
	MOV	SI,DATA
	MOV	DS,SI
	JC	@@BAD

	MOV	W [DOS_COUNT],AX
	MOV	W [DOS_ERROR],0
	MOV	W [DOS_READY],-1
	RET

@@BAD:	MOV	W [DOS_COUNT],0
	MOV	W [DOS_ERROR],AX
	MOV	W [DOS_READY],0
	RET

; -----------------------------------------------------------------------------

DOS_OK:
	MOV	SI,DATA
	MOV	DS,SI

	MOV	W [DOS_COUNT],0
	MOV	W [DOS_ERROR],0
	MOV	W [DOS_READY],-1
	RET

; -----------------------------------------------------------------------------

STRING_PTR_DS_DX:
	PUSH	[SI]
	CALL	GETSTRLOC

	MOV	DS,DX
	XCHG	DX,AX			; DS:DX -> CX BYTE STRING
	RET

STRING_PTR_DS_SI:
	PUSH	[SI]
	CALL	GETSTRLOC

	MOV	DS,DX
	XCHG	SI,AX			; DS:SI -> CX BYTE STRING
	RET

STRING_PTR_ES_DI:
	PUSH	[SI]
	CALL	GETSTRLOC

	MOV	ES,DX
	XCHG	DI,AX			; ES:DI -> CX BYTE STRING
	RET

STRING_FREE:
	CMP	W [SI],0
	JZ	@@RET

	PUSH	[SI]
	MOV	W [SI],0
	CALL	RLSSTRALLOC
@@RET:	RET

STRING_ALLOC:
	PUSH	DS
	PUSH	SI

	PUSH	AX
	CALL	GETSTRALLOC

	POP	SI
	POP	DS
	MOV	[SI],AX
	RET

; -----------------------------------------------------------------------------

DOS_BUF		DW	0

; -----------------------------------------------------------------------------

CODE	ENDS

; -----------------------------------------------------------------------------

DATA	SEGMENT
DATA	ENDS

; -----------------------------------------------------------------------------

	END
