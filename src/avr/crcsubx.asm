; CRCSUB.ASM

; -----------------------------------------------------------------------------

EXTRN	GETSTRLOC:FAR
EXTRN	GETSTRALLOC:FAR
EXTRN	RLSSTRALLOC:FAR

CODE	SEGMENT

	ASSUME	CS:CODE,DS:CODE

B	EQU	BYTE PTR
W	EQU	WORD PTR
D	EQU	DWORD PTR

	JUMPS
	LOCALS

; -----------------------------------------------------------------------------

;	CRC%=-1				' SEED
;	F$="...."
;	CALL CRC_CALCULATE(CRC%, F$)
;	F$="...."
;	CALL CRC_CALCULATE(CRC%, F$)
;	F$="...."
;	CALL CRC_CALCULATE(CRC%, F$)

PUBLIC	CRC_CALCULATE

CRC_CALCULATE:
	PUSH	BP
	MOV	BP,SP
	PUSH	SI
	PUSH	DS

	LDS	SI,[BP+6]
	LODSW
	PUSH	AX
	CALL	GETSTRLOC
	JCXZ	@@L0

	LDS	SI,[BP+10]
	MOV	BX,[SI]			; CRC%

	MOV	SI,AX
	MOV	DS,DX

	; ENTER WITH
	; INITIAL CRC IN BX
	; CX = BYTE COUNT
	; SI --> BUFFER
	; RETURN WITH
	; NEW CRC IN BX

	CLD
	MOV	DX,1021H
@@L1:	LODSB
	XOR	BH,AL
	REPT	8
	SHL	BX,1
	JNC	$+4
	XOR	BX,DX
	ENDM
	LOOP	@@L1

	LDS	SI,[BP+10]
	MOV	[SI],BX			; CRC%

@@L0:	POP	DS
	POP	SI
	POP	BP
	RETF	8

; -----------------------------------------------------------------------------

CODE	ENDS

; -----------------------------------------------------------------------------

DATA	SEGMENT
DATA	ENDS

; -----------------------------------------------------------------------------

	END
