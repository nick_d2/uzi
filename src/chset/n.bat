copy chset.sh ..\..\bin
copy demos\demos.sh ..\..\bin

call cognitiv.bat
copy demos\cog-lab ..\..\bin\demos

call headers.bat
copy demos\ned01-1 ..\..\bin\demos
copy demos\ned01-2 ..\..\bin\demos
copy demos\ned01-3 ..\..\bin\demos
copy demos\ned01-4 ..\..\bin\demos
copy demos\ned02-1 ..\..\bin\demos
copy demos\ned02-2 ..\..\bin\demos
copy demos\ned02-3 ..\..\bin\demos
copy demos\ned02-4 ..\..\bin\demos
copy demos\ned02-5 ..\..\bin\demos
copy demos\ned02-6 ..\..\bin\demos
copy demos\ned03-1 ..\..\bin\demos
copy demos\ned03-2 ..\..\bin\demos
copy demos\ned04-1 ..\..\bin\demos
copy demos\ned04-2 ..\..\bin\demos
copy demos\ned04-3 ..\..\bin\demos
copy demos\ned05-1 ..\..\bin\demos
copy demos\ned05-2 ..\..\bin\demos
copy demos\ned05-3 ..\..\bin\demos
copy demos\ned05-4 ..\..\bin\demos
copy demos\ned05-5 ..\..\bin\demos
copy demos\ned06-1 ..\..\bin\demos
copy demos\ned06-2 ..\..\bin\demos
copy demos\ned06-3 ..\..\bin\demos
copy demos\ned06-4 ..\..\bin\demos
copy demos\ned06-5 ..\..\bin\demos
copy demos\ned06-6 ..\..\bin\demos

call printer.bat
copy demos\lpr-rec ..\..\bin\demos

