; setjmpl.asm
; Nick's reverse engineered subset of the IAR large 'C' library

; -----------------------------------------------------------------------------

		module	_setjmpl
		rseg	CODE
code_base:
		public	setjmp
setjmp		equ	code_base+00000000h
		defb	021h,002h,000h,039h,0EBh,073h,023h
		defb	072h,023h,071h,023h,070h,023h,0DDh
		defb	0E5h,0D1h,073h,023h,072h,023h,0FDh
		defb	0E5h,0D1h,073h,023h,072h,023h,0D1h
		defb	0D5h,073h,023h,072h,021h,000h,000h
		defb	0C9h
		endmod

; -----------------------------------------------------------------------------

		END
