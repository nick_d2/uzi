; longjmpl.asm
; Nick's reverse engineered subset of the IAR large 'C' library

; -----------------------------------------------------------------------------

		module	_longjmpl
		rseg	CODE
code_base:
		public	longjmp
longjmp		equ	code_base+00000000h
		defb	078h,0B1h,020h,001h,003h,0EBh,05Eh
		defb	023h,056h,023h,0EBh,0F9h,0EBh,0C5h
		defb	04Eh,023h,046h,023h,05Eh,023h,056h
		defb	023h,0D5h,0DDh,0E1h,05Eh,023h,056h
		defb	023h,0D5h,0FDh,0E1h,05Eh,023h,056h
		defb	0E1h,0D5h,0C9h
		endmod

; -----------------------------------------------------------------------------

		END
