/* malloc.c */
/* Copyright (C) 1984 by Manx Software Systems */

#include "malloc-l.h"

/*static*/ FREE head, *last; /* see #defines in malloc-l.h */

void *malloc(size_t size)
{
	register FREE *tp, *prev;
	size_t units;

#ifdef MALLOC_DEBUG
 printf("malloc(0x%x) starting\n", size);
 fflush(stdout);
#endif

	units = (size+sizeof(FREE)-1)/sizeof(FREE) + 1;
	if ((prev = last) == NULL)
		last = head.f_chain = prev = &head;

	for (tp = prev->f_chain ; ; prev = tp, tp = tp->f_chain) {
		while (tp != tp->f_chain && tp+tp->f_size == tp->f_chain) {
			if (last == tp->f_chain)
				last = tp->f_chain->f_chain;
			tp->f_size += tp->f_chain->f_size;
			tp->f_chain = tp->f_chain->f_chain;
		}

		if (tp->f_size >= units) {
			if (tp->f_size == units)
				prev->f_chain = tp->f_chain;
			else {
				last = tp + units;
				prev->f_chain = last;
				last->f_chain = tp->f_chain;
				last->f_size = tp->f_size - units;
				tp->f_size = units;
			}
			last = prev;
			tp->f_chain = NULL;
#ifdef MALLOC_DEBUG
 printf("malloc() returning 0x%x\n", tp+1);
 fflush(stdout);
#endif
			return (char *)(tp+1);
		}
		if (tp == last) {
			if ((tp = (FREE *)sbrk(GRAIN)) == (FREE *)-1)
 {
#ifdef MALLOC_DEBUG
 printf("malloc() returning NULL\n");
 fflush(stdout);
#endif
				return (char *)NULL;
 }
			tp->f_size = GRAIN/sizeof(FREE);
			tp->f_chain = NULL;
			free(tp+1);
			tp = last;
		}
	}
}

