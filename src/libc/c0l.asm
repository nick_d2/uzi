; c0l.asm by Nick for UZI180 large memory model

; -----------------------------------------------------------------------------

 .if 0
$ io64180.s01
 .endif

	module	_c0l

	public	__init			; vendor _init
 .if 0
	extern	brk			; vendor _brk
 .endif
	extern	main			; vendor _main
	extern	exit			; vendor _exit

;	extern	l__DEFAULT		; e_hsize
	extern	s_RCODE			; e_idata
	extern	s_UDATA0		; e_udata
	extern	s_CSTACK		; e_stack
	extern	e_CSTACK		; e_break

 .if 0
	extern	?BANK_CALL_DIRECT_L08
 .endif

E_MAGIC		equ	0a6c9h
E_FORMAT_LARGE	equ	1
E_STACK_SIZE	equ	1000h

; -----------------------------------------------------------------------------
; forward definitions of segments, to set the linkage order (c0l must be first)

	rseg	RCODE
	rseg	CODE			; needs to be at start for large model
;	rseg	TEMP
;	rseg	DATA0
;	rseg	WCSTR
	rseg	CONST
	rseg	CSTR
	rseg	IDATA0(NUL)
	rseg	CDATA0
	rseg	ECSTR(NUL)
	rseg	CCSTR
	rseg	UDATA0
	rseg	CSTACK

; -----------------------------------------------------------------------------

 .if 1
	rseg	_DEFAULT		; means header is discarded at loading
 .else
	rseg	RCODE			; means header is visible to loaded pgm
 .endif

 .if 1
	defw	E_MAGIC			; e_magic
	defw	E_FORMAT_LARGE		; e_format
	defd	12345678h		; e_size
	defw	14h ;l__DEFAULT		; e_hsize
	defw	s_RCODE			; e_idata
	defw	__init			; e_entry
	defw	s_UDATA0		; e_udata
	defw	s_CSTACK		; e_stack
	defw	e_CSTACK		; e_break
 .else
	jp	__init
 .endif

; -----------------------------------------------------------------------------

	rseg	RCODE

__init::
 .if 0
 ld a,'A'
 call abyte
 .endif

 .if 0
	ld	de,.sfb.CSTACK	; 1st argument to brk()

 .if 0
	ld	hl,LWRD brk
	ld	a,BYTE3 brk
	call	?BANK_CALL_DIRECT_L08
 .else
	call	brk
 .endif

	call	seg_init
 .endif

; now there are the next stack structure:
;	+4 envp
;	+2 argv
; sp->	+0 argc
 .if 1
	pop	de
	ld	(_argc),de		; vendor __argc (2nd argument to main)
	pop	bc
	ld	(_argv),bc		; vendor __argv (1st argument to main)
	pop	hl
	ld	(environ),hl		; vendor _environ
 .else
	ld	ix,0
	add	ix,sp
	ld	l,(ix+4)
	ld	h,(ix+5)
	ld	(environ),hl		; vendor _environ
	ld	c,(ix+2)
	ld	b,(ix+3)
	ld	(_argv),bc		; vendor __argv (1st argument to main)
	ld	e,(ix+0)
	ld	d,(ix+1)
	ld	(_argc),de		; vendor __argc (2nd argument to main)
 .endif

 .if 0
	ld	HL,LWRD main		; banked call to _main()
	ld	A,BYTE3 main
	call	?BANK_CALL_DIRECT_L08
 .else
	call	main ; vendor _main	; non-banked call to _main()
 .endif

	ex	de,hl			; de = exitcode (1st argument to exit)

 .if 0
	ld	HL,LWRD exit		; banked call to _exit()
	ld	A,BYTE3 exit
	jp	?BANK_CALL_DIRECT_L08
 .else
	jp	exit ; vendor _exit	; non-banked call to _exit()
 .endif

; -----------------------------------------------------------------------------

 .if 0
seg_init:
	ld	HL,.sfe.UDATA0
	ld	DE,.sfb.UDATA0
	call	zero_mem

	ld	DE,.sfb.IDATA0		; destination address
	ld	HL,.sfe.CDATA0 ;+4000h	; really 8:0000 + .sfe.CDATA0
	ld	BC,.sfb.CDATA0 ;+4000h	; really 8:0000 + .sfb.CDATA0
	call	copy_mem

	ld	DE,.sfb.ECSTR			; destination address
	ld	HL,.sfe.CCSTR ;+4000h		; really 8:0000 + .sfe.CCSTR
	ld	BC,.sfb.CCSTR ;+4000h		; really 8:0000 + .sfb.CCSTR

	; Just fall in to the copy_mem function

copy_mem:
	XOR	A
	SBC	HL,BC
	PUSH	BC
	LD	C,L
	LD	B,H				; BC - that many bytes
	POP	HL				; source address
	RET	Z				; If block size = 0 return now
	LDIR
	RET

zero_mem:
	XOR	A
again:	PUSH	HL
	SBC	HL,DE
	POP	HL
	RET	Z
	LD	(DE),A
	INC	DE
	JR	again
 .endif

; -----------------------------------------------------------------------------

 .if 0
	public	_abyte

_abyte::
 .if 0 ; SDCC
	ld	hl,2
	add	hl,sp
	ld	a,(hl)
 .else ; IAR
	ld	a,e
 .endif

	public	abyte

abyte::
 .if 0
 ret
 .else
	push	af
 .endif

L1$:	in0	a,(STAT1)
	and	10b
	jr	z,L1$

	pop	af
	out0	(TDR1),a

 .if 0
 di
 .endif
 .if 0
 ei
 .endif
	ret

	public	_acrlf

_acrlf::

	public	acrlf

acrlf::
	ld	a,0dh
	call	abyte
	ld	a,0ah
	jr	abyte

	public	_ahexw

_ahexw::
 .if 0 ; SDCC
	ld	hl,2
	add	hl,sp
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
 .endif
	ex	de,hl

	public	ahexw

ahexw::
	ld	a,h
	call	ahexb
	ld	a,l

	public	ahexb

ahexb::
	push	af
	rrca
	rrca
	rrca
	rrca
	call	ahexn
	pop	af

	public	ahexn

ahexn::
	and	0fh
	add	a,90h
	daa
	adc	a,40h
	daa
	jr	abyte

	public	amess

amess::
	ex	(sp),hl
	push	af

L01$:	ld	a,(hl)
	inc	hl
	or	a
	jr	z,L02$

	call	abyte
	jr	L01$

L02$:	pop	af
	ex	(sp),hl
	ret
 .endif

; -----------------------------------------------------------------------------

		public	_argc, _argv, environ, errno, __cleanup
		;public	__argc, __argv, _environ, _errno, ___cleanup

		rseg	UDATA0
_argc:		defs	2		; vendor __argc
_argv:		defs	2		; vendor __argv
environ:	defs	2		; vendor _environ
errno:		defs	2		; vendor _errno
__cleanup:	defs	2		; vendor ___cleanup

		rseg	CSTACK
		defs	E_STACK_SIZE

; -----------------------------------------------------------------------------

	END
