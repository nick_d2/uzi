; longjmpb.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

	module	_longjmpb

	rseg	CODE

	extern	?BANK_FAST_LEAVE_L08

	public	longjmp

longjmp:
 .if 1
	ld	a,b
	or	c
	jr	nz,ok_return
	inc	bc
ok_return:
	ex	de,hl
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	inc	hl
	ex	de,hl
	ld	sp,hl
	ex	de,hl
	push	bc
	ld	c,(hl)
	inc	hl
	ld	b,(hl)
	inc	hl
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	inc	hl
	push	de
	pop	ix
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	inc	hl
	push	de
	pop	iy
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	inc	hl
	ld	a,(hl)
	pop	hl
	push	de
	push	af
	jp	?BANK_FAST_LEAVE_L08
 .else
	defb	078h,0B1h,020h,001h,003h,0EBh,05Eh
	defb	023h,056h,023h,0EBh,0F9h,0EBh,0C5h
	defb	04Eh,023h,046h,023h,05Eh,023h,056h
	defb	023h,0D5h,0DDh,0E1h,05Eh,023h,056h
	defb	023h,0D5h,0FDh,0E1h,05Eh,023h,056h
	defb	023h,07Eh,0E1h,0D5h,0F5h,0C3h
	defw	LWRD ?BANK_FAST_LEAVE_L08
 .endif

	endmod

; -----------------------------------------------------------------------------

	end
