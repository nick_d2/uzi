/* etime.c added by Nick from UZI280 clib, since not included in UZIX1.0 */

#include <time.h> /* also types.h */
#include <unix.h> /* for the HZ definition */
#define HZ TICKSPERSEC /* due to change in nomenclature */

/*  Convert date to string */

static char *s;

#if 0 /* Nick... see ctime.c, it is more advanced and has GMT conversion */
char *
ctime(t)
register time_t *t;
{
    static char str[24];

    s = str;

    dout((t->t_time&0xf800)>>11);
    *s++ = ':';
    dout((t->t_time&0x07e0)>>5);
    *s++ = ':';
    dout(t->t_time&0x001f * 2);
    *s++ = ' ';
    dout((t->t_date&0x01e0)>>5);
    *s++ = '/';
    dout(t->t_date&0x001f);
    *s++ = '/';
    dout((t->t_date&0xfe00)>>9);
    *s = '\0';

    return(str);
}
#endif


/* Convert elapsed time to string as: hh:mm:ss.t */

char *
etime(t)
register time_t *t;
{
    static char str[24];

    s = str;

    dout(t->t_date / 60);
    *s++ = ':';
    dout(t->t_date % 60);
    *s++ = ':';
    dout(t->t_time / HZ);
    *s++ = '.';
    *s++ = (((t->t_time % HZ) * 10) / HZ) + '0';
    *s = '\0';

    return(str);
}


static dout(n)
register int n;
{
    n %= 100;
    *s++ = n/10 + '0';
    *s++ = n%10 + '0';
}


/* This decrements t1 by t2 */

ediff(t1, t2)
register time_t *t1, *t2;
{
    if (t1->t_time < t2->t_time)
    {
	--t1->t_date;
	t1->t_time += (60 * HZ);
    }
    t1->t_date -= t2->t_date;
    t1->t_time -= t2->t_time;
}

