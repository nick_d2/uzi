;[]------------------------------------------------------------[]
;|	C0.ASM -- TC UZIX Start	Up Code				|
;|	MSX/HTC/UZIX Run Time Library	version	1.0		|
;[]------------------------------------------------------------[]

	public	small_model
small_model equ	1

;	psect	text,class=CODE
;	psect	strings,class=CODE
;	psect	const,class=CODE
;	psect	data,class=DATA
;	psect	bss,class=DATA
;	psect	_bssend,class=DATA

	extrn	_main:near, __exit:near
	public	start, _exit

; -----------------------------------------------------------------------------

;	signat	_exit,4152	; arg in DE
;	signat	__exit,4152	; arg in DE
;	psect	text
_TEXT	segment	byte public 'CODE'

DGROUP	group	_TEXT, _DATA, _BSS, _BSSEND
	assume	cs:DGROUP, ds:DGROUP

; At the start,	SP points to user stack.
	org	100h
;;; executable header layout
start:	db	0E9h ;0C3h	; for ???
	dw	start0-($+2) ;start0

	org	103h
e_flags	db	0		; bit 7: 1-not refresh system vectors on swapin()
e_text	dw	offset DGROUP:etext
e_data	dw	offset DGROUP:edata
e_bss	dw	offset DGROUP:ebss
e_heap	dw	0
e_stack	dw	0
e_env	dw	offset DGROUP:__argc
;;; total size of header == 16 bytes

;	org	110h		; not necessary, we're already at exactly 110h
start0:
; int 3
	mov	[___stktop], sp

; Clear	BSS
;	mov	bx, (e_data)
;	xchg	dx, bx
;	mov	bx, (e_bss)
;	or	a	; CLC
;	sbc	bx, dx
;	mov	cl, bl
;	mov	ch, bh
;	dec	cx	; cx = counter-1
;	mov	bl, dl
;	mov	bh, dh	; bx = e_data
;	inc	dx	; dx = e_data+1
;	mov	byte ptr [bx], 0
;	ldir		; clear	bss - always >=	10 bytes
	mov	di, [e_data]
	mov	cx, [e_bss]
	sub	cx, di
	sub	al, al
	rep	stosb		; assumes es set up on entry (done by kernel)

	pop	cx		; drop retaddr (required for stack consistency)
; now there are	the next stack structure:
;	+4 envp
;	+2 argv
; sp->	+0 argc
;	mov	bp, 0
;	add	bp, sp
;	mov	bl, [bp+4]
;	mov	bh, [bp+5]
;	mov	[_environ],bx
;	mov	bl, [bp+2]
;	mov	bh, [bp+3]
;	mov	[__argv],bx
;	mov	bl, [bp+0]
;	mov	bh, [bp+1]
;	mov	[__argc],bx
	push	bp
	mov	bp,sp
	mov	ax,[bp+6]
	mov	[_environ],ax
	mov	ax,[bp+4]
	mov	[__argv],ax
	mov	ax,[bp+2]
	mov	[__argc],ax
	pop	bp

start1:
	call	_main
	pop	cx
	pop	cx
	pop	cx

;	xchg	dx, ax		; exit arg in dx
	push	ax		; push first (and only) argument to exit()

_exit:
;	push	dx
	mov	bx, [___cleanup]

;	mov	al, bl
;	or	al, bh
;	call	nz, indirect	; (*__cleanup)(exitcode, ???)
	test	bx,bx
	jz	skip_call
	call	bx
skip_call:

;	pop	dx
;	jp	__exit		; to kernel - arg in dx (no, it's on stack)
	call	__exit
	jmp	$		; dynamic halt if kernel returns (extra safe)

;indirect:
;	jp	(bx)

_TEXT	ends

;[]------------------------------------------------------------[]
;|	Start Up Data Area					|
;|								|
;|	WARNING		Do not move any	variables in the data	|
;|			segment	unless you're absolutely sure	|
;|			that it	does not matter.		|
;|								|
;[]------------------------------------------------------------[]

;	psect	data
_DATA	segment	byte public 'DATA'

etext		label	word

;	Memory management variables
	public	___heapbase, ___brklvl,	___heaptop, ___stktop

___heapbase	dw	ebss
___brklvl	dw	ebss
___heaptop	dw	ebss
___stktop	dw	0

_DATA	ends

;	psect	bss
_BSS	segment	byte public 'BSS'

	public	__argc,	__argv,	_environ, _errno, ___cleanup

edata		label	word
__argc		dw	?
__argv		dw	?
_environ	dw	?
_errno		dw	?
___cleanup	dw	?

_BSS	ends

;	psect	_bssend
_BSSEND	segment	byte public 'BSSEND' ;'STACK'

ebss		label	word

_BSSEND	ends

	end	start
