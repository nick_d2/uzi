/* free.c */
/* Copyright (C) 1984 by Manx Software Systems */

#include "malloc-l.h"

int free(void *area)
	{
	register FREE *tp, *hole;

#ifdef MALLOC_DEBUG
 printf("free(0x%x) starting\n");
 fflush(stdout);
#endif

	hole = (FREE *)area - 1;
	if (hole->f_chain != NULL)
 {
#ifdef MALLOC_DEBUG
 printf("free() returning -1\n");
 fflush(stdout);
#endif
		return -1;
 }
	for (tp = last ; tp > hole || hole > tp->f_chain ; tp = tp->f_chain)
		if (tp >= tp->f_chain && (hole > tp || hole < tp->f_chain))
			break;

	hole->f_chain = tp->f_chain;
	tp->f_chain = hole;
	last = tp;
#ifdef MALLOC_DEBUG
 printf("free() returning 0\n");
 fflush(stdout);
#endif
	return 0;
	}

