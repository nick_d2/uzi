/* getpwnam.c
 */

/* #include <stdio.h> Nick */
#include "passwd.h"

#ifdef L_getpwnam
struct passwd *getpwnam(name)
	char *name;
{
	struct passwd *pwd;

	if (name == NULL) {
		errno = EINVAL;
		return NULL;
	}
	setpwent();
	while ((pwd = getpwent()) != NULL) {
/* printf("getpwent() returned pw_name = \"%s\"\n", pwd->pw_name); */
		if (!strcmp(pwd->pw_name, name))
			break;
	}
	endpwent();
	return pwd;
}
#endif

