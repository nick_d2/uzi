/* malloc-l.h */
/* Copyright (C) 1984 by Manx Software Systems */

/* #define MALLOC_DEBUG */

#ifdef MALLOC_DEBUG
#include <stdio.h>
#endif
#include <types.h> /* Nick, for size_t */
#include <syscalls.h> /* Nick, for sbrk() */

typedef struct freelist {
	size_t f_size;
	struct freelist *f_chain;
} FREE;

#define NULL	(FREE *)0
#define GRAIN 1024

void *realloc(void *area, size_t size);
void *malloc(size_t size);
int free(void *area);

extern FREE __malloc_head, *__malloc_last;

/* trick to get the effect of "static FREE head, *last" over multiple files */
#define head __malloc_head
#define last __malloc_last

