	ifndef	??version
?debug	macro
	endm
	endif
	?debug	S "perror.c"
_TEXT	segment	byte public 'CODE'
DGROUP	group	_DATA,_BSS
	assume	cs:_TEXT,ds:DGROUP,ss:DGROUP
_TEXT	ends
_DATA	segment word public 'DATA'
d@	label	byte
d@w	label	word
_DATA	ends
_BSS	segment word public 'BSS'
b@	label	byte
b@w	label	word
	?debug	C E9C0137E2D08706572726F722E63
	?debug	C E9CA367E2D122E2E5C696E636C7564655C737464696F2E68
	?debug	C E9CA367E2D122E2E5C696E636C7564655C74797065732E68
	?debug	C E9CA367E2D152E2E5C696E636C7564655C66656174757265732E68
	?debug	C E9E8367E2D162E2E5C696E636C7564655C7379732F63646566732E+
	?debug	C 68
	?debug	C E9CA367E2D132E2E5C696E636C7564655C7374646172672E68
	?debug	C E9CA367E2D132E2E5C696E636C7564655C756E697374642E68
	?debug	C E90832812D152E2E5C696E636C7564655C73797363616C6C732E68
	?debug	C E9CA367E2D132E2E5C696E636C7564655C7369676E616C2E68
	?debug	C E9CA367E2D132E2E5C696E636C7564655C737472696E672E68
	?debug	C E9CA367E2D132E2E5C696E636C7564655C7374646465662E68
	?debug	C E9CA367E2D122E2E5C696E636C7564655C6572726E6F2E68
_BSS	ends
_TEXT	segment	byte public 'CODE'
;	?debug	L 8
wr2	proc	near
	push	bp
	mov	bp,sp
	push	si
	push	di
	mov	di,word ptr [bp+4]
;	?debug	L 11
	mov	si,di
	jmp	short @2
@4:
;	?debug	L 14
	inc	si
@2:
;	?debug	L 13
	cmp	byte ptr [si],0
	jne	@4
;	?debug	L 15
	mov	ax,si
	sub	ax,di
	push	ax
	push	di
	mov	ax,2
	push	ax
	call	near ptr _write
	add	sp,6
;	?debug	L 16
	pop	di
	pop	si
	pop	bp
	ret	
wr2	endp
_TEXT	ends
_DATA	segment word public 'DATA'
_DATA	ends
_TEXT	segment	byte public 'CODE'
;	?debug	L 18
_perror	proc	near
	push	bp
	mov	bp,sp
	push	si
	mov	si,word ptr [bp+4]
;	?debug	L 21
	or	si,si
	jne	@6
;	?debug	L 22
	mov	si,offset DGROUP:s@
@6:
;	?debug	L 23
	push	si
	call	near ptr wr2
	pop	cx
;	?debug	L 24
	mov	ax,offset DGROUP:s@+6
	push	ax
	call	near ptr wr2
	pop	cx
;	?debug	L 25
	push	word ptr DGROUP:_errno
	call	near ptr _strerror
	pop	cx
	mov	si,ax
;	?debug	L 26
	push	si
	call	near ptr wr2
	pop	cx
;	?debug	L 27
	mov	ax,offset DGROUP:s@+9
	push	ax
	call	near ptr wr2
	pop	cx
;	?debug	L 28
	pop	si
	pop	bp
	ret	
_perror	endp
_TEXT	ends
	?debug	C E9
_DATA	segment word public 'DATA'
s@	label	byte
	db	101
	db	114
	db	114
	db	111
	db	114
	db	0
	db	58
	db	32
	db	0
	db	10
	db	0
_DATA	ends
	extrn	_errno:word
_TEXT	segment	byte public 'CODE'
	extrn	_strerror:near
	extrn	_write:near
_TEXT	ends
	public	_perror
_wr2	equ	wr2
	end
