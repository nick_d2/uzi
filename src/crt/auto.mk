# auto.mk by Nick for Hytech NOS/UZI project

# -----------------------------------------------------------------------------

SUBDIRS=

# we don't need crt0_$(MODEL).* yet, because main provides crt0_kernel.*
#OUTPUTS=	crt_$(MODEL).$(LIBEXT)
OUTPUTS=	$(LIBOUT)crt0_$(MODEL).$(OBJEXT) crt_$(MODEL).$(LIBEXT)

$(call CANON, $(LIBOUT))crt0_$(MODEL)_$(OBJEXT)_SOURCES= \
		crt0_$(MODEL).S

# this should be phased out by editing the files eg. alloca-l.h
crt_$(MODEL)_$(LIBEXT)_DEFINES= \
		$(DEFINES) MAKE_ALL

# this should be phased out by removing $(NOSINCDIR)/nos from $(INCDIRS),
# see host.mk, normally $(NOSINCDIR)/nos is searched before $(NOSINCDIR),
# but this causes problems because nos/stdio.h is found before stdio.h...
# for now we solve this problem by putting an extra $(NOSINCDIR) in front
crt_$(MODEL)_$(LIBEXT)_INCDIRS= \
		$(NOSINCDIR) $(INCDIRS)

crt_$(MODEL)_$(LIBEXT)_SOURCES= \
		abort.c alloca.c asctime.c assert.c atexit.c atoi.c atol.c \
		bsearch.c calloc.c clock.c closedir.c convtime.c crypt.c \
		ctime.c ctype.c difftime.c error.c etime.c execl.c execle.c \
		execlp.c execlpe.c exect.c execv.c execvp.c execvpe.c exit.c \
		fclose.c fflush.c fgetc.c fgetgren.c fgetpwen.c fgets.c \
		fopen.c fprintf.c fputc.c fputs.c fputter.c fread.c \
		fscanf.c ftell.c fwrite.c getcwd.c getenv.c getgrent.c \
		getgrgid.c getgrnam.c getopt.c getpass.c getpw.c getpwent.c \
		getpwnam.c getpwuid.c gets.c gmtime.c initgrup.c isatty.c \
		itoa.c localtim.c longjmp_$(MODEL).S lsearch.c lstat.c ltoa.c \
		ltostr.c malloc.c memccpy.c memchr.c memcmp.c memcpy.c \
		memmove.c memset.c mkdir.c mktime.c opendir.c perror.c \
		popen.c printf.c putenv.c putgetch.c putpwent.c qsort.c \
		rand.c readdir.c readlink.c regerror.c regexp.c \
		regsub.c rename.c rewind.c rewindir.c rmdir.c scanf.c \
		setbuff.c setenv.c setgrent.c setjmp_$(MODEL).S setpwent.c \
		setvbuff.c sleep.c sprintf.c sputter.c sscanf.c stdio0.c \
		strcat.c strchr.c strcmp.c strcpy.c strcspn.c strdup.c \
		stricmp.c strlen.c strncat.c strncmp.c strncpy.c strnicmp.c \
		strpbrk.c strrchr.c strsep.c strspn.c strstr.c strtok.c \
		strtol.c strtoul.c system.c termcap.c tmpnam.c tmpfile.c \
		tparam.c ttyname.c tzset.c ultoa.c ungetc.c utsname.c \
		vfprintf.c vfscanf.c vprinter.c vprintf.c vscanf.c vsprintf.c \
		vsscanf.c xitoa.c xltoa.c diag.S
# free.c realloc.c strtod.c

malloc_c_MODULES= \
		__allocs __alloct __allocx allock malloc free realloc
# __allocp

# -----------------------------------------------------------------------------

