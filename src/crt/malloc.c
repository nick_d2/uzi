/*	@(#)malloc.c	2.3	(2.11BSD) 1999/1/18 */

#include <stdlib.h>
#include <unistd.h>
/*#include "kernel/dprintf.h"*/

#ifndef DEBUG
#define DEBUG 0
#endif

#if DEBUG >= 4
void amess(char *ptr);
#define ASSERT(expr) if (!(expr)) { amess("expr"); while (1); }
#define ALLOCK() allock()
#else
#define ASSERT(expr)
#define ALLOCK()
#endif

#if 0 /* Nick, for kernel "memory" command */
extern unsigned long Memfail;		/* Count of allocation failures */
extern unsigned long Allocs;		/* Total allocations */
extern unsigned long Frees;		/* Total frees */
extern unsigned long Invalid;		/* Total calls to free with bad arg */
extern unsigned char Memwait;		/* Count of tasks waiting for memory */
extern unsigned int Freebytes;		/* Unallocated heap, bytes */
extern unsigned int Freefrags;		/* Unallocated heap, fragments */
extern unsigned int Usedbytes;		/* Allocated heap, bytes */
extern unsigned int Usedfrags;		/* Allocated heap, fragments */
extern unsigned int Morecores;		/* Successful calls to sbrk() */
#endif

/*
 * The origins of the following ifdef are lost.  The only comment attached
 * to it, "avoid break bug", probably has something to do with a bug in
 * an older PDP-11 kernel.  Maybe it's still a bug in the current kernel.
 * We'll probably never know ...
 */
#if 0 /*def pdp11*/
#define GRANULE 64
#else
#define GRANULE 0
#endif

/*
 * C storage allocator
 *
 * Uses circular first-fit strategy.  Works with a non-contiguous, but
 * monotonically linked, arena.  Each block is preceded by a pointer to the
 * pointer of the next following block.  Blocks are an exact number of words
 * long aligned to the data type requirements of ALIGN.  For Z80, 2 bytes.
 *
 * Bit 0 (LSB) of pointers is used to indicate whether the block associated
 * with the pointer is in use.  A 1 indicates a busy block and a 0 a free
 * block (obviously pointers can't point at odd addresses).  Gaps in arena
 * are noted as busy blocks.  Idle blocks are coalesced during space search.
 *
 * The last block of the arena, pointed to by __alloct, is empty and has
 * a busy pointer to first.  This means __alloct->ptr == setbusy(__allocs).
 * This circular linking helps to advance the rover pointer during search.
 * The rover pointer is left pointing just above the last block allocated.
 *
 * Every time we free a block, the rover pointer is left pointing to it.
 * So, if the blocks are freed in LIFO order, the rover pointer retreats in
 * the same way it advanced as the blocks were allocated.  However, the
 * strategy may result in unnecessary fragmentation in the non-LIFO case.
 *
 * The __allocx variable is a hack which allows the realloc() call to be
 * implemented as a call to free() followed by a call to malloc().  User
 * data in the block is left almost untouched in the process, except maybe
 * 1 word just after the new block.  This value is preserved in __allocx.
 *
 * Each call to malloc(0) returns a unique non-NULL pointer.  No storage
 * is available to the user, but the usual linked list overhead (1 word)
 * applies. Therefore a call to malloc(0) may fail and return NULL.  When
 * malloc(0) is successful, the user needs to free() it in the normal way.
 *
 * Calls to free(NULL) are accepted and ignored.  Calls to realloc(NULL, n)
 * behave exactly the same as a call to malloc(n).  Unfortunately, calls to
 * realloc(p, 0) don't have the same effect as free(p).  This would return
 * a unique (maybe unchanged) pointer each time, which the user must free().
 *
 * Bug:  When realloc() fails, the user's storage has been freed.  Check!!
 *
 * Bug:  Doesn't handle gracefully the situation where sbrk() fails.  If we
 * need to call sbrk(), we assume the sbrk() call must provide the user's
 * entire request.  This assumption is made because we can't be sure that
 * sbrk(0) will return the same value as last time (non-continguous arena).
 *
 * Different implementations may need to redefine ALIGN, NALIGN, BLOCK,
 * BUSY, INT where INT is integer type to which a pointer can be cast.
 */
#define	INT		int
#define	ALIGN		int
#define	NALIGN		1
#define	WORD		sizeof(union store)
#define	BLOCK		4096 /*1024*/	/* a multiple of WORD */

#define	BUSY		1

#define	testbusy(p)	((INT)(p)&BUSY)
#define	setbusy(p)	(union store *)((INT)(p)|BUSY)
#define	clearbusy(p)	(union store *)((INT)(p)&~BUSY)

union store {
	union store	*ptr;
	ALIGN		dummy[NALIGN];
	int		calloc;	/* calloc clears an array of integers */
};

#ifdef MODULE
#define STATIC
extern union store *__allocs;	/* initial arena */
/*extern union store *__allocp;*/	/* search ptr */
extern union store *__alloct;	/* arena top */
extern union store *__allocx;	/* for benefit of realloc */
#else
#define STATIC static
#define MODULE___allocs
/*#define MODULE___allocp*/
#define MODULE___alloct
#define MODULE___allocx
#define MODULE_allock
#define MODULE_malloc
#define MODULE_free
#define MODULE_realloc
#endif

#ifdef MODULE___allocs
STATIC union store *__allocs;	/* initial arena */
#endif
#if 0 /*def MODULE___allocp*/
STATIC union store *__allocp;	/* search ptr */
#endif
#ifdef MODULE___alloct
STATIC union store *__alloct;	/* arena top */
#endif
#ifdef MODULE___allocx
STATIC union store *__allocx;	/* for benefit of realloc */
#endif

STATIC void allock(void);

#ifdef MODULE_allock
STATIC void
allock(void)
{
	union store *p;
#ifdef CIRCULAR
	unsigned char x;
#endif
#if 0 /* Nick, for kernel "memory" command */
	unsigned int freebytes, freefrags;
	unsigned int usedbytes, usedfrags;
#endif

#ifdef CIRCULAR
	x = 0;
#endif
#if 0 /* Nick, for kernel "memory" command */
	freebytes = 0;
	freefrags = 0;
	usedbytes = 0;
	usedfrags = 0;
#endif
	for (p = __allocs; clearbusy(p->ptr) > p; p = clearbusy(p->ptr)) {
#ifdef CIRCULAR
		if (p == __allocp)
			x++;
#endif
#if 0 /* Nick, for kernel "memory" command */
		if (testbusy(p->ptr))
			{
			usedbytes += (char *)clearbusy(p->ptr) - (char *)p;
			usedfrags++;
			}
		else
			{
			freebytes += (char *)p->ptr - (char *)p;
			freefrags++;
			}
#endif
	}
#ifdef CIRCULAR
	if (p == __allocp)
		x++;
	ASSERT(x == 1);
#endif
	ASSERT(p == __alloct);
#if 0 /* Nick, for kernel "memory" command */
	ASSERT(Freebytes == freebytes && Freefrags == freefrags);
	ASSERT(Usedbytes == usedbytes && Usedfrags == usedfrags);
/* dprintf(0, "Allocs %lu Frees %lu (Allocs - Frees) %lu Usedfrags %u\n", */
/*   Allocs, Frees, Allocs - Frees, Usedfrags); */
	ASSERT((Allocs - Frees) == Usedfrags);
#endif
}
#endif

#ifdef MODULE_malloc
void *
malloc(size_t nbytes)
{
	union store *p, *q;
	unsigned int nw, temp;
#ifdef CIRCULAR
	unsigned char x;
#else
	union store *__allocp;
#endif
#if 0 /* Nick, for kernel "memory" command */
	unsigned long bytes;
#endif

/* unsigned char flag = 0; */

#if DEBUG >= 4
 /* can't be po'd since _dprintf() re-enters malloc() */
 dprintf(4, "\n%02x:%04x malloc(0x%04x) = ",
   ((unsigned char *)&nbytes)[8], ((unsigned int *)&nbytes)[5], nbytes);
#endif
	if (__allocs == NULL) {	/* first time */
		while (testbusy(sbrk(0))) {	/* arena start is not even */
			sbrk(1);		/* waste 1 byte to make even */
		}
		__allocs = (union store *)sbrk(2 * sizeof(union store));
#ifdef CIRCULAR
		__allocp = __allocs;
#endif
		__alloct = __allocs + 1;
		__allocs->ptr = setbusy(__alloct);
		__alloct->ptr = setbusy(__allocs);
#if 0 /* Nick, for kernel "memory" command */
		Allocs = 1;
		Usedbytes = sizeof(union store);
		Usedfrags = 1;
#endif
	}
	/* note: this may fail if nbytes is very large eg. 0xffff */
	nw = (nbytes+2*WORD-1)/WORD;
#ifdef CIRCULAR
	ASSERT(__allocp >= __allocs && __allocp <= __alloct);
#endif
	ALLOCK();
#ifdef CIRCULAR
	p = __allocp;
#else
	p = __allocs;
#endif
	for (;;) {
/* abyte(';'); */
#ifdef CIRCULAR
		x = 0;
#endif
		for (;;) {
/* abyte(':'); */
			if (!testbusy(p->ptr)) {
				while(!testbusy((q = p->ptr)->ptr)) {
					ASSERT(q > p && q < __alloct);
					p->ptr = q->ptr;
#ifdef CIRCULAR
 if (__allocp == q)
  {
/*  flag = 1; */
  __allocp = p;
  }
#endif
#if 0 /* Nick, for kernel "memory" command */
					Freefrags--;
#endif
				}
				if (q >= p+nw && p+nw >= p)
					goto found;
			}
			q = p;
			p = clearbusy(p->ptr);
			if (p > q) {
				ASSERT(p <= __alloct);
			} else if (q != __alloct || p != __allocs) {
				ASSERT(q == __alloct && p == __allocs);
#if DEBUG >= 4
 dprintf(4, "NULL a ");
#endif
#if 0 /* Nick, for kernel "memory" command */
				Memfail++;
#endif
				return NULL;
			} else
#ifdef CIRCULAR
				if (++x > 1)
#endif
					break;
		}
		while (testbusy(q = (union store *)sbrk(0))) {
			sbrk(1);	/* waste 1 byte to make even */
		}
#if 1
		/* allocate at least 1 word, ending on a block boundary */
		temp = (BLOCK - ((unsigned int)q & (BLOCK - 1))) / WORD;
#else
		/*
		 * Line up on page boundary so we can get the last drip at
		 * the end ...
		 */
		temp = ((((unsigned)q + WORD*nw + BLOCK-1)/BLOCK)*BLOCK
			- (unsigned)q) / WORD;
#endif
		if (q+temp+GRANULE < q) {
#if DEBUG >= 4
 dprintf(4, "NULL b ");
#endif
#if 0 /* Nick, for kernel "memory" command */
			Memfail++;
#endif
/* if (flag) */
/*  { */
/*  abyte('%'); while (1); */
/*  } */
			return NULL;
		}
		q = (union store *)sbrk(temp*WORD);
		if ((INT)q == -1) {
#if DEBUG >= 4
 dprintf(4, "NULL c ");
#endif
#if 0 /* Nick, for kernel "memory" command */
			Memfail++;
#endif
			return NULL;
		}
#if 0 /* Nick, for kernel "memory" command */
		Morecores++;
		Freebytes += temp*WORD;
		Freefrags += 2;
		if (Memwait)
			{
			ksignal(&Memwait, 0);
			}
#endif
		ASSERT(q > __alloct);
		__alloct->ptr = q;
		if (q != __alloct+1)
			__alloct->ptr = setbusy(__alloct->ptr);
		__alloct = q->ptr = q+temp-1;
		__alloct->ptr = setbusy(__allocs);
	}
found:
	__allocp = p + nw;
	ASSERT(__allocp <= __alloct);
	if (q > __allocp) {
		__allocx = __allocp->ptr;
		__allocp->ptr = p->ptr;
#if 0 /* Nick, for kernel "memory" command */
		Freefrags++;
#endif
	}
	p->ptr = setbusy(__allocp);
#if 0 /* Nick, for kernel "memory" command */
	bytes = (char *)__allocp - (char *)p;
	Freebytes -= bytes;
	Usedbytes += bytes;
	Freefrags--;
	Usedfrags++;
	Allocs++;
#endif
#if DEBUG >= 4
 dprintf(4, "0x%04x ", p + 1);
#endif
	return p + 1;
}
#endif

/*
 * Freeing strategy tuned for LIFO allocation.
 */
#ifdef MODULE_free
void
free(void *ap)
{
	union store *p;
#if 1
	unsigned long bytes;
#endif

#if DEBUG >= 4
 /* can't be po'd since _dprintf() re-enters malloc() */
 dprintf(4, "\n%02x:%04x free(0x%04x) ",
   ((unsigned char *)&ap)[8], ((unsigned int *)&ap)[5], ap);
#endif
	if (ap == NULL) { 	/* may be first time */
		return;
	}
	p = (union store *)ap;
#if 0 /* Nick, for kernel "memory" command */
	if (__allocs == NULL || p <= clearbusy(__allocs[1].ptr) ||
			p > __alloct || testbusy((--p)->ptr) == 0)
		{
		Invalid++;
		/* can't be po'd since _dprintf() would re-enter malloc() */
		dprintf(0, "\n%02x:%04x free(0x%04x) ",
				((unsigned char *)&ap)[8],
				((unsigned int *)&ap)[5], ap);
		return;
		}
	ALLOCK();
#ifdef CIRCULAR
	__allocp = p;
#endif
#else
	ASSERT(__allocs && p > clearbusy(__allocs[1].ptr) && p <= __alloct);
	ALLOCK();
#ifdef CIRCULAR
	__allocp = --p;
#else
	--p;
#endif
	ASSERT(testbusy(p->ptr));
#endif
	p->ptr = clearbusy(p->ptr);
	ASSERT(p->ptr > p && p->ptr <= __alloct);
#if 0 /* Nick, for kernel "memory" command */
	bytes = (char *)p->ptr - (char *)p;
	Freebytes += bytes;
	Usedbytes -= bytes;
	Freefrags++;
	Usedfrags--;
	Frees++;
	if (Memwait)
		{
		ksignal(&Memwait, 0);
		}
#endif
}
#endif

/*
 * Realloc(p, nbytes) reallocates a block obtained from malloc() and freed
 * since last call of malloc() to have new size nbytes, and old content
 * returns new location, or 0 on failure.
 */
#ifdef MODULE_realloc
void *
realloc(void *ap, size_t nbytes)
{
	union store *p, *q;
#if 0
	union store *s, *t;
#endif
	unsigned int nw, onw;

#if DEBUG >= 4
 /* can't be po'd since _dprintf() re-enters malloc() */
 dprintf(4, "\n%02x:%04x realloc(0x%04x, 0x%04x) ",
   ((unsigned char *)&ap)[8], ((unsigned int *)&ap)[5], ap, nbytes);
#endif
	if (ap == NULL) {	/* may be first time */
		return malloc(nbytes);
	}
	p = (union store *)ap;
	/*if (testbusy(p[-1].ptr))*/
		free(p);
	onw = p[-1].ptr - p;
	q = malloc(nbytes);
	if (q == NULL || q == p)
		return q; /* note: in failure case, we freed p!! */
#if 0
	s = p;
	t = q;
#endif
	nw = (nbytes+WORD-1)/WORD;
	if (nw < onw)
		onw = nw;
#if 1
	memcpy(q, p, onw * sizeof(union store));
#else
	while (onw-- != 0)
		*t++ = *s++;
#endif
	if (q < p && q+nw >= p)
		(q+(q+nw-p))->ptr = __allocx;
	return q;
}
#endif

