; longjmp_banked.S
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

	module	_longjmp_banked

 .if 0 ; virtual memory
	extrn	leave_entry
 .else
	extrn	?BANK_FAST_LEAVE_L08
 .endif

#include "z80/asm.h"

; -----------------------------------------------------------------------------

	rseg	CODE

	public	_longjmp
_longjmp:
 .if 1
	ld	a,b
	or	c
	jr	nz,ok_return
	inc	bc
ok_return:
	ex	de,hl
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	inc	hl
	ex	de,hl
 ; need to disable interrupts here for proper bank switching
	ld	sp,hl
	ex	de,hl
 .if 0 ; virtual memory
	ld	e,(hl)
	inc	hl
	ld	d,(hl)			; de = BBR_REFERENCE at time of setjmp
	inc	hl
	ex	de,hl
	ld	a,(hl)
 ; note: this won't work if the BBR value is actually being changed
	out0	(BBR),a
	inc	hl
	ld	a,(hl)
 ; note: this won't work if the CBAR value is actually being changed
	out0	(CBAR),a
	ex	de,hl
 .endif
	push	bc			; bc = integer value passed to longjmp
	ld	c,(hl)
	inc	hl
	ld	b,(hl)			; bc = bc value at time of setjmp
	inc	hl
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	inc	hl
	push	de
	pop	ix
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	inc	hl
	push	de
	pop	iy
	ld	e,(hl)
	inc	hl
	ld	d,(hl)			; de = return addr from call to setjmp
	inc	hl
 .if 0 ; virtual memory
	ex	de,hl			; hl = return addr from call to setjmp
	ex	(sp),hl			; hl = integer value passed to longjmp
	push	hl			; stack them both in correct order
	ex	de,hl
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	ex	de,hl			; hl = CBR_REFERENCE at time of setjmp
	jp	leave_entry		; ?BANK_FAST_LEAVE_L08 ex (sp),hl skip
 .else
	ld	a,(hl)
	pop	hl
	push	de
	push	af
	jp	?BANK_FAST_LEAVE_L08
 .endif
 .else
	defb	078h,0B1h,020h,001h,003h,0EBh,05Eh
	defb	023h,056h,023h,0EBh,0F9h,0EBh,0C5h
	defb	04Eh,023h,046h,023h,05Eh,023h,056h
	defb	023h,0D5h,0DDh,0E1h,05Eh,023h,056h
	defb	023h,0D5h,0FDh,0E1h,05Eh,023h,056h
	defb	023h,07Eh,0E1h,0D5h,0F5h,0C3h
	defw	LWRD ?BANK_FAST_LEAVE_L08
 .endif

	endmod

; -----------------------------------------------------------------------------

	end
