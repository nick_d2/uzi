/* realloc.c */
/* Copyright (C) 1984 by Manx Software Systems */

#include "malloc-l.h"

void *realloc(void *area, size_t size)
	{
	register void *cp;
	size_t osize;

#ifdef MALLOC_DEBUG
 amess("realloc(0x");
 ahexw((unsigned int)area);
 amess(", 0x");
 ahexw(size);
 amess(") starting");
 acrlf();
 /* printf("realloc(0x%x, 0x%x) starting\n", area, size); */
 /* fflush(stdout); */
#endif

	osize = (((FREE *)area-1)->f_size - 1) * sizeof(FREE);
	free(area);
	if ((cp = malloc(size)) != 0 && cp != area)
#if 1 /* Nick */
		memcpy(cp, area, size>osize ? osize : size);
#else
		movmem(area, cp, size>osize ? osize : size);
#endif
#ifdef MALLOC_DEBUG
 amess("realloc() returning 0x");
 ahexw((unsigned int)cp);
 acrlf();
 /* printf("realloc() returning 0x%x\n", cp); */
 /* fflush(stdout); */
#endif
	return cp;
	}

