/* fputter.c
 * Nick Downing, Hytech
 *
 *    Dale Schumacher			      399 Beacon Ave.
 *    (alias: Dalnefre')		      St. Paul, MN  55104
 *    dal@syntel.UUCP			      United States of America
 *
 * Altered to use stdarg, made the core function vprinter (Nick).
 * Hooked into the stdio package using 'inside information'
 * Altered sizeof() assumptions, now assumes all integers except chars
 * will be either
 *  sizeof(xxx) == sizeof(long) or sizeof(xxx) == sizeof(short)
 *
 * -RDB
 */

#include "printf.h"

#ifdef L_fputter
unsigned int __fputter(buffer, count, op)
	void *buffer;
	unsigned int count;
	FILE *op;
{
	unsigned int cnt;
	unsigned char ch;
	int buffer_mode;

	/* This speeds things up a bit for unbuffered */
	buffer_mode = (op->mode & __MODE_BUF);
	op->mode &= (~__MODE_BUF);

	for (cnt = 0; cnt < count; cnt++)
		{
		ch = *(unsigned char *)buffer;
		putc(ch, op); /* normal char out */
		if (ch == '\n' && buffer_mode == _IOLBF)
			fflush(op);
		((unsigned char *)buffer)++;
		}

	op->mode |= buffer_mode;
	if (buffer_mode == _IONBF)
		fflush(op);
	if (buffer_mode == _IOLBF)
		op->bufwrite = op->bufstart;

	return cnt;
}
#endif

