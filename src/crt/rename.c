/* rename.c
 */
#include <stdio.h>
#include <unistd.h>

int rename(oldname, newname)
	const char *oldname; /* Nick added const */
	const char *newname; /* Nick added const */
{
	int error = link(oldname, newname);

	if (error)
		return error;
	return unlink(oldname);
}
