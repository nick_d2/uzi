; /*			-  64180.inc -
;
;	This files defines the internal register addresses
;	for HD64180
;
;	File version:   $Revision: 1.1 $
;
; */
;
; /* ================================== */
; /* 					*/
; /*	ASCI Channel Control Registers 	*/
; /* 					*/
; /* ================================== */

CNTLA0	= 0x00;		/* ASCI Ctrl Reg A, Ch 0 */
CNTLA1	= 0x01;		/* ASCI Ctrl Reg A, Ch 1 */
CNTLB0	= 0x02;		/* ASCI Ctrl Reg B, Ch 0 */
CNTLB1	= 0x03;		/* ASCI Ctrl Reg B, Ch 1 */
; -----------------------------------------------------------------------------
STAT0	= 0x04;		/* ASCI Status Reg, Ch 0 */
STAT1	= 0x05;		/* ASCI Status Reg, Ch 1 */
; -----------------------------------------------------------------------------
TDR0	= 0x06;		/* ASCI Transmit Data Reg, Ch 0 */
TDR1	= 0x07;		/* ASCI Transmit Data Reg, Ch 1 */
RDR0	= 0x08;		/* ASCI Receive Data Reg, Ch 0 */
RDR1	= 0x09;		/* ASCI Receive Data Reg, Ch 1 */

; /* ================================== */
; /* 					*/
; /*	CSI/O Registers 		*/
; /* 					*/
; /* ================================== */

CNTR	= 0x0A;		/* CSI/O Ctrl Reg */
TRDR	= 0x0B;		/* CSI/O Trans/Rec Data Reg */

; /* ================================== */
; /* 					*/
; /*	Timer Registers 		*/
; /* 					*/
; /* ================================== */

TMDR0L	= 0x0C;		/* Timer 0 Data Reg L */
TMDR0H	= 0x0D;		/* Timer 0 Data Reg H */
TMDR1L	= 0x14;		/* Timer 1 Data Reg L */
TMDR1H	= 0x15;		/* Timer 1 Data Reg H */
; -----------------------------------------------------------------------------
RLDR0L	= 0x0E;		/* Timer 0 Reload Reg L */
RLDR0H	= 0x0F;		/* Timer 0 Reload Reg H */
RLDR1L	= 0x16;		/* Timer 1 Reload Reg L */
RLDR1H	= 0x17;		/* Timer 1 Reload Reg H */
; -----------------------------------------------------------------------------
TCR	= 0x10;		/* Timer Ctrl Reg */

; /* ================================== */
; /* 					*/
; /*	Free Running Counter 		*/
; /* 					*/
; /* ================================== */

FRC	= 0x18;		/* Free Running Counter */

; /* ================================== */
; /* 					*/
; /*	DMA Registers 			*/
; /* 					*/
; /* ================================== */

SAR0L	= 0x20;		/* DMA 0 Source Addr Reg */
SAR0H	= 0x21;		/* DMA 0 Source Addr Reg */
SAR0B	= 0x22;		/* DMA 0 Source Addr Reg */
; -----------------------------------------------------------------------------
DAR0L	= 0x23;		/* DMA 0 Destination Addr Reg */
DAR0H	= 0x24;		/* DMA 0 Destination Addr Reg */
DAR0B	= 0x25;		/* DMA 0 Destination Addr Reg */
; -----------------------------------------------------------------------------
BCR0L	= 0x26;		/* DMA 0 Counter Reg */
BCR0H	= 0x27;		/* DMA 0 Counter Reg */
; -----------------------------------------------------------------------------
MAR1L	= 0x28;		/* DMA 1 Memory Addr Reg */
MAR1H	= 0x29;		/* DMA 1 Memory Addr Reg */
MAR1B	= 0x2A;		/* DMA 1 Memory Addr Reg */
; -----------------------------------------------------------------------------
IAR1L	= 0x2B;		/* DMA I/O Addr Reg */
IAR1H	= 0x2C;		/* DMA I/O Addr Reg */
; -----------------------------------------------------------------------------
BCR1L	= 0x2E;		/* DMA 1 Byte Count Reg */
BCR1H	= 0x2F;		/* DMA 1 Byte Count Reg */
; -----------------------------------------------------------------------------
DSTAT	= 0x30;		/* DMA Status Reg */
DMODE	= 0x31;		/* DMA Mode Reg */
DCNTL	= 0x32;		/* DMA/WAIT Ctrl Reg */

; /* ================================== */
; /* 					*/
; /*	MMU Registers 			*/
; /* 					*/
; /* ================================== */

CBR     = 0x38;		/* MMU Common Base Reg */
BBR     = 0x39;		/* MMU Bank Base Reg */
CBAR    = 0x3A;		/* MMU Bank/Common Area Reg */
; CBR_location     = 0x38;   /* MMU Common Base Reg */
; BBR_location     = 0x39;   /* MMU Bank Base Reg */
; CBAR_location    = 0x3A;   /* MMU Bank/Common Area Reg */
; #define CBR  CBR_location
; #define BBR  BBR_location
; #define CBAR  CBAR_location

; /* ================================== */
; /* 					*/
; /*	Interrupt Registers 		*/
; /* 					*/
; /* ================================== */

IL	= 0x33;		/* Int Vect Low Reg */
ITC	= 0x34;		/* Int/Trap Ctrl Reg */

; /* ================================== */
; /* 					*/
; /*	Refresh Registers 		*/
; /* 					*/
; /* ================================== */

RCR	= 0x36;		/* Refresh Ctrl Reg */

; /* ================================== */
; /* 					*/
; /*	I/O Registers 			*/
; /* 					*/
; /* ================================== */

ICR	= 0x3F;		/* I/O Ctrl Reg */

; /* ================================== */
; /* 					*/
; /*	Bit Definitions			*/
; /* 					*/
; /* ================================== */

INT0SW	= 0x01;		/* interrupt enable bits in ITC */
INT1SW	= 0x02;		/* interrupt enable bits in ITC */
INT2SW	= 0x04;		/* interrupt enable bits in ITC */

; -----------------------------------------------------------------------------
; Hytech specific hardware:  (For the WPO30-V5, Hytech 1000, Hytech 1500)

BC8530	= 0x214;	/* channel B control in 8530 */
AC8530	= 0x215;	/* channel A control in 8530 */
BD8530	= 0x216;	/* channel B data in 8530 */
AD8530	= 0x217;	/* channel A data in 8530 */

; -----------------------------------------------------------------------------

