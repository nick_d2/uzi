/* string.c
 * Copyright (C) 1995,1996 Robert de Bath <rdebath@cix.compulink.co.uk>
 * This file is part of the Linux-8086 C library and is distributed
 * under the GNU Library General Public License.
 */

#include "string-l.h"

/********************** Function strcmp ************************************/
#ifdef L_strcmp
int strcmp(d, s)
	const char *d; /* Nick */
	const char *s; /* Nick */
{
	register const char *s1 = d; /* Nick */
	register const char *s2 = s; /* Nick */
	register char c1, c2;

	while ((c1 = *s1++) == (c2 = *s2++) && c1)
		;
	return c1 - c2;
}
#endif
