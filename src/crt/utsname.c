/*
 * utsname.c for UZIX
 * by A&L Software 1999
 *
 */

#include <utsname.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#if 1 /* Nick for GI_KDAT etc */
#include <sys/ioctl.h>
#else
#ifdef __TURBOC__
#include <..\kernel\unix.h>
/* Nick #endif */
#else /* Nick #ifdef HI_TECH_C */
#include <unix.h>
#endif
#endif

int uname (__utsbuf)
	struct utsname *__utsbuf;
{
	struct s_kdata kdata;
	int i;

	getfsys(GI_KDAT, &kdata);
	strcpy(__utsbuf->sysname,kdata.k_name);
#if 1 /* Nick */
	strcpy(__utsbuf->nodename,kdata.k_host);
#else
	strcpy(__utsbuf->nodename,kdata.k_name);
	for (i=0;i<strlen(__utsbuf->nodename);i++)
		__utsbuf->nodename[i]=tolower(__utsbuf->nodename[i]);
#endif
	strcpy(__utsbuf->release,kdata.k_release);
	strcpy(__utsbuf->version,kdata.k_version);
	strcpy(__utsbuf->machine,kdata.k_machine);
	strcpy(__utsbuf->domainname,"(localhost)");
	return 0;
}
