/* tmpfile.c by Nick for NOS/UZI project */

#include <stdio.h>
#include <unistd.h>

FILE *
tmpfile __P((void))
	{
	char fn[L_tmpnam];
	FILE *fp;

	if (tmpnam(fn) == NULL)
		{
		return NULL;
		}

	fp = fopen(fn, "w+");
	if (fp == NULL)
		{
		return NULL;
		}

	unlink(fn); /* doesn't take effect until after closure */
	return fp;
	}

