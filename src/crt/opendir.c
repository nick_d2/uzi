/* opendir.c	opendir implementation
 *
 */
#include <unistd.h>
#include <alloc.h>
#include <dirent.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
/*RPB*/
/*#include <signal.h>*/
/*#include <z80/asm.h>*/
/*#include <uzi/devio.h>*/
/*#include <uzi/rtc.h>*/
/*#include <uzi/filesys.h>*/
/*#include <uzi/process.h>*/
/*RPB*/

DIR *opendir(path)
	char *path;
{
	struct stat statbuf;
	register DIR *dir;
/*RPB*/
/*abyte('Q');*/
/*RPB*/
	if (stat(path, &statbuf) != 0)
/*RPB*/
{
/*abyte('W');*/
/*RPB*/
		goto Err;
/*RPB*/
}
/*RPB*/
/*RPB*/
/*abyte('E');*/
/*RPB*/
	if ((statbuf.st_mode & S_IFDIR) == 0) {
/*RPB*/
/*abyte('R');*/
/*RPB*/
		errno = ENOTDIR;
		goto Err;
	}
/*RPB*/
/*abyte('T');*/
/*RPB*/
	if ((dir = (DIR *)calloc(1,sizeof(DIR))) == NULL) {
/*RPB*/
/*abyte('Y');*/
/*RPB*/
		errno = ENOMEM;
		goto Err;
	}
/*RPB*/
/*abyte('U');*/
/*RPB*/
	if ((dir->dd_buf = calloc(1,sizeof(struct dirent))) == NULL) {
/*RPB*/
/*abyte('I');*/
/*RPB*/
		free(dir);
		errno = ENOMEM;
		goto Err;
	}
	if ((dir->dd_fd = open(path, O_BINARY)) < 0) {
/*RPB*/
/*abyte('O');*/
/*ahexw(errno);*/
/*RPB*/
		free(dir->dd_buf);
		free(dir);
Err:		return NULL;
	}
/*RPB*/
/*abyte('P');*/
/*RPB*/
	return dir;
}

