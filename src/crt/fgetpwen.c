/* fgetpwen.c
 */

#include "passwd.h"
/*RPB*/
/*#include <signal.h>*/
/*#include <z80/asm.h>*/
/*#include <uzi/devio.h>*/
/*#include <uzi/rtc.h>*/
/*#include <uzi/filesys.h>*/
/*#include <uzi/process.h>*/
/*RPB*/
 
#ifdef L_fgetpwen
struct passwd *fgetpwent(file)
	FILE *file;
{
	if (file == NULL) {
		errno = EINTR;
		return NULL;
	}
	return __getpwent(fileno(file));
}
#endif

