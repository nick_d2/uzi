/* perror.c
 */
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
/*RPB*/
/*#include <signal.h>*/
/*#include <z80/asm.h>*/
/*#include <uzi/devio.h>*/
/*#include <uzi/rtc.h>*/
/*#include <uzi/filesys.h>*/
/*#include <uzi/process.h>*/
/*RPB*/

static void wr2(str)
	char *str;
{
	char *p = str;
	
	while (*p)
		++p;
	write(2, str, (unsigned int)(p-str));
}

void perror(str)
	register const char *str; /* Nick added const */
{
	if (!str)
		str = "error";
	wr2(str);
	wr2(": ");
	str = strerror(errno);
	wr2(str);
	wr2("\n");
}
