/* free.c */
/* Copyright (C) 1984 by Manx Software Systems */

#include "malloc-l.h"

void free(void *area)
	{
	register FREE *tp, *hole;

#ifdef MALLOC_DEBUG
 amess("free(0x");
 ahexw((unsigned int)area);
 amess(") starting");
 acrlf();
 /* printf("free(0x%x) starting\n"); */
 /* fflush(stdout); */
#endif

	hole = (FREE *)area - 1;
	if (hole->f_chain != NULL)
 {
#ifdef MALLOC_DEBUG
 amess("free() returning error");
 acrlf();
 /* printf("free() returning error\n"); */
 /* fflush(stdout); */
#endif
		return; /* Nick -1; */
 }
	for (tp = last ; tp > hole || hole > tp->f_chain ; tp = tp->f_chain)
		if (tp >= tp->f_chain && (hole > tp || hole < tp->f_chain))
			break;

	hole->f_chain = tp->f_chain;
	tp->f_chain = hole;
	last = tp;
#ifdef MALLOC_DEBUG
 amess("free() returning ok");
 acrlf();
 /* printf("free() returning ok\n"); */
 /* fflush(stdout); */
#endif
	return; /* Nick 0; */
	}

