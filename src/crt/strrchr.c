/* string.c
 * Copyright (C) 1995,1996 Robert de Bath <rdebath@cix.compulink.co.uk>
 * This file is part of the Linux-8086 C library and is distributed
 * under the GNU Library General Public License.
 */

#include "string-l.h"

/********************** Function strrchr ************************************/
#ifdef L_strrchr
char *strrchr(s, c)
	const char *s; /* Nick */
	int c;
{
	register const char *p = s + strlen(s); /* Nick */

	/* For null it's just like strlen */
	if (c == '\0')
		return (char *)p; /* Nick */
	while (p != s) {
		if (*--p == c)
			return (char *)p; /* Nick */
	}
	return NULL;
}
#endif
