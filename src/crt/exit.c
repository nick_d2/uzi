/* exit.c for UZI180 by Nick */

#include <stdlib.h> /* for exit() prototype */
#include <syscalls.h> /* for _exit() prototype */

void exit(int val)
	{
	if (__cleanup != NULL)
		{
		(*__cleanup)(val, NULL);
		}
	_exit(val);
	}

