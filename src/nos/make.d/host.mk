# /uzi/src/nos/make.d/host.mk by Nick for Hytech NOS/UZI project

# Enter with TOPSRCDIR = relative path to current directory, which will be
# blank if the project is at the same level as this host.mk file, for example
# /uzi/src/nos/ppp has an "include ../make.d/host.mk" with TOPSRCDIR = blank

# -----------------------------------------------------------------------------

TOPSRCDIR:=	$(TOPSRCDIR)../
TOPNOSDIR:=	$(TOPSRCDIR)

include $(TOPNOSDIR)../make.d/host.mk

# -----------------------------------------------------------------------------

# now want kernel.bin placed directly under /uzi/bin
#NOSBINDIR:=	$(TOPNOSDIR)bin/
NOSINCDIR:=	$(TOPNOSDIR)include/
NOSLIBDIR:=	$(TOPNOSDIR)lib/

DEFINES=	__STDC__ DEBUG=3 UNIX ZILOG
INCDIRS=	$(NOSINCDIR)$(ARCH)/ $(NOSINCDIR)
LIBDIRS=	$(NOSLIBDIR)
# removed stdlibs as crt must be specified explicitly as a dependency to main
#STDLIBS=	crt_$(MODEL).$(LIBEXT)
STDLIBS=

# now want kernel.bin placed directly under /uzi/bin
#BINOUT=	$(NOSBINDIR)
HDROUT=		$(NOSINCDIR)
LIBOUT=		$(NOSLIBDIR)

# -----------------------------------------------------------------------------

