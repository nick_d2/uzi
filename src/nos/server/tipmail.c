/* "Dumb terminal" mailbox interface
 * Copyright 1991 Phil Karn, KA9Q
 *
 *	May '91	Bill Simpson
 *		move to separate file for compilation & linking
 *	Sep '91 Bill Simpson
 *		minor changes for DTR & RLSD
 */
#include <errno.h> /* Nick */
#include "nos/global.h"
#include "main/mbuf.h"
#include "main/timer.h"
#include "kernel/thread.h"
#include "main/iface.h"
#include "z80/asci.h"
#include "socket/socket.h"
#include "socket/usock.h"
#include "client/telnet.h"
#include "server/mailbox.h"
#include "server/tipmail.h"
#include "main/devparam.h"
#include "main/main.h"
#include <libintl.h>
#include "po/messages.h"

static struct tipcb {
	struct tipcb *next;
	struct thread_s *proc;
	struct thread_s *in;
	struct iface_s *iface;
	int (*rawsave)(struct iface_s *,struct mbuf_s **);
	FILE *network;
	int echo;
	struct timer timer;
} *Tiplist;
#define	NULLTIP	(struct tipcb *)0

static void tip_in(int dev,void *n1,void *n2);
static void tipidle(void *t);

unsigned Tiptimeout = 180;	/* Default tip inactivity timeout (seconds) */

/* Input process */
static void
tip_in(dev,n1,n2)
int dev;
void *n1,*n2;
{
	struct tipcb *tip;
	struct mbuf_s *bp;
	char *buf[2], line[MBXLINE];
	int c, ret, pos = 0;

	tip = (struct tipcb *) n1;
	while((c = get_asci(dev)) != -1){
		Asci[dev].iface->lastrecv = secclock();
		ret = 0;
		if(tip->echo == WONT){
			switch(c){
			case 18:	/* CTRL-R */
				bp = NULL;
				pushdown(&bp,line,pos);
				pushdown(&bp,"^R\r\n",4);
				ret = 1;
				break;
			case 0x7f:	/* DEL */
			case '\b':
				bp = NULL;
				if(pos){
					--pos;
					bp = qdata("\b \b",3);
 /* note: bp can now be NULL.. gotta check for this */
				}
				ret = 1;
				break;
			case '\r':
				c = '\n';	/* CR => NL */
			case '\n':
				bp = qdata("\r\n",2);
 /* note: bp can now be NULL.. gotta check for this */
				break;
			default:
				bp = NULL;
				pushdown(&bp,NULL,1);
				*bp->data = c;
				break;
			}
			asci_send(dev,&bp);
			tip->iface->lastsent = secclock();
			if(ret)
				continue;
		}
		line[pos++] = c;
		if(pos == MBXLINE - 1 || tip->echo == WILL
		  || c == '\n'){
			line[pos] = '\0';
			pos = 0;
			fputs(line,tip->network);
			fflush(tip->network);
		}
	}
	/* get_asci() failed, terminate */
	fclose(tip->network);
	tip->in = tip->proc;
	tip->proc = my_thread_p;
	buf[1] = Asci[dev].iface->name;
	tip0(2,buf,NULL);
}
/* Start mailbox on serial line */
int
tipstart(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct iface_s *ifp;
	register struct asci_s *ap;
	struct tipcb *tip;
	struct mbuf_s *bp;
	char *buf[2];
	int dev, c, cmd, s[2], type = TIP;

	if((ifp = if_lookup(argv[1])) == NULL){
		_printf(_("Interface %s unknown\n"),argv[1]);
		return 1;
	}
	for(dev=0,ap = Asci;dev < ASCI_MAX;dev++,ap++)
		if(ap->iface == ifp)
			break;
	if(dev == ASCI_MAX){
		_printf(_("Interface %s not asci port\n"),argv[1]);
		return 1;
	}
	if(ifp->raw == bitbucket){
		_printf(_("Tip session already active on %s\n"),argv[1]);
		return 1;
	}
	ksignal(my_thread_p,0);	/* Don't keep the parser waiting */
	chname(my_thread_p,"Mbox tip");
	tip = (struct tipcb *) callocw(1,sizeof(struct tipcb));

	/* Save output handler and temporarily redirect output to null */
	tip->rawsave = ifp->raw;
	ifp->raw = bitbucket;
	tip->iface = ifp;
	tip->proc = my_thread_p;
	tip->timer.func = tipidle;
	tip->timer.arg = (void *) tip;
	tip->next = Tiplist;
	Tiplist = tip;
	buf[1] = ifp->name;

	/* Suspend packet input drivers */
	suspend(ifp->rxproc);

	for(;;) {
		/* Wait for DCD to be asserted */
		get_rlsd_asci(dev,1);

		if(socketpair(AF_LOCAL,SOCK_STREAM,0,s) == -1){
			_printf(_("Could not create socket pair, errno %d\n"),errno);
			tip0(2,buf,p);
			return 1;
		}
		tip->echo = WONT;
		tip->network = fdopen(s[0],"r+t");
		process_thread_create(daemon_process_p, "mbx_incom",2048,mbx_incom,s[1],(void *)type,NULL,0);
		set_timer(&tip->timer,Tiptimeout*1000);
		start_timer(&tip->timer);

		/* Now fork into two paths, one rx, one tx */
		tip->in = process_thread_create(daemon_process_p, "Mbox tip in",
				256,tip_in,dev,(void *)tip,NULL,0);
		while((c = getc(tip->network)) != -1) {
			if(c == IAC){	/* ignore most telnet options */
				if((cmd = getc(tip->network)) == -1)
					break;
				if(cmd > 250 && cmd < 255) {
					if((c = getc(tip->network)) == -1)
						break;
					switch(cmd){
					case WILL:
						if(c == TN_ECHO) {
							tip->echo = cmd;
							cmd = DO;
						}
						else
							cmd = DONT;
						break;
					case WONT:
						if(c == TN_ECHO)
							tip->echo = cmd;
						cmd = DONT;
						break;
					case DO:
					case DONT:
						cmd = WONT;
						break;
					}
					_fprintf(tip->network, _("%c%c%c"),IAC,cmd,c);
					fflush(tip->network);
				}
				continue;
			}
			if(c == '\n')
				bp = qdata("\r\n",2);
 /* note: bp can now be NULL.. gotta check for this */
			else {
				bp = NULL;
				pushdown(&bp,NULL,1);
				*bp->data = c;
			}
			asci_send(dev,&bp);
			ifp->lastsent = secclock();
		}
		fclose(tip->network);
		killproc(tip->in);
		tip->in = NULL;
		kwait(itop(s[1])); /* let mailbox terminate, if necessary */
		stop_timer(&tip->timer);

		/* Tell line to go down */
		ifp->ioctl(ifp,PARAM_DOWN,TRUE,0L);

		/* Wait for DCD to be dropped */
		get_rlsd_asci(dev,0);
	}
}
int
tip0(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct iface_s *ifp;
	struct tipcb *tip, *prev = NULLTIP;
	struct thread_s *proc;

	if((ifp = if_lookup(argv[1])) == NULL){
		_printf(_("Interface %s unknown\n"),argv[1]);
		return 1;
	}
	for(tip = Tiplist; tip != NULLTIP; prev = tip, tip = tip->next)
		if(tip->iface == ifp) {
			if(prev != NULLTIP)
				prev->next = tip->next;
			else
				Tiplist = tip->next;
			proc = tip->proc;
			fclose(tip->network);
			ifp->raw = tip->rawsave;
			resume(ifp->rxproc);
			stop_timer(&tip->timer);
			killproc(tip->in);
			free(tip);
			killproc(proc);
			return 0;
		}
	return 0;
}
static void
tipidle(t)
void *t;
{
	struct tipcb *tip;
	static char *msg = "You have been idle too long. Please hang up.\r\n";
	struct mbuf_s *bp;
	tip = (struct tipcb *) t;
	if(secclock() - tip->iface->lastrecv < Tiptimeout){
		set_timer(&tip->timer,(Tiptimeout-secclock() *
		 tip->iface->lastrecv)*1000);
		start_timer(&tip->timer);
		return;
	}
	bp = qdata(msg,strlen(msg));
 /* note: bp can now be NULL.. gotta check for this */
	asci_send(tip->iface->dev,&bp);
	tip->iface->lastsent = secclock();
	fclose(tip->network);
}

