/* Internet FTP Server
 * Copyright 1991 Phil Karn, KA9Q
 */
#include <stdio.h>
#include <errno.h> /* Nick */
#include <ctype.h>
#include <time.h>
#ifdef	__TURBOC__
#include <io.h>
#include <dir.h>
#endif
#include "nos/global.h"
#include "main/mbuf.h"
#include "kernel/thread.h"
#include "socket/socket.h" /* for IPPORT_FTP */
#include "z80/dirutil.h"
#include "main/commands.h"
#include "main/files.h"
#include "main/ftp.h"
#include "server/ftpserv.h"
#include "main/md5.h"
#include "socket/opensock.h" /* for os_deref(), struct usock */
#include "main/logmsg.h"
#include "kernel/dprintf.h"
#include <libintl.h>
#include "po/messages.h"

#define FTPSCMD_MAX 25

#ifdef MODULE
#define STATIC
extern _char *Ftpscmd[FTPSCMD_MAX];
extern _char *Ftpsmsg_banner;
extern _char *Ftpsmsg_badcmd;
extern _char *Ftpsmsg_binwarn;
extern _char *Ftpsmsg_unsupp;
extern _char *Ftpsmsg_givepass;
extern _char *Ftpsmsg_logged;
extern _char *Ftpsmsg_typeok;
extern _char *Ftpsmsg_only8;
extern _char *Ftpsmsg_deleok;
extern _char *Ftpsmsg_mkdok;
extern _char *Ftpsmsg_delefail;
extern _char *Ftpsmsg_pwdmsg;
extern _char *Ftpsmsg_badtype;
extern _char *Ftpsmsg_badport;
extern _char *Ftpsmsg_unimp;
extern _char *Ftpsmsg_bye;
extern _char *Ftpsmsg_nodir;
extern _char *Ftpsmsg_cantopen;
extern _char *Ftpsmsg_sending;
extern _char *Ftpsmsg_cantmake;
extern _char *Ftpsmsg_writerr;
extern _char *Ftpsmsg_portok;
extern _char *Ftpsmsg_rxok;
extern _char *Ftpsmsg_txok;
extern _char *Ftpsmsg_noperm;
extern _char *Ftpsmsg_noconn;
extern _char *Ftpsmsg_lowmem;
extern _char *Ftpsmsg_notlog;
extern _char *Ftpsmsg_userfirst;
extern _char *Ftpsmsg_okay;
extern _char *Ftpsmsg_syst;
#else
#define STATIC static
#define MODULE_Ftpscmd
#define MODULE_Ftpsmsg_banner
#define MODULE_Ftpsmsg_badcmd
#define MODULE_Ftpsmsg_binwarn
#define MODULE_Ftpsmsg_unsupp
#define MODULE_Ftpsmsg_givepass
#define MODULE_Ftpsmsg_logged
#define MODULE_Ftpsmsg_typeok
#define MODULE_Ftpsmsg_only8
#define MODULE_Ftpsmsg_deleok
#define MODULE_Ftpsmsg_mkdok
#define MODULE_Ftpsmsg_delefail
#define MODULE_Ftpsmsg_pwdmsg
#define MODULE_Ftpsmsg_badtype
#define MODULE_Ftpsmsg_badport
#define MODULE_Ftpsmsg_unimp
#define MODULE_Ftpsmsg_bye
#define MODULE_Ftpsmsg_nodir
#define MODULE_Ftpsmsg_cantopen
#define MODULE_Ftpsmsg_sending
#define MODULE_Ftpsmsg_cantmake
#define MODULE_Ftpsmsg_writerr
#define MODULE_Ftpsmsg_portok
#define MODULE_Ftpsmsg_rxok
#define MODULE_Ftpsmsg_txok
#define MODULE_Ftpsmsg_noperm
#define MODULE_Ftpsmsg_noconn
#define MODULE_Ftpsmsg_lowmem
#define MODULE_Ftpsmsg_notlog
#define MODULE_Ftpsmsg_userfirst
#define MODULE_Ftpsmsg_okay
#define MODULE_Ftpsmsg_syst
#define MODULE_ftpstart
#define MODULE_ftpserv
#define MODULE_ftpserv_switch
#define MODULE_ftpserv_switch_xmd5
#define MODULE_ftp0
#define MODULE_pport
#define MODULE_ftplogin
#define MODULE_permcheck
#define MODULE_sendit
#define MODULE_recvit
#endif

STATIC void ftpserv(int argc,void *argv,void *p);
STATIC int ftpserv_switch(int switch_value, struct ftpserv *ftp, char *arg);
STATIC void ftpserv_switch_xmd5(struct ftpserv *ftp, char *arg);
STATIC int pport(struct sockaddr_in *sock,char *arg);
STATIC void ftplogin(struct ftpserv *ftp,char *pass);
STATIC int sendit(struct ftpserv *ftp,char *command,char *file);
STATIC int recvit(struct ftpserv *ftp,char *command,char *file);

/* Command table */
#ifdef MODULE_Ftpscmd
STATIC _char *Ftpscmd[FTPSCMD_MAX] = {
	N_("user"),
	N_("acct"),
	N_("pass"),
	N_("type"),
	N_("list"),
	N_("cwd"),
	N_("dele"),
	N_("name"),
	N_("quit"),
	N_("retr"),
	N_("stor"),
	N_("port"),
	N_("nlst"),
	N_("pwd"),
	N_("xpwd"),			/* For compatibility with 4.2BSD */
	N_("mkd "),
	N_("xmkd"),			/* For compatibility with 4.2BSD */
	N_("xrmd"),			/* For compatibility with 4.2BSD */
	N_("rmd "),
	N_("stru"),
	N_("mode"),
	N_("syst"),
	N_("xmd5"),
	N_("xcwd"),
	NULL
};
#endif

/* Response messages */
#ifdef MODULE_Ftpsmsg_banner
STATIC _char *Ftpsmsg_banner = N_("220 %s FTP version %s ready at %s\r\n");
#endif
#ifdef MODULE_Ftpsmsg_badcmd
STATIC _char *Ftpsmsg_badcmd = N_("500 Unknown command '%s'\r\n");
#endif
#ifdef MODULE_Ftpsmsg_binwarn
STATIC _char *Ftpsmsg_binwarn = N_("100 Warning: type is ASCII and %s appears to be binary\r\n");
#endif
#ifdef MODULE_Ftpsmsg_unsupp
STATIC _char *Ftpsmsg_unsupp = N_("500 Unsupported command or option\r\n");
#endif
#ifdef MODULE_Ftpsmsg_givepass
STATIC _char *Ftpsmsg_givepass = N_("331 Enter PASS command\r\n");
#endif
#ifdef MODULE_Ftpsmsg_logged
STATIC _char *Ftpsmsg_logged = N_("230 Logged in\r\n");
#endif
#ifdef MODULE_Ftpsmsg_typeok
STATIC _char *Ftpsmsg_typeok = N_("200 Type %s OK\r\n");
#endif
#ifdef MODULE_Ftpsmsg_only8
STATIC _char *Ftpsmsg_only8 = N_("501 Only logical bytesize 8 supported\r\n");
#endif
#ifdef MODULE_Ftpsmsg_deleok
STATIC _char *Ftpsmsg_deleok = N_("250 File deleted\r\n");
#endif
#ifdef MODULE_Ftpsmsg_mkdok
STATIC _char *Ftpsmsg_mkdok = N_("200 MKD ok\r\n");
#endif
#ifdef MODULE_Ftpsmsg_delefail
STATIC _char *Ftpsmsg_delefail = N_("550 Delete failed: %s\r\n");
#endif
#ifdef MODULE_Ftpsmsg_pwdmsg
STATIC _char *Ftpsmsg_pwdmsg = N_("257 \"%s\" is current directory\r\n");
#endif
#ifdef MODULE_Ftpsmsg_badtype
STATIC _char *Ftpsmsg_badtype = N_("501 Unknown type \"%s\"\r\n");
#endif
#ifdef MODULE_Ftpsmsg_badport
STATIC _char *Ftpsmsg_badport = N_("501 Bad port syntax\r\n");
#endif
#ifdef MODULE_Ftpsmsg_unimp
STATIC _char *Ftpsmsg_unimp = N_("502 Command not yet implemented\r\n");
#endif
#ifdef MODULE_Ftpsmsg_bye
STATIC _char *Ftpsmsg_bye = N_("221 Goodbye!\r\n");
#endif
#ifdef MODULE_Ftpsmsg_nodir
STATIC _char *Ftpsmsg_nodir = N_("553 Can't read directory \"%s\": %s\r\n");
#endif
#ifdef MODULE_Ftpsmsg_cantopen
STATIC _char *Ftpsmsg_cantopen = N_("550 Can't read file \"%s\": %s\r\n");
#endif
#ifdef MODULE_Ftpsmsg_sending
STATIC _char *Ftpsmsg_sending = N_("150 Opening data connection for %s %s\r\n");
#endif
#ifdef MODULE_Ftpsmsg_cantmake
STATIC _char *Ftpsmsg_cantmake = N_("553 Can't create \"%s\": %s\r\n");
#endif
#ifdef MODULE_Ftpsmsg_writerr
STATIC _char *Ftpsmsg_writerr = N_("552 Write error: %s\r\n");
#endif
#ifdef MODULE_Ftpsmsg_portok
STATIC _char *Ftpsmsg_portok = N_("200 Port command okay\r\n");
#endif
#ifdef MODULE_Ftpsmsg_rxok
STATIC _char *Ftpsmsg_rxok = N_("226 File received OK\r\n");
#endif
#ifdef MODULE_Ftpsmsg_txok
STATIC _char *Ftpsmsg_txok = N_("226 File sent OK\r\n");
#endif
#ifdef MODULE_Ftpsmsg_noperm
STATIC _char *Ftpsmsg_noperm = N_("550 Permission denied\r\n");
#endif
#ifdef MODULE_Ftpsmsg_noconn
STATIC _char *Ftpsmsg_noconn = N_("425 Data connection reset\r\n");
#endif
#ifdef MODULE_Ftpsmsg_lowmem
STATIC _char *Ftpsmsg_lowmem = N_("421 System overloaded, try again later\r\n");
#endif
#ifdef MODULE_Ftpsmsg_notlog
STATIC _char *Ftpsmsg_notlog = N_("530 Please log in with USER and PASS\r\n");
#endif
#ifdef MODULE_Ftpsmsg_userfirst
STATIC _char *Ftpsmsg_userfirst = N_("503 Login with USER first.\r\n");
#endif
#ifdef MODULE_Ftpsmsg_okay
STATIC _char *Ftpsmsg_okay = N_("200 Ok\r\n");
#endif
#ifdef MODULE_Ftpsmsg_syst
STATIC _char *Ftpsmsg_syst = N_("215 %s Type: L%d Version: %s\r\n");
#endif

/* Start up FTP service */
#ifdef MODULE_ftpstart
int
ftpstart(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	uint16 port;

	if(argc < 2)
		port = IPPORT_FTP;
	else
		port = atoi(argv[1]);

	return start_tcp(port,"ftpd" /*"FTP Server"*/,ftpserv,768 /*2048*/);
}
#endif


#ifdef MODULE_ftpserv
STATIC void
ftpserv(argc,argv,p)
int argc;
void *argv;
void *p;
{
	jmp_buf try_b;
	unsigned int catch;
	struct ftpserv ftp;
	_char **cmdp;
	char buf[512], *arg, *cp, *cp1;
#if 1 /* Nick */
	time_t t;
#else
	long t;
#endif
	int i;
	struct sockaddr_in socket;
	unsigned int fd;

	catch = setjmp(try_b);
	if (catch)
		{
		_dprintf(0, _("ftpd uf_alloc() error %u\r\n"), catch);

		os_deref((struct usock *)p);
		return;
		}

	fd = uf_alloc(try_b); /* this will always be 3 (handle after stderr) */
	my_thread_p->process_p->object_p[fd] = &((struct usock *)p)->object;

	memset(&ftp, 0, sizeof(ftp));	/* Start with clear slate */
	ftp.control = fdopen(fd, "r+t");

	sockowner(fd, my_thread_p);	/* We own it now */
	setvbuf(ftp.control, NULL, _IOLBF, BUFSIZ);

	if(availmem()){
		_fprintf(ftp.control, Ftpsmsg_lowmem);
		fclose(ftp.control);
		return;
	}

#if 0 /* doesn't seem to be referenced? */
#ifdef ZILOG /* haven't got fdup() with CRT stdio */
	fclose(stdin);
	stdin = fdopen(fd, "r+t");
	fclose(stdout);
	stdout = fdopen(fd, "r+t"); /* should be "w+t" ?? */
#else
	fclose(stdin);
	stdin = fdup(ftp.control);
	fclose(stdout);
	stdout = fdup(ftp.control);
#endif
#endif

	/* Set default data port */
	i = SOCKSIZE;
	getpeername(fileno(ftp.control),(struct sockaddr *)&socket,&i);
	socket.sin_port = IPPORT_FTPD;
	ASSIGN(ftp.port,socket);

	logmsg(fileno(ftp.control), _("open FTP"));
	time(&t);
	cp = ctime(&t);
	if((cp1 = strchr(cp,'\n')) != NULL)
		*cp1 = '\0';
	_fprintf(ftp.control, Ftpsmsg_banner, Hostname, Version, cp);
loop:	fflush(ftp.control);
	if((fgets(buf,sizeof(buf),ftp.control)) == NULL){
		/* He closed on us */
		goto finish;
	}
	if(strlen(buf) == 0){
		/* Can't be a legal FTP command */
		_fprintf(ftp.control, Ftpsmsg_badcmd, buf);
		goto loop;
	}
	rip(buf);
	/* Translate first word to lower case */
	for(cp = buf;*cp != ' ' && *cp != '\0';cp++)
		*cp = tolower(*cp);
	/* Find command in table; if not present, return syntax error */
	for(cmdp = Ftpscmd;*cmdp != NULL;cmdp++)
#if 1 /* Nick */
		if(_strncmp(buf, *cmdp, _strlen(*cmdp)) == 0)
#else
		if(strncmp(*cmdp,buf,strlen(*cmdp)) == 0)
#endif
			break;
	if(*cmdp == NULL){
		_fprintf(ftp.control, Ftpsmsg_badcmd, buf);
		goto loop;
	}
	/* Allow only USER, PASS and QUIT before logging in */
	if(ftp.cd == NULL || ftp.path == NULL){
		switch(cmdp-Ftpscmd){
		case USER_CMD:
		case PASS_CMD:
		case QUIT_CMD:
			break;
		default:
			_fprintf(ftp.control, Ftpsmsg_notlog);
			goto loop;
		}
	}
	arg = &buf[_strlen(*cmdp)];
	while(*arg == ' ')
		arg++;

	/* Execute specific command */
	if (ftpserv_switch(cmdp-Ftpscmd, &ftp, arg))
		{
		goto loop;
		}

finish:
	logmsg(fileno(ftp.control), _("close FTP"));
	/* Clean up */
	fclose(ftp.control);
	if(ftp.data != NULL)
		fclose(ftp.data);
	if(ftp.fp != NULL)
		fclose(ftp.fp);
	free(ftp.username);
	free(ftp.path);
	free(ftp.cd);
}
#endif

#ifdef MODULE_ftpserv_switch
STATIC int
ftpserv_switch(switch_value, ftp, arg)
	int switch_value;
	struct ftpserv *ftp;
	char *arg;
{
	char *file, *mode;

	switch(switch_value) {
	case USER_CMD:
		free(ftp->username);
		ftp->username = strdupw(arg);
		_fprintf(ftp->control, Ftpsmsg_givepass);
		break;
	case TYPE_CMD:
		switch(arg[0]){
		case 'A':
		case 'a':	/* Ascii */
			ftp->type = ASCII_TYPE;
			_fprintf(ftp->control, Ftpsmsg_typeok, arg);
			break;
		case 'l':
		case 'L':
			while(*arg != ' ' && *arg != '\0')
				arg++;
			if(*arg == '\0' || *++arg != '8'){
				_fprintf(ftp->control, Ftpsmsg_only8);
				break;
			}
			ftp->type = LOGICAL_TYPE;
			ftp->logbsize = 8;
			_fprintf(ftp->control, Ftpsmsg_typeok, arg);
			break;
		case 'B':
		case 'b':	/* Binary */
		case 'I':
		case 'i':	/* Image */
			ftp->type = IMAGE_TYPE;
			_fprintf(ftp->control, Ftpsmsg_typeok, arg);
			break;
		default:	/* Invalid */
			_fprintf(ftp->control, Ftpsmsg_badtype, arg);
			break;
		}
		break;
	case QUIT_CMD:
		_fprintf(ftp->control, Ftpsmsg_bye);
		return 0; /*goto finish;*/
	case RETR_CMD:
		file = pathname(ftp->cd,arg);
		switch(ftp->type){
		case IMAGE_TYPE:
		case LOGICAL_TYPE:
			mode = READ_BINARY;
			break;
		case ASCII_TYPE:
			mode = READ_TEXT;
			break;
		}

		if(!permcheck(ftp->path,ftp->perms,RETR_CMD,file)){
		 	_fprintf(ftp->control, Ftpsmsg_noperm);
		} else if((ftp->fp = fopen(file,mode)) == NULL){
			_fprintf(ftp->control, Ftpsmsg_cantopen, file, strerror(errno));
		} else {
			logmsg(fileno(ftp->control), _("RETR %s"), file);
			if(ftp->type == ASCII_TYPE && isbinary(ftp->fp)){
				_fprintf(ftp->control, Ftpsmsg_binwarn, file);
			}
			sendit(ftp,"RETR",file);
		}

		FREE(file);
		break;
	case STOR_CMD:
		file = pathname(ftp->cd,arg);
		switch(ftp->type){
		case IMAGE_TYPE:
		case LOGICAL_TYPE:
			mode = WRITE_BINARY;
			break;
		case ASCII_TYPE:
			mode = WRITE_TEXT;
			break;
		}
		if(!permcheck(ftp->path,ftp->perms,STOR_CMD,file)){
		 	_fprintf(ftp->control, Ftpsmsg_noperm);
		} else if((ftp->fp = fopen(file,mode)) == NULL){
			_fprintf(ftp->control, Ftpsmsg_cantmake, file, strerror(errno));
		} else {
			logmsg(fileno(ftp->control), _("STOR %s"), file);
			recvit(ftp,"STOR",file);
		}
		FREE(file);
		break;
	case PORT_CMD:
		if(pport(&ftp->port,arg) == -1){
			_fprintf(ftp->control, Ftpsmsg_badport);
		} else {
			_fprintf(ftp->control, Ftpsmsg_portok);
		}
		break;
#ifndef CPM
	case LIST_CMD:
		file = pathname(ftp->cd,arg);
		if(!permcheck(ftp->path,ftp->perms,RETR_CMD,file)){
		 	_fprintf(ftp->control, Ftpsmsg_noperm);
		} else if((ftp->fp = dir(file,1)) == NULL){
			_fprintf(ftp->control, Ftpsmsg_nodir, file, strerror(errno));
		} else {
			sendit(ftp,"LIST",file);
		}
		FREE(file);
		break;
	case NLST_CMD:
		file = pathname(ftp->cd,arg);
		if(!permcheck(ftp->path,ftp->perms,RETR_CMD,file)){
		 	_fprintf(ftp->control, Ftpsmsg_noperm);
		} else if((ftp->fp = dir(file,0)) == NULL){
			_fprintf(ftp->control, Ftpsmsg_nodir, file, strerror(errno));
		} else {
			sendit(ftp,"NLST",file);
		}
		FREE(file);
		break;
	case XCWD_CMD:
	case CWD_CMD:
		file = pathname(ftp->cd,arg);
		if(!permcheck(ftp->path,ftp->perms,RETR_CMD,file)){
		 	_fprintf(ftp->control, Ftpsmsg_noperm);
			FREE(file);
#ifdef	MSDOS
		/* Don'tcha just LOVE %%$#@!! MS-DOS? */
		} else if(strcmp(file,"/") == 0 || access(file,0) == 0){
#else
		} else if(access(file,0) == 0){	/* See if it exists */
#endif
			/* Succeeded, record in control block */
			free(ftp->cd);
			ftp->cd = file;
			_fprintf(ftp->control, Ftpsmsg_pwdmsg, file);
		} else {
			/* Failed, don't change anything */
			_fprintf(ftp->control, Ftpsmsg_nodir, file, strerror(errno));
			FREE(file);
		}
		break;
	case XPWD_CMD:
	case PWD_CMD:
		_fprintf(ftp->control, Ftpsmsg_pwdmsg, ftp->cd);
		break;
#else
	case LIST_CMD:
	case NLST_CMD:
	case CWD_CMD:
	case XCWD_CMD:
	case XPWD_CMD:
	case PWD_CMD:
#endif
	case ACCT_CMD:
		_fprintf(ftp->control, Ftpsmsg_unimp);
		break;
	case DELE_CMD:
		file = pathname(ftp->cd,arg);
		if(!permcheck(ftp->path,ftp->perms,DELE_CMD,file)){
		 	_fprintf(ftp->control, Ftpsmsg_noperm);
		} else if(unlink(file) == 0){
			logmsg(fileno(ftp->control), _("DELE %s"), file);
			_fprintf(ftp->control, Ftpsmsg_deleok);
		} else {
			_fprintf(ftp->control, Ftpsmsg_delefail, strerror(errno));
		}
		FREE(file);
		break;
	case PASS_CMD:
		if(ftp->username == NULL)
			_fprintf(ftp->control, Ftpsmsg_userfirst);
		else
			ftplogin(ftp,arg);
		break;
#ifndef	CPM
	case XMKD_CMD:
	case MKD_CMD:
		file = pathname(ftp->cd,arg);
		if(!permcheck(ftp->path,ftp->perms,MKD_CMD,file)){
			_fprintf(ftp->control, Ftpsmsg_noperm);
#ifdef	__TURBOC__
		} else if(mkdir(file) == 0){
#else
		} else if(mkdir(file,0777) == 0){
#endif
			logmsg(fileno(ftp->control), _("MKD %s"), file);
			_fprintf(ftp->control, Ftpsmsg_mkdok);
		} else {
			_fprintf(ftp->control, Ftpsmsg_cantmake, file, strerror(errno));
		}
		FREE(file);
		break;
	case XRMD_CMD:
	case RMD_CMD:
		file = pathname(ftp->cd,arg);
		if(!permcheck(ftp->path,ftp->perms,RMD_CMD,file)){
		 	_fprintf(ftp->control, Ftpsmsg_noperm);
		} else if(rmdir(file) == 0){
			logmsg(fileno(ftp->control), _("RMD %s"), file);
			_fprintf(ftp->control, Ftpsmsg_deleok);
		} else {
			_fprintf(ftp->control, Ftpsmsg_delefail, strerror(errno));
		}
		FREE(file);
		break;
	case STRU_CMD:
		if(tolower(arg[0]) != 'f')
			_fprintf(ftp->control, Ftpsmsg_unsupp);
		else
			_fprintf(ftp->control, Ftpsmsg_okay);
		break;
	case MODE_CMD:
		if(tolower(arg[0]) != 's')
			_fprintf(ftp->control, Ftpsmsg_unsupp);
		else
			_fprintf(ftp->control, Ftpsmsg_okay);
		break;
	case SYST_CMD:
		_fprintf(ftp->control, Ftpsmsg_syst, System, NBBY, Version);
		break;
	case XMD5_CMD:
		ftpserv_switch_xmd5(ftp, arg);
		break;
	}
#endif
	return 1; /*goto loop;*/
}
#endif

#ifdef MODULE_ftpserv_switch_xmd5
STATIC void
ftpserv_switch_xmd5(ftp, arg)
	struct ftpserv *ftp;
	char *arg;
{
	int i;
	char *file, *mode;

	file = pathname(ftp->cd,arg);
	switch(ftp->type){
	case IMAGE_TYPE:
	case LOGICAL_TYPE:
		mode = READ_BINARY;
		break;
	case ASCII_TYPE:
		mode = READ_TEXT;
		break;
	}
	if(!permcheck(ftp->path,ftp->perms,RETR_CMD,file)){
	 	_fprintf(ftp->control, Ftpsmsg_noperm);
	} else if((ftp->fp = fopen(file,mode)) == NULL){
		_fprintf(ftp->control, Ftpsmsg_cantopen, file, strerror(errno));
	} else {
		uint8 hash[16];

		logmsg(fileno(ftp->control), _("XMD5 %s"), file);
		if(ftp->type == ASCII_TYPE && isbinary(ftp->fp))
			_fprintf(ftp->control, Ftpsmsg_binwarn, file);

		md5hash(ftp->fp,hash,ftp->type == ASCII_TYPE);
		fclose(ftp->fp);
		ftp->fp = NULL;
		_fprintf(ftp->control, _("200 "));
		for(i=0;i<16;i++)
			_fprintf(ftp->control, _("%02x"),hash[i]);
		_fprintf(ftp->control, _(" %s\r\n"),file);
	}
	FREE(file);
}
#endif

/* Shut down FTP server */

#ifdef MODULE_ftp0
int
ftp0(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	uint16 port;

	if(argc < 2)
		port = IPPORT_FTP;
	else
		port = atoi(argv[1]);

	return stop_tcp(port);
}
#endif


#ifdef MODULE_pport
STATIC int
pport(sock,arg)
struct sockaddr_in *sock;
char *arg;
{
	int32 n;
	int i;

	n = 0;
	for(i=0;i<4;i++){
		n = atoi(arg) + (n << 8);
		if((arg = strchr(arg,',')) == NULL)
			return -1;
		arg++;
	}
	sock->sin_addr.s_addr = n;
	n = atoi(arg);
	if((arg = strchr(arg,',')) == NULL)
		return -1;
	arg++;
	n = atoi(arg) + (n << 8);
	sock->sin_port = n;
	return 0;
}
#endif

/* Attempt to log in the user whose name is in ftp->username and password
 * in pass
 */

#ifdef MODULE_ftplogin
STATIC void
ftplogin(ftp,pass)
struct ftpserv *ftp;
char *pass;
{
	char *path;
	int anony = 0;

	path = mallocw(200);
	if((ftp->perms = userlogin(ftp->username,pass,&path,200,&anony))
	   == -1){
		_fprintf(ftp->control, Ftpsmsg_noperm);
		free(path);
		return;
	}
	/* Set up current directory and path prefix */
/*#if	defined(AMIGAGONE)*/
/*	ftp->cd = pathname("", path);*/
/*	ftp->path = strdupw(ftp->cd);*/
/*	free(path);*/
/*#else*/
#if 1 /* trim the path, as it was returned in a fixed size (200 byte) buffer */
	ftp->cd = strdupw(path);
	ftp->path = strdupw(path);
	free(path);
#else
	ftp->cd = path;
	ftp->path = strdupw(path);
#endif
/*#endif*/

	_fprintf(ftp->control, Ftpsmsg_logged);
	if(!anony)
		logmsg(fileno(ftp->control), _("%s logged in"), ftp->username);
	else
		logmsg(fileno(ftp->control), _("%s logged in, ID %s"), ftp->username,pass);
}
#endif

#ifdef	MSDOS
/* Illegal characters in a DOS filename */
STATIC char badchars[] = "\"[]:|<>+=;,";
#endif

/* Return 1 if the file operation is allowed, 0 otherwise */

#ifdef MODULE_permcheck
int
permcheck(path,perms,op,file)
char *path;
int perms;
int op;
char *file;
{
#ifdef	MSDOS
	char *cp;
#endif

	if(file == NULL || path == NULL)
		return 0;	/* Probably hasn't logged in yet */
#ifdef	MSDOS
	/* Check for characters illegal in MS-DOS file names */
	for(cp = badchars;*cp != '\0';cp++){
		if(strchr(file,*cp) != NULL)
			return 0;
	}
#endif
#ifndef MAC
	/* The target file must be under the user's allowed search path */
	if(strncmp(file,path,strlen(path)) != 0)
		return 0;
#endif

	switch(op){
	case RETR_CMD:
		/* User must have permission to read files */
		if(perms & FTP_READ)
			return 1;
		return 0;
	case DELE_CMD:
	case RMD_CMD:
		/* User must have permission to (over)write files */
		if(perms & FTP_WRITE)
			return 1;
		return 0;
	case STOR_CMD:
	case MKD_CMD:
		/* User must have permission to (over)write files, or permission
		 * to create them if the file doesn't already exist
		 */
		if(perms & FTP_WRITE)
			return 1;
		if(access(file,2) == -1 && (perms & FTP_CREATE))
			return 1;
		return 0;
	}
	return 0;	/* "can't happen" -- keep lint happy */
}
#endif


#ifdef MODULE_sendit
STATIC int
sendit(ftp,command,file)
struct ftpserv *ftp;
char *command;
char *file;
{
	long total;
	struct sockaddr_in dport;
	int s;

	s = socket(AF_INET,SOCK_STREAM,0);
	dport.sin_family = AF_INET;
	dport.sin_addr.s_addr = INADDR_ANY;
	dport.sin_port = IPPORT_FTPD;
	bind(s,(struct sockaddr *)&dport,SOCKSIZE);
	_fprintf(ftp->control, Ftpsmsg_sending, command, file);
	fflush(ftp->control);
	if(connect(s,(struct sockaddr *)&ftp->port,SOCKSIZE) == -1){
		fclose(ftp->fp);
		ftp->fp = NULL;
		close(s);
		ftp->data = NULL;
		_fprintf(ftp->control, Ftpsmsg_noconn);
		return -1;
	}
	ftp->data = fdopen(s,"r+");
	/* Do the actual transfer */
	total = sendfile(ftp->fp,ftp->data,ftp->type,0);

	if(total == -1){
		/* An error occurred on the data connection */
		_fprintf(ftp->control, Ftpsmsg_noconn);
		shutdown(fileno(ftp->data),2);	/* Blow away data connection */
		fclose(ftp->data);
	} else {
		_fprintf(ftp->control, Ftpsmsg_txok);
	}
	fclose(ftp->fp);
	ftp->fp = NULL;
	fclose(ftp->data);
	ftp->data = NULL;
	if(total == -1)
		return -1;
	else
		return 0;
}
#endif


#ifdef MODULE_recvit
STATIC int
recvit(ftp,command,file)
struct ftpserv *ftp;
char *command;
char *file;
{
	struct sockaddr_in dport;
	long total;
	int s;

	s = socket(AF_INET,SOCK_STREAM,0);
	dport.sin_family = AF_INET;
	dport.sin_addr.s_addr = INADDR_ANY;
	dport.sin_port = IPPORT_FTPD;
	bind(s,(struct sockaddr *)&dport,SOCKSIZE);
	_fprintf(ftp->control, Ftpsmsg_sending, command, file);
	fflush(ftp->control);
	if(connect(s,(struct sockaddr *)&ftp->port,SOCKSIZE) == -1){
		fclose(ftp->fp);
		ftp->fp = NULL;
		close(s);
		ftp->data = NULL;
		_fprintf(ftp->control, Ftpsmsg_noconn);
		return -1;
	}
	ftp->data = fdopen(s,"r+");
	/* Do the actual transfer */
	total = recvfile(ftp->fp,ftp->data,ftp->type,0);

#ifdef	CPM
	if(ftp->type == ASCII_TYPE)
		putc(CTLZ,ftp->fp);
#endif
	if(total == -1) {
		/* An error occurred while writing the file */
		_fprintf(ftp->control, Ftpsmsg_writerr, strerror(errno));
		shutdown(fileno(ftp->data),2);	/* Blow it away */
		fclose(ftp->data);
	} else {
		_fprintf(ftp->control, Ftpsmsg_rxok);
		fclose(ftp->data);
	}
	ftp->data = NULL;
	fclose(ftp->fp);
	ftp->fp = NULL;
	if(total == -1)
		return -1;
	else
		return 0;
}
#endif

