# auto.mk by Nick for Hytech NOS/UZI project

# -----------------------------------------------------------------------------

SUBDIRS=

OUTPUTS=	server.$(LIBEXT)

server_$(LIBEXT)_SOURCES= \
		bootpcmd.c bootpd.c bootpdip.c fingerd.c ftpserv.c \
		ttylink.c telnetd.c
#bmutil.c forward.c mailbox.c popserv.c rewrite.c smisc.c smtpserv.c
#tipmail.c

ftpserv_c_MODULES= \
		Ftpscmd Ftpsmsg_banner Ftpsmsg_badcmd Ftpsmsg_binwarn \
		Ftpsmsg_unsupp Ftpsmsg_givepass Ftpsmsg_logged Ftpsmsg_typeok \
		Ftpsmsg_only8 Ftpsmsg_deleok Ftpsmsg_mkdok Ftpsmsg_delefail \
		Ftpsmsg_pwdmsg Ftpsmsg_badtype Ftpsmsg_badport Ftpsmsg_unimp \
		Ftpsmsg_bye Ftpsmsg_nodir Ftpsmsg_cantopen Ftpsmsg_sending \
		Ftpsmsg_cantmake Ftpsmsg_writerr Ftpsmsg_portok Ftpsmsg_rxok \
		Ftpsmsg_txok Ftpsmsg_noperm Ftpsmsg_noconn Ftpsmsg_lowmem \
		Ftpsmsg_notlog Ftpsmsg_userfirst Ftpsmsg_okay Ftpsmsg_syst \
		ftpstart ftpserv ftpserv_switch ftpserv_switch_xmd5 ftp0 \
		pport ftplogin permcheck sendit recvit

# -----------------------------------------------------------------------------

