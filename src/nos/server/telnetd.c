/*
 * Tiny Telnet Server
 * Based on FTP server and "Dumb terminal" mailbox interface
 * (both Copyright 1991 Phil Karn, KA9Q)
 *
 *	May '04	RPB
 *		initial version
 *
 *	Aug '04	Nick
 *		revised to use /dev/vty0
 *		moved telnet logic to devtty.c
 *		rewritten login/password code
 */
#include <fcntl.h> /* for O_RDWR */
#include <sgtty.h> /* for TIOC_CONNECT */
#include <unistd.h> /* for read(), write() */
#include "nos/global.h" /* for typedef int32 */
#include "main/logmsg.h" /* for logmsg() prototype (VARARGS!!) */
#include "main/files.h" /* for EXCLUDED_CMD */
#include "socket/socket.h" /* for IPPORT_TELNET */
#include "z80/asci.h" /* for ASCI_MAX (rather inelegant) */
#include "driver/devtty.h" /* for TTY_MINOR_LIST (rather inelegant) */
#include "filesys/openfile.h" /* for of_ref(), struct open_file_s */
#include "socket/opensock.h" /* for os_deref(), struct usock */
#include "kernel/userfile.h" /* for uf_alloc() */
#include "kernel/dprintf.h"
#include <libintl.h>
#include "po/messages.h"

void net_prompt(int argc, void *argv, void *p); /* in misc.c */

static void telnetd(int argc, void *argv, void *p);
static unsigned int telnetd_setup(struct usock *os_p);
static unsigned int telnetd_login(int *privs, int needs, int anony);

/* Start up Telnet service */
int
telnet1(int argc, char *argv[], void *p)
	{
	uint16 port;

	if(argc < 2)
		port = IPPORT_TELNET;
	else
		port = atoi(argv[1]);

	return start_tcp(port, "telnetd", telnetd, 768);
	}

/* Stop Telnet server */
int
telnet0(int argc, char *argv[], void *p)
	{
	uint16 port;

	if(argc < 2)
		port = IPPORT_TELNET;
	else
		port = atoi(argv[1]);

	return stop_tcp(port);
	}

/*
 * The actual Telnet server (daemon)
 */
static void
telnetd(int argc, void *argv, void *p)
	{
	if (telnetd_setup((struct usock *)p))
		{
		return;
		}

	putchar('\n');
	net_prompt(0, NULL, NULL);
	}

static unsigned int
telnetd_setup(struct usock *os_p)
	{
	jmp_buf try_b;
	unsigned int catch;
	int fd, privs;
	struct sgttyb state;
	unsigned char device_name[0x10];
	struct process_s *process_p;

	for (fd = ASCI_MAX; fd < TTY_MINOR_LIST; fd++)
		{
		if (tty_minor_list[fd] == NULL ||
				tty_minor_list[fd]->refs == 0)
			{
			goto found;
			}
		}

	_dprintf(0, _("telnetd no free vty\n"));
	return -1;

found:
	sprintf(device_name, "/dev/vty%u", fd - ASCI_MAX);

#if 1
	process_p = my_thread_p->process_p;

	process_fclose(process_p, process_p->stream_p[0]); /* stdin */
	process_fclose(process_p, process_p->stream_p[1]); /* stdout */
	process_fclose(process_p, process_p->stream_p[2]); /* stderr */

	close(0);
	if (open(device_name, O_RDWR) != 0 || fdopen(0, "rb") == NULL ||
			dup2(0, 1) != 1 || fdopen(1, "wb") == NULL ||
			dup2(0, 2) != 2 || fdopen(2, "wb") == NULL ||
			setvbuf(stderr, NULL, _IONBF, 0) != 0)
		{
		_panic(_("failed to open %s"), device_name);
		}
#else
	if (open(device_name, O_RDWR) != 0)
		{
		_dprintf(0, _("telnetd open() fail\n"));

		os_deref(os_p);
		return -1;
		}

	if (dup2(0, 1) != 1 || dup2(0, 2) != 2)
		{
		_dprintf(0, _("telnetd dup2() fail\n"));

		os_deref(os_p);
		return -1;
		}
#endif

	catch = setjmp(try_b);
	if (catch)
		{
		_dprintf(0, _("telnetd uf_alloc() error %u\n"), catch);

		os_deref(os_p);
		return -1;
		}

/* abyte('A'); */
	fd = uf_alloc(try_b); /* this will always be 3 (handle after stderr) */
	process_p->object_p[fd] = &os_p->object;

/* abyte('B'); */
	gtty(STDIN_FILENO, &state);
	state.sg_flags |= TNET;
	stty(STDIN_FILENO, &state);

/* abyte('C'); */
	if (ioctl(0, TIOC_CONNECT, &fd) != 0)
		{
		_dprintf(0, _("telnetd ioctl() fail\n"));
		return -1;
		}

/* abyte('D'); */
	if (close(fd) != 0)
		{
		_dprintf(0, _("telnetd close() fail\n"));
		return -1;
		}

/* abyte('E'); */
	/* Turn off remote echo and print welcome banner */
	_printf(_("%c%c%c\nHytech KA9Q NOS/UZI telnetd %s\n\n"),
			IAC, WILL, TN_ECHO, device_name);

	/* Login using routine that's also useful elsewhere */
	return telnetd_login(&privs, TELNET_CMD, 0);
	}

static unsigned int
telnetd_login(int *privs, int needs, int anony)
	{
	char name[20];
	char pass[20];
	char path[129], *path_p;
	struct sgttyb state;
	int raw;

	gtty(STDIN_FILENO, &state);
	raw = state.sg_flags;

	for(;;)
		{
		_printf(_("%s login: "), Hostname);
		fflush(stdout);

		if (fgets(name, sizeof(name), stdin) == NULL)
			{
			return -1;
			}
		rip(name); /* remove cr/lf */
		if (*name == '\0')
			{
			continue;
			}

		_fputs(_("Password: "), stdout);
		fflush(stdout);

		state.sg_flags &= ~ECHO;
		stty(STDIN_FILENO, &state);

		state.sg_flags = raw;
		if (fgets(pass, sizeof(pass), stdin) == NULL)
			{
			stty(STDIN_FILENO, &state);
			putchar('\n');
			return -1;
			}
		stty(STDIN_FILENO, &state);
		putchar('\n');
		rip(pass); /* remove cr/lf */

		path_p = path; /* temporary, unnecessary kludge */
		*privs = userlogin(name, pass, &path_p, 128, &anony);

		/* note: if *privs == -1 then EXCLUDED_CMD is set! */
		if ((*privs & (EXCLUDED_CMD | needs)) == needs)
			{
			if (anony)
				{
				logmsg(STDOUT_FILENO,
						_("Login: %s Password: %s"),
						name, pass);
				}
			else
				{
				logmsg(STDOUT_FILENO, _("Login: %s"), name);
				}
			return 0;
			}

		_puts(_("Login incorrect\n")); /* really \n\n due to _puts() */
		}
	}

