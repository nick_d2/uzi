/* Internet Finger server
 * Copyright 1991 Phil Karn, KA9Q
 */
#include <stdio.h>
#include <string.h>
#include "nos/global.h"
#include "main/files.h"
#include "main/mbuf.h"
#include "socket/socket.h" /* for IPPORT_FINGER */
#include "kernel/session.h"
#include "kernel/thread.h"
#include "z80/dirutil.h"
#include "main/commands.h"
#include "server/mailbox.h"
#include "socket/opensock.h" /* for os_deref(), struct usock */
#include "main/logmsg.h"
#include "kernel/dprintf.h"
#include <libintl.h>
#include "po/messages.h"

static void fingerd(int argc,void *argv,void *p);

/* Start up finger service */
int
finstart(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	uint16 port;

	if(argc < 2)
		port = IPPORT_FINGER;
	else
		port = atoi(argv[1]);

	return start_tcp(port,"fingerd" /*"Finger Server"*/,fingerd,512);
}

static void
fingerd(argc,argv,p)
int argc;
void *argv;
void *p;
{
	jmp_buf try_b;
	unsigned int catch;
	char user[80];
	FILE *fp;
	char *file,*cp;
	FILE *network;
	unsigned int fd;

	catch = setjmp(try_b);
	if (catch)
		{
		_dprintf(0, _("fingerd uf_alloc() error %u\n"), catch);

		os_deref((struct usock *)p);
		return;
		}

	fd = uf_alloc(try_b); /* this will always be 3 (handle after stderr) */
	my_thread_p->process_p->object_p[fd] = &((struct usock *)p)->object;

	network = fdopen(fd, "r+t");

	sockowner(fd, my_thread_p);
	logmsg(fd, _("open Finger"));

	fgets(user,sizeof(user),network);
	rip(user);

	if(strlen(user) == 0){
		fp = dir(Fdir,0);
		if(fp == NULL)
			_fprintf(network, _("No finger information available\n"));
		else
			_fprintf(network, _("Known users on this system:\n"));
	} else {
		file = pathname(Fdir,user);
		cp = pathname(Fdir,"");
		/* Check for attempted security violation (e.g., somebody
		 * might be trying to finger "../ftpusers"!)
		 */
		if(strncmp(file,cp,strlen(cp)) != 0){
			fp = NULL;
			_fprintf(network, _("Invalid user name %s\n"),user);
		} else if((fp = fopen(file,READ_TEXT)) == NULL)
			_fprintf(network, _("User %s not known\n"),user);
		free(cp);
		free(file);
	}

	if(fp){
		sendfile(fp,network,ASCII_TYPE,0);
		fclose(fp);
	}

	if(strlen(user) == 0 && Listusers != NULL)
		(*Listusers)(network);

	fclose(network);
	logmsg(fd, _("close Finger"));
}

int
fin0(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	uint16 port;

	if(argc < 2)
		port = IPPORT_FINGER;
	else
		port = atoi(argv[1]);

	return stop_tcp(port);
}

