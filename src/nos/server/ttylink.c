/* Internet TTY "link" (keyboard chat) server
 * Copyright 1991 Phil Karn, KA9Q
 *
 * Apr 04       RPB     Po-ed most of the remaining strings
 */
#include <stdio.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include "socket/socket.h"
#include "client/telnet.h"
#ifdef JOB_CONTROL
#include "kernel/session.h"
#endif
#include "kernel/thread.h"
#include "driver/devtty.h" /* for struct ttystate */
#include "server/mailbox.h"
#include "main/commands.h"
#include "main/logmsg.h"
#include "main/main.h"
#include <libintl.h>
#include "po/messages.h"

static int Sttylink = -1;	/* Protoype socket for service */

void tnrecv(struct telnet *tn); /* note: declared STATIC in clients/telnet.c */

int
ttylstart(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct sockaddr_in lsocket;
	int s,type;
	FILE *network;

	if(Sttylink != -1){
		return 0;
	}
	ksignal(my_thread_p,0);	/* Don't keep the parser waiting */
	chname(my_thread_p,"TTYlink listener");

	lsocket.sin_family = AF_INET;
	lsocket.sin_addr.s_addr = INADDR_ANY;
	if(argc < 2)
		lsocket.sin_port = IPPORT_TTYLINK;
	else
		lsocket.sin_port = atoi(argv[1]);

	Sttylink = socket(AF_INET,SOCK_STREAM,0);
	bind(Sttylink,(struct sockaddr *)&lsocket,sizeof(lsocket));
	listen(Sttylink,1);
	for(;;){
		if((s = accept(Sttylink,NULL,(int *)NULL)) == -1)
			break;	/* Service is shutting down */

		network = fdopen(s,"r+t");
		if(availmem() != 0){
			_fprintf(network, _("System is overloaded; try again later\n"));
			fclose(network);
		} else {
			type = TELNET;
			process_thread_create(daemon_process_p, "chat",2048,ttylhandle,s,
			 (void *)&type,(void *)network,0);
		}
	}
	return 0;
}
/* This function handles all incoming "chat" sessions, be they TCP,
 * NET/ROM or AX.25
 */
void
ttylhandle(s,t,p)
int s;
void *t;
void *p;
{
	int type;
#ifdef JOB_CONTROL
	struct session_s *sp;
#endif
	struct sockaddr addr;
	int len = MAXSOCKSIZE;
	struct telnet tn;
	FILE *network;
#ifdef JOB_CONTROL
	char *tmp;
#endif
#if 0
	char *duptext; /*RPB*/
#endif

	type = *(int *)t;
	network = (FILE *)p;
	sockowner(fileno(network),my_thread_p);	/* We own it now */
	getpeername(fileno(network),&addr,&len);

#if 0
	/*RPB*/
	if ((duptext = _strdup(Sestypes[type])) != NULL)
		logmsg(fileno(network), _("open %s"), duptext);
	else
		__getfail(Sestypes[type]);
	/*RPB*/
#endif

#ifdef JOB_CONTROL
	tmp = malloc(BUFSIZ);
	_sprintf(tmp,_("ttylink %s"),psocket(&addr));

	/* Allocate a session descriptor */
	if((sp = newsession(tmp,type,1)) == NULL){
		_fprintf(network, _("Too many sessions\n"));
		fclose(network);
		free(tmp);
		free(duptext); /*RPB*/
		return;
	}
	free(tmp);
#endif
	/* Initialize a Telnet protocol descriptor */
	memset(&tn,0,sizeof(tn));
#ifdef JOB_CONTROL
	tn.session = sp;	/* Upward pointer */
	sp->cb.telnet = &tn;	/* Downward pointer */
	sp->network = network;
	sp->proc = my_thread_p;
#endif
	tn.network = network; /* Nick */
	setvbuf(/*sp->*/network,NULL,_IOLBF,BUFSIZ);

#if 0
	/*RPB*/
	if (duptext != NULL)
	{
		_printf(_("\007Incoming %s session %u from %s\007\n"),
	 	        duptext,sp->index,psocket(&addr));
		free(duptext);
	}
	/*RPB*/
#endif

	tnrecv(&tn);
}

/* Shut down Ttylink server */
int
ttyl0(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	close/*_s*/(Sttylink);
	Sttylink = -1;
	return 0;
}
