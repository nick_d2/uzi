/* ICMP-related user commands
 * Copyright 1991 Phil Karn, KA9Q
 */
#include <stdio.h>
#include "nos/global.h"
#include "internet/icmp.h"
#include "main/mbuf.h"
#include "internet/netuser.h"
#include "internet/internet.h"
#include "socket/socket.h"
#ifdef JOB_CONTROL
#include "kernel/session.h"
#endif
#include "main/commands.h"
#include "internet/ping.h"
#include "main/main.h" /* for Display global variable... ZILOG only (Nick) */
#include "kernel/thread.h" /* for my_thread_p */
#include <libintl.h>
#include "po/messages.h"

static void pingtx(int s,void *ping1,void *p);
static void pinghdr(/*struct session_s *sp,*/struct ping *ping);
static int pingproc(int c);
static int pingem(int s,int32 target,uint16 seq,uint16 id,uint16 len);
static int keychar(int c);

/* Send ICMP Echo Request packets */
int
doping(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct sockaddr_in from;
	struct icmp icmp;
	struct mbuf_s *bp;
	int32 timestamp,rtt,abserr;
	int s,fromlen;
	struct ping ping;
#ifdef JOB_CONTROL
	struct session_s *sp;
#else
	struct thread_s *proc1;
#endif

	memset(&ping,0,sizeof(ping));
#ifdef JOB_CONTROL
	/* Allocate a session descriptor */
	if((sp = ping.sp = newsession(Cmdline,PING,1)) == NULL){
		_printf(_("Too many sessions\n"));
		return 1;
	}
#endif
	if((ping.s = s = socket(AF_INET,SOCK_RAW,ICMP_PTCL)) == -1){
		_printf(_("Can't create socket\n"));
#ifdef JOB_CONTROL
		freesession(sp, 1); /* does keywait(NULL, 1); beforehand */
#endif
		return 1;
	}
#ifdef JOB_CONTROL
	sp->inproc = keychar;	/* Intercept ^C */
#endif
	if(SETSIG(EABORT)){
/*RPB*/
		/*
		 * Necessary in some cases
		 * Actually, freesession() should do a complete cleanup
		 * NICK NOTE: COULD DO SOMETHING LIKE nrcmd.c BEFOREHAND:
		 *	sp->network = fdopen(s,"r+t");
		 * IT WOULD WASTE BUFFER SPACE, BUT ENSURES s GETS CLOSED
		 */
		close(s);
/*RPB*/
#ifdef JOB_CONTROL
		freesession(sp, 1); /* does keywait(NULL, 1); beforehand */
#endif
		return 1;
	}
	_printf(_("Resolving %s...\n"),argv[1]);
	fflush(stdout); /* Nick, we don't have sesflush() anymore */
	if((ping.target = resolve(argv[1])) == 0){
		_printf(_("unknown\n"));
/*RPB*/
		/* Actually, freesession() should do a complete cleanup
		 * NICK NOTE: COULD DO SOMETHING LIKE nrcmd.c BEFOREHAND:
		 *	sp->network = fdopen(s,"r+t");
		 * IT WOULD WASTE BUFFER SPACE, BUT ENSURES s GETS CLOSED
		 */
		close(s);
/*RPB*/
#ifdef JOB_CONTROL
		freesession(sp, 1); /* does keywait(NULL, 1); beforehand */
#endif
		return 1;
	}
	_printf(_("Pinging %s\n"),inet_ntoa(ping.target));
	fflush(stdout); /* Nick, we don't have sesflush() anymore */
#ifdef JOB_CONTROL
	sp->cb.p = &ping;
#endif
	if(argc > 2)
		ping.len = atoi(argv[2]);

	if(argc > 3)
		ping.interval = atol(argv[3]);

	/* Optionally ping a range of IP addresses */
	if(argc > 4)
		ping.incflag = 1;

	if(ping.interval != 0){
#ifdef JOB_CONTROL
		sp->proc1 = process_thread_create(daemon_process_p, "ping tx",300,pingtx,s,&ping,NULL,0);
#else
		proc1 = process_thread_create(daemon_process_p, "ping tx",300,pingtx,s,&ping,NULL,0);
#endif
	} else {
		/* One shot ping; let echo_proc hook handle response.
		 * An ID of MAXINT16 will not be confused with a legal socket
		 * number, which is used to identify repeated pings
		 */
		pingem(s,ping.target,0,MAXINT16,ping.len);
		close(s);
#ifdef JOB_CONTROL
		freesession(sp, 0); /* doesn't keywait(NULL, 1); beforehand */
#endif
		return 0;
	}
#if 0 /* temporarily out, due to Command / Lastcurr conflict (telnet server) */
	sp->inproc = pingproc;
#endif
	/* Now collect the replies */
	pinghdr(/*sp,*/&ping);
	for(;;){
		fromlen = sizeof(from);
#if 1 /* slower but eventually allows ping to be moved out of the kernel */
		bp = alloc_mbuf(ICMPLEN + sizeof(timestamp));
 if (bp == NULL)
  {
  _printf(_("Not enough memory\n"));
  break;
  }
		if ((bp->cnt = recvfrom(s,
				bp->data, ICMPLEN + sizeof(timestamp), 0,
				(struct sockaddr *)&from, &fromlen)) <= 0)
			{
			free_mbuf(&bp);
			break;
			}
#else
		if(recv_mbuf(s,&bp,0,(struct sockaddr *)&from,&fromlen) == -1)
			break;
#endif
		ntohicmp(&icmp,&bp);
		if(icmp.type != ICMP_ECHO_REPLY || icmp.args.echo.id != s){
			/* Ignore other people's responses */
			free_p(&bp);
			continue;
		}
		/* Get stamp */
		if(pullup(&bp,&timestamp,sizeof(timestamp))
		 != sizeof(timestamp)){
			/* The timestamp is missing! */
			free_p(&bp);	/* Probably not necessary */
			continue;
		}
		free_p(&bp);

		ping.responses++;

		/* Compute round trip time, update smoothed estimates */
		rtt = msclock() - timestamp;
		abserr = (rtt > ping.srtt) ? (rtt-ping.srtt) : (ping.srtt-rtt);

		if(ping.responses == 1){
			/* First response, base entire SRTT on it */
			ping.srtt = rtt;
			ping.mdev = 0;
			ping.maxrtt = ping.minrtt = rtt;
		} else {
			ping.srtt = (7*ping.srtt + rtt + 4) >> 3;
			ping.mdev = (3*ping.mdev + abserr + 2) >> 2;
			if(rtt > ping.maxrtt)
				ping.maxrtt = rtt;
			if(rtt < ping.minrtt)
				ping.minrtt = rtt;
		}
		if((ping.responses % 20) == 0)
			pinghdr(/*sp,*/&ping);
		_printf(_("%10lu%10lu%5lu%8lu%8lu%8lu%8lu%8lu\n"),
		 ping.sent,ping.responses,
		 (ping.responses*100 + ping.sent/2)/ping.sent,
		 rtt,ping.srtt,ping.mdev,ping.maxrtt,ping.minrtt);
		fflush(stdout); /* Nick, we don't have sesflush() anymore */
	}
#ifdef JOB_CONTROL
	if(sp->proc1 != NULL){
		killproc(sp->proc1);
		sp->proc1 = NULL;
	}
#else
	if (proc1)
		{
		killproc(proc1);
		}
#endif
/*RPB*/
	/* Actually, freesession() should do a complete cleanup
	 * NICK NOTE: COULD DO SOMETHING LIKE nrcmd.c BEFOREHAND:
	 *	sp->network = fdopen(s,"r+t");
	 * IT WOULD WASTE BUFFER SPACE, BUT ENSURES s GETS CLOSED
	 */
/*RPB*/
	close(s);
#ifdef JOB_CONTROL
	freesession(sp, 1); /* does keywait(NULL, 1); beforehand */
#endif
	return 0;
}

#ifdef JOB_CONTROL
static int
keychar(c)
int c;
{
	if(c != CTLC)
		return 1;	/* Ignore all but ^C */

#if 1
	_printf(_("^C\n"));
	fflush(stdout);
	alert(my_thread_p, EABORT);
#else
	_fprintf(my_session_p->output, _("^C\n"));
	alert(my_session_p->proc,EABORT);
#endif
	return 0;
}
#endif

static void
pinghdr(/*sp,*/ping)
/*struct session_s *sp;*/
struct ping *ping;
{
	_printf(_("      sent      rcvd    %     rtt     avg    mdev     max     min\n"));
}

void
echo_proc(
int32 source,
int32 dest,
struct icmp *icmp,
struct mbuf_s **bpp
){
	int32 timestamp,rtt;

	if(Icmp_echo && icmp->args.echo.id == MAXINT16
	 && pullup(bpp,&timestamp,sizeof(timestamp))
	 == sizeof(timestamp)){
		/* Compute round trip time */
		rtt = msclock() - timestamp;
		_printf(_("%s: rtt %lu\n"),inet_ntoa(source),rtt);
#ifdef ZILOG /* temporary, later I want daemons to share stdout with main */
 fflush(stdout);
#endif
	}
	free_p(bpp);
#if 0 /* Nick is trying this out def ZILOG */
	ksignal((void *)64, 0); /* wake up all instances of doping() */
#endif
}
/* Ping transmit process. Runs until killed */
static void
pingtx(s,ping1,p)
int s;		/* Socket to use */
void *ping1;
void *p;
{
	struct ping *ping;

	ping = (struct ping *)ping1;
	if(ping->incflag){
		for(;;){
			pingem(s,ping->target++,0,MAXINT16,ping->len);
			ping->sent++;
			ppause(ping->interval);
		}
	} else {
		for(;;){
			pingem(s,ping->target,(uint16)ping->sent++,(uint16)s,ping->len);
			ppause(ping->interval);
		}
	}
}


/* Send ICMP Echo Request packet */
static int
pingem(s,target,seq,id,len)
int s;		/* Raw socket on which to send ping */
int32 target;	/* Site to be pinged */
uint16 seq;	/* ICMP Echo Request sequence number */
uint16 id;	/* ICMP Echo Request ID */
uint16 len;	/* Length of optional data field */
{
	struct mbuf_s *data;
	struct icmp icmp;
	struct sockaddr_in to;
	int32 clock;
	int i;
	uint8 *cp;
#if 1 /* slower but eventually allows ping to be moved out of the kernel */
	struct mbuf_s *bp;
#endif

	clock = msclock();
	data = alloc_mbuf((uint16)(len+sizeof(clock)));
 if (data == NULL)
  {
  _printf(_("Not enough memory\n"));
  return -1;
  }
	data->cnt = len+sizeof(clock);
#define	counter	1
#ifdef	rnd
	/* Set data field to random pattern */
	cp = data->data+sizeof(clock);
	while(len-- != 0)
		*cp++ = rand();
#else
#ifdef	alternate
	/* Set optional data field, if any, to all 55's */
	if(len != 0)
		memset(data->data+sizeof(clock),0x55,len);
#else
#ifdef	counter
	cp = data->data+sizeof(clock);
	i = 0;
	while(len-- != 0)
		*cp++ = i++;
#endif
#endif
#endif
	/* Insert timestamp and build ICMP header */
	memcpy(data->data,&clock,sizeof(clock));
	icmpOutEchos++;
	icmpOutMsgs++;
	icmp.type = ICMP_ECHO;
	icmp.code = 0;
	icmp.args.echo.seq = seq;
	icmp.args.echo.id = id;
	htonicmp(&icmp,&data);
	to.sin_family = AF_INET;
	to.sin_addr.s_addr = target;

#if 1 /* slower but eventually allows ping to be moved out of the kernel */
	i = len_p(data);
	bp = alloc_mbuf(i);
 if (bp == NULL)
  {
  free_p(&data);
  _printf(_("Not enough memory\n"));
  return -1;
  }
	bp->cnt = pullup(&data, bp->data, (uint16)i);
	free_p(&data);

	sendto(s, bp->data, bp->cnt, 0, (struct sockaddr *)&to, sizeof(to));
	free_mbuf(&bp);
#else
	send_mbuf(s,&data,0,(struct sockaddr *)&to,sizeof(to));
#endif
	return 0;
}

#if 0 /* temporarily out, due to Command / Lastcurr conflict (telnet server) */
static int
pingproc(c)
int c;
{
	struct ping *p;
	struct session_s *sp;

	sp = my_session_p;
	p = (struct ping *)sp->cb.p;

	if(p->s == -1)
		return 1;	/* Shutting down, let keywait have it */
	switch(c){
	case '\033':
	case 'Q':
	case 'q':
	case 3:	/* ctl-c - quit */
		alert(sp->proc,EABORT);
		if(my_session_p->proc1 != NULL){
			killproc(sp->proc1);
			sp->proc1 = NULL;
		}
		shutdown(p->s,2);
		p->s = -1;
		break;
	case ' ':	/* Toggle pinger */
		if(sp->proc1 != NULL){
			killproc(sp->proc1);
			sp->proc1 = NULL;
			_printf(_("Pinging suspended, %lu sent\n"), p->sent);
		} else {
			p->sent = p->responses = 0;
			sp->proc1 = process_thread_create(daemon_process_p, "ping tx",300,pingtx,p->s,p,NULL,0);
			_printf(_("Pinging resumed\n"));
		}
		break;
	}
	return 0;
}
#endif

