/* Internet Control Message Protocol (ICMP)
 * Copyright 1991 Phil Karn, KA9Q
 *
 * Apr 04	RPB	Po-ed most of the remaining strings
 */
#include <stdio.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include "main/iface.h"
#include "internet/ip.h"
#include "internet/icmp.h"
#include "internet/ping.h"
#include "internet/netuser.h"
#include "main/smsg.h"
#include <libintl.h>
#include "po/messages.h"

struct mib_entry Icmp_mib[ICMP_MIB_MAX] = {
	N_(""),				0,
	N_("icmpInMsgs"),		0,
	N_("icmpInErrors"),		0,
	N_("icmpInDestUnreachs"),	0,
	N_("icmpInTimeExcds"),		0,
	N_("icmpInParmProbs"),		0,
	N_("icmpInSrcQuenchs"),		0,
	N_("icmpInRedirects"),		0,
	N_("icmpInEchos"),		0,
	N_("icmpInEchoReps"),		0,
	N_("icmpInTimestamps"),		0,
	N_("icmpInTimestampReps"),	0,
	N_("icmpInAddrMasks"),		0,
	N_("icmpInAddrMaskReps"),	0,
	N_("icmpOutMsgs"),		0,
	N_("icmpOutErrors"),		0,
	N_("icmpOutDestUnreachs"),	0,
	N_("icmpOutTimeExcds"),		0,
	N_("icmpOutParmProbs"),		0,
	N_("icmpOutSrcQuenchs"),	0,
	N_("icmpOutRedirects"),		0,
	N_("icmpOutEchos"),		0,
	N_("icmpOutEchoReps"),		0,
	N_("icmpOutTimestamps"),	0,
	N_("icmpOutTimestampReps"),	0,
	N_("icmpOutAddrMasks"),		0,
	N_("icmpOutAddrMaskReps"),	0,
};

/* Process an incoming ICMP packet */
void
icmp_input(
struct iface_s *iface,	/* Incoming interface (ignored) */
struct ip *ip,		/* Pointer to decoded IP header structure */
struct mbuf_s **bpp,	/* Pointer to ICMP message */
int rxbroadcast,
int32 said
){
	struct icmplink *ipp;
	struct icmp icmp;	/* ICMP header */
	struct ip oip;		/* Offending datagram header */
	uint16 type;		/* Type of ICMP message */
	uint16 length;
	char *duptext;		/*RPB*/

	icmpInMsgs++;
	if(rxbroadcast){
		/* Broadcast ICMP packets are to be IGNORED !! */
		icmpInErrors++;
		free_p(bpp);
		return;
	}
	length = ip->length - IPLEN - ip->optlen;
	if(cksum(NULL,*bpp,length) != 0){
		/* Bad ICMP checksum; discard */
		icmpInErrors++;
		free_p(bpp);
		return;
	}
	ntohicmp(&icmp,bpp);

	/* Process the message. Some messages are passed up to the protocol
	 * module for handling, others are handled here.
	 */
	type = icmp.type;

	switch(type){
	case ICMP_TIME_EXCEED:	/* Time-to-live Exceeded */
	case ICMP_DEST_UNREACH:	/* Destination Unreachable */
	case ICMP_QUENCH:	/* Source Quench */
	case ICMP_IPSP:		/* Bad security packet */
		switch(type){
		case ICMP_TIME_EXCEED:	/* Time-to-live Exceeded */
			icmpInTimeExcds++;
			break;
		case ICMP_DEST_UNREACH:	/* Destination Unreachable */
			icmpInDestUnreachs++;
			break;
		case ICMP_QUENCH:	/* Source Quench */
			icmpInSrcQuenchs++;
			break;
		}
		ntohip(&oip,bpp);	/* Extract offending IP header */
		if(Icmp_trace){
			_printf(_("ICMP from %s:"),inet_ntoa(ip->source));
			/*RPB*/
			duptext = smsg(Icmptypes,ICMP_TYPES,type);
			_printf(_(" dest %s %s"),inet_ntoa(oip.dest),duptext);
			free(duptext);
			/*RPB*/
			switch(type){
			case ICMP_TIME_EXCEED:
				/*RPB*/
				duptext = smsg(Exceed,NEXCEED,icmp.code);
				_printf(_(" %s\n"), duptext);
				free(duptext);
				/*RPB*/
				break;
			case ICMP_DEST_UNREACH:
				/*RPB*/
				duptext = smsg(Unreach,NUNREACH,icmp.code);
				_printf(_(" %s\n"), duptext);
				free(duptext);
				/*RPB*/
				break;
			case ICMP_IPSP:
				/*RPB*/
				duptext = smsg(Said_icmp,NIPSP,icmp.code);
				_printf(_(" %s\n"), duptext);
				free(duptext);
				/*RPB*/
				break;
			default:
				_printf(_(" %u\n"),icmp.code);
				break;
			}
		}
		for(ipp = Icmplink;ipp->funct != NULL;ipp++)
			if(ipp->proto == oip.protocol)
				break;
		if(ipp->funct != NULL){
			(*ipp->funct)(ip->source,oip.source,oip.dest,icmp.type,
			 icmp.code,bpp);
		}
		break;
	case ICMP_ECHO:		/* Echo Request */
		/* Change type to ECHO_REPLY, recompute checksum,
		 * and return datagram.
		 */
		icmpInEchos++;
		icmp.type = ICMP_ECHO_REPLY;
		htonicmp(&icmp,bpp);
		icmpOutEchoReps++;

		ip_send(ip->dest,ip->source,ICMP_PTCL,ip->tos,0,bpp,length,0,0);
		return;
	case ICMP_REDIRECT:	/* Redirect */
		icmpInRedirects++;
		ntohip(&oip,bpp);	/* Extract offending IP header */
		if(Icmp_trace){
			_printf(_("ICMP from %s:"),inet_ntoa(ip->source));
			/*RPB*/
			duptext = smsg(Icmptypes,ICMP_TYPES,type);
			_printf(_(" dest %s %s"),inet_ntoa(oip.dest), duptext);
			free(duptext);
			/*RPB*/
			_printf(_(" new gateway %s\n"),inet_ntoa(icmp.args.address));
		}
		break;
	case ICMP_PARAM_PROB:	/* Parameter Problem */
		icmpInParmProbs++;
		break;
	case ICMP_ECHO_REPLY:	/* Echo Reply */
		icmpInEchoReps++;
		echo_proc(ip->source,ip->dest,&icmp,bpp);
		break;
	case ICMP_TIMESTAMP:	/* Timestamp */
		icmpInTimestamps++;
		break;
	case ICMP_TIME_REPLY:	/* Timestamp Reply */
		icmpInTimestampReps++;
		break;
	case ICMP_INFO_RQST:	/* Information Request */
		break;
	case ICMP_INFO_REPLY:	/* Information Reply */
		break;
	}
	free_p(bpp);
}
/* Return an ICMP response to the sender of a datagram.
 * Unlike most routines, the callER frees the mbuf.
 */
int
icmp_output(
struct ip *ip,		/* Header of offending datagram */
struct mbuf_s *data,	/* Data portion of datagram - FREED BY CALLER */
uint8 type,		/* Codes to send */
uint8 code,
union icmp_args *args
){
	struct mbuf_s *bp;
	struct icmp icmp;	/* ICMP protocol header */
	uint16 dlen;		/* Length of data portion of offending pkt */
	uint16 length;		/* Total length of reply */

	if(ip == NULL)
		return -1;
	if(ip->protocol == ICMP_PTCL){
		/* Peek at type field of ICMP header to see if it's safe to
		 * return an ICMP message
		 */
		switch(data->data[0]){
		case ICMP_ECHO_REPLY:
		case ICMP_ECHO:
		case ICMP_TIMESTAMP:
		case ICMP_TIME_REPLY:
		case ICMP_INFO_RQST:
		case ICMP_INFO_REPLY:
			break;	/* These are all safe */
		default:
			/* Never send an ICMP error message about another
			 * ICMP error message!
			 */
			return -1;
		}
	}
	/* Compute amount of original datagram to return.
	 * We return the original IP header, and up to 8 bytes past that.
	 */
	dlen = min(8,len_p(data));
	length = dlen + ICMPLEN + IPLEN + ip->optlen;
	/* Take excerpt from data portion */
	if(data != NULL && dup_p(&bp,data,0,dlen) == 0)
		return -1;	/* The caller will free data */

	/* Recreate and tack on offending IP header */
	htonip(ip,&bp,IP_CS_NEW);
	icmp.type = type;
	icmp.code = code;
	icmp.args.unused = 0;
	switch(icmp.type){
	case ICMP_PARAM_PROB:
		icmpOutParmProbs++;
		icmp.args.pointer = args->pointer;
		break;
	case ICMP_REDIRECT:
		icmpOutRedirects++;
		icmp.args.address = args->address;
		break;
	case ICMP_ECHO:
		icmpOutEchos++;
		break;
	case ICMP_ECHO_REPLY:
		icmpOutEchoReps++;
		break;
	case ICMP_INFO_RQST:
		break;
	case ICMP_INFO_REPLY:
		break;
	case ICMP_TIMESTAMP:
		icmpOutTimestamps++;
		break;
	case ICMP_TIME_REPLY:
		icmpOutTimestampReps++;
		icmp.args.echo.id = args->echo.id;
		icmp.args.echo.seq = args->echo.seq;
		break;
	case ICMP_ADDR_MASK:
		icmpOutAddrMasks++;
		break;
	case ICMP_ADDR_MASK_REPLY:
		icmpOutAddrMaskReps++;
		break;
	case ICMP_DEST_UNREACH:
		if(icmp.code == ICMP_FRAG_NEEDED)
			icmp.args.mtu = args->mtu;
		icmpOutDestUnreachs++;
		break;
	case ICMP_TIME_EXCEED:
		icmpOutTimeExcds++;
		break;
	case ICMP_QUENCH:
		icmpOutSrcQuenchs++;
		break;
	}
	icmpOutMsgs++;
	/* Now stick on the ICMP header */
	htonicmp(&icmp,&bp);
	return ip_send(INADDR_ANY,ip->source,ICMP_PTCL,ip->tos,0,&bp,length,0,0);
}
