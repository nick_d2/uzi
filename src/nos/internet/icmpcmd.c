/* ICMP-related user commands
 * Copyright 1991 Phil Karn, KA9Q
 *
 * Apr 06	RPB	Changes needed for intl' of strings
 */
#include <stdio.h>
#include "nos/global.h"
#include "internet/icmp.h"
#include "internet/ip.h"
#include "main/mbuf.h"
#include "internet/netuser.h"
#include <libintl.h> /*RPB*/
#include "internet/internet.h"
#include "main/timer.h"
#include "socket/socket.h"
#include "kernel/thread.h"
#include "main/cmdparse.h"
#include "main/commands.h"
#include "po/messages.h"

static int doicmpec(int argc, char *argv[],void *p);
static int doicmpstat(int argc, char *argv[],void *p);
static int doicmptr(int argc, char *argv[],void *p);

static struct cmds Icmpcmds[] = {
	N_("echo"),	doicmpec,	0, 0, NULL,
	N_("status"),	doicmpstat,	0, 0, NULL,
	N_("trace"),	doicmptr,	0, 0, NULL,
	NULL
};

int Icmp_trace;
int Icmp_echo = 1;

int
doicmp(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return subcmd(Icmpcmds,argc,argv,p);
}

static int
doicmpstat(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register int i;
	int lim;

	/* Note that the ICMP variables are shown in column order, because
	 * that lines up the In and Out variables on the same line
	 */
	lim = NUMICMPMIB/2;
	for(i=1;i<=lim;i++){
		/*RPB*/
		char *duptext1 = NULL,
		     *duptext2 = NULL;

		if ((duptext1 = _strdup(Icmp_mib[i].name)) != NULL)
			if ((duptext2 = _strdup(Icmp_mib[i+lim].name)) != NULL)
			{
				_printf(_("(%2u)%-20s%10lu"),i,duptext1,
				        Icmp_mib[i].value.integer);
				_printf(_("     (%2u)%-20s%10lu\n"),i+lim,
				        duptext2,Icmp_mib[i+lim].value.integer);
			}
			else
				__getfail(Icmp_mib[i+lim].name);
		else
			__getfail(Icmp_mib[i].name);

		free(duptext1);
		free(duptext2);
		/*RPB*/
	}
	return 0;
}
static int
doicmptr(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return setbool(&Icmp_trace, _("ICMP tracing"), argc, argv);
}
static int
doicmpec(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return setbool(&Icmp_echo, _("ICMP echo response accept"), argc, argv);
}
