#undef SIM
/* Upper half of IP, consisting of send/receive primitives, including
 * fragment reassembly, for higher level protocols.
 * Not needed when running as a standalone gateway.
 * Copyright 1991 Phil Karn, KA9Q
 *
 * Apr 06	RPB	Changes needed for intl' of strings
 */
#include "nos/global.h"
#include "main/mbuf.h"
#include "main/timer.h"
#include "internet/internet.h"
#include "internet/netuser.h"
#include "main/iface.h"
/*#include "x86/pktdrvr.h"*/
#include "internet/ip.h"
#include "internet/icmp.h"
#include "kernel/dprintf.h"
#include <libintl.h>
#include "po/messages.h"


#ifdef MODULE
#define STATIC
#else
#define STATIC static
#define MODULE_Ip_mib
#define MODULE_Reasmq
#define MODULE_Id_cntr
#define MODULE_Rawip
#define MODULE_Ip_trace
#define MODULE_ip_send
#define MODULE_ip_recv
#define MODULE_ipip_recv
#define MODULE_fraghandle
#define MODULE_raw_ip
#define MODULE_del_ip
#define MODULE_lookup_reasm
#define MODULE_creat_reasm
#define MODULE_free_reasm
#define MODULE_ip_timeout
#define MODULE_newfrag
#define MODULE_freefrag
#define MODULE_ip_garbage
#define MODULE_ttldec
#define MODULE_rquench
#endif

STATIC int fraghandle(struct ip *ip,struct mbuf_s **bpp);
STATIC void ip_timeout(void *arg);
STATIC void free_reasm(struct reasm *rp);
STATIC void freefrag(struct frag *fp);
STATIC struct reasm *lookup_reasm(struct ip *ip);
STATIC struct reasm *creat_reasm(struct ip *ip);
STATIC struct frag *newfrag(uint16 offset,uint16 last,struct mbuf_s **bpp);
void ttldec(struct iface_s *ifp);

#ifdef MODULE_Ip_mib
struct mib_entry Ip_mib[IP_MIB_MAX] = {
/*RPB*/
	N_(""),				0,
	N_("ipForwarding"),		1,
	N_("ipDefaultTTL"),		MAXTTL,
	N_("ipInReceives"),		0,
	N_("ipInHdrErrors"),		0,
	N_("ipInAddrErrors"),		0,
	N_("ipForwDatagrams"),		0,
	N_("ipInUnknownProtos"),	0,
	N_("ipInDiscards"),		0,
	N_("ipInDelivers"),		0,
	N_("ipOutRequests"),		0,
	N_("ipOutDiscards"),		0,
	N_("ipOutNoRoutes"),		0,
	N_("ipReasmTimeout"),		TLB,
	N_("ipReasmReqds"),		0,
	N_("ipReasmOKs"),		0,
	N_("ipReasmFails"),		0,
	N_("ipFragOKs"),		0,
	N_("ipFragFails"),		0,
	N_("ipFragCreates"),		0,
/*RPB*/
};
#endif

#ifdef MODULE_Reasmq
struct reasm *Reasmq;
#endif
#ifdef MODULE_Id_cntr
uint16 Id_cntr = 0;	/* Datagram serial number */
#endif
#ifdef MODULE_Rawip
/* formerly Raw_ip but this conflicts with raw_ip() on a Win32 filesystem */
/* Nick STATIC */ struct raw_ip *Rawip;
#endif
#ifdef MODULE_Ip_trace
int Ip_trace = 0;
#endif

#define	INSERT	0
#define	APPEND	1
#define	PREPEND	2

/* Send an IP datagram. Modeled after the example interface on p 32 of
 * RFC 791
 */
#ifdef MODULE_ip_send
int
ip_send(
int32 source,			/* source address */
int32 dest,			/* Destination address */
char protocol,			/* Protocol */
char tos,			/* Type of service */
char ttl,			/* Time-to-live */
struct mbuf_s **bpp,		/* Data portion of datagram */
uint16 length,			/* Optional length of data portion */
uint16 id,			/* Optional identification */
char df				/* Don't-fragment flag */
){
	struct ip ip;			/* IP header */

	ipOutRequests++;

	if(bpp == NULL)
		return -1;
	if(source == INADDR_ANY)
		source = locaddr(dest);
	if(length == 0 && *bpp != NULL)
		length = len_p(*bpp);
	if(id == 0)
		id = Id_cntr++;
	if(ttl == 0)
		ttl = ipDefaultTTL;

	/* Fill in IP header */
	ip.version = IPVERSION;
	ip.tos = tos;
	ip.length = IPLEN + length;
	ip.id = id;
	ip.offset = 0;
	ip.flags.mf = 0;
	ip.flags.df = df;
	ip.flags.congest = 0;
	ip.ttl = ttl;
	ip.protocol = protocol;
	ip.source = source;
	ip.dest = dest;
	ip.optlen = 0;
	if(Ip_trace)
		dumpip(NULL,&ip,*bpp,0);

	htonip(&ip,bpp,IP_CS_NEW);
	if(ismyaddr(ip.dest)){
		/* Pretend it has been sent by the loopback interface before
		 * it appears in the receive queue
		 */
#ifdef	SIM
		net_sim(bpp);
#else
		net_route(&Loopback,bpp);
#endif
		Loopback.ipsndcnt++;
		Loopback.rawsndcnt++;
		Loopback.lastsent = secclock();
	} else
		net_route(NULL,bpp);
	return 0;
}
#endif

/* Reassemble incoming IP fragments and dispatch completed datagrams
 * to the proper transport module
 */

#ifdef MODULE_ip_recv
void
ip_recv(
struct iface_s *iface,	/* Incoming interface */
struct ip *ip,		/* Extracted IP header */
struct mbuf_s **bpp,	/* Data portion */
int rxbroadcast,	/* True if received on subnet broadcast address */
int32 spi		/* Security association, if any */
){
	/* Function to call with completed datagram */
	register struct raw_ip *rp;
	struct mbuf_s *bp1;
	int rxcnt = 0;
	register struct iplink *ipp;

	/* If we have a complete packet, call the next layer
	 * to handle the result. Note that fraghandle passes back
	 * a length field that does NOT include the IP header
	 */
	if(bpp == NULL || fraghandle(ip,bpp) == -1)
		return;		/* Not done yet */

	/* Trim data segment if necessary. */
	trim_mbuf(bpp,ip->length - (IPLEN + ip->optlen));

	ipInDelivers++;
	if(Ip_trace)
		dumpip(iface,ip,*bpp,spi);

	for(rp = Rawip;rp != NULL;rp = rp->next){
		if(rp->protocol != ip->protocol)
			continue;
		rxcnt++;
		/* Duplicate the data portion, and put the header back on */
		dup_p(&bp1,*bpp,0,len_p(*bpp));
		if(bp1 != NULL){
			htonip(ip,&bp1,IP_CS_OLD);
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
			if (rp->rcvq.cnt < RECEIVE_ENTRIES)
				{
				*rp->rcvq.wp++ = bp1;
				if (rp->rcvq.wp >=
						rp->rcvq.buf + RECEIVE_ENTRIES)
					{
					rp->rcvq.wp = rp->rcvq.buf;
					}
				rp->rcvq.cnt++;

				/* cast shouldn't be needed */
				/*ksignal((void *)&rp->rcvq.cnt, 0);*/
				if (rp->rcvq.hiwat < rp->rcvq.cnt)
					{
					rp->rcvq.hiwat = rp->rcvq.cnt;
					}

				bp1 = NULL;
				}
			else
				{
				rp->rcvq.overrun++;

				/* should send a quench message here...??? */
				free_p(&bp1);
#if DEBUG >= 1
 dprintf(1, "ip_recv() drop\n");
#endif
				}
#else
			enqueue(&rp->rcvq,&bp1);
#endif
			if(rp->r_upcall != NULL)
				(*rp->r_upcall)(rp);
		} /*else {
			free_p(&bp1);
		}*/
	}

	/* Look it up in the transport protocol table */
	for(ipp = Iplink;ipp->funct != NULL;ipp++){
		if(ipp->proto == ip->protocol)
			break;
	}
	if(ipp->funct != NULL){
		/* Found, call transport protocol */
		(*ipp->funct)(iface,ip,bpp,rxbroadcast,spi);
	} else {
		/* Not found */
		if(rxcnt == 0){
			/* Send an ICMP Protocol Unknown response... */
			ipInUnknownProtos++;
			/* ...unless it's a broadcast */
			if(!rxbroadcast){
				icmp_output(ip,*bpp,ICMP_DEST_UNREACH,
				 ICMP_PROT_UNREACH,NULL);
			}
		}
		free_p(bpp);
	}
}
#endif

/* Handle IP packets encapsulated inside IP */
#ifdef MODULE_ipip_recv
void
ipip_recv(
struct iface_s *iface,	/* Incoming interface */
struct ip *ip,		/* Extracted IP header */
struct mbuf_s **bpp,	/* Data portion */
int rxbroadcast,	/* True if received on subnet broadcast address */
int32 spi
){
	net_route(&Encap,bpp);
}
#endif

/* Process IP datagram fragments
 * If datagram is complete, return its length (MINUS header);
 * otherwise return -1
 */
#ifdef MODULE_fraghandle
STATIC int
fraghandle(
struct ip *ip,		/* IP header, host byte order */
struct mbuf_s **bpp	/* The fragment itself */
){
	register struct reasm *rp; /* Pointer to reassembly descriptor */
	struct frag *lastfrag,*nextfrag,*tfp;
	struct mbuf_s *tbp;
	uint16 i;
	uint16 last;		/* Index of first byte beyond fragment */

	last = ip->offset + ip->length - (IPLEN + ip->optlen);

	rp = lookup_reasm(ip);
	if(ip->offset == 0 && !ip->flags.mf){
		/* Complete datagram received. Discard any earlier fragments */
		if(rp != NULL){
			free_reasm(rp);
			ipReasmOKs++;
		}
		return ip->length;
	}
	ipReasmReqds++;
	if(rp == NULL){
		/* First fragment; create new reassembly descriptor */
		if((rp = creat_reasm(ip)) == NULL){
			/* No space for descriptor, drop fragment */
			ipReasmFails++;
			free_p(bpp);
#if DEBUG >= 1
 dprintf(1, "fraghandle() drop\n");
#endif
			return -1;
		}
	}
	/* Keep restarting timer as long as we keep getting fragments */
	stop_timer(&rp->timer);
	start_timer(&rp->timer);

	/* If this is the last fragment, we now know how long the
	 * entire datagram is; record it
	 */
	if(!ip->flags.mf)
		rp->length = last;

	/* Set nextfrag to the first fragment which begins after us,
	 * and lastfrag to the last fragment which begins before us
	 */
	lastfrag = NULL;
	for(nextfrag = rp->fraglist;nextfrag != NULL;nextfrag = nextfrag->next){
		if(nextfrag->offset > ip->offset)
			break;
		lastfrag = nextfrag;
	}
	/* Check for overlap with preceeding fragment */
	if(lastfrag != NULL  && ip->offset < lastfrag->last){
		/* Strip overlap from new fragment */
		i = lastfrag->last - ip->offset;
		pullup(bpp,NULL,i);
		if(*bpp == NULL)
			return -1;	/* Nothing left */
		ip->offset += i;
	}
	/* Look for overlap with succeeding segments */
	for(; nextfrag != NULL; nextfrag = tfp){
		tfp = nextfrag->next;	/* save in case we delete fp */

		if(nextfrag->offset >= last)
			break;	/* Past our end */
		/* Trim the front of this entry; if nothing is
		 * left, remove it.
		 */
		i = last - nextfrag->offset;
		pullup(&nextfrag->buf,NULL,i);
		if(nextfrag->buf == NULL){
			/* superseded; delete from list */
			if(nextfrag->prev != NULL)
				nextfrag->prev->next = nextfrag->next;
			else
				rp->fraglist = nextfrag->next;
			if(tfp->next != NULL)
				nextfrag->next->prev = nextfrag->prev;
			freefrag(nextfrag);
		} else
			nextfrag->offset = last;
	}
	/* Lastfrag now points, as before, to the fragment before us;
	 * nextfrag points at the next fragment. Check to see if we can
	 * join to either or both fragments.
	 */
	i = INSERT;
	if(lastfrag != NULL && lastfrag->last == ip->offset)
		i |= APPEND;
	if(nextfrag != NULL && nextfrag->offset == last)
		i |= PREPEND;
	switch(i){
	case INSERT:	/* Insert new desc between lastfrag and nextfrag */
		tfp = newfrag(ip->offset,last,bpp);
		tfp->prev = lastfrag;
		tfp->next = nextfrag;
		if(lastfrag != NULL)
			lastfrag->next = tfp;	/* Middle of list */
		else
			rp->fraglist = tfp;	/* First on list */
		if(nextfrag != NULL)
			nextfrag->prev = tfp;
		break;
	case APPEND:	/* Append to lastfrag */
		append(&lastfrag->buf,bpp);
		lastfrag->last = last;	/* Extend forward */
		break;
	case PREPEND:	/* Prepend to nextfrag */
		tbp = nextfrag->buf;
		nextfrag->buf = *bpp;
		bpp = NULL;
		append(&nextfrag->buf,&tbp);
		nextfrag->offset = ip->offset;	/* Extend backward */
		break;
	case (APPEND|PREPEND):
		/* Consolidate by appending this fragment and nextfrag
		 * to lastfrag and removing the nextfrag descriptor
		 */
		append(&lastfrag->buf,bpp);
		append(&lastfrag->buf,&nextfrag->buf);
		nextfrag->buf = NULL;
		lastfrag->last = nextfrag->last;

		/* Finally unlink and delete the now unneeded nextfrag */
		lastfrag->next = nextfrag->next;
		if(nextfrag->next != NULL)
			nextfrag->next->prev = lastfrag;
		freefrag(nextfrag);
		break;
	}
	if(rp->fraglist->offset == 0 && rp->fraglist->next == NULL
		&& rp->length != 0){

		/* We've gotten a complete datagram, so extract it from the
		 * reassembly buffer and pass it on.
		 */
		*bpp = rp->fraglist->buf;
		rp->fraglist->buf = NULL;
		/* Tell IP the entire length */
		ip->length = rp->length + (IPLEN + ip->optlen);
		free_reasm(rp);
		ipReasmOKs++;
		ip->offset = 0;
		ip->flags.mf = 0;
		return ip->length;
	} else
		return -1;
}
#endif

/* Arrange for receipt of raw IP datagrams */
#ifdef MODULE_raw_ip
struct raw_ip *
raw_ip(
int protocol,
void (*r_upcall)()
){
	register struct raw_ip *rp;

	rp = (struct raw_ip *)callocw(1,sizeof(struct raw_ip));
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	rp->rcvq.rp = rp->rcvq.buf;
	rp->rcvq.wp = rp->rcvq.buf;
#endif
	rp->protocol = protocol;
	rp->r_upcall = r_upcall;
	rp->next = Rawip;
	Rawip = rp;
	return rp;
}
#endif

/* Free a raw IP descriptor */
#ifdef MODULE_del_ip
void
del_ip(
struct raw_ip *rpp
){
	struct raw_ip *rplast = NULL;
	register struct raw_ip *rp;
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	struct mbuf_s **op;
	unsigned int tmp, cnt;
#endif

	/* Do sanity check on arg */
	for(rp = Rawip;rp != NULL;rplast=rp,rp = rp->next)
		if(rp == rpp)
			break;
	if(rp == NULL)
		return;	/* Doesn't exist */

	/* Unlink */
	if(rplast != NULL)
		rplast->next = rp->next;
	else
		Rawip = rp->next;

#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	/* Drop any leftover receive packets */
	op = rp->rcvq.rp;
	cnt = rp->rcvq.cnt;

	for (tmp = 0; tmp < cnt; tmp++)
		{
		free_p(op);

		op++;
		if (op >= rp->rcvq.buf + RECEIVE_ENTRIES)
			{
			op = rp->rcvq.buf;
			}
		}
#else
	/* Free resources */
	free_q(&rp->rcvq);
#endif

	free(rp);
}
#endif


#ifdef MODULE_lookup_reasm
STATIC struct reasm *
lookup_reasm(
struct ip *ip
){
	register struct reasm *rp;
	struct reasm *rplast = NULL;

	for(rp = Reasmq;rp != NULL;rplast=rp,rp = rp->next){
		if(ip->id == rp->id && ip->source == rp->source
		 && ip->dest == rp->dest && ip->protocol == rp->protocol){
			if(rplast != NULL){
				/* Move to top of list for speed */
				rplast->next = rp->next;
				rp->next = Reasmq;
				Reasmq = rp;
			}
			return rp;
		}

	}
	return NULL;
}
#endif
/* Create a reassembly descriptor,
 * put at head of reassembly list
 */

#ifdef MODULE_creat_reasm
STATIC struct reasm *
creat_reasm(
struct ip *ip
){
	register struct reasm *rp;

	if((rp = (struct reasm *)calloc(1,sizeof(struct reasm))) == NULL)
		return NULL;	/* No space for descriptor */
	rp->source = ip->source;
	rp->dest = ip->dest;
	rp->id = ip->id;
	rp->protocol = ip->protocol;
	set_timer(&rp->timer,ipReasmTimeout * 1000L);
	rp->timer.func = ip_timeout;
	rp->timer.arg = rp;

	rp->next = Reasmq;
	Reasmq = rp;
	return rp;
}
#endif

/* Free all resources associated with a reassembly descriptor */
#ifdef MODULE_free_reasm
STATIC void
free_reasm(
struct reasm *r
){
	register struct reasm *rp;
	struct reasm *rplast = NULL;
	register struct frag *fp;

	for(rp = Reasmq;rp != NULL;rplast = rp,rp=rp->next)
		if(r == rp)
			break;
	if(rp == NULL)
		return;	/* Not on list */

	stop_timer(&rp->timer);
	/* Remove from list of reassembly descriptors */
	if(rplast != NULL)
		rplast->next = rp->next;
	else
		Reasmq = rp->next;

	/* Free any fragments on list, starting at beginning */
	while((fp = rp->fraglist) != NULL){
		rp->fraglist = fp->next;
		free_p(&fp->buf);
		free(fp);
	}
	free(rp);
}
#endif

/* Handle reassembly timeouts by deleting all reassembly resources */
#ifdef MODULE_ip_timeout
STATIC void
ip_timeout(
void *arg
){
	free_reasm((struct reasm *)arg);
	ipReasmFails++;
}
#endif
/* Create a fragment */

#ifdef MODULE_newfrag
STATIC struct frag *
newfrag(
uint16 offset,
uint16 last,
struct mbuf_s **bpp
){
	struct frag *fp;

	if((fp = (struct frag *)calloc(1,sizeof(struct frag))) == NULL){
		/* Drop fragment */
		free_p(bpp);
#if DEBUG >= 1
 dprintf(1, "newfrag() drop\n");
#endif
		return NULL;
	}
	fp->buf = *bpp;
	*bpp = NULL;
	fp->offset = offset;
	fp->last = last;
	return fp;
}
#endif
/* Delete a fragment, return next one on queue */

#ifdef MODULE_freefrag
STATIC void
freefrag(
struct frag *fp
){
	free_p(&fp->buf);
	free(fp);
}
#endif

/* In red alert mode, blow away the whole reassembly queue. Otherwise crunch
 * each fragment on each reassembly descriptor
 */

#ifdef MODULE_ip_garbage
void
ip_garbage(
int red
){
	struct reasm *rp,*rp1;
	struct frag *fp;
	struct raw_ip *rwp;
	struct iface_s *ifp;
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	struct mbuf_s **op;
	unsigned int tmp, cnt;
#endif

	/* Run through the reassembly queue */
	for(rp = Reasmq;rp != NULL;rp = rp1){
		rp1 = rp->next;
		if(red){
			free_reasm(rp);
#if DEBUG >= 1
 dprintf(1, "ip_garbage() drop\n");
#endif
		} else {
			for(fp = rp->fraglist;fp != NULL;fp = fp->next){
				mbuf_crunch(&fp->buf);
			}
		}
	}

	/* Run through the raw IP queue */
	for(rwp = Rawip;rwp != NULL;rwp = rwp->next)
 {
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
		op = rwp->rcvq.rp;
		cnt = rwp->rcvq.cnt;

		for (tmp = 0; tmp < cnt; tmp++)
			{
			mbuf_crunch(op);

			op++;
			if (op >= rwp->rcvq.buf + RECEIVE_ENTRIES)
				{
				op = rwp->rcvq.buf;
				}
			}
#else
		/* NOTE: WASN'T CORRECT AS MBUF_CRUNCH EXPECTS A PACKET!!?? */
		mbuf_crunch(&rwp->rcvq);
#endif
 }

	/* Walk through interface output queues and decrement IP TTLs.
	 * Discard and return ICMP TTL exceeded messages for any that
	 * go to zero. (Some argue that this ought to be done all the
	 * time, but it would probably break a lot of machines with
	 * small IP TTL settings using amateur packet radio paths.)
	 *
	 * Also send an ICMP source quench message to one
	 * randomly chosen packet on each queue. If in red mode,
	 * also drop the packet.
	 */
	for(ifp=Ifaces;ifp != NULL;ifp = ifp->next){
		ttldec(ifp);
		rquench(ifp,red);
	}
}
#endif

/* Decrement the IP TTL field in each packet on the send queue. If
 * a TTL goes to zero, discard the packet.
 */
#ifdef MODULE_ttldec
void
ttldec(
struct iface_s *ifp
){
	struct mbuf_s *bp;
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	struct output_entry_s *rp, *wp;
	unsigned int cnt, tmp;
#else
	struct mbuf_s *bpprev,*bpnext;
	struct qhdr qhdr;
#endif
	struct ip ip;

#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	rp = ifp->outq.rp;
	wp = ifp->outq.rp;
	cnt = ifp->outq.cnt;

	tmp = 0;
	while (tmp < cnt)
		{
		bp = rp->mbuf_p;
		ntohip(&ip, &bp);

		ip.ttl--;
		if (ip.ttl)
			{
			/* Put IP header back, restore to queue */
			htonip(&ip, &bp, 0);

			wp->tos = rp->tos; /* may be same entry */
			wp->gateway = rp->gateway; /* may be same entry */
			wp->mbuf_p = bp;
#if DEBUG >= 2
 dprintf(2, "o %04x %04x = %04x\n", ifp, wp, rp);
#endif

			wp++;
			if (wp >= ifp->outq.buf + OUTPUT_ENTRIES)
				{
				wp = ifp->outq.buf;
				}
			tmp++;
			}
		else
			{
#if DEBUG >= 2
 dprintf(2, "o %04x %04x -> %02x %08lx %04x\n", ifp, rp,
   rp->tos, rp->gateway, rp->mbuf_p);
#endif
			/* Drop packet */
			icmp_output(&ip,bp,ICMP_TIME_EXCEED,0,NULL);
			free_p(&bp);
			cnt--;
#if DEBUG >= 1
 dprintf(1, "ttldec() drop\n");
#endif
			}

		rp++;
		if (rp >= ifp->outq.buf + OUTPUT_ENTRIES)
			{
			rp = ifp->outq.buf;
			}
		}

	ifp->outq.wp = wp;
	ifp->outq.cnt = cnt;
#else
	bpprev = NULL;
	for(bp = ifp->outq; bp != NULL;bpprev = bp,bp = bpnext){
		bpnext = bp->anext;
		pullup(&bp,&qhdr,sizeof(qhdr));
		ntohip(&ip,&bp);
		if(--ip.ttl == 0){
			/* Drop packet */
			icmp_output(&ip,bp,ICMP_TIME_EXCEED,0,NULL);
			if(bpprev == NULL)	/* First on queue */
				ifp->outq = bpnext;
			else
				bpprev->anext = bpnext;
			free_p(&bp);
			bp = bpprev;
			continue;
		}
		/* Put IP and queue headers back, restore to queue */
		htonip(&ip,&bp,0);
		pushdown(&bp,&qhdr,sizeof(qhdr));
		if(bpprev == NULL)	/* First on queue */
			ifp->outq = bp;
		else
			bpprev->anext = bp;
		bp->anext = bpnext;
	}
#endif
}
#endif

/* Execute random quench algorithm on an interface's output queue */
#ifdef MODULE_rquench
void
rquench(
struct iface_s *ifp,
int drop
){
	struct mbuf_s *bp;
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	struct output_entry_s *rp, *wp;
	unsigned int cnt, tmp;
#else
	struct mbuf_s *bplast;
	int i;
	struct qhdr qhdr;
#endif
	struct ip ip;
	struct mbuf_s *bpdup;

#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	cnt = ifp->outq.cnt;
	if (cnt == 0)
		{
		/* Queue is empty */
		return;
		}

	/* Select a victim */
	tmp = urandom(cnt);

	/* Compute i-th message on queue */
	rp = ifp->outq.rp + tmp;
	if (rp >= ifp->outq.buf + OUTPUT_ENTRIES)
		{
		rp -= OUTPUT_ENTRIES;
		}
	bp = rp->mbuf_p;
#if DEBUG >= 2
 dprintf(2, "o %04x %04x -> %02x %08lx %04x\n", ifp, rp,
   rp->tos, rp->gateway, bp);
#endif

	/* Send a source quench */
	if (drop == 0)
		{
		dup_p(&bpdup,bp,0,len_p(bp));
		ntohip(&ip,&bpdup);
		icmp_output(&ip,bpdup,ICMP_QUENCH,0,NULL);
		free_p(&bpdup);
		return;
		}

	/* Drop the packet */
	ntohip(&ip,&bp);
	icmp_output(&ip,bp,ICMP_QUENCH,0,NULL);
	free_p(&bp);

	/* Move remaining queue entries */
	cnt--;
	for (; tmp < cnt; tmp++)
		{
		wp = rp++;
		if (rp >= ifp->outq.buf + OUTPUT_ENTRIES)
			{
			rp = ifp->outq.buf;
			}

		wp->tos = rp->tos;
		wp->gateway = rp->gateway;
		wp->mbuf_p = rp->mbuf_p;
#if DEBUG >= 2
 dprintf(2, "o %04x %04x = %04x\n", ifp, wp, rp);
#endif
		}

	ifp->outq.wp = rp;
	ifp->outq.cnt = cnt;
#if DEBUG >= 1
 dprintf(1, "rquench() drop\n");
#endif
#else
	if((i = len_q(ifp->outq)) == 0)
		return;	/* Queue is empty */

	i = urandom(i);	/* Select a victim */

	/* Search for i-th message on queue */
	bplast = NULL;
	for(bp = ifp->outq;bp != NULL && i>0;i--,bplast=bp,bp=bp->anext)
		;
	if(bp == NULL)
		return;	/* "Can't happen" */

	/* Send a source quench */
	dup_p(&bpdup,bp,0,len_p(bp));
	pullup(&bpdup,&qhdr,sizeof(qhdr));
	ntohip(&ip,&bpdup);
	icmp_output(&ip,bpdup,ICMP_QUENCH,0,NULL);
	free_p(&bpdup);
	if(!drop)
		return;	/* All done */

	/* Drop the packet */
	if(bplast != NULL)
		bplast->anext = bp->anext;
	else
		ifp->outq = bp->anext;	/* First on list */
	free_p(&bp);
#endif
}
#endif
