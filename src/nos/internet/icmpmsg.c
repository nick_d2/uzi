/* ICMP message type tables
 * Copyright 1991 Phil Karn, KA9Q
 *
 * Apr 04	RPB	Po-ed most of the remaining strings
 */

#include "nos/global.h"
#include "internet/icmp.h"
#include <libintl.h>
#include "po/messages.h"

/* ICMP message types */
_char *Icmptypes[ICMP_TYPES] = {
	N_("Echo Reply"),
	NULL,
	NULL,
	N_("Unreachable"),
	N_("Source Quench"),
	N_("Redirect"),
	NULL,
	NULL,
	N_("Echo Request"),
	NULL,
	NULL,
	N_("Time Exceeded"),
	N_("Parameter Problem"),
	N_("Timestamp"),
	N_("Timestamp Reply"),
	N_("Information Request"),
	N_("Information Reply"),
	N_("Address mask request"),
	N_("Address mask reply"),
	N_("Security Association")
};

/* ICMP unreachable messages */
_char *Unreach[NUNREACH] = {
	N_("Network"),
	N_("Host"),
	N_("Protocol"),
	N_("Port"),
	N_("Fragmentation"),
        N_("Source route"),
	N_("Dest net unknown"),
	N_("Dest host unknown"),
	N_("Source host isolated"),
	N_("Net prohibited"),
	N_("Host prohibited"),
	N_("Net TOS"),
	N_("Host TOS"),
	N_("Administratively Prohibited")
};

/* ICMP Time exceeded messages */
_char *Exceed[NEXCEED] = {
	N_("Time-to-live"),
	N_("Fragment reassembly")
};

/* ICMP redirect messages */
_char *Redirect[NREDIRECT] = {
	N_("Network"),
	N_("Host"),
	N_("TOS & Network"),
	N_("TOS & Host")
};

/* Bad SAID messages */
_char *Said_icmp[NIPSP] = {
	N_("Unknown SAID"),
	N_("Authentication failed"),
	N_("Decryption failed")
};

