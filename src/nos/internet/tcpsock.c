/* tcpsock.c */

#include <errno.h> /* Nick */
#include "nos/global.h"
#include "internet/tcp.h"
#include "socket/opensock.h"
#include "main/main.h" /* for daemon_process_p */
#include "kernel/thread.h" /* for struct thread_s */

static void s_trcall(struct tcb *tcb,int32 cnt);
static void s_tscall(struct tcb *tcb,int old,int new);
static void s_ttcall(struct tcb *tcb,int32 cnt);
static void trdiscard(struct tcb *tcb,int32 cnt);
static void autobind(struct usock *up);

uint16 Lport = 1024;

int
so_tcp(struct usock *up,int protocol)
{
	up->type = TYPE_TCP;
	return 0;
}
int
so_tcp_listen(struct usock *up,int backlog)
{
	struct sockaddr_in *local;
	struct socket lsock;

	if(up->name == NULL)
		autobind(up);

	local = (struct sockaddr_in *)up->name;
	lsock.address = local->sin_addr.s_addr;
	lsock.port = local->sin_port;
#if 1
	up->cb.tcb = open_tcp(&lsock,NULL,
	 backlog ? TCP_SERVER:TCP_PASSIVE,0,
	s_trcall,s_ttcall,s_tscall,up->tos,up);
#else
	up->cb.tcb = open_tcp(&lsock,NULL,
	 backlog ? TCP_SERVER:TCP_PASSIVE,0,
	s_trcall,s_ttcall,s_tscall,up->tos,up->index);
#endif
	return 0;
}
int
so_tcp_conn(struct usock *up)
{
#if 0
	int s;
#endif
	struct tcb *tcb;
	struct socket lsock,fsock;
	struct sockaddr_in *local,*remote;

	if(up->name == NULL){
		autobind(up);
	}

	if(checkipaddr(up->peername,up->peernamelen) == -1){
		errno = EAFNOSUPPORT;
		return -1;
	}
#if 0
	s = up->index;
#endif
	/* Construct the TCP-style ports from the sockaddr structs */
	local = (struct sockaddr_in *)up->name;
	remote = (struct sockaddr_in *)up->peername;

	if(local->sin_addr.s_addr == INADDR_ANY)
		/* Choose a local address */
		local->sin_addr.s_addr = locaddr(remote->sin_addr.s_addr);

	lsock.address = local->sin_addr.s_addr;
	lsock.port = local->sin_port;
	fsock.address = remote->sin_addr.s_addr;
	fsock.port = remote->sin_port;

	/* Open the TCB in active mode */
#if 1
	up->cb.tcb = open_tcp(&lsock,&fsock,TCP_ACTIVE,0,
	 s_trcall,s_ttcall,s_tscall,up->tos,up);
#else
	up->cb.tcb = open_tcp(&lsock,&fsock,TCP_ACTIVE,0,
	 s_trcall,s_ttcall,s_tscall,up->tos,s);
#endif

	/* Wait for the connection to complete */
	while((tcb = up->cb.tcb) != NULL && tcb->state != TCP_ESTABLISHED){
		if(up->noblock){
			errno = EWOULDBLOCK;
			return -1;
		} else if((errno = kwait(up)) != 0){
			return -1;
		}
	}
	if(tcb == NULL){
		/* Probably got refused */
		FREE(up->peername);
		errno = ECONNREFUSED;
		return -1;
	}
	return 0;
}
int
so_tcp_recv(struct usock *up,void *buf,int cnt,/*struct mbuf_s **bpp,*/
 struct sockaddr *from,int *fromlen)
{
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
	struct mbuf_s *bp;
#else
	long cnt;
#endif
	struct tcb *tcb;

/* abyte('A'); */
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
	if (cnt == 0)
		{
		return 0; /* otherwise we'd take all the waiting data */
		}
#endif

/* abyte('B'); */
	while((tcb = up->cb.tcb) != NULL && tcb->r_upcall != trdiscard
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
	 && recv_tcp(tcb,&bp,cnt) == -1){
#else
	 && (cnt = recv_tcp(tcb,bpp,0)) == -1){
#endif
/* abyte('C'); */
		if(up->noblock){
			errno = EWOULDBLOCK;
/* abyte('D'); */
			return -1;
		} else if((errno = kwait(up)) != 0){
/* abyte('E'); */
			return -1;
		}
	}
/* abyte('F'); */
	if(tcb == NULL){
		/* Connection went away */
		errno = ENOTCONN;
/* abyte('G'); */
		return -1;
	} else if(tcb->r_upcall == trdiscard){
		/* Receive shutdown has been done */
		errno = ENOTCONN;	/* CHANGE */
/* abyte('H'); */
		return -1;
	}
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
/* abyte('I'); */
	cnt = pullup(&bp, buf, (uint16)cnt);
/* abyte('J'); */
	free_p(&bp);
#endif
/* abyte('K'); */
	return cnt;
}

int
so_tcp_send(struct usock *up,void *buf,int cnt,/*struct mbuf_s **bpp,*/
 struct sockaddr *to)
{
	struct tcb *tcb;
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
	struct mbuf_s *bp;
#else
	long cnt;
#endif

	if((tcb = up->cb.tcb) == NULL){
#if 0 /* now that we don't have to support recv_mbuf() or send_mbuf() */
		free_p(bpp);
#endif
		errno = ENOTCONN;
		return -1;
	}
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
	bp = qdata(buf, cnt);
	if (bp == NULL)
		{
		errno = ENOMEM;
		return -1;
		}
	cnt = (int)send_tcp(tcb,&bp); /* it returns a long... why? */
#else
	cnt = send_tcp(tcb,bpp);
#endif

	while((tcb = up->cb.tcb) != NULL &&
	 tcb->sndcnt > tcb->window){
		/* Send queue is full */
		if(up->noblock){
			errno = EWOULDBLOCK;
			return -1;
		} else if((errno = kwait(up)) != 0){
			return -1;
		}
	}
	if(tcb == NULL){
		errno = ENOTCONN;
		return -1;
	}
	return cnt;
}
int
so_tcp_qlen(struct usock *up,int rtx)
{
	int len;

	switch(rtx){
	case 0:
		len = up->cb.tcb->rcvcnt;
		break;
	case 1:
		len = up->cb.tcb->sndcnt;
		break;
	}
	return len;
}
int
so_tcp_kick(struct usock *up)
{
	kick_tcp(up->cb.tcb);
	return 0;
}
int
so_tcp_shut(struct usock *up,int how)
{
	switch(how){
	case 0:	/* No more receives -- replace upcall */
		up->cb.tcb->r_upcall = trdiscard;
		break;
	case 1:	/* Send EOF */
		close_tcp(up->cb.tcb);
		break;
	case 2:	/* Blow away TCB */
		reset_tcp(up->cb.tcb);
		up->cb.tcb = NULL;
		break;
	}
	return 0;
}
int
so_tcp_close(struct usock *up)
{
	if(up->cb.tcb != NULL){	/* In case it's been reset */
		up->cb.tcb->r_upcall = trdiscard;
		/* Tell the TCP_CLOSED upcall there's no more socket */
#if 1
		up->cb.tcb->user = NULL;
#else
		up->cb.tcb->user = -1;
#endif
		close_tcp(up->cb.tcb);
	}
	return 0;
}
/* TCP receive upcall routine */
static void
s_trcall(struct tcb *tcb,int32 cnt)
{
	/* Wake up anybody waiting for data, and let them run */
#if 1
	ksignal(tcb->user, 1);
#else
	ksignal(itop(tcb->user),1);
#endif
	kwait(NULL);
}
/* TCP transmit upcall routine */
static void
s_ttcall(struct tcb *tcb,int32 cnt)
{
	/* Wake up anybody waiting to send data, and let them run */
#if 1
	ksignal(tcb->user, 1);
#else
	ksignal(itop(tcb->user),1);
#endif
	kwait(NULL);
}
/* TCP state change upcall routine */
static void
s_tscall(struct tcb *tcb,int old,int new)
{
	jmp_buf try_b;
	unsigned int catch;
	struct usock *up,*nup,*oup;
	union sp sp;

	oup = up = tcb->user;

	switch(new){
	case TCP_CLOSED:
		/* Clean up. If the user has already closed the socket,
		 * then up will be null (s was set to -1 by the close routine).
		 * If not, then this is an abnormal close (e.g., a reset)
		 * and clearing out the pointer in the socket structure will
		 * prevent any further operations on what will be a freed
		 * control block. Also wake up anybody waiting on events
		 * related to this tcb so they will notice it disappearing.
		 */
		if(up != NULL){
			up->cb.tcb = NULL;
			up->errcodes[0] = tcb->reason;
			up->errcodes[1] = tcb->type;
			up->errcodes[2] = tcb->code;
			ksignal(up,0); /* Wake up anybody waiting */
		}
		del_tcp(tcb);
		break;
	case TCP_SYN_RECEIVED:
		/* Handle an incoming connection. If this is a server TCB,
		 * then we're being handed a "clone" TCB and we need to
		 * create a new socket structure for it. In either case,
		 * find out who we're talking to and wake up the guy waiting
		 * for the connection.
		 */
		if(tcb->flags.clone){
			/* means the backlog argument to listen() was > 0 */

			/* Clone the socket */
			catch = setjmp(try_b);
			if (catch)
				{
/* abyte('a'); */
				break; /* ENFILE, ignore it, inelegant!! */
				}

/* abyte('b'); */
			nup = os_new(try_b, AF_INET, SOCK_STREAM, 0);
			ASSIGN(*nup,*up);

/* abyte('c'); */
			/* Allocate new memory for the name areas */
			nup->name = malloc(SOCKSIZE);
			if (nup->name == NULL)
				{
/* abyte('d'); */
				goto error1;
				}

/* abyte('e'); */
			nup->peername = malloc(SOCKSIZE);
			if (nup->peername == NULL)
				{
/* abyte('f'); */
			error1:
				object_release(&nup->object);
				os_deref(nup); /* will free nup->name */
				break; /* ENOMEM, ignore it, inelegant!! */
				}

/* abyte('g'); */
			tcb->user = nup;
			nup->cb.tcb = tcb;

			/* Store the new socket # in the old one */
			up->rdysock = nup;
/* abyte('h'); */
			object_release(&nup->object);

			/* up is used here while non acquired (revisit!) */
			up = nup;
/* abyte('i'); */
		} else {
			/* means the backlog argument to listen() was == 0 */
			/* note: susv3 conforming apps should not come here */

/* abyte('j'); */
			/* Allocate space for the peer's name */
			up->peername = malloc(SOCKSIZE);
			if (up->peername == NULL)
				{
/* abyte('J'); */
				break; /* ENOMEM, ignore it, inelegant!! */
				}

			/* Store the old socket # in the old socket */
			up->rdysock = up; /* the same socket will be reused */
		}
		/* Load the addresses. Memory for the name has already
		 * been allocated, either above or in the original bind.
		 */
/* abyte('k'); */
		sp.sa = up->name;
		sp.in->sin_family = AF_INET;
		sp.in->sin_addr.s_addr = up->cb.tcb->conn.local.address;
		sp.in->sin_port = up->cb.tcb->conn.local.port;
		up->namelen = SOCKSIZE;

/* abyte('l'); */
		sp.sa = up->peername;
		sp.in->sin_family = AF_INET;
		sp.in->sin_addr.s_addr = up->cb.tcb->conn.remote.address;
		sp.in->sin_port = up->cb.tcb->conn.remote.port;
		up->peernamelen = SOCKSIZE;

		/* Wake up the guy accepting it, and let him run */
/* abyte('m'); */
		ksignal(oup, 0); /* 1); */
/* abyte('n'); */
		kwait(NULL);
/* abyte('o'); */
		break;
	default:	/* Ignore all other state transitions */
		break;
	}
/* abyte('p'); */
	ksignal(up, 0);	/* In case anybody's waiting */
/* abyte('q'); */
}
/* Discard data received on a TCP connection. Used after a receive shutdown or
 * close_s until the TCB disappears.
 */
static void
trdiscard(struct tcb *tcb,int32 cnt)
{
	struct mbuf_s *bp;

	recv_tcp(tcb,&bp,cnt);
	free_p(&bp);
}

/* Issue an automatic bind of a local address */
static void
autobind(struct usock *up)
{
	struct sockaddr_in local;

	local.sin_family = AF_INET;
	local.sin_addr.s_addr = INADDR_ANY;
	local.sin_port = Lport++;
	bind(up->index,(struct sockaddr *)&local,sizeof(struct sockaddr_in));
}
char *
tcpstate(struct usock *up)
{
	if(up->cb.tcb == NULL)
		return NULL;
	return Tcpstates[up->cb.tcb->state];
}
int
so_tcp_stat(struct usock *up)
{
	st_tcp(up->cb.tcb);
	return 0;
}

struct inet {
	struct inet *next;
	struct tcb *tcb;
	char *name;
	int stack;
	void (*task)(int,void *,void *);
};

#define	NULLINET	(struct inet *)0
static struct inet *Inet_list;

static void i_upcall(struct tcb *tcb,int oldstate,int newstate);


/* Start a TCP server. Create TCB in listen state and post upcall for
 * when a connection comes in
 */
int
start_tcp(uint16 port,char *name,void (*task)(int,void *,void *),int stack)
{
	struct inet *in;
	struct socket lsocket;

	in = (struct inet *)calloc(1,sizeof(struct inet));
#if 1 /* Nick */
	if (in == NULL)
		{
		goto error;
		}
	in->name = strdup(name);
	if (in->name == NULL)
		{
		goto error1;
		}
#endif
	lsocket.address = INADDR_ANY;
	lsocket.port = port;
#if 1
	in->tcb = open_tcp(&lsocket,NULL,TCP_SERVER,0,
			NULL,NULL,i_upcall,0,NULL);
#else
	in->tcb = open_tcp(&lsocket,NULL,TCP_SERVER,0,NULL,NULL,i_upcall,0,-1);
#endif
	if(in->tcb == NULL){
#if 1 /* Nick */
		free(in->name);
	error1:
#endif
		free(in);
#if 1 /* Nick */
	error:
#endif
		return -1;
	}
	in->stack = stack;
	in->task = task;
#if 0 /* Nick */
	in->name = strdupw(name);
#endif
	in->next = Inet_list;
	Inet_list = in;
	return 0;
}

/* State change upcall that takes incoming TCP connections */
static void
i_upcall(struct tcb *tcb,int oldstate,int newstate)
{
	jmp_buf try_b;
	unsigned int catch;
	struct inet *in;
	struct sockaddr_in sock;
	struct usock *up;
	struct process_s *process_p;
	struct thread_s *thread_p;
	struct object_s **base_object_pp, **limit_object_pp;
	struct object_s **object_pp;

	if(oldstate != TCP_LISTEN)
		return;	/* "can't happen" */
	if(newstate == TCP_CLOSED){
		/* Called when server is shut down */
		del_tcp(tcb);
		return;
	}
	for(in = Inet_list;in != NULLINET;in = in->next)
		if(in->tcb->conn.local.port == tcb->conn.local.port)
			break;
	if(in == NULLINET)
		return;	/* not in table - "can't happen" */

	/* Create a socket, hook it up with the TCB */
	catch = setjmp(try_b);
	if (catch)
		{
		return; /* ENOMEM, ignore it, inelegant!! */
		}

	up = os_new(try_b, AF_INET, SOCK_STREAM, 0);
	up->cb.tcb = tcb;
	tcb->user = up;

	/* Set the normal upcalls */
	tcb->r_upcall = s_trcall;
	tcb->t_upcall = s_ttcall;
	tcb->s_upcall = s_tscall;

	up->name = malloc(SOCKSIZE);
	if (up->name == NULL)
		{
		goto error1;
		}

	up->peername = malloc(SOCKSIZE);
	if (up->peername == NULL)
		{
	error1:
		object_release(&up->object);
	error2:
		os_deref(up);
		return;
		}

	sock.sin_family = AF_INET;
	sock.sin_addr.s_addr = tcb->conn.local.address;
	sock.sin_port = tcb->conn.local.port;
	memcpy(up->name, &sock, SOCKSIZE);
	up->namelen = SOCKSIZE;

	sock.sin_addr.s_addr = tcb->conn.remote.address;
	sock.sin_port = tcb->conn.remote.port;
	memcpy(up->peername,&sock,SOCKSIZE);
	up->peernamelen = SOCKSIZE;

	object_release(&up->object);

	/* And spawn the server task */
	process_p = process_fork(daemon_process_p, 1);
	if (process_p == NULL)
		{
		goto error2;
		}
	thread_p = process_thread_create(process_p, in->name, in->stack,
			in->task, 0, NULL, up, 0);
	process_deref(process_p);
	if (thread_p == NULL)
		{
		goto error2;
		}
}

/* Close down a TCP server created earlier by inet_start */
int
stop_tcp(uint16 port)
{
	struct inet *in,*inprev;

	inprev = NULLINET;
	for(in = Inet_list;in != NULLINET;inprev=in,in = in->next)
		if(in->tcb->conn.local.port == port)
			break;
	if(in == NULLINET)
		return -1;
	close_tcp(in->tcb);
	free(in->name);
	if(inprev != NULLINET)
		inprev->next = in->next;
	else
		Inet_list = in->next;
	free(in);
	return 0;
}

