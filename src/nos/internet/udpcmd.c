/* UDP-related user commands
 * Copyright 1991 Phil Karn, KA9Q
 *
 * Apr 04       RPB     Po-ed most of the remaining strings
 */
#include <stdio.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include "internet/netuser.h"
#include "internet/udp.h"
#include <libintl.h> /*RPB*/
#include "internet/internet.h"
#include "main/cmdparse.h"
#include "main/commands.h"
#include "po/messages.h"

static int doudpstat(int argc,char *argv[],void *p);

static struct cmds Udpcmds[] = {
	_("status"),	doudpstat,	0, 0,	NULL,
	NULL,
};
int
doudp(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return subcmd(Udpcmds,argc,argv,p);
}
int
st_udp(udp,n)
struct udp_cb *udp;
int n;
{
	if(n == 0)
		_printf(_("&UCB      Rcv-Q  Local socket\n"));

#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	return _printf(_("%9p%6u  %s\n"),udp,udp->rcvq.cnt,pinet(&udp->socket));
#else
	return _printf(_("%9p%6u  %s\n"),udp,udp->rcvcnt,pinet(&udp->socket));
#endif
}

/* Dump UDP statistics and control blocks */
static int
doudpstat(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct udp_cb *udp;
	register int i;

	for(i=1;i<=NUMUDPMIB;i++){
		/*RPB*/
		char *duptext;
		if ((duptext = _strdup(Udp_mib[i].name)) == NULL)
		{
			__getfail(Udp_mib[i].name);
			return 1; /* Ok ?? */
		}
		_printf(_("(%2u)%-20s%10lu"),i,
		 duptext,Udp_mib[i].value.integer);
		free(duptext);
		/*RPB*/
		if(i % 2)
			_printf(_("     "));
		else
			_printf(_("\n"));
	}
	if((i % 2) == 0)
		_printf(_("\n"));

	_printf(_("    &UCB Rcv-Q  Local socket\n"));
	for(udp = Udps;udp != NULL; udp = udp->next){
		if(st_udp(udp,1) == EOF)
			return 0;
	}
	return 0;
}
