/* ipsock.c */

#include <errno.h> /* Nick */
#include "nos/global.h"
#include "main/mbuf.h"
#include "internet/ip.h"
#include "socket/usock.h"
#include "socket/socket.h"

char Inet_eol[EOL_MAX] = "\r\n";

static void rip_recv(struct raw_ip *rp);
static void autobind(struct usock *up);

int
so_ip_sock(up,protocol)
struct usock *up;
int protocol;
{
#if 1
	up->cb.rip = raw_ip(protocol, rip_recv);
	up->cb.rip->user = up;
#else
	int s;

	s = up->index;
	up->cb.rip = raw_ip(protocol,rip_recv);
	up->cb.rip->user = s;
#endif
	return 0;
}

int
so_ip_conn(up)
struct usock *up;
{
	if(up->name == NULL)
		autobind(up);
	return 0;
}

int
so_ip_recv(up,buf,cnt,/*bpp,*/from,fromlen)
struct usock *up;
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
void *buf;
int cnt;
#else
struct mbuf_s **bpp;
#endif
struct sockaddr *from;
int *fromlen;
{
	struct raw_ip *rip;
	struct sockaddr_in *remote;
	struct ip ip;
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
	struct mbuf_s *bp;
#else
	int cnt;
#endif

#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	while((rip = up->cb.rip) != NULL && rip->rcvq.cnt == 0){
#else
	while((rip = up->cb.rip) != NULL && rip->rcvq == NULL){
#endif
		if(up->noblock){
			errno = EWOULDBLOCK;
			return -1;
		} else if((errno = kwait(up)) != 0){
			return -1;
		}
	}
	if(rip == NULL){
		/* Connection went away */
		errno = ENOTCONN;
		return -1;
	}
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	rip->rcvq.cnt--;

	bp = *rip->rcvq.rp++;
	if (rip->rcvq.rp >= rip->rcvq.buf + RECEIVE_ENTRIES)
		{
		rip->rcvq.rp = rip->rcvq.buf;
		}
#else
	bp = dequeue(&rip->rcvq);
#endif
	ntohip(&ip, &bp);

	cnt = pullup(&bp, buf, (uint16)cnt);
	free_p(&bp);
#else
	*bpp = dequeue(&rip->rcvq);
	ntohip(&ip,bpp);

	cnt = len_p(*bpp);
#endif
	if(from != NULL && fromlen != (int *)NULL && *fromlen >= SOCKSIZE){
		remote = (struct sockaddr_in *)from;
		remote->sin_family = AF_INET;
		remote->sin_addr.s_addr = ip.source;
		remote->sin_port = 0;
		*fromlen = SOCKSIZE;
	}
	return cnt;
}

int
so_ip_send(
struct usock *up,
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
void *buf,
int cnt,
#else
struct mbuf_s **bpp,
#endif
struct sockaddr *to
){
	struct sockaddr_in *local,*remote;
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
	struct mbuf_s *bp;
#endif

	if(up->name == NULL)
		autobind(up);
	local = (struct sockaddr_in *)up->name;
	if(to != NULL){
		remote = (struct sockaddr_in *)to;
	} else if(up->peername != NULL) {
		remote = (struct sockaddr_in *)up->peername;
	} else {
#if 0 /* now that we don't have to support recv_mbuf() or send_mbuf() */
		free_p(bpp);
#endif
		errno = ENOTCONN;
		return -1;
	}

#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
	bp = qdata(buf, cnt);
	if (bp == NULL)
		{
		errno = ENOMEM;
		return -1;
		}
	ip_send(local->sin_addr.s_addr,remote->sin_addr.s_addr,
		(char)up->cb.rip->protocol,0,0,&bp,0,0,0);
	return cnt;
#else
	ip_send(local->sin_addr.s_addr,remote->sin_addr.s_addr,
		(char)up->cb.rip->protocol,0,0,bpp,0,0,0);
	return 0;
#endif
}

int
so_ip_qlen(up,rtx)
struct usock *up;
int rtx;
{
	int len;

	switch(rtx){
	case 0:
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
		len = up->cb.rip->rcvq.cnt;
#else
		len = len_q(up->cb.rip->rcvq);
#endif
		break;
	case 1:
		len = 0;
		break;
	}
	return len;
}

int
so_ip_close(up)
struct usock *up;
{
	del_ip(up->cb.rip);
	return 0;
}

int
checkipaddr(name,namelen)
struct sockaddr *name;
int namelen;
{
	struct sockaddr_in *sock;

	sock = (struct sockaddr_in *)name;
	if(sock->sin_family != AF_INET || namelen != sizeof(struct sockaddr_in))
		return -1;
	return 0;
}

/* Raw IP receive upcall routine */
static void
rip_recv(rp)
struct raw_ip *rp;
{
#if 1
	ksignal(rp->user, 1);
#else
	ksignal(itop(rp->user),1);
#endif
	kwait(NULL);
}

/* Issue an automatic bind of a local address */
static void
autobind(up)
struct usock *up;
{
	struct sockaddr_in local;
	int s;

	s = up->index;
	local.sin_family = AF_INET;
	local.sin_addr.s_addr = INADDR_ANY;
	local.sin_port = Lport++;
	bind(s,(struct sockaddr *)&local,sizeof(struct sockaddr_in));
}

char *
ippsocket(p)
struct sockaddr *p;
{
	struct sockaddr_in *sp;
	struct socket socket;
	static char buf[30];

	sp = (struct sockaddr_in *)p;
	socket.address = sp->sin_addr.s_addr;
	socket.port = sp->sin_port;
	strcpy(buf,pinet(&socket));

	return buf;
}

