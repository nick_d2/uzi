# auto.mk by Nick for Hytech NOS/UZI project

# -----------------------------------------------------------------------------

SUBDIRS=

OUTPUTS=	internet.$(LIBEXT)

internet_$(LIBEXT)_SOURCES=\
		domain.c domhdr.c hop.c icmp.c \
		icmpcmd.c icmphdr.c icmpmsg.c ip.c ipcmd.c iphdr.c \
		iproute.c ipsock.c netuser.c ping.c \
		sim.c tcpcmd.c tcphdr.c tcpin.c tcpout.c tcpsock.c \
		tcpsubr.c tcptimer.c tcpuser.c udp.c udpcmd.c \
		udphdr.c udpsock.c
#arp.c arpcmd.c arphdr.c rip.c ripcmd.c

domain_c_MODULES= \
		Dcache Dcache_size Dcache_time Dfile_clean Dfile_reading \
		Dfile_writing Dfile_updater Dfile_wait_absolute \
		Dfile_wait_relative Dservers Dserver_retries Dsuffix Dtrace \
		Dtypes delim Dcmds Dcachecmds dodomain docache \
		dosuffix docacheclean docachelist docachesize docachewait \
		dlist_add dlist_drop dodnsadd add_nameserver dodnsdrop \
		dodnslist dodnsquery dodnsretry dodnstrace dtype check_ttl \
		compare_rr compare_rr_list copy_rr copy_rr_list free_rr \
		make_rr dcache_add dcache_drop dcache_search dcache_update \
		get_rr put_rr dfile_search dfile_update dumpdomain \
		dns_makequery dns_query isaddr checksuffix resolver inverse_a \
		resolve_rr resolve_a resolve resolve_mx resolve_mailb
#Ndtypes

ip_c_MODULES=	Ip_mib Reasmq Id_cntr Rawip Ip_trace ip_send ip_recv \
		ipip_recv fraghandle raw_ip del_ip lookup_reasm creat_reasm \
		free_reasm ip_timeout newfrag freefrag ip_garbage ttldec \
		rquench

iproute_c_MODULES= \
		Routes route Rt_cache Rtlookups Rtchits ip_route q_pkt \
		ip_proc ip_encap rt_add rt_drop ip_mtu locaddr rt_lookup \
		rt_blookup rt_merge rt_timeout
#ipinit

tcpcmd_c_MODULES = \
		Tcp_tstamps Tcpcmds dotcp dotcptr dotimestamps dotcpreset \
		doirtt dortt dotcpkick domss dowindow dosyndata dotcpstat \
		tcprepstat tstat st_tcp
#tcpcmd_keychar

tcpin_c_MODULES= \
		tcp_input tcp_icmp reset update in_window proc_syn send_syn \
		add_reseq get_reseq trim

# -----------------------------------------------------------------------------

