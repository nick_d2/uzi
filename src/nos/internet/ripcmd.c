/* RIP-related user commands
 *   Al Broscious, N3FCT
 *   Phil Karn, KA9Q
 */
#include <stdio.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include "internet/netuser.h"
#include <libintl.h> /*RPB*/
#include "internet/internet.h"
#include "main/cmdparse.h"
#include "main/timer.h"
#include "main/iface.h"
#include "internet/udp.h"
#include "internet/rip.h"
#include "main/commands.h"
#include "po/messages.h"

struct cmds Ripcmds[] = {
	_("accept"),	dodroprefuse,	0,	2,
		_("rip accept <gateway> "),
	_("add"),		doripadd,	0,	3,
		_("rip add <dest> <interval> [<flags>]"),
	_("drop"),		doripdrop,	0,	2,
		_("rip drop <dest>"),
	_("merge"),	doripmerge,	0,	0,	NULL,
	_("refuse"),	doaddrefuse,	0,	2,
		_("rip refuse <gateway>"),
	_("request"),	doripreq,	0,	2,	NULL,
	_("status"),	doripstat,	0,	0,	NULL,
	_("trace"),	doriptrace,	0,	0,	NULL,
	NULL,
};

int
dorip(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return subcmd(Ripcmds,argc,argv,p);
}

/* Add an entry to the RIP output list */
int
doripadd(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	int split = 1;
	int us = 0;

	if(argc > 3)
		split = atoi(argv[3]);
	if(argc > 4)
		us = atoi(argv[4]);

	return rip_add(resolve(argv[1]),atol(argv[2]),split,us);
}

/* Add an entry to the RIP refuse list */
int
doaddrefuse(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return riprefadd(resolve(argv[1]));
}

/* Drop an entry from the RIP output list */
int
doripdrop(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return rip_drop(resolve(argv[1]));
}

/* Drop an entry from the RIP refuse list */
int
dodroprefuse(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return riprefdrop(resolve(argv[1]));
}

/* Initialize the RIP listener */
int
doripinit(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return rip_init();
}
int
doripstop(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	del_udp(Rip_cb);
	Rip_cb = NULL;
	return 0;
}
int
doripreq(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	uint16 replyport;

	if(argc > 2)
		replyport = atoi(argv[2]);
	else
		replyport = RIP_PORT;
	return ripreq(resolve(argv[1]),replyport);
}
/* Dump RIP statistics */
int
doripstat(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct rip_list *rl;
	struct rip_refuse *rfl;

	_printf(_("RIP: sent %lu rcvd %lu reqst %lu resp %lu unk %lu refused %lu\n"),
	 Rip_stat.output, Rip_stat.rcvd, Rip_stat.request, Rip_stat.response,
	 Rip_stat.unknown,Rip_stat.refusals);
	if(Rip_list != NULL){
		_printf(_("Active RIP output interfaces:\n"));
		_printf(_("Dest Addr       Interval Split\n"));
		for(rl=Rip_list; rl != NULL; rl = rl->next){
			_printf(_("%-16s%-9lu%-6u\n"),inet_ntoa(rl->dest),
			 rl->interval,rl->flags.rip_split);
		}
	}
	if(Rip_refuse != NULL){
		_printf(_("Refusing announcements from gateways:\n"));
		for(rfl=Rip_refuse; rfl != NULL;rfl = rfl->next){
			_printf(_("%s\n"),inet_ntoa(rfl->target));
		}
	}
	return 0;
}

int
doriptrace(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return setshort(&Rip_trace, _("RIP tracing"), argc, argv);
}
int
doripmerge(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return setbool(&Rip_merge, _("RIP merging"), argc, argv);
}
