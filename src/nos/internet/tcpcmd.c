/* TCP control and status routines
 * Copyright 1991 Phil Karn, KA9Q
 *
 * Apr 04       RPB     Po-ed most of the remaining strings
 */
#include <stdio.h>
#include "nos/global.h"
#include "main/timer.h"
#include "main/mbuf.h"
#include "internet/netuser.h"
#include <libintl.h> /*RPB*/
#include "internet/internet.h"
#include "internet/tcp.h"
#include "main/cmdparse.h"
#include "main/commands.h"
#include "socket/socket.h"
#ifdef JOB_CONTROL
#include "kernel/session.h"
#endif
#include "kernel/thread.h" /* for my_thread_p */
#include "main/main.h"
#include <libintl.h>
#include "po/messages.h"

#define TCPCMDS_MAX 11

#ifdef MODULE
#define STATIC
extern struct cmds Tcpcmds[TCPCMDS_MAX];
#else
#define STATIC static
#define MODULE_Tcp_tstamps
#define MODULE_Tcpcmds
#define MODULE_dotcp
#define MODULE_dotcptr
#define MODULE_dotimestamps
#define MODULE_dotcpreset
#define MODULE_doirtt
#define MODULE_dortt
#define MODULE_dotcpkick
#define MODULE_domss
#define MODULE_dowindow
#define MODULE_dosyndata
#define MODULE_dotcpstat
#define MODULE_tcprepstat
#define MODULE_tstat
#define MODULE_st_tcp
/*#define MODULE_tcpcmd_keychar*/
#endif

#ifdef MODULE_Tcp_tstamps
int Tcp_tstamps = 1;
#endif

STATIC int doirtt(int argc,char *argv[],void *p);
STATIC int domss(int argc,char *argv[],void *p);
STATIC int dortt(int argc,char *argv[],void *p);
STATIC int dotcpkick(int argc,char *argv[],void *p);
STATIC int dotcpreset(int argc,char *argv[],void *p);
STATIC int dotcpstat(int argc,char *argv[],void *p);
STATIC int dotcptr(int argc,char *argv[],void *p);
STATIC int dowindow(int argc,char *argv[],void *p);
STATIC int dosyndata(int argc,char *argv[],void *p);
STATIC int dotimestamps(int argc,char *argv[],void *p);
STATIC int tstat(void);
STATIC int tcpcmd_keychar(int c);
STATIC void tcprepstat(int interval,void *p1,void *p2);

/* TCP subcommand table */
#ifdef MODULE_Tcpcmds
STATIC struct cmds Tcpcmds[TCPCMDS_MAX] = {
	N_("irtt"),	doirtt,		0, 0,	NULL,
	N_("kick"),	dotcpkick,	0, 2,	N_("tcp kick <tcb>"),
	N_("mss"),	domss,		0, 0,	NULL,
	N_("reset"),	dotcpreset,	0, 2,	N_("tcp reset <tcb>"),
	N_("rtt"),	dortt,		0, 3,	N_("tcp rtt <tcb> <val>"),
	N_("status"),	dotcpstat,	0, 0,	N_("tcp stat <tcb> [<interval>]"),
	N_("syndata"),	dosyndata,	0, 0,	NULL,
	N_("timestamps"), dotimestamps,	0, 0,   NULL,
	N_("trace"),	dotcptr,	0, 0,	NULL,
	N_("window"),	dowindow,	0, 0,	NULL,
	NULL,
};
#endif

#ifdef MODULE_dotcp
int
dotcp(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return subcmd(Tcpcmds,argc,argv,p);
}
#endif

#ifdef MODULE_dotcptr
STATIC int
dotcptr(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return setbool(&Tcp_trace, _("TCP state tracing"), argc, argv);
}
#endif

#ifdef MODULE_dotimestamps
STATIC int
dotimestamps(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return setbool(&Tcp_tstamps, _("TCP timestamps"), argc, argv);
}
#endif

/* Eliminate a TCP connection */

#ifdef MODULE_dotcpreset
STATIC int
dotcpreset(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct tcb *tcb;

	tcb = (struct tcb *)ltop(htol(argv[1]));
	if(!tcpval(tcb)){
		_printf(_("Not a valid control block\n"));
		return 1;
	}
	reset_tcp(tcb);
	return 0;
}
#endif

/* Set initial round trip time for new connections */

#ifdef MODULE_doirtt
STATIC int
doirtt(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct tcp_rtt *tp;

	setlong(&Tcp_irtt, _("TCP default irtt"), argc, argv);
	if(argc < 2){
		for(tp = Tcp_rtt; tp < Tcp_rtt + RTTCACHE; tp++){
			if(tp->addr != 0){
				_printf(_("%s: srtt %lu mdev %lu\n"),
				 inet_ntoa(tp->addr),
				 tp->srtt,tp->mdev);
			}
		}
	}
	return 0;
}
#endif

/* Set smoothed round trip time for specified TCB */

#ifdef MODULE_dortt
STATIC int
dortt(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct tcb *tcb;

	tcb = (struct tcb *)ltop(htol(argv[1]));
	if(!tcpval(tcb)){
		_printf(_("Not a valid control block\n"));
		return 1;
	}
	tcb->srtt = atol(argv[2]);
	return 0;
}
#endif

/* Force a retransmission */

#ifdef MODULE_dotcpkick
STATIC int
dotcpkick(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct tcb *tcb;

	tcb = (struct tcb *)ltop(htol(argv[1]));
	if(kick_tcp(tcb) == -1){
		_printf(_("Not a valid control block\n"));
		return 1;
	}
	return 0;
}
#endif

/* Set default maximum segment size */

#ifdef MODULE_domss
STATIC int
domss(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return setshort(&Tcp_mss, _("TCP MSS"), argc, argv);
}
#endif

/* Set default window size */

#ifdef MODULE_dowindow
STATIC int
dowindow(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return setshort(&Tcp_window, _("TCP window"), argc, argv);
}
#endif


#ifdef MODULE_dosyndata
STATIC int
dosyndata(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return setbool(&Tcp_syndata, _("TCP syn+data piggybacking"),
		       argc, argv);
}
#endif


/* Display status of TCBs */

#ifdef MODULE_dotcpstat
STATIC int
dotcpstat(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct tcb *tcb;
	int32 interval = 0;

	if(argc < 2){
		tstat();
		return 0;
	}
	if(argc > 2)
		interval = atol(argv[2]);

	tcb = (struct tcb *)ltop(htol(argv[1]));
	if(!tcpval(tcb)){
		_printf(_("Not a valid control block\n"));
		return 1;
	}
	if(interval == 0){
		st_tcp(tcb);
		return 0;
	}
	process_thread_create(daemon_process_p, "rep tcp stat",512,tcprepstat,interval,(void *)tcb,NULL,0);
	return 0;
}
#endif


#ifdef MODULE_tcprepstat
STATIC void
tcprepstat(interval,p1,p2)
int interval;
void *p1;
void *p2;
{
	struct tcb *tcb = (struct tcb *)p1;
#ifdef JOB_CONTROL
	struct session_s *sp;

	if((sp = newsession(Cmdline,REPEAT,1)) == NULL){
		_printf(_("Too many sessions\n"));
		return;
	}
	sp->inproc = tcpcmd_keychar;	/* Intercept ^C */
	while(sp->inproc == tcpcmd_keychar){	/* ^C will clear sp->inproc */
/* ????? */
#endif
		_printf(_("%c[2J"),ESC);	/* Clear screen */
		st_tcp(tcb);
#ifdef JOB_CONTROL
/* ????? */
		if(tcb->state == TCP_CLOSED || ppause(interval) == -1)
			break;
	}
	freesession(sp, 1); /* does keywait(NULL, 1); beforehand */
#endif
}
#endif

/* Dump TCP stats and summary of all TCBs
/*     &TCB Rcv-Q Snd-Q  Local socket           Remote socket          State
 *     1234     0     0  xxx.xxx.xxx.xxx:xxxxx  xxx.xxx.xxx.xxx:xxxxx  Established
 */

#ifdef MODULE_tstat
STATIC int
tstat()
{
	register int i;
	register struct tcb *tcb;
	int j;

	for(j=i=1;i<=NUMTCPMIB;i++){
		char *duptext; /*RPB*/
		if(Tcp_mib[i].name == NULL)
			continue;
		/*RPB*/
		if ((duptext = _strdup(Tcp_mib[i].name)) == NULL)
		{
			__getfail(Tcp_mib[i].name);
			return 1; /* Ok? */
		}
		_printf(_("(%2u)%-20s%10lu"),i,duptext,
		 Tcp_mib[i].value.integer);
		free(duptext);
		/*RPB*/
		if(j++ % 2)
			_printf(_("     "));
		else
			_printf(_("\n"));
	}
	if((j % 2) == 0)
		_printf(_("\n"));

	_printf(_("&TCB      Rcv-Q Snd-Q  Local socket           Remote socket          State\n"));
	for(tcb=Tcbs;tcb != NULL;tcb = tcb->next){
		_printf(_("%9p%6u%6u  "),tcb,tcb->rcvcnt,tcb->sndcnt);
		_printf(_("%-23s"),pinet(&tcb->conn.local));
		_printf(_("%-23s"),pinet(&tcb->conn.remote));
		_printf(_("%-s"),Tcpstates[tcb->state]);
		if(tcb->state == TCP_LISTEN && tcb->flags.clone)
			_printf(_(" (S)"));
		_printf(_("\n"));
	}
	return 0;
}
#endif
/* Dump a TCP control block in detail */

#ifdef MODULE_st_tcp
void
st_tcp(tcb)
struct tcb *tcb;
{
	int32 sent,recvd;

	if(tcb == NULL)
		return;
	/* Compute total data sent and received; take out SYN and FIN */
	sent = tcb->snd.una - tcb->iss;	/* Acknowledged data only */
	recvd = tcb->rcv.nxt - tcb->irs;
	switch(tcb->state){
	case TCP_LISTEN:
	case TCP_SYN_SENT:	/* Nothing received or acked yet */
		sent = recvd = 0;
		break;
	case TCP_SYN_RECEIVED:
		recvd--;	/* Got SYN, no data acked yet */
		sent = 0;
		break;
	case TCP_ESTABLISHED:	/* Got and sent SYN */
	case TCP_FINWAIT1:	/* FIN not acked yet */
		sent--;
		recvd--;
		break;
	case TCP_FINWAIT2:	/* Our SYN and FIN both acked */
		sent -= 2;
		recvd--;
		break;
	case TCP_CLOSE_WAIT:	/* Got SYN and FIN, our FIN not yet acked */
	case TCP_CLOSING:
	case TCP_LAST_ACK:
		sent--;
		recvd -= 2;
		break;
	case TCP_TIME_WAIT:	/* Sent and received SYN/FIN, all acked */
		sent -= 2;
		recvd -= 2;
		break;
	}
	_printf(_("Local: %s"),pinet(&tcb->conn.local));
	_printf(_(" Remote: %s"),pinet(&tcb->conn.remote));
	_printf(_(" State: %s\n"),Tcpstates[tcb->state]);
	_printf(_("         Unack     Next Resent CWind Thrsh  Wind  MSS Queue  Thruput      Total\n"));
	_printf(_("Send: %08lx %08lx%7lu%6lu%6lu%6lu%5lu%6lu%9lu%11lu\n"),
	 tcb->snd.una,tcb->snd.nxt,tcb->resent,tcb->cwind,tcb->ssthresh,
	 tcb->snd.wnd,tcb->mss,tcb->sndcnt,tcb->txbw,sent);

	_printf(_("Recv:          %08lx%7lu            %6lu     %6lu%9lu%11lu\n"),
	 tcb->rcv.nxt,tcb->rerecv,tcb->rcv.wnd,tcb->rcvcnt,tcb->rxbw,recvd);

	_printf(_("Dup acks   Backoff   Timeouts   Source Quench   Unreachables   Power\n"));
	_printf(_("%8u%10u%11lu%16lu%15lu"),tcb->dupacks,tcb->backoff,tcb->timeouts,
	 tcb->quench,tcb->unreach);
	if(tcb->srtt != 0)
		_printf(_("%8lu"),1000*tcb->txbw/tcb->srtt);
	else
		_printf(_("     INF"));
	if(tcb->flags.retran)
		_printf(_(" Retry"));
	_printf(_("\n"));

	_printf(_("Timer        Count  Duration  Last RTT      SRTT      Mdev   Method\n"));
	switch(tcb->timer.state){
	case TIMER_STOP:
		_printf(_("stopped"));
		break;
	case TIMER_RUN:
		_printf(_("running"));
		break;
	case TIMER_EXPIRE:
		_printf(_("expired"));
		break;
	}
	_printf(_(" %10lu%10lu%10lu%10lu%10lu"),(long)read_timer(&tcb->timer),
	 (long)dur_timer(&tcb->timer),tcb->rtt,tcb->srtt,tcb->mdev);
	_printf(_("   %s\n"),tcb->flags.ts_ok ? "timestamps":"standard");

	if(tcb->reseq != (struct reseq *)NULL){
		register struct reseq *rp;

		_printf(_("Reassembly queue:\n"));
		for(rp = tcb->reseq;rp != (struct reseq *)NULL; rp = rp->next){
			_printf(_("  seq 0x%lx %u bytes\n"),rp->seg.seq,rp->length);
		}
	}
}
#endif

#if 0 /*def MODULE_tcpcmd_keychar*/
STATIC int
tcpcmd_keychar(c)
int c;
{
	if(c != CTLC)
		return 1;	/* Ignore all but ^C */

#if 1
	_printf(_("^C\n"));
	fflush(stdout);
	alert(my_thread_p, EABORT);
#else
	_fprintf(my_session_p->output, _("^C\n"));
	alert(my_session_p->proc,EABORT);
#endif
	/* ??? WHAT DOES THIS DO ??? my_session_p->inproc = NULL; */
	return 0;
}
#endif

