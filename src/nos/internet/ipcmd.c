/* IP-related user commands
 * Copyright 1991 Phil Karn, KA9Q
 *
 * Apr 06	RPB	Changes needed for intl' of strings
 */
#include <stdio.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include <libintl.h> /*RPB*/
#include "internet/internet.h"
#include "main/timer.h"
#include "internet/netuser.h"
#include "main/iface.h"
#include "internet/ip.h"
#include "main/cmdparse.h"
#include "main/commands.h"
#include "internet/rip.h"
#include "po/messages.h"

int32 Ip_addr;

static int doadd(int argc,char *argv[],void *p);
static int dodrop(int argc,char *argv[],void *p);
static int doflush(int argc,char *argv[],void *p);
static int doipaddr(int argc,char *argv[],void *p);
static int doipstat(int argc,char *argv[],void *p);
static int dolook(int argc,char *argv[],void *p);
static int dortimer(int argc,char *argv[],void *p);
static int dottl(int argc,char *argv[],void *p);
static int doiptrace(int argc,char *argv[],void *p);
static int dumproute(struct route *rp);

static struct cmds Ipcmds[] = {
	N_("address"),	doipaddr,	0,	0, NULL,
	N_("rtimer"),	dortimer,	0,	0, NULL,
	N_("status"),	doipstat,	0,	0, NULL,
	N_("trace"),	doiptrace,	0,	0, NULL,
	N_("ttl"),	dottl,		0,	0, NULL,
	NULL,
};
/* "route" subcommands */
static struct cmds Rtcmds[] = {
	N_("add"),		doadd,		0,	3,
	N_("route add <dest addr>[/<bits>] <if name> [gateway] [metric]"),

	N_("addprivate"),	doadd,		0,	3,
	N_("route addprivate <dest addr>[/<bits>] <if name> [gateway] [metric]"),

	N_("drop"),		dodrop,		0,	2,
	N_("route drop <dest addr>[/<bits>]"),

	N_("flush"),	doflush,	0,	0,
	NULL,

	N_("lookup"),	dolook,		0,	2,
	N_("route lookup <dest addr>"),

	NULL,
};

int
doip(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return subcmd(Ipcmds,argc,argv,p);
}
static int
doiptrace(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return setbool(&Ip_trace, _("IP rx tracing"), argc, argv);
}

static int
doipaddr(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	int32 n;

	if(argc < 2) {
		_printf(_("%s\n"),inet_ntoa(Ip_addr));
	} else if((n = resolve(argv[1])) == 0){
		_printf(Badhost,argv[1]);
		return 1;
	} else
		Ip_addr = n;
	return 0;
}
static int
dortimer(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return setlong(&ipReasmTimeout, _("IP reasm timeout (sec)"),
	       argc, argv);
}
static int
dottl(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return setlong(&ipDefaultTTL, _("IP Time-to-live"), argc, argv);
}


/* Display and/or manipulate routing table */
int
doroute(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register int i,bits;
	register struct route *rp;

	if(argc >= 2)
		return subcmd(Rtcmds,argc,argv,p);

	/* Dump IP routing table
	 * Dest            Len Interface    Gateway          Use
	 * 192.001.002.003 32  sl0          192.002.003.004  0
	 */
	_printf(
_("Dest            Len Interface    Gateway          Metric  P Timer  Use\n"));

	for(bits=31;bits>=0;bits--){
		for(i=0;i<HASHMOD;i++){
			for(rp = Routes[bits][i];rp != NULL;rp = rp->next){
				if(dumproute(rp) == EOF)
					return 0;
			}
		}
	}
	if(R_default.iface != NULL)
		dumproute(&R_default);

	return 0;
}
/* Add an entry to the routing table
 * E.g., "add 1.2.3.4 ax0 5.6.7.8 3"
 */
static int
doadd(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct iface_s *ifp;
	int32 dest,gateway;
	unsigned bits;
	char *bitp;
	int32 metric;
	char private;

	if(_strncmp(argv[0],_("addp"),4) == 0)
		private = 1;
	else
		private = 0;
	if(_strcmp(argv[1],_("default")) == 0){
		dest = 0;
		bits = 0;
	} else {
		/* If IP address is followed by an optional slash and
		 * a length field, (e.g., 128.96/16) get it;
		 * otherwise assume a full 32-bit address
		 */
		if((bitp = strchr(argv[1],'/')) != NULL){
			/* Terminate address token for resolve() call */
			*bitp++ = '\0';
			bits = atoi(bitp);
		} else
			bits = 32;

		if((dest = resolve(argv[1])) == 0){
			_printf(Badhost,argv[1]);
			return 1;
		}
	}
	if((ifp = if_lookup(argv[2])) == NULL){
		_printf(_("Interface \"%s\" unknown\n"),argv[2]);
		return 1;
	}
	if(argc > 3){
		if((gateway = resolve(argv[3])) == 0){
			_printf(Badhost,argv[3]);
			return 1;
		}
	} else {
		gateway = 0;
	}
	if (argc > 4)
		metric = atol(argv[4]);
	else
		metric = 1;

	if(rt_add(dest,bits,gateway,ifp,metric,0,private) == NULL)
		_printf(_("Can't add route\n"));
	return 0;
}
/* Drop an entry from the routing table
 * E.g., "drop 128.96/16
 */
static int
dodrop(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	char *bitp;
	unsigned bits;
	int32 n;

	if(_strcmp(argv[1],_("default")) == 0){
		n = 0;
		bits = 0;
	} else {
		/* If IP address is followed by an optional slash and length field,
		 * (e.g., 128.96/16) get it; otherwise assume a full 32-bit address
		 */
		if((bitp = strchr(argv[1],'/')) != NULL){
			/* Terminate address token for resolve() call */
			*bitp++ = '\0';
			bits = atoi(bitp);
		} else
			bits = 32;

		if((n = resolve(argv[1])) == 0){
			_printf(Badhost,argv[1]);
			return 1;
		}
	}
	return rt_drop(n,bits);
}
/* Force a timeout on all temporary routes */
static int
doflush(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct route *rp;
	struct route *rptmp;
	int i,j;

	if(R_default.timer.state == TIMER_RUN){
		rt_drop(0,0);	/* Drop default route */
	}
	for(i=0;i<HASHMOD;i++){
		for(j=0;j<32;j++){
			for(rp = Routes[j][i];rp != NULL;rp = rptmp){
				rptmp = rp->next;
				if(rp->timer.state == TIMER_RUN){
					rt_drop(rp->target,rp->bits);
				}
			}
		}
	}
	return 0;
}
/* Dump a routing table entry */
static int
dumproute(rp)
register struct route *rp;
{
	char *cp;

	if(rp->target != 0)
		cp = inet_ntoa(rp->target);
	else
		cp = "default"; /* RPB: it's not worth it to po this one */
	_printf(_("%-16s"),cp);
	_printf(_("%-4u"),rp->bits);
	_printf(_("%-13s"),rp->iface->name);
	if(rp->gateway != 0)
		cp = inet_ntoa(rp->gateway);
	else
		cp = "";
	_printf(_("%-17s"),cp);
	_printf(_("%-8lu"),rp->metric);
	_printf(_("%c "),rp->flags.rtprivate ? 'P' : ' ');
	_printf(_("%-7lu"),
	 read_timer(&rp->timer) / 1000L);
	return _printf(_("%lu\n"),rp->uses);
}

static int
dolook(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct route *rp;
	int32 addr;

	addr = resolve(argv[1]);
	if(addr == 0){
		_printf(_("Host %s unknown\n"),argv[1]);
		return 1;
	}
	if((rp = rt_lookup(addr)) == NULL){
		_printf(_("Host %s (%s) unreachable\n"),argv[1],inet_ntoa(addr));
		return 1;
	}
	dumproute(rp);
	return 0;
}

static int
doipstat(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct reasm *rp;
	register struct frag *fp;
	int i;

	for(i=1;i<=NUMIPMIB;i++){
		/*RPB*/
		char *duptext = NULL;

		if ((duptext = _strdup(Ip_mib[i].name)) != NULL)
			_printf(_("(%2u)%-20s%10lu"),i,
			 duptext,Ip_mib[i].value.integer);
		else
			__getfail(Ip_mib[i].name);
		free(duptext);
		/*RPB*/
		if(i % 2)
			_printf(_("     "));
		else
			_printf(_("\n"));
	}
	if((i % 2) == 0)
		_printf(_("\n"));
	_printf(_("Routing lookups: %lu, cache hits %lu (%lu%%)\n"),
	 Rtlookups,Rtchits,
	 Rtlookups != 0 ? (Rtchits*100 + Rtlookups/2)/Rtlookups: 0);

	if(Reasmq != NULL)
		_printf(_("Reassembly fragments:\n"));
	for(rp = Reasmq;rp != NULL;rp = rp->next){
		_printf(_("src %s"),inet_ntoa(rp->source));
		_printf(_(" dest %s"),inet_ntoa(rp->dest));
		_printf(_(" id %u pctl %u time %lu len %u\n"),
		 rp->id,rp->protocol,read_timer(&rp->timer),
		 rp->length);
		for(fp = rp->fraglist;fp != NULL;fp = fp->next){
			_printf(_(" offset %u last %u\n"),fp->offset,
			fp->last);
		}
	}
	return 0;
}
