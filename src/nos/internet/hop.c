/*
 *	HOP.C   -- trace route packets take to a remote host
 *
 *	02-90	-- Katie Stevens (dkstevens@ucdavis.edu)
 *		   UC Davis, Computing Services
 *		   Davis, CA
 *	04-90	-- Modified by Phil Karn to use raw IP sockets to read replies
 *	08-90	-- Modified by Bill Simpson to display domain names
 *	02-04	-- RPB:	Changes needed for intl' of strings
 */

#include <stdio.h>
#include <errno.h> /* Nick */
#include <string.h>
#include <sys/socket.h>
#include "nos/global.h"
#include "main/mbuf.h"
#if 0
#include "socket/usock.h"
#include "socket/socket.h"
#endif
#ifdef JOB_CONTROL
#include "kernel/session.h"
#endif
#include "main/timer.h"
#include "kernel/thread.h"
#include "internet/netuser.h"
#include "internet/domain.h"
#include "main/commands.h"
#include "driver/devtty.h" /* for struct ttystate */
#include "main/cmdparse.h"
#include "internet/ip.h"
#include "internet/icmp.h"
#include "internet/udp.h"
#include "nos/hardware.h"
#include <libintl.h>
#include "po/messages.h"
#include "main/logmsg.h"

#define HOPMAXQUERY	5		/* Max# queries each TTL value */
static uint16 Hoprport = 32768+666;	/* funny port for udp probes */
#define HOP_HIGHBIT	32768		/* Mask to check ICMP msgs */


#define HOPTRACE	1		/* Enable HOP tracing */
#ifdef HOPTRACE
static int Hoptrace = 0;
static int hoptrace(int argc,char *argv[],void *p);
#endif


static unsigned  short Hopmaxttl  = 30;		/* max attempts */
static unsigned  short Hopmaxwait = 5;		/* secs timeout each attempt */
static unsigned  short Hopquery   = 3;		/* #probes each attempt */

static int hopcheck(int argc,char *argv[],void *p);
static int hopttl(int argc,char *argv[],void *p);
static int hokwait(int argc,char *argv[],void *p);
static int hopnum(int argc,char *argv[],void *p);
static int geticmp(int s,uint16 lport,uint16 fport,
	int32 *sender,char *type,char *code);
static int keychar(int c);

static struct cmds Hopcmds[] = {
	N_("check"),	hopcheck,	2048,	2,	N_("check <host>"),
	N_("maxttl"),	hopttl,		0,	0,	NULL,
	N_("maxwait"),	hokwait,	0,	0,	NULL,
	N_("queries"),	hopnum,		0,	0,	NULL,
#ifdef HOPTRACE
	N_("trace"),	hoptrace,	0,	0,	NULL,
#endif
	NULL,
};

/* attempt to trace route to a remote host */
int
dohop(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return subcmd(Hopcmds,argc,argv,p);
}

/* Set/show # queries sent each TTL value */
static int
hopnum(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	uint16 r;
	uint16 x = Hopquery;
	r = setshort(&x, _("# queries each attempt"), argc, argv);
	if ((x <= 0)||(x > HOPMAXQUERY)) {
		_printf(_("Must be  0 < x <= %d\n"),HOPMAXQUERY);
		return 0;
	} else {
		Hopquery = x;
	}
    return (int)r;
}
#ifdef HOPTRACE
/* Set/show tracelevel */
static int
hoptrace(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return setbool(&Hoptrace, _("HOPCHECK tracing"), argc, argv);
}
#endif
/* Set/show maximum TTL value for a traceroute query */
static int
hopttl(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	uint16 r;
	uint16 x = Hopmaxttl;
	r = setshort(&x, _("Max attempts to reach host"), argc, argv);
	if ((x <= 0)||(x > 255)) {
		_printf(_("Must be  0 < x <= 255\n"));
		return 0;
	} else {
		Hopmaxttl = x;
	}
    return (int)r;
}
/* Set/show #secs until timeout for a traceroute query */
static int
hokwait(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	uint16 r;
	uint16 x = Hopmaxwait;
	r = setshort(&x, _("# secs to wait for reply to query"), argc, argv);
	if (x <= 0) {
		_printf(_("Must be >= 0\n"));
		return 0;
	} else {
		Hopmaxwait = x;
	}
    return (int)r;
}

/* send probes to trace route of a remote host */
static int
hopcheck(argc,argv,p)
int argc;
char *argv[];
void *p;
{
#ifdef JOB_CONTROL
	struct session_s *sp;		/* Session for trace output */
#endif
	int s;				/* Socket for queries */
	int s1;				/* Raw socket for replies */
	struct socket lsocket;		/* Local socket sending queries */
	struct socket rsocket;		/* Final destination of queries */
	int32 cticks;			/* Timer for query replies */
	int32 icsource;			/* Sender of last ICMP reply */
	char ictype;			/* ICMP type last ICMP reply */
	char iccode;			/* ICMP code last ICMP reply */
	int32 lastaddr;			/* Sender of previous ICMP reply */
#if 1
	struct sockaddr_in lsock;
	struct sockaddr_in rsock;
	int i;
#else
	struct sockaddr_in sock;
#endif
	register struct usock *usp;
	register struct sockaddr_in *sinp;
	unsigned char sndttl, q;
	int tracedone = 0;
	int ilookup = 1;		/* Control of inverse domain lookup */
	int c;
	extern int optind;
	char *hostname;
	int save_trace;
	int user_reset = 0;

	optind = 1;
	while((c = getopt(argc,argv,"n")) != EOF){
		switch(c){
		case 'n':
			ilookup = 0;
			break;
		}
	}
	hostname = argv[optind];
#ifdef JOB_CONTROL
	/* Allocate a session descriptor */
	if((sp = newsession(Cmdline,HOP,1)) == NULL){
		_printf(_("Too many sessions\n"));
#ifndef ZILOG
		keywait(NULL,1);
#endif
		return 1;
	}
	sp->inproc = keychar;
#endif
	s = -1;

	/* Setup UDP socket to remote host */
#if 1
	rsock.sin_family = AF_INET;
	rsock.sin_port = Hoprport;
#else
	sock.sin_family = AF_INET;
	sock.sin_port = Hoprport;
#endif
	_printf(_("Resolving %s... "),hostname);
#if 1
	rsock.sin_addr.s_addr = resolve(hostname);
	if(rsock.sin_addr.s_addr == 0){
#else
	if((sock.sin_addr.s_addr = resolve(hostname)) == 0){
#endif
		_printf(_("unknown\n"),hostname);
#ifdef JOB_CONTROL
		freesession(sp, 1); /* does keywait(NULL, 1); beforehand */
#endif
		return 1;
	}

	/* Open socket to remote host */
#if 1
	_printf(_("%s "),psocket((struct sockaddr *)&rsock));
#else
	_printf(_("%s "),psocket((struct sockaddr *)&sock));
#endif
	if((s = socket(AF_INET,SOCK_DGRAM,0)) == -1){
		_printf(_("Can't create udp socket\n"));
#ifdef JOB_CONTROL
		freesession(sp, 1); /* does keywait(NULL, 1); beforehand */
#endif
		return 1;
	}
#if 1
	if(connect(s,(struct sockaddr *)&rsock,sizeof(rsock)) == -1){
#else
	if(connect(s,(struct sockaddr *)&sock,sizeof(sock)) == -1){
#endif
		_printf(_("Connect failed\n"));
#ifdef JOB_CONTROL
		freesession(sp, 1); /* does keywait(NULL, 1); beforehand */
#endif
		return 1;
	}
	if((s1 = socket(AF_INET,SOCK_RAW,ICMP_PTCL)) == -1){
		_printf(_("Can't create raw socket\n"));
		close(s); /* Nick */
#ifdef JOB_CONTROL
		freesession(sp, 1); /* does keywait(NULL, 1); beforehand */
#endif
		return 1;
	}
	_printf(_("\n"));

	/* turn off icmp tracing while hop-checking */
	save_trace = Icmp_trace;
	Icmp_trace = 0;

	/* Setup structures to send queries */
	/* Retrieve socket details for user socket control block */
#if 1
	i = sizeof(lsock);
	getsockname(s, (struct sockaddr *)&lsock, &i);
	lsocket.address = lsock.sin_addr.s_addr;
	lsocket.port = lsock.sin_port;
	rsocket.address = rsock.sin_addr.s_addr;
#else
	usp = itop(s);
	sinp = (struct sockaddr_in *)usp->name;
	lsocket.address = sinp->sin_addr.s_addr;
	lsocket.port = sinp->sin_port;
	sinp = (struct sockaddr_in *)usp->peername;
	rsocket.address = sinp->sin_addr.s_addr;
#endif

	/* Send queries with increasing TTL; start with TTL=1 */
	if (Hoptrace)
		logmsg(s, _("HOPCHECK start trace to %s\n"), hostname/*sp->name*/);
#if 1
	for (sndttl=1; (sndttl < Hopmaxttl); ++sndttl, rsock.sin_port++) {
		/* Increment funny UDP port number each round */
		rsocket.port = rsock.sin_port;
		connect(s, (struct sockaddr *)&rsocket, sizeof(rsocket));
#else
	for (sndttl=1; (sndttl < Hopmaxttl); ++sndttl, sinp->sin_port++) {
		/* Increment funny UDP port number each round */
		rsocket.port = sinp->sin_port;
#endif
		_printf(_("%3d:"),sndttl);
		lastaddr = (int32)0;
		/* Send a round of queries */
		for (q=0; (q < Hopquery); ++q) {
			struct mbuf_s *bp;
			bp = alloc_mbuf(0);
 if (bp == NULL)
  {
  _printf(_("Not enough memory\n"));
  goto done;
  }

			send_udp(&lsocket,&rsocket,0,sndttl,&bp,0,0,0);
			cticks = msclock();
			kalarm( ((long)Hopmaxwait*1000L) );

			/* Wait for a reply to our query */
			if(geticmp(s1,lsocket.port,rsocket.port,
			 &icsource,&ictype,&iccode) == -1){
				if(errno != EALARM){
					user_reset = 1;
					goto done;	/* User reset */
				}
				/* Alarm rang, give up waiting for replies */
				_printf(_(" ***"));
				continue;
			}
			/* Save #ticks taken for reply */
                        cticks = msclock() - cticks;
			/* Report ICMP reply */
			if (icsource != lastaddr) {
				struct rr *save_rrlp, *rrlp;

				if(lastaddr != (int32)0)
					_printf(_("\n    "));
				_printf(_(" %-15s"),inet_ntoa(icsource));
				if(ilookup){
					for(rrlp = save_rrlp = inverse_a(icsource);
					    rrlp != NULL;
					    rrlp = rrlp->next){
						if(rrlp->rdlength > 0){
							switch(rrlp->type){
							case TYPE_PTR:
								_printf(_(" %s"), rrlp->rdata.name);
								goto got_name;
							case TYPE_A:
								_printf(_(" %s"), rrlp->name);
								goto got_name;
							}
#ifdef notdef
							if(rrlp->next != NULL)
								_printf(_("\n%20s")," ");
#endif
						}
					}
					got_name: ;
					free_rr(save_rrlp);

				}
				lastaddr = icsource;
			}
                        _printf(_(" (%ld ms)"),cticks);
#ifdef HOPTRACE
			if (Hoptrace)
			/*RPB*/
			{
				char *duptext1 = NULL,
				     *duptext2 = NULL,
				     *duptext3 = NULL;

				if ((duptext1 = _strdup(Icmptypes[ictype])) != NULL)
				  if ((duptext2 = _strdup(Exceed[iccode])) != NULL)
				    if  ((duptext3 = _strdup(Unreach[iccode])) != NULL)
				    {
				      logmsg(s, _(
				          "(hopcheck) ICMP from %s (%ldms) %s %s"),
				          inet_ntoa(icsource),
				          cticks,
				          duptext1,
				          ((ictype == ICMP_TIME_EXCEED)?
				            duptext2 : duptext3));
				    }
				    else
				      __getfail(Unreach[iccode]);
				  else
				    __getfail(Exceed[iccode]);
				else
				  __getfail(Icmptypes[ictype]);

				free(duptext1);
				free(duptext2);
				free(duptext3);
			}
			/*RPB*/
#endif

			/* Check type of reply */
			if (ictype == ICMP_TIME_EXCEED)
				continue;
			/* Reply was: destination unreachable */
			switch(iccode) {
			case ICMP_PORT_UNREACH:
				++tracedone;
				break;
			case ICMP_NET_UNREACH:
				++tracedone;
				_printf(_(" !N"));
				break;
			case ICMP_HOST_UNREACH:
				++tracedone;
				_printf(_(" !H"));
				break;
			case ICMP_PROT_UNREACH:
				++tracedone;
				_printf(_(" !P"));
				break;
			case ICMP_FRAG_NEEDED:
				++tracedone;
				_printf(_(" !F"));
				break;
			case ICMP_ROUTE_FAIL:
				++tracedone;
				_printf(_(" !S"));
				break;
                        case ICMP_ADMIN_PROHIB:
                                ++tracedone;
                                _printf(_(" !A"));
                                break;
                        default:
                                _printf(_(" !?"));
                                break;
			}
		}
		/* Done with this round of queries */
		kalarm((long)0);
		_printf(_("\n"));
		/* Check if we reached remote host this round */
		if (tracedone != 0)
			break;
	}

	/* Done with traceroute */
done:	close(s);
	s = -1;
	close(s1);
	if(user_reset)
		_printf(_("\n"));	/* May have been in middle of line */
	_printf(_("traceroute done: "));
	Icmp_trace = save_trace;
	if(user_reset){
		_printf(_("user abort\n"));
	} else if (sndttl >= Hopmaxttl) {
		_printf(_("!! maximum TTL exceeded\n"));
	}
	/*RPB*/
	else
	{
		char *duptext1 = NULL,
		     *duptext2 = NULL;

		if ((duptext1 = _strdup(Icmptypes[ictype])) != NULL)
			if ((duptext2 = _strdup(Unreach[iccode])) != NULL)
				if ((icsource == rsocket.address)
				     &&(iccode == ICMP_PORT_UNREACH)) {
					_printf(_("normal (%s %s)\n"), duptext1, duptext2);
				} else {
					_printf(_("!! %s %s\n"), duptext1, duptext2);
				}
			else
				__getfail(Unreach[iccode]);
		else
			__getfail(Icmptypes[ictype]);

		free(duptext1);
		free(duptext2);
	}
	/*RPB*/

#ifdef HOPTRACE
	if (Hoptrace)
		logmsg(s, _("HOPCHECK to %s done"), hostname/*sp->name*/);
#endif
#ifdef JOB_CONTROL
	freesession(sp, 1); /* does keywait(NULL, 1); beforehand */
#endif
	return 0;
}

#ifdef JOB_CONTROL
/* Hop check session keyboard upcall routine -- handles ^C */
static int
keychar(c)
int c;
{
#if 1
	if(c != CTLC)
		return 1;	/* Ignore all but ^C */

	_printf(_("^C\n"));
	fflush(stdout);
	alert(my_thread_p, EABORT);
	return 0;
#else
	switch(c){
	case CTLC:
		alert(my_session_p->proc,EABORT);
		return 0;
	}
	return 1;
#endif
}
#endif

/* Read raw network socket looking for ICMP messages in response to our
 * UDP probes
 */
static int
geticmp(s,lport,fport,sender,type,code)
int s;
uint16 lport;
uint16 fport;
int32 *sender;
char *type,*code;
{
	int size;
	struct icmp icmphdr;
	struct ip iphdr;
	struct udp udphdr;
	struct mbuf_s *bp;
	struct sockaddr_in sock;

	for(;;){
		size = sizeof(sock);
#if 1 /* slower but eventually allows hop to be moved out of the kernel */
		/* note maximum received packet size is based on the 4 bit */
		/* field "ihl" in the ip header, which is expressed in dword */
		/* units - so the maximum is 0xf << 2, plus the UDP header */
		/* (we also assume this total will be greater than ICMPLEN) */
		bp = alloc_mbuf((0xf << 2) + UDPHDR);
 if (bp == NULL)
  {
  return -1;
  }
		if ((bp->cnt = recvfrom(s, bp->data, (0xf << 2) + UDPHDR, 0,
				(struct sockaddr *)&sock, &size)) <= 0)
			{
			free_mbuf(&bp);
			return -1;
			}
#else
		if(recv_mbuf(s,&bp,0,(struct sockaddr *)&sock,&size) == -1)
			return -1;
#endif
		/* It's an ICMP message, let's see if it's interesting */
		ntohicmp(&icmphdr,&bp);
		if((icmphdr.type != ICMP_TIME_EXCEED ||
		 icmphdr.code != ICMP_TTL_EXCEED)
		 && icmphdr.type != ICMP_DEST_UNREACH){
			/* We're not interested in these */
			free_p(&bp);
			continue;
		}
		ntohip(&iphdr,&bp);
		if(iphdr.protocol != UDP_PTCL){
			/* Not UDP, so can't be interesting */
			free_p(&bp);
			continue;
		}
		ntohudp(&udphdr,&bp);
		if(udphdr.dest != fport || udphdr.source != lport){
			/* Not from our hopcheck session */
			free_p(&bp);
			continue;
		}
		/* Passed all of our checks, so return it */
		*sender = sock.sin_addr.s_addr;
		*type = icmphdr.type;
		*code = icmphdr.code;
		free_p(&bp);
		return 0;
	}
}
