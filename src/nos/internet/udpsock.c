/* udpsock.c */

#include <errno.h> /* Nick */
#include "nos/global.h"
#include "internet/udp.h"
#include "socket/socket.h"
#include "socket/usock.h"

static void s_urcall(struct iface_s *iface,struct udp_cb *udp,int cnt);
static void autobind(struct usock *up);

int
so_udp(up,protocol)
struct usock *up;
int protocol;
{
	return 0;
}
int
so_udp_bind(up)
struct usock *up;
{
#if 0
	int s;
#endif
	struct sockaddr_in *sp;
	struct socket lsock;

#if 0
	s = up->index;
#endif
	sp = (struct sockaddr_in *)up->name;
	lsock.address = sp->sin_addr.s_addr;
	lsock.port = sp->sin_port;
	up->cb.udp = open_udp(&lsock,s_urcall);
#if 1
	up->cb.udp->user = up;
#else
	up->cb.udp->user = s;
#endif
	return 0;
}
int
so_udp_conn(up)
struct usock *up;
{
	if(up->name == NULL){
		autobind(up);
	}
	return 0;
}
int
so_udp_recv(up,buf,cnt,/*bpp,*/from,fromlen)
struct usock *up;
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
void *buf;
int cnt;
#else
struct mbuf_s **bpp;
#endif
struct sockaddr *from;
int *fromlen;
{
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
	struct mbuf_s *bp;
#else
	int cnt;
#endif
	struct udp_cb *udp;
	struct sockaddr_in *remote;
	struct socket fsocket;

	while((udp = up->cb.udp) != NULL
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
	&& recv_udp(udp,&fsocket,&bp) == -1){
#else
	&& (cnt = recv_udp(udp,&fsocket,bpp)) == -1){
#endif
		if(up->noblock){
			errno = EWOULDBLOCK;
			return -1;
		} else if((errno = kwait(up)) != 0){
			return -1;
		}
	}
	if(udp == NULL){
		/* Connection went away */
		errno = ENOTCONN;
		return -1;
	}
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
	cnt = pullup(&bp, buf, (uint16)cnt);
	free_p(&bp);
#endif
	if(from != NULL && fromlen != (int *)NULL && *fromlen >= SOCKSIZE){
		remote = (struct sockaddr_in *)from;
		remote->sin_family = AF_INET;
		remote->sin_addr.s_addr = fsocket.address;
		remote->sin_port = fsocket.port;
		*fromlen = SOCKSIZE;
	}
	return cnt;
}
int
so_udp_send(
struct usock *up,
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
void *buf,
int cnt,
#else
struct mbuf_s **bpp,
#endif
struct sockaddr *to
){
	struct sockaddr_in *local,*remote;
	struct socket lsock,fsock;
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
	struct mbuf_s *bp;
#endif

	if(up->name == NULL)
		autobind(up);
	local = (struct sockaddr_in *)up->name;
	lsock.address = local->sin_addr.s_addr;
	lsock.port = local->sin_port;
	if(to != NULL) {
		remote = (struct sockaddr_in *)to;
	} else if(up->peername != NULL){
		remote = (struct sockaddr_in *)up->peername;
	} else {
#if 0 /* now that we don't have to support recv_mbuf() or send_mbuf() */
		free_p(bpp);
#endif
		errno = ENOTCONN;
		return -1;
	}
	fsock.address = remote->sin_addr.s_addr;
	fsock.port = remote->sin_port;

#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
	bp = qdata(buf, cnt);
	if (bp == NULL)
		{
		errno = ENOMEM;
		return -1;
		}
	send_udp(&lsock,&fsock,up->tos,0,&bp,0,0,0);
	return cnt;
#else
	send_udp(&lsock,&fsock,up->tos,0,bpp,0,0,0);
	return 0;
#endif
}
int
so_udp_qlen(up,rtx)
struct usock *up;
int rtx;
{
	int len;

	switch(rtx){
	case 0:
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
		len = up->cb.udp->rcvq.cnt;
#else
		len = up->cb.udp->rcvcnt;
#endif
		break;
	case 1:
		len = 0;
		break;
	}
	return len;
}
int
so_udp_close(up)
struct usock *up;
{
	if(up->cb.udp != NULL){
		del_udp(up->cb.udp);
	}
	return 0;
}
#if 0 /* not referenced (see net/socket.c shutdown() routine */
int
so_udp_shut(up,how)
struct usock *up;
int how;
{
	int s;

	s = up->index;
	close/*_s*/(s);
	return 0;
}
#endif
static void
s_urcall(iface,udp,cnt)
struct iface_s *iface;
struct udp_cb *udp;
int cnt;
{
#if 1
	ksignal(udp->user, 1);
#else
	ksignal(itop(udp->user),1);
#endif
	kwait(NULL);
}

/* Issue an automatic bind of a local address */
static void
autobind(up)
struct usock *up;
{
	struct sockaddr_in local;
	int s;

	s = up->index;
	local.sin_family = AF_INET;
	local.sin_addr.s_addr = INADDR_ANY;
	local.sin_port = Lport++;
	bind(s,(struct sockaddr *)&local,sizeof(struct sockaddr_in));
}
int
so_udp_stat(up)
struct usock *up;
{
	st_udp(up->cb.udp,0);
	return 0;
}
