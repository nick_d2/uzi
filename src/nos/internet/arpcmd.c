/* ARP commands
 * Copyright 1991, Phil Karn, KA9Q
 *
 * Apr 04	RPB     Po-ed most of the remaining strings
 */
#include <stdio.h>
#include <ctype.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include "main/timer.h"
/*#include "x86/enet.h"*/
/*#include "ax25/ax25.h"*/
#include "internet/arp.h"
#include "internet/netuser.h"
#include "main/cmdparse.h"
#include "main/commands.h"
#include "main/smsg.h"
#include <libintl.h>
#include "po/messages.h"

static int doarpadd(int argc,char *argv[],void *p);
static int doarpdrop(int argc,char *argv[],void *p);
static int doarpflush(int argc,char *argv[],void *p);
static void dumparp(void);

static struct cmds Arpcmds[] = {
	N_("add"), doarpadd, 0, 4,
	N_("arp add <hostid> ether|ax25|netrom|arcnet <ether addr|callsign>"),

	N_("drop"), doarpdrop, 0, 3,
	N_("arp drop <hostid> ether|ax25|netrom|arcnet"),

	N_("flush"), doarpflush, 0, 0,
	NULL,

	N_("publish"), doarpadd, 0, 4,
	N_("arp publish <hostid> ether|ax25|netrom|arcnet <ether addr|callsign>"),

	NULL,
};
_char *Arptypes[ARPTYPES_MAX] = {
	N_("NET/ROM"),
	N_("10 Mb Ethernet"),
	N_("3 Mb Ethernet"),
	N_("AX.25"),
	N_("Pronet"),
	N_("Chaos"),
	N_(""),
	N_("Arcnet"),
	N_("Appletalk")
};

int
doarp(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	if(argc < 2){
		dumparp();
		return 0;
	}
	return subcmd(Arpcmds,argc,argv,p);
}
static
doarpadd(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	uint16 hardware;
	int32 addr;
	uint8 *hwaddr;
	struct arp_tab *ap;
	struct arp_type *at;
	int pub = 0;

	if(argv[0][0] == 'p')	/* Is this entry published? */
		pub = 1;
	if((addr = resolve(argv[1])) == 0){
		_printf(Badhost,argv[1]);
		return 1;
	}
	/* This is a kludge. It really ought to be table driven */
	switch(tolower(argv[2][0])){
	case 'n':	/* Net/Rom pseudo-type */
		hardware = ARP_NETROM;
		break;
	case 'e':	/* "ether" */
		hardware = ARP_ETHER;
		break;
	case 'a':	/* "ax25" */
		switch(tolower(argv[2][1])) {
		case 'x':
			hardware = ARP_AX25;
			break;
		case 'r':
			hardware = ARP_ARCNET;
			break;
		default:
			_printf(_("unknown hardware type \"%s\"\n"),argv[2]);
			return -1;
		}
		break;
	case 'm':	/* "mac appletalk" */
		hardware = ARP_APPLETALK;
		break;
	default:
		_printf(_("unknown hardware type \"%s\"\n"),argv[2]);
		return -1;
	}
	/* If an entry already exists, clear it */
	if((ap = arp_lookup(hardware,addr)) != NULL)
		arp_drop(ap);

	at = &Arp_type[hardware];
	if(at->scan == NULL){
		_printf(_("Attach device first\n"));
		return 1;
	}
	/* Allocate buffer for hardware address and fill with remaining args */
	hwaddr = mallocw(at->hwalen);
	/* Destination address */
	(*at->scan)(hwaddr,argv[3]);
	ap = arp_add(addr,hardware,hwaddr,pub);	/* Put in table */
	free(hwaddr);				/* Clean up */
	stop_timer(&ap->timer);			/* Make entry permanent */
	set_timer(&ap->timer,0L);
	return 0;
}
/* Remove an ARP entry */
static
doarpdrop(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	uint16 hardware;
	int32 addr;
	struct arp_tab *ap;

	if((addr = resolve(argv[1])) == 0){
		_printf(Badhost,argv[1]);
		return 1;
	}
	/* This is a kludge. It really ought to be table driven */
	switch(tolower(argv[2][0])){
	case 'n':
		hardware = ARP_NETROM;
		break;
	case 'e':	/* "ether" */
		hardware = ARP_ETHER;
		break;
	case 'a':	/* "ax25" */
		switch(tolower(argv[2][1])) {
		case 'x':
			hardware = ARP_AX25;
			break;
		case 'r':
			hardware = ARP_ARCNET;
			break;
		default:
			hardware = 0;
			break;
		}
		break;
	case 'm':	/* "mac appletalk" */
		hardware = ARP_APPLETALK;
		break;
	default:
		hardware = 0;
		break;
	}
	if((ap = arp_lookup(hardware,addr)) == NULL)
		return -1;
	arp_drop(ap);
	return 0;
}
/* Flush all automatic entries in the arp cache */
static int
doarpflush(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct arp_tab *ap;
	struct arp_tab *aptmp;
	int i;

	for(i=0;i<HASHMOD;i++){
		for(ap = Arp_tab[i];ap != NULL;ap = aptmp){
			aptmp = ap->next;
			if(dur_timer(&ap->timer) != 0)
				arp_drop(ap);
		}
	}
	return 0;
}

/* Dump ARP table */
static void
dumparp()
{
	register int i;
	register struct arp_tab *ap;
	char e[128];
	char *duptext; /*RPB*/

	_printf(_("received %u badtype %u bogus addr %u reqst in %u replies %u reqst out %u\n"),
	 Arp_stat.recv,Arp_stat.badtype,Arp_stat.badaddr,Arp_stat.inreq,
	 Arp_stat.replies,Arp_stat.outreq);

	_printf(_("IP addr         Type           Time Q Addr\n"));
	for(i=0;i<HASHMOD;i++){
		for(ap = Arp_tab[i];ap != (struct arp_tab *)NULL;ap = ap->next){
			_printf(_("%-16s"),inet_ntoa(ap->ip_addr));
			/*RPB*/
			duptext = smsg(Arptypes,NHWTYPES,ap->hardware);
			_printf(_("%-15s"), duptext);
			free(duptext);
			/*RPB*/
			_printf(_("%-5ld"),read_timer(&ap->timer)/1000L);
			if(ap->state == ARP_PENDING)
				_printf(_("%-2u"),len_q(ap->pending));
			else
				_printf(_("  "));
			if(ap->state == ARP_VALID){
				if(Arp_type[ap->hardware].format != NULL){
					(*Arp_type[ap->hardware].format)(e,ap->hw_addr);
				} else {
					e[0] = '\0';
				}
				_printf(_("%s"),e);
			} else {
				_printf(_("[unknown]"));
			}
			if(ap->pub)
				_printf(_(" (published)"));
			_printf(_("\n"));
		}
	}
}
