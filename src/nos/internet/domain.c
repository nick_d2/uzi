/*
 *	DOMAIN.C -- domain name system stub resolver
 *
 *	Original code by Phil Karn, KA9Q.
 *
 *	Apr 90	Bill Simpson added address->name resolution, time-to-live,
 *	thru	memory caching, generalized multi-record multi-type searches,
 *	Oct 90	and many minor changes to conform more closely to the RFCs.
 *	Feb 91	Bill Simpson added "query" command and TYPE_ANY processing.
 *	Jul 91	Bill Simpson added "more" sessions for query and cache list.
 *	Apr 04	RPB  Po-ed most of the remaining strings
 */

#include <stdio.h>
#include <errno.h> /* Nick */
#include <sgtty.h> /* Nick */
#include <ctype.h>
#include <time.h>
#include <sys/stat.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include "kernel/thread.h"
#include "internet/ip.h"
#include "internet/netuser.h"
#ifdef JOB_CONTROL
#include "kernel/session.h"
#endif
#include "socket/socket.h"
#include "main/cmdparse.h"
#include "main/commands.h"
#include "main/files.h"
#include "main/main.h"
#include "internet/domain.h"
#include "kernel/process.h"
#include <libintl.h>
#include "po/messages.h"
#include "main/logmsg.h"

#define DTYPES_MAX 17
#define DELIM_MAX 5
#define DCMDS_MAX 9
#define DCACHECMDS_MAX 5

#ifdef MODULE
#define STATIC
extern struct rr *Dcache;
extern int Dcache_size;
extern time_t Dcache_time;

extern int Dfile_clean;
extern int Dfile_reading;
extern int Dfile_writing;

extern int32 Dfile_wait_absolute;
extern int Dfile_wait_relative;

extern struct dserver *Dservers;
extern int Dserver_retries;

extern char *Dsuffix;
extern int Dtrace;
extern _char *Dtypes[DTYPES_MAX];
/*extern int Ndtypes;*/
extern char delim[DELIM_MAX];

extern struct cmds Dcmds[DCMDS_MAX];
extern struct cmds Dcachecmds[DCACHECMDS_MAX];
#else
#define STATIC static
#define MODULE_Dcache
#define MODULE_Dcache_size
#define MODULE_Dcache_time
#define MODULE_Dfile_clean
#define MODULE_Dfile_reading
#define MODULE_Dfile_writing
#define MODULE_Dfile_updater
#define MODULE_Dfile_wait_absolute
#define MODULE_Dfile_wait_relative
#define MODULE_Dservers
#define MODULE_Dserver_retries
#define MODULE_Dsuffix
#define MODULE_Dtrace
#define MODULE_Dtypes
/*#define MODULE_Ndtypes*/
#define MODULE_delim
#define MODULE_Dcmds
#define MODULE_Dcachecmds
#define MODULE_dodomain
#define MODULE_docache
#define MODULE_dosuffix
#define MODULE_docacheclean
#define MODULE_docachelist
#define MODULE_docachesize
#define MODULE_docachewait
#define MODULE_dlist_add
#define MODULE_dlist_drop
#define MODULE_dodnsadd
#define MODULE_add_nameserver
#define MODULE_dodnsdrop
#define MODULE_dodnslist
#define MODULE_dodnsquery
#define MODULE_dodnsretry
#define MODULE_dodnstrace
#define MODULE_dtype
#define MODULE_check_ttl
#define MODULE_compare_rr
#define MODULE_compare_rr_list
#define MODULE_copy_rr
#define MODULE_copy_rr_list
#define MODULE_free_rr
#define MODULE_make_rr
#define MODULE_dcache_add
#define MODULE_dcache_drop
#define MODULE_dcache_search
#define MODULE_dcache_update
#define MODULE_get_rr
#define MODULE_put_rr
#define MODULE_dfile_search
#define MODULE_dfile_update
#define MODULE_dumpdomain
#define MODULE_dns_makequery
#define MODULE_dns_query
#define MODULE_isaddr
#define MODULE_checksuffix
#define MODULE_resolver
#define MODULE_inverse_a
#define MODULE_resolve_rr
#define MODULE_resolve_a
#define MODULE_resolve
#define MODULE_resolve_mx
#define MODULE_resolve_mailb
#endif

#undef	DEBUG				/* for certain trace messages */
#undef	DEBUG_PAIN			/* for painful debugging */

#ifdef MODULE_Dcache
STATIC struct rr *Dcache = NULL;	/* Cache of resource records */
#endif
#ifdef MODULE_Dcache_size
STATIC int Dcache_size = 20;		/* size limit */
#endif
#ifdef MODULE_Dcache_time
STATIC time_t Dcache_time = 0L; 	/* timestamp */
#endif

#ifdef MODULE_Dfile_clean
STATIC int Dfile_clean = FALSE; 	/* discard expired records (flag) */
#endif
#ifdef MODULE_Dfile_reading
STATIC int Dfile_reading = 0;		/* read interlock (count) */
#endif
#ifdef MODULE_Dfile_writing
STATIC int Dfile_writing = 0;		/* write interlock (count) */
#endif

#ifdef MODULE_Dfile_updater
struct thread_s *Dfile_updater = NULL;
#endif
#ifdef MODULE_Dfile_wait_absolute
STATIC int32 Dfile_wait_absolute = 0L;	/* timeout Clock time */
#endif
#ifdef MODULE_Dfile_wait_relative
STATIC int Dfile_wait_relative = 300;	/* timeout file activity (seconds) */
#endif

#ifdef MODULE_Dservers
STATIC struct dserver *Dservers = NULL; /* List of potential servers */
#endif
#ifdef MODULE_Dserver_retries
STATIC int Dserver_retries = 2;		/* Attempts to reach servers */
#endif

#ifdef MODULE_Dsuffix
STATIC char *Dsuffix = NULL;	/* Default suffix for names without periods */
#endif
#ifdef MODULE_Dtrace
STATIC int Dtrace = FALSE;
#endif
#ifdef MODULE_Dtypes
STATIC _char *Dtypes[DTYPES_MAX] = {
	N_(""),
	N_("A"),
	N_("NS"),
	N_("MD"),
	N_("MF"),
	N_("CNAME"),
	N_("SOA"),
	N_("MB"),
	N_("MG"),
	N_("MR"),
	N_("NULL"),
	N_("WKS"),
	N_("PTR"),
	N_("HINFO"),
	N_("MINFO"),
	N_("MX"),
	N_("TXT")
};
#endif
#if 0 /*def MODULE_Ndtypes*/
STATIC int Ndtypes = 17;
#endif
#ifdef MODULE_delim
STATIC char delim[DELIM_MAX] = " \t\r\n";
#endif

STATIC int docache(int argc,char *argv[],void *p);
STATIC int dosuffix(int argc,char *argv[],void *p);

STATIC int docacheclean(int argc,char *argv[],void *p);
STATIC int docachelist(int argc,char *argv[],void *p);
STATIC int docachesize(int argc,char *argv[],void *p);
STATIC int docachewait(int argc,char *argv[],void *p);

STATIC void dlist_add(struct dserver *dp);
STATIC void dlist_drop(struct dserver *dp);
STATIC int dodnsadd(int argc,char *argv[],void *p);
STATIC int dodnsdrop(int argc,char *argv[],void *p);
STATIC int dodnslist(int argc,char *argv[],void *p);
STATIC int dodnsquery(int argc,char *argv[],void *p);
STATIC int dodnsretry(int argc,char *argv[],void *p);
STATIC int dodnstrace(int argc,char *argv[],void *p);

STATIC char * dtype(int value);
STATIC int check_ttl(struct rr *rrlp);
STATIC int compare_rr(struct rr *search_rrp,struct rr *target_rrp);
STATIC int compare_rr_list(struct rr *rrlp,struct rr *target_rrp);
STATIC struct rr *copy_rr(struct rr *rrp);
STATIC struct rr *copy_rr_list(struct rr *rrlp);
STATIC struct rr *make_rr(int source,
	char *dname,uint16 class,uint16 type,int32 ttl,uint16 rdl,void *data);

STATIC void dcache_add(struct rr *rrlp);
STATIC void dcache_drop(struct rr *rrp);
STATIC struct rr *dcache_search(struct rr *rrlp);
STATIC void dcache_update(struct rr *rrlp);

STATIC struct rr *get_rr(FILE *fp, struct rr *lastrrp);
STATIC void put_rr(FILE *fp,struct rr *rrp);
STATIC struct rr *dfile_search(struct rr *rrlp);
STATIC void dfile_update(int s,void *unused,void *p);

STATIC void dumpdomain(struct dhdr *dhp,int32 rtt);
STATIC int dns_makequery(uint16 op,struct rr *rrp,
	uint8 *buffer,uint16 buflen);
STATIC int dns_query(struct rr *rrlp);

STATIC int isaddr(char *s);
STATIC char *checksuffix(char *dname);
STATIC struct rr *resolver(struct rr *rrlp);

/**
 **	Domain Resolver Commands
 **/

#ifdef MODULE_Dcmds
STATIC struct cmds Dcmds[DCMDS_MAX] = {
	N_("addserver"),	dodnsadd,	0, 2, N_("add <hostid>"),
	N_("dropserver"),	dodnsdrop,	0, 2, N_("drop <hostid>"),
	N_("list"),		dodnslist,	0, 0, NULL,
	N_("query"),		dodnsquery,   512, 2, N_("query <hostid>"),
	N_("retry"),		dodnsretry,	0, 0, NULL,
	N_("suffix"),		dosuffix,	0, 0, NULL,
	N_("trace"),		dodnstrace,	0, 0, NULL,
	N_("cache"),		docache,	0, 0, NULL,
	NULL,
};
#endif

#ifdef MODULE_Dcachecmds
STATIC struct cmds Dcachecmds[DCACHECMDS_MAX] = {
	N_("clean"),	docacheclean,	0, 0, NULL,
	N_("list"),	docachelist,  512, 0, NULL,
	N_("size"),	docachesize,	0, 0, NULL,
	N_("wait"),	docachewait,	0, 0, NULL,
	NULL,
};
#endif

#ifdef MODULE_dodomain
int
dodomain(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return subcmd(Dcmds,argc,argv,p);
}
#endif

#ifdef MODULE_docache
STATIC int
docache(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return subcmd(Dcachecmds,argc,argv,p);
}
#endif

#ifdef MODULE_dosuffix
STATIC int
dosuffix(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	if(argc < 2){
		if(Dsuffix != NULL)
			_printf(_("%s\n"),Dsuffix);
		return 0;
	}
	free(Dsuffix);
	Dsuffix = strdupw(argv[1]);
	return 0;
}
#endif

#ifdef MODULE_docacheclean
STATIC int
docacheclean(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return setbool(&Dfile_clean, _("discard expired records"), argc, argv);
}
#endif

#ifdef MODULE_docachelist
STATIC int
docachelist(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct rr *rrp;
#ifdef JOB_CONTROL
	struct session_s *sp;
#endif
#ifndef ZILOG /* bad luck... could be reinstated after fixing keywait() ?? */
	int row = 25;
#endif
#if 1 /* Nick */
	struct sgttyb state;
	int raw;
#endif

#ifdef JOB_CONTROL
	if((sp = newsession(Cmdline,DCLIST,1)) == NULL){
		return -1;
	}
#endif

	(void)dcache_search(NULL); /* update ttl */

	/* Put tty into raw mode so single-char responses will work */
#if 1 /* Nick */
	gtty(STDIN_FILENO, &state);
	raw = state.sg_flags;
	state.sg_flags |= RAW;
	state.sg_flags &= ~ECHO;
	stty(STDIN_FILENO, &state);
#else
	sp->ttystate.echo = sp->ttystate.edit = 0;
#endif

	for(rrp=Dcache;rrp!=NULL;rrp=rrp->next)
	{
		put_rr(stdout,rrp);
#ifndef ZILOG /* bad luck... could be reinstated after fixing keywait() ?? */
		if(--row == 0){
			row = keywait("--More--",0);
			switch(row){
			case -1:
			case 'q':
			case 'Q':
				rrp = NULL;
				break;
			case '\n':
			case '\r':
				row = 1;
				break;
			case ' ':
			default:
				row = 25;
			};
		}
#endif
	}
	fflush(stdout);
#if 1 /* Nick */
	state.sg_flags = raw;
	stty(STDIN_FILENO, &state);
#endif
#ifdef JOB_CONTROL
	freesession(sp, 1); /* does keywait(NULL, 1); beforehand */
#endif
	return 0;
}
#endif

#ifdef MODULE_docachesize
STATIC int
docachesize(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	int newsize;
	int oldsize;
	int result;

	newsize = oldsize = Dcache_size;
	result = setint(&newsize, _("memory cache size"), argc, argv);

	if(newsize > 0){
		Dcache_size = newsize;
		if(newsize < oldsize){
			(void)dcache_search(NULL); /* update size */
		}
	}
	return result;
}
#endif

#ifdef MODULE_docachewait
STATIC int
docachewait(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return setint(&Dfile_wait_relative,
	       _("time before file update (seconds)"), argc,argv );
}
#endif

#ifdef MODULE_dlist_add
STATIC void
dlist_add(dp)
register struct dserver *dp;
{
	dp->prev = NULL;
	dp->next = Dservers;
	if(Dservers != NULL)
		Dservers->prev = dp;
	Dservers = dp;
}
#endif

#ifdef MODULE_dlist_drop
STATIC void
dlist_drop(dp)
register struct dserver *dp;
{
	if(dp->prev != NULL)
		dp->prev->next = dp->next;
	else
		Dservers = dp->next;
	if(dp->next != NULL)
		dp->next->prev = dp->prev;
}
#endif

#ifdef MODULE_dodnsadd
STATIC int
dodnsadd(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	int32 address;

	if((address = resolve(argv[1])) == 0L){
		_printf(_("Resolver %s unknown\n"),argv[1]);
		return 1;
	}
	return add_nameserver(address);
}
#endif

#ifdef MODULE_add_nameserver
int
add_nameserver(address)
int32 address;
{
	struct dserver *dp;

	dp = (struct dserver *)callocw(1,sizeof(struct dserver));
	dp->address = address;
	dp->srtt = INITRTT;
	dp->mdev = 0;
	dp->timeout = 2 * dp->mdev + dp->srtt + 3;
	dlist_add(dp);
	return 0;
}
#endif

#ifdef MODULE_dodnsdrop
STATIC int
dodnsdrop(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct dserver *dp;
	int32 addr;

	addr = resolve(argv[1]);
	for(dp = Dservers;dp != NULL;dp = dp->next)
		if(addr == dp->address)
			break;

	if(dp == NULL){
		_printf(_("Not found\n"));
		return 1;
	}

	dlist_drop(dp);
	free(dp);
	return 0;
}
#endif

#ifdef MODULE_dodnslist
STATIC int
dodnslist(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct dserver *dp;

	_printf(_("Server address          srtt    mdev   timeout   queries responses\n"));
	for(dp = Dservers;dp != NULL;dp = dp->next){
		_printf(_("%-20s%8lu%8lu%10lu%10lu%10lu\n"),
		 inet_ntoa(dp->address),
		 dp->srtt,dp->mdev,dp->timeout,
		 dp->queries,dp->responses);
	}
	return 0;
}
#endif

#ifdef MODULE_dodnsquery
STATIC int
dodnsquery(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct rr *rrp;
	struct rr *result_rrlp;
	char *sname;
#ifdef JOB_CONTROL
	struct session_s *sp;
#endif
#ifndef ZILOG /* bad luck... could be reinstated after fixing keywait() ?? */
	int row = 25;
#endif
#if 1 /* Nick */
	struct sgttyb state;
	int raw;
#endif

#ifdef JOB_CONTROL
	if((sp = newsession(Cmdline,DQUERY,1)) == NULL){
		return -1;
	}
#endif

	if ( isaddr( argv[1] ) ) {
		result_rrlp = inverse_a( aton( argv[1] ) );
	} else {
		sname = checksuffix( argv[1] );
		rrp = make_rr(RR_QUERY,sname,CLASS_IN,TYPE_ANY,0,0,NULL);
		FREE(sname);

		dns_query(rrp);
		result_rrlp = dcache_search(rrp);
		free_rr(rrp);
	}

	/* Put tty into raw mode so single-char responses will work */
#if 1 /* Nick */
	gtty(STDIN_FILENO, &state);
	raw = state.sg_flags;
	state.sg_flags |= RAW;
	state.sg_flags &= ~ECHO;
	stty(STDIN_FILENO, &state);
#else
	sp->ttystate.echo = sp->ttystate.edit = 0;
#endif

	for( rrp=result_rrlp; rrp!=NULL; rrp=rrp->next)
	{
		put_rr(stdout,rrp);
#ifndef ZILOG /* bad luck... could be reinstated after fixing keywait() ?? */
		if(--row == 0){
			row = keywait("--More--",0);
			switch(row){
			case -1:
			case 'q':
			case 'Q':
				rrp = NULL;
				break;
			case '\n':
			case '\r':
				row = 1;
				break;
			case ' ':
			default:
				row = 25;
			};
		}
#endif
	}
	fflush(stdout);
	free_rr(result_rrlp);
#if 1 /* Nick */
	state.sg_flags = raw;
	stty(STDIN_FILENO, &state);
#endif
#ifdef JOB_CONTROL
	freesession(sp, 1); /* does keywait(NULL, 1); beforehand */
#endif
	return 0;
}
#endif

#ifdef MODULE_dodnsretry
STATIC int
dodnsretry(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return setint(&Dserver_retries, _("server retries"), argc, argv);
}
#endif

#ifdef MODULE_dodnstrace
STATIC int
dodnstrace(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return setbool(&Dtrace, _("server trace"), argc, argv);
}
#endif


/**
 **	Domain Resource Record Utilities
 **/

#ifdef MODULE_dtype
STATIC char *
dtype(value)
int value;
{
	char *buf;	/*RPB*/
	char *duptext;	/*RPB*/

	if (value < DTYPES_MAX /*Ndtypes*/)
/*RPB*/
	{
		if ((duptext = _strdup(Dtypes[value])) == NULL)
		{
			__getfail(Dtypes[value]);
			return NULL;
		}

		return duptext;
	}

	if ((buf = mallocw(10)) == NULL)
		return NULL;
/*RPB*/

	_sprintf( buf, _("{%d}"), value);
	return buf;
}
#endif

/* check list of resource records for any expired ones.
 * returns number of expired records.
 */
#ifdef MODULE_check_ttl
STATIC int
check_ttl(rrlp)
register struct rr *rrlp;
{
	int count = 0;

	while(rrlp != NULL){
		if(rrlp->ttl == 0L)
			count++;
		rrlp = rrlp->next;
	}
	return count;
}
#endif


/* Compare two resource records.
 * returns 0 if match, nonzero otherwise.
 */
#ifdef MODULE_compare_rr
STATIC int
compare_rr(search_rrp,target_rrp)
register struct rr *search_rrp,*target_rrp;
{
	int i;

	if(search_rrp == NULL || target_rrp == NULL)
		return -32765;

	if(search_rrp->class != target_rrp->class)
		return -32763;

	if(search_rrp->type != TYPE_ANY
	&& search_rrp->type != target_rrp->type
	&& (search_rrp->source != RR_QUERY
	 || (target_rrp->type != TYPE_CNAME
	  && target_rrp->type != TYPE_PTR)))
		return -32761;

	if(search_rrp->source != RR_INQUERY){
		if((i = strlen(search_rrp->name)) != strlen(target_rrp->name))
			return -32759;
		if((i = strnicmp(search_rrp->name,target_rrp->name,i)) != 0)
			return i;

		/* match negative records so that they are replaced */
		if(target_rrp->rdlength == 0)
			return 0;
	}

	/* if a query has gotten this far, match it */
	if(search_rrp->source == RR_QUERY)
		return 0;

	/* ensure negative records don't replace older records */
	if(search_rrp->rdlength == 0)
		return -32757;

	/* match expired records so that they are replaced */
	if(search_rrp->source != RR_INQUERY){
		if(target_rrp->ttl == 0L)
			return 0;
	}

	/* Note: rdlengths are not compared because they vary depending
	 * on the representation (ASCII or encoded) this record was
	 * generated from.
	 */

	switch(search_rrp->type){
	case TYPE_A:
		i = search_rrp->rdata.addr != target_rrp->rdata.addr;
		break;
	case TYPE_CNAME:
	case TYPE_MB:
	case TYPE_MG:
	case TYPE_MR:
	case TYPE_NS:
	case TYPE_PTR:
	case TYPE_TXT:
		i = stricmp(search_rrp->rdata.data,target_rrp->rdata.data);
		break;
	case TYPE_HINFO:
		i = strcmp(search_rrp->rdata.hinfo.cpu,target_rrp->rdata.hinfo.cpu) ||
			strcmp(search_rrp->rdata.hinfo.os,target_rrp->rdata.hinfo.os);
		break;
	case TYPE_MX:
		i = stricmp(search_rrp->rdata.mx.exch,target_rrp->rdata.mx.exch);
		break;
	case TYPE_SOA:
		i = search_rrp->rdata.soa.serial != target_rrp->rdata.soa.serial;
		break;
	default:
		i = -32755;	/* unsupported */
	}
	return i;
}
#endif

#ifdef MODULE_compare_rr_list
STATIC int
compare_rr_list(rrlp,target_rrp)
register struct rr *rrlp,*target_rrp;
{
	while(rrlp != NULL){
		if(compare_rr(rrlp,target_rrp) == 0)
			return 0;
#ifdef DEBUG_PAIN
		if(Dtrace)
			_printf(_("%15d %s\n"),
				compare_rr(rrlp,target_rrp),
				target_rrp->name);
#endif
		rrlp = rrlp->next;
	}
	return -32767;
}
#endif


/* Make a new copy of a resource record */
#ifdef MODULE_copy_rr
STATIC struct rr *
copy_rr(rrp)
register struct rr *rrp;
{
	register struct rr *newrr;

	if(rrp == NULL)
		return NULL;

	newrr = (struct rr *)callocw(1,sizeof(struct rr));
	newrr->source =	rrp->source;
	newrr->name =	strdupw(rrp->name);
	newrr->type =	rrp->type;
	newrr->class =	rrp->class;
	newrr->ttl =	rrp->ttl;
	if((newrr->rdlength = rrp->rdlength) == 0)
		return newrr;

	switch(rrp->type){
	case TYPE_A:
		newrr->rdata.addr = rrp->rdata.addr;
		break;
	case TYPE_CNAME:
	case TYPE_MB:
	case TYPE_MG:
	case TYPE_MR:
	case TYPE_NS:
	case TYPE_PTR:
	case TYPE_TXT:
		newrr->rdata.name = strdupw(rrp->rdata.name);
		break;
	case TYPE_HINFO:
		newrr->rdata.hinfo.cpu = strdupw(rrp->rdata.hinfo.cpu);
		newrr->rdata.hinfo.os = strdupw(rrp->rdata.hinfo.os);
		break;
	case TYPE_MX:
		newrr->rdata.mx.pref = rrp->rdata.mx.pref;
		newrr->rdata.mx.exch = strdupw(rrp->rdata.mx.exch);
		break;
	case TYPE_SOA:
		newrr->rdata.soa.mname = 	strdupw(rrp->rdata.soa.mname);
		newrr->rdata.soa.rname = 	strdupw(rrp->rdata.soa.rname);
		newrr->rdata.soa.serial = 	rrp->rdata.soa.serial;
		newrr->rdata.soa.refresh = 	rrp->rdata.soa.refresh;
		newrr->rdata.soa.retry = 	rrp->rdata.soa.retry;
		newrr->rdata.soa.expire = 	rrp->rdata.soa.expire;
		newrr->rdata.soa.minimum = 	rrp->rdata.soa.minimum;
		break;
	}
	return newrr;
}
#endif

#ifdef MODULE_copy_rr_list
STATIC struct rr *
copy_rr_list(rrlp)
register struct rr *rrlp;
{
	register struct rr **rrpp;
	struct rr *result_rrlp;

	rrpp = &result_rrlp;
	while(rrlp != NULL){
		*rrpp = copy_rr(rrlp);
		rrpp = &(*rrpp)->next;
		rrlp = rrlp->next;
	}
	*rrpp = NULL;
	return result_rrlp;
}
#endif


/* Free (list of) resource records */
#ifdef MODULE_free_rr
void
free_rr(rrlp)
register struct rr *rrlp;
{
	register struct rr *rrp;

	while((rrp = rrlp) != NULL){
		rrlp = rrlp->next;

		free(rrp->comment);
		free(rrp->name);
		if(rrp->rdlength > 0){
			switch(rrp->type){
			case TYPE_A:
				break;	/* Nothing allocated in rdata section */
			case TYPE_CNAME:
			case TYPE_MB:
			case TYPE_MG:
			case TYPE_MR:
			case TYPE_NS:
			case TYPE_PTR:
			case TYPE_TXT:
				free(rrp->rdata.name);
				break;
			case TYPE_HINFO:
				free(rrp->rdata.hinfo.cpu);
				free(rrp->rdata.hinfo.os);
				break;
			case TYPE_MX:
				free(rrp->rdata.mx.exch);
				break;
			case TYPE_SOA:
				free(rrp->rdata.soa.mname);
				free(rrp->rdata.soa.rname);
				break;
			}
		}
		free(rrp);
	}
}
#endif

#ifdef MODULE_make_rr
STATIC struct rr *
make_rr(source,dname,dclass,dtype,ttl,rdl,data)
int source;
char *dname;
uint16 dclass;
uint16 dtype;
int32 ttl;
uint16 rdl;
void *data;
{
	register struct rr *newrr;

	newrr = (struct rr *)callocw(1,sizeof(struct rr));
	newrr->source = source;
	newrr->name = strdupw(dname);
	newrr->class = dclass;
	newrr->type = dtype;
	newrr->ttl = ttl;
	if((newrr->rdlength = rdl) == 0)
		return newrr;

	switch(dtype){
	case TYPE_A:
	  {
		register int32 *ap = (int32 *)data;
		newrr->rdata.addr = *ap;
		break;
	  }
	case TYPE_CNAME:
	case TYPE_MB:
	case TYPE_MG:
	case TYPE_MR:
	case TYPE_NS:
	case TYPE_PTR:
	case TYPE_TXT:
	  {
		newrr->rdata.name = strdupw((char *)data);
		break;
	  }
	case TYPE_HINFO:
	  {
		register struct hinfo *hinfop = (struct hinfo *)data;
		newrr->rdata.hinfo.cpu = strdupw(hinfop->cpu);
		newrr->rdata.hinfo.os = strdupw(hinfop->os);
		break;
	  }
	case TYPE_MX:
	  {
		register struct mx *mxp = (struct mx *)data;
		newrr->rdata.mx.pref = mxp->pref;
		newrr->rdata.mx.exch = strdupw(mxp->exch);
		break;
	  }
	case TYPE_SOA:
	  {
		register struct soa *soap = (struct soa *)data;
		newrr->rdata.soa.mname = 	strdupw(soap->mname);
		newrr->rdata.soa.rname = 	strdupw(soap->rname);
		newrr->rdata.soa.serial = 	soap->serial;
		newrr->rdata.soa.refresh = 	soap->refresh;
		newrr->rdata.soa.retry = 	soap->retry;
		newrr->rdata.soa.expire = 	soap->expire;
		newrr->rdata.soa.minimum = 	soap->minimum;
		break;
	  }
	}
	return newrr;
}
#endif


/**
 **	Domain Cache Utilities
 **/

#ifdef MODULE_dcache_add
STATIC void
dcache_add(rrlp)
register struct rr *rrlp;
{
	register struct rr *last_rrp;
	struct rr *save_rrp;

	if(rrlp == NULL)
		return;

	save_rrp = rrlp;
	last_rrp = NULL;
	while(rrlp != NULL){
		rrlp->last = last_rrp;
		last_rrp = rrlp;
		rrlp = rrlp->next;
	}
	last_rrp->next = Dcache;
	if(Dcache != NULL)
		Dcache->last = last_rrp;
	Dcache = save_rrp;
}
#endif

#ifdef MODULE_dcache_drop
STATIC void
dcache_drop(rrp)
register struct rr *rrp;
{
	if(rrp->last != NULL)
		rrp->last->next = rrp->next;
	else
		Dcache = rrp->next;
	if(rrp->next != NULL)
		rrp->next->last = rrp->last;
	rrp->last =
	rrp->next = NULL;
}
#endif

/* Search cache for resource records, removing them from the cache.
 * Also, timeout cache entries, and trim cache to size.
 * (Calling with NULL is legal -- will timeout & trim only.)
 * Note that an answer from the cache cannot be authoritative, because
 * we cannot guarantee that all the entries remain from a previous request.
 * Returns RR list, or NULL if no record found.
 */
#ifdef MODULE_dcache_search
STATIC struct rr *
dcache_search(rrlp)
struct rr *rrlp;
{
	register struct rr *rrp, *test_rrp;
	struct rr **rrpp, *result_rrlp;
	int32 elapsed;
	time_t now;
	int count = 0;

#ifdef DEBUG
	if(Dtrace && rrlp != NULL){
		_printf(_("dcache_search: searching for %s\n"),rrlp->name);
	}
#endif

	elapsed = (int32)(time(&now) - Dcache_time);
	Dcache_time = now;

	rrpp = &result_rrlp;
	for(rrp = Dcache; (test_rrp = rrp) != NULL;){
		rrp = rrp->next;
					/* timeout entries */
		if(test_rrp->ttl > 0L
		&& (test_rrp->ttl -= elapsed) <= 0L)
			test_rrp->ttl = 0L;

		if(compare_rr_list(rrlp,test_rrp) == 0){
			dcache_drop( *rrpp = test_rrp );
			rrpp = &(*rrpp)->next;
		} else if(test_rrp->source == RR_FILE && ++count > Dcache_size){
			dcache_drop(test_rrp);
			free_rr(test_rrp);
		}
	}
	*rrpp = NULL;
	return result_rrlp;
}
#endif


/* Move a list of resource records to the cache, removing duplicates. */
#ifdef MODULE_dcache_update
STATIC void
dcache_update(rrlp)
register struct rr *rrlp;
{
	if(rrlp == NULL)
		return;

	free_rr(dcache_search(rrlp));	/* remove duplicates, first */
	dcache_add(rrlp);
}
#endif


/**
 **	File Utilities
 **/

#ifdef MODULE_get_rr
STATIC struct rr *
get_rr(fp,lastrrp)
FILE *fp;
struct rr *lastrrp;
{
	char *line,*lp,*strtok();
	struct rr *rrp;
	char *name,*ttl,*class,*type,*data;
	int i;

	line = mallocw(256);
	if(fgets(line,256,fp) == NULL){
		free(line);
		return NULL;
	}

	rrp = (struct rr *)callocw(1,sizeof(struct rr));
	rrp->source = RR_FILE;

	if(line[0] == '\0' || line[0] == '#' || line[0] == ';'){
		rrp->comment = line;
		return rrp;
	}

	if(!isspace(line[0]) || lastrrp == NULL){
		name = strtok(line,delim);
		lp = NULL;
	} else {	/* Name field is missing */
		name = lastrrp->name;
		lp = line;
	}
	if(name == NULL || (i = strlen(name)) == 0){
		rrp->comment = strdupw("\n");
		free(line);
		return rrp;
	}

	if(name[i-1] != '.'){
		/* Tack on a trailing period if it's not there */
		/* !!! need to implement $ORIGIN suffix here */
		rrp->name = mallocw(i+2);
		strcpy(rrp->name,name);
		strcat(rrp->name,".");
	} else
		rrp->name = strdupw(name);

	ttl = strtok(lp,delim);

	if(ttl == NULL || (!isdigit(ttl[0]) && ttl[0] != '-')){
		/* Optional ttl field is missing */
		rrp->ttl = TTL_MISSING;
		class = ttl;
	} else {
		rrp->ttl = atol(ttl);
		class = strtok(NULL,delim);
	}

	if(class == NULL){
		/* we're in trouble, but keep processing */
		rrp->class = CLASS_MISSING;
		type = class;
	} else if(class[0] == '<'){
		rrp->class = atoi(&class[1]);
		type = strtok(NULL,delim);
	} else if(_stricmp(class,_("IN")) == 0){
		rrp->class = CLASS_IN;
		type = strtok(NULL,delim);
	} else {
		/* Optional class field is missing; assume IN */
		rrp->class = CLASS_IN;
		type = class;
	}

	if(type == NULL){
		/* we're in trouble, but keep processing */
		rrp->type = TYPE_MISSING;
		data = type;
	} else if(type[0] == '{'){
		rrp->type = atoi(&class[1]);
		data = strtok(NULL,delim);
	} else {
		rrp->type = TYPE_MISSING;
		for(i=1;i<DTYPES_MAX /*Ndtypes*/;i++){
			if(_stricmp(type,Dtypes[i]) == 0){
				rrp->type = i;
				data = strtok(NULL,delim);
				break;
			}
		}
	}

	if(rrp->type == TYPE_MISSING){
		data = NULL;
	}

	if(data == NULL){
		/* Empty record, just return */
		free(line);
		return rrp;
	}
	switch(rrp->type){
	case TYPE_A:
		rrp->rdlength = 4;
		rrp->rdata.addr = aton(data);
		break;
	case TYPE_CNAME:
	case TYPE_MB:
	case TYPE_MG:
	case TYPE_MR:
	case TYPE_NS:
	case TYPE_PTR:
	case TYPE_TXT:
		rrp->rdlength = strlen(data);
		rrp->rdata.name = strdupw(data);
		break;
	case TYPE_HINFO:
		rrp->rdlength = strlen(data);
		rrp->rdata.hinfo.cpu = strdupw(data);
		if((data = strtok(NULL,delim)) != NULL){
			rrp->rdlength += strlen(data);
			rrp->rdata.hinfo.os = strdupw(data);
		}
		break;
	case TYPE_MX:
		rrp->rdata.mx.pref = atoi(data);
		rrp->rdlength = 2;

		/* Get domain name of exchanger */
		if((data = strtok(NULL,delim)) != NULL){
			rrp->rdlength += strlen(data);
			rrp->rdata.mx.exch = strdupw(data);
		}
		break;
	case TYPE_SOA:
		/* Get domain name of master name server */
		rrp->rdlength = strlen(data);
		rrp->rdata.soa.mname = strdupw(data);

		/* Get domain name of irresponsible person */
		if((data = strtok(NULL,delim)) != NULL){
			rrp->rdata.soa.rname = strdupw(data);
			rrp->rdlength += strlen(data);
		}
		data = strtok(NULL,delim);
		rrp->rdata.soa.serial = atol(data);
		data = strtok(NULL,delim);
		rrp->rdata.soa.refresh = atol(data);
		data = strtok(NULL,delim);
		rrp->rdata.soa.retry = atol(data);
		data = strtok(NULL,delim);
		rrp->rdata.soa.expire = atol(data);
		data = strtok(NULL,delim);
		rrp->rdata.soa.minimum = atol(data);
		rrp->rdlength += 20;
		break;
	}

	/* !!! need to handle trailing comments */
	free(line);
	return rrp;
}
#endif


/* Print a resource record */
#ifdef MODULE_put_rr
STATIC void
put_rr(fp,rrp)
FILE *fp;
struct rr *rrp;
{
	char * stuff;

	if(fp == NULL || rrp == NULL)
		return;

	if(rrp->name == NULL && rrp->comment != NULL){
		_fprintf(fp, _("%s"),rrp->comment);
		return;
	}

	_fprintf(fp, _("%s"),rrp->name);
	if(rrp->ttl != TTL_MISSING)
		_fprintf(fp, _("\t%ld"),rrp->ttl);
	if(rrp->class == CLASS_IN)
		_fprintf(fp, _("\tIN"));
	else
		_fprintf(fp, _("\t<%u>"),rrp->class);

	/*RPB*/
	if (stuff = dtype(rrp->type))
	{
		_fprintf(fp, _("\t%s"),stuff);
		free(stuff);
	}
	/*RPB*/
	if(rrp->rdlength == 0){
		/* Null data portion, indicates nonexistent record */
		/* or unsupported type.  Hopefully, these will filter */
		/* as time goes by. */
		_fprintf(fp, _("\n"));
		return;
	}
	switch(rrp->type){
	case TYPE_A:
		_fprintf(fp, _("\t%s\n"),inet_ntoa(rrp->rdata.addr));
		break;
	case TYPE_CNAME:
	case TYPE_MB:
	case TYPE_MG:
	case TYPE_MR:
	case TYPE_NS:
	case TYPE_PTR:
	case TYPE_TXT:
		/* These are all printable text strings */
		_fprintf(fp, _("\t%s\n"),rrp->rdata.data);
		break;
	case TYPE_HINFO:
		_fprintf(fp, _("\t%s\t%s\n"),
		 rrp->rdata.hinfo.cpu,
		 rrp->rdata.hinfo.os);
		break;
	case TYPE_MX:
		_fprintf(fp, _("\t%u\t%s\n"),
		 rrp->rdata.mx.pref,
		 rrp->rdata.mx.exch);
		break;
	case TYPE_SOA:
		_fprintf(fp, _("\t%s\t%s\t%lu\t%lu\t%lu\t%lu\t%lu\n"),
		 rrp->rdata.soa.mname,rrp->rdata.soa.rname,
		 rrp->rdata.soa.serial,rrp->rdata.soa.refresh,
		 rrp->rdata.soa.retry,rrp->rdata.soa.expire,
		 rrp->rdata.soa.minimum);
		break;
	default:
		_fprintf(fp, _("\n"));
		break;
	}
}
#endif


/* Search local database for resource records.
 * Returns RR list, or NULL if no record found.
 */
#ifdef MODULE_dfile_search
STATIC struct rr *
dfile_search(rrlp)
struct rr *rrlp;
{
	register struct rr *frrp;
	struct rr **rrpp, *result_rrlp, *oldrrp;
	int32 elapsed;
	FILE *dbase;
	struct stat dstat;

#ifdef DEBUG
	if(Dtrace){
		_printf(_("dfile_search: searching for %s\n"),rrlp->name);
	}
#endif

	while(Dfile_writing > 0)
		kwait(&Dfile_reading);
	Dfile_reading++;

	if((dbase = fopen(Dfile,READ_TEXT)) == NULL){
		Dfile_reading--;
		return NULL;
	}
	if(fstat(fileno(dbase),&dstat) != 0){
		_printf(_("dfile_search: can't get file status\n"));
		fclose(dbase);
		Dfile_reading--;
		return NULL;
	}
	if((elapsed = (int32)(Dcache_time - (time_t)dstat.st_ctime)) < 0L)
		elapsed = -elapsed;	/* arbitrary time mismatch */

	result_rrlp = NULL;		/* for contiguous test below */
	oldrrp = NULL;
	rrpp = &result_rrlp;
	while((frrp = get_rr(dbase,oldrrp)) != NULL){
		free_rr(oldrrp);
		if(frrp->type != TYPE_MISSING
		&& frrp->rdlength > 0
		&& compare_rr_list(rrlp,frrp) == 0){
			if(frrp->ttl > 0L
			&& (frrp->ttl -= elapsed) <= 0L)
				frrp->ttl = 0L;
			*rrpp = frrp;
			rrpp = &(*rrpp)->next;
			oldrrp = copy_rr(frrp);
		} else {
			oldrrp = frrp;
			/*
				All records of the same name and the same type
				are contiguous.  Therefore, for a single query,
				we can stop searching.  Multiple queries must
				read the whole file.
			*/
			if(rrlp->type != TYPE_ANY
			&& rrlp->next == NULL
			&& result_rrlp != NULL)
				break;
		}
		if(!main_exit)
			kwait(NULL);	/* run multiple sessions */
	}
	free_rr(oldrrp);
	*rrpp = NULL;

	fclose(dbase);

	if(--Dfile_reading <= 0){
		Dfile_reading = 0;
		ksignal(&Dfile_writing,0);
	}

	return result_rrlp;
}
#endif


/* Process which will add new resource records from the cache
 * to the local file, eliminating duplicates while it goes.
 */
#ifdef MODULE_dfile_update
STATIC void
dfile_update(s,unused,p)
int s;
void *unused;
void *p;
{
	struct rr **rrpp, *rrlp, *oldrrp;
	char *newname;
	FILE *old_fp, *new_fp;
	struct stat old_stat, new_stat;

	logmsg(-1, _("update Domain.txt initiated"));

	newname = strdupw(Dfile);
	_strcpy(&newname[strlen(newname)-3],_("tmp"));

	while(Dfile_wait_absolute != 0L && !main_exit){
		register struct rr *frrp;
		int32 elapsed;

		while(Dfile_wait_absolute != 0L){
			elapsed = Dfile_wait_absolute - secclock();
			Dfile_wait_absolute = 0L;
			if(elapsed > 0L && !main_exit){
				kalarm(elapsed*1000L);
				kwait(&Dfile_wait_absolute);
				kalarm(0L);
			}
		}

		logmsg(-1, _("update Domain.txt"));

		/* create new file for copy */
		if((new_fp = fopen(newname,WRITE_TEXT)) == NULL){
			_printf(_("dfile_update: can't create %s!\n"),newname);
			break;
		}
		if(fstat(fileno(new_fp),&new_stat) != 0){
			_printf(_("dfile_update: can't get new_file status!\n"));
			fclose(new_fp);
			break;
		}

		kwait(NULL);	/* file operations can be slow */

		/* timeout the cache one last time before writing */
		(void)dcache_search(NULL);

		/* copy new RRs out to the new file */
		/* (can't wait here, the cache might change) */
		rrpp = &rrlp;
		for(frrp = Dcache; frrp != NULL; frrp = frrp->next ){
			switch(frrp->source){
			case RR_QUESTION:
			case RR_ANSWER:
			case RR_AUTHORITY:
			case RR_ADDITIONAL:
				*rrpp = copy_rr(frrp);
				if(frrp->type != TYPE_MISSING
				&& frrp->rdlength > 0)
					put_rr(new_fp,frrp);
				rrpp = &(*rrpp)->next;
				frrp->source = RR_FILE;
				break;
			}
		}
		*rrpp = NULL;

		/* open up the old file, concurrently with everyone else */
		if((old_fp = fopen(Dfile,READ_TEXT)) == NULL){
			/* great! no old file, so we're ready to go. */
			fclose(new_fp);
			rename(newname,Dfile);
			free_rr(rrlp);
			break;
		}
		if(fstat(fileno(old_fp),&old_stat) != 0){
			_printf(_("dfile_update: can't get old_file status!\n"));
			fclose(new_fp);
			fclose(old_fp);
			free_rr(rrlp);
			break;
		}
		if((elapsed = (int32)(new_stat.st_ctime - old_stat.st_ctime)) < 0L)
			elapsed = -elapsed;	/* file times are inconsistant */

		/* Now append any non-duplicate records */
		oldrrp = NULL;
		while((frrp = get_rr(old_fp,oldrrp)) != NULL){
			free_rr(oldrrp);
			if(frrp->name == NULL
			&& frrp->comment != NULL)
				put_rr(new_fp,frrp);
			if(frrp->type != TYPE_MISSING
			&& frrp->rdlength > 0
			&& compare_rr_list(rrlp,frrp) != 0){
				if(frrp->ttl > 0L
				&& (frrp->ttl -= elapsed) <= 0L)
					frrp->ttl = 0L;
				if(frrp->ttl != 0 || !Dfile_clean)
					put_rr(new_fp,frrp);
			}
			oldrrp = frrp;
			if(!main_exit)
				kwait(NULL);	/* run in background */
		}
		free_rr(oldrrp);
		fclose(new_fp);
		fclose(old_fp);
		free_rr(rrlp);

		/* wait for everyone else to finish reading */
		Dfile_writing++;
		while(Dfile_reading > 0)
			kwait(&Dfile_writing);

		unlink(Dfile);
		rename(newname,Dfile);

		Dfile_writing = 0;
		ksignal(&Dfile_reading,0);
	}
	free(newname);

	logmsg(-1, _("update Domain.txt finished"));
	Dfile_updater = NULL;
}
#endif


/**
 **	Domain Server Utilities
 **/

#ifdef MODULE_dumpdomain
STATIC void
dumpdomain(dhp,rtt)
struct dhdr *dhp;
int32 rtt;
{
	struct rr *rrp;
	char * stuff;

	_printf(_("response id %u (rtt %lu sec) qr %u opcode %u aa %u tc %u rd %u ra %u rcode %u\n"),
	 dhp->id,(long)rtt / 1000L,
	 dhp->qr,dhp->opcode,dhp->aa,dhp->tc,dhp->rd,
	 dhp->ra,dhp->rcode);
	_printf(_("%u questions:\n"),dhp->qdcount);
	for(rrp = dhp->questions; rrp != NULL; rrp = rrp->next){
		/*RPB*/
		if (stuff = dtype(rrp->type))
		{
			_printf(_("%s type %s class %u\n"),rrp->name,
		 	 stuff,rrp->class);
			 free(stuff);
		}
		/*RPB*/
	}
	_printf(_("%u answers:\n"),dhp->ancount);
	for(rrp = dhp->answers; rrp != NULL; rrp = rrp->next){
		put_rr(stdout,rrp);
	}
	_printf(_("%u authority:\n"),dhp->nscount);
	for(rrp = dhp->authority; rrp != NULL; rrp = rrp->next){
		put_rr(stdout,rrp);
	}
	_printf(_("%u additional:\n"),dhp->arcount);
	for(rrp = dhp->additional; rrp != NULL; rrp = rrp->next){
		put_rr(stdout,rrp);
	}
	fflush(stdout);
}
#endif

#ifdef MODULE_dns_makequery
STATIC int
dns_makequery(op,srrp,buffer,buflen)
uint16 op;	/* operation */
struct rr *srrp;/* Search RR */
uint8 *buffer;	/* Area for query */
uint16 buflen;	/* Length of same */
{
	uint8 *cp;
	char *cp1;
	char *dname, *sname;
	uint16 parameter;
	uint16 dlen,len;

	cp = buffer;
	/* Use millisecond clock for timestamping */
	cp = put16(cp,(uint16)msclock());
	parameter = (op << 11)
			| 0x0100;	/* Recursion desired */
	cp = put16(cp,parameter);
	cp = put16(cp,1);
	cp = put16(cp,0);
	cp = put16(cp,0);
	cp = put16(cp,0);

	sname = strdupw(srrp->name);
	dname = sname;
	dlen = strlen(dname);
	for(;;){
		/* Look for next dot */
		cp1 = strchr(dname,'.');
		if(cp1 != NULL)
			len = cp1-dname;	/* More to come */
		else
			len = dlen;	/* Last component */
		*cp++ = len;		/* Write length of component */
		if(len == 0)
			break;
		/* Copy component up to (but not including) dot */
		strncpy((char *)cp,dname,len);
		cp += len;
		if(cp1 == NULL){
			*cp++ = 0;	/* Last one; write null and finish */
			break;
		}
		dname += len+1;
		dlen -= len+1;
	}
	free(sname);
	cp = put16(cp,srrp->type);
	cp = put16(cp,srrp->class);
	return cp - buffer;
}
#endif


/* domain server resolution loop
 * returns: any answers in cache.
 *	(future features)
 *	multiple queries.
 *	inverse queries.
 * return value: 0 if something added to cache, -1 if error
 */
#ifdef MODULE_dns_query
STATIC int
dns_query(rrlp)
struct rr *rrlp;
{
	struct mbuf_s *bp;
	struct dhdr *dhp;
	struct dserver *dp;	/* server list */
	int32 rtt,abserr;
	int tried = 0;		/* server list has been retried (count) */
	struct process_s *process_p;

	if((dp = Dservers) == NULL)
		return -1;

	for(;;){
		uint8 *buf;
		int len;
		struct sockaddr_in server_in;
		int s;
		int rval;

		dp->queries++;

		s = socket(AF_INET,SOCK_DGRAM,0);
		server_in.sin_family = AF_INET;
		server_in.sin_port = IPPORT_DOMAIN;
		server_in.sin_addr.s_addr = dp->address;

		if(Dtrace){
			_printf(_("dns_query: querying server %s for %s\n"),
			 inet_ntoa(dp->address),rrlp->name);
		}

		buf = malloc(512);
 if (buf == NULL)
  {
  close(s);
  _printf(_("Not enough memory\n"));
  return -1;
  }
		len = dns_makequery(0,rrlp,buf,512);
		if(sendto(s,buf,len,0,(struct sockaddr *)&server_in,sizeof(server_in)) == -1)
			_perror(_("domain sendto"));
		FREE(buf);
		kalarm(max(dp->timeout,100));
		/* Wait for something to happen */
#if 1 /* slower but eventually allows domain to be moved out of the kernel */
		bp = alloc_mbuf(512); /* see RFC 1035 */
 if (bp == NULL)
  {
  close(s);
  _printf(_("Not enough memory\n"));
  return -1;
  }
		rval = recvfrom(s, bp->data, 512, 0, NULL, 0);
		if (rval > 0)
			{
			bp->cnt = rval;
			}
		else
			{
			free_mbuf(&bp);
			}
#else
		rval = recv_mbuf(s,&bp,0,NULL,0);
#endif
		kalarm(0L);
		close(s);

		if(Dtrace){
			if(errno == 0)
				_printf(_("dns_query: received message length %d\n"),rval);
			else
				_perror(_("dns_query"));
		}

		if(rval > 0)
			break;

		if(errno == EABORT)
			return -1;		/* Killed by "reset" command */

		/* Timeout; back off this one and try another server */
		dp->timeout <<= 1;
		if((dp = dp->next) == NULL){
			dp = Dservers;
			if(Dserver_retries > 0 && ++tried > Dserver_retries)
				return -1;
		}
	}

	/* got a response */
	dp->responses++;
	dhp = (struct dhdr *) mallocw(sizeof(struct dhdr));
	ntohdomain(dhp,&bp);	/* Convert to local format */

	/* Compute and update the round trip time */
	rtt = (int32) ((uint16)msclock() - dhp->id);
	abserr = rtt > dp->srtt ? rtt - dp->srtt : dp->srtt - rtt;
	dp->srtt = ((AGAIN-1) * dp->srtt + rtt + (AGAIN/2)) >> LAGAIN;
	dp->mdev = ((DGAIN-1) * dp->mdev + abserr + (DGAIN/2)) >> LDGAIN;
	dp->timeout = 4 * dp->mdev + dp->srtt;

	/* move to top of list for next time */
	if(dp->prev != NULL){
		dlist_drop(dp);
		dlist_add(dp);
	}

	if(Dtrace)
		dumpdomain(dhp,rtt);

	/* Add negative reply to answers.  This assumes that there was
	 * only one question, which is true for all questions we send.
	 */
	if(dhp->aa && (dhp->rcode == NAME_ERROR || dhp->ancount == 0)){
		register struct rr *rrp;
		long ttl = 600L; /* Default TTL for negative records */

		/* look for SOA ttl */
		for(rrp = dhp->authority; rrp != NULL; rrp = rrp->next){
			if(rrp->type == TYPE_SOA)
				ttl = rrp->ttl;
		}

		/* make the questions the negative answers */
		for(rrp = dhp->questions; rrp != NULL; rrp = rrp->next)
			rrp->ttl = ttl;
	} else {
		free_rr(dhp->questions);
		dhp->questions = NULL;
	}

	/* post in reverse order to maintain original order */
	dcache_update(dhp->additional);
	dcache_update(dhp->authority);
	dcache_update(dhp->answers);
	dcache_update(dhp->questions);

	Dfile_wait_absolute = secclock() + Dfile_wait_relative;
	if(Dfile_updater == NULL)
		{
		process_p = process_fork(daemon_process_p, 1);
		if (process_p)
			{
			Dfile_updater = process_thread_create(process_p,
					"dom upd", 768, dfile_update,
					0, NULL, NULL, 0);
			process_deref(process_p);
			}
		}

#ifndef ZILOG /* bad luck... could be reinstated after fixing keywait() ?? */
#ifdef DEBUG
	if(Dtrace)
		keywait(NULL,1);	/* so we can look around */
#endif
#endif
	free(dhp);
	return 0;
}
#endif


/**
 **	Resolver Utilities
 **/

/* Return TRUE if string appears to be an IP address in dotted decimal;
 * return FALSE otherwise (i.e., if string is a domain name)
 */
#ifdef MODULE_isaddr
STATIC int
isaddr(s)
register char *s;
{
	char c;

	if(s == NULL)
		return TRUE;	/* Can't happen */

	while((c = *s++) != '\0'){
		if(c != '[' && c != ']' && !isdigit(c) && c != '.')
			return FALSE;
	}
	return TRUE;
}
#endif


/* Return "normalized" domain name, with default suffix and trailing '.'
 */
#ifdef MODULE_checksuffix
STATIC char *
checksuffix(dname)
char *dname;
{
	char *sname, *tname;

	sname = strdupw(dname);
	if(strchr(sname,'.') == NULL && Dsuffix != NULL){
		/* Append default suffix */
		tname = mallocw(strlen(sname)+strlen(Dsuffix)+2);
		_sprintf(tname,_("%s.%s"),sname,Dsuffix);
		free(sname);
		sname = tname;
	}
	if(sname[strlen(sname)-1] != '.'){
		/* Append trailing dot */
		tname = mallocw(strlen(sname)+2);
		_sprintf(tname,_("%s."),sname);
		free(sname);
		sname = tname;
	}
	return sname;
}
#endif


/* Search for resource records.
 * Returns RR list, or NULL if no record found.
 */
#ifdef MODULE_resolver
STATIC struct rr *
resolver(rrlp)
register struct rr *rrlp;
{
	register struct rr *result_rrlp;

	if((result_rrlp = dcache_search(rrlp)) == NULL){
		result_rrlp = dfile_search(rrlp);
	}
	if(result_rrlp == NULL || check_ttl(result_rrlp) != 0){
		dcache_add(result_rrlp); 	/* save any expired RRs */
		if(dns_query(rrlp) == -1)
			return NULL;
		result_rrlp = dcache_search(rrlp);
	}
	dcache_add(copy_rr_list(result_rrlp));
	return result_rrlp;
}
#endif


/* general entry point for address -> domain name resolution.
 * Returns RR list, or NULL if no record found.
 */
#ifdef MODULE_inverse_a
struct rr *
inverse_a(ip_address)
int32 ip_address;
{
	struct rr *prrp;
	struct rr *result_rrlp;
	char pname[30];

	if(ip_address == 0L)
		return NULL;

	_sprintf( pname, _("%u.%u.%u.%u.IN-ADDR.ARPA."),
			lobyte(loword(ip_address)),
			hibyte(loword(ip_address)),
			lobyte(hiword(ip_address)),
			hibyte(hiword(ip_address)) );

	prrp = make_rr(RR_QUERY,pname,CLASS_IN,TYPE_PTR,0,0,NULL);

	prrp->next = 		/* make list to speed search */
		make_rr(RR_INQUERY,NULL,CLASS_IN,TYPE_A,0,4,&ip_address);

	result_rrlp = resolver(prrp);

	free_rr(prrp);
	return result_rrlp;
}
#endif

/* general entry point for domain name -> resource resolution.
 * Returns RR list, or NULL if no record found.
 */
#ifdef MODULE_resolve_rr
struct rr *
resolve_rr(dname,dtype)
char *dname;
uint16 dtype;
{
	struct rr *qrrp;
	struct rr *result_rrlp;
	char *sname;
	int looping = MAXCNAME;

	if(dname == NULL)
		return NULL;

	sname = checksuffix(dname);
	qrrp = make_rr(RR_QUERY,sname,CLASS_IN,dtype,0,0,NULL);
	FREE(sname);

	while(looping > 0){
		if((result_rrlp=resolver(qrrp)) == NULL
		|| result_rrlp->type == dtype)
			break;
#ifdef DEBUG
		if(Dtrace)
			put_rr(stdout,result_rrlp);
#endif
		/* Should be CNAME or PTR record */
		/* Replace name and try again */
		free(qrrp->name);
		qrrp->name = strdupw(result_rrlp->rdata.name);
		free_rr(result_rrlp);
		result_rrlp = NULL;
		looping--;
	}
	free_rr(qrrp);
	return result_rrlp;
}
#endif


/* main entry point for address -> domain name resolution.
 * Returns string, or NULL if no name found.
 */
#ifdef MODULE_resolve_a
char *
resolve_a(ip_address,shorten)
int32 ip_address;		/* search address */
int shorten;			/* return only first part of name (flag)*/
{
	struct rr *save_rrlp, *rrlp;
	char *result = NULL;

	for( rrlp = save_rrlp = inverse_a(ip_address);
	     rrlp != NULL && result == NULL;
	     rrlp = rrlp->next ){
		if(rrlp->rdlength > 0){
			switch(rrlp->type){
			case TYPE_PTR:
				result = strdupw(rrlp->rdata.name);
				break;
			case TYPE_A:
				result = strdupw(rrlp->name);
				break;
			}
		}
	}
	free_rr(save_rrlp);

	if(result != NULL && shorten){
		int dot;
		char *shortened;

		if((dot = strcspn(result, ".")) == 0){
			shortened = mallocw(dot+1);
			strncpy(shortened, result, dot);
			shortened[dot] = '\0';
			free(result);
			result = shortened;
		}
	}
	return result;
}
#endif


/* Main entry point for domain name -> address resolution.
 * Returns 0 if name is currently unresolvable.
 */
#ifdef MODULE_resolve
int32
resolve(name)
char *name;
{
	register struct rr *rrlp;
	int32 ip_address = 0;

	if(name == NULL)
		return 0;

	if(isaddr(name))
		return aton(name);

	if((rrlp = resolve_rr(name,TYPE_A)) != NULL
	 && rrlp->rdlength > 0)
		ip_address = rrlp->rdata.addr;

	/* multi-homed hosts are handled here */
	if(rrlp != NULL && rrlp->next != NULL) {
		register struct rr *rrp;
		register struct route *rp;
		uint16 cost = MAXINT16;
		rrp = rrlp;
		/* choose the best of a set of routes */
		while(rrp != NULL) {
			if(rrp->rdlength > 0
			 && (rp = rt_lookup(rrp->rdata.addr)) != NULL
			 && rp->metric <= cost) {
				ip_address = rrp->rdata.addr;
				cost = rp->metric;
			}
			rrp = rrp->next;
		}
	}

	free_rr(rrlp);
	return ip_address;
}
#endif


/* Main entry point for MX record lookup.
 * Returns 0 if name is currently unresolvable.
 */
#ifdef MODULE_resolve_mx
int32
resolve_mx(name)
char *name;
{
	register struct rr *rrp, *arrp;
	char *sname, *tmp, *cp;
	int32 addr, ip_address = 0;
	uint16 pref = MAXINT16;

	if(name == NULL)
		return 0;

	if(isaddr(name)){
		if((sname = resolve_a(aton(name),FALSE)) == NULL)
			return 0;
	}
	else
		sname = strdupw(name);

	cp = sname;
	while(1){
		rrp = arrp = resolve_rr(sname,TYPE_MX);
		/* Search this list of rr's for an MX record */
		while(rrp != NULL){
			if(rrp->rdlength > 0 && rrp->rdata.mx.pref <= pref &&
			   (addr = resolve(rrp->rdata.mx.exch)) != 0L){
				pref = rrp->rdata.mx.pref;
				ip_address = addr;
			}
			rrp = rrp->next;
		}
		free_rr(arrp);
		if(ip_address != 0)
			break;
		/* Compose wild card one level up */
		if((cp = strchr(cp,'.')) == NULL)
			break;
		tmp = mallocw(strlen(cp)+2);
		_sprintf(tmp,_("*%s"),cp);	/* wildcard expansion */
		free(sname);
		sname = tmp;
		cp = sname + 2;
	}
	free(sname);
	return ip_address;
}
#endif


/* Search for local records of the MB, MG and MR type. Returns list of
 * matching records.
 */
#ifdef MODULE_resolve_mailb
struct rr *
resolve_mailb(name)
char *name;		/* local username, without trailing dot */
{
	register struct rr *result_rrlp;
	struct rr *rrlp;
	char *sname;

	/* Append trailing dot */
	sname = mallocw(strlen(name)+2);
	_sprintf(sname,_("%s."),name);
	rrlp = make_rr(RR_QUERY,sname,CLASS_IN,TYPE_MB,0,0,NULL);
	rrlp->next = make_rr(RR_QUERY,sname,CLASS_IN,TYPE_MG,0,0,NULL);
	rrlp->next->next = make_rr(RR_QUERY,sname,CLASS_IN,TYPE_MR,0,0,NULL);
	FREE(sname);
	if((result_rrlp = dcache_search(rrlp)) == NULL){
		result_rrlp = dfile_search(rrlp);
	}
	free_rr(rrlp);
	if(Dsuffix != NULL){
		rrlp = result_rrlp;
		while(rrlp != NULL){	/* add domain suffix to data */
			if(rrlp->rdlength > 0 &&
			   rrlp->rdata.name[rrlp->rdlength-1] != '.'){
				sname = mallocw(rrlp->rdlength +
					strlen(Dsuffix)+2);
				_sprintf(sname,_("%s.%s"),rrlp->rdata.name,Dsuffix);
				free(rrlp->rdata.name);
				rrlp->rdata.name = sname;
				rrlp->rdlength = strlen(sname);
			}
			rrlp = rrlp->next;
		}
	}
	dcache_add(copy_rr_list(result_rrlp));
	return result_rrlp;
}
#endif
