# auto.mk by Nick for Hytech NOS/UZI project

# -----------------------------------------------------------------------------

SUBDIRS=

OUTPUTS=	messages.po

DEFINES+=	MAKE_ALL	# needed for crt

messages_po_SOURCES= \
		../client/bootp.c ../client/finger.c ../client/ftpcli.c \
		../client/telnet.c ../crt/abort.c ../crt/alloca.c \
		../crt/asctime.c ../crt/assert.c ../crt/atexit.c \
		../crt/atoi.c ../crt/atol.c ../crt/bsearch.c ../crt/calloc.c \
		../crt/clock.c ../crt/closedir.c ../crt/convtime.c \
		../crt/crypt.c ../crt/ctime.c ../crt/ctype.c \
		../crt/difftime.c ../crt/error.c ../crt/etime.c \
		../crt/execl.c ../crt/execle.c ../crt/execlp.c \
		../crt/execlpe.c ../crt/exect.c ../crt/execv.c \
		../crt/execvp.c ../crt/execvpe.c ../crt/exit.c \
		../crt/fclose.c ../crt/fflush.c ../crt/fgetc.c \
		../crt/fgetgren.c ../crt/fgetpwen.c ../crt/fgets.c \
		../crt/fopen.c ../crt/fprintf.c ../crt/fputc.c ../crt/fputs.c \
		../crt/fputter.c ../crt/fread.c ../crt/fscanf.c \
		../crt/ftell.c ../crt/fwrite.c ../crt/getcwd.c \
		../crt/getenv.c ../crt/getgrent.c ../crt/getgrgid.c \
		../crt/getgrnam.c ../crt/getopt.c ../crt/getpass.c \
		../crt/getpw.c ../crt/getpwent.c ../crt/getpwnam.c \
		../crt/getpwuid.c ../crt/gets.c ../crt/gmtime.c \
		../crt/initgrup.c ../crt/isatty.c ../crt/itoa.c \
		../crt/localtim.c ../crt/lsearch.c ../crt/lstat.c \
		../crt/ltoa.c ../crt/ltostr.c ../crt/malloc.c \
		../crt/memccpy.c ../crt/memchr.c ../crt/memcmp.c \
		../crt/memmove.c ../crt/mkdir.c ../crt/mktime.c \
		../crt/opendir.c ../crt/perror.c ../crt/popen.c \
		../crt/printf.c ../crt/putenv.c ../crt/putgetch.c \
		../crt/putpwent.c ../crt/qsort.c ../crt/rand.c \
		../crt/readdir.c ../crt/readlink.c ../crt/regerror.c \
		../crt/regexp.c ../crt/regsub.c ../crt/rename.c \
		../crt/rewind.c ../crt/rewindir.c ../crt/rmdir.c \
		../crt/scanf.c ../crt/setbuff.c ../crt/setenv.c \
		../crt/setgrent.c ../crt/setpwent.c ../crt/setvbuff.c \
		../crt/sleep.c ../crt/sprintf.c ../crt/sputter.c \
		../crt/sscanf.c ../crt/stdio0.c ../crt/strcat.c \
		../crt/strchr.c ../crt/strcmp.c ../crt/strcpy.c \
		../crt/strcspn.c ../crt/strdup.c ../crt/stricmp.c \
		../crt/strlen.c ../crt/strncat.c ../crt/strncmp.c \
		../crt/strncpy.c ../crt/strnicmp.c ../crt/strpbrk.c \
		../crt/strrchr.c ../crt/strsep.c ../crt/strspn.c \
		../crt/strstr.c ../crt/strtok.c ../crt/strtol.c \
		../crt/strtoul.c ../crt/system.c ../crt/termcap.c \
		../crt/tmpfile.c ../crt/tmpnam.c ../crt/tparam.c \
		../crt/ttyname.c ../crt/tzset.c ../crt/ultoa.c \
		../crt/ungetc.c ../crt/utsname.c ../crt/vfprintf.c \
		../crt/vfscanf.c ../crt/vprinter.c ../crt/vprintf.c \
		../crt/vscanf.c ../crt/vsprintf.c ../crt/vsscanf.c \
		../crt/xitoa.c ../crt/xltoa.c ../driver/bufpool.c \
		../driver/devapi.c ../driver/devcfl.c ../driver/device.c \
		../driver/devkmem.c ../driver/devnull.c ../driver/devram.c \
		../driver/devtty.c ../driver/devzero.c ../driver/rtc.c \
		../dump/icmpdump.c ../dump/ipdump.c ../dump/tcpdump.c \
		../dump/trace.c ../dump/udpdump.c ../filesys/bitmap.c \
		../filesys/cinode.c ../filesys/execve.c ../filesys/filename.c \
		../filesys/filesys.c ../filesys/maxmin.c \
		../filesys/openfile.c ../filesys/xip.c ../internet/domain.c \
		../internet/domhdr.c ../internet/hop.c ../internet/icmp.c \
		../internet/icmpcmd.c ../internet/icmphdr.c \
		../internet/icmpmsg.c ../internet/ip.c ../internet/ipcmd.c \
		../internet/iphdr.c ../internet/iproute.c \
		../internet/ipsock.c ../internet/netuser.c ../internet/ping.c \
		../internet/sim.c ../internet/tcpcmd.c ../internet/tcphdr.c \
		../internet/tcpin.c ../internet/tcpout.c \
		../internet/tcpsock.c ../internet/tcpsubr.c \
		../internet/tcptimer.c ../internet/tcpuser.c \
		../internet/udp.c ../internet/udpcmd.c ../internet/udphdr.c \
		../internet/udpsock.c ../intl/intl.c ../kernel/arena.c \
		../kernel/dprintf.c ../kernel/fork.c ../kernel/kernel.c \
		../kernel/object.c ../kernel/process.c ../kernel/session.c \
		../kernel/strace.c ../kernel/thread.c ../kernel/unix.c \
		../kernel/userfile.c ../kernel/usrmem.c ../kernel/valadr.c \
		../main/ahdlc.c ../main/alloc.c ../main/audit.c \
		../main/cmdparse.c ../main/config.c ../main/crc.c \
		../main/devparam.c ../main/files.c ../main/ftpsubr.c \
		../main/getopt.c ../main/iface.c ../main/main.c \
		../main/mbuf.c ../main/md5c.c ../main/misc.c ../main/parm.c \
		../main/pathname.c ../main/timer.c ../main/version.c \
		../ppp/dialer.c ../ppp/ppp.c ../ppp/pppcmd.c ../ppp/pppdump.c \
		../ppp/pppfsm.c ../ppp/pppipcp.c ../ppp/ppplcp.c \
		../ppp/ppppap.c ../ppp/slhc.c ../ppp/slhcdump.c ../ppp/slip.c \
		../server/bootpcmd.c ../server/bootpd.c ../server/bootpdip.c \
		../server/fingerd.c ../server/ftpserv.c ../server/telnetd.c \
		../server/ttylink.c ../socket/locsock.c ../socket/opensock.c \
		../socket/sockutil.c ../sys/sys.c ../z80/abus.c ../z80/asci.c \
		../z80/async.c ../z80/dirutil.c ../z80/escc.c ../z80/rand.c \
		../z80/z80.c
#../ax25/ax25.c ../ax25/ax25cmd.c ../ax25/ax25hdr.c ../ax25/ax25mail.c
#../ax25/ax25subr.c ../ax25/ax25user.c ../ax25/axheard.c ../ax25/axsock.c
#../ax25/kiss.c ../ax25/lapb.c ../ax25/lapbtime.c ../netrom/nr3.c
#../netrom/nr4.c ../netrom/nr4hdr.c ../netrom/nr4mail.c ../netrom/nr4subr.c
#../netrom/nr4timer.c ../netrom/nr4user.c ../netrom/nrcmd.c ../netrom/nrhdr.c
#../netrom/nrs.c ../netrom/nrsock.c

# -----------------------------------------------------------------------------

