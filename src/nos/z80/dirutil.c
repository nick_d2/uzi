/* dirutil.c - MS-DOS directory reading routines
 *
 * Bdale Garbee, N3EUA, Dave Trulli, NN2Z, and Phil Karn, KA9Q
 * Directory sorting by Mike Chepponis, K3MC
 * New version using regs.h by Russell Nelson.
 * Rewritten for Turbo-C 2.0 routines by Phil Karn, KA9Q 25 March 89
 *
 * Apr 04       RPB     Po-ed most of the remaining strings
 */

#include <stdio.h>
#include <dirent.h> /* Nick <dir.h> */
#include <sys/stat.h> /* Nick */
/* Nick #include <dos.h> */
#include <stdlib.h>
#include "nos/global.h"
#include "z80/dirutil.h"
#include "kernel/thread.h"
#include "main/commands.h"
#include "time.h" /*RPB*/
#include <libintl.h>
#include "po/messages.h"

#ifdef MODULE
#define STATIC
#else
#define STATIC static
#define MODULE_dir
#define MODULE_nextname
#define MODULE_filedir
#define MODULE_getdir
#define MODULE_fncmp
#define MODULE_docd
#define MODULE_dodir
#define MODULE_domkd
#define MODULE_dormd
#define MODULE_commas
#define MODULE_wildcardize
#define MODULE_format_fname_full
#define MODULE_print_free_space
#define MODULE_free_clist
#define MODULE_undosify
#endif

#ifdef ZILOG /* Nick */
#define FA_HIDDEN 0
#define FA_SYSTEM 0
#define FA_DIREC S_IFDIR

struct ffblk
	{
	DIR *ff_dir;
	struct dirent *ff_dirent;
	char ff_name[DIRECTNAMELEN];
	mode_t ff_attrib;
	unsigned int ff_ftime;
	unsigned int ff_fdate;
	unsigned long ff_fsize;
	};
#endif

struct dirsort {
	struct dirsort *next;
	struct ffblk de;
};
#define	NULLSORT (struct dirsort *)0

STATIC void commas(char *dest);
STATIC int fncmp(char *a, char *b);
STATIC void format_fname_full(FILE *file,struct ffblk *sbuf,int full,
	int n);
STATIC void free_clist(struct dirsort *this);

#if 0 /*def notdef*/
STATIC int getdir_nosort(char *path,int full,FILE *file);
#endif
STATIC int nextname(int command, char *name, struct ffblk *sbuf);
STATIC void print_free_space(FILE *file,int n);
STATIC void undosify(char *s);
STATIC char *wildcardize(char *path);

#define REGFILE	(FA_HIDDEN|FA_SYSTEM|FA_DIREC)

#define	insert_ptr(list,new)	(new->next = list,list = new)

/* Create a directory listing in a temp file and return the resulting file
 * descriptor. If full == 1, give a full listing; else return just a list
 * of names.
 */

#ifdef MODULE_dir
FILE *
dir(path,full)
char *path;
int full;
{
	FILE *fp;

	if((fp = tmpfile()) != NULL){
		getdir(path,full,fp);
		rewind(fp);
	}
	return fp;
}
#endif

/* find the first or next file and lowercase it. */

#ifdef MODULE_nextname
STATIC int
nextname(command, name, sbuf)
int command;
char *name;
struct ffblk *sbuf;
{
	int found;
#ifdef ZILOG /* Nick */
	struct stat statbuf;
	char statname[MAXNAMLEN+MAXNAMLEN]; /* Kludgey length */
#endif

	switch(command){
	case 0:
#ifdef ZILOG /* Nick */
		sbuf->ff_dir = opendir(name); /* note: remove filename mask! */

		if (sbuf->ff_dir == NULL)
			{
			found = 0;
			break;
			}
		/* fallthru */
#else
		found = findfirst(name,sbuf,REGFILE);
		break;
#endif
	default:
#ifdef ZILOG /* Nick */
		sbuf->ff_dirent = readdir(sbuf->ff_dir);
		found = (sbuf->ff_dirent != NULL);
#else
		found = findnext(sbuf);
#endif
	}
#ifdef ZILOG /* Nick */
	if (found)
	{
	strcpy(sbuf->ff_name, sbuf->ff_dirent->d_name);

	if (command != 0 && strchr(name, '*') == NULL)
	{
		strcpy(statname, name);
		strcat(statname, "/");
		strcat(statname, sbuf->ff_name);
	}
	else
		strcpy(statname, sbuf->ff_name);

	if (stat(statname, &statbuf) < 0)
		{
		return 0;
		}
	sbuf->ff_attrib = statbuf.st_mode;
	sbuf->ff_ftime = (int)statbuf.st_mtime;
	sbuf->ff_fdate = (int)(statbuf.st_mtime >> 16);
	sbuf->ff_fsize = statbuf.st_size;
	}
#else
	found = found == 0;
	if(found)
		strlwr(sbuf->ff_name);
#endif

	return found;
}
#endif

/* wildcard filename lookup */

#ifdef MODULE_filedir
int
filedir(name,times,ret_str)
char *name;
int times;
char *ret_str;
{
	/*static*/ struct ffblk sbuf;
	int found;

	switch(times){
	case 0:
#ifdef ZILOG /* Nick */
		sbuf.ff_dir = opendir(name); /* note: remove filename mask! */
		if (sbuf.ff_dir == NULL)
			{
			found = 0;
			break;
			}
		/* fallthru */
#else
		found = findfirst(name,&sbuf,REGFILE);
		break;
#endif
	default:
#ifdef ZILOG /* Nick */
		sbuf.ff_dirent = readdir(sbuf.ff_dir);
		found = (sbuf.ff_dirent != NULL);
#else
		found = findnext(&sbuf);
#endif
		break;
	}
#ifndef ZILOG /* Nick */
	found = found == 0;
#endif
	if(found == 0){
		ret_str[0] = '\0';
	} else {
		/* Copy result to output */
#ifdef ZILOG /* Nick */
		strcpy(ret_str, sbuf.ff_dirent->d_name);
#else
		strcpy(ret_str, sbuf.ff_name);
#endif
	}
	return found - 1; /* dodgy way of indicating -1 on error, 0 success */
}
#endif

/* do a directory list to the stream
 * full = 0 -> short form, 1 is long
*/

#ifdef MODULE_getdir
int
getdir(path,full,file)
char *path;
int full;
FILE *file;
{
	struct ffblk sbuf;
	int command = 0;
	int n = 0;
	struct dirsort *head, *here, *new;

	path = wildcardize(path);

	head = NULLSORT;	/* No head of chain yet... */
	for(;;){
		if (!nextname(command, path, &sbuf))
			break;
		command = 1;	/* Got first one already... */
		if (sbuf.ff_name[0] == '.')	/* drop "." and ".." */
			continue;

		new = (struct dirsort *) mallocw(sizeof(struct dirsort));
		new->de = sbuf;	/* Copy contents of directory entry struct */

		/* insert it into the list */
		if (!head || fncmp(new->de.ff_name, head->de.ff_name) < 0) {

			insert_ptr(head, new);
		} else {
			register struct dirsort *this;
			for (this = head;
			    this->next != NULLSORT;
			    this = this->next)
				if (fncmp(new->de.ff_name, this->next->de.ff_name) < 0)
					break;
			insert_ptr(this->next, new);
		}
	} /* infinite FOR loop */

	for (here = head; here; here = here->next)
		format_fname_full(file,&here->de,full,++n);

	/* Give back all the memory we temporarily needed... */
	free_clist(head);

	if(full)
		print_free_space(file, n);

	closedir(sbuf.ff_dir);
	return 0;
}
#endif

#ifdef MODULE_fncmp
STATIC int
fncmp(a,b)
register char *a, *b;
{
        int i;

	for(;;){
		if (*a == '.')
			return -1;
		if (*b == '.')
			return 1;
		if ((i = *a - *b++) != 0)
			return i;
		if (!*a++)
			return -1;
	}
}
#endif

/* Change working directory */

#ifdef MODULE_docd
int
docd(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	char dirname[128];

	if(argc > 1){
		if(chdir(argv[1]) == -1){
			_printf(_("Can't change directory\n"));
			return 1;
		}
	}
	if(getcwd(dirname,128) != NULL){
		undosify(dirname);
		_printf(_("%s\n"),dirname);
	}
	return 0;
}
#endif

/* List directory to console */

#ifdef MODULE_dodir
int
dodir(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	char *path;

	if(argc >= 2)
		path = argv[1];
	else
		path = "*.*";

	getdir(path,1,stdout);
	return 0;
}
#endif

/* Create directory */

#ifdef MODULE_domkd
int
domkd(argc,argv,p)
int argc;
char *argv[];
void *p;
{
#ifdef UNIX
	if(mkdir(argv[1], 0777) == -1)
#else
	if(mkdir(argv[1]) == -1)
#endif
		_perror(_("Can't mkdir"));
	return 0;
}
#endif

/* Remove directory */

#ifdef MODULE_dormd
int
dormd(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	if(rmdir(argv[1]) == -1)
		_perror(_("Can't rmdir"));
	return 0;
}
#endif

/*
 * Return a string with commas every 3 positions.
 * the original string is replace with the string with commas.
 *
 * The caller must be sure that there is enough room for the resultant
 * string.
 *
 *
 * k3mc 4 Dec 87
 */

#ifdef MODULE_commas
STATIC void
commas(dest)
char *dest;
{
	char *src, *core;	/* Place holder for malloc */
	unsigned cc;		/* The comma counter */
	unsigned len;

	len = strlen(dest);
	/* Make a copy, so we can muck around */
	core = src = strdupw(dest);

	cc = (len-1)%3 + 1;	/* Tells us when to insert a comma */

	while(*src != '\0'){
		*dest++ = *src++;
		if( ((--cc) == 0) && *src ){
			*dest++ = ','; cc = 3;
		}
	}
	free(core);
	*dest = '\0';
}
#endif

/* fix up the filename so that it contains the proper wildcard set */

#ifdef MODULE_wildcardize
STATIC char *
wildcardize(path)
char *path;
{
	struct ffblk sbuf;
	/*static*/ char ourpath[64];

	/* Root directory is a special case */
	if(path == NULL ||
	   *path == '\0' ||
	   _strcmp(path,_("\\")) == 0 ||
	   _strcmp(path,_("/")) == 0)
		path = "*.*";

	/* if they gave the name of a subdirectory, append \*.* to it */
	if (nextname(0, path, &sbuf) &&
	    (sbuf.ff_attrib & FA_DIREC) &&
	    !nextname(1, path, &sbuf)) {
		closedir(sbuf.ff_dir);
		/* if there isn't enough room, give up -- it's invalid anyway */
		if (strlen(path) + 4 > 63) return path;
		strcpy(ourpath, path);
		strcat(ourpath, "*.*");
		return ourpath;
	}
	closedir(sbuf.ff_dir);
	return path;
}
#endif

#ifdef MODULE_format_fname_full
STATIC void
format_fname_full(file, sbuf, full, n)
FILE *file;
struct ffblk *sbuf;
int full, n;
{
	char line_buf[50];		/* for long dirlist */
	char cbuf[20];			/* for making line_buf */

	strcpy(cbuf,sbuf->ff_name);
	if(sbuf->ff_attrib & FA_DIREC) strcat(cbuf, "/");
	if (full) {
		/* Long form, give other info too */
		_sprintf(line_buf,_("%-13s"),cbuf);
		if(sbuf->ff_attrib & FA_DIREC)
			strcat(line_buf,"           ");/* 11 spaces */
		else {
			_sprintf(cbuf,_("%ld"),sbuf->ff_fsize);
			commas(cbuf);
			_sprintf(line_buf+strlen(line_buf),_("%10s "),cbuf);
		}
		_sprintf(line_buf+strlen(line_buf),_("%2d:%02d %2d/%02d/%02d%s"),
		  (sbuf->ff_ftime >> 11) & 0x1f,	/* hour */
		  (sbuf->ff_ftime >> 5) & 0x3f,	/* minute */
		  (sbuf->ff_fdate >> 5) & 0xf,	/* month */
		  (sbuf->ff_fdate ) & 0x1f,		/* day */
		  ((sbuf->ff_fdate >> 9) + 80) % 100,	/* year */
		  (n & 1) ? "   " : "\n");
#if 0
		{
		struct tm tmbuf;
		int32 mtime = (int32)sbuf->ff_fdate << 16 | (int32)sbuf->ff_ftime;

		__tm_conv(&tmbuf, (time_t *)&mtime, 0);
		_sprintf(line_buf+strlen(line_buf),_("%2d:%02d %2d/%02d/%02d%s"),
		  tmbuf.tm_hour,        /* hour */
		  tmbuf.tm_min ,        /* minute */
		  tmbuf.tm_mon ,        /* month */
		  tmbuf.tm_mday,        /* day */
		  tmbuf.tm_year,        /* year */
		  (n & 1) ? "   " : "\n");

		}
#endif
		fputs(line_buf,file);
	} else {
		fputs(cbuf,file);
		_fputs(_("\n"),file);
	}
}
#endif

/* Provide additional information only on DIR */

#ifdef MODULE_print_free_space
STATIC void
print_free_space(file, n)
FILE *file;
int n;
{
	unsigned long free_bytes, total_bytes;
	char s_free[11], s_total[11];
	char cbuf[20];
	/* Nick change to a getfsys() call struct dfree dtable; */
	unsigned long bpcl;

	if(n & 1)
		_fputs(_("\n"),file);

	/* Find disk free space */
	/* Nick getdfree(0,&dtable); */

	/* Nick bpcl = dtable.df_bsec * dtable.df_sclus; */
	free_bytes  = 0; /* dtable.df_avail * bpcl; */
	total_bytes = 0; /* dtable.df_total * bpcl; */

	_sprintf(s_free,_("%ld"),free_bytes);
	commas(s_free);
	_sprintf(s_total,_("%ld"),total_bytes);
	commas(s_total);

	if(n)
		_sprintf(cbuf,_("%d"),n);
	else
		_strcpy(cbuf,_("No"));

	_fprintf(file, _("%s file%s. %s bytes free. Disk size %s bytes.\n"),
		cbuf,(n==1? "":"s"),s_free,s_total);
}
#endif

#ifdef MODULE_free_clist
STATIC void
free_clist(this)
struct dirsort *this;
{
	struct dirsort *next;

	while (this != NULLSORT) {
		next = this->next;
		free(this);
		this = next;
	}
}
#endif

#if 0 /*def notdef*/
STATIC int
getdir_nosort(path,full,file)
char *path;
int full;
FILE *file;
{
	struct ffblk sbuf;
	int command;
	int n = 0;	/* Number of directory entries */

	path = wildcardize(path);
	command = 0;
	while(nextname(command, path, &sbuf)){
		command = 1;	/* Got first one already... */
		if (sbuf.ff_name[0] == '.')	/* drop "." and ".." */
			continue;
		format_fname_full(file, &sbuf, full, ++n);
	}
	closedir(sbuf.ff_dir);
	if(full)
		print_free_space(file, n);
	return 0;
}
#endif

/* Translate those %$#@!! backslashes to proper form */

#ifdef MODULE_undosify
STATIC void
undosify(s)
char *s;
{
	while(*s != '\0'){
		if(*s == '\\')
			*s = '/';
		s++;
	}
}
#endif

