# auto.mk by Nick for Hytech NOS/UZI project

# -----------------------------------------------------------------------------

SUBDIRS=

OUTPUTS=	z80.$(LIBEXT)

z80_$(LIBEXT)_SOURCES= \
		abus.c abus_vec.S asci.c asci_int.S asci_vec.S async.c \
		async_rx.S async_tx.S copyr.S diag.S diag_vec.S dirutil.c \
		escc.c escc_aex.S escc_asp.S escc_gen.S escc_hit.S escc_int.S \
		escc_vec.S rand.c unix_vec.S z80.c z80_gen.S
#stktrace.c sw.c

abus_c_MODULES= Api Api_refs Api_cflag Api_txsti Api_rxsti Api_count \
		Api_state Api_dirn Api_robin Api_rxovr Api_devok Api_kick \
		abus_init abus_stop abus_ioctl abus_kick doabusstat pabus
#Api_turbo abus_open abus_close

asci_c_MODULES=	Asci Asci_rate Handle asci_init asci_stop asci_speed \
		asci_ioctl asci_kick doascistat
#asci_int
#pasci asci_open asci_close ascimsint asci_timer get_rlsd_asci

async_c_MODULES= \
		async_getc async_recv async_read async_send async_write
#async_rx async_tx

dirutil_c_MODULES= \
		dir nextname filedir getdir fncmp docd dodir domkd dormd \
		commas wildcardize format_fname_full print_free_space free_clist \
		undosify

escc_c_MODULES=	Origvec Esccinfo Esccchan escc_attach escc_init escc_async \
		escc_speed escc_stop escc_exit escc_aioctl \
		doesccstat
#escc_kick escc_aex escc_asp escc_int
#get_rlsd_escc Esccmaxvec Esccpolltab
#Random escc_sdlc escc_sioctl escc_raw escc_sdlctx escc_sdlcex escc_sdlcrx
#escc_sdlcsp escc_tossb escc_txon escc_txoff escc_timer

# -----------------------------------------------------------------------------

