/* OS- and machine-dependent stuff for the Z180 ASCI in a Hytech 1000/1500
 * Copyright 1991 Phil Karn, KA9Q
 *
 * 16550A support plus some statistics added mah@hpuviea.at 15/7/89
 *
 * CTS hardware flow control from dkstevens@ucdavis,
 * additional stats from delaroca@oac.ucla.edu added by karn 4/17/90
 * Feb '91      RLSD line control reorganized by Bill_Simpson@um.cc.umich.edu
 * Sep '91      All control signals reorganized by Bill Simpson
 * Apr '92	Control signals redone again by Phil Karn
 * Apr '04	Intl' of strings
 */
#include <errno.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include "z80/async.h"
#include "kernel/thread.h" /* for kwait() */
#include <libintl.h>
#include "po/messages.h"

#ifndef MODULE
#define MODULE_async_getc
#define MODULE_async_recv
#define MODULE_async_read
#define MODULE_async_send
#define MODULE_async_write
#define MODULE_async_rx
#define MODULE_async_tx
#endif

/* Blocking read from asynch line
 * Blocks until at least 1 byte of data is available.
 * returns char 0-255, or -1 if wait was interrupted
 */
#ifdef MODULE_async_getc
unsigned int
async_getc(struct async_s *async_p)
	{
	int i_state;
	unsigned char ch;

	i_state = dirps();
	for (;;)
		{
		if (async_p->cnt)
			{
			async_p->cnt--;
			break;
			}
		errno = kwait(&async_p->cnt);
		if (errno)
			{
			restore(i_state);
			return -1;
			}
		}
	restore(i_state);

	ch = *async_p->rp++;
	if (async_p->rp >= &async_p->buf[async_p->bufsize])
		{
		async_p->rp = async_p->buf;
		}

	return ch;
	}
#endif

/* Read data from asynch line as a packet
 * Blocks until at least 1 byte of data is available.
 */
#ifdef MODULE_async_recv
unsigned int
async_recv(struct async_s *async_p, struct mbuf_s **mbuf_pp, size_t cnt)
	{
	struct mbuf_s *mbuf_p;

	if (async_p->iface == NULL)
		{
		return -1;
		}

	if (cnt == 0)
		{
		cnt = MED_MBUF;
		}

	mbuf_p = alloc_mbuf(cnt);
	if (mbuf_p == NULL)
		{
		return -1;
		}

	cnt = async_read(async_p, mbuf_p->data, cnt);
	if (cnt == -1)
		{
		free_mbuf(&mbuf_p);
		return -1;
		}
	mbuf_p->cnt = cnt;

	*mbuf_pp = mbuf_p;
	return 0;
	}
#endif

/* Read data from asynch line
 * Blocks until at least 1 byte of data is available.
 * returns number of bytes read, up to 'cnt' max
 */
#ifdef MODULE_async_read
ssize_t
async_read(struct async_s *async_p, void *buf, size_t cnt)
	{
	int i_state;
	size_t tmp;
	unsigned char *obp;

	if (async_p->iface == NULL)
		{
		return -1;
		}

	if (cnt == 0)
		{
		return 0;
		}

	i_state = dirps();
	for (;;)
		{
		/* Atomic read of and subtract from async_p->cnt */
		tmp = async_p->cnt;
		if (tmp)
			{
			if (cnt > tmp)
				{
				cnt = tmp;	/* Limit to data on hand */
				}
			async_p->cnt -= cnt;
			break;
			}
		errno = kwait(&async_p->cnt);
		if (errno)
			{
			restore(i_state);
			return -1;
			}
		}
	restore(i_state);

	obp = (unsigned char *)buf;
	for (tmp = 0; tmp < cnt; tmp++)
		{
		/* This can be optimized later if necessary */
		*obp++ = *async_p->rp++;
		if (async_p->rp >= &async_p->buf[async_p->bufsize])
			{
			async_p->rp = async_p->buf;
			}
		}

	return cnt;
	}
#endif

/* Send a message on the specified serial line */
#ifdef MODULE_async_send
unsigned int
async_send(struct async_s *async_p, struct mbuf_s **mbuf_pp)
	{
	int i_state;

	if (async_p->iface == NULL)
		{
		return -1;
		}

	i_state = dirps();
	append(&async_p->sndq, mbuf_pp);
	async_p->kick(async_p);
	restore(i_state);

	return 0;
	}
#endif

/*
 * Convert a non-mbuf buffer to mbuf and send it on the asy serial
 * transmitter
 */
#ifdef MODULE_async_write
ssize_t
async_write(struct async_s *async_p, void *buf, size_t cnt)
	{
	struct mbuf_s *mbuf_p;
	int i_state;

	if (async_p->iface == NULL)
		{
		return -1;
		}

	if (cnt == 0)
		{
		return 0;
		}

	mbuf_p = qdata(buf, cnt);
	if (mbuf_p == NULL)
		{
		return -1;
		}

	i_state = dirps();
	append(&async_p->sndq, &mbuf_p);
	async_p->kick(async_p);
	restore(i_state);

	return cnt;
	}
#endif

/* Process 8250 receiver interrupts */
#ifdef MODULE_async_rx
void
async_rx(struct async_s *async_p, unsigned char ch)
	{
	if (async_p->cnt >= async_p->bufsize)
		{
		/* If buffer is full, we have no choice but
		 * to drop the character
		 */
		async_p->overrun++;
		return;
		}

	*async_p->wp++ = ch;
	if (async_p->wp >= &async_p->buf[async_p->bufsize])
		{
		async_p->wp = async_p->buf; /* Wrap around */
		}
	async_p->cnt++;
	if (async_p->cnt > async_p->hiwat)
		{
		async_p->hiwat = async_p->cnt;
		}

	if (async_p->trigchar == -1 || (unsigned char)async_p->trigchar == ch)
		{
		ksig(&async_p->cnt, 0);
		}
	}
#endif

/*
 * Transmitter interrupt handler
 * This routine sends data from mbufs. It doesn't try to find out
 * nor does it make assumptions as to the type of frame in the buffer
 * being sent.
 */
#ifdef MODULE_async_tx
unsigned int
async_tx(struct async_s *async_p)
	{
	struct mbuf_s *mbuf_p;

	mbuf_p = async_p->sndq;
	while (mbuf_p)
		{
		if (mbuf_p->cnt)
			{
			mbuf_p->cnt--;
			return (unsigned char)*mbuf_p->data++;
			}

		mbuf_p = free_m(mbuf_p);
		async_p->sndq = mbuf_p;
		}

	return -1;
	}
#endif

