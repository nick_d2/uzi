/* OS- and machine-dependent stuff for the Z180 ASCI in a Hytech 1000/1500
 * Copyright 1991 Phil Karn, KA9Q
 *
 * 16550A support plus some statistics added mah@hpuviea.at 15/7/89
 *
 * CTS hardware flow control from dkstevens@ucdavis,
 * additional stats from delaroca@oac.ucla.edu added by karn 4/17/90
 * Feb '91      RLSD line control reorganized by Bill_Simpson@um.cc.umich.edu
 * Sep '91      All control signals reorganized by Bill Simpson
 * Apr '92	Control signals redone again by Phil Karn
 */
#include <stdio.h>
/* Nick #include <dos.h> */
#include <errno.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include "kernel/thread.h"
#include "main/iface.h"
#include "z80/abus.h"
#include "main/devparam.h"
#include "nos/nospc.h"
#include "ppp/dialer.h"
#include <libintl.h>
#include "po/messages.h"

#ifdef MODULE
#define STATIC
extern unsigned char Api_refs; /* how many different lines open */
#else
#define STATIC static
#define MODULE_Api
#define MODULE_Api_refs
#define MODULE_Api_cflag
#define MODULE_Api_txsti
#define MODULE_Api_rxsti
/*#define MODULE_Api_turbo*/
#define MODULE_Api_count
#define MODULE_Api_state
#define MODULE_Api_dirn
#define MODULE_Api_robin
#define MODULE_Api_rxovr
#define MODULE_Api_devok
#define MODULE_Api_kick
#define MODULE_abus_init
#define MODULE_abus_stop
#define MODULE_abus_ioctl
/*#define MODULE_abus_open*/
/*#define MODULE_abus_close*/
#define MODULE_abus_kick
#define MODULE_doabusstat
#define MODULE_pabus
#endif

STATIC void abus_kick(struct async_s *async_p);
STATIC void pabus(struct abus_s *abus_p);

#ifdef MODULE_Api
struct abus_s Api[API_MAX];
#endif

/* private variable for use by apibus.c */
#ifdef MODULE_Api_refs
STATIC unsigned char Api_refs; /* how many different lines open */
#endif

/* public interface to apivec.S (must be zeroed on boot) */
#ifdef MODULE_Api_cflag
unsigned char Api_cflag; /* apibus injected command byte */
#endif
#ifdef MODULE_Api_txsti
unsigned char Api_txsti; /* apibus sticky counter for tx */
#endif
#ifdef MODULE_Api_rxsti
unsigned char Api_rxsti; /* apibus sticky counter for rx */
#endif
#if 0 /*def MODULE_Api_turbo*/
unsigned char Api_turbo; /* counter for continuous polls */
#endif

/* private variables for apivec.S (must be initialized on attach) */
#ifdef MODULE_Api_count
unsigned char Api_count;
#endif
#ifdef MODULE_Api_state
unsigned char Api_state;
#endif
#ifdef MODULE_Api_dirn
unsigned char Api_dirn;
#endif
#ifdef MODULE_Api_robin
unsigned char Api_robin;
#endif
#ifdef MODULE_Api_rxovr
unsigned char Api_rxovr;
#endif
#ifdef MODULE_Api_devok
unsigned char Api_devok;
#endif
#ifdef MODULE_Api_kick
unsigned char Api_kick;
#endif

/* Initialize apibus port "dev" */
#ifdef MODULE_abus_init
int
abus_init(dev,ifp,bufsize,trigchar)
int dev;
struct iface_s *ifp;
unsigned int bufsize;
int trigchar;
{
	struct abus_s *abus_p;
	int i_state;

	abus_p = &Api[dev];

	/* Set up receiver fifo */
	abus_p->async.buf = malloc(bufsize);
	if (abus_p->async.buf == NULL)
		{
		return -1;
		}

	abus_p->async.bufsize = bufsize;
	abus_p->async.wp = abus_p->async.rp = abus_p->async.buf;
	abus_p->async.cnt = 0;
	abus_p->async.hiwat = 0;
	abus_p->async.overrun = 0;
	abus_p->async.trigchar = trigchar;

	abus_p->async.iface = ifp;
	abus_p->async.kick = abus_kick;

	if (Api_refs == (unsigned char)0)
		{
		i_state = dirps();

		/* atomically initialize private variables for apivec.S */
		Api_robin = 1;
		Api_rxovr = 1;

#if 0 /* note, this wouldn't work properly, ap may be different on closure */
		/* Save original interrupt vectors */
		abus_p->save.int1vec = getirq(8);
		abus_p->save.csiovec = getirq(14);
#endif

		/* Set interrupt vectors to INT1 and CSI/O handlers */
		setirq(8,int1_vec);
		setirq(14,csio_vec);

		/* Enable interrupt */
		outportb(CNTR, 0); /* maskoff(14); is not implemented */
		maskon(8); /* want to kick things off with an INT1 */

		restore(i_state);
		}

	/* assert(Api_refs != (unsigned char)0xff); */
	Api_refs++;

	return 0;
}
#endif

#ifdef MODULE_abus_stop
int
abus_stop(ifp)
struct iface_s *ifp;
{
	struct abus_s *abus_p;
	struct dialer_s *dialer;
	int i_state;

/* note: should wait for an appropriate time, so as not to lock up the bus!! */
/* for now, we can make sure this is never called, also not from main_exit() */

	abus_p = &Api[ifp->dev];

	if(abus_p->async.iface == NULL)
		{
		return -1;	/* Not allocated */
		}
	abus_p->async.iface = NULL;

#if 0
	if(ifp->dstate != NULL){
		dialer = (struct dialer_s *)ifp->dstate;
		stop_timer(&dialer->idle);	/* Stop the idle timer, if running */
		free(dialer);
		ifp->dstate = NULL;
	}
#endif

	abus_p->async.bufsize = 0; /* just to make sure */
	abus_p->async.cnt = 0; /* so bufsize - cnt == 0 */
	free(abus_p->async.buf);

/* OOPS: this is a bit abrupt, as we may have promised some bytes to a node */
	free_p(&abus_p->async.sndq); /* just to make sure */

	if (Api_refs != (unsigned char)0)
		{
		Api_refs--;
		if (Api_refs == (unsigned char)0)
			{
			/* Restore original vectors and disable interrupts */
			i_state = dirps();
#if 0 /* note, this wouldn't work properly, ap may be different on closure */
			setirq(8,abus_p->save.int1vec);
			setirq(14,abus_p->save.csiovec);
#endif
			maskoff(8);
			outportb(CNTR, 0);
			restore(i_state);
			}
		}

	return 0;
}
#endif

/* apibus line I/O control */
#ifdef MODULE_abus_ioctl
unsigned long
abus_ioctl(struct iface_s *ifp, int cmd, int set, unsigned long val)
{
	struct abus_s *abus_p = &Api[ifp->dev];

	switch(cmd){
	case PARAM_DTR:
		return TRUE;
	case PARAM_RTS:
		return TRUE;
	case PARAM_DOWN:
		return FALSE;
	case PARAM_UP:
		return TRUE;
	}
	return -1;
}
#endif

/* Open an apibus port for direct I/O, temporarily suspending any
 * packet-mode operations. Returns device number for abus_write and get_api
 */
#if 0 /*def MODULE_abus_open*/
int
abus_open(name)
char *name;
{
	struct iface_s *ifp;
	int dev;
	int i_state;

	if((ifp = if_lookup(name)) == NULL){
		errno = ENODEV;
		return -1;
	}
	if((dev = ifp->dev) >= API_MAX || Api[dev].async.iface != ifp){
		errno = EINVAL;
		return -1;
	}

	/* Save and clear the trigger character */
	i_state = dirps();
	if (Api[dev].suspend_count == 0)
		{
		Api[dev].trigchar_save = Api[dev].trigchar;
		Api[dev].trigchar = -1; /*0x100;*/ /* none */
		}
	Api[dev].suspend_count++;
	restore(i_state);

	/* Suspend the packet drivers */
	suspend(ifp->rxproc);
	suspend(ifp->txproc);

	/* bring the line up (just in case) */
	if(ifp->ioctl != NULL)
		(*ifp->ioctl)(ifp,PARAM_UP,TRUE,0L);
	return dev;
}
#endif

#if 0 /*def MODULE_abus_close*/
int
abus_close(dev)
int dev;
{
	struct iface_s *ifp;
	int i_state;

	if(dev < 0 || dev >= API_MAX){
		errno = EINVAL;
		return -1;
	}
	/* Resume the packet drivers */
	if((ifp = Api[dev].async.iface) == NULL){
		errno = EINVAL;
		return -1;
	}

	/* Restore protocol's trigger character */
	i_state = dirps();
	Api[dev].suspend_count--;
	if (Api[dev].suspend_count)
		{
		restore(i_state);
		return 0;
		}
	Api[dev].trigchar = Api[dev].trigchar_save;
	restore(i_state);

	/* this should be protected by dirps but may be too slow: */
	resume(ifp->rxproc);
	resume(ifp->txproc);
	return 0;
}
#endif

/* Tell the round robin algorithm that something's waiting to be sent */
#ifdef MODULE_abus_kick
STATIC void
abus_kick(struct async_s *async_p)
	{
	Api_kick = 1;
	}
#endif

#ifdef MODULE_doabusstat
int
doabusstat(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct abus_s *abus_p;
	struct iface_s *ifp;
	int i;

	if(argc < 2){
		for(abus_p = Api;abus_p < &Api[API_MAX];abus_p++){
			if(abus_p->async.iface != NULL)
				pabus(abus_p);
		}
		return 0;
	}
	for(i=1;i<argc;i++){
		if((ifp = if_lookup(argv[i])) == NULL){
			_printf(_("Interface %s unknown\n"),argv[i]);
			continue;
		}
		for(abus_p = Api;abus_p < &Api[API_MAX];abus_p++){
			if(abus_p->async.iface == ifp){
				pabus(abus_p);
				break;
			}
		}
		if(abus_p == &Api[API_MAX])
			_printf(_("Interface %s not api\n"),argv[i]);
	}

	return 0;
}
#endif

#ifdef MODULE_pabus
STATIC void
pabus(abus_p)
struct abus_s *abus_p;
{
	int mcr;

	_printf(_("%s:"),abus_p->async.iface->name);
	if(abus_p->async.trigchar != -1)
		_printf(_(" [trigger 0x%02x]"),abus_p->async.trigchar);
	_printf(_("\n"));

	_printf(_(" sw over %lu sw hi %u\n"),
	 abus_p->async.overrun,abus_p->async.hiwat);
	abus_p->async.hiwat = 0;
}
#endif

