/* OS- and machine-dependent stuff for the Z180 ASCI in a Hytech 1000/1500
 * Copyright 1991 Phil Karn, KA9Q
 *
 * 16550A support plus some statistics added mah@hpuviea.at 15/7/89
 *
 * CTS hardware flow control from dkstevens@ucdavis,
 * additional stats from delaroca@oac.ucla.edu added by karn 4/17/90
 * Feb '91      RLSD line control reorganized by Bill_Simpson@um.cc.umich.edu
 * Sep '91      All control signals reorganized by Bill Simpson
 * Apr '92	Control signals redone again by Phil Karn
 * Apr '04	Intl' of strings
 */
#include <stdio.h>
/* Nick #include <dos.h> */
#include <errno.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include "main/iface.h"
#include "z80/asci.h"
#include "main/devparam.h"
#include "nos/nospc.h"
#include "ppp/dialer.h"
#include <libintl.h>
#include "po/messages.h"

struct asci_rate_s
	{
	unsigned char cntlb;
	unsigned long cutbps;
	};
#define ASCI_RATE_MAX 20

#ifdef MODULE
#define STATIC
extern struct asci_rate_s Asci_rate[ASCI_RATE_MAX];
extern void INTERRUPT (*Handle[ASCI_MAX])();
#else
#define STATIC static
#define MODULE_Asci
#define MODULE_Asci_rate
#define MODULE_Handle
#define MODULE_asci_init
#define MODULE_asci_stop
#define MODULE_asci_speed
#define MODULE_asci_ioctl
/*#define MODULE_asci_open*/
/*#define MODULE_asci_close*/
#define MODULE_asci_kick
#define MODULE_asci_int
/*#define MODULE_ascimsint*/
/*#define MODULE_get_rlsd_asci*/
/*#define MODULE_asci_timer*/
#define MODULE_doascistat
/*#define MODULE_pasci*/
#endif

STATIC void asci_kick(struct async_s *async_p);
/*STATIC void ascimsint(struct asci_s *asci_p);*/
/*STATIC void pasci(struct asci_s *asci_p);*/

#ifdef MODULE_Asci
struct asci_s *Asci[ASCI_MAX];
#endif

#ifdef MODULE_Asci_rate
STATIC struct asci_rate_s Asci_rate[ASCI_RATE_MAX] =
	{
	{ 0x00, 86400 },	/* PS=0 DR=0 SS=0 rat=115200 cut=86400 */
	{ 0x01, 48000 },	/* PS=0 DR=0 SS=1 rat= 57600 cut=48000 */
	{ 0x20, 33600 },	/* PS=1 DR=0 SS=0 rat= 38400 cut=33600 */
	{ 0x02, 24000 },	/* PS=0 DR=0 SS=2 rat= 28800 cut=24000 */
	{ 0x21, 16800 },	/* PS=1 DR=0 SS=1 rat= 19200 cut=16800 */
	{ 0x03, 12000 },	/* PS=0 DR=0 SS=3 rat= 14400 cut=12000 */
	{ 0x22,  8400 },	/* PS=1 DR=0 SS=2 rat=  9600 cut= 8400 */
	{ 0x04,  6000 },	/* PS=0 DR=0 SS=4 rat=  7200 cut= 6000 */
	{ 0x23,  4200 },	/* PS=1 DR=0 SS=3 rat=  4800 cut= 4200 */
	{ 0x05,  3000 },	/* PS=0 DR=0 SS=5 rat=  3600 cut= 3000 */
	{ 0x24,  2100 },	/* PS=1 DR=0 SS=4 rat=  2400 cut= 2100 */
	{ 0x06,  1500 },	/* PS=0 DR=0 SS=6 rat=  1800 cut= 1500 */
	{ 0x25,  1050 },	/* PS=1 DR=0 SS=5 rat=  1200 cut= 1050 */
	{ 0x07,   750 },	/* PS=0 DR=0 SS=7 rat=   900 cut=  750 */
	{ 0x26,   525 },	/* PS=1 DR=0 SS=6 rat=   600 cut=  525 */
	{ 0x0e,   375 },	/* PS=0 DR=1 SS=6 rat=   450 cut=  375 */
	{ 0x27,   263 },	/* PS=1 DR=0 SS=7 rat=   300 cut=  263 */
	{ 0x0f,   188 },	/* PS=0 DR=1 SS=7 rat=   225 cut=  188 */
	{ 0x2e,   113 },	/* PS=1 DR=1 SS=6 rat=   150 cut=  113 */
	{ 0x2f,     0 },	/* PS=1 DR=1 SS=7 rat=    75 */
	};
#endif

/* ASCI interrupt handlers */
#ifdef MODULE_Handle
STATIC void INTERRUPT (*Handle[ASCI_MAX])() =
	{
	asci0_vec,
	asci1_vec
	};
#endif

/* Initialize asynch port "dev" */
#ifdef MODULE_asci_init
struct asci_s *
asci_init(dev,ifp,base,irq,bufsize,trigchar,speed,cts,rlsd,chain)
int dev;
struct iface_s *ifp;
int base;
int irq;
unsigned int bufsize;
int trigchar;
long speed;
int cts;		/* Use CTS flow control */
int rlsd;		/* Use Received Line Signal Detect (aka CD) */
int chain;		/* Chain interrupts */
	{
	register struct asci_s *asci_p;
	int i_state;

	if (dev < 0 || dev >= ASCI_MAX)
		{
		return NULL;
		}

	asci_p = Asci[dev];
	if (asci_p == NULL)
		{
		asci_p = malloc(sizeof(struct asci_s));
		if (asci_p == NULL)
			{
			return NULL;
			}
		memset(asci_p, 0, sizeof(struct asci_s));
		Asci[dev] = asci_p;
		}
	else if (asci_p->async.iface)
		{
		_printf(_("ASCI channel %d already attached\n"), dev);
		return NULL;
		}

	asci_p->async.buf = malloc(bufsize);
	if (asci_p->async.buf == NULL)
		{
		return NULL;
		}

	asci_p->async.bufsize = bufsize;
	asci_p->async.wp = asci_p->async.rp = asci_p->async.buf;
	asci_p->async.cnt = 0;
	asci_p->async.hiwat = 0;
	asci_p->async.overrun = 0;
	asci_p->async.trigchar = trigchar;

	asci_p->async.iface = ifp;
	asci_p->async.kick = asci_kick;

	asci_p->addr = base;
	asci_p->vec = irq;
	asci_p->chain = chain;

	/* Purge the receive data buffer */
	(void)inportb(base + RDR);

	i_state = dirps();

	/* Save original interrupt vector, mask state, control bits */
	if (asci_p->vec != -1)
		{
		asci_p->save.vec = getirq(asci_p->vec);
#if 0 /* out for now */
		asci_p->save.mask = getmask(asci_p->vec);
#endif
		}
	asci_p->save.cntla = inportb(base + CNTLA);
	asci_p->save.stat = inportb(base + STAT);
	/* no point saving cntlb, but it may be valid from an earlier init */

	/* save modem control flags */
	asci_p->cts = cts;
	asci_p->rlsd = rlsd;

	/* Set interrupt vector to SIO handler */
	if (asci_p->vec != -1)
		{
		setirq(asci_p->vec,Handle[dev]);
		}

	/* Set line control register: 8 bits, no parity */
	outportb(base + CNTLA,CNTLA_8BITS|CNTLA_RTS_CKA1D|CNTLA_TE|CNTLA_RE);
	outportb(base + STAT,STAT_RIE);

#if 0 /* Nick... I don't think they are connected */
	/* Enable use of DCD & CTS on chan0 if so requested */
	outportb(base + EXCR0, (asci_p->cts ? EXCR0_CTS0 : 0) |
	                    (asci_p->rlsd ? EXCR0_DCD0 : 0));
#endif

	/* Enable interrupt */
	if (asci_p->vec != -1)
		{
		maskon(asci_p->vec);
		}
	restore(i_state);

	if (asci_speed(asci_p, speed) == -1)
		{
		asci_p->async.iface = NULL;
		return NULL;
		}

	return asci_p;
	}
#endif

#ifdef MODULE_asci_stop
int
asci_stop(struct asci_s *asci_p)
	{
	struct iface_s *ifp;
	unsigned int base;
	struct dialer_s *dialer;
	int i_state;

	ifp = asci_p->async.iface;
	if (ifp == NULL)
		{
		return -1;
		}
	asci_p->async.iface = NULL;

	if (ifp->dstate != NULL)
		{
		dialer = (struct dialer_s *)ifp->dstate;
		stop_timer(&dialer->idle);	/* Stop the idle timer, if running */
		free(dialer);
		ifp->dstate = NULL;
		}

	base = asci_p->addr;
	inportb(base + RDR); /* Purge the receive data buffer */

#if 0 /* good idea but not needed as no interrupt can occur until restarted */
	asci_p->dma.cnt = 0; /* just to make sure */
	asci_p->async.bufsize = 0; /* just to make sure */
	asci_p->async.cnt = 0; /* so bufsize - cnt == 0 */
#endif

	/* Restore original interrupt vector and 8259 mask state */
	i_state = dirps();
	if (asci_p->vec != -1)
		{
		setirq(asci_p->vec,asci_p->save.vec);
#if 0 /* out for now */
		if (asci_p->save.mask)
			{
			maskon(asci_p->vec);
			}
		else
#endif
			{
			maskoff(asci_p->vec);
			}
		}

	/* Restore control regs */
	outportb(base + CNTLA,asci_p->save.cntla);
	outportb(base + STAT,asci_p->save.stat);
	restore(i_state);

	asci_p->async.bufsize = 0; /* just to make sure */
	asci_p->async.cnt = 0; /* so bufsize - cnt == 0 */
	free(asci_p->async.buf);

	free_p(&asci_p->async.sndq); /* just to make sure */
	return 0;
	}
#endif

/* Set asynch line speed */
#ifdef MODULE_asci_speed
int
asci_speed(struct asci_s *asci_p, unsigned long bps)
	{
	unsigned int base;
	unsigned char cntlb;
	struct asci_rate_s *rate_p;
	int i_state;

	if (asci_p->async.iface == NULL)
		{
		return -1;
		}

	if (bps <= 0)
		{
		return -1;
		}
	asci_p->speed = bps;

	base = asci_p->addr;
	for (rate_p = Asci_rate; bps < rate_p->cutbps; rate_p++)
		;
	cntlb = rate_p->cntlb;

	i_state = dirps();

	/* Purge the receive data buffer */
	inportb(base + RDR);

	/* modify PS, DR, SS2, SS1, SS0, preserving MPBT, MP, PEO */
	cntlb = ((asci_p->cntlb ^ cntlb) &
			(CNTLB_MPBT|CNTLB_MP|CNTLB_PEO)) ^ cntlb;

	outportb(base + CNTLB, cntlb);
	asci_p->cntlb = cntlb;

	restore(i_state);
	return 0;
	}
#endif

/* Asynchronous line I/O control */
#ifdef MODULE_asci_ioctl
unsigned long
asci_ioctl(struct asci_s *asci_p, int cmd, int set, unsigned long val)
	{
	unsigned int base;

	if (asci_p->async.iface == NULL)
		{
		return -1;
		}
	base = asci_p->addr;

	switch(cmd){
	case PARAM_SPEED:
		if (set)
			{
			asci_speed(asci_p, val);
			}
		return asci_p->speed;
	case PARAM_DTR:
#if 1 /* temporary */
		return TRUE;
#else
		if (set)
			{
			writebit(base + MCR,MCR_DTR,(int)val);
			}
		return (inportb(base + MCR) & MCR_DTR) ? TRUE : FALSE;
#endif
	case PARAM_RTS:
		return TRUE;
	case PARAM_DOWN:
#if 0
		clrbit(base + MCR,MCR_RTS);
		clrbit(base + MCR,MCR_DTR);
#endif
		return FALSE;
	case PARAM_UP:
#if 0
		setbit(base + MCR,MCR_RTS);
		setbit(base + MCR,MCR_DTR);
#endif
		return TRUE;
		}
	return -1;
	}
#endif

/* Open an asynch port for direct I/O, temporarily suspending any
 * packet-mode operations. Returns device number for async_write and async_getc
 */
#if 0 /*def MODULE_asci_open*/
int
asci_open(name)
char *name;
{
	struct iface_s *ifp;
	int dev;
	int i_state;

	if ((ifp = if_lookup(name)) == NULL){
		errno = ENODEV;
		return -1;
	}
	if ((dev = ifp->dev) >= ASCI_MAX || Asci[dev]->async.iface != ifp){
		errno = EINVAL;
		return -1;
	}

	/* Save and clear the trigger character */
	i_state = dirps();
	if (Asci[dev]->suspend_count == 0)
		{
		Asci[dev]->trigchar_save = Asci[dev]->trigchar;
		Asci[dev]->trigchar = -1; /* none */
		}
	Asci[dev]->suspend_count++;
	restore(i_state);

	/* Suspend the packet drivers */
	suspend(ifp->rxproc);
	suspend(ifp->txproc);

	/* bring the line up (just in case) */
	if (ifp->ioctl != NULL)
		(*ifp->ioctl)(ifp,PARAM_UP,TRUE,0L);
	return dev;
}
#endif

#if 0 /*def MODULE_asci_close*/
int
asci_close(dev)
int dev;
{
	struct iface_s *ifp;
	int i_state;

	if (dev < 0 || dev >= ASCI_MAX){
		errno = EINVAL;
		return -1;
	}

	/* Resume the packet drivers */
	ifp = Asci[dev]->async.iface;
	if (ifp == NULL){
		errno = EINVAL;
		return -1;
	}

	/* Restore protocol's trigger character */
	i_state = dirps();
	Asci[dev]->suspend_count--;
	if (Asci[dev]->suspend_count)
		{
		restore(i_state);
		return 0;
		}
	Asci[dev]->trigchar = Asci[dev]->trigchar_save;
	restore(i_state);

	/* this should be protected by dirps but may be too slow: */
	resume(ifp->rxproc);
	resume(ifp->txproc);
	return 0;
}
#endif

/* Send first char(s) in polled mode, in case setting TIE doesn't interrupt */
#ifdef MODULE_asci_kick
STATIC void
asci_kick(struct async_s *async_p)
	{
	unsigned int base;
	unsigned char stat;
	unsigned int ch;

	base = ((struct asci_s *)async_p)->addr;
#if 0
	if (((struct asci_s *)async_p)->cts &&
			(inportb(base + CNTLB) & CNTLB_CTS_PS) == 0)
		{
		return;
		}
#endif
	stat = inportb(base + STAT);
	outportb(base + STAT, stat | STAT_TIE);
	if (stat & STAT_TDRE)
		{
		ch = async_tx(async_p);
		if (ch == -1)
			{
			outportb(base + STAT, stat & ~STAT_TIE);
			}
		else
			{
			outportb(base + TDR, ch);
			}
		}
	}
#endif

/* Interrupt handler for 8250 asynch chip (called from ascivec.S) */
#ifdef MODULE_asci_int
void INTERRUPT asci_int(struct asci_s *asci_p)
{
	unsigned int base;
	unsigned char stat;
	unsigned int ch;

	asci_p->ints++;

	base = asci_p->addr;
	stat = inportb(base + STAT);

#if 0 /* Nick... I don't think they are connected */
	if ((stat & STAT_DCD0) != (asci_p->stat & STAT_DCD0))
		{
		asci_p->stat = stat;
		ascimsint(asci_p);
		}
#endif

	/* check for receiver interrupt */
	if (stat & (STAT_OVRN|STAT_RDRF))
		{
		if (stat & STAT_OVRN)
			{
			asci_p->overrun++;
			}

		if (stat & STAT_RDRF)
			{
			async_rx(&asci_p->async, inportb(base + RDR));
			/*asci_p->rxchar++;*/
			}
		}

	/* check for transmitter interrupt */
	if ((stat & (STAT_TIE|STAT_TDRE)) == (STAT_TIE|STAT_TDRE))
		{
		ch = async_tx(&asci_p->asy);
		if (ch == -1)
			{
			outportb(base + STAT, stat & ~STAT_TIE);
			}
		else
			{
			outportb(base + TDR, ch);
			/*asci_p->txchar++;*/
			}
		}
	}
#endif

#if 0 /*def MODULE_ascimsint*/
STATIC void
ascimsint(asci_p)
struct asci_s *asci_p;
	{
	asci_p->msint_count++;
	ksig(&asci_p->stat, 0);
	}
#endif

/* Wait for a signal that the RLSD modem status has changed */
#if 0 /*def MODULE_get_rlsd_asci*/
int
get_rlsd_asci(dev, new_rlsd)
int dev;
int new_rlsd;
{
	struct asci_s *asci_p;

	if (dev < 0 || dev >= ASCI_MAX)
		{
		return -1;
		}

	asci_p = Asci[dev];
	if (asci_p == NULL || asci_p->async.iface == NULL)
		{
		return -1;
		}

	if (asci_p->rlsd == 0)
		return -1;

	for (;;){
		if (new_rlsd && (asci_p->stat & STAT_DCD0))
			return 0;
		if (!new_rlsd && !(asci_p->stat & STAT_DCD0))
			return 1;

		/* Wait for state change to requested value */
		ppause(2L);
		kwait(&(asci_p->stat));
	}
}
#endif

/* Poll the asynch input queues; called on every clock tick.
 * This helps limit the interrupt ring buffer occupancy when long
 * packets are being received.
 */

#if 0 /*def MODULE_asci_timer*/
void
asci_timer(void)
	{
	register struct asci_s *asci_p;
	unsigned int base;
	register int i;
	int i_state;

	for (i = 0; i < ASCI_MAX; i++)
		{
		asci_p = Asci[i];
		if (asci_p && asci_p->async.iface)
			{
			if (asci_p->async.cnt)
				ksignal(&asci_p->async.cnt, 0);

			base = asci_p->addr;
			if (asci_p->dma.cnt &&
					(inportb(base + STAT) & STAT_TDRE)
#if 0
					&& (asci_p->cts == 0 ||
					(inportb(base + CNTLB) & CNTLB_CTS_PS))
#endif
					)
				{
				asci_p->txto++;
				i_state = dirps();
				asci_kick(&asci_p->asy);
				restore(i_state);
				}
			}
		}
	}
#endif

#ifdef MODULE_doascistat
int
doascistat(argc,argv,p)
int argc;
char *argv[];
void *p;
	{
#if 1
	struct asci_s *asci_p;
	int i;

	_printf(_("Ch Iface    SwOver   HwOver     Ints\n"));
	for (i = 0; i <= ASCI_MAX; i++)
		{
		asci_p = Asci[i];
		if (asci_p == NULL || asci_p->async.iface == NULL)
			{
			continue;
			}

		_printf(_("%2d %-6s %8lu %8lu %8lu\n"),
				i,asci_p->async.iface->name,asci_p->async.overrun,
				asci_p->overrun,asci_p->ints);
		}
#else
	register struct asci_s *asci_p;
	struct iface_s *ifp;
	unsigned int i, j;

	if (argc < 2)
		{
		for (i = 0; i < ASCI_MAX; i++)
			{
			asci_p = Asci[i];
			if (asci_p && asci_p->async.iface)
				{
				pasci(asci_p);
				}
			}
		return 0;
		}

	for (j = 1; j < argc; j++)
		{
		ifp = if_lookup(argv[j]);
		if (ifp == NULL)
			{
			_printf(_("Interface %s unknown\n"), argv[j]);
			continue;
			}

		for (i = 0; i < ASCI_MAX; i++)
			{
			asci_p = Asci[i];
			if (asci_p && asci_p->async.iface == ifp)
				{
				pasci(asci_p);
				goto found;
				}
			}

		_printf(_("Interface %s not asci\n"), argv[j]);
	found:
		;
		}
#endif

	return 0;
	}
#endif

#if 0 /*def MODULE_pasci*/
STATIC void
pasci(struct asci_s *asci_p)
	{
	unsigned char stat, cntla, cntlb;

	_printf(_("%s:"),asci_p->async.iface->name);

	/* Very quick and dirty */
	if (!strcmp(asci_p->async.iface->name, "asci0"))
		{
		if (asci_p->async.trigchar != -1)
			_printf(_(" [trigger 0x%02x]"),asci_p->async.trigchar);
		if (asci_p->cts)
			_printf(_(" [cts flow control]"));
		if (asci_p->rlsd)
			_printf(_(" [rlsd line control]"));

		_printf(_(" %lu bps\n"),asci_p->speed);

		cntla = inportb(asci_p->addr + CNTLA);
		cntlb = inportb(asci_p->addr + CNTLB);

		_printf(_(" ST: int %lu DCD %s  RTS %s  CTS %s  DSR %s  RI %s  CD %s\n"),
		 asci_p->ints,
		 !(asci_p->stat & STAT_DCD0) ? "On" : "Off",
		 (cntla & CNTLA_RTS0) ? "On" : "Off",
		 (cntlb & CNTLB_CTS_PS) ? "On" : "Off",
		 "On",
		 "Off",
		 "On");

		_printf(_(" RX: hw over %lu sw over %lu sw hi %u\n"),
		 asci_p->overrun,asci_p->async.overrun,asci_p->async.hiwat);
		asci_p->async.hiwat = 0;
		}
	else
		{
		if (asci_p->async.trigchar != -1)
			_printf(_(" [trigger 0x%02x]"),asci_p->async.trigchar);
		if (asci_p->cts)
			_printf(_(" [cts flow control]"));
		if (asci_p->rlsd)
			_printf(_(" [rlsd line control]"));

		_printf(_(" %lu bps\n"),asci_p->speed);

		_printf(_(" ST: int %lu DTR %s  RTS %s  CTS %s  DSR %s  RI %s  CD %s\n"),
		 asci_p->ints,
		"On", "On", "On", "On", "Off", "On");

		_printf(_(" RX: hw over %lu sw over %lu sw hi %u\n"),
		 asci_p->overrun,asci_p->async.overrun,asci_p->async.hiwat);
		asci_p->async.hiwat = 0;
		}
	}
#endif

