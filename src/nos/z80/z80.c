/* OS- and machine-dependent stuff for Hytech 1000/1500 running IAR 'C'
 * Copyright 1991 Phil Karn, KA9Q
 *
 * Apr 04       RPB     Po-ed most of the remaining strings
 */
#include <stdio.h>
/* Nick #include <conio.h> */
/* Nick #include <dir.h> */
/* Nick #include <dos.h> */
/* Nick #include <io.h> */
#include <sys/stat.h>
#include <string.h>
/* Nick #include <process.h> */
#include <fcntl.h>
/*#include <alloc.h> */
#include <stdarg.h>
/* Nick #include <bios.h> */
#include <time.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include "kernel/thread.h"
#include "main/iface.h"
#include "internet/internet.h"
#include "kernel/session.h"
#include "socket/socket.h"
#include "socket/usock.h"
#include "main/cmdparse.h"
#include "nos/nospc.h"
/*#include "x86/display.h"*/
#include "kernel/unix.h" /* for unix_tick */
#include <libintl.h>
#include "po/messages.h"

int Tick;
/* Nick static */ int32 Clock;

#if 1
void clockinit(void);
void clockshut(void);
#else
/* for ZILOG, Kbvec doesn't have a value for now, only an address */
extern void INTERRUPT (*Hwtvec)();
#endif

/* handler to be installed into rst 30h, see unixvec.S */
unsigned int unixvec(unsigned int rq, va_list ap);

/* Nick, lifted from x86/dos.c */
/* Reference count table for open file descriptors */
/* unsigned *Refcnt; */

/* crt0_kernel.S array describing the layout of RST vectors in the basepage */
extern struct
	{
	char opcode;
	void *vector;
	char padding[5];
	} __base_vectors[8];

/* crt0_kernel.S array describing the layout of the Z180's I/IL vector table */
extern void *__vector_table[9];

static void setvect(void **vector, void INTERRUPT (*handler)());
static void INTERRUPT (*getvect(void **vector))();

/* Called at startup time to set up console I/O, memory heap */
void
ioinit()
{
	/* Link timer handler into timer interrupt chain */
#if 0
	Hwtvec = getirq(HWTIRQ); /* this isn't really necessary for Z80 */
#endif
	setirq(HWTIRQ, hwtick);
	setirq(6, (void INTERRUPT (*)(void))unixvec); /* install into rst 30h */

#if 1
	clockinit(); /* new routine, set reload/counter regs, enable ints */
#endif
}

/* Called just before exiting to restore console state */
void
iostop()
{
	struct iface_s *ifp,*iftmp;
	void (**fp)(void);

	for(ifp = Ifaces;ifp != NULL;ifp = iftmp){
		iftmp = ifp->next;
		if_detach(ifp);
	}
	/* Call list of shutdown functions */
	for(fp = Shutdown;*fp != NULL;fp++){
		(**fp)();
	}

#if 1
	clockshut(); /* new routine, disable ints */
#else
	/* Restore original system tick vector */
	setirq(HWTIRQ, Hwtvec);
#endif
}

static void
setvect(vector, handler)
void **vector;
void INTERRUPT (*handler)();
{
	int state;

	state = dirps();
	*vector = (void *)(unsigned int)handler; /* explicitly toss the bank */
	restore(state);
}

static void INTERRUPT
(*getvect(vector))()
void **vector;
{
	int state;
	void INTERRUPT (*handler)();

	state = dirps();
	handler = (void INTERRUPT (*)())(unsigned long)*vector; /* zero the bank */
	restore(state);
	return handler;
}

/* Install hardware interrupt handler.
 * Takes IRQ numbers from 0-7 (0-15 on AT) and maps to actual 8086/286 vectors
 * Note that bus line IRQ2 maps to IRQ9 on the AT
 */
int
setirq(irq,handler)
unsigned irq;
void INTERRUPT (*handler)();
{
	/* Set interrupt vector */
	if(irq < 8){
		setvect(&__base_vectors[irq].vector, handler);
	} else if(irq < 17){
		setvect(&__vector_table[irq - 8], handler);
	} else {
		return -1;
	}
	return 0;
}

/* Return pointer to hardware interrupt handler.
 * Takes IRQ numbers from 0-7 (0-15 on AT) and maps to actual 8086/286 vectors
 */
void INTERRUPT
(*getirq(irq))()
unsigned int irq;
{
	/* Set interrupt vector */
	if(irq < 8){
		return getvect(&__base_vectors[irq].vector);
	} else if(irq < 17){
		return getvect(&__vector_table[irq - 8]);
	} else {
		return NULL;
	}
}

/* Disable hardware interrupt */
int
maskoff(irq)
unsigned irq;
	{
	switch (irq)
		{
	case 7:
		clrbit(ITC, INT0SW);
		break;
	case 8:
		clrbit(ITC, INT1SW);
		break;
	case 9:
		clrbit(ITC, INT2SW);
		break;
	default:
		return -1;
		}
	return 0;
	}

/* Enable hardware interrupt */
int
maskon(irq)
unsigned irq;
	{
	switch (irq)
		{
	case 7:
		setbit(ITC, INT0SW);
		break;
	case 8:
		setbit(ITC, INT1SW);
		break;
	case 9:
		setbit(ITC, INT2SW);
		break;
	default:
		return -1;
		}
	return 0;
	}

#if 0 /* doesn't seem to be referenced */
/* Return 1 if specified interrupt is enabled, 0 if not, -1 if invalid */
int
getmask(irq)
unsigned irq;
{
	if(irq < 8)
		return (inportb(0x21) & (1 << irq)) ? 0 : 1;
	else if(irq < 16){
		irq -= 8;
		return (inportb(0xa1) & (1 << irq)) ? 0 : 1;
	} else
		return -1;
}
#endif

#if 0 /* see z80_gen.S */
/* Called from assembler stub linked to BIOS interrupt 1C, called on each
 * hardware clock tick. Signal a clock tick to the timer process.
 */
void
ctick()
{
	unix_tick++;
	Tick++;
	Clock++;
	ksig/*nal*/(&Tick,1);
}
#endif

/* Read the Clock global variable, with interrupts off to avoid possible
 * inconsistency on 16-bit machines
 */
int32
rdclock()
{
	int i_state;
	int32 rval;

	i_state = dirps();
	rval = Clock;
	restore(i_state);
	return rval;
}

/* Set bit(s) in I/O port */
void
setbit(port,bits)
unsigned port;
char bits;
{
	outportb(port,inportb(port)|bits);
}

/* Clear bit(s) in I/O port */
void
clrbit(port,bits)
unsigned port;
char bits;
{
	outportb(port,inportb(port) & ~bits);
}

#if 0 /* doesn't seem to be referenced */
/* Set or clear selected bits(s) in I/O port */
void
writebit(port,mask,val)
unsigned port;
char mask;
int val;
{
	register char x;

	x = inportb(port);
	if(val)
		x |= mask;
	else
		x &= ~mask;
	outportb(port,x);
}
#endif

void *
ltop(l)
long l;
{
#ifdef ZILOG /* Nick */
	return (void *)l;
#else
	register unsigned seg,offset;

	seg = l >> 16;
	offset = l;
	return MK_FP(seg,offset);
#endif
}

#if 0 /* TEMPORARY!  secclock() and msclock() now a coarser resolution! */
/* Multiply a 16-bit multiplier by an arbitrary length multiplicand.
 * Product is left in place of the multiplicand, and the carry is
 * returned
 */
uint16
longmul(multiplier,n,multiplicand)
uint16 multiplier;
int n;				/* Number of words in multiplicand[] */
register uint16 *multiplicand;	/* High word is in multiplicand[0] */
{
	register int i;
	unsigned long pc;
	uint16 carry;

	carry = 0;
	multiplicand += n;
	for(i=n;i != 0;i--){
		multiplicand--;
		pc = carry + (unsigned long)multiplier * *multiplicand;
		*multiplicand = pc;
		carry = pc >> 16;
	}
	return carry;
}

/* Divide a 16-bit divisor into an arbitrary length dividend using
 * long division. The quotient is returned in place of the dividend,
 * and the function returns the remainder.
 */
uint16
longdiv(divisor,n,dividend)
uint16 divisor;
int n;				/* Number of words in dividend[] */
register uint16 *dividend;	/* High word is in dividend[0] */
{
	/* Before each division, remquot contains the 32-bit dividend for this
	 * step, consisting of the 16-bit remainder from the previous division
	 * in the high word plus the current 16-bit dividend word in the low
	 * word.
	 *
	 * Immediately after the division, remquot contains the quotient
	 * in the low word and the remainder in the high word (which is
	 * exactly where we need it for the next division).
	 */
	unsigned long remquot;
	register int i;

	if(divisor == 0)
		return 0;	/* Avoid divide-by-zero crash */
	remquot = 0;
	for(i=0;i<n;i++,dividend++){
		remquot |= *dividend;
		if(remquot == 0)
			continue;	/* Avoid unnecessary division */
#ifdef	__TURBOC__
		/* Use assembly lang routine that returns both quotient
		 * and remainder, avoiding a second costly division
		 */
		remquot = divrem(remquot,divisor);
		*dividend = remquot;	/* Extract quotient in low word */
		remquot &= ~0xffffL;	/* ... and mask it off */
#else
		*dividend = remquot / divisor;
		remquot = (remquot % divisor) << 16;
#endif
	}
	return remquot >> 16;
}
#endif

/* Return time since startup in milliseconds. If the system has an
 * 8254 clock chip (standard on ATs and up) then resolution is improved
 * below 55 ms (the clock tick interval) by reading back the instantaneous
 * value of the counter and combining it with the global clock tick counter.
 * Otherwise 55 ms resolution is provided.
 *
 * Reading the 8254 is a bit tricky since a tick could occur asynchronously
 * between the two reads. The tick counter is examined before and after the
 * hardware counter is read. If the tick counter changes, try again.
 * Note: the hardware counter counts down from 65536.
 */
int32
msclock()
{
#if 0 /* TEMPORARY!  secclock() and msclock() now a coarser resolution! */
	int32 hi;
	uint16 lo;
	uint16 count[4];	/* extended (48-bit) counter of timer clocks */

	if(!Isat)
#endif
		return rdclock() * MSPTICK;

#if 0 /* TEMPORARY!  secclock() and msclock() now a coarser resolution! */
	do {
		hi = rdclock();
		lo = clockbits();
	} while(hi != rdclock());

	count[0] = 0;
	count[1] = hi >> 16;
	count[2] = hi;
	count[3] = -lo;
	longmul(11,4,count);	/* The ratio 11/13125 is exact */
	longdiv(13125,4,count);
	return ((long)count[2] << 16) + count[3];
#endif
}

/* Return clock in seconds */
int32
secclock()
{
#if 0 /* TEMPORARY!  secclock() and msclock() now a coarser resolution! */
	int32 hi;
	uint16 lo;
	uint16 count[4];	/* extended (48-bit) counter of timer clocks */

	if(!Isat)
#endif
		return (rdclock() * MSPTICK) / 1000L;

#if 0 /* TEMPORARY!  secclock() and msclock() now a coarser resolution! */
	do {
		hi = rdclock();
		lo = clockbits();
	} while(hi != rdclock());

	count[0] = 0;
	count[1] = hi >> 16;
	count[2] = hi;
	count[3] = -lo;
	longmul(11,4,count);	/* The ratio 11/13125 is exact */
	longdiv(13125,4,count);
	longdiv(1000,4,count);	/* Convert to seconds */
	return ((long)count[2] << 16) + count[3];
#endif
}

#if 0 /* not used at the moment (reinstate later) */
/* Return time in raw clock counts, approx 838 ns */
int32
usclock()
{
	int32 hi;
	uint16 lo;

	do {
		hi = rdclock();
		lo = clockbits();
	} while(hi != rdclock());

	return (hi << 16) - (int32)lo;
}
#endif

#if 0 /* doesn't seem to be referenced */
/* Atomic read-and-decrement operation.
 * Read the variable pointed to by p. If it is
 * non-zero, decrement it. Return the original value.
 */
int
arddec(p)
volatile int *p;
{
	int tmp;
	int i_state;

	i_state = dirps();
	tmp = *p;
	if(tmp != 0)
		(*p)--;
	restore(i_state);
	return tmp;
}
#endif

