/* Generic driver for Z8530 boards, modified from the PE1CHL
 * driver for use with NOS. This version also supports the NRS
 * mode when used as an asynch port. Device setup is similar to
 * that of the PE1CHL version, with the addition of user specification
 * of buffer size (bufsize). See the file "escc.txt" for general
 * information on the use of this driver and setup procedures.
 *
 * General differences between this driver and the original version:
 *
 * 1) Slip encoding and decoding is not done in the driver, but
 *    using the routines in slip.c, and these routines are supported
 *    in a manner similar to the asynch routines for the 8250. The
 *    input is handled via async.fifo buffer, while output is direct. The
 *    routines async_send and async_getc are called via pointers in the
 *    Slip and Nrs structs for the parcticular channel.
 *
 * 2) The timer routine, escctim, is not installed directly in the
 *    timer interrupt chain, but is called through the systick routine
 *    in pc.c.
 *
 * 3) Facilities of nos are used whenever possible in place of direct
 *    structure or variable manipulation. Mbuf management is handled
 *    this way, along with interface initialization.
 *
 * 4) Nrs mode support is added in a manner similar to that of the
 *    Slip support. I have not had an opportunity to test this, but
 *    it is essentially identical to the way the 8250 version works.
 *
 * 5) Callsign specification on radio modes (kiss,nrs,ax25) is an
 *    option. If not supplied, the value of Mycall will be used.
 *
 * 6) Bufsize specification is now a parameter on setup of each channel.
 *    This is the size of the async.fifo on asynch input, and the size of
 *    mbuf buffers for sdlc mode. Since the async.fifo buffer can fill up,
 *    this value should be reasonably large for asynch mode. Mbufs
 *    are chained when they fill up, so having a small bufsize with
 *    sdlc modes (ax25) does not result in loss of characters.
 *
 * 7) Because slip and nrs decoding is handled outside the driver,
 *    esccstat cannot be used to report sent and receive packet counts
 *    in asynch mode, and these fields are blanked on display in asynch
 *    modes.
 *
 *
 * I am interested in setting up some default initializations for
 * the popular Z8530 boards, to minimize user problems in constructing
 * the proper attach init entries. These would allow for shortened
 * entries to use the defaults, such as "attach escc 1 init drsi" to
 * attach a DRSI board in standard configuration at its default address.
 * Since I do not have complete technical information on all such boards,
 * I would very much appreciate any information that users can provide
 * me regarding particular boards.
 *
 * 1/25/90
 *
 * Modifications:
 *
 * 2/17/90:
 *
 * 1) Added mods from PE1CHL which reflect relevent changes to the
 *    escc driver in his version of net between 10/89 and 1/90. Changes
 *    incorporated include additional delays in esccvec.asm, addition
 *    of external clock mode, and initialization for the 8536 as a
 *    clock divider on the DRSI board. "INLINE" is a slight delay
 *    for register access incorporated for use with the inline i/o
 *    code in MSC. This may not be useful or necessary with TURBO.
 *    Changes making "TPS" a variable were not added, since the
 *    escc timer does not install itself on the hardware interrupt
 *    in this version.
 *
 *
 * Ken Mitchum, KY3B       km@cs.pitt.edu  km@dsl.pitt.edu
 *                             or mail to the tcpip group
 *
 * 5 Aug 91:
 * Support added for Sealevel Systems Inc's ACB-IV 8530 card (HWSEALEVEL)
 * <hdwe> == HWSEALEVEL (0x10) sets the control/status port at chipbase + 4
 * to the value in <param>. (Where the control bits for both side's DTR
 * lines are; the 8530-internal DTR/REQB is used for DMA...)
 *
 * Added a side-effect to ATTACH ESCC's <speed> == 'ext'. Previously, all
 * async modes set a 16X clock; now when external clock is selected, the
 * baud rate divider in R4 is set to 1X.
 *
 * Tom Jennings, Cygnus Support		(tomj@cygnus.com)
 *
 *
 * 9 Aug 91:
 * Allow 'v' to specify Van Jacobson TCP header compression (SLIP only).
 * Because the 8th arg is optional [<call>], this checks for 'v' in the
 * 9th arg if that exists, otherwise the 8th arg is used. Not pretty
 * but servicable.
 *
 * Tom Jennings, Cygnus Support		(tomj@cygnus.com)
 */

/* Added ANSI-style prototypes, reformatted source, minor delinting.
 * Integrated into standard 900201 NOS by KA9Q.
 */

/*
 * Generic driver for Z8530 ESCC chip in SLIP, KISS or AX.25 mode.
 *
 * Written by R.E. Janssen (PE1CHL) using material from earlier
 * EAGLE and PC100 drivers in this package.
 *
 * The driver has initially been written for my own Atari ESCC interface
 * board, but it could eventually replace the other ESCC drivers.
 *
 * Unfortunately, there is little consistency between the different interface
 * boards, as to the use of a clock source, the solution for the fullduplex
 * clocking problem, and most important of all: the generation of the INTACK
 * signal.	Most designs do not even support the generation of an INTACK and
 * the read of the interrupt vector provided by the chip.
 * This results in lots of configuration parameters, and a fuzzy
 * polltable to be able to support multiple chips connected at one interrupt
 * line...
 *
 */

/*
 * 031126 - RPB: - Changed initialization of Z8530
 *               - Added PPP & modem control support
 *               - Added escc_write routine
 *		 - Added flags parameter to attach command (callsign is now
 *		   9th argument)
 *               - Extended esccstat with modem control signals
 * 070404 - RPB: - Po-ed most of the remaining strings
 */

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <time.h>
#include <errno.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include "main/config.h"
#include "internet/netuser.h"
#include "kernel/thread.h"
#include "main/iface.h"
/*#include "x86/pktdrvr.h"*/
#include "ppp/slip.h"
#include "ppp/ppp.h"
/*#include "netrom/nrs.h"*/
#include "z80/escc.h"
/*#include "x86/z8536.h"*/
/*#include "ax25/ax25.h"*/
#include "dump/trace.h"
#include "nos/nospc.h"
/*#include "ax25/kiss.h"*/
#include "main/devparam.h"
#include "main/main.h" /* for driver_process_p */
#include <libintl.h>
#include "po/messages.h"

#ifdef MODULE
#define STATIC
extern void INTERRUPT (*Origvec)();
#else
#define STATIC static
#define MODULE_Origvec
#define MODULE_Esccinfo
#define MODULE_Esccchan
/*#define MODULE_Esccmaxvec*/
/*#define MODULE_Esccpolltab*/
/*#define MODULE_Random*/
#define MODULE_escc_attach
#define MODULE_escc_init
#define MODULE_escc_async
/*#define MODULE_escc_sdlc*/
#define MODULE_escc_speed
#define MODULE_escc_stop
#define MODULE_escc_exit
#define MODULE_escc_aioctl
/*#define MODULE_escc_sioctl*/
#define MODULE_escc_kick
#define MODULE_doesccstat
/*#define MODULE_escc_raw*/
/*#define MODULE_escc_call*/
#define MODULE_escc_aex
#define MODULE_escc_asp
/*#define MODULE_escc_sdlctx*/
/*#define MODULE_escc_sdlcex*/
/*#define MODULE_escc_sdlcrx*/
/*#define MODULE_escc_sdlcsp*/
/*#define MODULE_escc_tossb*/
/*#define MODULE_escc_txon*/
/*#define MODULE_escc_txoff*/
/*#define MODULE_escc_timer*/
/*#define MODULE_get_rlsd_escc*/
#endif

/* interrupt handlers */
extern void INTERRUPT escc_vec();

/* variables used by the ESCC interrupt handler in escc_vec.S */
#ifdef MODULE_Origvec
STATIC void INTERRUPT (*Origvec)();	/* original interrupt vector */
#endif

#ifdef MODULE_Esccinfo
struct escc_info_s Esccinfo;		/* global info about ESCCs */
#endif
#ifdef MODULE_Esccchan
struct escc_s *Esccchan[2 * MAXESCC];	/* information per channel */
#endif
#if 0 /*def MODULE_Esccmaxvec*/
unsigned char Esccmaxvec;		/* maximum legal vector from ESCC */
#endif
#if 0 /*def MODULE_Esccpolltab*/
unsigned int Esccpolltab[MAXESCC+1][2];	/* polling table when no vectoring */
#endif
#if 0 /*def MODULE_Random*/
unsigned char Random;			/* random number for p-persist */
#endif

STATIC int escc_init(int nchips, unsigned int iobase, int space, int aoff,
		int boff, int doff, unsigned int intack, int ivec, long clk,
		int pclk, int hwtype, int hwparam);
/*STATIC int escc_raw(struct iface_s *ifp, struct mbuf_s **bpp);*/
/*STATIC int escc_call(struct iface_s *ifp, char *call);*/
STATIC int escc_stop(struct escc_s *escc);
STATIC int escc_async(struct escc_s *escc);
/*STATIC void escc_sdlc(struct escc_s *escc);*/
/*STATIC void escc_tossb(struct escc_s *escc);*/
/*STATIC void escc_txon(struct escc_s *escc);*/
STATIC void escc_txoff(struct escc_s *escc);
STATIC void escc_kick(struct async_s *async_p);
STATIC void escc_aex(struct escc_s *escc, unsigned char ch);
STATIC void escc_asp(struct escc_s *escc, unsigned char ch);
/*STATIC void escc_sdlctx(struct escc_s *escc);*/
/*STATIC void escc_sdlcex(struct escc_s *escc);*/
/*STATIC void escc_sdlcrx(struct escc_s *escc);*/
/*STATIC void escc_sdlcsp(struct escc_s *escc);*/
/*STATIC int get_rlsd_escc(struct escc_s *escc, int new_rlsd);*/

/* Attach an ESCC channel to the system, or initialize ESCC driver.
 * operation depends on argv[2]:
 * when "init", the ESCC driver is initialized, and global information about
 * the hardware is set up.
 * argv[0]: hardware type, must be "escc"
 * argv[1]: number of ESCC chips we will support
 * argv[2]: mode, must be: "init" in this case
 * argv[3]: base address of ESCC chip #0 (hex)
 * argv[4]: spacing between ESCC chip base addresses
 * argv[5]: offset from chip base address to channel A control register
 * argv[6]: offset from chip base address to channel B control register
 * argv[7]: offset from each channel's control register to data register
 * argv[8]: address of INTACK/Read Vector port. 0 to read from RR3A/RR2B
 * argv[9]: CPU interrupt vector number for all connected ESCCs
 * argv[10]: clock frequency (PCLK/RTxC) of all ESCCs in cycles per second
 *		 prefix with "p" for PCLK, "r" for RTxC clock (for baudrate gen)
 * argv[11]: optional hardware type (for special features)
 * argv[12]: optional extra parameter for special hardware
 *
 * otherwise, a single channel is attached using the specified parameters:
 * argv[0]: hardware type, must be "escc"
 * argv[1]: ESCC channel number to attach, 0/1 for first chip A/B, 2/3 for 2nd...
 *-RPB-
 * argv[2]: mode, can be:
 *		"slip", "kiss", "ax25", "ppp"
 *-RPB-
 * argv[3]: interface label, e.g., "sl0"
 * argv[4]: maximum transmission unit, bytes
 * argv[5]: interface speed, e.g, "1200". prefix with "d" when an external
 *		divider is available to generate the TX clock.	When the clock
 *		source is PCLK, this can be a /32 divider between TRxC and RTxC.
 *		When the clock is at RTxC, the TX rate must be supplied at TRxC.
 *		This is needed only for AX.25 fullduplex.
 *	    When this arg is given as "ext", the transmit and receive clock
 *	    are external, and the BRG and DPLL are not used.
 * argv[6]: buffer size
 *-RPB-
 * argv[7]: flags
 *		'v' for Van Jacobson TCP header compression (SLIP)
 *              'c' for cts flow control
 *              'r' for rlsd (dcd) detection
 *		'0' for no flags and there is a 9th arg (ie. [8] is a callsign.)
 * argv[8]: callsign used on the radio channels (optional)
 *-RPB-
 */
#ifdef MODULE_escc_attach
int
escc_attach(int argc, char *argv[], void *p)
{
	register struct iface_s *ifp;
	struct escc_s *escc;
	unsigned int chan,brgrate;
	int pclk = 0,hwtype = 0,hwparam = 0;
	int xdev;
	char *cp;

	/* first handle the special "init" mode, to initialize global stuff */

	if (!strcmp(argv[2],"init")){

	if (argc < 11) {			/* need at least argv[1]..argv[10] */
		_printf(_("Too few arguments for 'init' command\n"));
		return 0; /*-1;*/
	}

	if (isupper(argv[10][0]))
		argv[10][0] = tolower(argv[10][0]);

	if (argv[10][0] == 'p'){	/* wants to use PCLK as clock? */
		pclk = 1;
		argv[10]++;
	} else {
		if (argv[10][0] == 'r')	/* wants to use RTxC? */
		argv[10]++;		/* that's the default */
	}

	if (argc > 11)			/* optional hardware type */
		hwtype = htoi(argv[11]);	/* it is given in hex */

	if (argc > 12)			/* optional hardware param */
		hwparam = htoi(argv[12]);	/* also in hex */

	return escc_init(atoi(argv[1]),(unsigned int)htol(argv[3]),
		atoi(argv[4]),atoi(argv[5]),atoi(argv[6]),atoi(argv[7]),
		(unsigned int)htol(argv[8]),atoi(argv[9]),atol(argv[10]),
		pclk,hwtype,hwparam);
	}
	/* not "init", so it must be a valid mode to attach a channel */
	if (
#ifdef AX25
			strcmp(argv[2],"ax25") &&
#endif
#ifdef KISS
			strcmp(argv[2],"kiss") &&
#endif
#ifdef PPP
			strcmp(argv[2],"ppp") &&
#endif
			strcmp(argv[2],"slip")){
		_printf(_("Mode %s unknown for ESCC\n"),argv[2]);
		return 0; /*-1;*/
	}
	if (strcmp(argv[2],"slip") == 0
#ifdef KISS
			|| strcmp(argv[2],"kiss") == 0)
#endif
			){
		for (xdev = 0;xdev < SLIP_MAX;xdev++)
			if (Slip[xdev].iface == NULL)
				break;
		if (xdev >= SLIP_MAX){
			_printf(_("Too many slip devices\n"));
			return 0; /*-1;*/
		}
	}
#ifdef NRS
	if (strcmp(argv[2],"nrs") == 0){
		for (xdev = 0;xdev < NRS_MAX;xdev++)
			if (Nrs[xdev].iface == NULL)
				break;

		if (xdev >= NRS_MAX){
			_printf(_("Too many nrs devices\n"));
			return 0; /*-1;*/
		}
	}
#endif
	if (!Esccinfo.init){
		_printf(_("First init ESCC driver\n"));
		return 0; /*-1;*/
	}
	if ((chan = atoi(argv[1])) > Esccinfo.maxchan){
		_printf(_("ESCC channel %d out of range\n"),chan);
		return 0; /*-1;*/
	}
	escc = Esccchan[chan];
	if (escc == NULL)
		{
		escc = malloc(sizeof(struct escc_s));
		if (escc == NULL)
			{
			return NULL;
			}
		memset(escc, 0, sizeof(struct escc_s));
		Esccchan[chan] = escc;
		}
	else if (escc->async.iface)
		{
		_printf(_("ESCC channel %d already attached\n"), chan);
		return 0; /*-1;*/
		}

	/* create interface structure and fill in details */
	ifp = (struct iface_s *)callocw(1,sizeof(struct iface_s));
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	ifp->outq.rp = ifp->outq.buf;
	ifp->outq.wp = ifp->outq.buf;
#endif
	ifp->name = mallocw(strlen(argv[3]) + 1);
	strcpy(ifp->name,argv[3]);

	ifp->mtu = atoi(argv[4]);
	ifp->dev = chan;
	ifp->stop = escc_stop;

	escc->async.iface = ifp;
	escc->async.kick = escc_kick;

	escc->ctrl = Esccinfo.iobase + (chan >> 1) * Esccinfo.space +
			Esccinfo.off[chan & 1];
	escc->data = escc->ctrl + Esccinfo.doff;

#if 0
	if (isupper(argv[5][0]))
		argv[5][0] = tolower(argv[5][0]);

	switch (argv[5][0]) {
	case 'd':				/* fulldup divider installed? */
		escc->fulldup = 1;		/* set appropriate flag */
		argv[5]++;			/* skip the 'd' */
		break;

	case 'e':				/* external clocking? */
		escc->extclock = 1;		/* set the flag */
		break;
 	}
#endif

	escc->async.bufsize = atoi(argv[6]);
	ifp->addr = Ip_addr;

	/* Convert flags arg to lower */
	if (argc > 7)
	{
		unsigned char i = 0;
		while (argv[7][i] != '\0')
		{
			argv[7][i] = tolower(argv[7][i]);
			i++;
		}
	}

	if (strchr(argv[7],'c') != NULL)
		escc->cts = 1;
	if (strchr(argv[7],'r') != NULL)
		escc->rlsd = 1;

	switch(argv[2][0]){ 			/* mode already checked above */
#ifdef AX25
	case 'a':	/* AX.25 */
		escc_sdlc(escc);				/* init ESCC in SDLC mode */
		brgrate = escc_speed(escc,32,atol(argv[5]));/* init ESCC speed */
		escc->speed = Esccinfo.clk / (64L * (brgrate + 2));/* calc real speed */

		setencap(ifp,"AX25UI");
		escc_call(ifp,argc > 8 ? argv[8] : (char *) 0);	/* set the callsign */

		ifp->ioctl = escc_sioctl;
		ifp->raw = escc_raw;

		/* default KISS Params */
		escc->a.txdelay = 36*TPS/100;	/* 360 ms */
		escc->a.persist = 25;		/* 10% persistence */
		escc->a.slottime = 16*TPS/100; /* 160 ms */
#if TPS > 67
		escc->a.tailtime = 3*TPS/100;	/* 30 ms */
#else
		escc->a.tailtime = 2;		/* minimal reasonable value */
#endif
		escc->a.fulldup = 0;		/* CSMA */
		escc->a.waittime = 50*TPS/100; /* 500 ms */
		escc->a.maxkeyup = 7;		/* 7 s */
		escc->a.mintime = 3;		/* 3 s */
		escc->a.idletime = 120;	/* 120 s */
		break;
	case 'k':	/* kiss */
		escc_async(escc);				/* init ESCC in async mode */
		brgrate = escc_speed(escc,16,atol(argv[5]));
		escc->speed = Esccinfo.clk / (32L * (brgrate + 2));

		setencap(ifp,"AX25UI");
		escc_call(ifp,argc > 8 ? argv[8] : (char *) 0);	/* set the callsign */

		ifp->ioctl = kiss_ioctl;
		ifp->raw = kiss_raw;

		for (xdev = 0;xdev < SLIP_MAX;xdev++){
			if (Slip[xdev].iface == NULL)
				break;
		}
		ifp->xdev = xdev;
		Slip[xdev].iface = ifp;
		Slip[xdev].type = CL_KISS;
#if 0
		Slip[xdev].recv = async_recv;
		Slip[xdev].send = async_send;
#endif
		cp = if_name(ifp,_(" rx"));
		ifp->rxproc = process_thread_create(driver_process_p,
				cp, 256, slip_rx, xdev, NULL, NULL, 0);
		free(cp);
		break;
#endif

#ifdef SLIP
	case 's':	/* slip */
		escc_async(escc);		/* init ESCC in async mode */
		escc->async.trigchar = FR_END;

		brgrate = escc_speed(escc,16,atol(argv[5]));
		escc->speed = Esccinfo.clk / (32L * (brgrate + 2));
		setencap(ifp,"SLIP");
		ifp->ioctl = escc_aioctl;

		for (xdev = 0;xdev < SLIP_MAX;xdev++){
			if (Slip[xdev].iface == ifp ||
			    Slip[xdev].iface == NULL)
				break;
		}

		Slip[xdev].type = CL_SERIAL_LINE;
#if 0
		Slip[xdev].recv = async_recv;
		Slip[xdev].send = async_send;
#endif

#ifdef VJCOMPRESS
		if (strchr(argv[7],'v') != NULL) {
			Slip[xdev].escaped |= SLIP_VJCOMPR;
			Slip[xdev].slcomp = slhc_init(16,16);
		}
#else
		Slip[xdev].slcomp = NULL;
#endif

		if (slip_init(ifp) != 0)
			{
			_printf(_("%s: mode %s Init failed\n"),
					ifp->name,argv[3]);
			if_detach(ifp);
			return 0; /*-1;*/
			}
		break;
#endif

#ifdef NRS
	case 'n':	/* nrs */
		escc_async(escc);				/* init ESCC in async mode */
		brgrate = escc_speed(escc,16,atol(argv[5]));
		escc->speed = Esccinfo.clk / (32L * (brgrate + 2));

		setencap(ifp,"AX25UI");
		escc_call(ifp,argc > 8 ? argv[8] : (char *) 0);	/* set the callsign */

		ifp->ioctl = escc_aioctl;
		ifp->raw = nrs_raw;

		for (xdev = 0;xdev < NRS_MAX;xdev++)
			if (Nrs[xdev].iface == NULL)
				break;

		ifp->xdev = xdev;
		Nrs[xdev].iface = ifp;
#if 0
		Nrs[xdev].send = async_send;
		Nrs[xdev].getc = async_getc;
#endif
		cp = if_name(ifp,_(" rx"));
		ifp->rxproc = process_thread_create(driver_process_p,
				cp, 256, nrs_recv, xdev, NULL, NULL, 0);
		free(cp);
		break;
#endif

#ifdef PPP
	case 'p':	/* ppp */
		escc_async(escc);		/* init ESCC in async mode */
		escc->async.trigchar = HDLC_FLAG;

		brgrate = escc_speed(escc,16,atol(argv[5]));
		escc->speed = Esccinfo.clk / (32L * (brgrate + 2));
		setencap(ifp,"PPP");
		ifp->ioctl = escc_aioctl;
		ifp->xdev = 0;
		if (ppp_init(ifp) != 0)
			{
			_printf(_("%s: mode %s Init failed\n"),
					ifp->name,argv[3]);
			if_detach(ifp);
			return 0; /*-1;*/
			}
		break;
#endif
	}

	ifp->next = Ifaces;			/* link interface in list */
	Ifaces = ifp;

	cp = if_name(ifp,_(" tx"));
	ifp->txproc = process_thread_create(driver_process_p, cp, 256 /*512*/,
			if_tx, 0, ifp, NULL, 0);
	free(cp);

	return 0;
}
#endif

/* ESCC driver initialisation. called on "attach escc <num> init ..." */
#ifdef MODULE_escc_init
STATIC int
escc_init(nchips,iobase,space,aoff,boff,doff,intack,ivec,clk,pclk,hwtype,hwparam)
int nchips;			/* number of chips */
unsigned int iobase;		/* base of first chip */
int space,aoff,boff,doff;
unsigned int intack;		/* INTACK ioport or 0 for no INTACK */
int ivec;			/* interrupt vector number */
long clk;			/* clock frequency */
int pclk;			/* PCLK or RTxC for clock */
int hwtype;			/* selection of special hardware types */
int hwparam;			/* extra parameter for special hardware */
{
	int chip,chan;
	unsigned int chipbase;
	register unsigned int ctrl;
	int d;
	int dum = 1;
	int i_state;

	if (Esccinfo.init){
		_printf(_("ESCC driver already initialized - nothing done\n"));
		return 1;
	}
	Esccinfo.init = 1;
	Esccinfo.nchips = nchips;
	Esccinfo.maxchan = (2 * nchips) - 1;
	Esccinfo.iobase = iobase;
	Esccinfo.space = space;
	Esccinfo.off[0] = aoff;
	Esccinfo.off[1] = boff;
	Esccinfo.doff = doff;
	Esccinfo.ivec = ivec;
	Esccinfo.clk = clk;
	Esccinfo.pclk = pclk;
	Esccinfo.hwtype = hwtype;
	Esccinfo.hwparam = hwparam;

	for (chip = 0; chip < nchips; chip++)
	{
		chipbase = iobase + chip * space;

		i_state = dirps();

		for (chan = 0; chan < 2; chan++)
		{
			int d, dum = 1;

			ctrl = chipbase + Esccinfo.off[chan];

#if 0			/* store ctrl addr for polling */
			Esccpolltab[chip][chan] = ctrl;
#endif

			/*
			 * Reset the corresponding ESCC channel
			 * NOTE: cannot use the "wr()" function
			 * because the "escc" structs are not available
			 * yet
			 */

			/* MIE off, status lo, no vector */
			write_escc(ctrl, R9, NV);

			/* Channel reset A|B, status lo, no vector */
			write_escc(ctrl, R9, (chan?CHRB:CHRA)|NV);

			/* MIE on, status lo, no vector */
			write_escc(ctrl, R9, MIE|NV);
		}

		restore(i_state);
	}

#if 0
	Esccpolltab[chip][0] = 0;	/* terminate the polling table */
	Esccmaxvec = 16 * nchips;	/* upper limit on valid vector */
#endif

	/* save original interrupt vector */
	Origvec = getirq(ivec);

	/* set interrupt vector to polling routine */
	setirq(ivec,escc_vec);

	/* enable the interrupt  */
	maskon(ivec);
	return 0;
}
#endif

/* initialize an ESCC channel in asynchronous mode */
#ifdef MODULE_escc_async
STATIC int
escc_async(escc)
register struct escc_s *escc;
{
	int i_state;

	escc->async.buf = malloc(escc->async.bufsize);
	if (escc->async.buf == NULL){
		_printf(_("escc%d: No space for rx buffer\n"),
				escc->async.iface->dev);
		return -1;
	}
	escc->async.wp = escc->async.rp = escc->async.buf;
	escc->async.cnt = 0;
	escc->async.overrun = 0;
	escc->async.trigchar = -1;

	i_state = dirps();

#if 0
	escc->int_transmit = escc_asytx;	/* set interrupt handlers */
	escc->int_extstat = escc_aex;
	escc->int_receive = escc_asyrx;
	escc->int_special = escc_asp;
#endif

	/*
	 * We assume here that the ESCC register copies (see
	 * definition of "wr" macro) have been init to 0 when
	 * the "escc" struct was created !
	 */

	/* MIE off, status lo, no vector */
	wr(escc, R9, NV);

	/* DTR on, 8 data, TX enable, RTS on */
	wr(escc, R5, DTR|Tx8|TxENAB|RTS);

	/* Set up interrupt vector */
	wr(escc, R2, 0);

	/* *16 with 1 stop bit, no parity */
	wr(escc, R4, X16CLK|SB1);

	/* 8 data, auto disable, RX enable */
	wr(escc, R3, Rx8|RxENABLE);

	/* Modem status interrupts enabled */
	wr(escc, R15, CTSIE|DCDIE);

	/* Disable baud rate generator */
	wr(escc, R14, 0);

	/* Set default speed: divisor 46 indicates 9600 bps */
	wr(escc, R12, 46);

	/* Upper byte of BRG */
	wr(escc, R13, 0);

	/* Set PCLK input */
	wr(escc, R14, BRSRC);

	/* Enable baud rate generator */
	or(escc, R14, BRENABL);

	/* Baud rate generator for Rx & Tx */
	wr(escc, R11, RCBR|TCBR|TRxCBR);

	/* Reset ext/status interrupts */
	outportb(escc->ctrl,RES_EXT_INT);

	escc->status = inportb(escc->ctrl);	/* read initial status */

	/* RX int on all, TX int, ext int */
	wr(escc, R1, INT_ALL_Rx|EXT_INT_ENAB);	/* TxINT_ENAB| */

	/* MIE on */
	or(escc, R9, MIE);

	restore(i_state);

	return 0;
}
#endif

/* initialize an ESCC channel in SDLC mode */
#if 0 /*def MODULE_escc_sdlc*/
STATIC void
escc_sdlc(escc)
register struct escc_s *escc;
{
	int i_state;

	i_state = dirps();

	escc->int_transmit = escc_sdlctx;	/* set interrupt handlers */
	escc->int_extstat = escc_sdlcex;
	escc->int_receive = escc_sdlcrx;
	escc->int_special = escc_sdlcsp;

	wr(escc,R4,X1CLK|SDLC);		/* *1 clock, SDLC mode */
	wr(escc,R1,0);			/* no W/REQ operation */
	wr(escc,R3,Rx8|RxCRC_ENAB);	/* RX 8 bits/char, CRC, disabled */
	wr(escc,R5,Tx8|DTR|TxCRC_ENAB);	/* TX 8 bits/char, disabled, DTR */
	wr(escc,R6,0);			/* SDLC address zero (not used) */
	wr(escc,R7,FLAG);		/* SDLC flag value */
	wr(escc,R9,VIS);			/* vector includes status */
	wr(escc,R10,CRCPS|NRZI|ABUNDER);	/* CRC preset 1, select NRZI, ABORT on underrun */

	if (escc->extclock){			/* when using external clocks */
		/* RXclk RTxC, TXclk TRxC. */
		wr(escc,R11,RCRTxCP|TCTRxCP);
		wr(escc,R14,0);			/* No BRG options */
		write_escc(escc->ctrl,R14,DISDPLL|escc->wreg[R14]); /* No DPLL operation */
	} else {
		if (escc->fulldup){		/* when external clock divider */
			if (Esccinfo.pclk){	/* when using PCLK as clock source */
				/* RXclk DPLL, TXclk RTxC, out=BRG.	 external /32 TRxC->RTxC */
				wr(escc,R11,RCDPLL|TCRTxCP|TRxCOI|TRxCBR);
			} else {
				/* RXclk DPLL, TXclk TRxC.	external TX clock to TRxC */
				wr(escc,R11,RCDPLL|TCTRxCP);
			}
		} else {		/* only half-duplex operation */
			/* RXclk DPLL, TXclk BRG. BRG reprogrammed at every TX/RX switch */
#ifdef	notdef	/* KA9Q - for PSK modem */
			wr(escc,R11,RCDPLL|TCBR);
#else
			/* DPLL -> Rx clk, DPLL -> Tx CLK, TxCLK -> TRxC pin */
			wr(escc,R11,RCDPLL|TCDPLL|TRxCOI|TRxCDP);
#endif
		}
		wr(escc,R14,Esccinfo.pclk? BRSRC:0);	/* BRG source = PCLK/RTxC */
		write_escc(escc->ctrl,R14,SSBR|escc->wreg[R14]); /* DPLL source = BRG */
		write_escc(escc->ctrl,R14,SNRZI|escc->wreg[R14]); /* DPLL NRZI mode */
	}
	wr(escc,R15,BRKIE|CTSIE|DCDIE);	/* enable ABORT, CTS & DCD interrupts */

	if (inportb(escc->ctrl) & DCD){	/* DCD is now ON */
		if (!escc->extclock)
			write_escc(escc->ctrl,R14,SEARCH|escc->wreg[R14]); /* DPLL: enter search mode */
		or(escc,R3,ENT_HM|RxENABLE);	/* enable the receiver, hunt mode */
	}
	outportb(escc->ctrl,RES_EXT_INT);	/* reset ext/status interrupts */
	outportb(escc->ctrl,RES_EXT_INT);	/* must be done twice */
	escc->status = inportb(escc->ctrl);	/* read initial status */

	or(escc,R1,INT_ALL_Rx|TxINT_ENAB|EXT_INT_ENAB); /* enable interrupts */
	or(escc,R9,MIE);			/* master interrupt enable */

	restore(i_state);
}
#endif

/* set ESCC channel speed
 * clkmode specifies the division rate (1,16,32) inside the ESCC
 * returns the selected brgrate for "real speed" calculation
 */
#ifdef MODULE_escc_speed
unsigned int
escc_speed(escc,clkmode,speed)
register struct escc_s *escc;
unsigned int clkmode;
unsigned long speed;				/* the desired baudrate */
{
	unsigned int brgrate;
	long spdclkm;
	int i_state;

	/* calculate baudrate generator value */
	if ((spdclkm = speed * clkmode) == 0)
		return 65000U;			/* avoid divide-by-zero */

	brgrate = (unsigned) ((Esccinfo.clk + spdclkm) / (spdclkm * 2)) - 2;

	i_state = dirps();		/* 2-step register accesses... */

	cl(escc,R14,BRENABL);		/* disable baudrate generator */
	wr(escc,R12,brgrate);		/* brg rate LOW */
	wr(escc,R13,brgrate >> 8);	/* brg rate HIGH */
	or(escc,R14,BRENABL);		/* enable baudrate generator */

	restore(i_state);
	return brgrate;
}
#endif

/* de-activate ESCC channel */
#ifdef MODULE_escc_stop
STATIC int
escc_stop(struct escc_s *escc)
	{
	struct iface_s *ifp;
#if 0 /* should be here? copied from asci.c */
	struct asydialer *dialer;
#endif
	int i_state;

	ifp = escc->async.iface;
	if (ifp == NULL)
		{
		return -1;
		}
	escc->async.iface = NULL;

#if 0 /* should be here? copied from asci.c */
	if (ifp->dstate != NULL)
		{
		dialer = (struct asydialer *)ifp->dstate;
		stop_timer(&dialer->idle);	/* Stop the idle timer, if running */
		free(dialer);
		ifp->dstate = NULL;
		}
#endif

	i_state = dirps();
	inportb(escc->ctrl);		/* make sure pointer is written */
	wr(escc,R9,(ifp->dev & 1) ? CHRB : CHRA); /* reset the channel */
	restore(i_state);

	escc->async.bufsize = 0; /* just to make sure */
	escc->async.cnt = 0; /* so bufsize - cnt == 0 */
	free(escc->async.buf);

	free_p(&escc->async.sndq); /* just to make sure */
	return 0;
	}
#endif

/* de-activate ESCC driver on program exit */
#ifdef MODULE_escc_exit
void
escc_exit(void)
{
	if (Esccinfo.init){			/* was it initialized? */
		maskoff(Esccinfo.ivec);		/* disable the interrupt */
		setirq(Esccinfo.ivec,Origvec);	/* restore original interrupt vector */
	}
}
#endif

/* perform ioctl on ESCC (async) channel
 * this is used for SLIP mode only, and will read/set the line speed
 */
#ifdef MODULE_escc_aioctl
unsigned long
escc_aioctl(struct escc_s *escc, int cmd, int set, unsigned long val)
	{
	unsigned int brgrate;

	switch(cmd)
		{
	case PARAM_SPEED:
		if (set)
			{
			brgrate = escc_speed(escc, 16, val);
			escc->speed = Esccinfo.clk / (32L * (brgrate + 2));
			}
		return escc->speed;
		}
	return 0;
	}
#endif

/* perform ioctl on ESCC (sdlc) channel
 * this is used for AX.25 mode only, and will set the "kiss" parameters
 */
#if 0 /*def MODULE_escc_sioctl*/
unsigned long
escc_sioctl(struct escc_s *escc, int cmd, int set, unsigned long val)
	{
	unsigned int brgrate;
	int i_state;

	switch(cmd)
		{
	case PARAM_SPEED:
		if (set)
			{
			if (val == 0)
				{
				escc->extclock = 1;
				}
			else
				{
				brgrate = escc_speed(escc,16,val);
				escc->speed = Esccinfo.clk /
						(64L * (brgrate + 2));
				}
			}
		return escc->speed;
	case PARAM_TXDELAY:
		if (set)
			{
			escc->a.txdelay = val;
			}
		return escc->a.txdelay;
	case PARAM_PERSIST:
		if (set)
			{
			escc->a.persist = val;
			}
		return escc->a.persist;
	case PARAM_SLOTTIME:
		if (set)
			{
			escc->a.slottime = val;
			}
		return escc->a.slottime;
	case PARAM_TXTAIL:
		if (set)
			{
			escc->a.tailtime = val;
			}
		return escc->a.tailtime;
	case PARAM_FULLDUP:
		if (set)
			{
			escc->a.fulldup = val;
			}
		return escc->a.fulldup;
	case PARAM_WAIT:
		if (set)
			{
			escc->a.waittime = val;
			}
		return escc->a.waittime;
	case PARAM_MAXKEY:
		if (set)
			{
			escc->a.maxkeyup = val;
			}
		return escc->a.maxkeyup;
	case PARAM_MIN:
		if (set)
			{
			escc->a.mintime = val;
			}
		return escc->a.mintime;
	case PARAM_IDLE:
		if (set)
			{
			escc->a.idletime = val;
			}
		return escc->a.idletime;
	case PARAM_DTR:
		if (set)
			{
			if (val)
				{
				escc->wreg[R5] |= DTR;
				}
			else
				{
				escc->wreg[R5] &= ~DTR;
				}

			i_state = dirps();
			if (escc->a.tstate == IDLE && escc->timercount == 0)
				{
				escc->timercount = 1;	/* force an update */
				}
			restore(i_state);
			}
		return (escc->wreg[R5] & DTR) ? 1 : 0;
	case PARAM_GROUP:
		if (set)
			{
			escc->group = val;
			}
		return escc->group;
		}
	return -1;
	}
#endif

/* Start ESCC transmitter when it is idle (asynch mode) */
#ifdef MODULE_escc_kick
STATIC void
escc_kick(struct async_s *async_p)
	{
	unsigned int ch;

	if (((struct escc_s *)async_p)->cts &&
			!(((struct escc_s *)async_p)->status & CTS))
		{
		return;
		}

	or((struct escc_s *)async_p, R1, TxINT_ENAB);
	if (inportb(((struct escc_s *)async_p)->ctrl) & Tx_BUF_EMP)
		{
		ch = async_tx(async_p);
		if (ch == -1)
			{
			cl((struct escc_s *)async_p, R1, TxINT_ENAB);
			}
		else
			{
			outportb(((struct escc_s *)async_p)->data, ch);
			}
		}
	}
#endif

/* show ESCC status */
#ifdef MODULE_doesccstat
int
doesccstat(int argc, char *argv[], void *p)
	{
	struct escc_s *escc;
	int i;

	if (!Esccinfo.init)
		{
		_printf(_("ESCC driver not initialized\n"));
		return 0;
		}

#if 1 /* Nick */
	_printf(_("Ch Iface    SwOver   HwOver   RxInts   TxInts   ExInts   SpInts\n"));
#else
	_printf(_("Ch Iface    Sent   Rcvd   Error Space Overr   Rxints   Txints   Exints   Spints\n"));
#endif
	for (i = 0; i <= Esccinfo.maxchan; i++)
		{
		escc = Esccchan[i];
		if (escc == NULL)
			{
			continue;
			}

#if 1 /* Nick */
		_printf(_("%2d %-6s %8lu %8lu %8lu %8lu %8lu %8lu\n"),
				i,escc->async.iface->name,escc->async.overrun,
				escc->rovers,escc->rxints,escc->txints,
				escc->exints,escc->spints);
#else
		if (escc->int_receive == escc_asyrx)
			{
			_printf(_("%2d %-6s  ** asynch ** %7lu %5lu %5lu %8lu %8lu %8lu %8lu\n"),
			    i,escc->async.iface->name,
			    escc->rxerrs,escc->async.overrun,escc->rovers,
			    escc->rxints,escc->txints,escc->exints,escc->spints);
			}
		else
			{
			_printf(_("%2d %-6s %6lu %6lu %7lu %5lu %5lu %8lu %8lu %8lu %8lu\n"),
			    i,escc->async.iface->name,
			    escc->enqueued,escc->rxframes,escc->rxerrs,escc->async.overrun,escc->rovers,
			    escc->rxints,escc->txints,escc->exints,escc->spints);
			}
#endif
		}

	_printf(_("\n"));
	_printf(_("Ch iface  CTS-flowctrl  DCD-linectrl  RTS CTS DCD\n"));
	for (i = 0; i <= Esccinfo.maxchan; i++)
		{
		escc = Esccchan[i];
		if (escc == NULL)
			{
			continue;
			}

		_printf(_("%2d %-6s %12s  %12s   %s   %s   %s\n"),
			        i, escc->async.iface->name,
				(escc->cts  ? "on" : "off"),
				(escc->rlsd ? "on" : "off"),
				(escc->wreg[R5] & RTS ? "x" : "-"),
				(escc->status & CTS   ? "x" : "-"),
				(escc->status & DCD   ? "x" : "-"));
		}

	return 0;
	}
#endif

/* send raw frame to ESCC. used for AX.25 */
#if 0 /*def MODULE_escc_raw*/
STATIC int
escc_raw(
struct iface_s *ifp,
struct mbuf_s **bpp
){
	struct escc_s *escc;
	int i_state;

	dump(ifp,IF_TRACE_OUT,*bpp);
	ifp->rawsndcnt++;
	ifp->lastsent = secclock();

	escc = Esccchan[ifp->dev];

	if (escc->tx_inhibit){		/* transmitter inhibit */
		free_p(bpp);
		return -1;
	}

	enqueue(&escc->async.sndq,bpp);		/* enqueue packet */
	escc->enqueued++;

	i_state = dirps();

	if (escc->a.tstate == IDLE){	/* when transmitter is idle */
		escc->a.tstate = DEFER;	/* start the key-up sequence */
		escc->a.maxdefer = TPS * escc->a.idletime /
			escc->a.slottime;
		escc->timercount = escc->a.waittime;
	}
	restore(i_state);
	return 0;
}
#endif

#if 0 /*def MODULE_escc_call, formerly def AX25*/
/* initialize interface for AX.25 use */
STATIC int
escc_call(ifp,call)
register struct iface_s *ifp;
char *call;
{
	uint8 out[AXALEN];

	ifp->hwaddr = mallocw(AXALEN);
	if (setcall(out,call) == 0)
		memcpy(ifp->hwaddr,out,AXALEN);
	else
		memcpy(ifp->hwaddr,Mycall,AXALEN);
	return 0;
}
#endif

/* Interrupt handlers for asynchronous modes (kiss, slip, PPP) */
#ifdef MODULE_escc_int
void INTERRUPT escc_int(struct escc_s **esccp)
	{
	struct escc_s *escc;
	unsigned int ch;

	escc = esccp[1];
	switch (read_escc(escc->ctrl, R2)) /* read RR2 in channel B */
		{
	case 0:
		escc->txints++;
		ch = async_tx(&escc->asy);
		if (ch == -1)
			{
			cl(escc, R1, TxINT_ENAB);
			}
		else
			{
			outportb(escc->data, ch);
			}
		break;

	case 2:
		escc->exints++;
		escc_aex(escc, inportb(escc->ctrl));
		outportb(escc->ctrl, RES_EXT_INT);
		break;

	case 4:
		escc->rxints++;
		async_rx(&escc->async, inportb(escc->data));
		break;

	case 6:
		escc->spints++;
		escc_asp(escc, read_escc(escc->ctrl, R1));
		outportb(escc->ctrl, ERR_RES);
		break;

	case 8:
		escc = esccp[0];
		escc->txints++;
		ch = async_tx(&escc->asy);
		if (ch == -1)
			{
			cl(escc, R1, TxINT_ENAB);
			}
		else
			{
			outportb(escc->data, ch);
			}
		break;

	case 0xa:
		escc = esccp[0];
		escc->exints++;
		escc_aex(escc, inportb(escc->ctrl));
		outportb(escc->ctrl, RES_EXT_INT);
		break;

	case 0xc:
		escc = esccp[0];
		escc->rxints++;
		async_rx(&escc->async, inportb(escc->data));
		break;

	case 0xe:
		escc = esccp[0];
		escc->spints++;
		escc_asp(escc, read_escc(escc->ctrl, R1));
		outportb(escc->ctrl, ERR_RES);
		break;
		}
	}
#endif

/* External/Status interrupt handler */
#ifdef MODULE_escc_aex
STATIC void
escc_aex(struct escc_s *escc, unsigned char ch)
	{
	unsigned char change;

	change = ch ^ escc->status;
	escc->status = ch;

	if (change & BRK_ABRT)			/* BREAK? */
		{
		if ((ch & BRK_ABRT) == 0)	/* BREAK now over? */
			{
			inportb(escc->data);	/* read the NUL character */
			}
		}

	if (escc->cts && (change & CTS))
		{
		if (ch & CTS)			/* cts gone high? */
			{
			escc_kick(&escc->asy);	/* yes, kick transmitter */
			}
		else
			{
			cl(escc, R1, TxINT_ENAB); /* no, stop transmitter */
			}
		}

	if (escc->rlsd && (change & DCD))
		{
		if (ch & DCD)			/* dcd gone high? */
			{
			or(escc, R3, RxENABLE);	/* yes, enable receiver */
			}
		else
			{
			cl(escc, R3, RxENABLE);	/* no, disable receiver */
			}
		escc->cdchanges++;
		ksig(&escc->rlsd, 0);
		}

        ksig(&escc->status, 0);
	}
#endif

/* Receive Special Condition interrupt handler */
#ifdef MODULE_escc_asp
STATIC void
escc_asp(struct escc_s *escc, unsigned char ch)
	{
	inportb(escc->data);			/* flush offending character */
	if (ch & (CRC_ERR | Rx_OVR))		/* framing error or overrun? */
		{
		escc->rovers++;			/* report as overrun */
		}
	}
#endif

/* Interrupt handlers for sdlc mode (AX.25) */

/* Transmitter interrupt handler */
#if 0 /*def MODULE_escc_sdlctx*/
STATIC void
escc_sdlctx(escc)
register struct escc_s *escc;
{
	register struct mbuf_s *bp;

	escc->txints++;

	switch(escc->a.tstate){		/* look at transmitter state */
	case ACTIVE:			/* busy sending data bytes */
		while ((bp = escc->tbp)->cnt == 0){	/* nothing left in this mbuf? */
			bp = bp->next;			/* save link to next */
			free_mbuf(&escc->tbp);	/*KM*/
			if ((escc->tbp = bp) == NULL){/* see if more mbufs follow */
				if (inportb(escc->ctrl) & TxEOM){	/* check tx underrun status */
					escc->rovers++;		/* oops, an underrun! count them */
					outportb(escc->ctrl,SEND_ABORT);/* send an abort to be sure */
					escc->a.tstate = TAIL;	/* key down tx after TAILTIME */
					escc->timercount = escc->a.tailtime;
					return;
				}
				cl(escc,R10,ABUNDER);		/* frame complete, allow CRC transmit */
				escc->a.tstate = FLUSH;
				outportb(escc->ctrl,RES_Tx_P);	/* reset pending int */
				return;
			}
		}
		/* now bp = escc->tbp (either from while or from if stmt above) */
		outportb(escc->data,*(bp->data++)); /* send the character */
		bp->cnt--;			/* decrease mbuf byte count */
		return;
	case FLUSH:	/* CRC just went out, more to send? */
		or(escc,R10,ABUNDER);		/* re-install underrun protection */
		/* verify that we are not exeeding max tx time (if defined) */
		if ((escc->timercount != 0 || escc->a.maxkeyup == 0) &&
		 (escc->tbp = escc->async.sndq) != NULL){ /* dequeue a frame */
			escc->async.sndq = escc->async.sndq->anext;
			outportb(escc->ctrl,RES_Tx_CRC); /* reset the TX CRC generator */
			escc->a.tstate = ACTIVE;
			escc_sdlctx(escc);		/* write 1st byte */
			outportb(escc->ctrl,RES_EOM_L); /* reset the EOM latch */
			return;
		}
		escc->a.tstate = TAIL;		/* no more, key down tx after TAILTIME */
		escc->timercount = escc->a.tailtime;
		outportb(escc->ctrl,RES_Tx_P);
		return;
	default:				/* another state */
		outportb(escc->ctrl,RES_Tx_P);	/* then don't send anything */
		return;
	}
}
#endif

/* External/Status interrupt handler */
#if 0 /*def MODULE_escc_sdlcex*/
STATIC void
escc_sdlcex(escc)
register struct escc_s *escc;
{
	register unsigned char status, change;

	escc->exints++;
	status = inportb(escc->ctrl);
	change = status ^ escc->status;

	if (change & BRK_ABRT){		/* Received an ABORT */
		if (status & BRK_ABRT){		/* is this the beginning? */
			if (escc->rbp != NULL){/* did we receive something? */
				/* check if a significant amount of data came in */
				/* this is because the drop of DCD tends to generate an ABORT */
				if (escc->rbp->next != NULL || escc->rbp->cnt > 0)
				escc->rxerrs++;	/* then count it as an error */
				escc_tossb(escc);		/* throw away buffer */
			}
/* Nick parenthesized VOID below */
			(VOID)(inportb(escc->data));	/* flush the fifo */
/* Nick parenthesized VOID below */
			(VOID)(inportb(escc->data));
/* Nick parenthesized VOID below */
			(VOID)(inportb(escc->data));
		}
	}
	if (change & CTS){			/* CTS input changed state */
		if (status & CTS){		/* CTS is now ON */
			if (escc->a.tstate == KEYWT &&
				escc->a.txdelay == 0) /* zero TXDELAY = wait for CTS */
			escc->timercount = 1;	/* it will start within 10 ms */
		}
	}
	if (change & DCD){			/* DCD input changed state */
		if (status & DCD){		/* DCD is now ON */
			if (!escc->extclock)
				write_escc(escc->ctrl,R14,SEARCH|escc->wreg[R14]); /* DPLL: enter search mode */
			or(escc,R3,ENT_HM|RxENABLE); /* enable the receiver, hunt mode */
		} else {			/* DCD is now OFF */
			cl(escc,R3,ENT_HM|RxENABLE); /* disable the receiver */
/* Nick parenthesized VOID below */
			(VOID)(inportb(escc->data));	/* flush the fifo */
/* Nick parenthesized VOID below */
			(VOID)(inportb(escc->data));
/* Nick parenthesized VOID below */
			(VOID)(inportb(escc->data));
			if (escc->rbp != NULL){/* did we receive something? */
				/* check if a significant amount of data came in */
				/* this is because some characters precede the drop of DCD */
				if (escc->rbp->next != NULL || escc->rbp->cnt > 0)
				escc->rxerrs++;	/* then count it as an error */
				escc_tossb(escc);		/* throw away buffer */
			}
		}
	}
	escc->status = status;
	outportb(escc->ctrl,RES_EXT_INT);
}
#endif

/* Receiver interrupt handler */
#if 0 /*def MODULE_escc_sdlcrx*/
STATIC void
escc_sdlcrx(escc)
register struct escc_s *escc;
{
	register struct mbuf_s *bp;

	escc->rxints++;

	if ((bp = escc->rbp1) == NULL){ /* no buffer available now */
		if (escc->rbp == NULL){
			if ((bp = alloc_mbuf(escc->async.bufsize+sizeof(struct iface_s *))) != NULL){
				escc->rbp = escc->rbp1 = bp;
				bp->cnt = 0;
			}
		} else if ((bp = alloc_mbuf(escc->async.bufsize)) != NULL){
			escc->rbp1 = bp;
			for (bp = escc->rbp; bp->next != NULL; bp = bp->next)
				;
			bp->next = escc->rbp1;
			bp = escc->rbp1;
		}
		if (bp == NULL){
/* Nick parenthesized VOID below */
			(VOID)(inportb(escc->data));	/* so we have to discard the char */
			or(escc,R3,ENT_HM);		/* enter hunt mode for next flag */
			escc_tossb(escc);		/* put buffers back on pool */
			escc->async.overrun++;	/* count these events */
			return;
		}
	}

	/* now, we have a buffer (at bp). read character and store it */
	bp->data[bp->cnt++] = inportb(escc->data);

	if (bp->cnt == bp->size)		/* buffer full? */
		escc->rbp1 = NULL;	/* acquire a new one next time */
}
#endif

/* Receive Special Condition interrupt handler */
#if 0 /*def MODULE_escc_sdlcsp*/
STATIC void
escc_sdlcsp(escc)
register struct escc_s *escc;
{
	register unsigned char status;
	register struct mbuf_s *bp;

	escc->spints++;

	status = read_escc(escc->ctrl,R1);	/* read receiver status */
/* Nick parenthesized VOID below */
	(VOID)(inportb(escc->data));		/* flush offending character */

	if (status & Rx_OVR){		/* receiver overrun */
		escc->rovers++;			/* count them */
		or(escc,R3,ENT_HM);		/* enter hunt mode for next flag */
		escc_tossb(escc);			/* rewind the buffer and toss */
	}
	if (status & END_FR &&		/* end of frame */
	escc->rbp != NULL){	/* at least received something */
		if ((status & CRC_ERR) == 0 &&	/* no CRC error is indicated */
		(status & 0xe) == RES8 &&	/* 8 bits in last byte */
		escc->rbp->cnt > 0){

			/* we seem to have a good frame. but the last byte received */
			/* from rx interrupt is in fact a CRC byte, so discard it */
			if (escc->rbp1 != NULL){
				escc->rbp1->cnt--;	/* current mbuf was not full */
			} else {
				for (bp = escc->rbp; bp->next != NULL; bp = bp->next);
					/* find last mbuf */

				bp->cnt--;		/* last byte is first CRC byte */
			}

			net_route(escc->async.iface,&escc->rbp);

			escc->rbp = escc->rbp1 = NULL;
			escc->rxframes++;
		} else {			/* a bad frame */
			escc_tossb(escc);		/* throw away frame */
			escc->rxerrs++;
		}
	}
	outportb(escc->ctrl,ERR_RES);
}
#endif

/* Throw away receive mbuf(s) when an error occurred */
#if 0 /*def MODULE_escc_tossb*/
STATIC void
escc_tossb (escc)
register struct escc_s *escc;
{
	register struct mbuf_s *bp;

	if ((bp = escc->rbp) != NULL){
		free_p(&bp->next);
		free_p(&bp->dup);	/* Should be NULL */
		bp->next = NULL;
		escc->rbp1 = bp;		/* Don't throw this one away */
		bp->cnt = 0;	/* Simply rewind it */
	}
}
#endif

/* Switch the ESCC to "transmit" mode */
/* Only to be called from an interrupt handler, while in AX.25 mode */
#if 0 /*def MODULE_escc_txon*/
STATIC void
escc_txon(escc)
register struct escc_s *escc;
{
    if (!escc->fulldup && !escc->extclock){ /* no fulldup divider? */
		cl(escc,R3,RxENABLE);		/* then switch off receiver */
		cl(escc,R5,TxENAB);		/* transmitter off during switch */
		escc_speed(escc,16,escc->speed);/* reprogram baudrate generator */
	}
	or(escc,R5,RTS|TxENAB);		/* set the RTS line and enable TX */
	if (Esccinfo.hwtype & HWPRIMUS)	/* PRIMUS has another PTT bit... */
		outportb(escc->ctrl + 4,Esccinfo.hwparam | 0x80); /* set that bit! */
}
#endif

/* Switch the ESCC to "receive" mode (or: switch off transmitter)
 * Only to be called from an interrupt handler, while in AX.25 mode
 */
#if 0 /*def MODULE_escc_txoff*/
STATIC void
escc_txoff(escc)
register struct escc_s *escc;
{
	cl(escc,R5,RTS);			/* turn off RTS line */
	if (Esccinfo.hwtype & HWPRIMUS)	/* PRIMUS has another PTT bit... */
		outportb(escc->ctrl + 4,Esccinfo.hwparam); /* clear that bit! */

    if (!escc->fulldup && !escc->extclock){ /* no fulldup divider? */
		cl(escc,R5,TxENAB);		/* then disable the transmitter */
		escc_speed(escc,16,escc->speed);	/* back to receiver baudrate */
	}
}
#endif

/* ESCC timer interrupt handler. Will be called every 1/TPS s by the
 * routine systick in pc.c
 */
#if 0 /*def MODULE_escc_timer*/
void
escc_timer(void)
{
	register struct escc_s *escc;
	register struct escc_s **esccp;
	int i_state;

	i_state = dirps();
	for (esccp = Esccchan + Esccinfo.maxchan; esccp >= Esccchan; esccp--){
		if ((escc = *esccp) != NULL &&
		  escc->timercount != 0 &&
		  --(escc->timercount) == 0){
			/* handle an ESCC timer event for this ESCC channel
			 * this can only happen when the channel is AX.25 type
			 * (the SLIP/KISS driver does not use timers)
			 */
			switch(escc->a.tstate){
			case IDLE:			/* it was idle, this is FULLDUP2 timeout */
				escc_txoff(escc);		/* switch-off the transmitter */
				break;
			case DEFER:			/* trying to get the channel */
				/* operation is as follows:
				 * CSMA: when channel clear AND persistence randomgenerator
				 *	 wins, AND group restrictions allow it:
				 *		keyup the transmitter
				 *	 if not, delay one SLOTTIME and try again
				 * FULL: always keyup the transmitter
				 */
				if (escc->a.fulldup == 0){
					Random = 21 * Random + 53;
					if (escc->status & DCD || escc->a.persist < Random){
						/* defer transmission again. check for limit */
defer_it:					if (--(escc->a.maxdefer) == 0){
							/* deferred too long. choice is to:
							 * - throw away pending frames, or
							 * - smash-on the transmitter and send them.
							 * the first would be the choice in a clean
							 * environment, but in the amateur radio world
							 * a distant faulty station could tie us up
							 * forever, so the second may be better...
							*/
#ifdef THROW_AWAY_AFTER_DEFER_TIMEOUT
							struct mbuf_s *bp,*bp1;

							while ((bp = escc->async.sndq) != NULL){
								escc->async.sndq = escc->async.sndq->anext;
								free_p(&bp);
							}
#else
							goto keyup; /* just keyup the transmitter... */
#endif
						}
						escc->timercount = escc->a.slottime;
						break;
					}
					if (escc->group != NOGROUP){
						int i;
						struct escc_s *escc2;

						for (i = 0; i <= Esccinfo.maxchan; i++)
							if ((escc2 = Esccchan[i]) != NULL &&
							 escc2 != escc &&
							 escc2->group & escc->group &&
							 ((escc->group & TXGROUP && escc2->wreg[R5] & RTS) ||
							 (escc->group & RXGROUP && escc2->status & DCD))){
								goto defer_it;
							}
					}
				}
			case KEYUP:			/* keyup transmitter (note fallthrough) */
keyup:				if ((escc->wreg[R5] & RTS) == 0){ /* when not yet keyed */
					escc->a.tstate = KEYWT;
					escc->timercount = escc->a.txdelay; /* 0 if CTSwait */
					escc_txon(escc);
					break;
				}
				/* when already keyed, directly fall through */
			case KEYWT:			/* waited for CTS or TXDELAY */
				/* when a frame is available (it should be...):
				 * - dequeue it from the send queue
				 * - reset the transmitter CRC generator
				 * - set a timeout on transmission length, if defined
				 * - send the first byte of the frame
				 * - reset the EOM latch
				 * when no frame available, proceed to TAIL handling
				 */
				if ((escc->tbp = escc->async.sndq) != NULL){
					escc->async.sndq = escc->async.sndq->anext;
					outportb(escc->ctrl,RES_Tx_CRC);
					escc->a.tstate = ACTIVE;
					escc->timercount = TPS * escc->a.maxkeyup;
					escc_sdlctx(escc);
					outportb(escc->ctrl,RES_EOM_L);
					break;
				}
				/* when no frame queued, fall through to TAIL case */
			case TAIL:			/* at end of frame */
				/* when fulldup is 0 or 1, switch off the transmitter.
				 * when frames are still queued (because of transmit time limit),
				 * restart the procedure to get the channel after MINTIME.
				 * when fulldup is 2, the transmitter remains keyed and we
				 * continue sending.	IDLETIME is an idle timeout in this case.
				 */
				if (escc->a.fulldup < 2){
					escc->a.tstate = IDLE;
					escc_txoff(escc);

					if (escc->async.sndq != NULL){
						escc->a.tstate = DEFER;
						escc->a.maxdefer = TPS * escc->a.idletime /
						 escc->a.slottime;
						escc->timercount = TPS * escc->a.mintime;
					}
					break;
				}
				if (escc->async.sndq != NULL){ /* still frames on the queue? */
					escc->a.tstate = KEYWT; /* continue sending */
					escc->timercount = TPS * escc->a.mintime; /* after mintime */
				} else {
					escc->a.tstate = IDLE;
					escc->timercount = TPS * escc->a.idletime;
				}
				break;
			case ACTIVE:	/* max keyup time expired */
			case FLUSH:	/* same while in flush mode */
				break;	/* no action required yet */
			default:			/* unexpected state */
				escc->a.tstate = IDLE; /* that should not happen, but... */
				escc_txoff(escc);		/* at least stop the transmitter */
				break;
			}
		}
	}
	restore(i_state);
}
#endif

/* Wait for a signal that the RLSD modem status has changed */
#if 0 /*def MODULE_get_rlsd_escc*/
int
get_rlsd_escc(struct escc_s *escc, int new_rlsd)
	{
	int i_state;

	i_state = dirps();
	if (escc->rlsd == 0)
		{
		restore (i_state);
		return -1;
		}

	for (;;)
		{
		if (new_rlsd && (escc->status & DCD))
			{
			restore (i_state);
			return 1;
			}
		if (!new_rlsd && !(escc->status & DCD))
			{
			restore (i_state);
			return 0;
			}

		/* Wait for state change to requested value */
		/*ppause(2L);*/
		kwait(&escc->rlsd);
		}
	}
#endif

