#!/bin/sh
rm -frv lib/crt0_banked.asm lib/crt0_banked.lst lib/crt0_banked.rel
rm -frv ax25/*.i ax25/build lib/ax25.lib lib/ax25_lib
rm -frv client/*.i client/build lib/client.lib lib/client_lib
rm -frv crt/*.i crt/build lib/crt_banked.lib lib/crt_banked_lib
rm -frv driver/*.i driver/build lib/driver.lib lib/driver_lib
rm -frv dump/*.i dump/build lib/dump.lib lib/dump_lib
rm -frv filesys/*.i filesys/build lib/filesys.lib lib/filesys_lib
rm -frv iar/*.i iar/build lib/iar.lib lib/iar_lib
rm -frv internet/*.i internet/build lib/internet.lib lib/internet_lib
rm -frv intl/*.i intl/build lib/intl.lib lib/intl_lib
rm -frv kernel/*.i kernel/build lib/kernel.lib lib/kernel_lib
rm -frv main/*.i main/build bin/net.bin lib/misc.lib lib/misc_lib
rm -frv po/messages.po po/build lib/messages.lib lib/messages_lib
rm -frv net/*.i net/build lib/net.lib lib/net_lib
rm -frv netrom/*.i netrom/build lib/netrom.lib lib/netrom_lib
rm -frv ppp/*.i ppp/build lib/ppp.lib lib/ppp_lib
rm -frv server/*.i server/build lib/server.lib lib/server_lib
rm -frv socket/*.i socket/build lib/socket.lib lib/socket_lib
rm -frv sys/*.i sys/build lib/sys_banked.lib lib/sys_banked_lib
rm -frv z80/*.i z80/build lib/z80.lib lib/z80_lib
