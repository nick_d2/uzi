/* stack.c based on malloc.c 2.11bsd */

#include "kernel/dprintf.h"
#include "kernel/stack.h"
#include "asm.h" /* for KRNL_BA_START */

#define BEST_FIT

#ifndef DEBUG
#define DEBUG 0
#endif

#if DEBUG >= 4
void amess(char *ptr);
#define ASSERT(expr) if (!(expr)) { amess("expr"); while (1); }
#define ALLOCK(arena_p) stack_allock(arena_p)
#else
#define ASSERT(expr)
#define ALLOCK(arena_p)
#endif

/*
 * C storage allocator, for stack arena (4k page at 0xe000), see crt/malloc.c
 */
#define	INT		int
#define	WORD		sizeof(union store)

#define	BUSY		1

#define	testbusy(p)	((INT)(p)&BUSY)
#define	setbusy(p)	(union store *)((INT)(p)|BUSY)
#define	clearbusy(p)	(union store *)((INT)(p)&~BUSY)

#ifndef MODULE
#define MODULE_stack_get
#define MODULE_stack_set
#define MODULE_stack_allock
#define MODULE_stack_malloc
#define MODULE_stack_free
#define MODULE_stack_realloc
#endif

extern union store *__allocx;	/* for benefit of realloc */
#define __allocs ((union store *)KRNL_BA_START)
#define __alloct ((union store *)(KRNL_CA1_START - WORD))

/* note: this is not really used, see stack_get.S for version using bbr */
#ifdef MODULE_stack_get
union store *
stack_get(struct arena_s *arena_p, union store *src)
	{
	return src->ptr;
	}
#endif

/* note: this is not really used, see stack_set.S for version using bbr */
#ifdef MODULE_stack_set
void
stack_set(struct arena_s *arena_p, union store *dest, union store *value)
	{
	dest->ptr = value;
	}
#endif

#ifdef MODULE_stack_allock
void
stack_allock(struct arena_s *arena_p)
{
	union store *p;

	for (p = __allocs; clearbusy(stack_get(arena_p, p)) > p;
			p = clearbusy(stack_get(arena_p, p)))
		;
	ASSERT(p == __alloct);
}
#endif

#ifdef MODULE_stack_malloc
void *
stack_malloc(struct arena_s *arena_p, size_t nbytes)
{
	union store *p, *q;
	unsigned int nw, temp;
	union store *__allocp;
#ifdef BEST_FIT
	unsigned int best;
#endif

#if DEBUG >= 4
 dprintf(4, "\n%02x:%04x stack_malloc(0x%04x, 0x%04x) = ",
   ((unsigned char *)&arena_p)[8], ((unsigned int *)&arena_p)[5],
   arena_p, nbytes);
#endif
#if 0
	if (__allocs == NULL) {	/* first time */
		/*__allocs = (union store *)KRNL_BA_START;*/
		/*__alloct = (union store *)(KRNL_CA1_START - WORD);*/
		stack_set(arena_p, __allocs, __alloct);
		stack_set(arena_p, __alloct, setbusy(__allocs));
	}
#endif
	/* note: this may fail if nbytes is very large eg. 0xffff */
	nw = (nbytes+2*WORD-1)/WORD;
	ALLOCK(arena_p);
	p = __allocs;
#ifdef BEST_FIT
	__allocp = NULL;
#endif
	for (;;) {
		if (!testbusy(stack_get(arena_p, p))) {
			while(!testbusy(stack_get(arena_p,
					q = stack_get(arena_p, p)))) {
				ASSERT(q > p && q < __alloct);
				stack_set(arena_p, p, stack_get(arena_p, q));
			}
#if 1
			temp = q - p;
			if (temp >= nw) {
#else
			if (q >= p+nw && p+nw >= p) {
#endif
#ifdef BEST_FIT
				if (__allocp == NULL || best > temp) {
					__allocp = p;
					best = temp;
				}
#else
				goto found;
#endif
			}
		}
		q = p;
		p = clearbusy(stack_get(arena_p, p));
		if (p > q) {
			ASSERT(p <= __alloct);
		}
		else {
			ASSERT(q == __alloct && p == __allocs);
#ifdef BEST_FIT
			if (__allocp) {
				p = __allocp;
				q = stack_get(arena_p, p);
				goto found;
			}
#endif
			break;
		}
	}
#if DEBUG >= 4
 dprintf(4, "NULL ");
#endif
 abyte('@');
	return NULL;
found:
	__allocp = p + nw;
	ASSERT(__allocp <= __alloct);
	if (q > __allocp) {
		__allocx = stack_get(arena_p, __allocp);
		stack_set(arena_p, __allocp, stack_get(arena_p, p));
	}
	stack_set(arena_p, p, setbusy(__allocp));
#if DEBUG >= 4
 dprintf(4, "0x%04x ", p + 1);
#endif
	return p + 1;
}
#endif

/*
 * Freeing strategy tuned for LIFO allocation.
 */
#ifdef MODULE_stack_free
void
stack_free(struct arena_s *arena_p, void *ap)
{
	union store *p;

#if DEBUG >= 4
 dprintf(4, "\n%02x:%04x stack_free(0x%04x, 0x%04x) ",
   ((unsigned char *)&arena_p)[8], ((unsigned int *)&arena_p)[5], arena_p, ap);
#endif
	if (ap == NULL) { 	/* may be first time */
		return;
	}
	p = (union store *)ap;
#if 1 /* Nick, for kernel "memory" command */
	if (p < (__allocs + 1) || p > __alloct ||
			testbusy(stack_get(arena_p, --p)) == 0)
		{
		dprintf(0, "\n%02x:%04x stack_free(0x%04x, 0x%04x) ",
				((unsigned char *)&arena_p)[8],
				((unsigned int *)&arena_p)[5], arena_p, ap);
		return;
		}
	ALLOCK(arena_p);
#else
	ASSERT(p >= (__allocs + 1) && p <= __alloct);
	ALLOCK(arena_p);
	--p;
	ASSERT(testbusy(stack_get(arena_p, p)));
#endif
	stack_set(arena_p, p, clearbusy(stack_get(arena_p, p)));
	ASSERT(stack_get(arena_p, p) > p && stack_get(arena_p, p) <= __alloct);
}
#endif

/*
 * stack_realloc(p, nbytes) reallocates a block obtained from stack_malloc() and freed
 * since last call of stack_malloc() to have new size nbytes, and old content
 * returns new location, or 0 on failure.
 */
#ifdef MODULE_stack_realloc
void *
stack_realloc(struct arena_s *arena_p, void *ap, size_t nbytes)
{
	union store *p, *q;
#if 1
	union store *s, *t;
#endif
	unsigned int nw, onw;

#if DEBUG >= 4
 dprintf(4, "\n%02x:%04x stack_realloc(0x%04x, 0x%04x) ",
   ((unsigned char *)&arena_p)[8], ((unsigned int *)&arena_p)[5],
   arena_p, ap, nbytes);
#endif
	if (ap == NULL) {	/* may be first time */
		return stack_malloc(arena_p, nbytes);
	}
	p = (union store *)ap;
	stack_free(arena_p, p);
	onw = stack_get(arena_p, p - 1) - p;
	q = stack_malloc(arena_p, nbytes);
	if (q == NULL || q == p)
		return q; /* note: in failure case, we freed p!! */
#if 1
	s = p;
	t = q;
#endif
	nw = (nbytes+WORD-1)/WORD;
	if (nw < onw)
		onw = nw;
#if 0
	memcpy(q, p, onw * sizeof(union store));
#else
	while (onw-- != 0)
		stack_set(arena_p, t++, stack_get(arena_p, s++));
#endif
	if (q < p && q+nw >= p)
		stack_set(arena_p, q + (q + nw - p), __allocx);
	return q;
}
#endif

