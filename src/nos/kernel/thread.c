/* thread.c by Nick for NOS/UZI project
 *
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 Thread table management routines
**********************************************************/

#include <errno.h>
#include <stdlib.h>
#include "kernel/dprintf.h"
#include "kernel/thread.h"
#include <libintl.h>
#include "po/messages.h"

#ifndef DEBUG
#define DEBUG 0
#endif

#ifndef MODULE
#define MODULE_thread_list
#define MODULE_thread_new
#define MODULE_thread_delete
#define MODULE_thread_garbage
#define MODULE_ksig
#define MODULE_delproc
#define MODULE_addproc
#endif

#ifdef MODULE_thread_list
struct thread_s *thread_list[THREAD_LIST];
#endif

#ifdef MODULE_thread_new
struct thread_s *
thread_new(void)
	{
	register struct thread_s **thread_pp, *thread_p;
	/* int i_state; */

#if DEBUG >= 3
	_dprintf(3, _("thread_new() starting\n"));
#endif
	for (thread_pp = thread_list;
			thread_pp < thread_list + THREAD_LIST; thread_pp++)
		{
		/* i_state = dirps(); */
		thread_p = *thread_pp;
		if (thread_p && thread_p->flags.active == 0)
			{
			goto found;
			}
		/* restore(i_state); */
		}

	thread_p = malloc(sizeof(struct thread_s));
	if (thread_p == NULL)
		{
		goto error;
		}

	for (thread_pp = thread_list;
			thread_pp < thread_list + THREAD_LIST; thread_pp++)
		{
		/* i_state = dirps(); */
		if (*thread_pp == NULL)
			{
			*thread_pp = thread_p;
			goto found;
			}
		/* restore(i_state); */
		}

	free(thread_p);
error:
#if DEBUG >= 3
	_dprintf(3, _("thread_new() returning NULL, error\n"));
#endif
	return NULL;

found:
	memset(thread_p, 0, sizeof(struct thread_s));
	thread_p->flags.active = 1;
	thread_p->sysio = 1;
	/* restore(i_state); */
#if DEBUG >= 3
	_dprintf(3, _("thread_new() returning 0x%x, success\n"), thread_p);
#endif
	return thread_p;
	}
#endif

#ifdef MODULE_thread_delete
void
thread_delete(struct thread_s *thread_p)
	{
#if DEBUG >= 3
	_dprintf(3, _("thread_delete(0x%x) starting\n"), thread_p);
#endif
	thread_p->flags.active = 0;
	if (thread_p->process_p)
		{
		process_deref(thread_p->process_p);
		}
	if (thread_p->stack_arena_p)
		{
		stack_free(thread_p->stack_arena_p, thread_p->stack);
		arena_deref(thread_p->stack_arena_p);
		}
	free(thread_p->name);
#if DEBUG >= 3
	_dprintf(3, _("thread_delete() returning, success\n"));
#endif
	}
#endif

#ifdef MODULE_thread_garbage
/* thread_list garbage collection - called by storage allocator when free space
 * runs low.  Zaps thread table entries with 0 reference count (P_EMPTY).
 */
void
thread_garbage(int red)
	{
	register struct thread_s **thread_pp, *thread_p;

	for (thread_pp = thread_list;
			thread_pp < thread_list + THREAD_LIST; thread_pp++)
		{
		thread_p = *thread_pp;
		if (thread_p && thread_p->flags.active == 0)
			{
			*thread_pp = NULL;
			free(thread_p);
			}
		}
	}
#endif

/* Make ready the first 'n' processes waiting for a given event. The ready
 * processes will see a return value of 0 from kwait().  Note that they don't
 * actually get control until we explicitly give up the CPU ourselves through
 * a kwait(). ksig is now called from pwait, which is never called at
 * interrupt time, so it is no longer necessary to protect the proc queues
 * against interrupts. This also helps interrupt latencies considerably.
 */
#ifdef MODULE_ksig
void
ksig(
void *event,	/* Event to signal */
int n		/* Max number of processes to wake up */
)
	{
	struct thread_s *thread_p;
	struct thread_s *next_thread_p;
	unsigned int hashval;

	hashval = phash(event);
	for (thread_p = Waittab[hashval]; thread_p; thread_p = next_thread_p)
		{
		next_thread_p = thread_p->next;
		if (thread_p->event == event)
			{
			delproc(thread_p);
			thread_p->flags.waiting = 0;
			thread_p->retval = 0;
			thread_p->event = NULL;
			addproc(thread_p);
			if (n)
				{
				return;
				}
			}
		}
	}
#endif

/* Remove a process entry from the appropriate table */
#ifdef MODULE_delproc
void
delproc(struct thread_s *thread_p)	/* Pointer to entry */
	{
	if (thread_p == NULL)
		{
		return;
		}
	if (thread_p->next)
		{
		thread_p->next->prev = thread_p->prev;
		}
	if (thread_p->prev)
		{
		thread_p->prev->next = thread_p->next;
		}
	else
		{
		if (thread_p->flags.waiting)
			{
			Waittab[phash(thread_p->event)] = thread_p->next;
			}
		else
			{
			Rdytab = thread_p->next; /* Ready */
			}
		}
	}
#endif

#ifdef MODULE_addproc
void
addproc(struct thread_s *thread_p)	/* Pointer to entry */
	{
	register struct thread_s **thread_pp, *other_thread_p;

	if (thread_p == NULL)
		{
		return;
		}
	if (thread_p->flags.waiting)
		{
		thread_pp = &Waittab[phash(thread_p->event)];
		}
	else
		{
		thread_pp = &Rdytab; /* Ready */
		}
	thread_p->next = NULL;
	if (*thread_pp == NULL)
		{
		/* Empty list, stick at beginning */
		thread_p->prev = NULL;
		*thread_pp = thread_p;
		}
	else
		{
		/* Find last thread_p on list */
		for (other_thread_p = *thread_pp; other_thread_p->next;
				other_thread_p = other_thread_p->next)
			;
		other_thread_p->next = thread_p;
		thread_p->prev = other_thread_p;
		}
	}
#endif

