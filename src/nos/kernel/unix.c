/* unix.c by Nick for NOS/UZI project
 *
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

#include <errno.h>
#include <setjmp.h>
#include <signal.h>
#include "driver/rtc.h"
#include "filesys/execve.h"
#include "filesys/filesys.h"
#include "filesys/openfile.h"
#include "kernel/dprintf.h"
#include "kernel/process.h"
#include "kernel/strace.h"
#include "kernel/unix.h"
#include "kernel/userfile.h"
#include "kernel/usrmem.h"
#include "socket/opensock.h"
#include <libintl.h>
#include "po/messages.h"

#ifndef DEBUG
#define DEBUG 0
#endif

#ifndef MODULE
#define MODULE_unix_tick
#define MODULE_unix_ticks
#define MODULE___unix
#define MODULE___unix_sync
#define MODULE___unix_utime
#define MODULE___unix_link
#define MODULE___unix_symlink
#define MODULE___unix_unlink
#define MODULE___unix_chdir
#define MODULE___unix_chroot
#define MODULE___unix_mknod
#define MODULE___unix_access
#define MODULE___unix_chmod
#define MODULE___unix_chown
#define MODULE___unix_stat
#define MODULE___unix_mount
#define MODULE___unix_umount
#define MODULE___unix_execve
#define MODULE___unix_open
#define MODULE___unix_close
#define MODULE___unix_read
#define MODULE___unix_write
#define MODULE___unix_lseek
#define MODULE___unix_fstat
#define MODULE___unix_falign
#define MODULE___unix_dup
#define MODULE___unix_dup2
#define MODULE___unix_ioctl
#define MODULE___unix_pipe
#define MODULE___unix_time
#define MODULE___unix_stime
#define MODULE___unix_times
#define MODULE___unix_brk
#define MODULE___unix_sbrk
#define MODULE___unix_waitpid
#define MODULE___unix__exit
#define MODULE___unix_pause
#define MODULE___unix_signal
#define MODULE___unix_kill
#define MODULE___unix_alarm
#define MODULE___unix_getset
#define MODULE___unix_reboot
#define MODULE___unix_fork
#define MODULE___unix_socket
#define MODULE___unix_bind
#define MODULE___unix_listen
#define MODULE___unix_connect
#define MODULE___unix_accept
#define MODULE___unix_shutdown
#define MODULE___unix_socketpair
#define MODULE___unix_recv
#define MODULE___unix_recvfrom
#define MODULE___unix_send
#define MODULE___unix_sendto
#define MODULE___unix_getsockname
#define MODULE___unix_getpeername
#define MODULE___unix___vector
#endif

#ifdef MODULE_unix_tick
unsigned int unix_tick;
#endif

#ifdef MODULE_unix_ticks
unsigned int unix_ticks;
#endif

#ifdef MODULE___unix
unsigned int
__unix(unsigned int request, ...)
	{
	jmp_buf try_b;
	unsigned int catch;
	va_list arg_p;
 unsigned char sysio_save;

/* abyte('{'); */
/* ahexw((unsigned int)&request); */
	if (request >= __UNIX___VECTOR)
		{
/* abyte('}'); */
		return ENOSYS;
		}

	disable();
	my_thread_p->process_p->p_utime += unix_tick;
	unix_ticks += unix_tick;
	unix_tick = 0;
	enable();

	if (my_thread_p->process_p->p_traceme)
		{
		va_start(arg_p, request);
		strace_entry(request, arg_p);
		va_end(arg_p);
		}

	if (unix_ticks >= my_thread_p->process_p->p_cprio)
		{
		kwait(NULL);
		}

	va_start(arg_p, request);

	catch = setjmp(try_b);
	if (catch)
		{
		va_end(arg_p);

		if (my_thread_p->process_p->p_traceme)
			{
			strace_bomb(request, catch);
			}

		disable();
		my_thread_p->process_p->p_stime += unix_tick;
		unix_ticks += unix_tick;
		unix_tick = 0;
		enable();

/* abyte('}'); */
		return catch;
		}

 sysio_save = my_thread_p->sysio;
/* abyte('('); */
	__unix___vector[request](try_b, arg_p);
/* abyte(')'); */
 if (sysio_save != my_thread_p->sysio)
  {
  _dprintf(0, _("__unix() sysio clobbered\n"));
  }

	va_end(arg_p);

	if (my_thread_p->process_p->p_traceme)
		{
		va_start(arg_p, request);
		strace_exit(request, arg_p);
		va_end(arg_p);
		}

	disable();
	my_thread_p->process_p->p_stime += unix_tick;
	unix_ticks += unix_tick;
	unix_tick = 0;
	enable();

/* abyte('}'); */
	return 0;
	}
#endif

#ifdef MODULE___unix_sync
void
__unix_sync(jmp_buf error, va_list arg_p)
	{
	fs_sync(error);
	}
#endif

#ifdef MODULE___unix_utime
void
__unix_utime(jmp_buf error, va_list arg_p)
	{
	unsigned char *path;
	struct utimbuf *buf;

	path = va_arg(arg_p, unsigned char *);
	buf = va_arg(arg_p, struct utimbuf *);

	fs_utime(error, path, buf);
	}
#endif

#ifdef MODULE___unix_link
void
__unix_link(jmp_buf error, va_list arg_p)
	{
	unsigned char *path;
	unsigned char *path_new;

	path = va_arg(arg_p, unsigned char *);
	path_new = va_arg(arg_p, unsigned char *);

	fs_link(error, path, path_new);
	}
#endif

#ifdef MODULE___unix_symlink
void
__unix_symlink(jmp_buf error, va_list arg_p)
	{
	unsigned char *path;
	unsigned char *path_new;

	path = va_arg(arg_p, unsigned char *);
	path_new = va_arg(arg_p, unsigned char *);

	fs_symlink(error, path, path_new);
	}
#endif

#ifdef MODULE___unix_unlink
void
__unix_unlink(jmp_buf error, va_list arg_p)
	{
	fs_unlink(error, va_arg(arg_p, unsigned char *));
	}
#endif

#ifdef MODULE___unix_chdir
void
__unix_chdir(jmp_buf error, va_list arg_p)
	{
	fs_chdir(error, va_arg(arg_p, unsigned char *));
	}
#endif

#ifdef MODULE___unix_chroot
void
__unix_chroot(jmp_buf error, va_list arg_p)
	{
	fs_chroot(error, va_arg(arg_p, unsigned char *));
	}
#endif

#ifdef MODULE___unix_mknod
void
__unix_mknod(jmp_buf error, va_list arg_p)
	{
	unsigned char *path;
	mode_t mode;
	dev_t dev;

	path = va_arg(arg_p, unsigned char *);
	mode = va_arg(arg_p, mode_t);
	dev = va_arg(arg_p, dev_t);

	fs_mknod(error, path, mode, dev);
	}
#endif

#ifdef MODULE___unix_access
void
__unix_access(jmp_buf error, va_list arg_p)
	{
	unsigned char *path;
	unsigned int mode;

	path = va_arg(arg_p, unsigned char *);
	mode = va_arg(arg_p, unsigned int);

	fs_access(error, path, mode);
	}
#endif

#ifdef MODULE___unix_chmod
void
__unix_chmod(jmp_buf error, va_list arg_p)
	{
	unsigned char *path;
	mode_t mode;

	path = va_arg(arg_p, unsigned char *);
	mode = va_arg(arg_p, mode_t);

	fs_chmod(error, path, mode);
	}
#endif

#ifdef MODULE___unix_chown
void
__unix_chown(jmp_buf error, va_list arg_p)
	{
	unsigned char *path;
	unsigned int owner;
	unsigned int group;

	path = va_arg(arg_p, unsigned char *);
	owner = va_arg(arg_p, unsigned int);
	group = va_arg(arg_p, unsigned int);

	fs_chown(error, path, owner, group);
	}
#endif

#ifdef MODULE___unix_stat
void
__unix_stat(jmp_buf error, va_list arg_p)
	{
	unsigned char *path;
	struct stat *buf;

	path = va_arg(arg_p, unsigned char *);
	buf = va_arg(arg_p, struct stat *);

	fs_stat(error, path, buf);
	}
#endif

#ifdef MODULE___unix_mount
void
__unix_mount(jmp_buf error, va_list arg_p)
	{
	unsigned char *path;
	unsigned char *path_dir;
	unsigned int flag;

	path = va_arg(arg_p, unsigned char *);
	path_dir = va_arg(arg_p, unsigned char *);
	flag = va_arg(arg_p, unsigned int);

	fs_mount(error, path, path_dir, flag);
	}
#endif

#ifdef MODULE___unix_umount
void
__unix_umount(jmp_buf error, va_list arg_p)
	{
	unsigned char *path;

	fs_umount(error, va_arg(arg_p, unsigned char *));
	}
#endif

#ifdef MODULE___unix_execve
void
__unix_execve(jmp_buf error, va_list arg_p)
	{
	unsigned char *path;
	unsigned char **argv;
	unsigned char **envp;

	path = va_arg(arg_p, unsigned char *);
	argv = va_arg(arg_p, unsigned char **);
	envp = va_arg(arg_p, unsigned char **);

	fs_execve(error, path, argv, envp);
	}
#endif

#ifdef MODULE___unix_open
void
__unix_open(jmp_buf error, va_list arg_p)
	{
	unsigned int *fd_p;
	unsigned char *path;
	unsigned int flag;
	mode_t mode;

	fd_p = va_arg(arg_p, unsigned int *);
	path = va_arg(arg_p, unsigned char *);
	flag = va_arg(arg_p, unsigned int);
	mode = va_arg(arg_p, mode_t);

	usrput_unsigned_int(fd_p, of_open(error, path, flag, mode));
	}
#endif

#ifdef MODULE___unix_close
void
__unix_close(jmp_buf error, va_list arg_p)
	{
	uf_close(error, va_arg(arg_p, unsigned int));
	}
#endif

#ifdef MODULE___unix_read
void
__unix_read(jmp_buf error, va_list arg_p)
	{
	size_t *count_p;
	unsigned int fd;
	void *data;
	size_t count;

	count_p = va_arg(arg_p, size_t *);
	fd = va_arg(arg_p, unsigned int);
	data = va_arg(arg_p, void *);
	count = va_arg(arg_p, size_t);

	usrput_unsigned_int(count_p, uf_read(error, fd, data, count));
	}
#endif

#ifdef MODULE___unix_write
void
__unix_write(jmp_buf error, va_list arg_p)
	{
	size_t *count_p;
	unsigned int fd;
	void *data;
	size_t count;

	count_p = va_arg(arg_p, size_t *);
	fd = va_arg(arg_p, unsigned int);
	data = va_arg(arg_p, void *);
	count = va_arg(arg_p, size_t);

/* abyte('<'); */
	usrput_unsigned_int(count_p, uf_write(error, fd, data, count));
/* abyte('>'); */
	}
#endif

#ifdef MODULE___unix_lseek
void
__unix_lseek(jmp_buf error, va_list arg_p)
	{
	unsigned long *position_p;
	unsigned int fd;
	unsigned long position;
	unsigned int flag;

	position_p = va_arg(arg_p, unsigned long *);
	fd = va_arg(arg_p, unsigned int);
	position = va_arg(arg_p, unsigned long);
	flag = va_arg(arg_p, unsigned int);

	usrput_unsigned_long(position_p, of_lseek(error, fd, position, flag));
	}
#endif

#ifdef MODULE___unix_fstat
void
__unix_fstat(jmp_buf error, va_list arg_p)
	{
	unsigned int fd;
	struct stat *buf;

	fd = va_arg(arg_p, unsigned int);
	buf = va_arg(arg_p, struct stat *);

	uf_fstat(error, fd, buf);
	}
#endif

#ifdef MODULE___unix_falign
void
__unix_falign(jmp_buf error, va_list arg_p)
	{
	unsigned int fd;
	unsigned int flag;

	fd = va_arg(arg_p, unsigned int);
	flag = va_arg(arg_p, unsigned int);

	of_falign(error, fd, flag);
	}
#endif

#ifdef MODULE___unix_dup
void
__unix_dup(jmp_buf error, va_list arg_p)
	{
	unsigned int *fd_p;
	unsigned int fd;

	fd_p = va_arg(arg_p, unsigned int *);
	fd = va_arg(arg_p, unsigned int);

	usrput_unsigned_int(fd_p, uf_dup(error, fd));
	}
#endif

#ifdef MODULE___unix_dup2
void
__unix_dup2(jmp_buf error, va_list arg_p)
	{
	unsigned int fd;
	unsigned int fd_new;

	fd = va_arg(arg_p, unsigned int);
	fd_new = va_arg(arg_p, unsigned int);

	uf_dup2(error, fd, fd_new);
	}
#endif

#ifdef MODULE___unix_ioctl
void
__unix_ioctl(jmp_buf error, va_list arg_p)
	{
	unsigned int fd;
	unsigned int request;
	void *data;

	fd = va_arg(arg_p, unsigned int);
	request = va_arg(arg_p, unsigned int);
	data = va_arg(arg_p, void *);

	uf_ioctl(error, fd, request, data);
	}
#endif

#ifdef MODULE___unix_pipe
void
__unix_pipe(jmp_buf error, va_list arg_p)
	{
	of_pipe(error, va_arg(arg_p, unsigned int *));
	}
#endif

#ifdef MODULE___unix_time
void
__unix_time(jmp_buf error, va_list arg_p)
	{
	usrput_unsigned_long(va_arg(arg_p, rtctime_t *), rtc_time());
	}
#endif

#ifdef MODULE___unix_stime
void
__unix_stime(jmp_buf error, va_list arg_p)
	{
	rtc_stime(error, va_arg(arg_p, rtctime_t));
	}
#endif

#ifdef MODULE___unix_getfsys
void
__unix_getfsys(jmp_buf error, va_list arg_p)
	{
	dev_t devno;
	void *data;

	devno = va_arg(arg_p, dev_t);
	data = va_arg(arg_p, void *);

	process_getfsys(error, devno, data);
	}
#endif

#ifdef MODULE___unix_times
void
__unix_times(jmp_buf error, va_list arg_p)
	{
	process_times(error, va_arg(arg_p, struct tms *));
	}
#endif

#ifdef MODULE___unix_brk
void
__unix_brk(jmp_buf error, va_list arg_p)
	{
	process_brk(error, va_arg(arg_p, void *));
	}
#endif

#ifdef MODULE___unix_sbrk
void
__unix_sbrk(jmp_buf error, va_list arg_p)
	{
	void **brk_p;
	size_t incr;

	brk_p = va_arg(arg_p, void **);
	incr = va_arg(arg_p, size_t);

	usrput_unsigned_int(brk_p, (unsigned int)process_sbrk(error, incr));
	}
#endif

#ifdef MODULE___unix_waitpid
void
__unix_waitpid(jmp_buf error, va_list arg_p)
	{
	unsigned int *pid_p;
	unsigned int pid;
	unsigned int *statloc;
	unsigned int flag;

	pid_p = va_arg(arg_p, unsigned int *);
	pid = va_arg(arg_p, unsigned int);
	statloc = va_arg(arg_p, unsigned int *);
	flag = va_arg(arg_p, unsigned int);

	usrput_unsigned_int(pid_p, process_waitpid(error, pid, statloc, flag));
	}
#endif

#ifdef MODULE___unix__exit
void
__unix__exit(jmp_buf error, va_list arg_p)
	{
	unsigned int code;

	code = va_arg(arg_p, unsigned int);
	va_end(arg_p); /* as we'll never return */

	process__exit(code);
	}
#endif

#ifdef MODULE___unix_pause
void
__unix_pause(jmp_buf error, va_list arg_p)
	{
	process_pause(error);
	}
#endif

#ifdef MODULE___unix_signal
void
__unix_signal(jmp_buf error, va_list arg_p)
	{
	unsigned long *func_p;
	signal_t sig;
	unsigned long func;

	func_p = va_arg(arg_p, unsigned long *);
	sig = va_arg(arg_p, signal_t);
	func = va_arg(arg_p, unsigned long);

	usrput_unsigned_long(func_p, process_signal(error, sig, func));
	}
#endif

#ifdef MODULE___unix_kill
void
__unix_kill(jmp_buf error, va_list arg_p)
	{
	unsigned int pid;
	signal_t sig;

	pid = va_arg(arg_p, unsigned int);
	sig = va_arg(arg_p, signal_t);

	process_kill(error, pid, sig);
	}
#endif

#ifdef MODULE___unix_alarm
void
__unix_alarm(jmp_buf error, va_list arg_p)
	{
	unsigned int *secs_p;
	unsigned int secs;

	secs_p = va_arg(arg_p, unsigned int *);
	secs = va_arg(arg_p, unsigned int);

	usrput_unsigned_int(secs_p, process_alarm(secs));
	}
#endif

#ifdef MODULE___unix_getset
void
__unix_getset(jmp_buf error, va_list arg_p)
	{
	unsigned int *nick_p;
	unsigned int what;
	unsigned int parm1;
	unsigned int parm2;

	nick_p = va_arg(arg_p, unsigned int *);
	what = va_arg(arg_p, unsigned int);
	parm1 = va_arg(arg_p, unsigned int);
	parm2 = va_arg(arg_p, unsigned int);

	usrput_unsigned_int(nick_p, process_getset(error, what, parm1, parm2));
	}
#endif

#ifdef MODULE___unix_reboot
void
__unix_reboot(jmp_buf error, va_list arg_p)
	{
	unsigned int parm1;
	unsigned int parm2;

	parm1 = va_arg(arg_p, unsigned int);
	parm2 = va_arg(arg_p, unsigned int);

	process_reboot(error, parm1, parm2);
	}
#endif

#ifdef MODULE___unix_fork
void
__unix_fork(jmp_buf error, va_list arg_p)
	{
	fork_parent(error, va_arg(arg_p, unsigned int *));
	}
#endif

#ifdef MODULE___unix_socket
void
__unix_socket(jmp_buf error, va_list arg_p)
	{
	unsigned int *fd_p;
	unsigned int af;
	unsigned int type;
	unsigned int pf;

	fd_p = va_arg(arg_p, unsigned *);
	af = va_arg(arg_p, unsigned int);
	type = va_arg(arg_p, unsigned int);
	pf = va_arg(arg_p, unsigned int);

	usrput_unsigned_int(fd_p, os_socket(error, af, type, pf));
	}
#endif

#ifdef MODULE___unix_bind
void
__unix_bind(jmp_buf error, va_list arg_p)
	{
	unsigned int fd;
	struct sockaddr *name;
	unsigned int namelen;

	fd = va_arg(arg_p, unsigned int);
	name = va_arg(arg_p, struct sockaddr *);
	namelen = va_arg(arg_p, unsigned int);

	os_bind(error, fd, name, namelen);
	}
#endif

#ifdef MODULE___unix_listen
void
__unix_listen(jmp_buf error, va_list arg_p)
	{
	unsigned int fd;
	unsigned int backlog;

	fd = va_arg(arg_p, unsigned int);
	backlog = va_arg(arg_p, unsigned int);

	os_listen(error, fd, backlog);
	}
#endif

#ifdef MODULE___unix_connect
void
__unix_connect(jmp_buf error, va_list arg_p)
	{
	unsigned int fd;
	struct sockaddr *peername;
	unsigned int peernamelen;

	fd = va_arg(arg_p, unsigned int);
	peername = va_arg(arg_p, struct sockaddr *);
	peernamelen = va_arg(arg_p, unsigned int);

	os_connect(error, fd, peername, peernamelen);
	}
#endif

#ifdef MODULE___unix_accept
void
__unix_accept(jmp_buf error, va_list arg_p)
	{
	unsigned int *fd_p;
	unsigned int fd;
	struct sockaddr *peername;
	unsigned int *peernamelen;

	fd_p = va_arg(arg_p, unsigned *);
	fd = va_arg(arg_p, unsigned int);
	peername = va_arg(arg_p, struct sockaddr *);
	peernamelen = va_arg(arg_p, unsigned int *);

	usrput_unsigned_int(fd_p, os_accept(error, fd, peername, peernamelen));
	}
#endif

#ifdef MODULE___unix_shutdown
void
__unix_shutdown(jmp_buf error, va_list arg_p)
	{
	unsigned int fd;
	unsigned int flag;

	fd = va_arg(arg_p, unsigned int);
	flag = va_arg(arg_p, unsigned int);

	os_shutdown(error, fd, flag);
	}
#endif

#ifdef MODULE___unix_socketpair
void
__unix_socketpair(jmp_buf error, va_list arg_p)
	{
	unsigned int af;
	unsigned int type;
	unsigned int pf;
	unsigned int *fd_p;

	af = va_arg(arg_p, unsigned int);
	type = va_arg(arg_p, unsigned int);
	pf = va_arg(arg_p, unsigned int);
	fd_p = va_arg(arg_p, unsigned *);

	os_socketpair(error, af, type, pf, fd_p);
	}
#endif

#ifdef MODULE___unix_recv
void
__unix_recv(jmp_buf error, va_list arg_p)
	{
	size_t *count_p;
	unsigned int fd;
	void *data;
	size_t count;
	unsigned int flag;

	count_p = va_arg(arg_p, size_t *);
	fd = va_arg(arg_p, unsigned int);
	data = va_arg(arg_p, void *);
	count = va_arg(arg_p, size_t);
	flag = va_arg(arg_p, unsigned int);

	usrput_unsigned_int(count_p, os_recv(error, fd, data, count, flag));
	}
#endif

#ifdef MODULE___unix_recvfrom
void
__unix_recvfrom(jmp_buf error, va_list arg_p)
	{
	size_t *count_p;
	unsigned int fd;
	void *data;
	size_t count;
	unsigned int flag;
	struct sockaddr *from;
	unsigned int *fromlen;

	count_p = va_arg(arg_p, size_t *);
	fd = va_arg(arg_p, unsigned int);
	data = va_arg(arg_p, void *);
	count = va_arg(arg_p, size_t);
	flag = va_arg(arg_p, unsigned int);
	from = va_arg(arg_p, struct sockaddr *);
	fromlen = va_arg(arg_p, unsigned int *);

	usrput_unsigned_int(count_p, os_recvfrom(error, fd, data, count, flag,
			from, fromlen));
	}
#endif

#ifdef MODULE___unix_send
void
__unix_send(jmp_buf error, va_list arg_p)
	{
	size_t *count_p;
	unsigned int fd;
	void *data;
	size_t count;
	unsigned int flag;

	count_p = va_arg(arg_p, size_t *);
	fd = va_arg(arg_p, unsigned int);
	data = va_arg(arg_p, void *);
	count = va_arg(arg_p, size_t);
	flag = va_arg(arg_p, unsigned int);

	usrput_unsigned_int(count_p, os_send(error, fd, data, count, flag));
	}
#endif

#ifdef MODULE___unix_sendto
void
__unix_sendto(jmp_buf error, va_list arg_p)
	{
	size_t *count_p;
	unsigned int fd;
	void *data;
	size_t count;
	unsigned int flag;
	struct sockaddr *to;
	unsigned int tolen;

	count_p = va_arg(arg_p, size_t *);
	fd = va_arg(arg_p, unsigned int);
	data = va_arg(arg_p, void *);
	count = va_arg(arg_p, size_t);
	flag = va_arg(arg_p, unsigned int);
	to = va_arg(arg_p, struct sockaddr *);
	tolen = va_arg(arg_p, unsigned int);

	usrput_unsigned_int(count_p, os_sendto(error, fd, data, count, flag,
			to, tolen));
	}
#endif

#ifdef MODULE___unix_getsockname
void
__unix_getsockname(jmp_buf error, va_list arg_p)
	{
	unsigned int fd;
	struct sockaddr *name;
	unsigned int *namelen;

	fd = va_arg(arg_p, unsigned int);
	name = va_arg(arg_p, struct sockaddr *);
	namelen = va_arg(arg_p, unsigned int *);

	os_getsockname(error, fd, name, namelen);
	}
#endif

#ifdef MODULE___unix_getpeername
void
__unix_getpeername(jmp_buf error, va_list arg_p)
	{
	unsigned int fd;
	struct sockaddr *peername;
	unsigned int *peernamelen;

	fd = va_arg(arg_p, unsigned int);
	peername = va_arg(arg_p, struct sockaddr *);
	peernamelen = va_arg(arg_p, unsigned int *);

	os_getpeername(error, fd, peername, peernamelen);
	}
#endif

#ifdef MODULE___unix___vector
void (*__unix___vector[__UNIX___VECTOR])(jmp_buf error, va_list arg_p) =
	{
	__unix_access,
	__unix_alarm,
	__unix_brk,
	__unix_chdir,
	__unix_chmod,
	__unix_chown,
	__unix_close,
	__unix_getset,
	__unix_dup,
	__unix_dup2,
	__unix_execve,
	__unix__exit,
	__unix_fork,
	__unix_fstat,
	__unix_getfsys,
	__unix_ioctl,
	__unix_kill,
	__unix_link,
	__unix_mknod,
	__unix_mount,
	__unix_open,
	__unix_pause,
	__unix_pipe,
	__unix_read,
	__unix_sbrk,
	__unix_lseek,
	__unix_signal,
	__unix_stat,
	__unix_stime,
	__unix_sync,
	__unix_time,
	__unix_times,
	__unix_umount,
	__unix_unlink,
	__unix_utime,
	__unix_waitpid,
	__unix_write,
	__unix_reboot,
	__unix_symlink,
	__unix_chroot,
	__unix_falign,
	__unix_fork, /* should be __unix_vfork */
	__unix_socket,
	__unix_bind,
	__unix_listen,
	__unix_connect,
	__unix_accept,
	__unix_shutdown,
	__unix_socketpair,
	__unix_recv,
	__unix_recvfrom,
	__unix_send,
	__unix_sendto,
	__unix_getsockname,
	__unix_getpeername
	};
#endif

