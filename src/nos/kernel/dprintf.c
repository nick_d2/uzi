/* dprintf.c by Nick for NOS/UZI project
 *
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 Diagnostic output routines
**********************************************************/

#include "kernel/thread.h" /* for Curptab and Stkchk */
#include "kernel/dprintf.h"
#include <libintl.h>
#include "po/messages.h"

#ifndef DEBUG
#define DEBUG 0
#endif

#ifndef MODULE
#define MODULE_panic
#define MODULE__panic
#define MODULE_dprintf
#define MODULE_vdprintf
#define MODULE_dputter
#endif

#ifdef MODULE_panic
int
panic(const char *message, ...)
{
	va_list arguments;

	_dprintf(0, _("\npanic: "));
	va_start(arguments, message);
	vdprintf(0, message, arguments);
	va_end(arguments);
	exit(1);
	}
#endif

#ifdef MODULE__panic
int
_panic(_char *message, ...)
{
	va_list arguments;

	_dprintf(0, _("\npanic: "));
	va_start(arguments, message);
	_vdprintf(0, message, arguments);
	va_end(arguments);
	exit(1);
	}
#endif

#ifdef MODULE_dprintf
int
dprintf(unsigned char level, const char *format, ...)
	{
	va_list arguments;
	int result;

#if 0
 if (*(unsigned int *)0x5ea != 0xc933)
  {
  abyte('!');
  }
#endif
	if (Debug_level < level)
		{
		return 0;
		}

	va_start(arguments, format);
	result = __vprinter(dputter, NULL, (char *)format, arguments);
						/* fix this nasty cast! */
	va_end(arguments);

	return result;
	}
#endif

#ifdef MODULE_vdprintf
int
vdprintf(unsigned char level, const char *format, va_list arguments)
	{
	if (Debug_level < level)
		{
		return 0;
		}

	return __vprinter(dputter, NULL, (char *)format, arguments);
						/* fix this nasty cast! */
	}
#endif

#ifdef MODULE_dputter
/* NOTE!  this is not really used, instead see z80/diag.S for the asm ver */
unsigned int /*NICK_PASCAL NICK_NEAR*/
dputter(void *ptr, unsigned int n, FILE *fp)
	{
	unsigned char ch, *p, *q;

	q = (unsigned char *)ptr + n;
	for (p = (unsigned char *)ptr; p < q; p++)
		{
		ch = *p;
		if (ch == '\n')
			{
			acrlf();
			}
		else
			{
			abyte(ch);
			}
		}
	return n;
	}
#endif

