/* usrmem.c by Nick for NOS/UZI implementation
 *
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 General utility subroutines
**********************************************************/

#include <string.h>
#include "asm.h" /* for PAGE_LOG */
#include "nos/global.h" /* for copyr() */
#include "kernel/thread.h"
#include "kernel/usrmem.h"

#ifndef MODULE
#define MODULE_usrget
#define MODULE_usrget_string
#define MODULE_usrget_unsigned_int
#define MODULE_usrget_unsigned_char
#define MODULE_usrget_unsigned_long
#define MODULE_usrput
#define MODULE_usrput_unsigned_int
#define MODULE_usrput_unsigned_char
#define MODULE_usrput_unsigned_long
#endif

#ifdef MODULE_usrget
void *usrget(void *dest, const void *src, size_t count)
	{
	copyr(my_thread_p->sysio ?
			(src < (void *)KRNL_BA_START ? (unsigned long)src :
			((unsigned long)
			(my_thread_p->stack_arena_p->page -
			(KRNL_BA_START >> PAGE_LOG)) << PAGE_LOG) +
			(unsigned long)src) :
			((unsigned long)
			(my_thread_p->process_p->arena_p->page -
			(USER_BA_START >> PAGE_LOG)) << PAGE_LOG) +
			(unsigned long)src,

			dest < (void *)KRNL_BA_START ? (unsigned long)dest :
			((unsigned long)
			(my_thread_p->stack_arena_p->page -
			(KRNL_BA_START >> PAGE_LOG)) << PAGE_LOG) +
			(unsigned long)dest,

			count);
	return dest;
	}
#endif

#ifdef MODULE_usrget_string
void *usrget_string(void *dest, const void *src, size_t count)
	{
#if 1 /* total hackarooney */
	usrget(dest, src, count);
#else
	if (my_thread_p->sysio)
		{
		return memccpy(dest, src, 0, count);
		}
	copyr(((unsigned long)(my_thread_p->process_p->arena_p->page -
			(USER_BA_START >> PAGE_LOG)) << PAGE_LOG) +
			(unsigned long)src,

			dest < (void *)KRNL_BA_START ? (unsigned long)dest :
			((unsigned long)
			(my_thread_p->stack_arena_p->page -
			(KRNL_BA_START >> PAGE_LOG)) << PAGE_LOG) +
			(unsigned long)dest,

			count);
#endif
	return memchr(dest, 0, count);
	}
#endif

#ifdef MODULE_usrget_unsigned_int
unsigned int usrget_unsigned_int(const void *src)
	{
	unsigned int value;

	usrget(&value, src, sizeof(value));
	return value;
	}
#endif

#ifdef MODULE_usrget_unsigned_char
unsigned char usrget_unsigned_char(const void *src)
	{
	unsigned char value;

	usrget(&value, src, sizeof(value));
	return value;
	}
#endif

#ifdef MODULE_usrget_unsigned_long
unsigned long usrget_unsigned_long(const void *src)
	{
	unsigned long value;

	usrget(&value, src, sizeof(value));
	return value;
	}
#endif

#ifdef MODULE_usrput
void *usrput(void *dest, const void *src, size_t count)
	{
	copyr(src < (void *)KRNL_BA_START ? (unsigned long)src :
			((unsigned long)
			(my_thread_p->stack_arena_p->page -
			(KRNL_BA_START >> PAGE_LOG)) << PAGE_LOG) +
			(unsigned long)src,

			my_thread_p->sysio ?
			(dest < (void *)KRNL_BA_START ? (unsigned long)dest :
			((unsigned long)
			(my_thread_p->stack_arena_p->page -
			(KRNL_BA_START >> PAGE_LOG)) << PAGE_LOG) +
			(unsigned long)dest) :
			((unsigned long)
			(my_thread_p->process_p->arena_p->page -
			(USER_BA_START >> PAGE_LOG)) << PAGE_LOG) +
			(unsigned long)dest,

			count);
	}
#endif

#ifdef MODULE_usrput_unsigned_int
void usrput_unsigned_int(void *dest, unsigned int value)
	{
/* abyte('X'); */
	usrput(dest, &value, sizeof(value));
/* abyte('Y'); */
	}
#endif

#ifdef MODULE_usrput_unsigned_char
void usrput_unsigned_char(void *dest, unsigned char value)
	{
	usrput(dest, &value, sizeof(value));
	}
#endif

#ifdef MODULE_usrput_unsigned_long
void usrput_unsigned_long(void *dest, unsigned long value)
	{
	usrput(dest, &value, sizeof(value));
	}
#endif

