/* strace.c by Nick for UZI180 */

#include "kernel/dprintf.h"
#include "kernel/process.h"
#include "kernel/thread.h"
#include "kernel/strace.h"
#include "kernel/usrmem.h"
#include <libintl.h>
#include "po/messages.h"

struct strace_s
	{
	unsigned char return_type;
	_char *function_name;
	unsigned char argument_type[6];
	};

#define STRACE_LIST_MAX 55
#define STRACE_ERROR_MAX 42

#ifdef MODULE
#define STATIC
extern struct strace_s strace_list[STRACE_LIST_MAX];
/*extern unsigned int strace_list_n;*/
extern _char *strace_error[STRACE_ERROR_MAX];
/*extern unsigned int strace_error_n;*/
#else
#define STATIC static
#define MODULE_strace_list
#define MODULE_strace_error
#define MODULE_strace_entry
#define MODULE_strace_exit
#define MODULE_strace_bomb
#define MODULE_strace_dump_memory
#define MODULE_strace_unsigned_int
#define MODULE_strace_unsigned_long
#endif

#ifdef MODULE_strace_list
STATIC struct strace_s strace_list[] =
	{
	{ 0,		N_("access"),	{ TYPE_STR, TYPE_OCT, 0, 0, 0, 0 } },
	{ TYPE_DEC,	N_("alarm"),	{ TYPE_DEC, 0, 0, 0, 0, 0 } },
	{ 0,		N_("brk"),	{ TYPE_HEX, 0, 0, 0, 0, 0 } },
	{ 0,		N_("chdir"),	{ TYPE_STR, 0, 0, 0, 0, 0 } },
	{ 0,		N_("chmod"),	{ TYPE_STR, TYPE_OCT, 0, 0, 0, 0 } },
	{ 0,		N_("chown"),	{ TYPE_STR, TYPE_DEC, TYPE_DEC, 0, 0, 0 } },
	{ 0,		N_("close"),	{ TYPE_DEC, 0, 0, 0, 0, 0 } },
	{ TYPE_DEC,	N_("getset"),	{ TYPE_HEX, TYPE_HEX, TYPE_HEX, 0, 0, 0 } },
	{ TYPE_DEC,	N_("dup"),	{ TYPE_DEC, 0, 0, 0, 0, 0 } },
	{ 0,		N_("dup2"),	{ TYPE_DEC, TYPE_DEC, 0, 0, 0, 0 } },
	{ 0,		N_("execve"),	{ TYPE_STR, TYPE_HEX, TYPE_HEX, 0, 0, 0 } },
	{ 0,		N_("_exit"),	{ TYPE_DEC, 0, 0, 0, 0, 0 } },
	{ TYPE_DEC,	N_("fork"),	{ 0, 0, 0, 0, 0, 0 } },
	{ 0,		N_("fstat"),	{ TYPE_DEC, TYPE_HEX, 0, 0, 0, 0 } },
	{ 0,		N_("getfsys"),	{ TYPE_HEX, TYPE_HEX, 0, 0, 0, 0 } },
	{ 0,		N_("ioctl"),	{ TYPE_DEC, TYPE_HEX, TYPE_HEX, 0, 0, 0 } },
	{ 0,		N_("kill"),	{ TYPE_DEC, TYPE_DEC, 0, 0, 0, 0 } },
	{ 0,		N_("link"),	{ TYPE_STR, TYPE_STR, 0, 0, 0, 0 } },
	{ 0,		N_("mknod"),	{ TYPE_STR, TYPE_OCT, 0, 0, 0, 0 } },
	{ 0,		N_("mount"),	{ TYPE_STR, TYPE_STR, 0, 0, 0, 0 } },
	{ TYPE_DEC,	N_("open"),	{ TYPE_STR, TYPE_HEX, TYPE_OCT, 0, 0, 0 } },
	{ 0,		N_("pause"),	{ 0, 0, 0, 0, 0, 0 } },
	{ 0,		N_("pipe"),	{ TYPE_HEX, 0, 0, 0, 0, 0 } },
	{ TYPE_BIN,	N_("read"),	{ TYPE_DEC, TYPE_HEX, TYPE_HEX, 0, 0, 0 } },
	{ TYPE_HEX,	N_("sbrk"),	{ TYPE_HEX, 0, 0, 0, 0, 0 } },
	{ TYPE_LNG,	N_("lseek"),	{ TYPE_DEC, TYPE_LNG, TYPE_DEC, 0, 0, 0 } },
	{ TYPE_LNG,	N_("signal"),	{ TYPE_DEC, TYPE_LNG, 0, 0, 0, 0 } },
	{ 0,		N_("stat"),	{ TYPE_STR, TYPE_HEX, 0, 0, 0, 0 } },
	{ 0,		N_("stime"),	{ TYPE_HEX, 0, 0, 0, 0, 0 } },
	{ 0,		N_("sync"),	{ 0, 0, 0, 0, 0, 0 } },
	{ TYPE_LNG,	N_("time"),	{ TYPE_HEX, 0, 0, 0, 0, 0 } },
	{ 0,		N_("times"),	{ TYPE_HEX, 0, 0, 0, 0, 0 } },
	{ 0,		N_("umount"),	{ TYPE_STR, 0, 0, 0, 0, 0 } },
	{ 0,		N_("unlink"),	{ TYPE_STR, 0, 0, 0, 0, 0 } },
	{ 0,		N_("utime"),	{ TYPE_STR, TYPE_HEX, 0, 0, 0, 0 } },
	{ TYPE_DEC,	N_("waitpid"),	{ TYPE_DEC, TYPE_HEX, TYPE_HEX, 0, 0, 0 } },
	{ TYPE_HEX,	N_("write"),	{ TYPE_DEC, TYPE_BIN, 0, 0, 0, 0 } },
	{ 0,		N_("reboot"),	{ TYPE_HEX, TYPE_HEX, 0, 0, 0, 0 } },
	{ 0,		N_("symlink"),	{ TYPE_STR, TYPE_STR, 0, 0, 0, 0 } },
	{ 0,		N_("chroot"),	{ TYPE_STR, 0, 0, 0, 0, 0 } },
	{ 0,		N_("falign"),	{ TYPE_DEC, TYPE_DEC, 0, 0 } },
	{ TYPE_DEC,	N_("vfork"),	{ 0, 0, 0, 0, 0, 0 } },
	{ TYPE_DEC,	N_("socket"),	{ TYPE_DEC, TYPE_DEC, TYPE_DEC, 0, 0, 0 } },
	{ 0,		N_("bind"),	{ TYPE_DEC, TYPE_HEX, TYPE_HEX, 0, 0, 0 } },
	{ 0,		N_("listen"),	{ TYPE_DEC, TYPE_DEC, 0, 0, 0, 0 } },
	{ 0,		N_("connect"),	{ TYPE_DEC, TYPE_HEX, TYPE_HEX, 0, 0, 0 } },
	{ TYPE_DEC,	N_("accept"),	{ TYPE_DEC, TYPE_HEX, TYPE_HEX, 0, 0, 0 } },
	{ 0,		N_("shutdown"),	{ TYPE_DEC, TYPE_DEC, 0, 0, 0, 0 } },
	{ 0,		N_("socketpair"), { TYPE_DEC, TYPE_DEC, TYPE_DEC, TYPE_HEX, 0, 0 } },
	{ TYPE_BIN,	N_("recv"),	{ TYPE_DEC, TYPE_HEX, TYPE_HEX, TYPE_HEX, 0, 0 } },
	{ TYPE_BIN,	N_("recvfrom"),	{ TYPE_DEC, TYPE_HEX, TYPE_HEX, TYPE_HEX, TYPE_HEX, TYPE_HEX } },
	{ TYPE_HEX,	N_("send"),	{ TYPE_DEC, TYPE_BIN, TYPE_HEX, 0, 0, 0 } },
	{ TYPE_HEX,	N_("sendto"),	{ TYPE_DEC, TYPE_BIN, TYPE_HEX, TYPE_HEX, TYPE_HEX, 0 } },
	{ 0,		N_("getsockname"), { TYPE_DEC, TYPE_HEX, TYPE_HEX, 0, 0, 0 } },
	{ 0,		N_("getpeername"), { TYPE_DEC, TYPE_HEX, TYPE_HEX, 0, 0, 0 } }
	};

/*STATIC unsigned int strace_list_n =
		sizeof(strace_list) / sizeof(strace_list[0]);*/
#endif

#ifdef MODULE_strace_error
STATIC _char *strace_error[STRACE_ERROR_MAX] =
	{
	N_(""),
	N_("EPERM"),
	N_("ENOENT"),
	N_("ESRCH"),
	N_("EINTR"),
	N_("EIO"),
	N_("ENXIO"),
	N_("E2BIG"),
	N_("ENOEXEC"),
	N_("EBADF"),
	N_("ECHILD"),
	N_("EAGAIN"),
	N_("ENOMEM"),
	N_("EACCES"),
	N_("EFAULT"),
	N_("ENOTBLK"),
	N_("EBUSY"),
	N_("EEXIST"),
	N_("EXDEV"),
	N_("ENODEV"),
	N_("ENOTDIR"),
	N_("EISDIR"),
	N_("EINVAL"),
	N_("ENFILE"),
	N_("EMFILE"),
	N_("ENOTTY"),
	N_("ETXTBSY"),
	N_("EFBIG"),
	N_("ENOSPC"),
	N_("ESPIPE"),
	N_("EROFS"),
	N_("EMLINK"),
	N_("EPIPE"),
	N_("EDOM"),
	N_("ERANGE"),
	N_("EDEADLK"),
	N_("ENAMETOOLONG"),
	N_("ENOLCK"),
	N_("EINVFNC"),
	N_("ENOTEMPTY"),
	N_("ELOOP"),
	N_("ESHELL")
	};

/*STATIC unsigned int strace_error_n =
		sizeof(strace_error) / sizeof(strace_error[0]);*/
#endif

STATIC void strace_dump_memory(unsigned char type, unsigned char *data,
		size_t count);
STATIC void strace_unsigned_int(unsigned char type, unsigned int value);
STATIC void strace_unsigned_long(unsigned long value);

#ifdef MODULE_strace_entry
void
strace_entry(unsigned int request, va_list arg_p)
	{
	unsigned char i;
	unsigned char *data;
	size_t count;
	unsigned char type;

#if 0
 if (my_thread_p->process_p->object_p[3] == NULL)
  {
  abyte('_');
  }
 else
  {
  abyte(my_thread_p->process_p->object_p[3]->busy ? '!' : '#');
  }
#endif
	_dprintf(0, _("\npid %u, call %u, "), my_thread_p->process_p->p_pid,
			request);

	if (request >= STRACE_LIST_MAX) /*strace_list_n)*/
		{
		_panic(_("can't trace invalid syscall index"));
		}

	_dprintf(0, strace_list[request].function_name);
	abyte('(');

	if (strace_list[request].return_type)
		{
		va_arg(arg_p, void *); /* skip past result ptr */
		}

	for (i = 0; i < 6; i++)
		{
		type = strace_list[request].argument_type[i];
		if (type == 0)
			{
			break;
			}

		if (i)
			{
			abyte(',');
			abyte(' ');
			}

		switch (type)
			{
		case TYPE_BIN:
			data = va_arg(arg_p, unsigned char *);
			count = va_arg(arg_p, size_t);
			strace_dump_memory(TYPE_BIN, data, count);
			abyte(',');
			abyte(' ');
			strace_unsigned_int(TYPE_HEX, count);
			break;

		case TYPE_STR:
			strace_dump_memory(TYPE_STR,
					va_arg(arg_p, unsigned char *), 32);
			break;

		case TYPE_OCT:
		case TYPE_DEC:
		case TYPE_HEX:
			strace_unsigned_int(type, va_arg(arg_p, unsigned int));
			break;

		case TYPE_LNG:
			strace_unsigned_long(va_arg(arg_p, unsigned long));
			break;
			}
		}

	abyte(')');
	abyte(' ');
	}
#endif

#ifdef MODULE_strace_exit
void
strace_exit(unsigned int request, va_list arg_p)
	{
	unsigned char type;
	size_t count;

#if 0
 if (my_thread_p->process_p->object_p[3] == NULL)
  {
  abyte('_');
  }
 else
  {
  abyte(my_thread_p->process_p->object_p[3]->busy ? '!' : '#');
  }
#endif
	_dprintf(0, _("\npid %u, call %u, ret "),
			my_thread_p->process_p->p_pid, request);

	type = strace_list[request].return_type;
	switch (type)
		{
	case 0:
		abyte('0');
		break;

	case TYPE_BIN:
		count = usrget_unsigned_int(va_arg(arg_p, size_t *));
		va_arg(arg_p, unsigned int); /* skip past fd */
		strace_dump_memory(TYPE_BIN, va_arg(arg_p, unsigned char *),
				count);
		abyte(',');
		abyte(' ');
		strace_unsigned_int(TYPE_HEX, count);

	case TYPE_OCT:
	case TYPE_DEC:
	case TYPE_HEX:
		strace_unsigned_int(type, usrget_unsigned_int(va_arg(arg_p,
				unsigned int *)));
		break;

	case TYPE_LNG:
		strace_unsigned_long(usrget_unsigned_long(va_arg(arg_p,
				unsigned long *)));
		break;
		}

	_dprintf(0, _(", err 0 "));
	}
#endif

#ifdef MODULE_strace_bomb
void
strace_bomb(unsigned int request, unsigned int error)
	{
#if 0
 if (my_thread_p->process_p->object_p[3] == NULL)
  {
  abyte('_');
  }
 else
  {
  abyte(my_thread_p->process_p->object_p[3]->busy ? '!' : '#');
  }
#endif
	_dprintf(0, _("\npid %u, call %u, ret -1, err %u "),
			my_thread_p->process_p->p_pid, request, error);

	if (error < STRACE_ERROR_MAX) /*strace_error_n)*/
		{
		abyte('(');
		_dprintf(0, strace_error[error]);
		abyte(')');
		abyte(' ');
		}
	}
#endif

#ifdef MODULE_strace_dump_memory
STATIC void
strace_dump_memory(unsigned char type, unsigned char *data, size_t count)
	{
	unsigned char c, j, flag;

	flag = 1; /* whether to show ellipsis */
	if (type == TYPE_BIN)
		{
		if (count > 8)
			{
			count = 8; /* limit size to be printed */
			}
		else
			{
			flag = 0; /* whether to show ellipsis */
			}
		}

	abyte('"');
	for (j = 0; j < count; j++)
		{
		c = ugetc(data++);
		if (c == 0 && type == TYPE_STR)
			{
			flag = 0; /* whether to show ellipsis */
			break;
			}
		if (c < 0x20 || c >= 0x80)
			{
			abyte('\\');
			switch (c)
				{
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
				abyte('0' + c);
				break;
			case 9:
				abyte('t');
				break;
			case 0xa:
				abyte('n');
				break;
			case 0xd:
				abyte('r');
				break;
			default:
				_dprintf(0, _("x%x"), (int)c);
				break;
				}
			}
		else
			{
			if (c == '\\' || c == '"')
				{
				abyte('\\');
				}
			abyte(c);
			}
		}

	if (flag)
		{
		abyte('.');
		abyte('.');
		abyte('.');
		}

	abyte('"');
	}
#endif

#ifdef MODULE_strace_unsigned_int
STATIC void
strace_unsigned_int(unsigned char type, unsigned int value)
	{
	switch (type)
		{
	case TYPE_OCT:
		if (value < 8)
			{
			abyte('0' + value);
			}
		else
			{
			_dprintf(0, _("0%o"), value);
			}
		break;

	case TYPE_DEC:
		_dprintf(0, _("%d"), value);
		break;

	case TYPE_HEX:
		if (value < 10)
			{
			abyte('0' + value);
			}
		else
			{
			_dprintf(0, _("0x%x"), value);
			}
		break;
		}
	}
#endif

#ifdef MODULE_strace_unsigned_long
STATIC void
strace_unsigned_long(unsigned long value)
	{
	if (value < 10)
		{
		abyte('0' + (unsigned int)value);
		}
	else
		{
		_dprintf(0, _("0x%lx"), value);
		}
	}
#endif

