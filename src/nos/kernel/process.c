/* process.c by Nick for NOS/UZI project
 *
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 Kernel process management routines
**********************************************************/

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/ioctl.h> /* for GI_PTAB etc */
#include <sys/wait.h> /* for WNOHANG */
#include "asm.h" /* for PAGE_LOG */
#include "nos/global.h" /* for ksignal(), kwait(), copyr() */
#include "main/commands.h" /* for doshutdown() */
#include "kernel/object.h" /* for object_inc_refs() */
#include "kernel/dprintf.h"
#include "kernel/process.h"
#include "filesys/openfile.h" /* for struct open_file_s, of_alloc(), of_deref() */
#include "driver/bufpool.h"
#include "filesys/cinode.h"
#include "filesys/filesys.h"
#include "kernel/usrmem.h"
#include "kernel/thread.h"
#include "kernel/valadr.h"
#include "socket/opensock.h" /* for os_deref() */
#include "kernel/stack.h" /* for stack_set() */
#ifdef JOB_CONTROL
#include "kernel/session.h"
#endif
#include <libintl.h>
#include "po/messages.h"

#ifndef DEBUG
#define DEBUG 0
#endif

#ifndef MODULE
#define MODULE_process_list
#define MODULE_process_new
#define MODULE_process_delete
#define MODULE_process_ref
#define MODULE_process_deref
#define MODULE_process_find
#define MODULE_process_fork
#define MODULE_process_garbage
#define MODULE_process_times
#define MODULE_process_brk
#define MODULE_process_sbrk
#define MODULE_process_waitpid
#define MODULE_process__exit
#define MODULE_process_pause
#define MODULE_process_signal
#define MODULE_process_kill
#define MODULE_process_alarm
#define MODULE_process_getset
#define MODULE_process_reboot
#define MODULE_process_fclose
#define MODULE_process_thread_create
#endif

#define addtick(d, s) ((*(d)) += (*(s)))

#ifdef MODULE_process_list
struct process_s *process_list[PROCESS_LIST];
#endif

#ifdef MODULE_process_new
struct process_s *
process_new(void)
	{
	register struct process_s **process_pp, *process_p;
	/* int i_state; */

#if DEBUG >= 3
	_dprintf(3, _("process_new() starting\n"));
#endif
	for (process_pp = process_list; process_pp < process_list + PROCESS_LIST;
			process_pp++)
		{
		/* i_state = dirps(); */
		process_p = *process_pp;
		if (process_p && process_p->status == P_EMPTY)
			{
			goto found;
			}
		/* restore(i_state); */
		}

	process_p = malloc(sizeof(struct process_s));
	if (process_p == NULL)
		{
		goto error;
		}

	for (process_pp = process_list; process_pp < process_list + PROCESS_LIST; process_pp++)
		{
		/* i_state = dirps(); */
		if (*process_pp == NULL)
			{
			*process_pp = process_p;
			goto found;
			}
		/* restore(i_state); */
		}

	free(process_p);
error:
#if DEBUG >= 3
	_dprintf(3, _("process_new() returning NULL, error\n"));
#endif
	return NULL;

found:
	memset(process_p, 0, sizeof(struct process_s));
	process_p->status == P_FORKING;
	process_p->refs = 1;
	/* restore(i_state); */
#if DEBUG >= 3
	_dprintf(3, _("process_new() returning 0x%x, success\n"), process_p);
#endif
	return process_p;
	}
#endif

#ifdef MODULE_process_delete
void
process_delete(struct process_s *process_p)
	{
	register int i;
	struct process_s **process_pp, *other_process_p;
#if 1 /* copied from uf_close(), combine them later */
	struct object_s *object_p;
#endif
	/* int i_state; */

#if DEBUG >= 3
	_dprintf(3, _("process_delete(0x%x) starting\n"), process_p);
#endif

/* abyte('['); */
	if (process_p->arena_p)
		{
		arena_deref(process_p->arena_p);
		}

	if (process_p->stack_arena_p)
		{
		arena_deref(process_p->stack_arena_p); /* shared stack area */
		}

#ifdef JOB_CONTROL
	if (process_p->session_p)
		{
		session_deref(process_p->session_p);
		}
#endif

	if (process_p->p_cwd)
		{
		i_deref(process_p->p_cwd); /* free process's cwd */
		}

	if (process_p->p_root)
		{
		i_deref(process_p->p_root); /* free process's root */
		}

	for (i = 0; i < USER_STREAM_LIST; i++)
		{
		object_p = process_p->stream_p[i];
		if (object_p)
			{
			process_fclose(process_p, object_p);
			}
		}

	for (i = 0; i < USER_OBJECT_LIST; i++)
		{
		object_p = process_p->object_p[i];
		if (object_p)
			{
#if 1 /* copied from uf_close(), combine them later */
			object_acquire(object_p);

			switch (object_p->type)
				{
			case _FL_SOCK:
				object_release(object_p); /* eliminate later */
				os_deref((struct usock *)object_p);
				break;

			case _FL_FILE:
				object_release(object_p); /* eliminate later */
				of_deref((struct open_file_s *)object_p);
				break;

			default:
				_panic(_("process_delete() unknown object"));
				}
#endif
			}
		}

	/* if we have any children, set child's parent to our parent */
	for (process_pp = process_list; process_pp < process_list + PROCESS_LIST;
			process_pp++)
		{
		/* i_state = dirps(); */
		other_process_p = *process_pp;
		if (other_process_p && other_process_p->status &&
				other_process_p->parent_p == process_p &&
				other_process_p != process_p)
			{
			other_process_p->parent_p = process_p->parent_p;
			}
		/* restore(i_state); */
		}

	/* wake up a waiting parent, if any */
#if 1 /* temporary */
	/* note: parent_p may be null, meaning orphaned */
	/* exitval -1 means death by loss of all threads */
	if (process_p->parent_p
#ifdef JOB_CONTROL
			&& process_p->p_exitval != -1
#endif
			)
		{
/* abyte('%'); */
		ksignal(process_p->parent_p, 1); /* wake only 1 */
		process_p->status = P_ZOMBIE;
		}
	else
		{
/* abyte('&'); */
		/* don't leak a process entry if no parent is waiting for us */
		process_p->status = P_EMPTY;
		}
#else
	ssig((char *)process_p->parent_p, 0);
	process_p->status = P_ZOMBIE;
#endif

/* abyte(']'); */
#if DEBUG >= 3
	_dprintf(3, _("process_delete() returning, success\n"));
#endif
	}
#endif

#ifdef MODULE_process_ref
void
process_ref(struct process_s *process_p)
	{
	/* int i_state; */

#if DEBUG >= 3
	_dprintf(3, _("process_ref(0x%x) starting\n"), process_p);
#endif
	/* i_state = dirps(); */
	/* assert(process_p->refs != (unsigned char)0xff); */
	process_p->refs++;
	/* restore(i_state); */
#if DEBUG >= 3
	_dprintf(3, _("process_ref() returning, refs %u\n"),
			(unsigned int)process_p->refs);
#endif
	}
#endif

#ifdef MODULE_process_deref
void
process_deref(struct process_s *process_p)
	{
	/* int i_state; */

#if DEBUG >= 3
	_dprintf(3, _("process_deref(0x%x) starting\n"), process_p);
#endif
	/* i_state = dirps(); */
	/* assert(process_p->refs != (unsigned char)0); */
	if (--(process_p->refs) == (unsigned char)0)
		{
		process_delete(process_p);
		}
	/* restore(i_state); */
#if DEBUG >= 3
	_dprintf(3, _("process_deref() returning, refs %u\n"),
			(unsigned int)process_p->refs);
#endif
	}
#endif

#ifdef MODULE_process_fork
struct process_s *
process_fork(struct process_s *parent_process_p, unsigned char daemon)
	{
	register int i;
	static int nextpid = 0;
	register struct process_s **process_pp, *process_p, *other_process_p;
	struct object_s *object_p;
	struct arena_s *stack_arena_p;
	void *temp;
	unsigned long page;

#if DEBUG >= 3
	_dprintf(3, _("process_fork(0x%x, %u) starting\n"), parent_process_p,
			(unsigned int)daemon);
#endif
	process_p = process_new();
	if (process_p == NULL)
		{
		goto error1;
		}

	stack_arena_p = arena_allocate(1); /* make page to appear at 0xe000 */
	if (stack_arena_p == NULL)
		{
		process_deref(process_p);
	error1:
#if DEBUG >= 3
		_dprintf(3, _("process_fork() returning NULL, "
				"error\n"));
#endif
		return NULL;
		}
	stack_set(stack_arena_p, (union store *)KRNL_BA_START,
			(union store *)KRNL_CA1_START - 1);
	stack_set(stack_arena_p, (union store *)KRNL_CA1_START - 1,
			(union store *)(KRNL_BA_START + 1));

	memcpy(process_p, parent_process_p, sizeof(struct process_s));
	process_p->refs = 1; /* process_new() also did this, fix up */
	process_p->parent_p = daemon ? NULL : parent_process_p;
	process_p->stack_arena_p = stack_arena_p;

/* abyte('x'); */
	while (1) /* loop until we find a unique pid */
		{
		/* i_state = dirps(); */
		nextpid++;
		if (nextpid >= 30000)
			{
			nextpid = 1;
			}
		process_p->p_pid = nextpid; /* publish our proposed new pid */
		/* restore(i_state); */

		for (process_pp = process_list;
				process_pp < process_list + PROCESS_LIST;
				process_pp++)
			{
			/* i_state = dirps(); */
			other_process_p = *process_pp;
			if (other_process_p && other_process_p != process_p &&
					other_process_p->p_pid == process_p->p_pid)
				{
				/* restore(i_state); */
				goto retry;
				}
			/* restore(i_state); */
			}

		break; /* the proposed pid was unique */

	retry:
		;
		}

/* abyte('y'); */
	process_p->p_time = rtc_time(); /* record start time for process */

	process_p->p_utime = 0;
	process_p->p_stime = 0;
	process_p->p_cutime = 0;
	process_p->p_cstime = 0; /* clear tick counters */
	process_p->p_pending = 0; /* don't process signals twice */

/* abyte('z'); */
	if (process_p->p_root)
		{
		i_ref(process_p->p_root); /* clone process's root */
		}

	if (process_p->p_cwd)
		{
		i_ref(process_p->p_cwd); /* clone process's cwd */
		}

	for (i = 0; i < USER_STREAM_LIST; i++)
		{
		object_p = process_p->stream_p[i];
		if (object_p)
			{
			object_inc_refs(object_p); /* clone an open stream */
			}
		}

	for (i = 0; i < USER_OBJECT_LIST; i++)
		{
		object_p = process_p->object_p[i];
		if (object_p)
			{
			object_inc_refs(object_p); /* clone an open fd */
			}
		}

#ifdef JOB_CONTROL
	if (process_p->session_p)
		{
		session_ref(process_p->session_p); /* clone session */
		}
#endif

	if (process_p->arena_p)
		{
		arena_ref(process_p->arena_p); /* clone arena ala vfork */
		}

	process_p->status = P_RUNNING;
#if DEBUG >= 3
	_dprintf(3, _("process_fork() returning 0x%x, success\n"),
			process_p);
#endif
	return process_p;
	}
#endif

#ifdef MODULE_process_find
struct process_s *
process_find(jmp_buf error, unsigned int pid)
	{
	register struct process_s **process_pp, *process_p;
	/* int i_state; */

#if DEBUG >= 3
	_dprintf(3, _("process_find(0x%x, %u) starting\n"), error, pid);
#endif
	for (process_pp = process_list; process_pp < process_list + PROCESS_LIST;
			process_pp++)
		{
		/* i_state = dirps(); */
		process_p = *process_pp;
		if (process_p->p_pid == pid)
			{
			if (process_p->p_uid ==
					my_thread_p->process_p->p_uid ||
					super())
				{
				/* restore(i_state); */
#if DEBUG >= 3
				_dprintf(3, _("process_find() "
						"returning 0x%x, success\n"),
						process_p);
#endif
				return process_p;
				}
			else
				{
				/* restore(i_state); */
#if DEBUG >= 3
				_dprintf(3, _("process_find() error EPERM\n"));
#endif
				longjmp(error, EPERM);
				}
			}
		/* restore(i_state); */
		}

#if DEBUG >= 3
	_dprintf(3, _("process_find() error EINVAL"));
#endif
	longjmp(error, EINVAL);
	}
#endif

#ifdef MODULE_process_garbage
void
process_garbage(int red)
	{
	register struct process_s **process_pp, *process_p;

	for (process_pp = process_list; process_pp < process_list + PROCESS_LIST;
			process_pp++)
		{
		process_p = *process_pp;
		if (process_p && process_p->status == P_EMPTY)
			{
			*process_pp = NULL;
			free(process_p);
			}
		}
	}
#endif

/* Special system call returns copies of UZIX
 * structures for system dependent utils like df, ps... */

#ifdef MODULE_process_getfsys
void
process_getfsys(jmp_buf error, dev_t devno, void *data)
	{
	void *temp;
	size_t count;
	struct kdata_s kbuf;

	switch (devno)
		{
	case GI_FTAB:
		count = sizeof(file_system_list);
		temp = file_system_list;
		break;
	case GI_ITAB:
		count = sizeof(core_inode_list);
		temp = core_inode_list;
		break;
	case GI_BTAB:
		count = sizeof(block_buffer_list);
		temp = block_buffer_list;
		break;
	case GI_PTAB:
		count = sizeof(process_list);
		temp = process_list;
		break;
	case GI_UTAB:
		count = sizeof(struct process_s);
		temp = my_thread_p->process_p;
		break;
#if 0 /* FIX THIS! */
	case GI_PDAT:
		return pdat(ubuf);
#endif

	case GI_KDAT:
		/* note: can't kwait() while using kbuf!! */
		memset(&kbuf, 0, sizeof(struct kdata_s));

		strncpy(kbuf.k_name, UZIX, 13);
		strncpy(kbuf.k_version, VERSION, 7);
		strncpy(kbuf.k_release, RELEASE, 7);

		strncpy(kbuf.k_host, HOST, 13);
		strncpy(kbuf.k_machine, MACHINE, 7);

		kbuf.k_tmem = 256;	/* 256k arena */
		kbuf.k_kmem = 60;	/* 60k kernel */

		count = sizeof(struct kdata_s);
		temp = &kbuf;
		break;

	default:
		temp = findfs(devno);
		if (temp == NULL)
			{
			longjmp(error, ENXIO);
			}
		count = sizeof(struct file_system_s);
		}

	valadr(error, data, count);
	usrput(data, temp, count);
	}
#endif

#ifdef MODULE_process_times
void
process_times(jmp_buf error, struct tms *buf)
	{
	valadr(error, buf, sizeof(struct tms));

	/* should use new usrput() routine here */
	disable();
	usrput(buf, &my_thread_p->process_p->p_utime,
			4 * sizeof(unsigned long));
	usrput(&buf->tms_etime, &Clock, sizeof(unsigned long));
	enable();
	}
#endif

#ifdef MODULE_process_brk
void
process_brk(jmp_buf error, void *addr)
	{
	struct arena_s *arena_p;
	struct process_s *process_p;
	unsigned char size;

	/* cache this thread's process as we'll need it a few times */
	process_p = my_thread_p->process_p;

	/* calculate how many 4 kbyte pages needed, excluding basepage */
	size = ((unsigned int)addr - 1) >> PAGE_LOG;
	if (size == 0 || size > 0xe)
		{
		longjmp(error, ENOMEM); /* can only have 0x1001 thru 0xf000 */
		}

	/* resize the program's arena, if space happens to be available */
	arena_p = process_p->arena_p;
	if (arena_p->size != size && arena_resize(arena_p, size) == 0)
		{
		/* arena_p = arena_allocate(size); */
		/* if (arena_p == NULL) */
		/*	{ */
			longjmp(error, ENOMEM);
		/*	} */
		/* arena_deref(process_p->arena_p); */
		/* process_p->arena_p = arena_p; */
		}
	my_thread_p->cbar_value = (size << 4) + 0x11;

	process_p->p_break = (unsigned char *)addr;
	}
#endif

#ifdef MODULE_process_sbrk
void *
process_sbrk(jmp_buf error, size_t incr)
	{
	unsigned char *oldbrk, *newbrk;

	oldbrk = my_thread_p->process_p->p_break;
	if (incr)
		{
		newbrk = oldbrk + incr;
		if (newbrk < oldbrk)
			{
			longjmp(error, ENOMEM);
			}
		process_brk(error, (void *)newbrk);
		}
	return (void *)oldbrk;
	}
#endif

#ifdef MODULE_process_waitpid
unsigned int
process_waitpid(jmp_buf error, unsigned int pid, unsigned int *statloc,
		unsigned int flag)
	{
	struct process_s **process_pp, *process_p;
	/* int i_state; */
	unsigned int retval;

/* abyte('{'); */
	valadr(error, statloc, sizeof(int));

	/* See if we have any children. */
	/* disable(); */
	for (process_pp = process_list; process_pp < process_list + PROCESS_LIST; process_pp++)
		{
		process_p = *process_pp;
		if (process_p && process_p->status != (unsigned char)P_EMPTY &&
				process_p->parent_p == my_thread_p->process_p &&
				process_p != my_thread_p->process_p)
			{
			/* enable(); */
			goto found;
			}
		}
	/* enable(); */

	longjmp(error, ECHILD);

found:
	while (1)
		{
/* abyte('|'); */
		/* Search for an exited child */
		/* disable(); */
		for (process_pp = process_list;
				process_pp < process_list + PROCESS_LIST;
				process_pp++)
			{
			/* i_state = dirps(); */
			process_p = *process_pp;
			if (process_p && process_p->parent_p ==
					my_thread_p->process_p &&
					 /* sweet child o' mine! */
					process_p->status ==
					(unsigned char)P_ZOMBIE)
				{
/* abyte('}'); */
				retval = process_p->p_pid;

			    	/* if (pid < -1)...
			    	   wait for any child whose group ID is equal
			    	   to the absolute value of PID
			    	   - NOT IMPLEMENTED -

			    	   if (pid == 0)...
			    	   wait for any child whose group ID is equal
			    	   to the group of the calling process
			    	   - NOT IMPLEMENTED -

			    	   if option == WUNTRACED...
			    	   - NOT IMPLEMENTED -
			    	*/
			    	if (pid != WAIT_ANY && retval != pid)
					{
					continue;
					}

				if (statloc)
					{
					usrput_unsigned_int(statloc,
							process_p->p_exitval);
					}

				/* free process_list now! */
				process_p->status = P_EMPTY;

				/* Add in child's time info.
				 * It was stored in p_zombie[] in the child's
				 * process table entry
				 */
				disable();
				addtick(&my_thread_p->process_p->p_cutime,
						process_p->p_zombie);
				addtick(&my_thread_p->process_p->p_cstime,
						process_p->p_zombie + 1);
				enable();
				return retval;
				}
			/* restore(i_state); */
			}
		/* enable(); */

		if (flag & WNOHANG)
			{
			longjmp(error, ECHILD);
			}

		/* Nothing yet, so wait */
/* abyte('#'); */
		if (kwait(my_thread_p->process_p))	/* P_WAIT */
			{
			/* awakened by signal */
			longjmp(error, EINTR);
			}
/* abyte('$'); */
		}
	}
#endif

#ifdef MODULE_process__exit
void
process__exit(unsigned int val)
	{
#if DEBUG >= 2
	_dprintf(2, _("process %d exiting\n"), my_thread_p->process_p->p_pid);
#endif

/* abyte('('); */
	my_thread_p->process_p->p_exitval = (val<<8) /*| (val2 & 0xff)*/;

#if 0
	/* close all files and sync now, rather than waiting for timerproc() */
	fclose(stdin);
	stdin = NULL;

	fclose(stdout);
	stdout = NULL;

	fclose(stderr);
	stdout = NULL;

	process_deref(my_thread_p->process_p);
	my_thread_p->process_p = NULL;
#endif

/* abyte(')'); */
	killself();
	}
#endif

#ifdef MODULE_process_pause
void
process_pause(jmp_buf error)
	{
	/* please look up this function in susv3 and revisit */
	kwait(NULL);
	longjmp(error, EINTR);
	}
#endif

#ifdef MODULE_process_signal
unsigned long
process_signal(jmp_buf error, signal_t sig, unsigned long func)
	{
	unsigned long retval = -1;
#if 1 /* fixes IAR internal error */
	signal_t mysig;
	void (*myfunc)(signal_t);
	void (**myvec)(signal_t);
#endif

	/* SIGKILL & SIGSTOP can't be caught !!!*/
	if (sig == __NOTASIGNAL || sig == SIGKILL || ((unsigned)sig) > NSIGS)
		{
		longjmp(error, EINVAL);
		}
	disable();
	if (func == SIG_IGN)
		{
		my_thread_p->process_p->p_ignored |= sigmask(sig);
		}
	else
		{
#if 0 /* Nick removed this for IAR compiler... now back in again... OUT!! */
		if ((sig_t)func != SIG_DFL && ((char *)func) < PROGBASE)
			{
			enable();
			longjmp(error, EFAULT);
			}
#endif
		my_thread_p->process_p->p_ignored &= ~(sigmask(sig));
		}

	retval = (long)my_thread_p->process_p->p_sigvec[sig-1];
#if 1 /* fixes IAR internal error */
	mysig = sig;
	myvec = my_thread_p->process_p->p_sigvec + (mysig - 1);
	myfunc = (void (*)(signal_t))func;
	*myvec = myfunc;
#else
	my_thread_p->process_p->p_sigvec[sig-1] = (void (*)(signal_t))func;
#endif
	enable();
	return retval;
	}
#endif

#ifdef MODULE_process_kill
void
process_kill(jmp_buf error, unsigned int pid, signal_t sig)
	{
	if (sig == __NOTASIGNAL || (unsigned char)sig >= NSIGS || pid <= 1)
		{
		longjmp(error, EINVAL);
		}

#if 0 /* FIX THIS! */
	sendsig(process_find(error, pid), sig);
#endif
	}
#endif

#ifdef MODULE_process_alarm
unsigned int
process_alarm(unsigned int secs)
	{
	unsigned int retval;

	disable();
	retval = my_thread_p->process_p->p_alarm;
	my_thread_p->process_p->p_alarm = secs;
	enable();
	return retval;
	}
#endif

#ifdef MODULE_process_getset
unsigned int
process_getset(jmp_buf error, unsigned int what, unsigned int parm1,
		unsigned int parm2)
	{
	unsigned int retval;
	struct process_s *process_p;
	signed char nice;

	switch (what)
		{
	case GET_PID:
		return my_thread_p->process_p->p_pid;
	case GET_PPID:
		return my_thread_p->process_p->parent_p->p_pid;
	case GET_UID:
		return my_thread_p->process_p->p_uid;
	case SET_UID:
		if (super() || my_thread_p->process_p->p_uid == parm1)
			{
			my_thread_p->process_p->p_uid = parm1;
			my_thread_p->process_p->p_euid = parm1;
			return 0;
			}
		break;
	case GET_EUID:
		return my_thread_p->process_p->p_euid;
	case GET_GID:
		return my_thread_p->process_p->p_gid;
	case SET_PRIO:
		/* parm1 = PID, parm2 = nice value */
		process_p = process_find(error, parm1);
#if 1
		nice = max_int(min_int((int)parm2, PRIO_MAX), PRIO_MIN);
#else
		nice = min_int((int)parm2, PRIO_MAX);
		if (nice < PRIO_MIN) nice = PRIO_MIN;
#endif
		if (!super())
			{
			if (nice < 0)
				{
				nice = 0;
				}
			}
		process_p->p_nice = nice;
		process_p->p_cprio = (nice < 1) ?
				(((-nice)*(TICKSPERSEC - MAXTICKS)) /
					(- PRIO_MIN)) + MAXTICKS :
				1 + (((PRIO_MAX - nice) * MAXTICKS) /
					PRIO_MAX);
		return 0;
	case GET_EGID:
		return my_thread_p->process_p->p_egid;
	case GET_PRIO:
		return my_thread_p->process_p->p_cprio; /* must be p_prio */
	case SET_GID:
		if (super() || my_thread_p->process_p->p_gid == parm1)
			{
			my_thread_p->process_p->p_gid = parm1;
			my_thread_p->process_p->p_egid = parm1;
			return 0;
			}
		break;
	case SET_UMASK:
		retval = my_thread_p->process_p->p_umask;
		my_thread_p->process_p->p_umask = parm1 & S_UMASK;
		return retval;
	case SET_TRACE:
		retval = my_thread_p->process_p->p_traceme;
		my_thread_p->process_p->p_traceme = parm1;
		return retval;
	case SET_DEBUG:
#if 1 /* see kernel/dprintf.c and intl/intl.c */
		retval = Debug_level;
		Debug_level = parm1;
#else
		retval = my_thread_p->process_p->p_debugme;
		my_thread_p->process_p->p_debugme = parm1;
#endif
		return retval;
		}
	longjmp(error, EPERM);
	}
#endif

#ifdef MODULE_process_reboot
void
process_reboot(jmp_buf error, unsigned int parm1, unsigned int parm2)
	{
	if (super() == 0)
		{
		longjmp(error, EPERM);
		}

	if (parm1 != 'm' || parm2 != 'e')
		{
		longjmp(error, EINVAL);
		}

	doshutdown(0, NULL, NULL);
	}
#endif

#ifdef MODULE_process_fclose
/* stdio.c
 * Copyright (C) 1996 Robert de Bath <rdebath@cix.compulink.co.uk>
 * This file is part of the Linux-8086 C library and is distributed
 * under the GNU Library General Public License.
 */

/* This is an implementation of the C standard IO package. */

extern FILE *__IO_list; 	/* For fflush at exit */

int process_fclose(struct process_s *process_p, struct object_s *object_p)
{
	int rv = 0;
#if 1 /* tracking per-process streams using my_thread_p->process_p->stream_p */
	struct object_s **base_stream_pp, **limit_stream_pp;
	struct object_s **stream_pp;
#endif

	if (object_p == NULL) {
		errno = EINVAL;
		return EOF;
	}
#if 0 /* this makes it process_fclose() */
	if (fflush((FILE *)object_p))
		return EOF;
	if (close(((FILE *)object_p)->fd))
		rv = EOF;
#endif

#if 1 /* streams within the kernel are shared by children of process_fork() */
	if (object_dec_refs(object_p))
		{
		/* was last reference, OLD:object is still acquired, destroy it */
		/* object_release(object_p); */
#endif
		((FILE *)object_p)->fd = -1;
		if (((FILE *)object_p)->mode & __MODE_FREEBUF) {
			free(((FILE *)object_p)->bufstart);
			((FILE *)object_p)->mode &= ~__MODE_FREEBUF;
			((FILE *)object_p)->bufstart = ((FILE *)object_p)->bufend = 0;
		}
		if (((FILE *)object_p)->mode & __MODE_FREEFIL) {
			FILE *ptr = __IO_list, *prev = 0;

			((FILE *)object_p)->mode = 0;
			while (ptr && ptr != ((FILE *)object_p))
				ptr = ptr->next;
			if (ptr == ((FILE *)object_p)) {
				if (prev == 0)
					__IO_list = ((FILE *)object_p)->next;
				else	prev->next = ((FILE *)object_p)->next;
			}
			free((FILE *)object_p);
		}
		else	((FILE *)object_p)->mode = 0;
#if 1 /* streams within the kernel are shared by children of process_fork() */
		}
#endif

#if 1 /* tracking per-process streams using process_p->stream_p */
	base_stream_pp = process_p->stream_p;
	limit_stream_pp = base_stream_pp + USER_STREAM_LIST;

	for (stream_pp = base_stream_pp; stream_pp < limit_stream_pp;
			stream_pp++)
		{
		if (*stream_pp == object_p)
			{
			*stream_pp = NULL;
			goto found;
			}
		}
	_panic(_("process_fclose() not in list"));

found:
#endif
	return rv;
}
#endif

#ifdef MODULE_process_thread_create
/* Create a new, ready process and return pointer to descriptor.
 * The general registers are not initialized, but optional args are pushed
 * on the stack so they can be seen by a C function.
 */
struct thread_s *
process_thread_create(
struct process_s *process_p, /* process whose resources thread will share */
unsigned char *name,	/* Arbitrary user-assigned name string */
unsigned int stksize,	/* Stack size in words to allocate */
void (*pc)(int argc, void *argv, void *p),	/* Initial execution address */
int iarg,		/* Integer argument (argc) */
void *parg1,		/* Generic pointer argument #1 (argv) */
void *parg2,		/* Generic pointer argument #2 (session ptr) */
unsigned char freeargs	/* If set, free arg list on parg1 at termination */
)
	{
	struct thread_s *thread_p;
	int i;
#if SIGNAL_QUEUE == 0
	int i_state;
#endif
#if 1
	struct arena_s *stack_arena_p;
#endif

#if DEBUG >= 1
	_dprintf(1, _("process_thread_create(0x%x, \"%s\", 0x%x, 0x%lx, "
			"%u, 0x%x, 0x%x, %u) starting\n"),
			process_p, name, stksize, (unsigned long)pc,
			iarg, parg1, parg2, (unsigned int)freeargs);
#endif
#if DEBUG >= 4
	stksize <<= 1; /* VERY TEMPORARY EXTRA STACK FOR USE WITH DPRINTF() */
#endif
#if STACK_GUARD /* much more comprehensive stack check */
	stksize += STACK_GUARD;
#endif

	/* Create process descriptor */
/* abyte('!'); */
	thread_p = thread_new();
	if (thread_p == NULL)
		{
		goto error1;
		}
/* abyte('@'); */
	thread_p->process_p = process_p;
	process_ref(process_p);

/* abyte('#'); */
	/* Create name */
	thread_p->name = strdup(name);
	if (thread_p->name == NULL)
		{
		goto error2;
		}

/* abyte('$'); */
	/* Allocate stack */
	thread_p->stksize = stksize;

#if 1
	stack_arena_p = process_p->stack_arena_p;
	thread_p->stack = (uint16 *)stack_malloc(stack_arena_p,
			sizeof(uint16)*stksize);
/* abyte('%'); */
	if (thread_p->stack == NULL)
		{
		stack_arena_p = arena_allocate(1); /* grab a fresh page */
		if (stack_arena_p == NULL)
			{
			goto error2;
			}
		stack_set(stack_arena_p, (union store *)KRNL_BA_START,
				(union store *)KRNL_CA1_START - 1);
		stack_set(stack_arena_p, (union store *)KRNL_CA1_START - 1,
				(union store *)(KRNL_BA_START + 1));

		arena_deref(process_p->stack_arena_p);
		process_p->stack_arena_p = stack_arena_p;

		thread_p->stack = (uint16 *)stack_malloc(stack_arena_p,
				sizeof(uint16)*stksize);
		if (thread_p->stack == NULL)
			{
		error2:
			thread_delete(thread_p);
		error1:
#if DEBUG >= 1
			_dprintf(1, _("process_thread_create() returning NULL\n"));
#endif
			return NULL;
			}
		}

/* abyte('^'); */
	arena_ref(stack_arena_p);
	thread_p->stack_arena_p = stack_arena_p;
#else
	thread_p->stack = (uint16 *)malloc(sizeof(uint16)*stksize);
	if (thread_p->stack == NULL)
		{
	error2:
		thread_delete(thread_p);
	error1:
#if DEBUG >= 1
		_dprintf(1, _("process_thread_create() returning NULL\n"));
#endif
		return NULL;
		}
#endif
	thread_p->stack_limit = (unsigned int)(thread_p->stack + stksize);

/* abyte('&'); */
	/* Initialize stack for high-water check */
	for (i=0; i<stksize; i++)
		{
#if 1
		stack_set(process_p->stack_arena_p,
				(union store *)thread_p->stack + i,
				(union store *)STACK_MAGIC);
#else
		thread_p->stack[i] = STACK_MAGIC;
#endif
		}

/* abyte('*'); */
	thread_p->flags.freeargs = freeargs;
	thread_p->iarg = iarg;
	thread_p->parg1 = parg1;
	thread_p->parg2 = parg2;
	thread_p->startp = pc;

	/* Do machine-dependent initialization of stack */
/* abyte('('); */
	psetup(thread_p);
/* abyte(')'); */

	/* Add to ready process table */
#if 0 /* out for now */
	thread_p->flags.suspend =
#endif
	thread_p->flags.waiting = 0;
#if SIGNAL_QUEUE == 0
	i_state = dirps();
#endif
	addproc(thread_p);
#if SIGNAL_QUEUE == 0
	restore(i_state);
#endif
/* abyte('_'); */
#if DEBUG >= 1
	_dprintf(1, _("process_thread_create() returning 0x%x\n"), thread_p);
#endif
	return thread_p;
	}
#endif

