/* valadr.c by Nick for NOS/UZI project
 *
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 Process management routines
**********************************************************/

#include <errno.h>
#include "kernel/valadr.h"

#ifndef DEBUG
#define DEBUG 0
#endif

#ifndef MODULE
#define MODULE_valadr
#endif

#ifdef MODULE_valadr
void
valadr(jmp_buf error, void *base, size_t size)
{
#ifdef ZILOG
	unsigned char *limit;

	if ((unsigned char *)base < (unsigned char *)USER_START)
		{
		longjmp(error, EFAULT);
		}

	limit = (unsigned char *)base + size;
	if (limit < (unsigned char *)base ||
			limit > (unsigned char *)USER_LIMIT)
		{
		longjmp(error, EFAULT);
		}
#endif
	}
#endif

