/* Non pre-empting synchronization kernel
 * Copyright 1992 Phil Karn, KA9Q
 */
#include <stdio.h>
#include <setjmp.h>
#include "asm.h"
#include "nos/global.h"
#include "main/mbuf.h"
#include "main/timer.h"
#include "socket/socket.h"
#include "main/daemon.h"
#include "nos/hardware.h"
/*#include "x86/display.h"*/
#include "main/main.h" /* for daemon_process_p */
#include "kernel/dprintf.h"
#include "kernel/unix.h" /* for unix_tick, unix_ticks */
#include "main/logmsg.h"
#include "kernel/stack.h" /* for stack_get() */
#include <libintl.h>
#include "po/messages.h"

#define	SIGNAL_QUEUE 0 /*64*/		/* Entries in ksignal queue */

struct thread_s *my_thread_p;		/* Currently running thread */
struct thread_s *Rdytab;		/* Processes ready to run */
struct thread_s *Waittab[SIGNAL_HASH];	/* Waiting process list */
#if 0 /* out for now */
struct thread_s *Susptab;		/* Suspended processes */
#endif

struct mbuf_s *Killq;

#if SIGNAL_QUEUE
struct sigentry
	{
	void *event;
	int n;
	};

struct ksig
	{
	struct sigentry entry[SIGNAL_QUEUE];
	struct sigentry *wp;
	struct sigentry *rp;
	volatile int nentries;	/* modified both by interrupts and main */
	int maxentries;
	int32 duksigs;
	int32 lostsigs;
	int32 ksigs;		/* Count of ksignal calls */
	int32 ksigwakes;	/* Processes woken */
	int32 ksignops;		/* ksignal calls that didn't wake anything */
	int32 ksigsqueued;	/* ksignal calls queued with ints off */
	int32 kwaits;		/* Count of kwait calls */
	int32 kwaitnops;	/* kwait calls that didn't block */
#if 0
	int32 kwaitints;	/* kwait calls from interrupt context (error) */
#endif
	};

static struct ksig Ksig;
#endif

#if 0 /* out for now */
int Kdebug;		/* Control display of current task on screen */
unsigned short oldNull;
#endif

static int chkintstk(void);
static int stkutil(struct thread_s *thread_p);

#if SIGNAL_QUEUE
static void procsigs(void);
#endif

#if 1
extern uint16 __stack_base[KERNEL_STACK >> 1];
#endif

void
kinit(void)
	{
	int i;

#if 1
	/* Initialize interrupt stack for high-water-mark checking */
	for (i = 0; i < (KERNEL_STACK >> 1); i++)
		{
		__stack_base[i] = STACK_MAGIC;
		}
#else
	/* Initialize interrupt stack for high-water-mark checking */
	for (i = 0; i < (INTERRUPT_STACK >> 1); i++)
		{
		Intstk[i] = STACK_MAGIC;
		}
#endif

#if 0 /* out for now */
	/* Remember location 0 pattern to detect null pointer derefs */
	oldNull = *(unsigned short *)NULL;
#endif

#if SIGNAL_QUEUE
	/* Initialize signal queue */
	Ksig.wp = Ksig.rp = Ksig.entry;
#endif
	}

/* Print process table info
 */
int
ps(argc, argv, p)
int argc;
char *argv[];
void *p;
	{
	register struct thread_s **thread_pp, *thread_p;
	jmp_buf *ep;

	_printf(_("Uptime %s intstk %u\n"), tformat(secclock()), chkintstk());

#if SIGNAL_QUEUE
	_printf(_("ksigs %lu queued %lu hiwat %u\n"
			"woken %lu nops %lu dups %lu lost %lu\n"
			"kwaits %lu nops %lu\n"),
			Ksig.ksigs, Ksig.ksigsqueued, Ksig.maxentries,
			Ksig.ksigwakes, Ksig.ksignops, Ksig.duksigs,
			Ksig.lostsigs, Ksig.kwaits, Ksig.kwaitnops);
	Ksig.maxentries = 0;
#endif

	_printf(_("  PID  arena  sessp stack usage level event fl   name\n"));
	for (thread_pp = thread_list;
			thread_pp < thread_list + THREAD_LIST; thread_pp++)
		{
		/* i_state = dirps(); */
		thread_p = *thread_pp;
		/* restore(i_state); */
		if (thread_p && thread_p->flags.active)
			{
			ep = &thread_p->env;
			_printf(_("%5d  %02x:%02x  %04x  %04x  %04x  "
					"%04x  %04x  %c%c  %s\n"),
					thread_p->process_p->p_pid,
					thread_p->bbr_value,
					thread_p->cbar_value,
					thread_p->process_p->session_p,
					thread_p->stksize, stkutil(thread_p),
					(unsigned int *)thread_p->stack_limit -
					(unsigned int *)((*ep)->j_sp),
					thread_p->event,
					(unsigned int)(thread_p->
							flags.istate ?
							'I' : ' '),
					(unsigned int)(thread_p->
							flags.waiting ?
							'W' : ' '),
#if 0 /* out for now */
					(unsigned int)(thread_p->
							flags.suspend ?
							'S' : ' '),
#endif
					thread_p->name);
			}
		}

	return 0;
}

static int
stkutil(thread_p)
struct thread_s *thread_p;
	{
	unsigned int i;
	uint16 *sp;
	struct arena_s *arena_p;

	i = thread_p->stksize;

	arena_p = thread_p->stack_arena_p;
	for (sp = thread_p->stack;
			sp < (thread_p->stack + thread_p->stksize) &&
			stack_get(arena_p, (union store *)sp) ==
			(union store *)STACK_MAGIC; sp++)
		{
		i--;
		}

	return i;
	}

/* Return number of used words in interrupt stack */
static int
chkintstk()
	{
	register int i;
	register uint16 *cp;

#if 1
	i = KERNEL_STACK >> 1;
	for (cp = __stack_base; i && *cp == STACK_MAGIC; cp++)
		{
		i--;
		}
#else
	i = INTERRUPT_STACK >> 1;
	for (cp = Intstk; i && *cp == STACK_MAGIC; cp++)
		{
		i--;
		}
#endif

	return i;
	}

/* Machine-dependent initialization of a task */
void
psetup(thread_p)
struct thread_s *thread_p;	/* Pointer to task structure */
	{
	jmp_buf *ep;

	if (setjmp(thread_p->env))
		{
		/* can't use any local variables now, we're on the new stack */
		my_thread_p->startp(my_thread_p->iarg,
				my_thread_p->parg1, my_thread_p->parg2);
		killself();
		}

	ep = &thread_p->env;
	(*ep)->j_sp = (unsigned int)(thread_p->stack + thread_p->stksize) -
			sizeof(jmp_buf);

	/* Task initially runs with interrupts on */
	thread_p->flags.istate = 1;
	}

/* Free resources allocated to specified process. If a process wants to kill
 * itself, the reaper is called to do the dirty work. This avoids some
 * messy situations that would otherwise occur, like freeing your own stack.
 */
void
killproc(struct thread_s *thread_p)
	{
	char **argv;

#if DEBUG >= 1
	_dprintf(1, _("killproc(0x%x) starting\n"), thread_p);
#endif
	if (thread_p == NULL)
		{
#if DEBUG >= 1
		_dprintf(1, _("killproc() returning, null\n"), thread_p);
#endif
		return;
		}

	/* Don't check the stack here! Will cause infinite recursion if
	 * called from a stack error
	 */
	if (thread_p == my_thread_p)
		{
		killself();	/* Doesn't return */
		}

	/* Stop alarm clock in case it's running */
	stop_timer(&thread_p->alarm);

	/* Alert everyone waiting for this proc to die */
	ksignal(thread_p, 0);

	/* Remove from appropriate table */
	delproc(thread_p);

	/* Free allocated memory resources */
	if (thread_p->flags.freeargs)
		{
		argv = thread_p->parg1;
		while (thread_p->iarg-- != 0)
			{
			free(*argv++);
			}
		free(thread_p->parg1);
		}

	thread_delete(thread_p);
#if DEBUG >= 1
	_dprintf(1, _("killproc() returning, ok\n"), thread_p);
#endif
	}

/* Terminate current process by sending a request to the killer process.
 * Automatically called when a process function returns. Does not return.
 */
void
killself(void)
	{
	struct mbuf_s *bp;

#if DEBUG >= 1
	_dprintf(1, _("killself() starting\n"));
#endif
	my_thread_p->sysio = 1;
	while ((bp = qdata(&my_thread_p, sizeof(my_thread_p))) == NULL)
		kwait(NULL);
	append(&Killq, &bp);
	/*bp = NULL;
	pushdown(&bp, &my_thread_p, sizeof(my_thread_p));
	enqueue(&Killq, &bp);*/

	/* "Wait for me; I will be merciful and quick." */
	for (;;)
		{
		kwait((void *)-1); /*kwait(NULL);*/
		}
	}

#if 0 /* out for now */
/* Inhibit a process from running */
void
suspend(struct thread_s *thread_p)
	{
#if SIGNAL_QUEUE == 0
	int i_state;
#endif

	if (thread_p == NULL)
		{
		return;
		}
	if (thread_p == my_thread_p)
		{
		thread_p->flags.suspend = 1;
		kwait(NULL);
		}
	else
		{
#if SIGNAL_QUEUE == 0
		i_state = dirps();
#endif
		delproc(thread_p);	/* Running process isn't on any list */
		thread_p->flags.suspend = 1;
		addproc(thread_p);
#if SIGNAL_QUEUE == 0
		restore(i_state);
#endif
		}
	}

/* Restart suspended process */
void
resume(struct thread_s *thread_p)
	{
#if SIGNAL_QUEUE == 0
	int i_state;
#endif

	if (thread_p == NULL)
		{
		return;
		}
#if SIGNAL_QUEUE == 0
	i_state = dirps();
#endif
	delproc(thread_p);	/* Can't be my_thread_p! */
	thread_p->flags.suspend = 0;
	addproc(thread_p);
#if SIGNAL_QUEUE == 0
	restore(i_state);
#endif
	}
#endif

/* Wakeup waiting process, regardless of event it's waiting for. The process
 * will see a return value of "val" from its kwait() call. Must not be
 * called from an interrupt handler.
 */
void
alert(struct thread_s *thread_p, int val)
	{
#if SIGNAL_QUEUE == 0
	int i_state;
#endif

	if (thread_p == NULL)
		{
		return;
		}
	if (thread_p == my_thread_p)
		{
		/*thread_p->flags.waiting = 0;*/
		thread_p->retval = val;
		thread_p->event = NULL;
		}
	else
		{
#if SIGNAL_QUEUE == 0
		i_state = dirps();
#endif
		delproc(thread_p);
		thread_p->flags.waiting = 0;
		thread_p->retval = val;
		thread_p->event = NULL;
		addproc(thread_p);
#if SIGNAL_QUEUE == 0
		restore(i_state);
#endif
		}
	}

/*void allock(void);*/

/* Post a wait on a specified event and give up the CPU until it happens. The
 * null event is special: it means "I don't want to block on an event, but let
 * somebody else run for a while". It can also mean that the present process
 * is terminating; in this case the wait never returns.
 *
 * Pwait() returns 0 if the event was signaled; otherwise it returns the
 * arg in an alert() call. Pwait must not be called from interrupt level.
 *
 * Before waiting and after giving up the CPU, kwait() processes the signal
 * queue containing events signaled when interrupts were off. This means
 * the process queues are no longer modified by interrupt handlers,
 * so it is no longer necessary to run with interrupts disabled here. This
 * greatly improves interrupt latencies.
 */
int
kwait(void *event)
	{
	register struct thread_s *old_thread_p;
	int tmp;
	int i_state;
#if STACK_GUARD /* much more comprehensive stack check */
	register struct thread_s **thread_pp, *thread_p;
	unsigned int *sp, *sp_top;
	/* int i_state; */

	for (thread_pp = thread_list;
			thread_pp < thread_list + THREAD_LIST; thread_pp++)
		{
		/* i_state = dirps(); */
		thread_p = *thread_pp;
		/* restore(i_state); */
		if (thread_p && thread_p->flags.active)
			{
			sp = (unsigned int *)thread_p->stack;
			sp_top = sp + STACK_GUARD;
			while (sp < sp_top)
				{
				if (*sp++ != STACK_MAGIC)
					{
					dprintf(0, "%s stack clobbered in %s\n",
							thread_p->name,
							my_thread_p->name);
/* sp--; */
/* ahexw((unsigned int)thread_p->stack); */
/* abyte(' '); */
/* ahexw((unsigned int)sp); */
/* abyte(' '); */
/* ahexw(*sp); */
/* abyte(' '); */
 sp = (unsigned int *)thread_p->stack;
 sp_top = sp + thread_p->stksize;
 while (sp < sp_top)
  {
  if (*sp++ == STACK_MAGIC)
   {
   abyte('.');
   }
  else
   {
   abyte('*');
   }
  }
					while (1)
						;
					}
				}
			}
		}
#endif
#if 0 /*DEBUG >= 1*/
	allock();
#endif
#if 0 /* handy for troubleshooting lost wakeups */
	if (event && *my_thread_p->name == '/')
		{
		dprintf(0, "\n%02x:%04x kwait(0x%04x) %s ",
				((unsigned char *)&event)[8],
				((unsigned int *)&event)[5],
				event, my_thread_p->name);
		}
#endif
#if DEBUG >= 6
	_dprintf(6, _("\n%02x:%04x kwait(0x%04x) starting "),
			((unsigned char *)&event)[4],
			((unsigned int *)&event)[3],
			(unsigned int)event);
#endif
#if SIGNAL_QUEUE
	Ksig.kwaits++;
#endif

	/* Enable interrupts, after saving the current state.
	 * This minimizes interrupt latency since we may have a lot
	 * of work to do. This seems safe, since care has been taken
	 * here to ensure that signals from interrupt level are not lost, e.g.,
	 * if we're waiting on an event, we post it before we scan the
	 * signal queue.
	 */
	i_state = dirps();
	my_thread_p->process_p->p_stime += unix_tick;
	unix_ticks = 0;
	unix_tick = 0;
#if SIGNAL_QUEUE
	enable();
#endif

	if (event)
		{
		/* Post a wait for the specified event */
		my_thread_p->event = event;
		my_thread_p->flags.waiting = 1;
		addproc(my_thread_p);	/* Put us on the wait list */
		}

#if SIGNAL_QUEUE
	/* If the signal queue contains a signal for the event that we're
	 * waiting for, this will wake us back up
	 */
	procsigs();
#endif
	if (event == NULL)
		{
		/* We remain runnable */
		if (Rdytab == NULL)
			{
			/* Nothing else is ready, so just return */
			restore(i_state);
#if SIGNAL_QUEUE
			Ksig.kwaitnops++;
#endif
			/* NOTE: at this point we haven't checked for ^C etc */
			return 0;
			}

		addproc(my_thread_p); /* Put us on the end of the ready list */
		}

	/* Look for a ready process and run it. If there are none,
	 * loop or halt until an interrupt makes something ready.
	 */
	while (Rdytab == NULL)
		{
#if SIGNAL_QUEUE == 0
		enable();
#endif

		/* Give system back to upper-level multitasker, if any.
		 * Note that this function enables interrupts internally
		 * to prevent deadlock, but it restores our state
		 * before returning.
		 */
#if DEBUG >= 1
		if (Debug_level >= 1)
			{
			abyte('.');
			}
#endif

		giveup();

#if SIGNAL_QUEUE
		/* Process signals that occurred during the giveup() */
		procsigs();
#else
		disable();
#endif
		}

	/* Remove first entry from ready list */
	old_thread_p = my_thread_p;
	my_thread_p = Rdytab;
	delproc(my_thread_p);

#if SIGNAL_QUEUE == 0
	enable();
#endif

#if DEBUG >= 1
	if (Debug_level >= 1)
		{
		acrlf();
		amess(my_thread_p->name);
		abyte(' ');
		}
#endif

	/* Now do the context switch.
	 * This technique was inspired by Rob, PE1CHL, and is a bit tricky.
	 *
	 * First save the current process's state. Then if
	 * this is still the old process, load the new environment. Since the
	 * new task will "think" it's returning from the setjmp() with a return
	 * value of 1, the comparison with 0 will bypass the longjmp(), which
	 * would otherwise cause an infinite loop.
	 */

	/* Save old state */
	old_thread_p->flags.istate = i_state;

	if (setjmp(old_thread_p->env) == 0)
		{
		/* We're still running in the old task; load new task context.
		 * The interrupt state is restored here in case longjmp
		 * doesn't do it (e.g., systems other than Turbo-C).
		 */
		restore(my_thread_p->flags.istate);
		longjmp(my_thread_p->env, 1);
		}

	/* At this point, we're running in the newly dispatched task */
	tmp = my_thread_p->retval;
	my_thread_p->retval = 0;

	/* Also restore the true interrupt state here, in case the longjmp
	 * DOES restore the interrupt state saved at the time of the setjmp().
	 * This is the case with Turbo-C's setjmp/longjmp.
	 */
	restore(my_thread_p->flags.istate);

	/* If an exception signal was sent and we're prepared, take it */
	if ((my_thread_p->flags.sset) && tmp == my_thread_p->signo)
		{
#if DEBUG >= 6
		_dprintf(6, _("\n%02x:%04x kwait(0x%04x) aborting %d "),
				((unsigned char *)&event)[4],
				((unsigned int *)&event)[3],
				(unsigned int)event, tmp);
#endif
		longjmp(my_thread_p->sig, 1);
		}

	/* Otherwise return normally to the new task */
#if DEBUG >= 6
	_dprintf(6, _("\n%02x:%04x kwait(0x%04x) returning %d "),
			((unsigned char *)&event)[4],
			((unsigned int *)&event)[3],
			(unsigned int)event, tmp);
#endif
	return tmp;
	}

void
ksignal(void *event, int n)
{
#if SIGNAL_QUEUE
	register struct sigentry *p;
	register int i;

	if (event == NULL)
		{
		return;		/* Null events are invalid */
		}

	if (istate())
		{
		/* Interrupts are on, just call ksig directly after
		 * processing the previously queued signals
		 */
		procsigs();
		ksig(event, n);
		return;
		}

	/* Interrupts are off, so quickly queue event */
	Ksig.ksigsqueued++;

 	/* Ignore duplicate signals to protect against a mad device driver
	 * overflowing the signal queue
	 */
	p = Ksig.wp;
	for (i = 0; i < Ksig.nentries; i++)
		{
		if (p <= Ksig.entry)
			{
			p = Ksig.entry + SIGNAL_QUEUE;
			}
		p--;
		if (p->event == event && p->n == n)
			{
			Ksig.duksigs++;
			return;
			}
		}

	if (Ksig.nentries == SIGNAL_QUEUE)
		{
		/* It's hard to handle this gracefully */
		Ksig.lostsigs++;
		return;
		}

	Ksig.wp->event = event;
	Ksig.wp->n = n;
	if (++Ksig.wp >= &Ksig.entry[SIGNAL_QUEUE])
		{
		Ksig.wp = Ksig.entry;
		}
	Ksig.nentries++;
#else
	int i_state;

	if (event == NULL)
		{
		return;		/* Null events are invalid */
		}

	i_state = dirps();
	ksig(event, n);
	restore(i_state);
#endif
	}

#if SIGNAL_QUEUE
static void
procsigs(void)
	{
	int cnt;
	int tmp;
	int i_state;

	i_state = dirps();
	cnt = Ksig.nentries;
	restore(i_state);

	if (cnt > Ksig.maxentries)
		{
		Ksig.maxentries = cnt;	/* Record high water mark */
		}

	for (tmp = 0; tmp < cnt; tmp++)
		{
		ksig(Ksig.rp->event, Ksig.rp->n);
		if (++Ksig.rp >= &Ksig.entry[SIGNAL_QUEUE])
			Ksig.rp = Ksig.entry;
		}

	i_state = dirps();
	Ksig.nentries -= cnt;
	restore(i_state);
	}
#endif

#if 0 /* not here anymore, see thread.c */
/* Make ready the first 'n' processes waiting for a given event. The ready
 * processes will see a return value of 0 from kwait().  Note that they don't
 * actually get control until we explicitly give up the CPU ourselves through
 * a kwait(). ksig is now called from pwait, which is never called at
 * interrupt time, so it is no longer necessary to protect the proc queues
 * against interrupts. This also helps interrupt latencies considerably.
 */
void
ksig(
void *event,	/* Event to signal */
int n		/* Max number of processes to wake up */
)
	{
	register struct thread_s *thread_p;
	struct thread_s *next_thread_p;
	unsigned int hashval;
#if SIGNAL_QUEUE
	int cnt = 0;

	Ksig.ksigs++;
#endif
	if (event == NULL)
		{
#if SIGNAL_QUEUE
		Ksig.ksignops++;
#endif
		return;		/* Null events are invalid */
		}

	hashval = phash(event);
	for (thread_p = Waittab[hashval]; thread_p; thread_p = next_thread_p)
		{
		next_thread_p = thread_p->next;
		if (thread_p->event == event)
			{
			delproc(thread_p);
			thread_p->flags.waiting = 0;
			thread_p->retval = 0;
			thread_p->event = NULL;
			addproc(thread_p);
			if (n)
				{
#if SIGNAL_QUEUE
				Ksig.ksigwakes++;
#endif
				return;
				}
#if SIGNAL_QUEUE
			cnt++;
#endif
			}
		}

#if 0 /* out for now */
	for (thread_p = Susptab; thread_p; thread_p = next_thread_p)
		{
		next_thread_p = thread_p->next;
		if (thread_p->event == event)
			{
			delproc(thread_p);
			thread_p->flags.waiting = 0;
			thread_p->event = 0;
			thread_p->retval = 0;
			addproc(thread_p);
			if (n)
				{
#if SIGNAL_QUEUE
				Ksig.ksigwakes++;
#endif
				return;
				}
#if SIGNAL_QUEUE
			cnt++;
#endif
			}
		}
#endif

#if SIGNAL_QUEUE
	if (cnt == 0)
		{
		Ksig.ksignops++;
		}
	else
		{
		Ksig.ksigwakes += cnt;
		}
#endif
	}
#endif

/* Rename a process */
void
chname(struct thread_s *thread_p, char *newname)
	{
	free(thread_p->name);
	thread_p->name = strdupw(newname);
	}

#if 0 /* not here anymore, see thread.c */
/* Remove a process entry from the appropriate table */
void
delproc(struct thread_s *thread_p)	/* Pointer to entry */
	{
	if (thread_p == NULL)
		{
		return;
		}
	if (thread_p->next)
		{
		thread_p->next->prev = thread_p->prev;
		}
	if (thread_p->prev)
		{
		thread_p->prev->next = thread_p->next;
		}
	else
		{
#if 0 /* out for now */
		if (thread_p->flags.suspend)
			{
			Susptab = thread_p->next;
			}
		else
#endif
		if (thread_p->flags.waiting)
			{
			Waittab[phash(thread_p->event)] = thread_p->next;
			}
		else
			{
			Rdytab = thread_p->next; /* Ready */
			}
		}
	}

void
addproc(struct thread_s *thread_p)	/* Pointer to entry */
	{
	register struct thread_s **thread_pp, *other_thread_p;

	if (thread_p == NULL)
		{
		return;
		}
#if 0 /* out for now */
	if (thread_p->flags.suspend)
		{
		thread_pp = &Susptab;
		}
	else
#endif
	if (thread_p->flags.waiting)
		{
		thread_pp = &Waittab[phash(thread_p->event)];
		}
	else
		{
		thread_pp = &Rdytab; /* Ready */
		}
	thread_p->next = NULL;
	if (*thread_pp == NULL)
		{
		/* Empty list, stick at beginning */
		thread_p->prev = NULL;
		*thread_pp = thread_p;
		}
	else
		{
		/* Find last thread_p on list */
		for (other_thread_p = *thread_pp; other_thread_p->next;
				other_thread_p = other_thread_p->next)
			;
		other_thread_p->next = thread_p;
		thread_p->prev = other_thread_p;
		}
	}
#endif

