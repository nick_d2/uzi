/* arena.c by Nick for NOS/UZI project
 *
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 Kernel arena management routines (similar to far malloc)
**********************************************************/

#include <errno.h>
#include <stdlib.h>
#include "kernel/dprintf.h"
#include "kernel/arena.h"
#include <libintl.h>
#include "po/messages.h"

#ifndef DEBUG
#define DEBUG 0
#endif

#ifndef MODULE
#define MODULE_arena_list
#define MODULE_head_arena_p
#define MODULE_arena_new
#define MODULE_arena_delete
#define MODULE_arena_ref
#define MODULE_arena_deref
#define MODULE_arena_allocate
#define MODULE_arena_resize
#define MODULE_arena_garbage
#endif

#ifdef MODULE_arena_list
struct arena_s *arena_list[ARENA_LIST];
#endif

#ifdef MODULE_head_arena_p
struct arena_s *head_arena_p;
#endif

#ifdef MODULE_arena_new
struct arena_s *
arena_new(void)
	{
	register struct arena_s **arena_pp, *arena_p;
	/* int i_state; */

#if DEBUG >= 3
	_dprintf(3, _("arena_new() starting\n"));
#endif
	for (arena_pp = arena_list; arena_pp < arena_list + ARENA_LIST;
			arena_pp++)
		{
		/* i_state = dirps(); */
		arena_p = *arena_pp;
		if (arena_p && arena_p->refs == (unsigned char)0)
			{
			goto found;
			}
		/* restore(i_state); */
		}

	arena_p = malloc(sizeof(struct arena_s));
	if (arena_p == NULL)
		{
		goto error;
		}

	for (arena_pp = arena_list; arena_pp < arena_list + ARENA_LIST;
			arena_pp++)
		{
		/* i_state = dirps(); */
		if (*arena_pp == NULL)
			{
			*arena_pp = arena_p;
			goto found;
			}
		/* restore(i_state); */
		}

	free(arena_p);
error:
#if DEBUG >= 3
	_dprintf(3, _("arena_new() returning NULL, error\n"));
#endif
	return NULL;

found:
	memset(arena_p, 0, sizeof(struct arena_s));
	arena_p->refs = 1;

	/* restore(i_state); */
#if DEBUG >= 3
	_dprintf(3, _("arena_new() returning 0x%x, success\n"), arena_p);
#endif
	return arena_p;
	}
#endif

#ifdef MODULE_arena_delete
void
arena_delete(struct arena_s *arena_p)
	{
	register struct arena_s **arena_pp, *other_arena_p;

#if DEBUG >= 3
 _dprintf(3, _("arena_delete(0x%x) starting\n"), arena_p);
#endif
	/* find the specified arena entry in the contiguous list */
	arena_pp = &head_arena_p; /* head of contiguous list */
	while (1)
		{
		/* i_state = dirps(); */
		other_arena_p = *arena_pp;
		if (other_arena_p == arena_p)
			{
			/* found it, remove from list, freeing the block */
			*arena_pp = arena_p->next;
			break;
			}
		if (arena_p == NULL)
			{
			/* it's not in the list, didn't have a block yet */
			break;
			}
		arena_pp = &other_arena_p->next;
		/* restore(i_state); */
		}

	/* restore(i_state); */
#if DEBUG >= 3
 _dprintf(3, _("arena_delete() returning, success\n"));
#endif
	}
#endif

#ifdef MODULE_arena_ref
void
arena_ref(struct arena_s *arena_p)
	{
	/* int i_state; */

#if DEBUG >= 3
	_dprintf(3, _("arena_ref(0x%x) starting\n"), arena_p);
#endif
	/* i_state = dirps(); */
	/* assert(arena_p->refs != (unsigned char)0xff); */
	arena_p->refs++;
	/* restore(i_state); */
#if DEBUG >= 3
	_dprintf(3, _("arena_ref() returning, refs %u\n"),
			(unsigned int)arena_p->refs);
#endif
	}
#endif

#ifdef MODULE_arena_deref
void
arena_deref(struct arena_s *arena_p)
	{
	/* int i_state; */

#if DEBUG >= 3
	_dprintf(3, _("arena_deref(0x%x) starting\n"), arena_p);
#endif
	/* i_state = dirps(); */
	/* assert(arena_p->refs != (unsigned char)0); */
	if (--(arena_p->refs) == (unsigned char)0)
		{
		arena_delete(arena_p);
		}
	/* restore(i_state); */
#if DEBUG >= 3
	_dprintf(3, _("arena_deref() returning, refs %u\n"),
			(unsigned int)arena_p->refs);
#endif
	}
#endif

#ifdef MODULE_arena_allocate
struct arena_s *
arena_allocate(unsigned char size)
	{
	register struct arena_s **arena_pp, *arena_p, *other_arena_p;
	unsigned char used;
	/* int i_state; */

#if DEBUG >= 3
	_dprintf(3, _("arena_allocate(0x%x) starting\n"), size);
#endif
	arena_p = arena_new();
	if (arena_p == NULL)
		{
		goto error;
		}

	/* walk the contiguous list, looking for a memory "hole" */
	arena_pp = &head_arena_p; /* head of contiguous list */
	used = 0; /* the page after the highest page used so far */
	while (1)
		{
		/* i_state = dirps(); */

		/* set up one entry of lookahead in the linked list */
		other_arena_p = *arena_pp;

		/* can we squeeze it in before the lookahead entry? */
		/* if no more entries, pretend 64, means 256 kbytes */
		if ((used + size) <= (other_arena_p ? other_arena_p->page : 64))
			{
			break; /* the arena_p fits - wear it */
			}

		/* if now at end of list, we don't have enough memory */
		if (other_arena_p == NULL)
			{
			/* restore(i_state); */
		error:
#if DEBUG >= 3
			_dprintf(3, _("arena_allocate() "
					"returning NULL, error\n"));
#endif
			return NULL;
			}

		/* make lookahead entry current, then look ahead again */
		used = other_arena_p->page + other_arena_p->size;
		arena_pp = &other_arena_p->next;

		/* restore(i_state); */
		}

	/* ready to insert the new arena entry into the list */
	arena_p->page = used;
	arena_p->size = size;
	arena_p->next = other_arena_p;
	*arena_pp = arena_p;

	/* restore(i_state); */
#if DEBUG >= 3
	_dprintf(3, _("arena_allocate() returning 0x%x, success\n"),
			arena_p);
#endif
	return arena_p;
	}
#endif

#ifdef MODULE_arena_resize
int
arena_resize(struct arena_s *arena_p, unsigned char size)
	{
	register struct arena_s *other_arena_p;
	/* int i_state; */

#if DEBUG >= 3
	_dprintf(3, _("arena_resize(0x%x, 0x%x) starting\n"),
			arena_p, size);
#endif

	/* i_state = dirps(); */

	/* set up one entry of lookahead in the linked list */
	other_arena_p = arena_p->next;

	/* can we squeeze it in before the lookahead entry? */
	/* if no more entries, pretend 64, means 256 kbytes */
	if ((arena_p->page + size) <= (other_arena_p ? other_arena_p->page : 64))
		{
		/* grow or shrink the block to the requested size */
		arena_p->size = size;

		/* restore(i_state); */
#if DEBUG >= 3
		_dprintf(3, _("arena_resize() returning 1, success\n"));
#endif
		return 1; /* indicates success */
		}

	/* restore(i_state); */
#if DEBUG >= 3
	_dprintf(3, _("arena_resize() returning 0, error\n"));
#endif
	return 0;
	}
#endif

#ifdef MODULE_arena_garbage
void
arena_garbage(int red)
	{
	register struct arena_s **arena_pp, *arena_p;

	for (arena_pp = arena_list; arena_pp < arena_list + ARENA_LIST;
			arena_pp++)
		{
		arena_p = *arena_pp;
		if (arena_p && arena_p->refs == 0)
			{
			*arena_pp = NULL;
			free(arena_p);
			}
		}
	}
#endif

