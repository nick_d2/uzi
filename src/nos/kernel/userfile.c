/*
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 File descriptor related routines
**********************************************************/

#include <errno.h>
#include <fcntl.h> /* for O_RDWR etc */
#include "filesys/cinode.h" /* for readwritei() */
#include "driver/device.h"
#include "kernel/dprintf.h"
#include "filesys/openfile.h" /* for of_deref() */
#include "socket/opensock.h" /* for os_deref() */
#include "kernel/userfile.h"
#include "kernel/valadr.h"
#include <libintl.h>
#include "po/messages.h"

#ifndef DEBUG
#define DEBUG 0
#endif

#ifndef MODULE
#define MODULE_uf_alloc
#define MODULE_uf_locate
#define MODULE_uf_close
#define MODULE_uf_read
#define MODULE_uf_write
#define MODULE_uf_fstat
#define MODULE_uf_dup
#define MODULE_uf_dup2
#define MODULE_uf_ioctl
#endif

/* uf_alloc() finds an unused slot in the user file table.
 */

#ifdef MODULE_uf_alloc
unsigned int
uf_alloc(jmp_buf error)
	{
	struct object_s **base_object_pp, **limit_object_pp;
	struct object_s **object_pp;

#if DEBUG >= 3
	_dprintf(3, _("uf_alloc(0x%x) starting\n"), error);
#endif
	base_object_pp = my_thread_p->process_p->object_p;
	limit_object_pp = base_object_pp + USER_OBJECT_LIST;

	for (object_pp = base_object_pp; object_pp < limit_object_pp;
			object_pp++)
		{
		if (*object_pp == NULL)
			{
#if DEBUG >= 3
			_dprintf(3, _("uf_alloc() returning %u\n"),
					object_pp - base_object_pp);
#endif
			return object_pp - base_object_pp;
			}
		}

#if DEBUG >= 3
	_dprintf(3, _("uf_alloc() error EMFILE\n"));
#endif
	longjmp(error, EMFILE);
	}
#endif

/* uf_locate() returns the oft pointer associated with a user's
 * file descriptor, it may be either a socket or a file
 */

#ifdef MODULE_uf_locate
struct object_s *
uf_locate(jmp_buf error, unsigned int fd)
	{
	struct object_s *object_p;

#if DEBUG >= 3
	_dprintf(3, _("uf_locate(0x%x, %u) starting\n"), error, fd);
#endif
	if (fd >= USER_OBJECT_LIST)
		{
#if DEBUG >= 3
		_dprintf(3, _("uf_locate() error EBADF a\n"));
#endif
		longjmp(error, EBADF);
		}

	object_p = my_thread_p->process_p->object_p[fd];
	if (object_p == NULL)
		{
#if DEBUG >= 3
		_dprintf(3, _("uf_locate() error EBADF b\n"));
#endif
		longjmp(error, EBADF);
		}

	object_acquire(object_p);
#if DEBUG >= 3
	_dprintf(3, _("uf_locate() returning 0x%x\n"), object_p);
#endif
	return object_p;
	}
#endif

/* uf_close() given the file descriptor and close them */

#ifdef MODULE_uf_close
void
uf_close(jmp_buf error, unsigned int fd)
	{
	struct object_s *object_p;

#if DEBUG >= 3
	_dprintf(3, _("uf_close(0x%x, %u) starting\n"), error, fd);
#endif
	object_p = uf_locate(error, fd);
	my_thread_p->process_p->object_p[fd] = NULL;

	switch (object_p->type)
		{
	case _FL_SOCK:
		object_release(object_p); /* eliminate this later */
		os_deref((struct usock *)object_p);
		break;

	case _FL_FILE:
		object_release(object_p); /* eliminate this later */
		of_deref((struct open_file_s *)object_p);
		break;

	default:
		_panic(_("uf_close() unknown object"));
		}
#if DEBUG >= 3
	_dprintf(3, _("uf_close() returning\n"));
#endif
	}
#endif

#ifdef MODULE_uf_read
size_t
uf_read(jmp_buf error, unsigned int fd, void *data, size_t count)
	{
	jmp_buf try_b;
	unsigned int catch;
	struct object_s *object_p;
	struct core_inode_s *cinode_p;
	struct socklink *sp;

	object_p = uf_locate(error, fd);

	catch = setjmp(try_b);
	if (catch)
		{
		object_release(object_p);
		longjmp(error, catch);
		}

	switch (object_p->type)
		{
	case _FL_SOCK:
		/* lifted directly from the os_recv() routine (combine!!) */
		object_release(object_p);

		sp = ((struct usock *)object_p)->sp;

		/* Fail if recv routine isn't present */
		if (sp->recv == NULL){
			longjmp(error, EOPNOTSUPP);
		}

		count = (*sp->recv)(((struct usock *)object_p), data, count,
				NULL, NULL);
		if (count == -1)
			{
			longjmp(error, errno);
			}
		return count;

	case _FL_FILE:
		if (((struct open_file_s *)object_p)->o_access == O_WRONLY)
			{
			longjmp(try_b, EACCES);
			}

		cinode_p = ((struct open_file_s *)object_p)->o_inode;
		setftim(cinode_p, A_TIME);

		if (count)
			{
			if (S_ISCHR(cinode_p->c_node.i_mode) ||
					S_ISPIPE(cinode_p->c_node.i_mode))
				{
 /* NOTE! THIS IS VERY NON-IDEAL, IT WAS NEEDED TO AVOID A DEADLOCK PROBLEM */
				object_release(object_p);
				count = readwritei(error, 0,
						cinode_p, data, count,
						((struct open_file_s *)object_p)->o_ptr);
				object_acquire(object_p);
				}
			else
				{
				count = readwritei(try_b, 0,
						cinode_p, data, count,
						((struct open_file_s *)object_p)->o_ptr);
				}
			((struct open_file_s *)object_p)->o_ptr += count;
			}
		break;

	default:
		_panic(_("uf_read() unknown object"));
		}

	object_release(object_p);
	return count;
	}
#endif

#ifdef MODULE_uf_write
size_t
uf_write(jmp_buf error, unsigned int fd, void *data, size_t count)
	{
	jmp_buf try_b;
	unsigned int catch;
	struct object_s *object_p;
	struct core_inode_s *cinode_p;
	struct socklink *sp;
	struct sockaddr sock;
	unsigned int i;

	object_p = uf_locate(error, fd);

	catch = setjmp(try_b);
	if (catch)
		{
		object_release(object_p);
		longjmp(error, catch);
		}

	switch (object_p->type)
		{
	case _FL_SOCK:
		/* lifted directly from the os_send() routine (combine!!) */
		object_release(&((struct usock *)object_p)->object);

		i = MAXSOCKSIZE;
 /* need to fudge sysio here */
		os_getpeername(error, fd, &sock, &i);

		sp = ((struct usock *)object_p)->sp;

		/* Fail if send routine isn't present (shouldn't happen) */
		if (sp->send == NULL){
			longjmp(error, EOPNOTSUPP);
		}

		/* Remote address always supplied, check (maybe redundant) */
		if (sp->check && (*sp->check)(&sock, i) == -1){
			longjmp(error, EAFNOSUPPORT);
		}

		count = (*sp->send)(((struct usock *)object_p), data, count,
				&sock);
		if (count == -1)
			{
			longjmp(error, EOPNOTSUPP); /* maybe already got errno? */
			}
		return count;

	case _FL_FILE:
		if (((struct open_file_s *)object_p)->o_access == O_RDONLY)
			{
			longjmp(try_b, EACCES);
			}

		cinode_p = ((struct open_file_s *)object_p)->o_inode;
		setftim(cinode_p, A_TIME | M_TIME);

		if (count)
			{
			if (S_ISCHR(cinode_p->c_node.i_mode) ||
					S_ISPIPE(cinode_p->c_node.i_mode))
				{
 /* NOTE! THIS IS VERY NON-IDEAL, IT WAS NEEDED TO AVOID A DEADLOCK PROBLEM */
				object_release(object_p);
/* abyte('['); */
				count = readwritei(error, 1,
						cinode_p, data, count,
						((struct open_file_s *)object_p)->o_ptr);
/* abyte(']'); */
				object_acquire(object_p);
/* abyte('A'); */
				}
			else
				{
				count = readwritei(try_b, 1,
						cinode_p, data, count,
						((struct open_file_s *)object_p)->o_ptr);
				}
/* abyte('B'); */
			((struct open_file_s *)object_p)->o_ptr += count;
/* abyte('C'); */
			}
		break;

	default:
		_panic(_("uf_write() unknown object"));
		}

/* abyte('D'); */
	object_release(object_p);
/* abyte('E'); */
	return count;
	}
#endif

#ifdef MODULE_uf_fstat
void
uf_fstat(jmp_buf error, unsigned int fd, struct stat *buf)
	{
	struct open_file_s *oft_p;

	valadr(error, buf, sizeof(struct stat));
	oft_p = of_locate(error, fd);
	i_stat(oft_p->o_inode, buf);
	object_release(&oft_p->object);
	}
#endif

#ifdef MODULE_uf_dup
unsigned int
uf_dup(jmp_buf error, unsigned int fd)
	{
	struct object_s *object_p;
	unsigned int fd_new;

	object_p = uf_locate(error, fd);

	fd_new = uf_alloc(error);
	my_thread_p->process_p->object_p[fd_new] = object_p;

	object_release(object_p);
 /* NOTE: inc_refs should be changed so it doesn't acquire/release itself */
	object_inc_refs(object_p);

	return fd_new;
	}
#endif

#ifdef MODULE_uf_dup2
void
uf_dup2(jmp_buf error, unsigned int fd, unsigned int fd_new)
	{
	struct object_s *object_p;

	/* must check for same descriptor to avoid deadlock in oft_acquire */
	if (fd == fd_new)
		{
		return;
		}
	if (fd_new >= USER_OBJECT_LIST)
		{
		longjmp(error, EBADF);
		}

	object_p = uf_locate(error, fd);

	if (my_thread_p->process_p->object_p[fd_new])
		{
		uf_close(error, fd_new);
		}
	my_thread_p->process_p->object_p[fd_new] = object_p;

	object_release(object_p);
 /* NOTE: inc_refs should be changed so it doesn't acquire/release itself */
	object_inc_refs(object_p);
	}
#endif

#ifdef MODULE_uf_ioctl
void
uf_ioctl(jmp_buf error, unsigned int fd, unsigned int request, void *data)
	{
	jmp_buf try_b;
	unsigned int catch;
	dev_t dev;
	struct open_file_s *oft_p;
	struct core_inode_s *cinode_p;

	oft_p = of_locate(error, fd);

	cinode_p = oft_p->o_inode;
	i_acquire(cinode_p);

	catch = setjmp(try_b);
	if (catch)
		{
		i_release(cinode_p);
		object_release(&oft_p->object);
		longjmp(error, catch);
		}

	if (isdevice(cinode_p) == 0)
		{
		longjmp(try_b, ENODEV);
		}

	i_release(cinode_p);
	if ((getperm(cinode_p) & S_IOWRITE) == 0)
		{
		i_acquire(cinode_p); /* VERY DODGY! NEED TO CHANGE getperm() */
		longjmp(try_b, EPERM);
		}
	i_acquire(cinode_p);

	dev = DEVNUM(cinode_p); /* NEED TO AVOID device_find() CALL HERE!!!! */
	device_find(try_b, dev)->ioctl(try_b, MINOR(dev), request, data);

	i_release(cinode_p);
	object_release(&oft_p->object);
	}
#endif

