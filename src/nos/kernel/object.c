/* object.c by Nick for NOS/UZI project */

#include "kernel/dprintf.h"
#include "kernel/object.h" /* for struct object_s */
#include "kernel/thread.h" /* for kwait(), ksignal() */

#ifndef MODULE
#define MODULE_object_acquire
#define MODULE_object_release
#define MODULE_object_inc_refs
#define MODULE_object_dec_refs
#endif

#ifdef MODULE_object_acquire
void object_acquire(struct object_s *object_p)
	{
	/* unsigned char i_state; */

#if DEBUG >= 4
 _dprintf(4, _("object_acquire(0x%x) starting\n"), object_p);
#endif
	/* i_state = dirps(); */
 if (object_p->busy)
  {
  abyte('<');
  ahexw((unsigned int)&object_p->busy);
  abyte(':');
  ahexb(object_p->busy);
	while (object_p->busy)
		{
		kwait(&object_p->busy);
		}
  abyte('>');
  }
	object_p->busy = 1;
	/* restore(i_state); */
#if DEBUG >= 4
 _dprintf(4, _("object_acquire() returning\n"));
#endif
	}
#endif

#ifdef MODULE_object_release
void object_release(struct object_s *object_p)
	{
	/* unsigned char i_state; */

#if DEBUG >= 4
 _dprintf(4, _("object_release(0x%x) starting\n"), object_p);
#endif
	/* i_state = dirps(); */
	/* assert(object_p->busy); */
/* abyte('n'); */
	object_p->busy = 0;
/* abyte('o'); */
	ksignal(&object_p->busy, 0);
/* abyte('p'); */
	/* restore(i_state); */
#if DEBUG >= 4
 _dprintf(4, _("object_release() returning\n"));
#endif
	}
#endif

#ifdef MODULE_object_inc_refs
void
object_inc_refs(struct object_s *object_p)
{
	/* int i_state; */

#if DEBUG >= 4
 _dprintf(4, _("object_ref(0x%x) starting\n"), object_p);
#endif
	/* object_acquire(object_p); */
	/* assert(object_p->refs != (unsigned char)0xff); */
	/* i_state = dirps(); */
	object_p->refs++;
	/* restore(i_state); */
	/* object_release(object_p); */
#if DEBUG >= 4
 _dprintf(4, _("object_ref() returning, refs %u\n"), object_p->refs);
#endif
}
#endif

#ifdef MODULE_object_dec_refs
unsigned char
object_dec_refs(struct object_s *object_p)
{
	/* int i_state; */

#if DEBUG >= 4
 _dprintf(4, _("object_dec_refs(0x%x) starting\n"), object_p);
#endif
	/* object_acquire(object_p); */
	/* assert(object_p->refs); */
	/* i_state = dirps(); */
	if (--(object_p->refs) == 0)
		{
		/* restore(i_state); */
#if DEBUG >= 4
 _dprintf(4, _("object_dec_refs() returning, refs 0\n"));
#endif
		return 1; /* OLD:object still acquired and must be destroyed */
		}
	/* restore(i_state); */
	/* object_release(object_p); */
#if DEBUG >= 4
 _dprintf(4, _("object_dec_refs() returning, refs %u\n"), object_p->refs);
#endif
	return 0;
	}
#endif

