/* fork.c by Nick for NOS/UZI project
 *
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 Kernel process management routines
**********************************************************/

#include <errno.h>
#include "nos/global.h" /* for copyr() */
#include "kernel/fork.h"
#include "kernel/dprintf.h"
#include "kernel/arena.h"
#include "kernel/process.h"
#include "kernel/thread.h"
#include "kernel/usrmem.h"
#include "kernel/unix.h" /* for unix_ret() */
#include <libintl.h>
#include "po/messages.h"

#ifndef DEBUG
#define DEBUG 0
#endif

#ifdef MODULE
#define STATIC
#else
#define STATIC static
#define MODULE_fork_parent
#define MODULE_fork_thread
#endif

STATIC void fork_thread(int argc, void *argv, void *p);

#ifdef MODULE_fork_parent
void
fork_parent(jmp_buf error, unsigned int *pid_p)
	{
	struct arena_s *arena_p;
	struct process_s *process_p;
	struct thread_s *thread_p;

	arena_p = arena_allocate(my_thread_p->process_p->arena_p->size);
	if (arena_p == NULL)
		{
		longjmp(error, ENOMEM);
		}

	process_p = process_fork(my_thread_p->process_p, 0);
	if (process_p == NULL)
		{
		goto error1;
		}

	thread_p = process_thread_create(process_p, my_thread_p->name, 768,
			fork_thread, 0, NULL, pid_p, 0);
	process_deref(process_p);
	if (thread_p == NULL)
		{
	error1:
		arena_deref(arena_p);
		longjmp(error, EAGAIN);
		}

	/* note: we can't kwait() until we've prepared the thread to run */
	arena_deref(process_p->arena_p);
	process_p->arena_p = arena_p;

	thread_p->c_sp = my_thread_p->c_sp;
	thread_p->c_cs = my_thread_p->c_cs;
	thread_p->bbr_value = arena_p->page - 1;
	thread_p->cbar_value = my_thread_p->cbar_value;

	copyr((unsigned long)
			my_thread_p->process_p->arena_p->page << PAGE_LOG,
			(unsigned long)arena_p->page << PAGE_LOG,
			(unsigned int)arena_p->size << PAGE_LOG);

	/* poke parent's return value from fork() */
	usrput_unsigned_int(pid_p, process_p->p_pid);
	}
#endif

#ifdef MODULE_fork_thread
STATIC void
fork_thread(int argc, void *argv, void *p)
	{
	my_thread_p->sysio = 0;

/* DO SOMETHING ABOUT SYSTRACE HERE! */

	/* poke child's return value from fork() */
	usrput_unsigned_int((unsigned int *)p, 0);

	/* return child's errno, 0 means success */
	unix_ret(0);
	}
#endif

