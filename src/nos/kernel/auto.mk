# auto.mk by Nick for Hytech NOS/UZI project

# -----------------------------------------------------------------------------

SUBDIRS=

OUTPUTS=	kernel.$(LIBEXT)

kernel_$(LIBEXT)_SOURCES= \
		arena.c dprintf.c fork.c kernel.c object.c thread.c process.c \
		strace.c unix.c userfile.c usrmem.c valadr.c ksig.S \
		addproc.S delproc.S stack.c stack_get.S stack_set.S
#session.c

dprintf_c_MODULES= \
		panic _panic dprintf vdprintf dputter

arena_c_MODULES= \
		arena_list head_arena_p arena_new \
		arena_delete arena_ref arena_deref \
		arena_allocate arena_resize arena_garbage

process_c_MODULES= \
		process_list process_new process_delete process_ref \
		process_deref process_find process_fork process_garbage \
		process_getfsys process_times process_brk process_sbrk \
		process_waitpid process__exit process_pause process_signal \
		process_kill process_alarm process_getset process_reboot \
		process_fclose process_thread_create

session_c_MODULES= \
		Notval Badsess Sestypes session_list session_new \
		session_delete session_ref session_deref session_garbage \
		sessptr dosession go doclose doreset dokick \
		newsession freesession dorecord doupload upload
#dosfsize keywait sesflush

thread_c_MODULES= \
		thread_list thread_new thread_delete thread_garbage
#ksig delproc addproc

object_c_MODULES= \
		object_acquire object_release object_inc_refs object_dec_refs

strace_c_MODULES= \
		strace_list strace_error strace_entry strace_exit strace_bomb \
		strace_dump_memory strace_unsigned_int strace_unsigned_long

unix_c_MODULES=	unix_tick unix_ticks \
		__unix __unix_sync __unix_utime __unix_link __unix_symlink \
		__unix_unlink __unix_chdir __unix_chroot __unix_mknod \
		__unix_access __unix_chmod __unix_chown __unix_stat \
		__unix_mount __unix_umount __unix_execve __unix_open \
		__unix_close __unix_read __unix_write __unix_lseek \
		__unix_fstat __unix_falign __unix_dup __unix_dup2 \
		__unix_ioctl __unix_pipe __unix_time __unix_stime \
		__unix_getfsys __unix_times __unix_brk __unix_sbrk \
		__unix_waitpid __unix__exit __unix_pause __unix_signal \
		__unix_kill __unix_alarm __unix_getset __unix_reboot \
		__unix_fork __unix_socket __unix_bind __unix_listen \
		__unix_connect __unix_accept __unix_shutdown \
		__unix_socketpair __unix_recv __unix_recvfrom __unix_send \
		__unix_sendto __unix_getsockname __unix_getpeername \
		__unix___vector

userfile_c_MODULES= \
		uf_alloc uf_locate uf_close uf_read uf_write uf_fstat uf_dup \
		uf_dup2 uf_ioctl

usrmem_c_MODULES= \
		usrget usrget_string usrget_unsigned_int usrget_unsigned_char \
		usrget_unsigned_long usrput usrput_unsigned_int \
		usrput_unsigned_char usrput_unsigned_long

valadr_c_MODULES= \
		valadr

fork_c_MODULES=	fork_parent fork_thread

stack_c_MODULES= \
		stack_allock stack_malloc stack_free stack_realloc
#stack_get stack_set

# -----------------------------------------------------------------------------

