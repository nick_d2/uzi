; stack_set.S

#include "z80/asm.h"

	NAME	build/z80/banked/stack_set(17)
	EXTERN	?BANK_FAST_LEAVE_L08

	rseg	CODE

	public	_stack_set
_stack_set:
	ld	hl,4
	add	hl,sp
	ld	a,(hl)
	inc	hl
	ld	h,(hl)
	ld	l,a

	inc	de
	inc	de
	inc	de			; STRUCT_ARENA_page

	di
	ld	a,(de)
	sub	#KRNL_BA_START >> PAGE_LOG
	in0	e,(BBR)
	out0	(BBR),a

	ld	a,l
	ld	(bc),a
	inc	bc
	ld	a,h
	ld	(bc),a

	ei
	out0	(BBR),e
	JP	LWRD ?BANK_FAST_LEAVE_L08

	END
