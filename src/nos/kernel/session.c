/* session.c by Nick for NOS/UZI project
 *
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/* NOS User Session control
 * Copyright 1991 Phil Karn, KA9Q
 */

/**********************************************************
 Kernel session management routines
**********************************************************/

#include <errno.h>
#include <sgtty.h>
#include <stdio.h>
#include <stdlib.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include "kernel/thread.h"
#include "client/ftpcli.h"
#include "internet/icmp.h"
#include "client/telnet.h"
#include "nos/hardware.h"
#include "socket/socket.h"
#include "main/cmdparse.h"
#include "main/commands.h"
#include "main/main.h"
#ifdef JOB_CONTROL
#include "driver/devtty.h" /* for struct ttystate */
#endif
#include "kernel/dprintf.h"
#include <libintl.h>
#include "po/messages.h"

#ifndef DEBUG
#define DEBUG 0
#endif

#ifdef MODULE
#define STATIC
extern _char *Badsess;
#else
#define STATIC static
#define MODULE_Notval
#define MODULE_Badsess
#define MODULE_Sestypes
#define MODULE_session_list
#define MODULE_session_new
#define MODULE_session_delete
#define MODULE_session_ref
#define MODULE_session_deref
#define MODULE_session_garbage
#define MODULE_sessptr
#define MODULE_dosession
#define MODULE_go
#define MODULE_doclose
#define MODULE_doreset
#define MODULE_dokick
/*#define MODULE_dosfsize*/
#define MODULE_newsession
#define MODULE_freesession
#define MODULE_dorecord
#define MODULE_doupload
#define MODULE_upload
/*#define MODULE_keywait*/
/*#define MODULE_sesflush*/
#endif

#ifdef MODULE_Notval
_char *Notval = N_("Not a valid control block\n");
#endif
#ifdef MODULE_Badsess
STATIC _char *Badsess = N_("Invalid session\n");
#endif
#ifdef MODULE_Sestypes
_char *Sestypes[SESTYPES_MAX] = {
	N_(""),
	N_("Telnet"),	/* TELNET, */
	N_("FTP"),	/* FTP, */
	N_("AX25"),	/* AX25TNC, */
	N_("Finger"),	/* FINGER, */
	N_("Ping"),	/* PING, */
	N_("NET/ROM"),	/* NRSESSION, */
	N_("Command"),	/* COMMAND, */
	N_("More"),	/* VIEW, */
	N_("Hopcheck"),	/* HOP, */
	N_("Tip"),	/* TIP, */
	N_("PPP PAP"),	/* PPPPASS, */
	N_("Dial"),	/* DIAL, */
	N_("Query"),	/* DQUERY, */
	N_("Cache"),	/* DCLIST, */
	N_("Trace"),	/* ITRACE, */
	N_("Repeat"),	/* REPEAT, */
	N_("Fax"),	/* FAX, */
	N_("Spawn")	/* SPAWN */
};
#endif

#ifdef MODULE_session_list
struct session_s *session_list[SESSION_LIST];
#endif

#ifdef MODULE_session_new
struct session_s *
session_new(void)
	{
	register struct session_s **session_pp, *session_p;
	/* int i_state; */

#if DEBUG >= 3
	_dprintf(3, _("session_new() starting\n"));
#endif
	for (session_pp = session_list; session_pp < session_list + SESSION_LIST;
			session_pp++)
		{
		/* i_state = dirps(); */
		session_p = *session_pp;
		if (session_p && session_p->refs == (unsigned char)0)
			{
			goto found;
			}
		/* restore(i_state); */
		}

	session_p = malloc(sizeof(struct session_s));
	if (session_p == NULL)
		{
		goto error;
		}

	for (session_pp = session_list; session_pp < session_list + SESSION_LIST; session_pp++)
		{
		/* i_state = dirps(); */
		if (*session_pp == NULL)
			{
			*session_pp = session_p;
			goto found;
			}
		/* restore(i_state); */
		}

	free(session_p);
error:
#if DEBUG >= 3
	_dprintf(3, _("session_new() returning NULL, error\n"));
#endif
	return NULL;

found:
	memset(session_p, 0, sizeof(struct session_s));
	session_p->refs = 1;
	session_p->index = session_pp - session_list;

	/* restore(i_state); */
#if DEBUG >= 3
	_dprintf(3, _("session_new() returning 0x%x, success\n"), session_p);
#endif
	return session_p;
	}
#endif

#ifdef MODULE_session_delete
void
session_delete(struct session_s *session_p)
	{
	register struct session_s **session_pp, *other_session_p;
#ifdef JOB_CONTROL
	register struct tty_minor_s **tm_pp, *tm_p;
#endif

#if DEBUG >= 3
	_dprintf(3, _("session_delete(0x%x) starting\n"), session_p);
#endif

	for (session_pp = session_list; session_pp < session_list + SESSION_LIST;
			session_pp++)
		{
		/* i_state = dirps(); */
		other_session_p = *session_pp;
		if (other_session_p && other_session_p->refs != (unsigned char)0 &&
				other_session_p->parent == session_p)
			{
			other_session_p->parent = session_p->parent;
			}
		/* restore(i_state); */
		}

/* abyte('A'); */
#ifdef JOB_CONTROL
	for (tm_pp = tty_minor_list; tm_pp < tty_minor_list + TTY_MINOR_LIST;
			tm_pp++)
		{
		/* i_state = dirps(); */
		tm_p = *tm_pp;
		if (tm_p && tm_p->refs != (unsigned char)0)
			{
			if (tm_p->root_session_p == session_p)
				{
				tm_p->root_session_p = session_p->parent;
				}
			if (tm_p->last_session_p == session_p)
				{
				tm_p->last_session_p = session_p->parent;
				}
			if (tm_p->session_p == session_p)
				{
				tty_focus(tm_p, session_p->parent);
				}
			}
		/* restore(i_state); */
		}
#endif

/* abyte('B'); */
	if(session_p->proc1 != NULL)
		killproc(session_p->proc1);

/* abyte('C'); */
	if(session_p->proc2 != NULL)
		killproc(session_p->proc2);

/* abyte('D'); */
	/* NOTE: THIS ONLY WORKS FOR SOCKETS, FILES MUST USE nick_fclose() */
	/* THIS IS BECAUSE WE MIGHT NOW BE ANOTHER PROCESS & CAN'T close() */
	if(session_p->network != NULL)
		fclose(session_p->network);

/* abyte('E'); */
	if(session_p->record != NULL)
		fclose(session_p->record);

/* abyte('F'); */
	if(session_p->upload != NULL)
		fclose(session_p->upload);

/* abyte('G'); */
	free(session_p->name);

#if DEBUG >= 3
	_dprintf(3, _("session_delete() returning, success\n"));
#endif
	}
#endif

#ifdef MODULE_session_ref
void
session_ref(struct session_s *session_p)
	{
	/* int i_state; */

#if DEBUG >= 3
	_dprintf(3, _("session_ref(0x%x) starting\n"), session_p);
#endif
	/* i_state = dirps(); */
	/* assert(session_p->refs != (unsigned char)0xff); */
	session_p->refs++;
	/* restore(i_state); */
#if DEBUG >= 3
	_dprintf(3, _("session_ref() returning, refs %u\n"),
			(unsigned int)session_p->refs);
#endif
	}
#endif

#ifdef MODULE_session_deref
void
session_deref(struct session_s *session_p)
	{
	/* int i_state; */

#if DEBUG >= 3
	_dprintf(3, _("session_deref(0x%x) starting\n"), session_p);
#endif
	/* i_state = dirps(); */
	/* assert(session_p->refs != (unsigned char)0); */
	if (--(session_p->refs) == (unsigned char)0)
		{
		session_delete(session_p);
		}
	/* restore(i_state); */
#if DEBUG >= 3
	_dprintf(3, _("session_deref() returning, refs %u\n"),
			(unsigned int)session_p->refs);
#endif
	}
#endif

#ifdef MODULE_session_garbage
void
session_garbage(int red)
	{
	register struct session_s **session_pp, *session_p;

	for (session_pp = session_list; session_pp < session_list + SESSION_LIST;
			session_pp++)
		{
		session_p = *session_pp;
		if (session_p && session_p->refs == 0)
			{
			*session_pp = NULL;
			free(session_p);
			}
		}
	}
#endif

/* Convert a character string containing a decimal session index number
 * into a pointer. If the arg is NULL, use the current default session.
 * If the index is out of range or unused, return NULL.
 */

/* NOTE: THIS DOESN'T WORK AT THE MOMENT, SORRY */

#ifdef MODULE_sessptr
struct session_s *
sessptr(cp)
char *cp;
{
	unsigned int i;

/*	if(cp == NULL) */
/*		return Lastcurr; */

	i = (unsigned)atoi(cp);
	if(i >= SESSION_LIST)
		return NULL;
	else
		return session_list[i];
}
#endif

/* Select and display sessions */

#ifdef MODULE_dosession
int
dosession(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct session_s *sp;
	struct sockaddr fsocket;
	int i,k,s,ses;
	int t;
	char *cp;

	sp = (struct session_s *)p;

#if 0 /* FIX THIS!!!!!!!!!!! */
	if(argc > 1){
		if((sp = sessptr(argv[1])) != NULL){
			go(0,NULL,sp);
		} else
			_printf(_("Session %s not active\n"),argv[1]);
		return 0;
	}
#endif
	_printf(_(" #  S#  Snd-Q State     Remote socket         Command\n"));
	for(ses=0;ses<SESSION_LIST;ses++){
		sp = session_list[ses];
		if(sp == NULL || sp->refs == (unsigned char)0 || /* Nick */
				sp->type == COMMAND)
			continue;

		t = 0;
		cp = NULL;
		if(sp->network != NULL && (s = fileno(sp->network)) != -1){
			i = SOCKSIZE;
			k = getpeername(s,&fsocket,&i);
			t = socklen(s, 1); /*+= socklen(s,1);*/
			cp = sockstate(s);
		} else {
			k = s = -1;
			t = 0;
			cp = NULL;
		}
		_printf(_("%c"), ' '); /* FIX THIS!!!!! (Lastcurr == sp)? '*':' '); */
		_printf(_("%-3u"),sp->index);
		_printf(_("%-4d%5d %-10s"),s,t,(cp != NULL) ? cp : "");
		_printf(_("%-22s"),(k == 0) ? psocket(&fsocket) : "");
		if(sp->name != NULL)
			_printf(_("%s"),sp->name);
		_printf(_("\n"));

		/* Display FTP data channel, if any */
		if(sp->type == FTP && (s = fileno(sp->cb.ftp->data)) != -1){
			i = SOCKSIZE;
			k = getpeername(s,&fsocket,&i);
			t = socklen(s, 1); /*+= socklen(s,1);*/
			cp = sockstate(s);
			_printf(_("    %-4d%5d %-10s"),s,t,(cp != NULL) ? cp : "");
			_printf(_("%-22s\n"),(k == 0) ? psocket(&fsocket) : "");
		}
#ifndef ZILOG /* haven't got fpname() with CRT stdio */
		if(sp->record != NULL)
			_printf(_("    Record: %s\n"),fpname(sp->record));
		if(sp->upload != NULL)
			_printf(_("    Upload: %s\n"),fpname(sp->upload));
#endif
	}
	return 0;
}
#endif

/* Resume current session, and wait for it */
/* NOTE: THIS DOESN'T WORK AT THE MOMENT, SORRY */

#ifdef MODULE_go
int
go(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct session_s *sp;

	sp = (struct session_s *)p;
	if(sp == NULL || sp->type == COMMAND)
		return 0;
/*	if(my_session_p != Command) */
/*		Lastcurr = my_session_p; */
/*	kernel_session_focus(sp); */
	return 0;
}
#endif

#ifdef MODULE_doclose
int
doclose(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct session_s *sp;

	sp = (struct session_s *)p;
	if(argc > 1)
		sp = sessptr(argv[1]);

	if(sp == NULL){
		_printf(Badsess);
		return -1;
	}
	shutdown(fileno(sp->network),1);
	return 0;
}
#endif

#ifdef MODULE_doreset
int
doreset(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct session_s *sp;

	sp = (struct session_s *)p;
	if(argc > 1)
		sp = sessptr(argv[1]);

	if(sp == NULL){
		_printf(Badsess);
		return -1;
	}
	/* Unwedge anyone waiting for a domain resolution, etc */
	alert(sp->proc,EABORT);
	shutdown(fileno(sp->network),2);
	if(sp->type == FTP)
		shutdown(fileno(sp->cb.ftp->data),2);
	return 0;
}
#endif

#ifdef MODULE_dokick
int
dokick(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct session_s *sp;

	sp = (struct session_s *)p;
	if(argc > 1)
		sp = sessptr(argv[1]);

	if(sp == NULL){
		_printf(Badsess);
		return -1;
	}
	sockkick(fileno(sp->network));
	if(sp->type == FTP)
		sockkick(fileno(sp->cb.ftp->data));
	return 0;
}
#endif


#if 0 /*def MODULE_dosfsize*/
int
dosfsize(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return setlong(&Sfsize, _("Scroll file size"), argc, argv);
}
#endif


#ifdef MODULE_newsession
struct session_s *
newsession(name,type,makecur)
char *name;
int type;
int makecur;
{
	struct process_s *process_p;
#ifndef JOB_CONTROL /* bit of a hack */
	struct session_s *session_p;

	session_p = session_new();
	if (session_p == NULL)
		{
		_panic(_("newsession() fail"));
		}

	session_p->type = type;
	if (name)
		{
		session_p->name = strdupw(name);
		}
	session_p->proc = my_thread_p;

	process_p = my_thread_p->process_p;
	session_p->parent = process_p->session_p;

	if (process_p->session_p)
		{
		session_deref(process_p->session_p);
		}
	process_p->session_p = session_p;
	return session_p;
#else
	struct tioc_new_session tns;

	tns.jid = 0;
	tns.name = name;
	tns.type = type;
	tns.makecur = makecur;

	if (ioctl(STDIN_FILENO, TIOC_NEW_SESSION, &tns) == -1 || tns.jid == 0)
		{
		_panic(_("newsession() fail"));
		}

	process_p = my_thread_p->process_p;

	process_fclose(process_p, process_p->stream_p[0]); /* stdin */
	process_fclose(process_p, process_p->stream_p[1]); /* stdout */
	process_fclose(process_p, process_p->stream_p[2]); /* stderr */

	if (fdopen(0, "rb") == NULL ||
			fdopen(1, "wb") == NULL ||
			fdopen(2, "wb") == NULL ||
			setvbuf(stderr, NULL, _IONBF, 0) != 0)
		{
		_panic(_("newsession() stdio"));
		}

	return (struct session_s *)tns.jid;
#endif
}
#endif

#ifdef MODULE_freesession
void
freesession(sp, promptflag)
struct session_s *sp;
int promptflag; /* Nick */
{
	int i;

#ifndef ZILOG /* Nick */
	if (promptflag)
		{
		keywait(NULL, 1);
		}
#endif

	if(sp == NULL || sp != session_list[sp->index])
		return;	/* Not on session list */

	/* this is needed for ping.c so that a sequence such as: */
	/*	printf("unknown\n"); */
	/*	freesession(sp, 1); */
	/* can work, otherwise it delays the printing until after */
	/* main()'s session has been re-referenced in this routine */
	/* and then deadlocks because the session being freed just */
	/* now is not really dead until the thread exits as well */
	if (stdout)
		{
		fflush(stdout);
		}

	kwait(NULL);	/* Wait for any pending output to go */

	if (my_thread_p->process_p->session_p != sp)
		{
		_panic(_("freesession() mismatch"));
		}

#ifndef JOB_CONTROL /* bit of a hack */
	my_thread_p->process_p->session_p = sp->parent;

	if (sp->parent)
		{
		session_ref(sp->parent);
		}
	if (sp)
		{
		session_deref(sp);
		}
#else
	if (ioctl(STDIN_FILENO, TIOC_FREE_SESSION) == -1)
		{
		_panic(_("freesession() fail"));
		}
#endif
}
#endif

/* Control session recording */

#ifdef MODULE_dorecord
int
dorecord(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct session_s *sp;
	char *mode;

	sp = (struct session_s *)p;
	if(sp == NULL){
		_printf(_("No current session\n"));
		return 1;
	}
	if(argc > 1){
		if(sp->record != NULL){
			fclose(sp->record);
			sp->record = NULL;
		}
		/* Open new record file, unless file name is "off", which means
		 * disable recording
		 */
		if(strcmp(argv[1],"off") != 0){
#if 0 /* due to sesflush() trying to fflush() a closed stream... */
			if(fmode(sp->output,-1) == STREAM_ASCII)
#endif
				mode = APPEND_TEXT;
#if 0 /* due to sesflush() trying to fflush() a closed stream... */
			else
				mode = APPEND_BINARY;
#endif

			if((sp->record = fopen(argv[1],mode)) == NULL)
				_printf(_("Can't open %s: %s\n"),argv[1],strerror(errno));
		}
	}
#ifndef ZILOG /* haven't got fpname() with CRT stdio */
	if(sp->record != NULL)
		_printf(_("Recording into %s\n"),fpname(sp->record));
	else
		_printf(_("Recording off\n"));
#endif
	return 0;
}
#endif
/* Control file transmission */

#ifdef MODULE_doupload
int
doupload(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct session_s *sp;

	sp = (struct session_s *)p;
	if(sp == NULL){
		_printf(_("No current session\n"));
		return 1;
	}
	if(argc < 2){
#ifndef ZILOG /* haven't got fpname() with CRT stdio */
		if(sp->upload != NULL)
			_printf(_("Uploading %s\n"),fpname(sp->upload));
		else
			_printf(_("Uploading off\n"));
#endif
		return 0;
	}
	if(strcmp(argv[1],"stop") == 0 && sp->upload != NULL){
		/* Abort upload */
		fclose(sp->upload);
		sp->upload = NULL;
		killproc(sp->proc2);
		sp->proc2 = NULL;
		return 0;
	}
	/* Open upload file */
	if((sp->upload = fopen(argv[1],READ_TEXT)) == NULL){
		_printf(_("Can't read %s: %s\n"),argv[1],strerror(errno));
		return 1;
	}
	/* All set, invoke the upload process */
	sp->proc2 = process_thread_create(daemon_process_p, "upload",1024,upload,0,sp,NULL,0);
	return 0;
}
#endif
/* File uploading task */

#ifdef MODULE_upload
void
upload(unused,sp1,p)
int unused;
void *sp1;
void *p;
{
	struct session_s *sp;
	char *buf;

	sp = (struct session_s *)sp1;

	buf = mallocw(BUFSIZ);
	while(fgets(buf,BUFSIZ,sp->upload) != NULL)
		if(fputs(buf,sp->network) == EOF)
			break;

	free(buf);
	fflush(sp->network);
	fclose(sp->upload);
	sp->upload = NULL;
	sp->proc2 = NULL;
}
#endif

/* Print prompt and read one character */

#if 0 /*def MODULE_keywait*/
int
keywait(prompt,flush)
char *prompt;	/* Optional prompt */
int flush;	/* Flush queued input? */
{
	int c;
	int i;

	if(prompt == NULL)
		prompt = "Hit enter to continue";
	printf(prompt);
	fflush(stdout);
	c = _fgetc(stdin);
	/* Get rid of the prompt */
	for(i=strlen(prompt);i != 0;i--)
		putchar('\b');
	for(i=strlen(prompt);i != 0;i--)
		putchar(' ');
	for(i=strlen(prompt);i != 0;i--)
		putchar('\b');
	fflush(stdout);
	return (int)c;
}
#endif

/* Flush the current session's standard output. Called on every clock tick */
/* NOTE: THIS DOESN'T WORK AT THE MOMENT, AND WILL NOT BE REINSTATED!!!!!! */
#if 0 /*def MODULE_sesflush*/
void
sesflush()
	{
#if 1 /* due to sesflush() trying to fflush() a closed stream... */
	register struct thread_s **thread_pp, *thread_p;
	/* int i_state; */

	if (my_session_p == NULL)
		{
		/* probably only a startup issue, but let's be careful */
		return;
		}

	for (thread_pp = thread_list;
			thread_pp < thread_list + THREAD_LIST; thread_pp++)
		{
		/* i_state = dirps(); */
		thread_p = *thread_pp;
		if (thread_p && thread_p->flags.active &&
				thread_p->output && thread_p->process_p &&
				thread_p->process_p->session_p == my_session_p)
			{
			/* restore(i_state); */
			fflush(thread_p->output);
			}
		/* else */
			/* { */
			/* restore(i_state); */
			/* } */
		}

#else
 /* struct session_s *sp; */ /* save location for my_session_p (my_session_p) */

 /* dprintf(0, "\nsesflush() 0x%x 0x%x ", my_session_p, my_session_p->output); */
#if 1
	if (my_session_p && my_session_p->output) /* don't want fflush() to flushall() */
#else
	if(my_session_p != NULL)
#endif
 {
 /* need to fake out the current session or we'll be blocked by devtty.c */
 /* sp = my_thread_p->process_p->session_p; */
 /* my_thread_p->process_p->session_p = my_session_p; */
#if 1
 if ((unsigned int)my_session_p->output->obuf == 6)
  {
  ahexw((unsigned int)my_session_p->output);
  abyte('%');
  }
#else
 ahexw((unsigned int)&my_session_p->output->obuf);
 abyte('=');
 ahexw((unsigned int)my_session_p->output->obuf);
		fflush(my_session_p->output);
#endif
 /* my_thread_p->process_p->session_p = sp; */
 }
#endif
	}
#endif

