#!/bin/sh
grep "$1" include/*.h
grep "$1" include/ax25/*.h
grep "$1" include/client/*.h
grep "$1" include/driver/*.h
grep "$1" include/dump/*.h
grep "$1" include/filesys/*.h
grep "$1" include/internet/*.h
grep "$1" include/kernel/*.h
grep "$1" include/main/*.h
grep "$1" include/netrom/*.h
grep "$1" include/nos/*.h
grep "$1" include/ppp/*.h
grep "$1" include/server/*.h
grep "$1" include/socket/*.h
grep "$1" include/sys/*.h
grep "$1" include/z80/*.h
