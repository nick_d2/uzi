#!/bin/sh
test -d build || mkdir build || exit
tradcpp -I../include gboot.S build/gboot.asm || exit
as-z80 -l -o -z build/gboot.asm || exit
cp gboot.lnk build/ || exit
link-z80 -f build/gboot.lnk || exit
ihex2bin build/gboot.i86 ../../../bin/boot.bin || exit
# note: this doesn't work, do it manually and then run s.sh
crcd.com ..\\..\\..\\bin\\boot.bin || exit
bin2c ../../../bin/boot.bin ../../fsutil/boot.c || exit
bin2c ../../../bin/boot.bin ../../init/boot.c || exit
