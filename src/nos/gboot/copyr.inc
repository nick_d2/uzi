; copyr.s01
; Reentrant DMA copy routine for Hytech CMX, doesn't disable ints for too long
; Now modified for use as an include file when compiling the boot routines

; -----------------------------------------------------------------------------

; $io64180.s01

BITE		equ	32
BITELOG		equ	5

PBITE		equ	16
PBITELOG	equ	4

;	extern	abyte, ahexw, ahexn, acrlf

; copy routine
; ------------

; copy bc bytes from d:iy --> e:hl

; 10 bits @ 38400 == 260 usec
; 1 dma cycle (read+write) == 6 t-states == .977 usec
; so 266 dma cycles burst per character			6*266 = 1596
; we will use a max of 256 characters burst mode	6*256 = 1536

	rseg	RCODE

;	public	_copyr
;
;_copyr:	push	iy
;
;	push	de
;	pop	iy
;	ld	d,c			; d:iy = entry c:de -> source data
;
;	ld	hl,9
;	add	hl,sp
;	ld	b,(hl)
;	dec	hl
;	ld	c,(hl)			; bc = entry (sp+6) = byte count
;
;	dec	hl
;	dec	hl
;	ld	e,(hl)
;	dec	hl
;	ld	a,(hl)
;	dec	hl
;	ld	l,(hl)
;	ld	h,a			; e:hl = entry (sp+2) -> destination
;	call	copyr
;
;	pop	iy
;	ret
;
;	public	copyb
;
;copyb:	ld	bc,1

	public	copyr

copyr:
 .if 0 ; diagnostic
	push	hl

	ld	a,d
	call	ahexn
	ld	a,':'
	call	abyte

	push	iy
	pop	hl
	call	ahexw

	ld	a,' '
	call	abyte

	ld	a,e
	call	ahexn
	ld	a,':'
	call	abyte

	pop	hl
	push	hl
	call	ahexw

	ld	a,' '
	call	abyte

	ld	l,c
	ld	h,b
	call	ahexw

	call	acrlf

	pop	hl
 .endif
	ld	a,e
	cp	4
 .if 1
	jp	nc,copyrp
 .else
	jr	nc,copyrp
 .endif

 .if 1
	LD	A,C
	AND	BITE-1 ;00011111B
;	JR	Z,CPYL0
	PUSH	BC
	LD	C,A
	LD	B,0
 .if 1
;;;	di
;	out0	(BCR0L),c
	call	cpyst0
	inc	c
	dec	c
	jr	z,cpyl0
	in0	a,(DSTAT)
	xor	01010000b
	out0	(DSTAT),a
	add	iy,bc
	call	c,inc_d
	add	hl,bc
	call	c,inc_e
cpyl0:
 .else
	CALL	CPYBC
 .endif
 .if 0
;	POP	BC
; .if 0
;	LD	A,C
;	AND	.binnot.(BITE-1) ;11100000B
;	LD	C,A
; .endif
;CPYL0:
;	POP AF			; HIGH ORDER CNT
;	PUSH	HL
;	LD	L,C
;	LD	H,B
 ex (sp),hl
 sub a ; high order count
	LD	B,8-BITELOG ;3
CPYL1:	ADD	HL,HL
	ADC	A,A
	DJNZ	CPYL1			; A:H = A:HL/BITE ;32
	LD	C,H
	LD	B,A
	POP	HL
 .else
	pop	bc
	ld	a,BITELOG ;5
cpyl30:	srl	b
	rr	c
	dec	a
	jr	nz,cpyl30		; bc = bc/BITE ;32
 .endif
	JR	CPYL3
CPYL2:	PUSH	BC
	LD	BC,BITE ;32
 .if 1
;;;	ei
;	out0	(BCR0L+1),b
 out0 (BCR0L),c
;;;	di
;	in0	(BCR0L+1) ;f,(BCR0L+1)
;	call	z,cpyst0
;;; in0 (BCR0L)
;;; call z,cpyst0
	in0	a,(DSTAT)
	xor	01010000b
	out0	(DSTAT),a
	add	iy,bc
	call	c,inc_d
	add	hl,bc
	call	c,inc_e
 .else
	CALL	CPYBCI
 .endif
	POP	BC
	DEC	BC
CPYL3:	LD	A,B
	OR	C
	JR	NZ,CPYL2
 .else
	push	bc
	ld	b,0
;;;	di
;	out0	(BCR0L),c
	call	cpyst0
	inc	c
	dec	c
	jr	z,cpyl0
	in0	a,(DSTAT)
	xor	01010000b
	out0	(DSTAT),a
	add	iy,bc
	call	c,inc_d
	add	hl,bc
	call	c,inc_e
cpyl0:	pop	bc
	inc	b
	jr	cpyl1
cpyl2:	push	bc
	ld	bc,100h
;;;	ei
	out0	(BCR0L+1),b
;;;	di
	in0	(BCR0L+1) ;f,(BCR0L+1)
	call	z,cpyst0
	in0	a,(DSTAT)
	xor	01010000b
	out0	(DSTAT),a
	add	iy,bc
	call	c,inc_d
	add	hl,bc
	call	c,inc_e
	pop	bc
cpyl1:	djnz	cpyl2
 .endif
;;;	ei
	ret

cpyst0:
 out0 (BCR0L),c
	out0	(BCR0L+1),b
	jr	cpyst2
cpyst1:	ld	a,1
	out0	(BCR0L),a
cpyst2:	push	hl
	push	iy
	out0	(DAR0L),l
	out0	(DAR0L+1),h
	out0	(DAR0L+2),e
	pop	hl
	out0	(SAR0L),l
	out0	(SAR0L+1),h
	out0	(SAR0L+2),d
	pop	hl
	ld	a,2			; memory+ --> memory+, burst mode
	out0	(DMODE),a
	ret

inc_d:	inc	d
	ret
inc_e:	inc	e
	ret

copyrp:	push	bc
	xor	a
;;;	di
	out0	(BCR0L),a
	out0	(BCR0L+1),a
 .if 0
	push	hl
	ld	l,c
	ld	h,b
 sub a ; high order count
	LD	B,8-PBITELOG
CPYL1X:	ADD	HL,HL
	ADC	A,A
	DJNZ	CPYL1X			; A:H = A:HL/PBITE ;32
	LD	C,H
	LD	B,A
	pop	hl
 .else
	ld	a,PBITELOG ;5
cpyl31:	srl	b
	rr	c
	dec	a
	jr	nz,cpyl31		; bc = bc/PBITE ;32
 .endif
	ld	a,b
	or	c
	jr	z,cpyc0
cpyl4:	push	bc			; 1464 t-states for this loop
	ld	bc,PBITE
	call	cpybc0
	pop	bc
	dec	bc
;;;	di
	ld	a,b
	or	c
	jr	nz,cpyl4
cpyc0:	pop	bc
	ld	b,a			; 0
	ld	a,c
	and	PBITE-1 ;00011111b
	jr	z,cpyl5
	ld	c,a
cpybc0:	in0	(BCR0L) ;f,(BCR0L)
	call	z,cpyst1
	add	iy,bc			; 1396 t-states for this subroutine
	call	c,inc_d			;   32 bytes, no boundary
	add	hl,bc			;   no interrupt, no dma1 cycle steal
	call	c,inc_e
	ld	b,c
	ld	c,1
	in0	a,(DSTAT)
	xor	01010000b
cpybc1:	out0	(DSTAT),a
	out0	(BCR0L),c
	djnz	cpybc1
cpyl5:
;;;	ei
	ret

; -----------------------------------------------------------------------------

;	END
