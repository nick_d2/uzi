# auto.mk by Nick for Hytech NOS/UZI project

# -----------------------------------------------------------------------------

SUBDIRS=

OUTPUTS=	intl.$(LIBEXT)

intl_$(LIBEXT)_SOURCES= \
		gettext.S intl.c

intl_c_MODULES=	__failtext __dgetfail __fgetfail __getfail _dprintf _vdprintf \
		_fprintf _vfprintf _fputs _printf _vprintf _puts _strcpy \
		_strlen _strncmp _strcmp _strncpy _stricmp _perror _sprintf

# -----------------------------------------------------------------------------

