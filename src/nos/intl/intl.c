/* intl.c by Nick for NOS/UZI project */
/* Apr 06	RPB	Added various "_" versions of functions for intl' */

#include <errno.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include "kernel/dprintf.h"
#include <libintl.h>

#ifdef MODULE
#define STATIC
extern char __failtext[];
#else
#define STATIC static
#define MODULE___failtext
#define MODULE___dgetfail
#define MODULE___fgetfail
#define MODULE___getfail
#define MODULE__dprintf
#define MODULE__vdprintf
#define MODULE__fprintf
#define MODULE__vfprintf
#define MODULE__fputs
#define MODULE__printf
#define MODULE__vprintf
#define MODULE__puts
#define MODULE__strcpy
#define MODULE__strlen
#define MODULE__strncmp
#define MODULE__strcmp
#define MODULE__strncpy
#define MODULE__stricmp
#define MODULE__perror
#define MODULE__sprintf
#endif

/* ------------------------------------------------------------------------- */

#ifdef MODULE___failtext
STATIC char __failtext[] = "_gettext(0x%lx) failure\n";
#endif

#ifdef MODULE___dgetfail
int __dgetfail(unsigned char level, void (*address)(void))
	{
	return dprintf(level, __failtext, (unsigned long)address);
	}
#endif

#ifdef MODULE___fgetfail
int __fgetfail(FILE *stream, void (*address)(void))
	{
	return fprintf(stream, __failtext, (unsigned long)address);
	}
#endif

#ifdef MODULE___getfail
int __getfail(void (*address)(void))
	{
	return printf(__failtext, (unsigned long)address);
	}
#endif

/* ------------------------------------------------------------------------- */

#ifdef MODULE__dprintf
int _dprintf(unsigned char level, _char *format, ...)
	{
	va_list arguments;
	int result;

#if 0 /* note: useful for finding pointer leaks ie. if malloc can't be used */
 abyte('(');
 ahexb(((unsigned long)format) >> 16);
 ahexw((unsigned int)format);
 abyte(')');
#endif
#if 0
 if (*(unsigned int *)0x5ea != 0xc933)
  {
  abyte('!');
  }
#endif
	if (Debug_level < level)
		{
		return 0;
		}

/* abyte('b'); */
	va_start(arguments, format);
/* abyte('c'); */
	result = _vdprintf(level, format, arguments);
/* abyte('d'); */
	va_end(arguments);

/* abyte('e'); */
	return result;
	}
#endif

#ifdef MODULE__vdprintf
int _vdprintf(unsigned char level, _char *format, va_list arguments)
	{
	char *duptext;
	int result;

/* abyte('A'); */
	if (Debug_level < level)
		{
		return 0;
		}

/* abyte('B'); */
	duptext = _strdup(format);
/* abyte('C'); */
	if (duptext == NULL)
		{
#if DEBUG >= 0
 dprintf(0, "\n%02x:%04x _vdprintf(0x%x, 0x%lx, 0x%x) ",
	 ((unsigned char *)&level)[4], ((unsigned int *)&level)[3],
	 (unsigned int)level, (unsigned long)format, (unsigned int)arguments);
#endif
		return __dgetfail(level, format);
		}

/* abyte('D'); */
	result = vdprintf(level, duptext, arguments);

/* abyte('E'); */
	free(duptext);
/* abyte('F'); */
	return result;
	}
#endif

/* ------------------------------------------------------------------------- */

#ifdef MODULE__fprintf
int _fprintf(FILE *stream, _char *format, ...)
	{
	va_list arguments;
	int result;

	va_start(arguments, format);
	result = _vfprintf(stream, format, arguments);
	va_end(arguments);

	return result;
	}
#endif

#ifdef MODULE__vfprintf
int _vfprintf(FILE *stream, _char *format, va_list arguments)
	{
	char *duptext;
	int result;

	duptext = _strdup(format);
	if (duptext == NULL)
		{
#if DEBUG >= 0
 dprintf(0, "\n%02x:%04x _vfprintf(0x%x, 0x%lx, 0x%x) ",
	 ((unsigned char *)&stream)[4], ((unsigned int *)&stream)[3],
	 (unsigned int)stream, (unsigned long)format, (unsigned int)arguments);
#endif
		return __fgetfail(stream, format);
		}

	result = vfprintf(stream, duptext, arguments);

	free(duptext);
	return result;
	}
#endif

#ifdef MODULE__fputs
int _fputs(_char *string, FILE *stream)
	{
	char *duptext;
	int result;

	duptext = _strdup(string);
	if (duptext == NULL)
		{
#if DEBUG >= 0
 dprintf(0, "\n%02x:%04x _fputs(0x%lx, 0x%x) ",
	 ((unsigned char *)&string)[4], ((unsigned int *)&string)[3],
	 (unsigned long)string, (unsigned int)stream);
#endif
		return __fgetfail(stream, string);
		}

	result = fputs(duptext, stream);

	free(duptext);
	return result;
	}
#endif

/* ------------------------------------------------------------------------- */

#ifdef MODULE__printf
int _printf(_char *format, ...)
	{
	va_list arguments;
	int result;

	va_start(arguments, format);
	result = _vprintf(format, arguments);
	va_end(arguments);

	return result;
	}
#endif

#ifdef MODULE__vprintf
int _vprintf(_char *format, va_list arguments)
	{
	char *duptext;
	int result;

	duptext = _strdup(format);
	if (duptext == NULL)
		{
#if DEBUG >= 0
 dprintf(0, "\n%02x:%04x _vprintf(0x%lx, 0x%x) ",
	 ((unsigned char *)&format)[4], ((unsigned int *)&format)[3],
	 (unsigned long)format, (unsigned int)arguments);
#endif
		return __getfail(format);
		}

	result = vprintf(duptext, arguments);

	free(duptext);
	return result;
	}
#endif

#ifdef MODULE__puts
int _puts(_char *string)
	{
	char *duptext;
	int result;

	duptext = _strdup(string);
	if (duptext == NULL)
		{
#if DEBUG >= 0
 dprintf(0, "\n%02x:%04x _puts(0x%lx) ",
	 ((unsigned char *)&string)[4], ((unsigned int *)&string)[3],
	 (unsigned long)string);
#endif
		return __getfail(string);
		}

	result = puts(duptext);

	free(duptext);
	return result;
	}
#endif

/* ------------------------------------------------------------------------- */

#ifdef MODULE__strcpy
char *_strcpy(char *buffer, _char *string)
	{
	char *duptext;

	duptext = _strdup(string);
	if (duptext == NULL)
		{
#if DEBUG >= 0
 dprintf(0, "\n%02x:%04x _strcpy(0x%x, 0x%lx) ",
	 ((unsigned char *)&buffer)[4], ((unsigned int *)&buffer)[3],
	 (unsigned int)buffer, (unsigned long)string);
#endif
		__getfail(string);

		/* attempt to continue with a null string */
		*buffer = 0;
		return buffer;
		}

	strcpy(buffer, duptext);

	free(duptext);
	return buffer;
	}
#endif

#ifdef MODULE__strlen
int _strlen(_char *string)
	{
	int result;
	char *duptext;

	duptext = _strdup(string);
	if (duptext == NULL)
		{
#if DEBUG >= 0
 dprintf(0, "\n%02x:%04x _strlen(0x%lx) ",
	 ((unsigned char *)&string)[4], ((unsigned int *)&string)[3],
	 (unsigned long)string);
#endif
		__getfail(string);

		/* attempt to continue with a null string */
		return 0;
		}

	/* note: it has to be done this way due to plurals handling */
	result = strlen(duptext);

	free(duptext);
	return result;
	}
#endif

#ifdef MODULE__strncmp
int _strncmp(const char *buffer, _char *string, size_t count)
	{
	int result;
	char *duptext;

	duptext = _strdup(string);
	if (duptext == NULL)
		{
#if DEBUG >= 0
 dprintf(0, "\n%02x:%04x _strncmp(0x%x, 0x%lx, 0x%x) ",
	 ((unsigned char *)&buffer)[4], ((unsigned int *)&buffer)[3],
	 (unsigned int)buffer, (unsigned long)string, (unsigned int)count);
#endif
		__getfail(string);

		/* attempt to continue with a null string */
		return *buffer ? 1 : 0;
		}

	result = strncmp(buffer, duptext, count);

	free(duptext);
	return result;
	}
#endif

/*RPB*/
#ifdef MODULE__strcmp
int _strcmp(const char *buffer, _char *string)
{
	int result;
	char *duptext;

	duptext = _strdup(string);
	if (duptext == NULL)
	{
		__getfail(string);

		/* attempt to continue with a null string */
		return *buffer ? 1 : 0;
	}

	result = strcmp(buffer, duptext);

	free(duptext);
	return result;
}
#endif
/*RPB*/

#ifdef MODULE__strncpy
char *_strncpy(char *buffer, _char *string, size_t count)
	{
	char *duptext;

	duptext = _strdup(string);
	if (duptext == NULL)
		{
#if DEBUG >= 0
 dprintf(0, "\n%02x:%04x _strncpy(0x%x, 0x%lx, 0x%x) ",
	 ((unsigned char *)&buffer)[4], ((unsigned int *)&buffer)[3],
	 (unsigned int)buffer, (unsigned long)string, (unsigned int)count);
#endif
		__getfail(string);

		/* attempt to continue with a null string */
		memset(buffer, 0, count);
		return buffer;
		}

	strncpy(buffer, duptext, count);

	free(duptext);
	return buffer;
	}
#endif

/*RPB*/
#ifdef MODULE__stricmp
int _stricmp(const char *buffer, _char *string)
{
	int result;
	char *duptext;

	duptext = _strdup(string);
	if (duptext == NULL)
	{
#if DEBUG >= 0
 dprintf(0, "\n%02x:%04x _stricmp(0x%x, 0x%lx) ",
	 ((unsigned char *)&buffer)[4], ((unsigned int *)&buffer)[3],
	 (unsigned int)buffer, (unsigned long)string);
#endif
		__getfail(string);

		/* attempt to continue with a null string */
		return *buffer ? 1 : 0;
	}

	result = stricmp(buffer, duptext);

	free(duptext);
	return result;
}
#endif

#ifdef MODULE__perror
void _perror(_char *string)
{
	int saved_errno;
	char *duptext;

	/* In case errno is changed by this call */
	saved_errno = errno;

	duptext = _strdup(string);
	if (duptext == NULL)
	{
		__getfail(string);

		return;
	}

	errno = saved_errno;
	perror(duptext);

	free(duptext);
	return;
}
#endif

#ifdef MODULE__sprintf
unsigned int __sputter __P((void *buffer, unsigned int count, FILE *op));

int _sprintf(char *sp, _char *format, ...)
{
	va_list arguments;
	char *duptext;
	int result;

	duptext = _strdup(format);

	if (duptext == NULL)
	{
		__getfail(format);
		return 0;
	}

	va_start(arguments, format);
	result = __vprinter(__sputter, (FILE *)&sp, duptext, arguments);
	va_end(arguments);
	*(unsigned char *)sp = 0;
	free(duptext);
	return result;
}
#endif
/*RPB*/

/* ------------------------------------------------------------------------- */

