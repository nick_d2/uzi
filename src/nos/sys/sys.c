/* sys.c by Nick for NOS/UZI implementation */

#ifdef _SYS
#define _exit __exit
#define access _access
#define alarm _alarm
#define brk _brk
#define chdir _chdir
#define chmod _chmod
#define chown _chown
#define chroot _chroot
#define close _close
#define creat _creat
#define dup _dup
#define dup2 _dup2
#define execve _execve
#define fork _fork
#define fstat _fstat
#define getegid _getegid
#define geteuid _geteuid
#define getfsys _getfsys
#define getgid _getgid
#define getpid _getpid
#define getppid _getppid
#define getprio _getprio
#define getuid _getuid
#define ioctl _ioctl
#define kill _kill
#define link _link
#define lseek _lseek
#define mkfifo _mkfifo
#define mknod _mknod
#define mount _mount
#define open _open
#define pause _pause
#define pipe _pipe
#define read _read
#define reboot _reboot
#define sbrk _sbrk
#define lseek _lseek
#define setgid _setgid
#define setprio _setprio
#define setuid _setuid
#define signal _signal
#define stat _stat
#define stime _stime
#define symlink _symlink
#define sync _sync
#define sysdebug _sysdebug
#define systrace _systrace
#define time _time
#define times _times
#define umask _umask
#define umount _umount
#define unlink _unlink
#define utime _utime
#define waitpid _waitpid
#define write _write
#define falign _falign
#define vfork _vfork
#define socket _socket
#define bind _bind
#define listen _listen
#define connect _connect
#define accept _accept
#define shutdown _shutdown
#define socketpair _socketpair
#define recv _recv
#define recvfrom _recvfrom
#define send _send
#define sendto _sendto
#define getsockname _getsockname
#define getpeername _getpeername
#endif

#define SKIP_SYSCALLS /* due to IAR handling of varargs functions */

#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include "kernel/process.h" /* for GET_EGID etc */
#include "driver/rtc.h" /* for rtctime_t */

void abyte(char data);
void acrlf(void);
void ahexb(char data);
void ahexw(int data);
void amess(char *ptr);

#ifndef MODULE
#define MODULE__exit
#define MODULE_access
#define MODULE_alarm
#define MODULE_brk
#define MODULE_chdir
#define MODULE_chmod
#define MODULE_chown
#define MODULE_chroot
#define MODULE_close
#define MODULE_creat
#define MODULE_dup
#define MODULE_dup2
#define MODULE_execve
#define MODULE_fork
#define MODULE_fstat
#define MODULE_getegid
#define MODULE_geteuid
#define MODULE_getfsys
#define MODULE_getgid
#define MODULE_getpid
#define MODULE_getppid
#define MODULE_getprio
#define MODULE_getuid
#define MODULE_ioctl
#define MODULE_kill
#define MODULE_link
#define MODULE_lseek
#define MODULE_mkfifo
#define MODULE_mknod
#define MODULE_mount
#define MODULE_open
#define MODULE_pause
#define MODULE_pipe
#define MODULE_read
#define MODULE_reboot
#define MODULE_sbrk
#define MODULE_lseek
#define MODULE_setgid
#define MODULE_setprio
#define MODULE_setuid
#define MODULE_signal
#define MODULE_stat
#define MODULE_stime
#define MODULE_symlink
#define MODULE_sync
#define MODULE_sysdebug
#define MODULE_systrace
#define MODULE_time
#define MODULE_times
#define MODULE_umask
#define MODULE_umount
#define MODULE_unlink
#define MODULE_utime
#define MODULE_waitpid
#define MODULE_write
#define MODULE_falign
#define MODULE_vfork
#define MODULE_socket
#define MODULE_bind
#define MODULE_listen
#define MODULE_connect
#define MODULE_accept
#define MODULE_shutdown
#define MODULE_socketpair
#define MODULE_recv
#define MODULE_recvfrom
#define MODULE_send
#define MODULE_sendto
#define MODULE_getsockname
#define MODULE_getpeername
#endif

/********** MSX-UZIX version of syscalls ************/
/* void _exit(int val); */

#ifdef MODULE__exit
#ifndef HI_TECH_C
void _exit(int val)
	{
	__unix(11, val);
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	__exit");
	asm("	signat	__exit,4152");
	asm("__exit:");
	asm("	ld hl,11");
	asm("	jp __sys__1");
#endif
#endif
/********** MSX-UZIX version of syscalls ************/
/* int access(char *path, int mode); */

#ifdef MODULE_access
#ifndef HI_TECH_C
int access(char *path, int mode)
	{
	unsigned int error;

	error = __unix(0, path, mode);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_access");
	asm("	signat	_access,8250");
	asm("_access:");
	asm("	ld hl,0");
	asm("	jp __sys__2");
#endif
#endif


/********** MSX-UZIX version of syscalls ************/
/* int alarm(uint secs); */

#ifdef MODULE_alarm
#ifndef HI_TECH_C
int alarm(uint secs)
	{
	unsigned int error;
	unsigned int result;

	error = __unix(1, &result, secs);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (int)result;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_alarm");
	asm("	signat	_alarm,4154");
	asm("_alarm:");
	asm("	ld hl,1");
	asm("	jp __sys__1");
#endif
#endif


/********** MSX-UZIX version of syscalls ************/
/* int brk(char *addr); */

#ifdef MODULE_brk
#ifndef HI_TECH_C
int brk(char *addr)
	{
	unsigned int error;

	error = __unix(2, addr);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_brk");
	asm("	signat	_brk,4154");
	asm("_brk:");
	asm("	ld hl,2");
	asm("	jp __sys__1");
#endif
#endif
/********** MSX-UZIX version of syscalls ************/
/* int chdir(char *dir); */

#ifdef MODULE_chdir
#ifndef HI_TECH_C
int chdir(char *dir)
	{
	unsigned int error;

	error = __unix(3, dir);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_chdir");
	asm("	signat	_chdir,4154");
	asm("_chdir:");
	asm("	ld hl,3");
	asm("	jp __sys__1");
#endif
#endif
/********** MSX-UZIX version of syscalls ************/
/* int chmod(char *path, mode_t mode); */

#ifdef MODULE_chmod
#ifndef HI_TECH_C
int chmod(char *path, mode_t mode)
	{
	unsigned int error;

	error = __unix(4, path, mode);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_chmod");
	asm("	signat	_chmod,8250");
	asm("_chmod:");
	asm("	ld hl,4");
	asm("	jp __sys__2");
#endif
#endif
/********** MSX-UZIX version of syscalls ************/
/* int chown(char *path, int owner, int group); */

#ifdef MODULE_chown
#ifndef HI_TECH_C
int chown(char *path, int owner, int group)
	{
	unsigned int error;

	error = __unix(5, path, owner, group);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#else
	asm("	psect text,class=CODE");
        asm("   global __sys__3");
	asm("	global	_chown");
	asm("	signat	_chown,12346");
	asm("_chown:");
	asm("	pop hl");
	asm("	ex (sp),hl");
	asm("	push hl");
	asm("	ld hl,5");
	asm("	jp __sys__3");
#endif
#endif
/********** MSX-UZIX version of syscalls ************/
/* int chroot(char *dir); */

#ifdef MODULE_chroot
#ifndef HI_TECH_C
int chroot(char *dir)
	{
	unsigned int error;

	error = __unix(39, dir);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_chroot");
	asm("	signat	_chroot,4154");
	asm("_chroot:");
	asm("	ld hl,39");
	asm("	jp __sys__1");
#endif
#endif
/********** MSX-UZIX version of syscalls ************/
/* int close(int uindex); */

#ifdef MODULE_close
#ifndef HI_TECH_C
int close(int uindex)
	{
	unsigned int error;

	error = __unix(6, uindex);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_close");
	asm("	signat	_close,4154");
	asm("_close:");
	asm("	ld hl,6");
	asm("	jp __sys__1");
#endif
#endif
/********** MSX-UZIX version of syscalls ************/
/* int creat(const char *name, mode_t mode); */

/* this was not defined by syscalls.h, because it would break open.c */
/* here we need to define it as varargs so that IAR will not use regparms */

#ifdef MODULE_creat
extern int open __P((const char *name, int flag, ...));
/* note:  this conflicts with the definition of open() below, */
/* in this file, so please use modules until I clean it all up */

int creat(const char *name, mode_t mode) {
	return open(name,O_CREAT|O_RDWR /*O_WRONLY*/|O_TRUNC,mode);
}
#endif


/********** MSX-UZIX version of syscalls ************/
/* int dup(int oldd); */

#ifdef MODULE_dup
#ifndef HI_TECH_C
int dup(int oldd)
	{
	unsigned int error;
	unsigned int result;

	error = __unix(8, &result, oldd);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (int)result;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_dup");
	asm("	signat	_dup,4154");
	asm("_dup:");
	asm("	ld hl,8");
	asm("	jp __sys__1");
#endif
#endif
/********** MSX-UZIX version of syscalls ************/
/* int dup2(int oldd, int newd); */

#ifdef MODULE_dup2
#ifndef HI_TECH_C
int dup2(int oldd, int newd)
	{
	unsigned int error;

	error = __unix(9, oldd, newd);
	if (error)
		{
		errno = error;
		return -1;
		}
	return newd;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_dup2");
	asm("	signat	_dup2,8250");
	asm("_dup2:");
	asm("	ld hl,9");
	asm("	jp __sys__2");
#endif
#endif
/********** MSX-UZIX version of syscalls ************/
/* int execve(char *name, char *argv[], char *envp[]); */

#ifdef MODULE_execve
#ifndef HI_TECH_C
int execve(char *name, char *argv[], char *envp[])
	{
	unsigned int error;

	error = __unix(10, name, argv, envp);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0; /* can never get here */
	}
#else
	asm("	psect text,class=CODE");
        asm("   global __sys__3");
	asm("	global	_execve");
	asm("	signat	_execve,12346");
	asm("_execve:");
	asm("	pop hl");
	asm("	ex (sp),hl");
	asm("	push hl");
	asm("	ld hl,10");
	asm("	jp __sys__3");
#endif
#endif

/********** MSX-UZIX version of syscalls ************/
/* int fork(void); */

#ifdef MODULE_fork
#ifndef HI_TECH_C
int fork(void)
	{
	unsigned int error;
	unsigned int result;

	error = __unix(12, &result);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (int)result;
	}
#else
	asm("	psect text, &result,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_fork");
	asm("	signat	_fork,26");
	asm("_fork:");
	asm("	ld hl,12");
	asm("	jp __sys__");
#endif
#endif
/********** MSX-UZIX version of syscalls ************/
/* int fstat(int fd, struct stat *buf); */

#ifdef MODULE_fstat
#ifndef HI_TECH_C
int fstat(int fd, struct stat *buf)
	{
	unsigned int error;

	error = __unix(13, fd, buf);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_fstat");
	asm("	signat	_fstat,8250");
	asm("_fstat:");
	asm("	ld hl,13");
	asm("	jp __sys__2");
#endif
#endif
/********** MSX-UZIX version of syscalls ************/
/* int getegid(void); */

#define __str3(x)	__STRING(x)
#define __str2(x)	__str3(x)
#define __str1(x)	__str2(x)

#ifdef MODULE_getegid
#ifndef HI_TECH_C
int getegid(void)
	{
	unsigned int error;
	unsigned int result;

#if 0
	__unix_get(GET_EGID);
#else
	error = __unix(7, &result, GET_EGID);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (int)result;
#endif
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __getset");
	asm("	global	_getegid");
	asm("	signat	_getegid,26");
	asm("_getegid:");
	asm("	ld de," __str1(GET_EGID));
	asm("	jp __getset");
#endif
#endif


/********** MSX-UZIX version of syscalls ************/
/* int geteuid(void); */

#define __str3(x)	__STRING(x)
#define __str2(x)	__str3(x)
#define __str1(x)	__str2(x)

#ifdef MODULE_geteuid
#ifndef HI_TECH_C
int geteuid(void)
	{
	unsigned int error;
	unsigned int result;

#if 0
	__unix_get(GET_EUID);
#else
	error = __unix(7, &result, GET_EUID);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (int)result;
#endif
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __getset");
	asm("	global	_geteuid");
	asm("	signat	_geteuid,26");
	asm("_geteuid:");
	asm("	ld de," __str1(GET_EUID));
	asm("	jp __getset");
#endif
#endif


/********** MSX-UZIX version of syscalls ************/
/* int getfsys(int dev, void *buf); */

#ifdef MODULE_getfsys
#ifndef HI_TECH_C
int getfsys(int dev, void *buf)
	{
	unsigned int error;

	error = __unix(14, dev, buf);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_getfsys");
	asm("	signat	_getfsys,8250");
	asm("_getfsys:");
	asm("	ld hl,14");
	asm("	jp __sys__2");
#endif
#endif
/********** MSX-UZIX version of syscalls ************/
/* int getgid(void); */

#define __str3(x)	__STRING(x)
#define __str2(x)	__str3(x)
#define __str1(x)	__str2(x)

#ifdef MODULE_getgid
#ifndef HI_TECH_C
int getgid(void)
	{
	unsigned int error;
	unsigned int result;

#if 0
	__unix_get(GET_GID);
#else
	error = __unix(7, &result, GET_GID);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (int)result;
#endif
	}
#else
	asm("	psect text,class=CODE");
        asm("   global __getset");
	asm("	global	_getgid");
	asm("	signat	_getgid,26");
	asm("_getgid:");
	asm("	ld de," __str1(GET_GID));
	asm("	jp __getset");
#endif
#endif


/********** MSX-UZIX version of syscalls ************/
/* int getpid(void); */

#define __str3(x)	__STRING(x)
#define __str2(x)	__str3(x)
#define __str1(x)	__str2(x)

#ifdef MODULE_getpid
#ifndef HI_TECH_C
int getpid(void)
	{
	unsigned int error;
	unsigned int result;

#if 0
	__unix_get(GET_PID);
#else
	error = __unix(7, &result, GET_PID);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (int)result;
#endif
	}
#else
	asm("	psect text,class=CODE");
        asm("   global __getset");
	asm("	global	_getpid");
	asm("	signat	_getpid,26");
	asm("_getpid:");
	asm("	ld de," __str1(GET_PID));
	asm("	jp __getset");
#endif
#endif


/********** MSX-UZIX version of syscalls ************/
/* int getppid(void); */

#define __str3(x)	__STRING(x)
#define __str2(x)	__str3(x)
#define __str1(x)	__str2(x)

#ifdef MODULE_getppid
#ifndef HI_TECH_C
int getppid(void)
	{
	unsigned int error;
	unsigned int result;

#if 0
	__unix_get(GET_PPID);
#else
	error = __unix(7, &result, GET_PPID);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (int)result;
#endif
	}
#else
	asm("	psect text,class=CODE");
        asm("   global __getset");
	asm("	global	_getppid");
	asm("	signat	_getppid,26");
	asm("_getppid:");
	asm("	ld de," __str1(GET_PPID));
	asm("	jp __getset");
#endif
#endif


/********** MSX-UZIX version of syscalls ************/
/* int getprio(void); */

#define __str3(x)	__STRING(x)
#define __str2(x)	__str3(x)
#define __str1(x)	__str2(x)

#ifdef MODULE_getprio
#ifndef HI_TECH_C
int getprio(void)
	{
	unsigned int error;
	unsigned int result;

#if 0
	__unix_get(GET_PRIO);
#else
	error = __unix(7, &result, GET_PRIO);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (int)result;
#endif
	}
#else
	asm("	psect text,class=CODE");
        asm("   global __getset");
	asm("	global	_getprio");
	asm("	signat	_getprio,26");
	asm("_getprio:");
	asm("	ld de," __str1(GET_PRIO));
	asm("	jp __getset");
#endif
#endif


/********** MSX-UZIX version of syscalls ************/
/* int getuid(void); */

#define __str3(x)	__STRING(x)
#define __str2(x)	__str3(x)
#define __str1(x)	__str2(x)

#ifdef MODULE_getuid
#ifndef HI_TECH_C
int getuid(void)
	{
	unsigned int error;
	unsigned int result;

#if 0
	__unix_get(GET_UID);
#else
	error = __unix(7, &result, GET_UID);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (int)result;
#endif
	}
#else
	asm("	psect text,class=CODE");
        asm("   global __getset");
	asm("	global	_getuid");
	asm("	signat	_getuid,26");
	asm("_getuid:");
	asm("	ld de," __str1(GET_UID));
	asm("	jp __getset");
#endif
#endif


/********** MSX-UZIX version of syscalls ************/
/* int ioctl(fd, req, ...) */

#define __str3(x)	__STRING(x)
#define __str2(x)	__str3(x)
#define __str1(x)	__str2(x)
#define dosyscall()	asm(" call " __str1(SYSCADDR))

#ifdef MODULE_ioctl
#ifndef HI_TECH_C
int ioctl(int fd, int req, int arg, ...) /* makes IAR use stack params */
	{
	unsigned int error;

	error = __unix(15, fd, req, arg);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_ioctl");
	asm("	signat	_ioctl,4122");
	asm("_ioctl:");
	asm("	push ix");
	asm("	ld ix,0");
	asm("	add ix,sp");
	asm("	ld l,(ix+6)");
	asm("	ld h,(ix+7)");
	asm("	push hl");
	asm("	ld l,(ix+4)");
	asm("	ld h,(ix+5)");
	asm("	push hl");
	asm("	push de");
	asm("	ld hl,15");
	asm("	push hl");
	dosyscall();
	asm("	call __ret__");
	asm("	pop ix"); /* discards HL */
	asm("	pop ix"); /* discards DE */
	asm("	pop ix"); /* discards first HL */
	asm("	pop ix"); /* discards second HL */
	asm("	pop ix");
	asm("	ret");
#endif
#endif
/********** MSX-UZIX version of syscalls ************/
/* int kill(int pid, int sig); */

#ifdef MODULE_kill
#ifndef HI_TECH_C
int kill(int pid, int sig)
	{
	unsigned int error;

	error = __unix(16, pid, sig);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_kill");
	asm("	signat	_kill,8250");
	asm("_kill:");
	asm("	ld hl,16");
	asm("	jp __sys__2");
#endif
#endif
/********** MSX-UZIX version of syscalls ************/
/* int link(const char *oldname, const char *newname); */

#ifdef MODULE_link
#ifndef HI_TECH_C
int link(const char *oldname, const char *newname)
	{
	unsigned int error;

	error = __unix(17, oldname, newname);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_link");
	asm("	signat	_link,8250");
	asm("_link:");
	asm("	ld hl,17");
	asm("	jp __sys__2");
#endif
#endif
/********** MSX-UZIX version of syscalls ************/
/* long lseek(int file, off_t offset, int flag); */

#define __str3(x)	__STRING(x)
#define __str2(x)	__str3(x)
#define __str1(x)	__str2(x)
#define dosyscall()	asm(" call " __str1(SYSCADDR))

#ifdef MODULE_lseek
#ifndef HI_TECH_C
long lseek(int file, off_t offset, int flag)
	{
	unsigned int error;
	unsigned long result;

	error = __unix(25, &result, file, offset, flag);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (long)result;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_lseek");
	asm("	signat	_lseek,12348");
	asm("_lseek:");
	asm("	pop hl");	/* HL=ret addr */
	asm("	push ix");	/* save IX */
	asm("	ld ix,0");
	asm("	add ix,sp");
	asm("	ld c,(ix+6)");
	asm("	ld b,(ix+7)");	/* BC=flag */
	asm("	push bc");
	asm("	ld (ix+6),l");
	asm("	ld (ix+7),h");	/* put ret addr after arguments in stack */
	asm("	pop ix");	/* IX=flag (was in BC) */
	asm("	ex (sp),ix");	/* IX=original value, put flag in stack */
	asm("	pop hl");	/* HL=flag */
	asm("	pop bc");	/* BC=offset lsw */
	asm("	ex (sp),hl");	/* put flag in stack, HL=offset msw */
	asm("	push hl");	/* put offset msw in stack */
	asm("	push bc");	/* put offset lsw in stack */
	asm("	push de");	/* put file in stack */
	asm("	ld hl,25");	/* syscall */
	asm("	push hl");
	dosyscall();
	asm("	inc sp");
	asm("	inc sp");	/* discards syscall number */
	asm("	inc sp");
	asm("	inc sp");	/* discards flag */
	asm("	inc sp");
	asm("	inc sp");
	asm("	inc sp");
	asm("	inc sp");	/* discards offset */
	asm("	inc sp");
	asm("	inc sp");	/* discards file */
	asm("	ex de,hl");	/* cancel EX DE,HL in __ret__ */
	asm("	jp __ret__");
#endif
#endif
/********** MSX-UZIX version of syscalls ************/
/* int mkfifo(char *name, mode_t mode); */

#ifdef MODULE_mkfifo
int mkfifo(char *name, mode_t mode) {
	return mknod(name, mode | S_IFPIPE, (dev_t) 0);
}
#endif
/********** MSX-UZIX version of syscalls ************/
/* int mknod(char *name, mode_t mode, int dev); */

#ifdef MODULE_mknod
#ifndef HI_TECH_C
int mknod(char *name, mode_t mode, int dev)
	{
	unsigned int error;

	error = __unix(18, name, mode, dev);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__3");
	asm("	global	_mknod");
	asm("	signat	_mknod,12346");
	asm("_mknod:");
	asm("	pop hl");
	asm("	ex (sp),hl");
	asm("	push hl");
	asm("	ld hl,18");
	asm("	jp __sys__3");
#endif
#endif
/********** MSX-UZIX version of syscalls ************/
/* int modulereg(uint sig, int (*func)()); */
/* int moduledereg(uint sig); */
/* int modulecall(uint sig, uint fcn, char *args, int argsize); */
/* int modulesendreply(int pid, int fcn, char *repladdr, int replysize); */
/* int modulereply(int sig, int fcn, char *repladdr); */

#define __str3(x)	__STRING(x)
#define __str2(x)	__str3(x)
#define __str1(x)	__str2(x)
#define dosyscall()	asm(" call " __str1(SYSCADDR))

#ifdef UZIX_MODULE
#ifndef HI_TECH_C
int modulereg(uint sig, int (*func)())
	{
	unsigned int error;
	unsigned int result;

	error = __unix(40, &result, sig, func);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (int)result;
	}

int moduledereg(uint sig)
	{
	unsigned int error;
	unsigned int result;

	error = __unix(41, &result, sig);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (int)result;
	}

int modulecall(uint sig, uint fcn, char *args, int argsize)
	{
	unsigned int error;
	unsigned int result;

	error = __unix(42, &result, sig, fcn, args, argsize);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (int)result;
	}

int modulesendreply(int pid, int fcn, char *repladdr, int replysize)
	{
	unsigned int error;
	unsigned int result;

	error = __unix(43, &result, pid, fcn, repladdr, replysize);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (int)result;
	}

int modulereply(int sig, int fcn, char *repladdr)
	{
	unsigned int error;
	unsigned int result;

	error = __unix(44, &result, sig, fcn, repladdr);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (int)result;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");

        asm("   global  _modulereg");
        asm("   signat  _modulereg, 8250");
        asm("_modulereg:");
        asm("   ld hl,40");
	asm("	jp __sys__2");

        asm("   global  _moduledereg");
        asm("   signat  _moduledereg, 4154");
        asm("_moduledereg:");
        asm("   ld hl,41");
        asm("   jp __sys__1");

        asm("   global  _modulecall");
        asm("   signat  _modulecall, 16442");
        asm("_modulecall:");
        /* DE=module sign
           BC=function
           on stack: retaddr args argsize
         */
        asm("	exx");
        asm("	pop bc");	/* BC'=ret addr */
        asm("	pop de");	/* DE'=args */
        asm("	pop hl");	/* HL'=argsize */
        asm("	push bc");
        asm("	push hl");
        asm("	push de");
        asm("	exx");		/* now stack is: args argsize retaddr */
        asm("	push bc");
        asm("	push de");	/* now is: DE BC args argsize retaddr */
        asm("   ld hl,42");     /* syscall */
	asm("	push hl");
	dosyscall();
	asm("	inc sp");
	asm("	inc sp");	/* discards syscall number */
	asm("	inc sp");
        asm("   inc sp");       /* discards argsize */
	asm("	inc sp");
        asm("   inc sp");       /* discards args */
	asm("	inc sp");
        asm("   inc sp");       /* discards fcn */
	asm("	inc sp");
        asm("   inc sp");       /* discards sig */
	asm("	ex de,hl");	/* cancel EX DE,HL in __ret__ */
	asm("	jp __ret__");

        asm("   global  _modulesendreply");
        asm("   signat  _modulesendreply, 16442");
        asm("_modulesendreply:");
        /* DE=PID
           BC=function
           on stack: retaddr args argsize
         */
        asm("	exx");
        asm("	pop bc");	/* BC'=ret addr */
        asm("	pop de");	/* DE'=args */
        asm("	pop hl");	/* HL'=argsize */
        asm("	push bc");
        asm("	push hl");
        asm("	push de");
        asm("	exx");		/* now stack is: args argsize retaddr */
        asm("	push bc");
        asm("	push de");	/* now is: DE BC args argsize retaddr */
        asm("   ld hl,43");     /* syscall */
	asm("	push hl");
	dosyscall();
	asm("	inc sp");
	asm("	inc sp");	/* discards syscall number */
	asm("	inc sp");
        asm("   inc sp");       /* discards replysize */
	asm("	inc sp");
        asm("   inc sp");       /* discards repladdr */
	asm("	inc sp");
        asm("   inc sp");       /* discards fcn */
	asm("	inc sp");
        asm("   inc sp");       /* discards pid */
	asm("	ex de,hl");	/* cancel EX DE,HL in __ret__ */
	asm("	jp __ret__");

        asm("   global  _modulereply");
        asm("   signat  _modulereply, 12346");
        asm("_modulereply:");
        /* DE=module sign
           BC=function
           on stack: retaddr addr
         */
        asm("	exx");
        asm("	pop bc");	/* BC'=ret addr */
        asm("	pop de");	/* DE'=addr */
        asm("	push bc");
        asm("	push de");
        asm("	exx");		/* now stack is: addr retaddr */
        asm("	push bc");
        asm("	push de");	/* now is: DE BC addr retaddr */
        asm("   ld hl,44");     /* syscall */
	asm("	push hl");
	dosyscall();
	asm("	inc sp");
	asm("	inc sp");	/* discards syscall number */
	asm("	inc sp");
        asm("   inc sp");       /* discards repladdr */
	asm("	inc sp");
        asm("   inc sp");       /* discards fcn */
	asm("	inc sp");
        asm("   inc sp");       /* discards sig */
	asm("	ex de,hl");	/* cancel EX DE,HL in __ret__ */
	asm("	jp __ret__");
#endif
#endif


/********** MSX-UZIX version of syscalls ************/
/* int mount(char *spec, char *dir, int rwflag); */

#ifdef MODULE_mount
#ifndef HI_TECH_C
int mount(char *spec, char *dir, int rwflag)
	{
	unsigned int error;

	error = __unix(19, spec, dir, rwflag);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__3");
	asm("	global	_mount");
	asm("	signat	_mount,12346");
	asm("_mount:");
	asm("	pop hl");
	asm("	ex (sp),hl");
	asm("	push hl");
	asm("	ld hl,19");
	asm("	jp __sys__3");
#endif
#endif
/********** MSX-UZIX version of syscalls ************/
/* int open(const char *name, int flag, ...); */

#define __str3(x)	__STRING(x)
#define __str2(x)	__str3(x)
#define __str1(x)	__str2(x)
#define dosyscall()	asm(" call " __str1(SYSCADDR))

#ifdef MODULE_open
#ifndef HI_TECH_C
int open(const char *name, int flag, int arg, ...) /* IAR use stack params */
	{
	unsigned int error;
	unsigned int result;

	error = __unix(20, &result, name, flag, arg);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (int)result;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_open");
	asm("	signat	_open,4122");
	asm("_open:");
	asm("	push ix");
	asm("	ld ix,0");
	asm("	add ix,sp");
	asm("	ld l,(ix+6)");
	asm("	ld h,(ix+7)");
	asm("	push hl");	/* last parameter in stack */
	asm("	ld l,(ix+4)");
	asm("	ld h,(ix+5)");
	asm("	push hl");	/* flag in stack */
	asm("	push de");	/* name in stack */
	asm("	ld hl,20");
	asm("	push hl");
	dosyscall();
	asm("	call __ret__");
	asm("	pop ix"); /* discards HL (syscall number) */
	asm("	pop ix"); /* discards DE */
	asm("	pop ix"); /* discards first HL */
	asm("	pop ix"); /* discards second HL */
	asm("	pop ix"); /* restore original IX */
	asm("	ret");
#endif
#endif
/********** MSX-UZIX version of syscalls ************/
/* int pause(void); */

#ifdef MODULE_pause
#ifndef HI_TECH_C
int pause(void)
	{
	unsigned int error;

	error = __unix(21);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#else
	asm("	psect text, &result,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_pause");
	asm("	signat	_pause,26");
	asm("_pause:");
	asm("	ld hl,21");
	asm("	jp __sys__");
#endif
#endif


/********** MSX-UZIX version of syscalls ************/
/* int pipe(int fildes[]); */

#ifdef MODULE_pipe
#ifndef HI_TECH_C
int pipe(int fildes[])
	{
	unsigned int error;

	error = __unix(22, fildes);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_pipe");
	asm("	signat	_pipe,4154");
	asm("_pipe:");
	asm("	ld hl,22");
	asm("	jp __sys__1");
#endif
#endif


/********** MSX-UZIX version of syscalls ************/
/* int read(int d, void *buf, uint nbytes); */

#ifdef MODULE_read
#ifndef HI_TECH_C
int read(int d, void *buf, uint nbytes)
	{
	unsigned int error;
	unsigned int result;

	error = __unix(23, &result, d, buf, nbytes);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (int)result;
	}
#else
	asm("	psect text,class=CODE");
        asm("   global __sys__3");
	asm("	global	_read");
	asm("	signat	_read,12346");
	asm("_read:");
	asm("	pop hl");
	asm("	ex (sp),hl");
	asm("	push hl");
	asm("	ld hl,23");
	asm("	jp __sys__3");
#endif
#endif


/********** MSX-UZIX version of syscalls ************/
/* int reboot(char p1, char p2); */

#ifdef MODULE_reboot
#ifndef HI_TECH_C
int reboot(char p1, char p2)
	{
	unsigned int error;

	error = __unix(37, p1, p2);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0; /* can never get here */
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_reboot");
	asm("	signat	_reboot,8250");
	asm("_reboot:");
	asm("	ld b,0");
	asm("	ld d,0");
	/* p1/p2 are char, not int, so D and B must be zeroed */
	asm("	ld hl,37");
	asm("	jp __sys__2");
#endif
#endif


/********** MSX-UZIX version of syscalls ************/
/* int sbrk(uint incr); */

#ifdef MODULE_sbrk
#ifndef HI_TECH_C
/* void abyte(int value); */

int sbrk(uint incr)
	{
	unsigned int error;
	unsigned int result;

/* abyte('s'); */
	error = __unix(24, &result, incr);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (int)result;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_sbrk");
	asm("	signat	_sbrk,4154");
	asm("_sbrk:");
	asm("	ld hl,24");
	asm("	jp __sys__1");
#endif
#endif


/********** MSX-UZIX version of syscalls ************/
/* int seek(int file, uint offset, int flag); */

#define __str3(x)	__STRING(x)
#define __str2(x)	__str3(x)
#define __str1(x)	__str2(x)
#define dosyscall()	asm(" call " __str1(SYSCADDR))

#if 0 /*def MODULE_lseek*/ /* should be MODULE_seek */
#ifndef HI_TECH_C
int seek(int file, uint offset, int flag)
	{
	unsigned int error;
	unsigned int result;

	error = __unix(25, &result, file, offset, flag);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (int)result;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_lseek");
	asm("	signat	_lseek,12348");
	asm("_lseek:");
	asm("	pop hl");	/* HL=ret addr */
	asm("	push ix");	/* save IX */
	asm("	ld ix,0");
	asm("	add ix,sp");
	asm("	ld c,(ix+6)");
	asm("	ld b,(ix+7)");	/* BC=flag */
	asm("	push bc");
	asm("	ld (ix+6),l");
	asm("	ld (ix+7),h");	/* put ret addr after arguments in stack */
	asm("	pop ix");	/* IX=flag (was in BC) */
	asm("	ex (sp),ix");	/* IX=original value, put flag in stack */
	asm("	pop hl");	/* HL=flag */
	asm("	pop bc");	/* BC=offset lsw */
	asm("	ex (sp),hl");	/* put flag in stack, HL=offset msw */
	asm("	push hl");	/* put offset msw in stack */
	asm("	push bc");	/* put offset lsw in stack */
	asm("	push de");	/* put file in stack */
	asm("	ld hl,25");	/* syscall */
	asm("	push hl");
	dosyscall();
	asm("	inc sp");
	asm("	inc sp");	/* discards syscall number */
	asm("	inc sp");
	asm("	inc sp");	/* discards flag */
	asm("	inc sp");
	asm("	inc sp");
	asm("	inc sp");
	asm("	inc sp");	/* discards offset */
	asm("	inc sp");
	asm("	inc sp");	/* discards file */
	asm("	ex de,hl");	/* cancel EX DE,HL in __ret__ */
	asm("	jp __ret__");
#endif
#endif
/********** MSX-UZIX version of syscalls ************/
/* int setgid(int gid); */

#define __str3(x)	__STRING(x)
#define __str2(x)	__str3(x)
#define __str1(x)	__str2(x)

#ifdef MODULE_setgid
#ifndef HI_TECH_C
int setgid(int gid)
	{
	unsigned int error;
	unsigned int result;

#if 0
	__unix_set(SET_GID, gid);
#else
	error = __unix(7, &result, SET_GID, gid);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (int)result;
#endif
	}
#else
	asm("	psect text,class=CODE");
        asm("   global __setset");
	asm("	global	_setgid");
	asm("	signat	_setgid,4154");
	asm("_setgid:");
	asm("	push de");
	asm("	ld de," __str1(SET_GID));
	asm("	jp __setset");
#endif
#endif


/********** MSX-UZIX version of syscalls ************/
/* int setprio(int pid, char prio); */

#define __str3(x)	__STRING(x)
#define __str2(x)	__str3(x)
#define __str1(x)	__str2(x)

#ifdef MODULE_setprio
#ifndef HI_TECH_C
int setprio(int pid, char prio)
	{
	unsigned int error;
	unsigned int result;

	error = __unix(7, &result, SET_PRIO, pid, prio);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (int)result;
	}
#else
	asm("	psect text,class=CODE");
        asm("   global __sys__3");
	asm("	global	_setprio");
	asm("	signat	_setprio,8250");
	asm("_setprio:");
	asm("	ld b,0");
	asm("	push bc");
	asm("	ld b,d");
	asm("	ld c,e");
	asm("	ld de," __str1(SET_PRIO));
	asm("	ld hl,7");
	asm("	jp __sys__3");
#endif
#endif


/********** MSX-UZIX version of syscalls ************/
/* int setuid(int uid); */

#define __str3(x)	__STRING(x)
#define __str2(x)	__str3(x)
#define __str1(x)	__str2(x)

#ifdef MODULE_setuid
#ifndef HI_TECH_C
int setuid(int uid)
	{
	unsigned int error;
	unsigned int result;

#if 0
	__unix_set(SET_UID, uid);
#else
	error = __unix(7, &result, SET_UID, uid);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (int)result;
#endif
	}
#else
	asm("	psect text,class=CODE");
        asm("   global __setset");
	asm("	global	_setuid");
	asm("	signat	_setuid,4154");
	asm("_setuid:");
	asm("	push de");
	asm("	ld de," __str1(SET_UID));
	asm("	jp __setset");
#endif
#endif


/********** MSX-UZIX version of syscalls ************/
/* int signal(signal_t sig, sig_t func); */

#ifdef MODULE_signal
#ifndef HI_TECH_C
sig_t signal(signal_t sig, sig_t func)
	{
	unsigned int error;
	unsigned long result;

	error = __unix(26, &result, sig, (unsigned long)func);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (sig_t)result;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_signal");
	asm("	signat	_signal,8250");
	asm("_signal:");
	asm("	ld d,0");	/* SIG is 8 uchar, but is passed to int as uint */
	asm("	ld hl,26");
	asm("	jp __sys__2");
#endif
#endif


/********** MSX-UZIX version of syscalls ************/
/* int stat(char *path, struct stat *buf); */

#ifdef MODULE_stat
#ifndef HI_TECH_C
int stat(char *path, struct stat *buf)
	{
	unsigned int error;

	error = __unix(27, path, buf);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_stat");
	asm("	signat	_stat,8250");
	asm("_stat:");
	asm("	ld hl,27");
	asm("	jp __sys__2");
#endif
#endif


/********** MSX-UZIX version of syscalls ************/
/* int stime(time_t *tvec); */

#ifdef MODULE_stime
#ifndef HI_TECH_C
int stime(time_t *tvec)
	{
	unsigned int error;

	error = __unix(28, *tvec);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_stime");
	asm("	signat	_stime,4154");
	asm("_stime:");
	asm("	ld hl,28");
	asm("	jp __sys__1");
#endif
#endif


/********** MSX-UZIX version of syscalls ************/
/* int symlink(char *oldname, char *newname); */

#ifdef MODULE_symlink
#ifndef HI_TECH_C
int symlink(char *oldname, char *newname)
	{
	unsigned int error;

	error = __unix(38, oldname, newname);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_symlink");
	asm("	signat	_symlink,8250");
	asm("_symlink:");
	asm("	ld hl,38");
	asm("	jp __sys__2");
#endif
#endif
/********** MSX-UZIX version of syscalls ************/
/* int sync(void); */

#ifdef MODULE_sync
#ifndef HI_TECH_C
void sync(void)
	{
	__unix(29);
	}
#else
	asm("	psect text, &result,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_sync");
	asm("	signat	_sync,26");
	asm("_sync:");
	asm("	ld hl,29");
	asm("	jp __sys__");
#endif
#endif


/********** MSX-UZIX version of syscalls ************/
/* int sysdebug(int onoff); */

#define __str3(x)	__STRING(x)
#define __str2(x)	__str3(x)
#define __str1(x)	__str2(x)

#ifdef MODULE_sysdebug
#ifndef HI_TECH_C
int sysdebug(int onoff)
	{
	unsigned int result;

#if 0
	__unix_set(SET_DEBUG, onoff);
#else
	__unix(7, &result, SET_DEBUG, onoff);
	return (int)result;
#endif
	}
#else
	asm("	psect text,class=CODE");
        asm("   global __setset");
	asm("	global	_sysdebug");
	asm("	signat	_sysdebug,4154");
	asm("_sysdebug:");
	asm("	push de");
	asm("	ld de," __str1(SET_DEBUG));
	asm("	jp __setset");
#endif
#endif


/********** MSX-UZIX version of syscalls ************/
/* int systrace(int onoff); */

#define __str3(x)	__STRING(x)
#define __str2(x)	__str3(x)
#define __str1(x)	__str2(x)

#ifdef MODULE_systrace
#ifndef HI_TECH_C
int systrace(int onoff)
	{
	unsigned int error;
	unsigned int result;

#if 0
	__unix_set(SET_TRACE, onoff);
#else
	error = __unix(7, &result, SET_TRACE, onoff);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (int)result;
#endif
	}
#else
	asm("	psect text,class=CODE");
        asm("   global __setset");
	asm("	global	_systrace");
	asm("	signat	_systrace,4154");
	asm("_systrace:");
	asm("	push de");
	asm("	ld de," __str1(SET_TRACE));
	asm("	jp __setset");
#endif
#endif


/********** MSX-UZIX version of syscalls ************/
/* time_t time(time_t *tvec); */

#ifdef MODULE_time
time_t time(time_t *tvec)
	{
	unsigned int error;
	rtctime_t result;

	error = __unix(30, &result);
	if (error)
		{
		errno = error;
		return (time_t)-1;
		}
	if (tvec)
		{
		*tvec = (time_t)result;
		}
	return (time_t)result;
	}
#endif
/********** MSX-UZIX version of syscalls ************/
/* int times(struct tms *tvec); */

#ifdef MODULE_times
#ifndef HI_TECH_C
int times(struct tms *tvec)
	{
	unsigned int error;

	error = __unix(31, tvec);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_times");
	asm("	signat	_times,4154");
	asm("_times:");
	asm("	ld hl,31");
	asm("	jp __sys__1");
#endif
#endif


/********** MSX-UZIX version of syscalls ************/
/* int umask(int mask); */

#define __str3(x)	__STRING(x)
#define __str2(x)	__str3(x)
#define __str1(x)	__str2(x)

#ifdef MODULE_umask
#ifndef HI_TECH_C
int umask(int mask)
	{
	unsigned int error;
	unsigned int result;

#if 0
	__unix_set(SET_UMASK, mask);
#else
	error = __unix(7, &result, SET_UMASK, mask);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (int)result;
#endif
	}
#else
	asm("	psect text,class=CODE");
        asm("   global __setset");
	asm("	global	_umask");
	asm("	signat	_umask,4154");
	asm("_umask:");
	asm("	push de");
	asm("	ld de," __str1(SET_UMASK));
	asm("	jp __setset");
#endif
#endif


/********** MSX-UZIX version of syscalls ************/
/* int umount(char *spec); */

#ifdef MODULE_umount
#ifndef HI_TECH_C
int umount(char *spec)
	{
	unsigned int error;

	error = __unix(32, spec);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_umount");
	asm("	signat	_umount,4154");
	asm("_umount:");
	asm("	ld hl,32");
	asm("	jp __sys__1");
#endif
#endif


/********** MSX-UZIX version of syscalls ************/
/* int unlink(const char *path); */

#ifdef MODULE_unlink
#ifndef HI_TECH_C
int unlink(const char *path)
	{
	unsigned int error;

	error = __unix(33, path);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_unlink");
	asm("	signat	_unlink,4154");
	asm("_unlink:");
	asm("	ld hl,33");
	asm("	jp __sys__1");
#endif
#endif


/********** MSX-UZIX version of syscalls ************/
/* int utime(char *path, struct utimbuf *buf); */

#ifdef MODULE_utime
#ifndef HI_TECH_C
int utime(char *path, struct utimbuf *buf)
	{
	unsigned int error;

	error = __unix(34, path, buf);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_utime");
	asm("	signat	_utime,8250");
	asm("_utime:");
	asm("	ld hl,34");
	asm("	jp __sys__2");
#endif
#endif


/********** MSX-UZIX version of syscalls ************/
/* int waitpid(int pid, int *statloc, int options); */

#ifdef MODULE_waitpid
#ifndef HI_TECH_C
int waitpid(int pid, int *statloc, int options)
	{
	unsigned int error;
	unsigned int result;

	error = __unix(35, &result, pid, statloc, options);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (int)result;
	}
#else
	asm("	psect text,class=CODE");
        asm("   global __sys__3");
	asm("	global	_waitpid");
	asm("	signat	_waitpid,12346");
	asm("_waitpid:");
	asm("	pop hl");
	asm("	ex (sp),hl");
	asm("	push hl");
	asm("	ld hl,35");
	asm("	jp __sys__3");
#endif
#endif
/********** MSX-UZIX version of syscalls ************/
/* int write(int d, const void *buf, uint nbytes); */

#ifdef MODULE_write
#ifndef HI_TECH_C
int write(int d, const void *buf, uint nbytes)
	{
	unsigned int error;
	unsigned int result;

	error = __unix(36, &result, d, buf, nbytes);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (int)result;
	}
#else
	asm("	psect text,class=CODE");
        asm("   global __sys__3");
	asm("	global	_write");
	asm("	signat	_write,12346");
	asm("_write:");
	asm("	pop hl");
	asm("	ex (sp),hl");
	asm("	push hl");
	asm("	ld hl,36");
	asm("	jp __sys__3");
#endif
#endif

/* the following routines were added by Nick */

#ifdef MODULE_falign
int falign(int fd, int parm)
	{
	unsigned int error;

	error = __unix(40, fd, parm);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#endif

#ifdef MODULE_vfork
int vfork(void)
	{
	unsigned int error;
	unsigned int result;

	error = __unix(41, &result);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (int)result;
	}
#endif

#ifdef MODULE_socket
int
socket(int af, int type, int pf)
	{
	unsigned int error;
	unsigned int result;

	error = __unix(42, &result, af, type, pf);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (int)result;
	}
#endif

#ifdef MODULE_bind
int
bind(int fd, struct sockaddr *name, int namelen)
	{
	unsigned int error;

	error = __unix(43, fd, name, namelen);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#endif

#ifdef MODULE_listen
int
listen(int fd, int backlog)
	{
	unsigned int error;

	error = __unix(44, fd, backlog);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#endif

#ifdef MODULE_connect
int
connect(int fd, struct sockaddr *peername, int peernamelen)
	{
	unsigned int error;

	error = __unix(45, fd, peername, peernamelen);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#endif

#ifdef MODULE_accept
int
accept(int fd, struct sockaddr *peername, int *peernamelen)
	{
	unsigned int error;
	unsigned int result;

	error = __unix(46, &result, fd, peername, peernamelen);
	if (error)
		{
		errno = error;
		return -1;
		}
	return (int)result;
	}
#endif

#ifdef MODULE_shutdown
int
shutdown(int fd, int flag)
	{
	unsigned int error;

	error = __unix(47, fd, flag);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#endif

#ifdef MODULE_socketpair
int
socketpair(int af, int type, int pf, int *fd_p)
	{
	unsigned int error;

	error = __unix(48, af, type, pf, fd_p);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#endif

#ifdef MODULE_recv
ssize_t
recv(int fd, void *data, size_t count, int flag)
	{
	unsigned int error;
	size_t result;

	error = __unix(49, &result, fd, data, count, flag);
	if (error)
		{
		errno = error;
		return (ssize_t)-1;
		}
	return (ssize_t)result;
	}
#endif

#ifdef MODULE_recvfrom
ssize_t
recvfrom(int fd, void *data, size_t count, int flag,
		struct sockaddr *from, int *fromlen)
	{
	unsigned int error;
	size_t result;

	error = __unix(50, &result, fd, data, count, flag, from, fromlen);
	if (error)
		{
		errno = error;
		return (ssize_t)-1;
		}
	return (ssize_t)result;
	}
#endif

#ifdef MODULE_send
ssize_t
send(int fd, void *data, size_t count, int flag)
	{
	unsigned int error;
	size_t result;

	error = __unix(51, &result, fd, data, count, flag);
	if (error)
		{
		errno = error;
		return (ssize_t)-1;
		}
	return (ssize_t)result;
	}
#endif

#ifdef MODULE_sendto
ssize_t
sendto(int fd, void *data, size_t count, int flag,
		struct sockaddr *to, int tolen)
	{
	unsigned int error;
	size_t result;

	error = __unix(52, &result, fd, data, count, flag, to, tolen);
	if (error)
		{
		errno = error;
		return (ssize_t)-1;
		}
	return (ssize_t)result;
	}
#endif

#ifdef MODULE_getsockname
int
getsockname(int fd, struct sockaddr *name, int *namelen)
	{
	unsigned int error;

	error = __unix(53, fd, name, namelen);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#endif

#ifdef MODULE_getpeername
int
getpeername(int fd, struct sockaddr *peername, int *peernamelen)
	{
	unsigned int error;

	error = __unix(54, fd, peername, peernamelen);
	if (error)
		{
		errno = error;
		return -1;
		}
	return 0;
	}
#endif

