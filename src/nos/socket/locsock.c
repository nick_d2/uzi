#include <errno.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include "socket/socket.h"
#include "socket/usock.h"
#include <libintl.h>
#include "po/messages.h"

int
so_los(struct usock *up,int protocol)
{
	up->cb.local = (struct loc *)callocw(1,sizeof(struct loc));
	up->cb.local->peer = up;	/* connect to self */
	up->type = TYPE_LOCAL_STREAM;
	up->cb.local->hiwat = LOCSFLOW;
	return 0;
}

int
so_lod(struct usock *up,int protocol)
{
	up->cb.lod = (struct lod *)callocw(1,sizeof(struct lod));
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	up->cb.lod->rcvq.rp = up->cb.lod->rcvq.buf;
	up->cb.lod->rcvq.wp = up->cb.lod->rcvq.buf;
#endif
	up->cb.lod->peer = up;	/* connect to self */
	up->type = TYPE_LOCAL_DGRAM;
	up->cb.lod->hiwat = LOCDFLOW;
	return 0;
}

int
so_los_recv(
struct usock *up,
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
void *buf,
int cnt,
#else
struct mbuf_s **bpp,
#endif
struct sockaddr *from,
int *fromlen
){
	int s;
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
	struct mbuf_s *bp;
#endif

	while(up->cb.local != NULL && up->cb.local->q == NULL
	  && up->cb.local->peer != NULL){
		if(up->noblock){
			errno = EWOULDBLOCK;
			return -1;
		} else if((errno = kwait(up)) != 0){
			return -1;
		}
	}
	if(up->cb.local == NULL){
		/* Socket went away */
		errno = EBADF;
		return -1;
	}
	if(up->cb.local->q == NULL &&
	   up->cb.local->peer == NULL){
		errno = ENOTCONN;
		return -1;
	}
	/* For datagram sockets, this will return the
	 * first packet on the queue. For stream sockets,
	 * this will return everything.
	 */
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
#if 1 /* fix for stream sockets so receive is limited by user parameter */
	cnt = pullup(&up->cb.local->q, buf, (uint16)cnt);
#else
	/* note: needs to be fixed to avoid data loss with stream sockets */
	bp = dequeue(&up->cb.local->q);

	cnt = pullup(&bp, buf, (uint16)cnt);
	free_p(&bp);
#endif
#else
	*bpp = dequeue(&up->cb.local->q);
#endif
	if(up->cb.local->q == NULL && (up->cb.local->flags & LOC_SHUTDOWN)){
		s = up->index;
		close(s); /* PLEASE REVISIT THIS! close_s(s); */
	}
	ksignal(up,0);
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
	return cnt;
#else
	return len_p(*bpp);
#endif
}

int
so_lod_recv(
struct usock *up,
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
void *buf,
int cnt,
#else
struct mbuf_s **bpp,
#endif
struct sockaddr *from,
int *fromlen
){
	int s;
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
	struct mbuf_s *bp;
#endif

	while(up->cb.lod != NULL && up->cb.lod->rcvq.cnt == 0
	  && up->cb.lod->peer != NULL){
		if(up->noblock){
			errno = EWOULDBLOCK;
			return -1;
		} else if((errno = kwait(up)) != 0){
			return -1;
		}
	}
	if(up->cb.lod == NULL){
		/* Socket went away */
		errno = EBADF;
		return -1;
	}
	if(up->cb.lod->rcvq.cnt == 0 &&
	   up->cb.lod->peer == NULL){
		errno = ENOTCONN;
		return -1;
	}
	/* For datagram sockets, this will return the
	 * first packet on the queue. For stream sockets,
	 * this will return everything.
	 */
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
	up->cb.lod->rcvq.cnt--;

	bp = *up->cb.lod->rcvq.rp++;
	if (up->cb.lod->rcvq.rp >= up->cb.lod->rcvq.buf + RECEIVE_ENTRIES)
		{
		up->cb.lod->rcvq.rp = up->cb.lod->rcvq.buf;
		}

	cnt = pullup(&bp, buf, (uint16)cnt);
	free_p(&bp);
#else
	*bpp = dequeue(&up->cb.lod->q);
#endif
	if(up->cb.lod->rcvq.cnt == 0 && (up->cb.lod->flags & LOC_SHUTDOWN)){
		s = up->index;
		close(s); /* PLEASE REVISIT THIS! close_s(s); */
	}
	ksignal(up,0);
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
	return cnt;
#else
	return len_p(*bpp);
#endif
}

int
so_los_send(
struct usock *up,
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
void *buf,
int cnt,
#else
struct mbuf_s **bpp,
#endif
struct sockaddr *to
){
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
	struct mbuf_s *bp;
#endif

	if(up->cb.local->peer == NULL){
#if 0 /* now that we don't have to support recv_mbuf() or send_mbuf() */
		free_p(bpp);
#endif
		errno = ENOTCONN;
		return -1;
	}

#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
	bp = qdata(buf, cnt);
	if (bp == NULL)
		{
		errno = ENOMEM;
		return -1;
		}
	append(&up->cb.local->peer->cb.local->q,&bp);
#else
	append(&up->cb.local->peer->cb.local->q,bpp);
#endif
	ksignal(up->cb.local->peer,0);
	/* If high water mark has been reached, block */
	while(up->cb.local->peer != NULL &&
	      len_p(up->cb.local->peer->cb.local->q) >=
	      up->cb.local->peer->cb.local->hiwat){
		if(up->noblock){
			errno = EWOULDBLOCK;
			return -1;
		} else if((errno = kwait(up->cb.local->peer)) != 0){
			return -1;
		}
	}
	if(up->cb.local->peer == NULL){
		errno = ENOTCONN;
		return -1;
	}
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
	return cnt;
#else
	return 0;
#endif
}

int
so_lod_send(
struct usock *up,
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
void *buf,
int cnt,
#else
struct mbuf_s **bpp,
#endif
struct sockaddr *to
){
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
	struct mbuf_s *bp;
#endif

	if(up->cb.lod->peer == NULL){
#if 0 /* now that we don't have to support recv_mbuf() or send_mbuf() */
		free_p(bpp);
#endif
		errno = ENOTCONN;
		return -1;
	}

#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	if (up->cb.lod->rcvq.cnt < RECEIVE_ENTRIES)
		{
		up->cb.lod->rcvq.overrun++;

		errno = EOVERFLOW;
 amess("so_lod_send() drop\r\n");
		return -1;
		}

	bp = qdata(buf, cnt);
	if (bp == NULL)
		{
		errno = ENOMEM;
		return -1;
		}

	*up->cb.lod->rcvq.wp++ = bp;
	if (up->cb.lod->rcvq.wp >= up->cb.lod->rcvq.buf + RECEIVE_ENTRIES)
		{
		up->cb.lod->rcvq.wp = up->cb.lod->rcvq.buf;
		}
	up->cb.lod->rcvq.cnt++;

	/* cast shouldn't be needed */
	/*ksignal((void *)&up->cb.lod->rcvq.cnt, 0);*/
	if (up->cb.lod->rcvq.hiwat < up->cb.lod->rcvq.cnt)
		{
		up->cb.lod->rcvq.hiwat = up->cb.lod->rcvq.cnt;
		}

	bp = NULL;
#else
	bp = qdata(buf, cnt);
	if (bp == NULL)
		{
		errno = ENOMEM;
		return -1;
		}
	enqueue(&up->cb.lod->peer->cb.lod->q,&bp);
#endif
#else
	enqueue(&up->cb.lod->peer->cb.lod->q,bpp);
#endif
	ksignal(up->cb.lod->peer,0);
	/* If high water mark has been reached, block */
	while(up->cb.lod->peer != NULL &&
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	      up->cb.lod->peer->cb.lod->rcvq.cnt >=
#else
	      len_q(up->cb.lod->peer->cb.lod->q) >=
#endif
	      up->cb.lod->peer->cb.lod->hiwat){
		if(up->noblock){
			errno = EWOULDBLOCK;
			return -1;
		} else if((errno = kwait(up->cb.lod->peer)) != 0){
			return -1;
		}
	}
	if(up->cb.lod->peer == NULL){
		errno = ENOTCONN;
		return -1;
	}
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
	return cnt;
#else
	return 0;
#endif
}

int
so_los_qlen(struct usock *up,int rtx)
{
	int len;

	switch(rtx){
	case 0:
		len = len_p(up->cb.local->q);
		break;
	case 1:
		if(up->cb.local->peer != NULL)
			len = len_p(up->cb.local->peer->cb.local->q);
		break;
	}
	return len;
}

int
so_lod_qlen(struct usock *up,int rtx)
{
	int len;

	switch(rtx){
	case 0:
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
		len = up->cb.lod->rcvq.cnt;
#else
		len = len_q(up->cb.lod->q);
#endif
		break;
	case 1:
		if(up->cb.lod->peer != NULL)
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
			len = up->cb.lod->peer->cb.lod->rcvq.cnt;
#else
			len = len_q(up->cb.lod->peer->cb.lod->q);
#endif
		break;
	}
	return len;
}

int
so_los_shut(struct usock *up,int how)
{
	int s;

	s = up->index;

	if(up->cb.local->q == NULL)
		close(s); /* PLEASE REVISIT THIS! close_s(s); */
	else
		up->cb.local->flags = LOC_SHUTDOWN;
	return 0;
}

int
so_lod_shut(struct usock *up,int how)
{
	int s;

	s = up->index;

	if(up->cb.lod->rcvq.cnt == 0)
		close(s); /* PLEASE REVISIT THIS! close_s(s); */
	else
		up->cb.lod->flags = LOC_SHUTDOWN;
	return 0;
}

int
so_los_close(struct usock *up)
{
	if(up->cb.local->peer != NULL){
		up->cb.local->peer->cb.local->peer = NULL;
		ksignal(up->cb.local->peer,0);
	}

	/* Drop any leftover receive data */
	free_p(&up->cb.local->q);

	free(up->cb.local);
	return 0;
}

int
so_lod_close(struct usock *up)
{
	struct mbuf_s **op;
	unsigned int tmp, cnt;

	if(up->cb.lod->peer != NULL){
		up->cb.lod->peer->cb.lod->peer = NULL;
		ksignal(up->cb.lod->peer,0);
	}

	/* Drop any leftover receive packets */
	op = up->cb.lod->rcvq.rp;
	cnt = up->cb.lod->rcvq.cnt;

	for (tmp = 0; tmp < cnt; tmp++)
		{
		free_p(op);

		op++;
		if (op >= up->cb.lod->rcvq.buf + RECEIVE_ENTRIES)
			{
			op = up->cb.lod->rcvq.buf;
			}
		}

	free(up->cb.lod);
	return 0;
}

char *
lopsocket(struct sockaddr *p)
{
	return "";
}

int
so_loc_stat(struct usock *up)
{
	int s;

	s = up->index;
	_printf(_("Inqlen: %d packets\n"),socklen(s,0));
	_printf(_("Outqlen: %d packets\n"),socklen(s,1));
	return 0;
}

