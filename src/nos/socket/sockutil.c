/* Application programming interface routines - based loosely on the
 * "socket" model in Berkeley UNIX.
 *
 * Copyright 1991 Phil Karn, KA9Q
 *
 * Apr 04	RPB	Po-ed most of the remaining strings
 */

/* Low level socket routines
 * Copyright 1991 Phil Karn, KA9Q
 */

#include <errno.h>
#include "main/config.h" /* for NAF */
#include "nos/global.h"
#include "main/mbuf.h"
#include "internet/netuser.h"
#include "socket/opensock.h"
#include "kernel/thread.h" /* can be removed later? */
#include <libintl.h>
#include "po/messages.h"

#ifndef MODULE
#define MODULE_Socktypes
/*#define MODULE_Sock_errlist*/
/*#define MODULE_Badsocket*/
/*#define MODULE_Usock*/
/*#define MODULE_sockinit*/
#define MODULE_socklen
#define MODULE_sockkick
#define MODULE_sockowner
/*#define MODULE_usesock*/
/*#define MODULE_freesock*/
#define MODULE_settos
#define MODULE_eolseq
#define MODULE_psocket
#define MODULE_sockerr
#define MODULE_sockstate
/*#define MODULE_itop*/
/*#define MODULE_st_garbage*/
#endif

#ifdef MODULE_Socktypes
/*RPB*/
_char *Socktypes[SOCKTYPES_MAX] = {
	N_("Not Used"),
	N_("TCP"),
	N_("UDP"),
	N_("AX25 I"),
	N_("AX25 UI"),
	N_("Raw IP"),
	N_("NETROM3"),
	N_("NETROM"),
	N_("Loc St"),
	N_("Loc Dg")
/*RPB*/
};
#endif

#if 0 /*def MODULE_Sock_errlist*/
_char *Sock_errlist[] = {
/*RPB*/
	N_("operation would block"),
	N_("not connected"),
	N_("socket type not supported"),
	N_("address family not supported"),
	N_("is connected"),
	N_("operation not supported"),
	N_("alarm"),
	N_("abort"),
	N_("interrupt"),
	N_("connection refused"),
	N_("message size"),
	N_("address in use")
/*RPB*/
};
#endif

#if 0 /*def MODULE_Badsocket*/
char Badsocket[] = "Bad socket";
#endif
#if 0 /*def MODULE_Usock*/
struct usock **Usock;		/* Socket entry array */
#endif

/* Initialize user socket array */

#if 0 /*def MODULE_sockinit*/
void
sockinit(void)
{
	if(Usock != (struct usock **)NULL)
		return;	/* Already initialized */
	Usock = (struct usock **)callocw(Nsock,sizeof(struct usock *));
}
#endif

/* Return length of protocol queue, either send or receive. */

#ifdef MODULE_socklen
int
socklen(
int s,		/* Socket index */
int rtx		/* 0 = receive queue, 1 = transmit queue */
){
	jmp_buf try_b;
	unsigned int catch;
	struct usock *up;
	struct socklink *sp;
	int len;

	catch = setjmp(try_b);
	if (catch)
		{
		errno = catch;
		return -1;
		}

	up = os_locate(try_b, s);
	if(up->cb.p == NULL){
		object_release(&up->object);
		errno = ENOTCONN;
		return -1;
	}
	if(rtx < 0 || rtx > 1){
		object_release(&up->object);
		errno = EINVAL;
		return -1;
	}
	sp = up->sp;
	/* Fail if qlen routine isn't present */
	if(sp->qlen == NULL || (len = (*sp->qlen)(up,rtx)) == -1){
		object_release(&up->object);
		errno = EOPNOTSUPP;
		return -1;
	}

	object_release(&up->object);
	return len;
}
#endif

/* Force retransmission. Valid only for connection-oriented sockets. */

#ifdef MODULE_sockkick
int
sockkick(
int s	/* Socket index */
){
	jmp_buf try_b;
	unsigned int catch;
	struct usock *up;
	struct socklink *sp;

	catch = setjmp(try_b);
	if (catch)
		{
		errno = catch;
		return -1;
		}

	up = os_locate(try_b, s);
	sp = up->sp;

	/* Fail if kick routine isn't present */
	if(sp->kick == NULL){
		object_release(&up->object);
		errno = EOPNOTSUPP;
		return -1;
	}

 	if((*sp->kick)(up) == -1)
		{
		object_release(&up->object);
		return -1;
		}

	object_release(&up->object);
	return 0;
}
#endif

/* Change owner of socket, return previous owner */

#ifdef MODULE_sockowner
struct thread_s *
sockowner(
int s,			/* Socket index */
struct thread_s *newowner	/* Process table address of new owner */
){
	jmp_buf try_b;
	unsigned int catch;
	struct usock *up;
	struct thread_s *pp;

	catch = setjmp(try_b);
	if (catch)
		{
		errno = catch;
		return NULL;
		}

	up = os_locate(try_b, s);

	pp = up->owner;
	if(newowner != NULL)
		up->owner = newowner;

	object_release(&up->object);
	return pp;
}
#endif

/* Increment reference count for specified socket */

#if 0 /*def MODULE_usesock*/
int
usesock(int s)
{
	struct usock *up;

	if((up = itop(s)) == NULL){
		errno = EBADF;
		return -1;
	}
	up->object.refs++;
	return 0;
}
#endif

/* Blow away all sockets belonging to a certain process. Used by killproc(). */

#if 0 /*def MODULE_freesock*/
void
freesock(struct thread_s *pp)
{
	register struct usock *up;
	register int i;

	for(i=0;i < Nsock;i++){
		up = Usock[i];
		if(up != NULL && up->type != NOTUSED && up->owner == pp)
			shutdown(i,2);
	}
}
#endif

/* Set Internet type-of-service to be used */

#ifdef MODULE_settos
int
settos(int s, int tos)
{
	jmp_buf try_b;
	unsigned int catch;
	struct usock *up;

	catch = setjmp(try_b);
	if (catch)
		{
		errno = catch;
		return -1;
		}

	up = os_locate(try_b, s);
	up->tos = tos;
	object_release(&up->object);

	return 0;
}
#endif

/* Return end-of-line convention for socket */

#ifdef MODULE_eolseq
char *
eolseq(int s)
{
	jmp_buf try_b;
	unsigned int catch;
	struct usock *up;
	char *eol;

	catch = setjmp(try_b);
	if (catch)
		{
		/* not a socket, use system Eol sequence */
		return Eol;
		}

/* abyte('<'); */
	up = os_locate(try_b, s);
/* abyte('>'); */
	eol = up->sp->eol;
	object_release(&up->object);

	return eol;
}
#endif

/* Convert a socket (address + port) to an ascii string of the form
 * aaa.aaa.aaa.aaa:ppppp
 */

#ifdef MODULE_psocket
char *
psocket(p)
void *p;
{
	struct sockaddr *sp;	/* Pointer to structure to decode */

	sp = (struct sockaddr *)p;
	if(sp->sa_family < AF_INET || sp->sa_family >= NAF)
		return NULL;

	return (*Psock[sp->sa_family])(sp);
}
#endif

/* Return ASCII string giving reason for connection closing */

#ifdef MODULE_sockerr
char *
sockerr(s)
int s;	/* Socket index */
{
	jmp_buf try_b;
	unsigned int catch;
	struct usock *up;
	struct socklink *sp;
	char *ret;

	catch = setjmp(try_b);
	if (catch)
		{
		errno = catch;
		return "Bad socket";
		}

	up = os_locate(try_b, s);
	sp = up->sp;

	if(sp->error)
		{
		ret = sp->error[up->errcodes[0]];
		}
	else
		{
		errno = EOPNOTSUPP;	/* not yet, anyway */
		ret = "No support";
		}

	object_release(&up->object);
	return ret;
}
#endif

/* Get state of protocol. Valid only for connection-oriented sockets. */

#ifdef MODULE_sockstate
char *
sockstate(s)
int s;		/* Socket index */
{
	jmp_buf try_b;
	unsigned int catch;
	struct usock *up;
	struct socklink *sp;
	char *ret;

	catch = setjmp(try_b);
	if (catch)
		{
		errno = catch;
		return NULL;
		}

	up = os_locate(try_b, s);
	if(up->cb.p == NULL){
		object_release(&up->object);
		errno = ENOTCONN;
		return NULL;
	}
	sp = up->sp;

	if(sp->state)
		{
		ret = (*sp->state)(up);
		}
	else
		{
		/* Datagram sockets don't have state */
		errno = EOPNOTSUPP;
		ret = NULL;
		}

	object_release(&up->object);
	return ret;
}
#endif

/* Convert a socket index to an internal user socket structure pointer */

#if 0 /*def MODULE_itop*/
struct usock *
itop(s)
register int s;	/* Socket index */
{
	if(s < 0 || _fd_type(s) != _FL_SOCK)
		return NULL;	/* Valid only for sockets */
	s = _fd_seq(s);
	if(s >= Nsock)
		return NULL;

	return Usock[s];
}
#endif

#if 0 /*def MODULE_st_garbage*/
void
st_garbage(red)
int red;
{
	int i;
	struct usock *up;

	for(i=0;i<Nsock;i++){
		up = Usock[i];
		if(up != NULL && up->type == TYPE_LOCAL_STREAM)
			mbuf_crunch(&up->cb.local->q);
	}
}
#endif

