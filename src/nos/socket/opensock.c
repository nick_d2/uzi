/*
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/* Application programming interface routines - based loosely on the
 * "socket" model in Berkeley UNIX.
 *
 * Copyright 1991 Phil Karn, KA9Q
 *
 * Apr 04	RPB	Po-ed most of the remaining strings
 */

/* Higher level user subroutines built on top of the socket primitives
 * Copyright 1991 Phil Karn, KA9Q
 */

/**********************************************************
 Socket related routines
**********************************************************/

#include <errno.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include "internet/netuser.h"
#include "driver/device.h" /* for device_find(), struct device_s */
#include "kernel/dprintf.h"
#include "filesys/maxmin.h" /* for min_uint() */
#include "kernel/object.h" /* for object_dec_refs() */
#include "socket/opensock.h"
#include "kernel/thread.h" /* for my_thread_p */
#include "kernel/userfile.h" /* for uf_alloc() */
#include <libintl.h>
#include "po/messages.h"

#ifndef DEBUG
#define DEBUG 0
#endif

#ifndef MODULE
#define MODULE_open_sock_list
#define MODULE_os_new
#define MODULE_os_deref
#define MODULE_os_garbage
#define MODULE_os_locate
#define MODULE_os_socket
#define MODULE_os_bind
#define MODULE_os_listen
#define MODULE_os_connect
#define MODULE_os_accept
#define MODULE_os_shutdown
#define MODULE_os_socketpair
#define MODULE_os_recv
#define MODULE_os_recvfrom
#define MODULE_os_send
#define MODULE_os_sendto
#define MODULE_os_getsockname
#define MODULE_os_getpeername
#endif

#ifdef MODULE_open_sock_list
struct usock *open_sock_list[OPEN_SOCK_LIST];
#endif

/* os_new() allocate entries in the open sockets table.
 */

#ifdef MODULE_os_new
struct usock *
os_new(jmp_buf error, unsigned int af, unsigned int type, unsigned int pf)
	{
	jmp_buf try_b;
	unsigned int catch;
	struct usock *os_p, **os_pp;
	struct socklink *sp;

#if DEBUG >= 3
	_dprintf(3, _("os_new(0x%x) starting\n"), error);
#endif
	for (os_pp = open_sock_list; os_pp < open_sock_list + OPEN_SOCK_LIST;
			os_pp++)
		{
		os_p = *os_pp;
		if (os_p && os_p->object.refs == 0)
			{
			goto found1;
			}
		}

	for (os_pp = open_sock_list; os_pp < open_sock_list + OPEN_SOCK_LIST;
			os_pp++)
		{
		os_p = *os_pp;
		if (os_p == NULL)
			{
			os_p = malloc(sizeof(struct usock));
			if (os_p == NULL)
				{
#if DEBUG >= 3
				_dprintf(3, _("os_new() error ENOMEM\n"));
#endif
				longjmp(error, ENOMEM);
				}
			*os_pp = os_p;
			goto found1;
			}
		}

#if DEBUG >= 3
	_dprintf(3, _("os_new() error ENFILE\n"));
#endif
	longjmp(error, ENFILE);

found1:
	memset(os_p, 0, sizeof(struct usock));
	os_p->object.busy = 1;
	os_p->object.refs = 1;
	os_p->object.type = _FL_SOCK;

	catch = setjmp(try_b);
	if (catch)
		{
		object_release(&os_p->object);
		os_p->object.refs = 0;

		longjmp(error, catch);
		}

	switch (af)
		{
	case AF_LOCAL:
		switch (type)
			{
		case SOCK_STREAM:
			os_p->type = TYPE_LOCAL_STREAM;
			break;
		case SOCK_DGRAM:
			os_p->type = TYPE_LOCAL_DGRAM;
			break;
		default:
			longjmp(try_b, ESOCKTNOSUPPORT);
			}
		break;
	case AF_AX25:
		switch (type)
			{
		case SOCK_STREAM:
			os_p->type = TYPE_AX25I;
			break;
		case SOCK_DGRAM:
			os_p->type = TYPE_AX25UI;
			break;
		default:
			longjmp(try_b, ESOCKTNOSUPPORT);
			}
		break;
	case AF_NETROM:
		switch (type)
			{
		case SOCK_RAW:
			os_p->type = TYPE_NETROML3;
			break;
		case SOCK_SEQPACKET:
			os_p->type = TYPE_NETROML4;
			break;
		default:
			longjmp(try_b, ESOCKTNOSUPPORT);
			}
		break;
	case AF_INET:
		switch (type)
			{
		case SOCK_STREAM:
			os_p->type = TYPE_TCP;
			break;
		case SOCK_DGRAM:
			os_p->type = TYPE_UDP;
			break;
		case SOCK_RAW:
			os_p->type = TYPE_RAW;
			break;
		default:
			longjmp(try_b, ESOCKTNOSUPPORT);
			}
		break;
	default:
		longjmp(try_b, EAFNOSUPPORT);
	}

	/* Look for entry in protocol table */
	for (sp = Socklink; sp->type != -1; sp++)
		{
		if (os_p->type == sp->type)
			{
			goto found2;
			}
		}
	longjmp(try_b, ESOCKTNOSUPPORT);

found2:
	os_p->sp = sp;
	if (sp->socket == NULL || (*sp->socket)(os_p, pf) == -1)
		{
		longjmp(try_b, ESOCKTNOSUPPORT);
		}

#if DEBUG >= 3
	_dprintf(3, _("os_new() returning 0x%x\n"), os_p);
#endif
	return os_p;
	}
#endif

/* Close a socket, freeing it for reuse. Try to do a graceful close on a
 * TCP socket, if possible
 */

#ifdef MODULE_os_deref
void
os_deref(struct usock *os_p)
	{
	struct socklink *sp;

#if DEBUG >= 3
	_dprintf(3, _("os_deref(0x%x) starting\n"), os_p);
#endif
	if (object_dec_refs(&os_p->object)) /* last reference? */
		{
		/* Call proto-specific close routine if there is one */
		sp = os_p->sp;
		if (sp && sp->close)
			{
			(*sp->close)(os_p);
			/* errno ?? */
			}

		free(os_p->name);
		free(os_p->peername);

		/* object_release(&os_p->object); */
		ksignal(os_p, 0); /* Wake anybody doing accept() or recv() */
		}
#if DEBUG >= 3
	_dprintf(3, _("os_deref() returning, refs %u\n"), os_p->object.refs);
#endif
	}
#endif

#ifdef MODULE_os_garbage
/* open_sock_list garbage collection - called by storage allocator when free
 * space runs low.  Zaps open sockets table entries with 0 reference count.
 */
void
os_garbage(int red)
	{
	struct usock **os_pp, *os_p;

	for (os_pp = open_sock_list; os_pp < open_sock_list + OPEN_SOCK_LIST;
			os_pp++)
		{
		os_p = *os_pp;
		if (os_p)
			{
			if (os_p->object.refs)
				{
				if (os_p->type == TYPE_LOCAL_STREAM)
					{
					mbuf_crunch(&os_p->cb.local->q);
					}
				}
			else
				{
				*os_pp = NULL;
				free(os_p);
				}
			}
		}
	}
#endif

/* os_locate() returns the struct usock pointer associated with a user's
 * file descriptor, checking that it's a socket and not a file
 */

#ifdef MODULE_os_locate
struct usock *
os_locate(jmp_buf error, unsigned int fd)
	{
	struct object_s *object_p;

#if DEBUG >= 3
	_dprintf(3, _("os_locate(0x%x, %u) starting\n"), error, fd);
#endif
	object_p = uf_locate(error, fd);
	if (object_p->type != _FL_SOCK)
		{
		object_release(object_p);
#if DEBUG >= 3
		_dprintf(3, _("os_locate() error ENOTSOCK\n"));
#endif
		longjmp(error, ENOTSOCK);
		}
#if DEBUG >= 3
	_dprintf(3, _("os_locate() returning 0x%x\n"), object_p);
#endif
	return (struct usock *)object_p;
	}
#endif

/* Create a user socket, return socket index
 * The mapping to actual protocols is as follows:
 *
 *
 * ADDRESS FAMILY	Stream		Datagram	Raw	    Seq. Packet
 *
 * AF_INET		TCP		UDP		IP
 * AF_AX25		I-frames	UI-frames
 * AF_NETROM						NET/ROM L3  NET/ROM L4
 * AF_LOCAL		stream loopback	packet loopback
 */

#ifdef MODULE_os_socket
unsigned int os_socket(jmp_buf error, unsigned int af, unsigned int type,
		unsigned int pf)
	{
	jmp_buf try_b;
	unsigned int catch;
	struct usock *os_p;
	int fd;

	os_p = os_new(error, af, type, pf);

	catch = setjmp(try_b);
	if (catch)
		{
		object_release(&os_p->object);
		longjmp(error, catch);
		}

	fd = uf_alloc(try_b);
	my_thread_p->process_p->object_p[fd] = &os_p->object;

	os_p->index = fd;
	os_p->owner = my_thread_p;

	object_release(&os_p->object);
	return fd;
	}
#endif

/* Attach a local address/port to a socket. If not issued before a connect
 * or listen, will be issued automatically
 */

#ifdef MODULE_os_bind
void os_bind(jmp_buf error, unsigned int fd, struct sockaddr *name,
		unsigned int namelen)
{
	jmp_buf try_b;
	unsigned int catch;
	struct usock *up;
	struct socklink *sp;

	up = os_locate(error, fd);

	catch = setjmp(try_b);
	if (catch)
		{
		object_release(&up->object);
		longjmp(error, catch);
		}

	if (name == NULL){
		longjmp(try_b, EFAULT);
	}
	if (up->name){
		/* Bind has already been issued */
		longjmp(try_b, EINVAL);
	}
	sp = up->sp;
	if (sp->check && (*sp->check)(name, namelen) == -1){
		/* Incorrect length or family for chosen protocol */
		longjmp(try_b, EAFNOSUPPORT);
	}

	/* Stash name in an allocated block */
	up->name = malloc(namelen);
	if (up->name == NULL)
		{
		longjmp(try_b, ENOMEM);
		}
	up->namelen = namelen;
	memcpy(up->name, name, namelen);

	/* a bind routine is optional - don't fail if it isn't present */
	if (sp->bind && (*sp->bind)(up) == -1){
		free(up->name);
		up->name = NULL;
		up->namelen = 0;

		longjmp(try_b, EOPNOTSUPP);
	}

	object_release(&up->object);
}
#endif

/* Post a listen on a socket */

#ifdef MODULE_os_listen
void os_listen(jmp_buf error, unsigned int fd, unsigned int backlog)
{
	jmp_buf try_b;
	unsigned int catch;
	struct usock *up;
	struct socklink *sp;

	up = os_locate(error, fd);

	catch = setjmp(try_b);
	if (catch)
		{
		object_release(&up->object);
		longjmp(error, catch);
		}

	if (up->cb.p){
		longjmp(try_b, EISCONN);
	}
	sp = up->sp;

	/* Fail if listen routine isn't present */
 object_release(&up->object);
	if (sp->listen == NULL || (*sp->listen)(up, backlog) == -1){
		longjmp(error /*try_b*/, EOPNOTSUPP);
	}

	/*object_release(&up->object);*/
}
#endif

/* Initiate active open. For datagram sockets, merely bind a remote address. */

#ifdef MODULE_os_connect
void os_connect(jmp_buf error, unsigned int fd, struct sockaddr *peername,
		unsigned int peernamelen)
{
	jmp_buf try_b;
	unsigned int catch;
	struct usock *up;
	struct socklink *sp;
	struct sockaddr *temp;

	up = os_locate(error, fd);

	catch = setjmp(try_b);
	if (catch)
		{
		object_release(&up->object);
		longjmp(error, catch);
		}

	if (peername == NULL){
		/* Connect must specify a remote address */
		longjmp(try_b, EFAULT);
	}
	sp = up->sp;

	/* Check name format, if checking routine is available */
	if (sp->check && (*sp->check)(peername, peernamelen) == -1){
		longjmp(try_b, EAFNOSUPPORT);
	}

	temp = malloc(peernamelen);
	if (temp == NULL)
		{
		longjmp(try_b, ENOMEM);
		}
	if (up->peername)
		{
		free(up->peername);
		}
	up->peername = temp;
	up->peernamelen = peernamelen;
	memcpy(temp, peername, peernamelen);

	/* a connect routine is optional - don't fail if it isn't present */
 object_release(&up->object);
	if (sp->connect && (*sp->connect)(up) == -1){
		up->peername = NULL;
		up->peernamelen = 0;
		free(temp);

		longjmp(error /*try_b*/, errno);
	}

	/*object_release(&up->object);*/
}
#endif

/* Wait for a connection. Valid only for connection-oriented sockets. */

#ifdef MODULE_os_accept
unsigned int os_accept(jmp_buf error, unsigned int fd,
		struct sockaddr *peername, unsigned int *peernamelen)
{
	jmp_buf try_b;
	unsigned int catch;
	struct usock *nup;
	struct usock *up;
	struct socklink *sp;
	unsigned int temp;
	unsigned int fd_new;

	up = os_locate(error, fd);

	catch = setjmp(try_b);
	if (catch)
		{
		object_release(&up->object);
		longjmp(error, catch);
		}

	if (up->cb.p == NULL){
		longjmp(try_b, EOPNOTSUPP);
	}
	sp = up->sp;

	/* Fail if accept flag isn't set */
	if (sp->accept == FALSE){
		longjmp(try_b, EOPNOTSUPP);
	}

	/* Wait for the state-change upcall routine to signal us */
	while (up->cb.p)
		{
		if (up->rdysock)
			{
			goto ready;
			}
		if (up->noblock)
			{
			longjmp(try_b, EWOULDBLOCK);
			}
		object_release(&up->object);
		catch = kwait(up);
		if (catch)
			{
			longjmp(error, catch);
			}
		object_acquire(&up->object);
		}

	/* Blown away */
	longjmp(try_b, EBADF);

ready:
	nup = up->rdysock;
	if (nup != up)
		{
		/* means the backlog argument to listen() was > 0 */
		fd_new = uf_alloc(try_b);
		my_thread_p->process_p->object_p[fd_new] = &nup->object;
		}
	else
		{
		/* means the backlog argument to listen() was == 0 */
		/* note: susv3 conforming apps should never come here */
		fd_new = fd;
		}
	up->rdysock = NULL;

	if (peername && peernamelen){
		temp = min_uint(nup->peernamelen, *peernamelen);
		memcpy(peername, nup->peername, temp);
		*peernamelen = temp;
	}

	object_release(&up->object);
	return fd_new;
}
#endif

/* Close down a socket three ways. Type 0 means "no more receives"; this
 * replaces the incoming data upcall with a routine that discards further
 * data. Type 1 means "no more sends", and obviously corresponds to sending
 * a TCP FIN. Type 2 means "no more receives or sends". This I interpret
 * as "abort the connection".
 */

#ifdef MODULE_os_shutdown
void os_shutdown(jmp_buf error, unsigned int fd, unsigned int flag)
{
	jmp_buf try_b;
	unsigned int catch;
	struct usock *up;
	struct socklink *sp;

	up = os_locate(error, fd);

	catch = setjmp(try_b);
	if (catch)
		{
		object_release(&up->object);
		longjmp(error, catch);
		}

	if (up->cb.p == NULL){
		longjmp(try_b, ENOTCONN);
	}
	sp = up->sp;

	/* Just close the socket if special shutdown routine not present */
	if (sp->shut)
		{
		if ((*sp->shut)(up, flag) == -1)
			{
			longjmp(try_b, errno);
			}
		object_release(&up->object);
		}
	else
		{
		my_thread_p->process_p->object_p[fd] = NULL;
		object_release(&up->object); /* eliminate this later */
		os_deref(up);
		}

	ksignal(up, 0);
}
#endif

/* Return a pair of mutually connected sockets in sv[0] and sv[1] */

#ifdef MODULE_os_socketpair
void os_socketpair(jmp_buf error, unsigned int af, unsigned int type,
		unsigned int pf, unsigned int *fd_p)
{
	jmp_buf try_b;
	unsigned int catch;
	struct usock *up0, *up1;
	unsigned int fd0, fd1;

	if (fd_p == NULL){
		longjmp(error, EFAULT);
	}
	if (af != AF_LOCAL){
		longjmp(error, EAFNOSUPPORT);
	}
	if (type != SOCK_STREAM && type != SOCK_DGRAM){
		longjmp(error, ESOCKTNOSUPPORT);
	}

	up0 = os_new(error, AF_LOCAL, type, pf);

	catch = setjmp(try_b);
	if (catch)
		{
		goto error1;
		}

	up1 = os_new(error, AF_LOCAL, type, pf);

	catch = setjmp(try_b);
	if (catch)
		{
		goto error2;
		}

	fd0 = uf_alloc(try_b);
	my_thread_p->process_p->object_p[fd0] = &up0->object;

	catch = setjmp(try_b);
	if (catch)
		{
		my_thread_p->process_p->object_p[fd0] = NULL;
	error2:
		object_release(&up1->object); /* eliminate this later */
		os_deref(up1);
	error1:
		object_release(&up0->object); /* eliminate this later */
		os_deref(up0);

		longjmp(error, catch);
		}

	fd1 = uf_alloc(try_b);
	my_thread_p->process_p->object_p[fd1] = &up1->object;

	fd_p[0] = fd0;
	fd_p[1] = fd1;

	up0->cb.local->peer = up1;
	up1->cb.local->peer = up0;

	object_release(&up0->object);
	object_release(&up0->object);
}
#endif

/* Higher-level receive routine, intended for connection-oriented sockets.
 * Can be used with datagram sockets, although the sender id is lost.
 */

#ifdef MODULE_os_recv
size_t os_recv(jmp_buf error, unsigned int fd, void *data, size_t count,
		unsigned int flag)
{
	struct usock *up;
	struct socklink *sp;

	up = os_locate(error, fd);
	object_release(&up->object);

	sp = up->sp;

	/* Fail if recv routine isn't present */
	if (sp->recv == NULL){
		longjmp(error, EOPNOTSUPP);
	}

	count = (*sp->recv)(up, data, count, NULL, NULL);
	if (count == -1)
		{
		longjmp(error, errno);
		}
	return count;
}
#endif

/* Higher level receive routine, intended for datagram sockets. Can also
 * be used for connection-oriented sockets, although from and fromlen are
 * ignored.
 */

#ifdef MODULE_os_recvfrom
size_t os_recvfrom(jmp_buf error, unsigned int fd, void *data, size_t count,
		unsigned int flag, struct sockaddr *from,
		unsigned int *fromlen)
{
	struct usock *up;
	struct socklink *sp;

	up = os_locate(error, fd);
	object_release(&up->object);

	sp = up->sp;

	/* Fail if recv routine isn't present */
	if (sp->recv == NULL){
		longjmp(error, EOPNOTSUPP);
	}

	count = (*sp->recv)(up, data, count, from, (int *)fromlen);
	if (count == -1)
		{
		longjmp(error, errno);
		}
	return count;
}
#endif

/* High level send routine */

#ifdef MODULE_os_send
size_t os_send(jmp_buf error, unsigned int fd, void *data, size_t count,
		unsigned int flag)
	{
	struct sockaddr sock;
	unsigned int i;
	register struct usock *up;
	struct socklink *sp;

	up = os_locate(error, fd);
	object_release(&up->object);

	i = MAXSOCKSIZE;
 /* need to fudge sysio here */
	os_getpeername(error, fd, &sock, &i);

	sp = up->sp;

	/* Fail if send routine isn't present (shouldn't happen) */
	if (sp->send == NULL){
		longjmp(error, EOPNOTSUPP);
	}

	/* Remote address always supplied, check (maybe redundant) */
	if (sp->check && (*sp->check)(&sock, i) == -1){
		longjmp(error, EAFNOSUPPORT);
	}

	count = (*sp->send)(up, data, count, &sock);
	if (count == -1)
		{
		longjmp(error, EOPNOTSUPP); /* maybe already got errno? */
		}
	return count;
}
#endif

/* High level send routine, intended for datagram sockets. Can be used on
 * connection-oriented sockets, but "to" and "tolen" are ignored.
 */

#ifdef MODULE_os_sendto
size_t os_sendto(jmp_buf error, unsigned int fd, void *data, size_t count,
		unsigned int flag, struct sockaddr *to, unsigned int tolen)
{
	struct usock *up;
	struct socklink *sp;

	up = os_locate(error, fd);
	object_release(&up->object);

	sp = up->sp;

	/* Fail if send routine isn't present (shouldn't happen) */
	if (sp->send == NULL){
		longjmp(error, EOPNOTSUPP);
	}

	/* If remote address is supplied, check it */
	if (to && sp->check && (*sp->check)(to, tolen) == -1){
		longjmp(error, EAFNOSUPPORT);
	}

	count = (*sp->send)(up, data, count, to);
	if (count == -1)
		{
		longjmp(error, EOPNOTSUPP); /* maybe already got errno? */
		}
	return count;
}
#endif

/* Return local name passed in an earlier bind() call */

#ifdef MODULE_os_getsockname
void os_getsockname(jmp_buf error, unsigned int fd, struct sockaddr *name,
		unsigned int *namelen)
{
	jmp_buf try_b;
	unsigned int catch;
	struct usock *up;
	unsigned int temp;

	up = os_locate(error, fd);

	catch = setjmp(try_b);
	if (catch)
		{
		object_release(&up->object);
		longjmp(error, catch);
		}

	if (name == NULL || namelen == NULL){
		longjmp(try_b, EFAULT);
	}

	if (up->name)
		{
		temp = min_uint(*namelen, up->namelen);
		memcpy(name, up->name, temp);
		*namelen = temp;
		}
	else
		{
		/* Not bound yet */
		*namelen = 0;
		}

	object_release(&up->object);
}
#endif

/* Get remote name, returning result of earlier connect() call. */

#ifdef MODULE_os_getpeername
void os_getpeername(jmp_buf error, unsigned int fd, struct sockaddr *peername,
		unsigned int *peernamelen)
{
	jmp_buf try_b;
	unsigned int catch;
	struct usock *up;
	unsigned int temp;

	up = os_locate(error, fd);

	catch = setjmp(try_b);
	if (catch)
		{
		object_release(&up->object);
		longjmp(error, catch);
		}

	if (up->peername == NULL){
		longjmp(error, ENOTCONN);
	}
	if (peername == NULL || peernamelen == NULL){
		longjmp(error, EFAULT);
	}

	temp = min(*peernamelen, up->peernamelen);
	memcpy(peername, up->peername, temp);
	*peernamelen = temp;

	object_release(&up->object);
}
#endif

