/* Socket status display code
 * Copyright 1991 Phil Karn, KA9Q
 *
 * Apr 04	RPB	Changes for intl' of strings
 */
#include <stdio.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include "kernel/thread.h"
#include "socket/usock.h"
#include "socket/socket.h"
#include "main/commands.h"
#include <libintl.h>
#include "po/messages.h"

/* Socket status display command */
int
dosock(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct usock *up;
	int s,i,n;
	struct sockaddr fsock;
	struct socklink *sp;
	char *cp;
	char *duptext; /*RPB*/

	if(argc < 2){
		_printf(_("S#   Type    PCB       Remote socket         Owner\n"));
		for(n=0;n<Nsock;n++){
			s = _mk_fd(n,_FL_SOCK);
			up = itop(s);
			if(up == NULL)
				continue;

			i = sizeof(fsock);
			if(getpeername(s,&fsock,&i) == 0 && i != 0)
				cp = psocket(&fsock);
			else
				cp = "";

			/*RPB*/
			if ((duptext = _strdup(Socktypes[up->type])) == NULL)
			{
				__getfail(Socktypes[up->type]);
				return 1;
			}

			_printf(_("%4d %-8s%-9p %-22s%-9p %-10s\n"),
			 s,duptext,up->cb.p,cp,
			 up->owner,up->owner->name);
			free(duptext);
			/*RPB*/
		}
		return 0;
	}
	s = atoi(argv[1]);
	if(_fd_type(s) != _FL_SOCK){
		_printf(_("Not a valid socket\n"));
		return 1;
	}
	up = itop(s);
	if(up == NULL){
		_printf(_("Socket not in use\n"));
		return 1;
	}
	sp = up->sp;

	/*RPB*/
	if ((duptext = _strdup(Socktypes[up->type])) == NULL)
	{
		__getfail(Socktypes[up->type]);
		return 1;
	}

	_printf(_("%s %p\n"),duptext,up->cb.p);
	free(duptext);
	/*RPB*/
	if(up->cb.p == NULL)
		return 0;
	if(sp->status != NULL)
		(*sp->status)(up);
	return 0;
}

