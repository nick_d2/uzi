
# -----------------------------------------------------------------------------

SUBDIRS=

OUTPUTS=	socket.$(LIBEXT)

socket_$(LIBEXT)_SOURCES= \
		locsock.c opensock.c sockutil.c
#sockcmd.c

opensock_c_MODULES= \
		open_sock_list os_new os_deref os_garbage os_locate \
		os_socket os_bind os_listen os_connect os_accept os_shutdown \
		os_socketpair os_recv os_recvfrom os_send os_sendto \
		os_getsockname os_getpeername

sockutil_c_MODULES= \
		Socktypes socklen sockkick sockowner settos \
		eolseq psocket sockerr sockstate
#Sock_errlist
#Badsocket Usock sockinit usesock freesock itop st_garbage

# -----------------------------------------------------------------------------

