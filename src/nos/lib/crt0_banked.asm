; crt0_banked.S by Nick for NOS/UZI banked executable (banked memory model)

; -----------------------------------------------------------------------------

	module	_crt0_banked

	extrn	_main
	extrn	_exit

	extrn	e__DEFAULT		; e_hsize
	extrn	s_RCODE			; e_idata
	extrn	s_UDATA0		; e_udata
	extrn	s_CSTACK		; e_stack
	extrn	e_CSTACK		; e_break

	extrn	?BANK_FAST_LEAVE_L08
	extrn	?BANK_CALL_DIRECT_L08






























































































































































































































; -----------------------------------------------------------------------------
; forward definitions of segments, to set the linkage order (c0b must be first)

	rseg	RCODE
;	rseg	TEMP
;	rseg	DATA0
;	rseg	WCSTR
	rseg	CONST
	rseg	CSTR
	rseg	IDATA0(NUL)
	rseg	CDATA0
	rseg	ECSTR(NUL)
	rseg	CCSTR
	rseg	CODE(NUL)		; needs to be at end for banked model
	rseg	UDATA0
	rseg	CSTACK

; -----------------------------------------------------------------------------

	rseg	_DEFAULT		; means header is discarded at loading

	defw	0xa6c9					; e_magic
	defw	2		; e_format
	defd	12345678h		; e_size
	defw	e__DEFAULT		; e_hsize
	defw	s_RCODE			; e_idata
	defw	init			; e_entry
	defw	s_UDATA0		; e_udata
	defw	s_CSTACK		; e_stack
	defw	e_CSTACK		; e_break

; -----------------------------------------------------------------------------

	rseg	RCODE

	public	init
init:






 ld hl,0
 add hl,sp
 ld de,___stack_base+2
 or a
 sbc hl,de
 ld c,l
 ld b,h
 ld hl,___stack_base+1
 ld (hl),.high.0x55aa		
 dec hl
 ld (hl),.low.0x55aa		
 ldir ; fill stack region up to sp

; now there are the next stack structure:
;	+4 envp
;	+2 argv
; sp->	+0 argc
	pop	de
	ld	(__argc),de		; 2nd argument to main
	pop	bc
	ld	(__argv),bc		; 1st argument to main
	pop	hl
	ld	(_environ),hl

	ld	HL,LWRD _main		; banked call to main()
	ld	A,BYTE3 _main
	call	?BANK_CALL_DIRECT_L08

	ex	de,hl			; de = exitcode (1st argument to exit)

	ld	HL,LWRD _exit		; banked call to exit()
	ld	A,BYTE3 _exit
	jp	?BANK_CALL_DIRECT_L08

; -----------------------------------------------------------------------------

		rseg	UDATA0

		public	__argc
__argc:		defs	2

		public	__argv
__argv:		defs	2

		public	_environ
_environ:	defs	2

		public	_errno
_errno:		defs	2

		public	___cleanup
___cleanup:	defs	3 ; oopsy 2

		rseg	CSTACK

		public	___stack_base
___stack_base:	defs	0x400 		

		public	___stack_limit
___stack_limit:

; -----------------------------------------------------------------------------

	end

