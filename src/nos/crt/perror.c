/* perror.c
 */
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

static void wr2(str)
const char *str; /* Nick added const */
{
	const char *p = str;

	while (*p)
		++p;
	write(2, str, (size_t)(p-str));
}

void perror(str)
const char *str; /* Nick added const */
{
	if (!str)
		str = "error";
	wr2(str);
	wr2(": ");
	str = strerror(errno);
	wr2(str);
	wr2("\n");
}
