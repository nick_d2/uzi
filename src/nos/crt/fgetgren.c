/* fgetgren.c	groups implementation
 */

#include "grp-l.h"
/*RPB*/
/*#include <signal.h>*/
/*#include <z80/asm.h>*/
/*#include <uzi/devio.h>*/
/*#include <uzi/rtc.h>*/
/*#include <uzi/filesys.h>*/
/*#include <uzi/process.h>*/
/*RPB*/

#ifdef L_fgetgren
struct group *fgetgrent(file)
	FILE *file;
{
	if (file == NULL) {
		errno = EINTR;
		return NULL;
	}
	return __getgrent(fileno(file));
}
#endif
