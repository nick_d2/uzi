/* execle.c
 *
 * function(s)
 *	  execle - load and execute a program
 */

#include "exec-l.h"

#ifdef L_execle
#if 1 /* Nick */
int execle(char *pathP, char *arg0, ...)
#else
int execle(pathP, arg0)
	char *pathP;
	char *arg0;
#endif
{
	register char **p = &arg0;

	/* Find the end of the argument list */
	while (*p++)
		;
	return execve(pathP, &arg0, (char **)*p);
}
#endif

