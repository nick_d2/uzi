/* stdio.c
 * Copyright (C) 1996 Robert de Bath <rdebath@cix.compulink.co.uk>
 * This file is part of the Linux-8086 C library and is distributed
 * under the GNU Library General Public License.
 */

/* This is an implementation of the C standard IO package. */

#include "stdio-l.h"
#include "kernel/dprintf.h"
#include "kernel/object.h"
#include "kernel/process.h"
#include "kernel/thread.h"
#include <libintl.h>
#include "po/messages.h"

#ifdef L_fclose
int fclose(fp)
	FILE *fp;
{
	int rv = 0;
#if 0 /* modification to save code space by sharing with process_fclose() */
#if 1 /* tracking per-process streams using my_thread_p->process_p->stream_p */
	struct object_s **base_stream_pp, **limit_stream_pp;
	struct object_s **stream_pp;
#endif
#endif

	if (fp == NULL) {
		errno = EINVAL;
		return EOF;
	}
	if (fflush(fp))
		return EOF;
 object_acquire(&fp->object);
	if (close(fp->fd))
		rv = EOF;
 object_release(&fp->object);

#if 1 /* modification to save code space by sharing with process_fclose() */
	process_fclose(my_thread_p->process_p, &fp->object);
#else
#if 1 /* streams within the kernel are shared by children of process_fork() */
	if (object_dec_refs(&fp->object))
		{
		/* was last reference, OLD:object is still acquired, destroy it */
		/* object_release(&fp->object); */
#endif
 acrlf();
 abyte('!');
 ahexw((unsigned int)fp);
		fp->fd = -1;
		if (fp->mode & __MODE_FREEBUF) {
			free(fp->bufstart);
			fp->mode &= ~__MODE_FREEBUF;
			fp->bufstart = fp->bufend = 0;
		}
		if (fp->mode & __MODE_FREEFIL) {
			FILE *ptr = __IO_list, *prev = 0;

			fp->mode = 0;
			while (ptr && ptr != fp)
				ptr = ptr->next;
			if (ptr == fp) {
				if (prev == 0)
					__IO_list = fp->next;
				else	prev->next = fp->next;
			}
			free(fp);
		}
		else	fp->mode = 0;
#if 1 /* streams within the kernel are shared by children of process_fork() */
		}
#endif

#if 1 /* tracking per-process streams using my_thread_p->process_p->stream_p */
	base_stream_pp = my_thread_p->process_p->stream_p;
	limit_stream_pp = base_stream_pp + USER_STREAM_LIST;

	for (stream_pp = base_stream_pp; stream_pp < limit_stream_pp;
			stream_pp++)
		{
		if (*stream_pp == &fp->object)
			{
			*stream_pp = NULL;
			goto found;
			}
		}
	_panic(_("fclose() not in list"));

found:
#endif
#endif
	return rv;
}
#endif

