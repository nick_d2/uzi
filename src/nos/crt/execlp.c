/* execlp.c
 *
 * function(s)
 *	  execlp - load and execute a program
 */

#include "exec-l.h"

#ifdef L_execlp
#if 1 /* Nick */
int execlp(char *pathP, char *arg0, ...)
#else
int execlp(pathP, arg0)
	char *pathP;
	char *arg0;
#endif
{
	return execve(_findPath(pathP), &arg0, environ);
}
#endif

