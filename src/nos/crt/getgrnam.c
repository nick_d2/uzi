/* getgrnam.c	groups implementation
 */

#include "grp-l.h"
/*RPB*/
/*#include <signal.h>*/
/*#include <z80/asm.h>*/
/*#include <uzi/devio.h>*/
/*#include <uzi/rtc.h>*/
/*#include <uzi/filesys.h>*/
/*#include <uzi/process.h>*/
/*RPB*/

#ifdef L_getgrnam
struct group *getgrnam(name)
	char *name;
{
	struct group *group;

	if (name == NULL) {
		errno = EINVAL;
		return NULL;
	}
	setgrent();
	while ((group = getgrent()) != NULL) {
		if (!strcmp(group->gr_name, name))
			break;
	}
	endgrent();
	return group;
}
#endif
