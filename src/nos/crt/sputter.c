/* sputter.c
 * Nick Downing, Hytech
 *
 *    Dale Schumacher			      399 Beacon Ave.
 *    (alias: Dalnefre')		      St. Paul, MN  55104
 *    dal@syntel.UUCP			      United States of America
 *
 * Altered to use stdarg, made the core function vprinter (Nick).
 * Hooked into the stdio package using 'inside information'
 * Altered sizeof() assumptions, now assumes all integers except chars
 * will be either
 *  sizeof(xxx) == sizeof(long) or sizeof(xxx) == sizeof(short)
 *
 * -RDB
 */

#include "printf-l.h"

#ifdef L_sputter
unsigned int __sputter(buffer, count, op)
	void *buffer;
	unsigned int count;
	FILE *op;
{
	memcpy(*((unsigned char **)op), buffer, count);
	*((unsigned char **)op) += count;
	return count;
}
#endif

