/* execlpe.c
 *
 * function(s)
 *	  execlpe - load and execute a program
 */

#include "exec-l.h"

#ifdef L_execlpe
#if 1 /* Nick */
int execlpe(char *pathP, char *arg0, ...)
#else
int execlpe(pathP, arg0)
	char *pathP;
	char *arg0;
#endif
{
	register char **p = &arg0;

	/* Find the end of the argument list */
	while (*p++)
		;
	return execve(_findPath(pathP), &arg0, (char **)*p);
}
#endif

