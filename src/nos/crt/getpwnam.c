/* getpwnam.c
 */

/* #include <stdio.h> Nick */
#include "passwd-l.h"
/*RPB*/
/*#include <signal.h>*/
/*#include <z80/asm.h>*/
/*#include <uzi/devio.h>*/
/*#include <uzi/rtc.h>*/
/*#include <uzi/filesys.h>*/
/*#include <uzi/process.h>*/
/*RPB*/

#ifdef L_getpwnam
struct passwd *getpwnam(name)
	char *name;
{
	struct passwd *pwd;

	if (name == NULL) {
		errno = EINVAL;
		return NULL;
	}
	setpwent();
	while ((pwd = getpwent()) != NULL) {
/* printf("getpwent() returned pw_name = \"%s\"\n", pwd->pw_name); */
		if (!strcmp(pwd->pw_name, name))
			break;
	}
	endpwent();
	return pwd;
}
#endif

