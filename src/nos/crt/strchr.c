/* string.c
 * Copyright (C) 1995,1996 Robert de Bath <rdebath@cix.compulink.co.uk>
 * This file is part of the Linux-8086 C library and is distributed
 * under the GNU Library General Public License.
 */

#include "string-l.h"

/********************** Function strchr ************************************/
#ifdef L_strchr
char *strchr(s, c)
	const char *s; /* Nick */
	int c;
{
	register char ch;

	for (;;) {
		if ((ch = *s) == c)
			return (char *)s; /* Nick */
		if (ch == 0)
			return NULL; /* Nick */
		s++;
	}
}
#endif
