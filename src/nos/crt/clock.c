/*************************** CLOCK ************************************/
#include "time-l.h"

#ifdef L_clock
#include <sys/types.h>
#include <unistd.h>

long clock(VOID) {
	struct tms __tms;

	times(&__tms);
	return __tms.tms_utime; /* Nick (__tms.tms_utime.t_time+__tms.tms_utime.t_date*CLOCsession_pER_SEC*60); */
}
#endif

