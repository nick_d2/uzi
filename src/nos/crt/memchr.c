/* memchr.c
 * Copyright (C) 1995,1996 Robert de Bath <rdebath@cix.compulink.co.uk>
 * This file is part of the Linux-8086 C library and is distributed
 * under the GNU Library General Public License.
 */

#include "mem-l.h"

/********************** Function memchr ************************************/
#ifdef L_memchr
void *memchr(str, c, l)
	const void *str; /* Nick */
	int c;
	size_t l;
{
	register const char *p = (char *) str; /* Nick */

	while (l-- != 0) {
		if (*p == c)
			return (void *)p; /* Nick */
		p++;
	}
	return NULL;
}
#endif
