/* close.c	closedir implementation
 *
 */
#include <unistd.h>
#include <alloc.h>
#include <dirent.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
/*RPB*/
/*#include <signal.h>*/
/*#include <z80/asm.h>*/
/*#include <uzi/devio.h>*/
/*#include <uzi/rtc.h>*/
/*#include <uzi/filesys.h>*/
/*#include <uzi/process.h>*/
/*RPB*/

int closedir(dir)
	register DIR *dir;
{
/*RPB*//*
	* TODO: I think fd==0 now remains open forever, however,
	* the change made here didn't work and resulted in no output
	* at all. Didn't look into it furter!! Original code is active now.
	*/
#if 1
	if (dir == NULL || dir->dd_buf == NULL || dir->dd_fd == 0) {
#else
	/* fd==0 is a valid value!! */
	if (dir == NULL || dir->dd_buf == NULL) {
#endif
/*RPB*/
		errno = EFAULT;
		return -1;
	}
	close(dir->dd_fd);
	free(dir->dd_buf);
	dir->dd_fd = 0;
	dir->dd_buf = NULL;
	free(dir);
	return 0;
}

