#!/bin/sh
grep "$1" ax25/*.S
grep "$1" client/*.S
grep "$1" crt/*.S
grep "$1" driver/*.S
grep "$1" dump/*.S
grep "$1" filesys/*.S
grep "$1" internet/*.S
grep "$1" intl/*.S
grep "$1" kernel/*.S
grep "$1" main/*.S
grep "$1" netrom/*.S
grep "$1" ppp/*.S
grep "$1" server/*.S
grep "$1" socket/*.S
grep "$1" sys/*.S
grep "$1" z80/*.S
