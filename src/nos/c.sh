#!/bin/sh
grep "$1" ax25/*.c
grep "$1" client/*.c
grep "$1" crt/*.c
grep "$1" driver/*.c
grep "$1" dump/*.c
grep "$1" filesys/*.c
grep "$1" internet/*.c
grep "$1" intl/*.c
grep "$1" kernel/*.c
grep "$1" main/*.c
grep "$1" netrom/*.c
grep "$1" ppp/*.c
grep "$1" server/*.c
grep "$1" socket/*.c
grep "$1" sys/*.c
grep "$1" z80/*.c
