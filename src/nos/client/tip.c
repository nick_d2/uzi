/* "Dumb terminal" session command for serial lines
 * Copyright 1991 Phil Karn, KA9Q
 *
 *	Feb '91	Bill Simpson
 *		rlsd control and improved dialer
 */
#include "nos/global.h"
#include "main/mbuf.h"
#include "kernel/thread.h"
#include "main/iface.h"
#include "z80/asci.h"
/*#include "driver/devtty.h"*/
#ifdef JOB_CONTROL
#include "kernel/session.h"
#endif
#include "socket/socket.h"
#include "main/commands.h"
#include "main/devparam.h"
#include "main/main.h"
#include <libintl.h>
#include "po/messages.h"

static void tip_out(int i,void *n1,void *n2);

/* Execute user telnet command */
int
dotip(argc,argv,p)
int argc;
char *argv[];
void *p;
{
#ifdef JOB_CONTROL
	struct session_s *sp;
#else
	struct thread_s *proc1;
#endif
	char *ifn;
	int c;
	FILE *asy;

	if((asy = asyopen(argv[1],"r+b")) == NULL){
		_printf(_("Can't open %s\n"),argv[1]);
		return 1;
	}
	setvbuf(asy,NULL,_IONBF,0);
#ifdef JOB_CONTROL
	/* Allocate a session descriptor */
	if((sp = newsession(Cmdline,TIP,1)) == NULL){
		_printf(_("Too many sessions\n"));
		return 1;
	}
#endif
	/* Put tty into raw mode */
#if 0 /* change this to use sgtty.h, eventually */
	sp->ttystate.echo = 0;
	sp->ttystate.edit = 0;
#endif
	fmode(stdin,STREAM_BINARY);
	fmode(stdout,STREAM_BINARY);

	/* Now fork into two paths, one rx, one tx */
	ifn = malloc(strlen(argv[1]) + 10);
	_sprintf(ifn,_("%s tip out"),argv[1]);
#ifdef JOB_CONTROL
	sp->proc1 = process_thread_create(daemon_process_p, ifn,256,tip_out,0,asy,NULL,0);
#else
	proc1 = process_thread_create(daemon_process_p, ifn,256,tip_out,0,asy,NULL,0);
#endif
	free( ifn );

	ifn = malloc(strlen(argv[1]) + 10);
	_sprintf(ifn,_("%s tip in"),argv[1]);
	chname( my_thread_p, ifn );
	free( ifn );

	while((c = fgetc(asy)) != EOF){
		putchar(c);
#ifdef JOB_CONTROL
		if(sp->record != NULL)
			putc(c,sp->record);
#endif
	}
	fflush(stdout);

#ifdef JOB_CONTROL
	killproc(proc1);
#else
	killproc(sp->proc1);
	sp->proc1 = NULL;
#endif
	fclose(asy);
#ifdef JOB_CONTROL
	freesession(sp, 1); /* does keywait(NULL, 1); beforehand */
#endif
	return 0;
}

/* Output process, DTE version */
static void
tip_out(i,n1,n2)
int i;
void *n1,*n2;
{
	int c;
	FILE *asy = (FILE *)n1;

	while((c = getchar()) != EOF){
		fputc(c,asy);
	}
}

