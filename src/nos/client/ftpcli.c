/* Internet FTP client (interactive user)
 * Copyright 1991 Phil Karn, KA9Q
 *
 * Apr 06	RPB	Changes for intl' of strings
 */
#include <stdio.h>
#include <errno.h> /* Nick */
#include <sgtty.h> /* Nick */
#include "nos/global.h"
#include "main/mbuf.h"
#ifdef JOB_CONTROL
#include "kernel/session.h"
#endif
#include "main/cmdparse.h"
/*#include "driver/devtty.h"*/
#include "socket/socket.h"
#include "main/ftp.h"
#include "client/ftpcli.h"
#include "main/commands.h"
#include "internet/netuser.h"
#include "z80/dirutil.h"
#include <libintl.h> /*RPB*/
#include "internet/internet.h"
#include "kernel/thread.h" /* for my_thread_p */
#include <libintl.h>
#include "po/messages.h"

#define	POLLRATE	500	/* 500ms between more file polls */
#define	DIRBUF	256

#define FTPCMDS_MAX 25

#ifdef MODULE
#define STATIC
extern _char *Notsess;
extern struct cmds Ftpcmds[FTPCMDS_MAX];
#else
#define STATIC static
#define MODULE_Notsess
#define MODULE_Ftpcmds
#define MODULE_doftp
#define MODULE_doverbose
#define MODULE_dobatch
#define MODULE_doupdate
#define MODULE_dohash
#define MODULE_doquit
#define MODULE_doftpcd
#define MODULE_domkdir
#define MODULE_dormdir
#define MODULE_dobinary
#define MODULE_doascii
#define MODULE_dotype
#define MODULE_doget
#define MODULE_doread
#define MODULE_domget
#define MODULE_dolist
#define MODULE_dols
#define MODULE_domd5
#define MODULE_docompare
#define MODULE_domcompare
#define MODULE_compsub
#define MODULE_getsub
#define MODULE_doput
#define MODULE_domput
#define MODULE_putsub
#define MODULE_sendport
#define MODULE_getresp
#define MODULE_getline
#define MODULE_ftpcli_keychar
#endif

STATIC int doascii(int argc,char *argv[],void *p);
STATIC int dobatch(int argc,char *argv[],void *p);
STATIC int dobinary(int argc,char *argv[],void *p);
STATIC int docompare(int argc,char *argv[],void *p);
STATIC int doftpcd(int argc,char *argv[],void *p);
STATIC int doget(int argc,char *argv[],void *p);
STATIC int dohash(int argc,char *argv[],void *p);
STATIC int doverbose(int argc,char *argv[],void *p);
STATIC int dolist(int argc,char *argv[],void *p);
STATIC int dols(int argc,char *argv[],void *p);
STATIC int domd5(int argc,char *argv[],void *p);
STATIC int domkdir(int argc,char *argv[],void *p);
STATIC int domcompare(int argc,char *argv[],void *p);
STATIC int domget(int argc,char *argv[],void *p);
STATIC int domput(int argc,char *argv[],void *p);
STATIC int doput(int argc,char *argv[],void *p);
STATIC int doquit(int argc,char *argv[],void *p);
STATIC int doread(int argc,char *argv[],void *p);
STATIC int dormdir(int argc,char *argv[],void *p);
STATIC int dotype(int argc,char *argv[],void *p);
STATIC int doupdate(int argc,char *argv[],void *p);
STATIC int getline(/*struct session_s *sp,*/char *prompt,char *buf,int n);
STATIC int getresp(struct ftpcli *ftp,int mincode);
STATIC long getsub(struct ftpcli *ftp,_char *command,char *remotename, /*RPB*/
	FILE *fp);
STATIC long putsub(struct ftpcli *ftp,char *remotename,char *localname);
STATIC int compsub(struct ftpcli *ftp,char *localname,char *remotename);
STATIC void sendport(FILE *fp,struct sockaddr_in *socket);
STATIC int ftpcli_keychar(int c);

#ifdef MODULE_Notsess
STATIC _char *Notsess = N_("Not an FTP session!\n");
#endif

#ifdef MODULE_Ftpcmds
STATIC struct cmds Ftpcmds[FTPCMDS_MAX] = {
	N_(""),		donothing,	0, 0, NULL,
	N_("ascii"),	doascii,	0, 0, NULL,
	N_("batch"),	dobatch,	0, 0, NULL,
	N_("binary"),	dobinary,	0, 0, NULL,
	N_("cd"),	doftpcd,	0, 2, N_("cd <directory>"),
	N_("compare"),	docompare,	0, 2, N_("compare <remotefile> [<localfile>]"),
	N_("dir"),	dolist,		0, 0, NULL,
	N_("list"),	dolist,		0, 0, NULL,
	N_("get"),	doget,		0, 2, N_("get <remotefile> <localfile>"),
	N_("hash"),	dohash,		0, 0, NULL,
	N_("ls"),	dols,		0, 0, NULL,
	N_("mcompare"),	domcompare,	0, 2, N_("mcompare <file> [<file> ...]"),
	N_("md5"),	domd5,		0, 2, N_("md5 <file>"),
	N_("mget"),	domget,		0, 2, N_("mget <file> [<file> ...]"),
	N_("mkdir"),	domkdir,	0, 2, N_("mkdir <directory>"),
	N_("mput"),	domput,		0, 2, N_("mput <file> [<file> ...]"),
	N_("nlst"),	dols,		0, 0, NULL,
	N_("quit"),	doquit,		0, 0, NULL,
	N_("read"),	doread,		0, 2, N_("read <remotefile>"),
	N_("rmdir"),	dormdir,	0, 2, N_("rmdir <directory>"),
	N_("put"),	doput,		0, 2, N_("put <localfile> <remotefile>"),
	N_("type"),	dotype,		0, 0, NULL,
	N_("update"),	doupdate,	0, 0, NULL,
	N_("verbose"),	doverbose,	0, 0, NULL,
	NULL,	NULL,		0, 0, NULL,
};
#endif

/* Handle top-level FTP command */
#ifdef MODULE_doftp
int
doftp(argc,argv,p)
int argc;
char *argv[];
void *p;
{
#ifdef JOB_CONTROL
	struct session_s *sp;
#endif
	struct ftpcli ftp;
	struct sockaddr_in fsocket;
	int resp,vsave;
	char *bufsav,*cp;
	FILE *control;
	int s;
#if 1 /* Nick */
	struct sgttyb state;
	int raw;
#endif

#ifdef JOB_CONTROL
	/* Allocate a session control block */
	if((sp = newsession(Cmdline,FTP,1)) == NULL){
		_printf(_("Too many sessions\n"));
		return 1;
	}
	sp->inproc = ftpcli_keychar;
#endif
#if 1 /* Nick */
	gtty(STDIN_FILENO, &state);
	raw = state.sg_flags;
#endif
	memset(&ftp,0,sizeof(ftp));
	ftp.control = ftp.data = NULL;
	ftp.verbose = V_NORMAL;

#ifdef JOB_CONTROL
	sp->cb.ftp = &ftp;	/* Downward link */
	ftp.session = sp;	/* Upward link */
#endif

	fsocket.sin_family = AF_INET;
	if(argc < 3)
		fsocket.sin_port = IPPORT_FTP;
	else
		fsocket.sin_port = atoi(argv[2]);

	if(SETSIG(EABORT)){
#ifdef JOB_CONTROL
		freesession(sp, 1); /* does keywait(NULL, 1); beforehand */
#endif
		return 1;
	}
	_printf(_("Resolving %s...\n"),argv[1]);
	fflush(stdout); /* Nick, we don't have sesflush() anymore */
	if((fsocket.sin_addr.s_addr = resolve(argv[1])) == 0){
		_printf(Badhost,argv[1]);
#ifdef JOB_CONTROL
		freesession(sp, 1); /* does keywait(NULL, 1); beforehand */
#endif
		return 1;
	}
	/* Open the control connection */
	if((s = socket(AF_INET,SOCK_STREAM,0)) == -1){
		_printf(_("Can't create socket\n"));
#ifdef JOB_CONTROL
		freesession(sp, 1); /* does keywait(NULL, 1); beforehand */
#endif
		return 1;
	}
	if(SETSIG(EABORT)){
		goto quit;
	}
#ifdef JOB_CONTROL
	sp->network =
#endif
	control = ftp.control = fdopen(s,"r+t");
	settos(s,LOW_DELAY);
	_printf(_("Trying %s...\n"),psocket(&fsocket));
	fflush(stdout); /* Nick, we don't have sesflush() anymore */
	if(connect(s,(struct sockaddr *)&fsocket,sizeof(fsocket)) == -1){
		_perror(_("Connect failed"));
		goto quit;
	}
	_printf(_("Connected\n"));

	/* Wait for greeting from server */
	resp = getresp(&ftp,200);

	if(resp >= 400)
		goto quit;
	/* Now process responses and commands */

	if(SETSIG(EABORT)){
#if 1 /* Nick */
		/* turn echo back on, in case ^C pressed at password prompt */
		state.sg_flags = raw;
		stty(STDIN_FILENO, &state);
#endif
		/* Come back here after a ^C in command state */
		resp = 200;
	}
	while(resp != -1){
		switch(resp){
		case 220:
			/* Sign-on banner; prompt for and send USER command */
			getline(/*sp,*/"Enter user name: ",ftp.buf,LINELEN);
			/* Send the command only if the user response
			 * was non-null
			 */
			if(ftp.buf[0] != '\n'){
#if 1
				rip(ftp.buf);
				_fprintf(control, _("USER %s\r\n"), ftp.buf);
#else
				_fprintf(control, _("USER %s"),ftp.buf);
#endif
				resp = getresp(&ftp,200);
			} else
				resp = 200;	/* dummy */
			break;
		case 331:
			/* turn off echo */
#if 1 /* Nick */
			state.sg_flags &= ~ECHO;
			stty(STDIN_FILENO, &state);
#else
			sp->ttystate.echo = 0;
#endif
			getline(/*sp,*/"Password: ",ftp.buf,LINELEN);
			_printf(_("\n"));
			/* Turn echo back on */
#if 1 /* Nick */
			state.sg_flags = raw;
			stty(STDIN_FILENO, &state);
#else
			sp->ttystate.echo = 1;
#endif
			/* Send the command only if the user response
			 * was non-null
			 */
			if(ftp.buf[0] != '\n'){
#if 1
				rip(ftp.buf);
				_fprintf(control, _("PASS %s\r\n"), ftp.buf);
#else
				_fprintf(control, _("PASS %s"),ftp.buf);
#endif
#if 1 /* modification for planetmirror.com server full (want to see 530 msg) */
			} else
				_fprintf(control, _("PASS\r\n"));
			resp = getresp(&ftp,200);
#else
				resp = getresp(&ftp,200);
			} else
				resp = 200;	/* dummy */
#endif
			break;
		case 230:	/* Successful login */
			/* Find out what type of system we're talking to */
			_printf(_("ftp> syst\n"));
			_fprintf(control, _("SYST\r\n"));
			resp = getresp(&ftp,200);
			break;
		case 215:
			/* Response to SYST command */
			cp = strchr(ftp.line,' ');
			if(cp != NULL && strnicmp(cp+1,System,strlen(System)) == 0){
				ftp.type = IMAGE_TYPE;
				_printf(_("Defaulting to binary mode\n"));
			}
			resp = 200;	/* dummy */
			break;
		default:
/* abyte('('); */
			/* Test the control channel first */
			if(sockstate(fileno(control)) == NULL){
/* abyte('|'); */
				resp = -1;
				break;
			}
/* abyte(')'); */
			getline(/*sp,*/"ftp> ",ftp.buf,LINELEN);

			/* Copy because cmdparse modifies the original */
/* abyte('a'); */
			bufsav = strdupw(ftp.buf);
/* abyte('b'); */
			if((resp = cmdparse(Ftpcmds,ftp.buf,&ftp)) != -1){
/* abyte('c'); */
				/* Valid command, free buffer and get another */
				FREE(bufsav);
/* abyte('d'); */
			} else {
/* abyte('e'); */
				/* Not a local cmd, send to remote server */
#if 1
				rip(bufsav);
/* abyte('f'); */
				_fprintf(control, _("%s\r\n"), bufsav);
/* abyte('g'); */
#else
				fputs(bufsav,control);
#endif
/* abyte('h'); */
				FREE(bufsav);

				/* Enable display of server response */
/* abyte('i'); */
				vsave = ftp.verbose;
				ftp.verbose = V_NORMAL;
/* abyte('j'); */
				resp = getresp(&ftp,200);
/* abyte('k'); */
				ftp.verbose = vsave;
			}
/* abyte('l'); */
		}
	}
quit:	cp = sockerr(fileno(control));
	_printf(_("Closed: %s\n"),cp != NULL ? cp : "EOF");

	if(ftp.fp != NULL && ftp.fp != stdout)
		fclose(ftp.fp);
	if(ftp.data != NULL)
		fclose(ftp.data);
	if(ftp.control != NULL){
		fclose(ftp.control);
		ftp.control = NULL;
#ifdef JOB_CONTROL
		sp->network = NULL;
#endif
	}
#ifdef JOB_CONTROL
	/* relying on freesession() to return harmlessly on NULL input */
	freesession(ftp.session, 1); /* does keywait(NULL, 1); beforehand */
#endif
	return 0;
}
#endif

/* Control verbosity level */

#ifdef MODULE_doverbose
STATIC int
doverbose(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct ftpcli *ftp;

	if((ftp = (struct ftpcli *)p) == NULL)
		return -1;
	return setshort(&ftp->verbose, _("Verbose"), argc, argv);
}
#endif

/* Enable/disable command batching */

#ifdef MODULE_dobatch
STATIC int
dobatch(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct ftpcli *ftp;

	if((ftp = (struct ftpcli *)p) == NULL)
		return -1;
	return setbool(&ftp->batch, _("Command batching"), argc, argv);
}
#endif
/* Enable/disable update flag */

#ifdef MODULE_doupdate
STATIC int
doupdate(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct ftpcli *ftp;

	if((ftp = (struct ftpcli *)p) == NULL)
		return -1;
	return setbool(&ftp->update, _("Update with MD5"), argc, argv);
}
#endif
/* Set verbosity to high (convenience command) */

#ifdef MODULE_dohash
STATIC int
dohash(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct ftpcli *ftp;

	if((ftp = (struct ftpcli *)p) == NULL)
		return -1;
	ftp->verbose = V_HASH;
	return 0;
}
#endif

/* Close session */
#ifdef MODULE_doquit
STATIC int
doquit(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct ftpcli *ftp;

	ftp = (struct ftpcli *)p;
	if(ftp == NULL)
		return -1;
	_fprintf(ftp->control, _("QUIT\r\n"));
	getresp(ftp,200);	/* Get the closing message */
	getresp(ftp,200);	/* Wait for the server to close */
	return -1;
}
#endif

/* Translate 'cd' to 'cwd' for convenience */

#ifdef MODULE_doftpcd
STATIC int
doftpcd(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct ftpcli *ftp;

	ftp = (struct ftpcli *)p;
	if(ftp == NULL)
		return -1;
	_fprintf(ftp->control, _("CWD %s\r\n"),argv[1]);
	return getresp(ftp,200);
}
#endif
/* Translate 'mkdir' to 'xmkd' for convenience */

#ifdef MODULE_domkdir
STATIC int
domkdir(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct ftpcli *ftp;

	ftp = (struct ftpcli *)p;
	if(ftp == NULL)
		return -1;
	_fprintf(ftp->control, _("XMKD %s\r\n"),argv[1]);
	return getresp(ftp,200);
}
#endif
/* Translate 'rmdir' to 'xrmd' for convenience */

#ifdef MODULE_dormdir
STATIC int
dormdir(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct ftpcli *ftp;

	ftp = (struct ftpcli *)p;
	if(ftp == NULL)
		return -1;
	_fprintf(ftp->control, _("XRMD %s\r\n"),argv[1]);
	return getresp(ftp,200);
}
#endif

#ifdef MODULE_dobinary
STATIC int
dobinary(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	char *args[2];

	args[1] = "I";
	return dotype(2,args,p);
}
#endif

#ifdef MODULE_doascii
STATIC int
doascii(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	char *args[2];

	args[1] = "A";
	return dotype(2,args,p);
}
#endif

/* Handle "type" command from user */

#ifdef MODULE_dotype
STATIC int
dotype(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct ftpcli *ftp;

	ftp = (struct ftpcli *)p;
	if(ftp == NULL)
		return -1;
	if(argc < 2){
		switch(ftp->type){
		case IMAGE_TYPE:
			_printf(_("Image\n"));
			break;
		case ASCII_TYPE:
			_printf(_("Ascii\n"));
			break;
		case LOGICAL_TYPE:
			_printf(_("Logical bytesize %u\n"),ftp->logbsize);
			break;
		}
		return 0;
	}
	switch(*argv[1]){
	case 'i':
	case 'I':
	case 'b':
	case 'B':
		ftp->type = IMAGE_TYPE;
		break;
	case 'a':
	case 'A':
		ftp->type = ASCII_TYPE;
		break;
	case 'L':
	case 'l':
		ftp->type = LOGICAL_TYPE;
		ftp->logbsize = atoi(argv[2]);
		break;
	default:
		_printf(_("Invalid type %s\n"),argv[1]);
		return 1;
	}
	return 0;
}
#endif
/* Start receive transfer. Syntax: get <remote name> [<local name>] */

#ifdef MODULE_doget
STATIC int
doget(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	char *remotename,*localname;
	register struct ftpcli *ftp;
	FILE *fp;
	char *mode;

	ftp = (struct ftpcli *)p;
	if(ftp == NULL){
		_printf(Notsess);
		return 1;
	}
	remotename = argv[1];
	if(argc < 3)
		localname = remotename;
	else
		localname = argv[2];

	switch(ftp->type){
	case IMAGE_TYPE:
	case LOGICAL_TYPE:
		mode = WRITE_BINARY;
		break;
	case ASCII_TYPE:
		mode = WRITE_TEXT;
		break;
	}
	if((fp = fopen(localname,mode)) == NULL){
		_printf(_("Can't write %s"),localname);
		_perror(_(""));
		return 1;
	}
	getsub(ftp,_("RETR"),remotename,fp);
	fclose(fp);
	return 0;
}
#endif
/* Read file direct to screen. Syntax: read <remote name> */

#ifdef MODULE_doread
STATIC int
doread(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct ftpcli *ftp;

	if((ftp = (struct ftpcli *)p) == NULL){
		_printf(Notsess);
		return 1;
	}
	getsub(ftp,_("RETR"),argv[1],stdout);
	return 0;
}
#endif
/* Get a collection of files */

#ifdef MODULE_domget
STATIC int
domget(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct ftpcli *ftp;
	FILE *files,*fp;
	char *buf,*mode;
	int i;
	long r;

	if((ftp = (struct ftpcli *)p) == NULL){
		_printf(Notsess);
		return 1;
	}
	switch(ftp->type){
	case IMAGE_TYPE:
	case LOGICAL_TYPE:
		mode = WRITE_BINARY;
		break;
	case ASCII_TYPE:
		mode = WRITE_TEXT;
		break;
	}
	buf = mallocw(DIRBUF);
	ftp->state = RECEIVING_STATE;
	for(i=1;i<argc;i++){
		files = tmpfile();
		r = getsub(ftp,_("NLST"),argv[i],files);
		if(ftp->abort)
 {
 fclose(files);
			break;	/* Aborted */
 }
		if(r == -1){
 fclose(files);
			_printf(_("Can't NLST %s\n"),argv[i]);
			continue;
		}
		/* The tmp file now contains a list of the remote files, so
		 * go get 'em. Break out if the user signals an abort.
		 */
		rewind(files);
		while(fgets(buf,DIRBUF,files) != NULL){
			rip(buf);
			if(!ftp->update || compsub(ftp,buf,buf) != 0){
				if((fp = fopen(buf,mode)) == NULL){
					_printf(_("Can't write %s"),buf);
					_perror(_(""));
					continue;
				}
				getsub(ftp,_("RETR"),buf,fp);
				fclose(fp);
			}
			if(ftp->abort){
				/* User abort */
				ftp->abort = 0;
				fclose(files);
				free(buf);
				ftp->state = COMMAND_STATE;
				return 1;
			}
		}
		fclose(files);
	}
	free(buf);
	ftp->state = COMMAND_STATE;
	ftp->abort = 0;
	return 0;
}
#endif
/* List remote directory. Syntax: dir <remote files> [<local name>] */

#ifdef MODULE_dolist
STATIC int
dolist(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct ftpcli *ftp;
	FILE *fp;

	ftp = (struct ftpcli *)p;
	if(ftp == NULL){
		_printf(Notsess);
		return 1;
	}

	if(argc > 2)
		{
		fp = fopen(argv[2],WRITE_TEXT);
		if(fp == NULL)
			{
			_printf(_("Can't write local file"));
			_perror(_(""));
			return 1;
			}
		getsub(ftp,_("LIST"),argv[1],fp);
		fclose(fp);
		}
	else
		{
		getsub(ftp,_("LIST"),argv[1],stdout);
		}

	return 0;
}
#endif
/* Remote directory list, short form. Syntax: ls <remote files> [<local name>] */

#ifdef MODULE_dols
STATIC int
dols(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct ftpcli *ftp;
	FILE *fp;

	if((ftp = (struct ftpcli *)p) == NULL){
		_printf(Notsess);
		return 1;
	}
	if(argc > 2)
		{
		fp = fopen(argv[2],WRITE_TEXT);
		if(fp == NULL)
			{
			_printf(_("Can't write local file"));
			_perror(_(""));
			return 1;
			}
		getsub(ftp,_("NLST"),argv[1],fp);
		fclose(fp);
		}
	else
		{
		getsub(ftp,_("NLST"),argv[1],stdout);
		}
	return 0;
}
#endif

#ifdef MODULE_domd5
STATIC int
domd5(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	char *remotename;
	register struct ftpcli *ftp;
	FILE *control;
	int resp;
	int typewait = 0;

	ftp = (struct ftpcli *)p;
	if(ftp == NULL){
		_printf(Notsess);
		return 1;
	}
	control = ftp->control;
	remotename = argv[1];
	if(ftp->typesent != ftp->type){
		switch(ftp->type){
		case ASCII_TYPE:
			_fprintf(control, _("TYPE A\r\n"));
			break;
		case IMAGE_TYPE:
			_fprintf(control, _("TYPE I\r\n"));
			break;
		case LOGICAL_TYPE:
			_fprintf(control, _("TYPE L %d\r\n"),ftp->logbsize);
			break;
		}
		ftp->typesent = ftp->type;
		if(!ftp->batch){
			resp = getresp(ftp,200);
			if(resp == -1 || resp > 299)
				goto error1;
		} else
			typewait = 1;

	}
	_fprintf(control, _("XMD5 %s\r\n"),remotename);
	if(typewait)
		(void)getresp(ftp,200);
	(void)getresp(ftp,200);
error1:
	return 0;
}
#endif

#ifdef MODULE_docompare
STATIC int
docompare(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	char *remotename,*localname;
	register struct ftpcli *ftp;

	ftp = (struct ftpcli *)p;
	if(ftp == NULL){
		_printf(Notsess);
		return 1;
	}
	remotename = argv[1];
	if(argc > 2)
		localname = argv[2];
	else
		localname = remotename;

	if(compsub(ftp,localname,remotename) == 0)
		_printf(_("Same\n"));
	else
		_printf(_("Different\n"));
	return 0;
}
#endif
/* Compare a collection of files */

#ifdef MODULE_domcompare
STATIC int
domcompare(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct ftpcli *ftp;
	FILE *files;
	char *buf;
	int i;
	long r;

	if((ftp = (struct ftpcli *)p) == NULL){
		_printf(Notsess);
		return 1;
	}
	buf = mallocw(DIRBUF);
	ftp->state = RECEIVING_STATE;
	for(i=1;i<argc;i++){
		files = tmpfile();
		r = getsub(ftp,_("NLST"),argv[i],files);
		if(ftp->abort)
 {
 fclose(files);
			break;	/* Aborted */
 }
		if(r == -1){
 fclose(files);
			_printf(_("Can't NLST %s\n"),argv[i]);
			continue;
		}
		/* The tmp file now contains a list of the remote files, so
		 * go get 'em. Break out if the user signals an abort.
		 */
		rewind(files);
		while(fgets(buf,DIRBUF,files) != NULL){
			rip(buf);
			if(compsub(ftp,buf,buf) == 0)
				_printf(_("%s - Same\n"),buf);
			else
				_printf(_("%s - Different\n"),buf);

			if(ftp->abort){
				/* User abort */
				ftp->abort = 0;
				fclose(files);
				free(buf);
				ftp->state = COMMAND_STATE;
				return 1;
			}
		}
		fclose(files);
	}
	free(buf);
	ftp->state = COMMAND_STATE;
	ftp->abort = 0;
	return 0;
}
#endif
/* Common subroutine to compare a local with a remote file
 * Return 1 if files are different, 0 if they are the same
 */

#ifdef MODULE_compsub
STATIC int
compsub(ftp,localname,remotename)
struct ftpcli *ftp;
char *localname;
char *remotename;
{
	char *mode,*cp;
	FILE *control,*fp;
	int resp,i;
	int typewait = 0;
	uint8 remhash[16];
	uint8 lochash[16];

	control = ftp->control;

	switch(ftp->type){
	case IMAGE_TYPE:
	case LOGICAL_TYPE:
		mode = READ_BINARY;
		break;
	case ASCII_TYPE:
		mode = READ_TEXT;
		break;
	}
	if((fp = fopen(localname,mode)) == NULL){
		_printf(_("Can't read local file %s\n"),localname);
		return 1;
	}
	if(ftp->typesent != ftp->type){
		switch(ftp->type){
		case ASCII_TYPE:
			_fprintf(control, _("TYPE A\r\n"));
			break;
		case IMAGE_TYPE:
			_fprintf(control, _("TYPE I\r\n"));
			break;
		case LOGICAL_TYPE:
			_fprintf(control, _("TYPE L %d\r\n"),ftp->logbsize);
			break;
		}
		ftp->typesent = ftp->type;
		if(!ftp->batch){
			resp = getresp(ftp,200);
			if(resp == -1 || resp > 299)
				goto error1;
		} else
			typewait = 1;
	}
	_fprintf(control, _("XMD5 %s\r\n"),remotename);
	/* Try to overlap the two MD5 operations */
	md5hash(fp,lochash,ftp->type == ASCII_TYPE);
	fclose(fp);
	if(typewait && (resp = getresp(ftp,200)) > 299)
		goto error1;
	if((resp = getresp(ftp,200)) > 299){
		if(resp == 500)
			ftp->update = 0;	/* XMD5 not supported */
		goto error1;
	}
	if((cp = strchr(ftp->line,' ')) == NULL){
		_printf(_("Error in response\n"));
		goto error1;
	}
	/* Convert ascii/hex back to binary */
	readhex(remhash,cp,sizeof(remhash));
	if(ftp->verbose > 1){
		_printf(_("Loc "));
		for(i=0;i<sizeof(lochash);i++)
			_printf(_("%02x"),lochash[i]);
		_printf(_(" %s\n"),localname);
	}
	if(memcmp(lochash,remhash,sizeof(remhash)) == 0)
		return 0;
	else
		return 1;
error1:
	return 1;
}
#endif


/* Common code to LIST/NLST/RETR and mget
 * Returns number of bytes received if successful
 * Returns -1 on error
 */

#ifdef MODULE_getsub
STATIC long
getsub(ftp,command,remotename,fp)
register struct ftpcli *ftp;
_char *command; char *remotename; /*RPB*/
FILE *fp;
{
	unsigned long total;
	FILE *control;
#if 0 /* fdopen() is now done after the accept() */
	int cnt;
#endif
	int resp,i,savmode;
	struct sockaddr_in lsocket;
	struct sockaddr_in lcsocket;
	int32 startclk,rate;
	int vsave;
	int typewait = 0;
	int prevstate;
	int d;
#if 1 /* fdopen() is now done after the accept() */
	int e;
#endif
	char *duptext; /*RPB*/

	if(ftp == NULL)
		return -1;

#if 1 /* now done before opening the server socket */
	duptext = _strdup(command);
	if (duptext == NULL)
		{
		__getfail(command);
		return -1;
		}
#endif

	savmode = ftp->type;
	control = ftp->control;

	/* Open the data connection */
	d = socket(AF_INET,SOCK_STREAM,0);
#if 1 /* testing testing */
	listen(d,1);	/* Make normal server socket */
#else
	listen(d,0);	/* Accept only one connection */
#endif

#if 0 /* fdopen() is now done after the accept() */
	switch(ftp->type){
	case IMAGE_TYPE:
	case LOGICAL_TYPE:
		ftp->data = fdopen(d,"r+b");
		break;
	case ASCII_TYPE:
		ftp->data = fdopen(d,"r+t");
		break;
	}
#endif

	prevstate = ftp->state;
	ftp->state = RECEIVING_STATE;

#if 0 /* now done before opening the server socket */
	/*RPB*/
	if ((duptext = _strdup(command)) == NULL)
	{
		__getfail(command);
		return -1;
	}
	/*RPB*/
#endif

	/* Send TYPE message, if necessary */
	if(_strcmp(duptext,_("LIST")) == 0 || _strcmp(duptext,_("NLST")) == 0){
		/* Directory listings are always in ASCII */
		ftp->type = ASCII_TYPE;
	}
	if(ftp->typesent != ftp->type){
		switch(ftp->type){
		case ASCII_TYPE:
			_fprintf(control, _("TYPE A\r\n"));
			break;
		case IMAGE_TYPE:
			_fprintf(control, _("TYPE I\r\n"));
			break;
		case LOGICAL_TYPE:
			_fprintf(control, _("TYPE L %d\r\n"),ftp->logbsize);
			break;
		}
		ftp->typesent = ftp->type;
		if(!ftp->batch){
			resp = getresp(ftp,200);
			if(resp == -1 || resp > 299)
				goto error1;
		} else
			typewait = 1;
	}
	/* Send the PORT message. Use the IP address
	 * on the local end of our control connection.
	 */
	i = SOCKSIZE;
	getsockname(d,(struct sockaddr *)&lsocket,&i); /* Get port number */
	i = SOCKSIZE;
	getsockname(fileno(ftp->control),(struct sockaddr *)&lcsocket,&i);
	lsocket.sin_addr.s_addr = lcsocket.sin_addr.s_addr;
	sendport(control,&lsocket);
	if(!ftp->batch){
		/* Get response to PORT command */
		resp = getresp(ftp,200);
		if(resp == -1 || resp > 299)
			goto error1;
	}

	/* Generate the command to start the transfer */
	if(remotename != NULL)
		_fprintf(control, _("%s %s\r\n"),duptext,remotename); /*RPB*/
	else
		_fprintf(control, _("%s\r\n"),duptext); /*RPB*/

	if(ftp->batch){
		/* Get response to TYPE command, if sent */
		if(typewait){
			resp = getresp(ftp,200);
			if(resp == -1 || resp > 299)
				goto error1;
		}
		/* Get response to PORT command */
		resp = getresp(ftp,200);
		if(resp == -1 || resp > 299)
			goto error1;
	}
	/* Get the intermediate "150" response */
	resp = getresp(ftp,100);
	if(resp == -1 || resp >= 400)
		goto error1;

	/* Wait for the server to open the data connection */
#if 0 /* fdopen() is now done after the accept() */
	cnt = 0;
	d = accept(d,NULL,&cnt);
#else
	e = accept(d,NULL,(int *)NULL);
	if (e != d)
		{
		/* means backlog argument to listen() was > 0 */
		/* note: susv3 conforming apps will always get here */
		close(d); /* don't want the server socket anymore */
		}
	switch(ftp->type){ /* change to savmode for original NOS behaviour */
	case IMAGE_TYPE:
	case LOGICAL_TYPE:
		ftp->data = fdopen(e,"r+b");
		break;
	case ASCII_TYPE:
		ftp->data = fdopen(e,"r+t");
		break;
	}
#endif
	startclk = msclock();

	/* If output is to the screen, temporarily disable hash marking */
	vsave = ftp->verbose;
	if(vsave >= V_HASH && fp == NULL)
		ftp->verbose = V_NORMAL;
	total = recvfile(fp,ftp->data,ftp->type,ftp->verbose);
	/* Immediately close the data connection; some servers (e.g., TOPS-10)
	 * wait for the data connection to close completely before returning
	 * the completion message on the control channel
	 */
	fclose(ftp->data);
	ftp->data = NULL;

#ifdef	CPM
	if(fp != NULL && ftp->type == ASCII_TYPE)
		putc(CTLZ,fp);
#endif
	if(remotename == NULL)
		remotename = "";
	if(total == -1){
		_printf(_("%s %s: Error/abort during data transfer\n"),duptext,remotename); /*RPB*/
	} else if(ftp->verbose >= V_SHORT){
		startclk = msclock() - startclk;
		rate = 0;
		if(startclk != 0){	/* Avoid divide-by-zero */
			if(total < 4294967L) {
				rate = (total*1000)/startclk;
			} else {	/* Avoid overflow */
				rate = total/(startclk/1000);
			}
		}
		_printf(_("%s %s: %lu bytes in %lu sec (%lu/sec)\n"),
		 duptext,remotename, total,startclk/1000,rate); /*RPB*/
	}
	/* Get the "Sent" message */
	getresp(ftp,200);

	ftp->state = prevstate;
	ftp->verbose = vsave;
	ftp->type = savmode;
	free(duptext); /*RPB*/
	return total;

error1:
	close(d); /* don't want the server socket anymore */
	/* Error, quit */
#if 0 /* this is not needed, our caller will always close it */
	if(fp != NULL && fp != stdout)
		fclose(fp);
#endif
	fclose(ftp->data); /* may be NULL, but if not, this does close(e); */
	ftp->data = NULL;
	ftp->state = prevstate;
	ftp->type = savmode;
	free(duptext); /*RPB*/
	return -1;
}
#endif
/* Send a file. Syntax: put <local name> [<remote name>] */

#ifdef MODULE_doput
STATIC int
doput(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct ftpcli *ftp;
	char *remotename,*localname;

	if((ftp = (struct ftpcli *)p) == NULL){
		_printf(Notsess);
		return 1;
	}
	localname = argv[1];
	if(argc < 3)
		remotename = localname;
	else
		remotename = argv[2];

	putsub(ftp,remotename,localname);
	return 0;
}
#endif
/* Put a collection of files */

#ifdef MODULE_domput
STATIC int
domput(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct ftpcli *ftp;
	FILE *files;
	int i;
	char *buf;

	if((ftp = (struct ftpcli *)p) == NULL){
		_printf(Notsess);
		return 1;
	}
	if((files = tmpfile()) == NULL){
		_printf(_("Can't list local files\n"));
		return 1;
	}
	for(i=1;i<argc;i++)
		getdir(argv[i],0,files);

	rewind(files);
	buf = mallocw(DIRBUF);
	ftp->state = SENDING_STATE;
	while(fgets(buf,DIRBUF,files) != NULL){
		rip(buf);
		if(!ftp->update || compsub(ftp,buf,buf) != 0)
			putsub(ftp,buf,buf);
		if(ftp->abort)
			break;		/* User abort */
	}
	fclose(files);
	free(buf);
	ftp->state = COMMAND_STATE;
	ftp->abort = 0;
	return 0;
}
#endif
/* Common code to put, mput.
 * Returns number of bytes sent if successful
 * Returns -1 on error
 */

#ifdef MODULE_putsub
STATIC long
putsub(ftp,remotename,localname)
register struct ftpcli *ftp;
char *remotename,*localname;
{
	char *mode;
	int i,resp,d;
#if 1 /* fdopen() is now done after the accept() */
	int e;
#endif
	unsigned long total;
	FILE *fp,*control;
	struct sockaddr_in lsocket,lcsocket;
	int32 startclk,rate;
	int typewait = 0;
	int prevstate;

	control = ftp->control;
	if(ftp->type == IMAGE_TYPE)
		mode = READ_BINARY;
	else
		mode = READ_TEXT;

	/* Open the file */
	if((fp = fopen(localname,mode)) == NULL){
#if 1 /* Nick */
		_printf(_("Can't read %s: %s\n"),localname,strerror(errno));
#else
		_printf(_("Can't read %s: %s\n"),localname,sys_errlist[errno]);
#endif
		return -1;
	}
	if(ftp->type == ASCII_TYPE && isbinary(fp)){
		_printf(_("Warning: type is ASCII and %s appears to be binary\n"),localname);
	}
	/* Open the data connection */
	d = socket(AF_INET,SOCK_STREAM,0);
#if 0 /* fdopen() is now done after the accept() */
	ftp->data = fdopen(d,"w+");
#endif
#if 1 /* testing testing */
	listen(d,1);	/* Make normal server socket */
#else
	listen(d,0);	/* Accept only one connection */
#endif
	prevstate = ftp->state;
	ftp->state = SENDING_STATE;

	/* Send TYPE message, if necessary */
	if(ftp->typesent != ftp->type){
		switch(ftp->type){
		case ASCII_TYPE:
			_fprintf(control, _("TYPE A\r\n"));
			break;
		case IMAGE_TYPE:
			_fprintf(control, _("TYPE I\r\n"));
			break;
		case LOGICAL_TYPE:
			_fprintf(control, _("TYPE L %d\r\n"),ftp->logbsize);
			break;
		}
		ftp->typesent = ftp->type;

		/* Get response to TYPE command */
		if(!ftp->batch){
			resp = getresp(ftp,200);
			if(resp == -1 || resp > 299){
				goto error1;
			}
		} else
			typewait = 1;
	}
	/* Send the PORT message. Use the IP address
	 * on the local end of our control connection.
	 */
	i = SOCKSIZE;
	getsockname(d,(struct sockaddr *)&lsocket,&i);
	i = SOCKSIZE;
	getsockname(fileno(ftp->control),(struct sockaddr *)&lcsocket,&i);
	lsocket.sin_addr.s_addr = lcsocket.sin_addr.s_addr;
	sendport(control,&lsocket);
	if(!ftp->batch){
		/* Get response to PORT command */
		resp = getresp(ftp,200);
		if(resp == -1 || resp > 299){
			goto error1;
		}
	}
	/* Generate the command to start the transfer */
	_fprintf(control, _("STOR %s\r\n"),remotename);

	if(ftp->batch){
		/* Get response to TYPE command, if sent */
		if(typewait){
			resp = getresp(ftp,200);
			if(resp == -1 || resp > 299){
				goto error1;
			}
		}
		/* Get response to PORT command */
		resp = getresp(ftp,200);
		if(resp == -1 || resp > 299){
			goto error1;
		}
	}
	/* Get the intermediate "150" response */
	resp = getresp(ftp,100);
	if(resp == -1 || resp >= 400){
		goto error1;
	}

	/* Wait for the data connection to open. Otherwise the first
	 * block of data would go out with the SYN, and this may confuse
	 * some other TCPs
	 */
#if 0 /* fdopen() is now done after the accept() */
	accept(d,NULL,(int *)NULL);
#else
	e = accept(d,NULL,(int *)NULL);
	if (e != d)
		{
		/* means backlog argument to listen() was > 0 */
		/* note: susv3 conforming apps will always get here */
		close(d); /* don't want the server socket anymore */
		}
	ftp->data = fdopen(e,"w+");
#endif
	startclk = msclock();

	total = sendfile(fp,ftp->data,ftp->type,ftp->verbose);
	fflush(ftp->data);
	shutdown(fileno(ftp->data),1);	/* Send EOF (FIN) */
	fclose(fp);

	/* Wait for control channel ack before calculating transfer time;
	 * this accounts for transmitted data in the pipe
	 */
	getresp(ftp,200);
	fclose(ftp->data);
	ftp->data = NULL;

	if(total == -1){
		_printf(_("STOR %s: Error/abort during data transfer\n"),remotename);
	} else if(ftp->verbose >= V_SHORT){
		startclk = msclock() - startclk;
		rate = 0;
		if(startclk != 0){	/* Avoid divide-by-zero */
			if(total < 4294967L) {
				rate = (total*1000)/startclk;
			} else {	/* Avoid overflow */
				rate = total/(startclk/1000);
			}
		}
		_printf(_("STOR %s: %lu bytes in %lu sec (%lu/sec)\n"),
		 remotename,total,startclk/1000,rate);
	}
	ftp->state = prevstate;
	return total;

error1:
	close(d); /* don't want the server socket anymore */
	/* Error, quit */
	fclose(fp);
	fclose(ftp->data); /* may be NULL, but if not, this does close(e); */
	ftp->data = NULL;
	ftp->state = prevstate;
	return -1;
}
#endif
/* send PORT message */

#ifdef MODULE_sendport
STATIC void
sendport(fp,socket)
FILE *fp;
struct sockaddr_in *socket;
{
	/* Send PORT a,a,a,a,p,p message */
	_fprintf(fp, _("PORT %u,%u,%u,%u,%u,%u\r\n"),
		hibyte(hiword(socket->sin_addr.s_addr)),
		lobyte(hiword(socket->sin_addr.s_addr)),
		hibyte(loword(socket->sin_addr.s_addr)),
		lobyte(loword(socket->sin_addr.s_addr)),
		hibyte(socket->sin_port),
		lobyte(socket->sin_port));
}
#endif

/* Wait for, read and display response from FTP server. Return the result code.
 */

#ifdef MODULE_getresp
STATIC int
getresp(ftp,mincode)
struct ftpcli *ftp;
int mincode;	/* Keep reading until at least this code comes back */
{
	int rval;

	fflush(stdout); /* Nick, we don't have sesflush() anymore */
	fflush(ftp->control);
	for(;;){
		/* Get line */
		if(fgets(ftp->line,LINELEN,ftp->control) == NULL){
			rval = -1;
			break;
		}
		rip(ftp->line);		/* Remove cr/lf */
		rval = atoi(ftp->line);
		if(rval >= 400 || ftp->verbose >= V_NORMAL)
			_printf(_("%s\n"),ftp->line);	/* Display to user */

		/* Messages with dashes are continued */
		if(ftp->line[3] != '-' && rval >= mincode)
			break;
	}
	return rval;
}
#endif

/* Issue a prompt and read a line from the user */
#ifdef MODULE_getline
STATIC int
getline(/*sp,*/prompt,buf,n)
/*struct session_s *sp;*/
char *prompt;
char *buf;
int n;
{
	printf(prompt);
	fflush(stdout);
	fgets(buf,n,stdin);
	return strlen(buf);
}
#endif

#if 0 /*def MODULE_ftpcli_keychar*/
STATIC int
ftpcli_keychar(c)
int c;
{
	struct ftpcli *ftp;

	if(c != CTLC)
		return 1;	/* Ignore all but ^C */

#if 1 /* due to sesflush() trying to fflush() a closed stream... */
	_printf(_("^C\n"));
	fflush(stdout); /* Nick, we don't have sesflush() anymore */
#else
	_fprintf(my_session_p->output, _("^C\n"));
#endif
	ftp = my_thread_p->process_p->session_p->cb.ftp;
	switch(ftp->state){
	case COMMAND_STATE:
#if 1
		alert(my_thread_p, EABORT);
#else
		alert(my_session_p->proc,EABORT);
#endif
		break;
	case SENDING_STATE:
		/* Send a premature EOF.
		 * Unfortunately we can't just reset the connection
		 * since the remote side might end up waiting forever
		 * for us to send something.
		 */
		shutdown(fileno(ftp->data),1);	/* Note fall-thru */
		ftp->abort = 1;
		break;
	case RECEIVING_STATE:
		/* Just blow away the receive socket */
		shutdown(fileno(ftp->data),2);	/* Note fall-thru */
		ftp->abort = 1;
		break;
	}
	return 0;
}
#endif

