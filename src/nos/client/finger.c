/* Internet finger client
 * Copyright 1991 Phil Karn, KA9Q
 */
#include <stdio.h>
#include <string.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include "socket/socket.h"
#ifdef JOB_CONTROL
#include "kernel/session.h"
#endif
#include "internet/netuser.h"
#include "main/commands.h"
/*#include "driver/devtty.h"*/
#include "kernel/thread.h" /* for my_thread_p */
#include <libintl.h>
#include "po/messages.h"

#ifdef JOB_CONTROL
static int keychar(int c);
#endif

int
dofinger(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct sockaddr_in sock;
	char *cp;
	int s,i;
	int c;
#ifdef JOB_CONTROL
	struct session_s *sp;
#endif
	FILE *network;

#ifdef JOB_CONTROL
	/* Allocate a session descriptor */
	if((sp = newsession(Cmdline,FINGER,1)) == NULL){
		_printf(_("Too many sessions\n"));
		keywait(NULL,1);
		return 1;
	}
	sp->inproc = keychar;	/* Intercept ^C */
#endif
#if 0 /* change this to use sgtty.h, eventually */
	sp->ttystate.echo = sp->ttystate.edit = 0;
#endif
	sock.sin_family = AF_INET;
	sock.sin_port = IPPORT_FINGER;
	for(i=1;i<argc;i++){
		cp = strchr(argv[i],'@');
		if(cp == NULL){
			_printf(_("%s: local names not supported\n"),argv[i]);
			continue;
		}
		*cp++ = '\0';
		_printf(_("%s@%s:\n"),argv[i],cp);
		_printf(_("Resolving %s...\n"),cp);
		if((sock.sin_addr.s_addr = resolve(cp)) == 0){
			_printf(_("unknown\n"));
			continue;
		}
		_printf(_("Trying %s...\n"),psocket((struct sockaddr *)&sock));
		if((s = socket(AF_INET,SOCK_STREAM,0)) == -1){
			_printf(_("Can't create socket\n"));
			break;
		}
		if(connect(s,(struct sockaddr *)&sock,sizeof(sock)) == -1){
			cp = sockerr(s);
			_printf(_("Connect failed: %s\n"),cp != NULL ? cp : "");
			close/*_s*/(s);
			continue;
		}
		_printf(_("Connected\n"));

#ifdef JOB_CONTROL
		sp->network =
#endif
		network = fdopen(s,"r+t");
		_fprintf(network, _("%s\n"),argv[i]);
		fflush(stdout);
		while((c = getc(network)) != EOF)
			putchar(c);

		fclose(network);
#ifdef JOB_CONTROL
		sp->network = NULL;
#endif
	}
#ifdef JOB_CONTROL
	freesession(sp, 1); /* does keywait(NULL, 1); beforehand */
#endif
	return 0;
}

#ifdef JOB_CONTROL
static int
keychar(c)
int c;
{
	if(c != CTLC)
		return 1;	/* Ignore all but ^C */

#if 1
	_printf(_("^C\n"));
	fflush(stdout);
	alert(my_thread_p, EABORT);
#else
	_fprintf(my_session_p->output, _("^C\n"));
	alert(my_session_p->proc,EABORT);
#endif
	return 0;
}
#endif

