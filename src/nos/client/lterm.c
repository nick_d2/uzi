/* Support local term on com port */
/* Apr 04       RPB     Po-ed most of the remaining strings */

#include <stdio.h>
#include "nos/global.h"
#include "internet/internet.h"
#include "internet/netuser.h"
#include "socket/socket.h"
#ifdef JOB_CONTROL
#include "kernel/session.h"
#endif
#include "z80/asci.h"
#include "main/main.h"
#include <libintl.h>
#include "po/messages.h"

static void lterm_rx(int,void *,void *);

int
dolterm(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	FILE *network = NULL;
	struct iface_s *ifp;
	int (*rawsave)(struct iface_s *,struct mbuf_s **);
	int s;	/* Network socket */
	struct sockaddr_in fsocket;
#ifdef JOB_CONTROL
	struct session_s *sp;
#else
	struct thread_s *proc1;
#endif
	int c;
	int otrigchar;

	if ((ifp = if_lookup(argv[1])) == NULL){
		_printf(_("Interface %s unknown\n"),argv[1]);
		return 1;
	}
	if (ifp->dev >= ASCI_MAX || Asci[ifp->dev] == NULL ||
			Asci[ifp->dev]->iface != ifp){
		_printf(_("Interface %s not asci port\n"),argv[1]);
		return 1;
	}
	if (ifp->raw == bitbucket){
		_printf(_("tip or dialer session already active on %s\n"),argv[1]);
		return 1;
	}
	fsocket.sin_family = AF_INET;
	if ((fsocket.sin_addr.s_addr = resolve(argv[2])) == 0){
		_printf(Badhost,argv[2]);
#ifdef JOB_CONTROL
		freesession(sp, 1); /* does keywait(NULL, 1); beforehand */
#endif
		return 1;
	}
	if (argc > 3)
		fsocket.sin_port = atoi(argv[3]);
	else
		fsocket.sin_port = IPPORT_TELNET;

#ifdef JOB_CONTROL
	/* Allocate a session descriptor */
	if ((sp = newsession(Cmdline,TIP,1)) == NULL){
		_printf(_("Too many sessions\n"));
		return 1;
	}
#endif
	/* Save output handler and temporarily redirect output to null */
	rawsave = ifp->raw;
	ifp->raw = bitbucket;

	/* Suspend the packet input driver. Note that the transmit driver
	 * is left running since we use it to send buffers to the line.
	 */
	suspend(ifp->rxproc);

	/* Temporarily change the trigger character */
	otrigchar = Asci[ifp->dev]->trigchar;
	Asci[ifp->dev]->trigchar = -1;

#ifdef	notdef
	/* Wait for CD (wired to DTR from local terminal) to go high */
	get_rlsd_asci(ifp->dev,1);
#endif
	if ((s = socket(AF_INET,SOCK_STREAM,0)) == -1){
		_printf(_("Can't create socket\n"));
#ifdef JOB_CONTROL
		freesession(sp, 1); /* does keywait(NULL, 1); beforehand */
#endif
		goto cleanup;
	}
	settos(s,LOW_DELAY);
	network = fdopen(s,"r+b");
	setvbuf(network,NULL,_IONBF,0);
	if (connect(s,(struct sockaddr *)&fsocket,SOCKSIZE) == -1){
		_perror(_("connect failed"));
#ifdef JOB_CONTROL
		freesession(sp, 1); /* does keywait(NULL, 1); beforehand */
#endif
		goto cleanup;
	}
	/* Spawn task to handle network -> serial port traffic */
#ifdef JOB_CONTROL
	sp->proc1 = process_thread_create(daemon_process_p, "lterm",512,lterm_rx,ifp->dev,(void *)network,NULL,0);
#else
	proc1 = process_thread_create(daemon_process_p, "lterm",512,lterm_rx,ifp->dev,(void *)network,NULL,0);
#endif

	/* Loop sending from the serial port to the network */
	while ((c = get_asci(ifp->dev)) != -1){
		putchar(c);
		putc(c,network);
		fflush(network);
	}
cleanup:
#ifdef JOB_CONTROL
	killproc(sp->proc1);
	sp->proc1 = NULL;
#else
	killproc(proc1);
#endif
	ifp->raw = rawsave;
	resume(ifp->rxproc);
#ifdef JOB_CONTROL
	freesession(sp, 1); /* does keywait(NULL, 1); beforehand */
#endif
	return 0;
}

/* Task to handle network -> serial port traffic */
static void
lterm_rx(dev,n1,n2)
int dev;
void *n1,*n2;
{
	int c;
	char c1;
	FILE *network = (FILE *)n1;

	while ((c = fgetc(network)) != EOF){
		c1 = c;
		putchar(c1);
		asci_write(dev,(uint8 *)&c1,1);
		Asci[dev]->iface->lastsent = secclock();
	}
}

