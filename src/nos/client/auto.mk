# auto.mk by Nick for Hytech NOS/UZI project

# -----------------------------------------------------------------------------

SUBDIRS=

OUTPUTS=	client.$(LIBEXT)

client_$(LIBEXT)_SOURCES= \
		bootp.c finger.c ftpcli.c telnet.c
#lterm.c nntpcli.c popcli.c smtpcli.c tip.c

ftpcli_c_MODULES= \
		Notsess Ftpcmds doftp doverbose dobatch doupdate dohash \
		doquit doftpcd domkdir dormdir dobinary doascii dotype doget \
		doread domget dolist dols domd5 docompare domcompare compsub \
		getsub doput domput putsub sendport getresp getline
#ftpcli_keychar

telnet_c_MODULES= \
		Refuse_echo Tn_cr_mode Topt T_options dotopt dotelnet \
		tel_connect tnrecv tel_output doecho doeol willopt wontopt \
		doopt dontopt answer
#telnet_keychar

# -----------------------------------------------------------------------------

