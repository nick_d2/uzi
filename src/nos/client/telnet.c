/* Internet Telnet client
 * Copyright 1991 Phil Karn, KA9Q
 *
 * Apr 04       RPB     Po-ed most of the remaining strings
 */
#include <stdio.h>
#include <sgtty.h> /* Nick */
#ifdef	__TURBOC__
#include <io.h>
#include <fcntl.h>
#endif
#include "nos/global.h"
#include "main/mbuf.h"
#include "socket/socket.h"
#include "client/telnet.h"
#ifdef JOB_CONTROL
#include "kernel/session.h"
#endif
/*#include "driver/devtty.h"*/
#include "main/commands.h"
#include "internet/internet.h"
#include "internet/netuser.h"
#include "main/cmdparse.h"
#include "kernel/thread.h"
#include "kernel/process.h"
#include "main/main.h"
#include <libintl.h>
#include "po/messages.h"

#define T_OPTIONS_MAX 7

#ifdef MODULE
#define STATIC
extern int Refuse_echo;
extern int Tn_cr_mode;    /* if true turn <cr> to <cr-nul> */
extern int Topt;
extern _char *T_options[T_OPTIONS_MAX];
#else
#define STATIC static
#define MODULE_Refuse_echo
#define MODULE_Tn_cr_mode
#define MODULE_Topt
#define MODULE_T_options
#define MODULE_dotopt
#define MODULE_dotelnet
#define MODULE_tel_connect
#define MODULE_tnrecv
#define MODULE_tel_output
#define MODULE_doecho
#define MODULE_doeol
#define MODULE_willopt
#define MODULE_wontopt
#define MODULE_doopt
#define MODULE_dontopt
#define MODULE_answer
#define MODULE_telnet_keychar
#endif

#ifdef MODULE_Refuse_echo
STATIC int Refuse_echo = 0;
#endif
#ifdef MODULE_Tn_cr_mode
STATIC int Tn_cr_mode = 0;    /* if true turn <cr> to <cr-nul> */
#endif
#ifdef MODULE_Topt
STATIC int Topt = 0;
#endif

/*RPB*/
#ifdef MODULE_T_options
STATIC _char *T_options[T_OPTIONS_MAX] = {
	N_("Transmit Binary"),
	N_("Echo"),
	N_(""),
	N_("Suppress Go Ahead"),
	N_(""),
	N_("Status"),
	N_("Timing Mark")
};
#endif
/*RPB*/

#ifdef JOB_CONTROL
STATIC int tel_connect(struct session_s *sp,struct sockaddr *fsocket,int len);
#else
STATIC int tel_connect(FILE *network,struct sockaddr *fsocket,int len);
#endif
STATIC void tel_output(int unused,void *p1,void *p2);
STATIC void tnrecv(struct telnet *tn);
STATIC void doopt(struct telnet *tn,int opt);
STATIC void dontopt(struct telnet *tn,int opt);
STATIC void willopt(struct telnet *tn,int opt);
STATIC void wontopt(struct telnet *tn,int opt);
STATIC void answer(struct telnet *tn,int r1,int r2);
STATIC int telnet_keychar(int c);

#ifdef MODULE_dotopt
int
dotopt(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	setbool(&Topt, _("Telnet option tracing"), argc, argv);
	return 0;
}
#endif

/* Execute user telnet command */

#ifdef MODULE_dotelnet
int
dotelnet(argc,argv,p)
int argc;
char *argv[];
void *p;
{
#ifdef JOB_CONTROL
	struct session_s *sp;
#else
	FILE *network;
#endif
	struct sockaddr_in fsocket;
	int s;

#ifdef JOB_CONTROL
	/* Allocate a session descriptor */
	if((sp = newsession(Cmdline,TELNET,1)) == NULL){
		_printf(_("Too many sessions\n"));
		return 1;
	}
	sp->inproc = telnet_keychar;	/* Intercept ^C */
#endif
	fsocket.sin_family = AF_INET;
	if(argc < 3)
		fsocket.sin_port = IPPORT_TELNET;
	else
		fsocket.sin_port = atoi(argv[2]);

	if(SETSIG(EABORT)){
#ifdef JOB_CONTROL
		freesession(sp, 1); /* does keywait(NULL, 1); beforehand */
#endif
		return 1;
	}
	_printf(_("Resolving %s...\n"),argv[1]);
	if((fsocket.sin_addr.s_addr = resolve(argv[1])) == 0){
		_printf(Badhost,argv[1]);
#ifdef JOB_CONTROL
		freesession(sp, 1); /* does keywait(NULL, 1); beforehand */
#endif
		return 1;
	}
	if((s = socket(AF_INET,SOCK_STREAM,0)) == -1){
		_printf(_("Can't create socket\n"));
#ifdef JOB_CONTROL
		freesession(sp, 1); /* does keywait(NULL, 1); beforehand */
#endif
		return 1;
	}
	settos(s,LOW_DELAY);
#ifdef JOB_CONTROL
	sp->network = fdopen(s,"r+t");
	setvbuf(sp->network,NULL,_IOLBF,BUFSIZ);
	return tel_connect(sp,(struct sockaddr *)&fsocket,SOCKSIZE);
#else
	network = fdopen(s,"r+t");
	setvbuf(network,NULL,_IOLBF,BUFSIZ);
	return tel_connect(network,(struct sockaddr *)&fsocket,SOCKSIZE);
#endif
}
#endif

/* Generic interactive connect routine, used by Telnet, AX.25, NET/ROM */
#ifdef MODULE_tel_connect
STATIC int
tel_connect(network/*sp*/,fsocket,len)
FILE *network/*struct session_s *sp*/;
struct sockaddr *fsocket;
int len;
{
	struct telnet tn;

	memset(&tn,0,sizeof(tn));
	tn.eolmode = Tn_cr_mode;
#ifdef JOB_CONTROL
	tn.session = sp;	/* Upward pointer */
	sp->cb.telnet = &tn;	/* Downward pointer */
#endif
	tn.network = network; /* Nick */

	_printf(_("Trying %s...\n"),psocket(fsocket));
	if(connect(fileno(network),fsocket,len) == -1){
		_perror(_("connect failed"));
#ifdef JOB_CONTROL
		freesession(sp, 1); /* does keywait(NULL, 1); beforehand */
#endif
		return 1;
	}
	_printf(_("Connected\n"));
#ifdef JOB_CONTROL
	sp->inproc = NULL;	/* No longer respond to ^C */
#endif
	tnrecv(&tn);
	return 0;
}
#endif

/* Telnet input routine, common to both telnet and ttylink */

#ifdef MODULE_tnrecv
STATIC void
tnrecv(tn)
struct telnet *tn;
{
	int c;
#ifdef JOB_CONTROL
	struct session_s *sp;
#endif
	char *cp;
	FILE *network;
#if 1 /* Nick */
	struct sgttyb state;

	gtty(STDIN_FILENO, &state);
#endif

#ifdef JOB_CONTROL
	sp = tn->session;
	/*network = sp->network;*/
#endif
	network = tn->network; /* Nick */

	/* Fork off the transmit process */
	tn->proc1 = process_thread_create(daemon_process_p, "tel_out",1024,tel_output,0,tn,NULL,0);

	/* Process input on the connection */
	while((c = getc(network)) != EOF){
		if(c != IAC){
			/* Ordinary character */
			putchar((char)c);
#ifdef JOB_CONTROL
			if(sp->record != NULL)
				putc(c,sp->record);
#endif
			continue;
		}
		/* IAC received, get command sequence */
		c = getc(network);
		switch(c){
		case WILL:
			c = getc(network);
			willopt(tn,c);
			break;
		case WONT:
			c = getc(network);
			wontopt(tn,c);
			break;
		case DO:
			c = getc(network);
			doopt(tn,c);
			break;
		case DONT:
			c = getc(network);
			dontopt(tn,c);
			break;
		case IAC:	/* Escaped IAC */
			putchar(IAC);
#ifdef JOB_CONTROL
			if(sp->record != NULL)
				putc(IAC,sp->record);
#endif
			break;
		}
	}
quit:	/* A close was received from the remote host.
	 * Notify the user, kill the output task and wait for a response
	 * from the user before freeing the session.
	 */
#ifndef ZILOG /* haven't got fmode() with CRT stdio */
	fmode(stdout,STREAM_ASCII); /* Restore newline translation */
#endif
	setvbuf(stdout,NULL,_IOLBF,BUFSIZ);
	cp = sockerr(fileno(network));
	_printf(_("Closed: %s\n"), cp != NULL ? cp : "EOF");
	killproc(tn->proc1);
	tn->proc1 = NULL;
#ifdef JOB_CONTROL
	fclose(sp->network);
	sp->network = NULL;
#else
	fclose(network);
#endif
#if 1 /* Nick */
	stty(STDIN_FILENO, &state);
#endif
#ifdef JOB_CONTROL
	freesession(sp, 1); /* does keywait(NULL, 1); beforehand */
#endif
}
#endif

/* User telnet output task, started by user telnet command */

#ifdef MODULE_tel_output
STATIC void
tel_output(unused,tn1,p)
int unused;
void *tn1;
void *p;
{
#ifdef JOB_CONTROL
	struct session_s *sp;
#endif
	int c;
	struct telnet *tn;

	tn = (struct telnet *)tn1;
#ifdef JOB_CONTROL
	sp = tn->session;
#endif

	/* Send whatever's typed on the terminal */
	while((c = getc(stdin)) != EOF){
		putc(c,tn->network);
#ifdef JOB_CONTROL
		if(!tn->remote[TN_ECHO] && sp->record != NULL)
			putc(c,sp->record);
#endif

		/* By default, output is transparent in remote echo mode.
		 * If eolmode is set, turn a cr into cr-null.
		 * This can only happen when in remote echo (raw) mode, since
		 * the tty driver normally maps \r to \n in cooked mode.
		 */
		if(c == '\r' && tn->eolmode)
			putc('\0',tn->network);

		if(tn->remote[TN_ECHO])
			fflush(tn->network);
	}

	/* Make sure our parent doesn't try to kill us after we exit */
	tn->proc1 = NULL;
}
#endif

#ifdef MODULE_doecho
int
doecho(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	if(argc < 2){
		if(Refuse_echo)
			_printf(_("Refuse\n"));
		else
			_printf(_("Accept\n"));
	} else {
		if(argv[1][0] == 'r')
			Refuse_echo = 1;
		else if(argv[1][0] == 'a')
			Refuse_echo = 0;
		else
			return -1;
	}
	return 0;
}
#endif

/* set for unix end of line for remote echo mode telnet */

#ifdef MODULE_doeol
int
doeol(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	if(argc < 2){
		if(Tn_cr_mode)
			_printf(_("null\n"));
		else
			_printf(_("standard\n"));
	} else {
		if(argv[1][0] == 'n')
			Tn_cr_mode = 1;
		else if(argv[1][0] == 's')
			Tn_cr_mode = 0;
		else {
			_printf(_("Usage: %s [standard|null]\n"),argv[0]);
			return -1;
		}
	}
	return 0;
}
#endif

/* The guts of the actual Telnet protocol: negotiating options */

#ifdef MODULE_willopt
STATIC void
willopt(tn,opt)
struct telnet *tn;
int opt;
{
	int ack;
#if 1 /* Nick */
	struct sgttyb state;
#endif

	if(Topt){
		_printf(_("recv: will "));
		if(opt <= NOPTIONS)
		/*RPB*/
		{
			_printf(T_options[opt]);
			_printf(_("\n"));
		}
		/*RPB*/
		else
			_printf(_("%u\n"),opt);
	}
	switch(opt){
	case TN_TRANSMIT_BINARY:
	case TN_ECHO:
	case TN_SUPPRESS_GA:
		if(tn->remote[opt] == 1)
			return;		/* Already set, ignore to prevent loop */
		if(opt == TN_ECHO){
			if(Refuse_echo){
				/* User doesn't want to accept */
				ack = DONT;
				break;
			} else {
				/* Put tty into raw mode */
#if 1 /* Nick */
				gtty(STDIN_FILENO, &state);
				state.sg_flags |= RAW;
				state.sg_flags &= ~ECHO;
				stty(STDIN_FILENO, &state);
#else
				tn->session->ttystate.edit = 0;
				tn->session->ttystate.echo = 0;
#endif
#ifndef ZILOG /* haven't got fmode() with CRT stdio */
				fmode(tn->/*session->*/network,STREAM_BINARY);
#endif
				setvbuf(tn->/*session->*/network,NULL,_IONBF,0);
#ifndef ZILOG /* haven't got fmode() with CRT stdio */
				fmode(stdout,STREAM_BINARY);
#endif
				setvbuf(stdout,NULL,_IONBF,0);
			}
		}
		tn->remote[opt] = 1;
		ack = DO;
		break;
	default:
		ack = DONT;	/* We don't know what he's offering; refuse */
	}
	answer(tn,ack,opt);
}
#endif

#ifdef MODULE_wontopt
STATIC void
wontopt(tn,opt)
struct telnet *tn;
int opt;
{
#if 1 /* Nick */
	struct sgttyb state;
#endif

	if(Topt){
		_printf(_("recv: wont "));
		if(opt <= NOPTIONS)
		/*RPB*/
		{
			_printf(T_options[opt]);
			_printf(_("\n"));
		}
		/*RPB*/
		else
			_printf(_("%u\n"),opt);
	}
	if(opt <= NOPTIONS){
		if(tn->remote[opt] == 0)
			return;		/* Already clear, ignore to prevent loop */
		tn->remote[opt] = 0;
		if(opt == TN_ECHO){
			/* Put tty into cooked mode */
#if 1 /* Nick */
			gtty(STDIN_FILENO, &state);
			state.sg_flags &= ~RAW;
			state.sg_flags |= ECHO;
			stty(STDIN_FILENO, &state);
#else
			tn->session->ttystate.edit = 1;
			tn->session->ttystate.echo = 1;
#endif
#ifndef ZILOG /* haven't got fmode() with CRT stdio */
			fmode(tn->/*session->*/network,STREAM_ASCII);
#endif
			setvbuf(tn->/*session->*/network,NULL,_IOLBF,BUFSIZ);
#ifndef ZILOG /* haven't got fmode() with CRT stdio */
			fmode(stdout,STREAM_ASCII);
#endif
			setvbuf(stdout,NULL,_IOLBF,BUFSIZ);
		}
	}
	answer(tn,DONT,opt);	/* Must always accept */
}
#endif

#ifdef MODULE_doopt
STATIC void
doopt(tn,opt)
struct telnet *tn;
int opt;
{
	int ack;

	if(Topt){
		_printf(_("recv: do "));
		if(opt <= NOPTIONS)
		/*RPB*/
		{
			_printf(T_options[opt]);
			_printf(_("\n"));
		}
		/*RPB*/
		else
			_printf(_("%u\n"),opt);
	}
	switch(opt){
	case TN_SUPPRESS_GA:
		if(tn->local[opt] == 1)
			return;		/* Already set, ignore to prevent loop */
		tn->local[opt] = 1;
		ack = WILL;
		break;
	default:
		ack = WONT;	/* Don't know what it is */
	}
	answer(tn,ack,opt);
}
#endif

#ifdef MODULE_dontopt
STATIC void
dontopt(tn,opt)
struct telnet *tn;
int opt;
{
	if(Topt){
		_printf(_("recv: dont "));
		if(opt <= NOPTIONS)
		/*RPB*/
		{
			_printf(T_options[opt]);
			_printf(_("\n"));
		}
		/*RPB*/
		else
			_printf(_("%u\n"),opt);
	}
	if(opt <= NOPTIONS){
		if(tn->local[opt] == 0){
			/* Already clear, ignore to prevent loop */
			return;
		}
		tn->local[opt] = 0;
	}
	answer(tn,WONT,opt);
}
#endif

#ifdef MODULE_answer
STATIC void
answer(tn,r1,r2)
struct telnet *tn;
int r1,r2;
{
	if(Topt){
		switch(r1){
		case WILL:
			_printf(_("sent: will "));
			break;
		case WONT:
			_printf(_("sent: wont "));
			break;
		case DO:
			_printf(_("sent: do "));
			break;
		case DONT:
			_printf(_("sent: dont "));
			break;
		}
		if(r2 <= NOPTIONS)
		/*RPB*/
		{
			_printf(T_options[r2]);
			_printf(_("\n"));
		}
		/*RPB*/
		else
			_printf(_("%u\n"),r2);
	}
	_fprintf(tn->/*session->*/network, _("%c%c%c"),IAC,r1,r2);
	fflush(tn->/*session->*/network);
}
#endif

#if 0 /*def MODULE_telnet_keychar*/
STATIC int
telnet_keychar(c)
int c;
{
	if(c != CTLC)
		return 1;	/* Ignore all but ^C */

#if 1
	_printf(_("^C\n"));
	fflush(stdout);
	alert(my_thread_p, EABORT);
#else
	_fprintf(my_session_p->output, _("^C\n"));
	alert(my_session_p->proc,EABORT);
#endif
	return 0;
}
#endif

