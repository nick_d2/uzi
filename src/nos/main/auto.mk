# auto.mk by Nick for Hytech NOS/UZI project

# -----------------------------------------------------------------------------

SUBDIRS=

OUTPUTS=	main.$(LIBEXT) kernel.$(BINEXT)

main_$(LIBEXT)_SOURCES= \
		config.c misc.c parm.c version.c ahdlc.c alloc.c audit.c \
		cmdparse.c crc.c devparam.c files.c ftpsubr.c getopt.c \
		iface.c mbuf.c md5c.c pathname.c timer.c free_m.S main.c
#vfprintf.c view.c wildmat.c

kernel_$(BINEXT)_SOURCES= \
		crt0_kernel.S main.$(LIBEXT) client.$(LIBEXT) \
		driver.$(LIBEXT) dump.$(LIBEXT) filesys.$(LIBEXT) \
		internet.$(LIBEXT) kernel.$(LIBEXT) \
		ppp.$(LIBEXT) server.$(LIBEXT) socket.$(LIBEXT) \
		$(ARCH).$(LIBEXT) iar.$(LIBEXT) intl.$(LIBEXT) \
		messages.$(LIBEXT) crt_$(MODEL).$(LIBEXT) \
		sys_$(MODEL).$(LIBEXT)
#ax25.$(LIBEXT) netrom.$(LIBEXT)
# note: sys.$(LIBEXT) must come last, due to NOS overriding close() etc
# note: for the moment, sys.$(LIBEXT) must come after filesys.(LIBEXT)

misc_c_MODULES=	Badhost Hostname Nospace Cmdline daemon_process_p \
		main_exit Prompt Logfp StartTime Verbose main_init net_prompt \
		doexec dospawn doexit dohostname dolog dohelp \
		doattach doparam dosysdebug dosystrace donothing logmsg \
		Rempass Whitespace doshutdown smsg htoi htob \
		readhex rip memcnt memxor strdupw _strdupw
#driver_process_p
#strnicmp strtok isdelim put32 put16 get16 get32 ilog2
#dodelete dorename doremote dorepeat dopage mon_lengths dodate

iface_c_MODULES= \
		Hopper Ifaces Loopback Encap Ifcmds if_tx network \
		net_route nu_send nu_output doifconfig ifipaddr iflinkadr \
		ifbroad ifnetmsk ifencap setencap ifrxbuf ifmtu ifforw \
		showiface dodetach if_detach iftxqlen if_lookup ismyaddr \
		mask2width if_name bitbucket dodialer
#Noipaddr

mbuf_c_MODULES=	Pushdowns Pushalloc Allocmbufs Freembufs Cachehits Msizes \
		Mbufcache alloc_mbuf ambufw free_mbuf free_p len_p \
		len_q trim_mbuf dup_p copy_p pullup extract append pushdown \
		qdata pull32 pull16 pull8 mbuf_crunch mbufstat mbufsizes \
		mbuf_garbage
#free_m
#free_q enqueue dequeue write_p

md5c_c_MODULES=	PADDING MD5Init MD5Update MD5Final MD5Transform \
		MD5Transform_Round1 MD5Transform_Round2 MD5Transform_Round3 \
		MD5Transform_Round4 Encode Decode

# -----------------------------------------------------------------------------

