/* A collection of stuff heavily dependent on the configuration info
 * in config.h. The idea is that configuration-dependent tables should
 * be located here to avoid having to pepper lots of .c files with #ifdefs,
 * requiring them to include config.h and be recompiled each time config.h
 * is modified.
 *
 * Copyright 1991 Phil Karn, KA9Q
 */

#include <errno.h>
#include <stdio.h>
#include "nos/global.h"
#include "main/config.h"
#include "main/mbuf.h"
#include "main/iface.h"
#include "internet/ip.h"
#ifdef IPSEC
#include "nos/ipsec.h"
#include "nos/photuris.h"
#endif
#include "internet/tcp.h"
#include "internet/udp.h"
#include "client/smtp.h"
#ifdef ARCNET
#include "nos/arcnet.h"
#endif
/*#include "ax25/lapb.h"*/
/*#include "ax25/ax25.h"*/
/*#include "x86/enet.h"*/
/*#include "ax25/kiss.h"*/
/*#include "netrom/nr4.h"*/
/*#include "netrom/nrs.h"*/
/*#include "netrom/netrom.h"*/
/*#include "x86/pktdrvr.h"*/
#include "ppp/ppp.h"
#include "ppp/slip.h"
#include "internet/arp.h"
#include "internet/icmp.h"
#include "nos/hardware.h"
#include "socket/usock.h"
#include "main/cmdparse.h"
#include "main/commands.h"
/*#include "server/mailbox.h"*/
/*#include "ax25/ax25mail.h"*/
/*#include "netrom/nr4mail.h"*/
/*#include "server/tipmail.h"*/
#include "server/telnetd.h"
#include "main/daemon.h"
#include "client/bootp.h"
#include "z80/asci.h"
#include "z80/abus.h"
#include "dump/trace.h"
#include "kernel/session.h"
#ifdef QTSO
#include "nos/qtso.h"
#endif
#ifdef CDMA_DM
#include "nos/dm.h"
#include "nos/rlp.h"
#endif
#ifdef DMLITE
#include "nos/dmlite.h"
#include "nos/rlp.h"
#endif
#ifdef SPPP
#include "ppp/sppp.h"
#endif
#include "ppp/dialer.h"
#ifdef KSP
#include "nos/ksp.h"
#endif
#ifdef SOUND
#include "x86/sb.h"
#endif
#include "filesys/cinode.h" /* for inotab_garbage() */
#include "filesys/openfile.h" /* for of_garbage() */
#include "socket/opensock.h" /* for os_garbage() */
#include "kernel/thread.h" /* "kernel/process.h", "kernel/arena.h", "filesys/filesys.h", "uzi/devio.h" */
#include "driver/devtty.h" /* for tty_minor_garbage() */
#include "driver/devapi.h" /* for api_minor_garbage() */
#include "driver/bufpool.h" /* for bufdump() */
#include <libintl.h>
#include "po/messages.h"

int dotest(int argc,char *argv[],void *p);	/**/
static int dostart(int argc,char *argv[],void *p);
static int dostop(int argc,char *argv[],void *p);

#ifdef AX25
static void axip(struct iface_s *iface,struct ax25_cb *axp,uint8 *src,
	uint8 *dest,struct mbuf_s **bp,int mcast);
static void axarp(struct iface_s *iface,struct ax25_cb *axp,uint8 *src,
	uint8 *dest,struct mbuf_s **bp,int mcast);
static void axnr(struct iface_s *iface,struct ax25_cb *axp,uint8 *src,
	uint8 *dest,struct mbuf_s **bp,int mcast);
#endif /* AX25 */

#if 0 /* remove anext member from mbufs and replace enqueue(), dequeue() */
struct mbuf_s *Hopper;			/* Queue of incoming packets */
#endif
#if 0 /* not used anymore, see uzi/opensock.h OPEN_SOCK_LIST) */
unsigned Nsock = DEFNSOCK;		/* Number of socket entries */
#endif

#if 0 /* now using MTHRESH directly */
/* Free memory threshold, below which things start to happen to conserve
 * memory, like garbage collection, source quenching and refusing connects
 */
unsigned int Memthresh = MTHRESH;
#endif

#if 0 /* not used anymore, see uzi/opensock.h OPEN_SOCK_LIST) */
unsigned Nfiles = DEFNFILES;
#endif

#if 0 /*def SCROLLBACK*/ /* Nick */
long Sfsize = SCROLLBACK; /* Default size of session scrollback file */
#endif

/* Command lookup and branch tables */
struct cmds Cmds[CMDS_MAX] = {
	/* The "go" command must be first */
	N_("abusstat"),	doabusstat,	0, 0, NULL,
	N_("ascistat"),	doascistat,	0, 0, NULL,
	N_("attach"),	doattach,	0, 2,
			N_("attach <hardware> <hw specific options>"),
	N_("bufdump"),	bufdump,	0, 0, NULL,
	N_("detach"),	dodetach,	0, 2, N_("detach <interface>"),
/* NOTE!  Nick has added a stacksize here, so that the dial command is run */
/* as a separate process, eliminating an annoying pause on bootup as the */
/* dialer initscript is run.  If this causes problems we'll change it back! */
	N_("dialer"),	dodialer,	512 /*0*/, 2,
		 	N_("dialer <iface> <timeout> [device-dependent args]"),
	N_("domain"),	dodomain,	0, 0, NULL,
	N_("echo"),	doecho,		0, 0, NULL,
	N_("eol"),	doeol,		0, 0, NULL,
	N_("esccstat"),	doesccstat,	0, 0, NULL,
	N_("exec"),	doexec,		0, 0, NULL,
	N_("exit"),	doexit,		0, 0, NULL,
	N_("ftp"),	doftp,		1024, /*768,*/ /*2048,*/ 2, N_("ftp <address>"),
	N_("help"),	dohelp,		0, 0, NULL,
	N_("hop"),	dohop,		0, 0, NULL,
	N_("hostname"),	dohostname,	0, 0, NULL,
	N_("icmp"),	doicmp,		0, 0, NULL,
	N_("ifconfig"),	doifconfig,	0, 0, NULL,
	N_("ip"),	doip,		0, 0, NULL,
	N_("log"),	dolog,		0, 0, NULL,
	N_("memory"),	domem,		0, 0, NULL,
	N_("param"),	doparam,	0, 2, N_("param <interface>"),
	N_("ping"),	doping,		512, 2,
	N_("ping <hostid> [<length> [<interval> [incflag]]]"),
	N_("ppp"),	doppp_commands,	0, 0, NULL,
	N_("ps"),	ps,		0, 0, NULL,
	N_("route"),	doroute,	0, 0, NULL,
#ifdef JOB_CONTROL
	N_("session"),	dosession,	0, 0, NULL,
#endif
	N_("shutdown"),	doshutdown,	0, 0, NULL,
	N_("start"),	dostart,	0, 2, N_("start <servername>"),
	N_("stop"),	dostop,		0, 2, N_("stop <servername>"),
	N_("sysdebug"),	dosysdebug,	0, 1, N_("sysdebug [on|off]"),
	N_("systrace"),	dosystrace,	0, 1, N_("systrace [on|off]"),
	N_("tcp"),		dotcp,		0, 0, NULL,
	N_("telnet"),	dotelnet,	512, /*1024,*/ 2, N_("telnet <address>"),
	N_("topt"),	dotopt,		0, 0, NULL,
	N_("trace"),	dotrace,	512, 0, NULL,
	N_("udp"),	doudp,		0, 0, NULL,
	N_("?"),	dohelp,		0, 0, NULL,
	NULL,		dospawn,	512, 0, NULL,
};

/* List of supported hardware devices */
struct cmds Attab[ATTAB_MAX] = {
	N_("escc"), escc_attach, 0, 7,
			N_("attach escc <devices> init <addr> <spacing> <Aoff> <Boff> <Dataoff>\n"
			"   <intack> <vec> [p]<clock> [hdwe] [param]\n"
			"attach escc <chan> slip|ppp <label> <mtu> <speed> <bufsize> <[r][c][v]|0> [call]"),
	NULL,
};

#ifdef SERVERS
/* "start" and "stop" subcommands */

#define STARTCMDS_MAX 3
static struct cmds Startcmds[STARTCMDS_MAX] = {
#if defined(AX25) && defined(MAILBOX)
	N_("ax25"),		ax25start,	0 /*256*/, 0, NULL,
#endif
#ifdef SMISC /* Nick */
	N_("bsr"),		bsr1,		0 /*256*/, 2, N_("start bsr <interface> [<port>]"),
	N_("discard"),		dis1,		0 /*256*/, 0, NULL,
	N_("echo"),		echo1,		0 /*256*/, 0, NULL,
#endif
#ifdef QFAX
	N_("fax"),		fax1,		0 /*256*/, 0, NULL,
#endif
#if 0 /* temporarily out to save space */
	N_("finger"),		finstart,	0 /*256*/, 0, NULL,
#endif
	N_("ftp"),		ftpstart,	0 /*256*/, 0, NULL,
#if defined(NETROM) && defined(MAILBOX)
	N_("netrom"),		nr4start,	0 /*256*/, 0, NULL,
#endif
#ifdef POP
	N_("pop"),		pop1,		0 /*256*/, 0, NULL,
#endif
#ifdef RIP
	N_("rip"),		doripinit,	0,   0, NULL,
#endif
#ifdef SMTP
	N_("smtp"),		smtp1,		0 /*256*/, 0, NULL,
#endif
#ifdef TELNETD /* Nick def	MAILBOX */
	N_("telnet"),		telnet1,	0 /*256*/, 0, NULL,
#endif
#ifdef MAILBOX
	N_("tip"),		tipstart,	0 /*256*/, 2, N_("start tip <interface>"),
#endif
#ifdef SMISC /* Nick */
	N_("term"),		term1,		0 /*256*/, 0, NULL,
	N_("ttylink"),		ttylstart,	0 /*256*/, 0, NULL,
	N_("remote"),		rem1,		0 /*768*/, 0, NULL,
#endif
	NULL,
};

#define STOPCMDS_MAX 3
static struct cmds Stopcmds[STOPCMDS_MAX] = {
#if defined(AX25) && defined(MAILBOX)
	N_("ax25"),		ax250,		0, 0, NULL,
#endif
#ifdef SMISC /* Nick */
	N_("bsr"),		bsr0,		0, 0, NULL,
	N_("discard"),		dis0,		0, 0, NULL,
	N_("echo"),		echo0,		0, 0, NULL,
#endif
#ifdef QFAX
	N_("fax"),		fax0,		0, 0, NULL,
#endif
#if 0 /* temporarily out to save space */
	N_("finger"),		fin0,		0, 0, NULL,
#endif
	N_("ftp"),		ftp0,		0, 0, NULL,
#if defined(NETROM) && defined(MAILBOX)
	N_("netrom"),		nr40,		0, 0, NULL,
#endif
#ifdef POP
	N_("pop"),		pop0,		0, 0, NULL,
#endif
#ifdef RIP
	N_("rip"),		doripstop,	0, 0, NULL,
#endif
#ifdef SMTP
	N_("smtp"),		smtp0,		0, 0, NULL,
#endif
#ifdef TELNETD /* Nick def	MAILBOX */
	N_("telnet"),		telnet0,	0, 0, NULL,
#endif
#ifdef MAILBOX
	N_("tip"),		tip0,		0, 2, N_("stop tip <interface>"),
#endif
#ifdef SMISC /* Nick */
	N_("term"),		term0,		0, 0, NULL,
	N_("ttylink"),		ttyl0,		0, 0, NULL,
	N_("remote"),		rem0,		0, 0, NULL,
#endif
	NULL,
};
#endif /* SERVERS */

/* Socket-protocol interface table */
struct socklink Socklink[SOCKLINK_MAX] = {
	/* type,
	 * socket,	bind,		listen,		connect,
	 * accept,	recv,		send,		qlen,
	 * kick,	shut,		close,		check,
	 * error,	state,		status,		eol_seq
	 */
	{
	TYPE_TCP,
	so_tcp,		NULL,		so_tcp_listen,	so_tcp_conn,
	TRUE,		so_tcp_recv,	so_tcp_send,	so_tcp_qlen,
	so_tcp_kick,	so_tcp_shut,	so_tcp_close,	checkipaddr,
	Tcpreasons,	tcpstate,	so_tcp_stat,	Inet_eol
	},

	{
	TYPE_UDP,
	so_udp,		so_udp_bind,	NULL,		so_udp_conn,
	FALSE,		so_udp_recv,	so_udp_send,	so_udp_qlen,
	NULL,		NULL,		so_udp_close,	checkipaddr,
	NULL,		NULL,		so_udp_stat,	Inet_eol
	},

#ifdef AX25
	{
	TYPE_AX25I,
	so_ax_sock,	NULL,		so_ax_listen,	so_ax_conn,
	TRUE,		so_ax_recv,	so_ax_send,	so_ax_qlen,
	so_ax_kick,	so_ax_shut,	so_ax_close,	checkaxaddr,
	Axreasons,	axstate,	so_ax_stat,	Ax25_eol
	},

	{
	TYPE_AX25UI,
	so_axui_sock,	so_axui_bind,	NULL,		so_axui_conn,
	FALSE,		so_axui_recv,	so_axui_send,	so_axui_qlen,
	NULL,		NULL,		so_axui_close,	checkaxaddr,
	NULL,		NULL,		NULL,		Ax25_eol
	},
#endif /* AX25 */

	{
	TYPE_RAW,
	so_ip_sock,	NULL,		NULL,		so_ip_conn,
	FALSE,		so_ip_recv,	so_ip_send,	so_ip_qlen,
	NULL,		NULL,		so_ip_close,	checkipaddr,
	NULL,		NULL,		NULL,		Inet_eol
	},

#ifdef NETROM
	{
	TYPE_NETROML3,
	so_n3_sock,	NULL,		NULL,		so_n3_conn,
	FALSE,		so_n3_recv,	so_n3_send,	so_n3_qlen,
	NULL,		NULL,		so_n3_close,	checknraddr,
	NULL,		NULL,		NULL,		Ax25_eol
	},

	{
	TYPE_NETROML4,
	so_n4_sock,	NULL,		so_n4_listen,	so_n4_conn,
	TRUE,		so_n4_recv,	so_n4_send,	so_n4_qlen,
	so_n4_kick,	so_n4_shut,	so_n4_close,	checknraddr,
	Nr4reasons,	nrstate,	so_n4_stat,	Ax25_eol
	},
#endif /* NETROM */

#ifdef LOCSOCK
	{
	TYPE_LOCAL_STREAM,
	so_los,		NULL,		NULL,		NULL,
	TRUE,		so_los_recv,	so_los_send,	so_los_qlen,
	NULL,		so_los_shut,	so_los_close,	NULL,
	NULL,		NULL,		so_loc_stat,	Eol
	},

	{
	TYPE_LOCAL_DGRAM,
	so_lod,		NULL,		NULL,		NULL,
	FALSE,		so_lod_recv,	so_lod_send,	so_lod_qlen,
	NULL,		so_lod_shut,	so_lod_close,	NULL,
	NULL,		NULL,		so_loc_stat,	Eol
	},
#endif

	{
	-1
	}
};

/* Table of functions for printing socket addresses */
char * (*Psock[NAF]) () = {
	ippsocket,
#ifdef AX25
	axpsocket,
#else
	NULL,
#endif
#ifdef NETROM
	nrpsocket,
#else
	NULL,
#endif
#ifdef LOCSOCK
	lopsocket,
#else
	NULL,
#endif
};

/* TCP port numbers to be considered "interactive" by the IP routing
 * code and given priority in queueing
 */
int Tcp_interact[TCP_INTERACT_MAX] = {
	IPPORT_FTP,	/* FTP control (not data!) */
	IPPORT_TELNET,	/* Telnet */
#if 1 /* Nick */
	IPPORT_TTYLINK,	/* TTYlink */
#else
	6000,		/* X server 0 */
	IPPORT_LOGIN,	/* BSD rlogin */
	IPPORT_MTP,	/* Secondary telnet */
#endif
	-1
};
#if 0 /* doesn't seem to be referenced */
int (*Kicklist[])() = {
	kick,
#ifdef SMTP
	smtptick,
#endif
	NULL
};
#endif

/* Transport protocols atop IP */
struct iplink Iplink[IPLINK_MAX] = {
	TCP_PTCL,	"TCP",	tcp_input,	tcp_dump,
	UDP_PTCL,	"UDP",	udp_input,	udp_dump,
	ICMP_PTCL,	"ICMP",	icmp_input,	icmp_dump,
#ifdef IPIP /* Nick */
	IP_PTCL,	"IP",	ipip_recv,	ipip_dump,
	IP4_PTCL,	"IP",	ipip_recv,	ipip_dump,
#endif
#ifdef IPSEC
	ESP_PTCL,	"ESP",	esp_input,	esp_dump,
	AH_PTCL,	"AH",	ah_input,	ah_dump,
#endif
	0,		NULL,	NULL,		NULL
};

/* Transport protocols atop ICMP */
struct icmplink Icmplink[ICMPLINK_MAX] = {
	TCP_PTCL,	tcp_icmp,
#ifdef IPSEC
	ESP_PTCL,	esp_icmp,
/*	AH_PTCL,	ah_icmp, */
#endif
	0,		0
};

#ifdef AX25
/* Linkage to network protocols atop ax25 */
struct axlink Axlink[] = {
	PID_IP,		axip,
	PID_ARP,	axarp,
#ifdef NETROM
	PID_NETROM,	axnr,
#endif
	PID_NO_L3,	axnl3,
	0,		NULL,
};
#endif /* AX25 */

#ifdef ARP /* Nick */
/* ARP protocol linkages, indexed by arp's hardware type */
struct arp_type Arp_type[ARP_TYPE_MAX /*NHWTYPES*/] = {
#ifdef NETROM
	AXALEN, 0, 0, 0, NULL, pax25, setcall,	/* ARP_NETROM */
#else
	0, 0, 0, 0, NULL,NULL,NULL,
#endif

#ifdef ETHER
	EADDR_LEN,IP_TYPE,ARP_TYPE,1,Ether_bdcst,pether,gether, /* ARP_ETHER */
#else
	0, 0, 0, 0, NULL,NULL,NULL,
#endif

	0, 0, 0, 0, NULL,NULL,NULL,			/* ARP_EETHER */

#ifdef AX25
	AXALEN, PID_IP, PID_ARP, 10, Ax25multi[0], pax25, setcall,
#else
	0, 0, 0, 0, NULL,NULL,NULL,			/* ARP_AX25 */
#endif

	0, 0, 0, 0, NULL,NULL,NULL,			/* ARP_PRONET */

	0, 0, 0, 0, NULL,NULL,NULL,			/* ARP_CHAOS */

	0, 0, 0, 0, NULL,NULL,NULL,			/* ARP_IEEE802 */

#ifdef ARCNET
	AADDR_LEN, ARC_IP, ARC_ARP, 1, ARC_bdcst, parc, garc, /* ARP_ARCNET */
#else
	0, 0, 0, 0, NULL,NULL,NULL,
#endif

	0, 0, 0, 0, NULL,NULL,NULL,			/* ARP_APPLETALK */
};
#endif

/* Get rid of trace references in Iftypes[] if TRACE is turned off */
#ifndef TRACE
#define	ip_dump		NULL
#define	ax25_dump	NULL
#define	ki_dump		NULL
#define	sl_dump		NULL
#define	ether_dump	NULL
#define	ppp_dump	NULL
#define	arc_dump	NULL
#endif /* TRACE */

/* Table of interface types. Contains most device- and encapsulation-
 * dependent info
 */
struct iftype Iftypes[IFTYPES_MAX] = {
	/* This entry must be first, since Loopback refers to it */
	"None",		nu_send,	nu_output,	NULL,
	NULL,		CL_NONE,	0,		ip_proc,
	NULL,		ip_dump,	NULL,		NULL,

#ifdef AX25
	"AX25UI",	axui_send,	ax_output,	pax25,
	setcall,	CL_AX25,	AXALEN,		ax_recv,
	ax_forus,	ax25_dump,	NULL,		NULL,

	"AX25I",	axi_send,	ax_output,	pax25,
	setcall,	CL_AX25,	AXALEN,		ax_recv,
	ax_forus,	ax25_dump,	NULL,		NULL,
#endif /* AX25 */

#ifdef KISS
	"KISSUI",	axui_send,	ax_output,	pax25,
	setcall,	CL_AX25,	AXALEN,		kiss_recv,
	ki_forus,	ki_dump,	NULL,		NULL,

	"KISSI",	axi_send,	ax_output,	pax25,
	setcall,	CL_AX25,	AXALEN,		kiss_recv,
	ki_forus,	ki_dump,	NULL,		NULL,
#endif /* KISS */

#ifdef SLIP
	"SLIP",		slip_send,	NULL,		NULL,
	NULL,		CL_NONE,	0,		ip_proc,
	NULL,		ip_dump,
#ifdef DIALER
					sd_init,	sd_stat,
#else
					NULL,		NULL,
#endif
#endif /* SLIP */

#ifdef VJCOMPRESS
	"VJSLIP",	vjslip_send,	NULL,		NULL,
	NULL,		CL_NONE,	0,		ip_proc,
	NULL,		sl_dump,
#ifdef DIALER
					sd_init,	sd_stat,
#else
					NULL,		NULL,
#endif
#endif /* VJCOMPRESS */

#ifdef ETHER
	/* Note: NULL is specified for the scan function even though
	 * gether() exists because the packet drivers don't support
	 * address setting.
	 */
	"Ethernet",	enet_send,	enet_output,	pether,
	NULL,		CL_ETHERNET,	EADDR_LEN,	eproc,
	ether_forus,	ether_dump,	NULL,		NULL,
#endif /* ETHER */

#ifdef NETROM
	"NETROM",	nr_send,	NULL,		pax25,
	setcall,	CL_NETROM,	AXALEN,		NULL,
	NULL,		NULL,	NULL,		NULL,
#endif /* NETROM */

#ifdef SLFP
	"SLFP",		pk_send,	NULL,		NULL,
	NULL,		CL_NONE,	0,		ip_proc,
	NULL,		ip_dump,	NULL,		NULL,
#endif /* SLFP */

#ifdef PPP
	"PPP",		ppp_send,	ppp_output,	NULL,
	NULL,		CL_PPP,		0,		ppp_proc,
	NULL,		ppp_dump,
#ifdef DIALER
					sd_init,	sd_stat,
#else
					NULL,		NULL,
#endif
#endif /* PPP */

#ifdef SPPP
	"sppp",		sppp_send,	NULL,		NULL,
	NULL,		CL_NONE,	0,		ip_proc,
	NULL,		ip_dump,	NULL,		NULL,
#endif /* SPPP */

#ifdef ARCNET
	"Arcnet",	anet_send,	anet_output,	parc,
	garc,		CL_ARCNET,	1,		aproc,
	arc_forus,	arc_dump,	NULL,		NULL,
#endif /* ARCNET */

#ifdef QTSO
	"QTSO",		qtso_send,	NULL,		NULL,
	NULL,		CL_NONE,	0,		ip_proc,
	NULL,		NULL,	NULL,		NULL,
#endif /* QTSO */

#ifdef CDMA_DM
	"CDMA",		rlp_send,	NULL,		NULL,
	NULL,		CL_NONE,	0,		ip_proc,
	NULL,		ip_dump,	dd_init,	dd_stat,
#endif

#ifdef DMLITE
	"DMLITE",	rlp_send,	NULL,		NULL,
	NULL,		CL_NONE,	0,		ip_proc,
	NULL,		ip_dump,	dl_init,	dl_stat,
#endif

	NULL,	NULL,		NULL,		NULL,
	NULL,		-1,		0,		NULL,
	NULL,		NULL,	NULL,		NULL,
};

/* Asynchronous interface mode table */
#ifdef ASY
struct asci_mode_s Asci_mode[ASCI_MODE_MAX] = {
#ifdef SLIP
	"SLIP",		FR_END,		slip_init,	slip_free,
	"VJSLIP",	FR_END,		slip_init,	slip_free,
#endif
#ifdef KISS
	"AX25UI",	FR_END,		kiss_init,	kiss_free,
	"AX25I",	FR_END,		kiss_init,	kiss_free,
	"KISSUI",	FR_END,		kiss_init,	kiss_free,
	"KISSI",	FR_END,		kiss_init,	kiss_free,
#endif
#ifdef NRS
	"NRS",		ETX,		nrs_init,	nrs_free,
#endif
#ifdef PPP
	"PPP",		HDLC_FLAG,	ppp_init,	ppp_free,
#endif
#ifdef SPPP
	"SPPP",		HDLC_FLAG,	sppp_init,	sppp_free,
#endif
#ifdef QTSO
	"QTSO",		HDLC_FLAG,	qtso_init,	qtso_free,
#endif
#ifdef DMLITE
	"DMLITE",	HDLC_FLAG,	dml_init,	dml_stop,
#endif
	NULL
};
#endif /* ASY */

/* daemons to be run at startup time */
struct daemon Daemons[DAEMONS_MAX] = {
	"gcol",		768, /*512,*/ /*256,*/	gcollect,
	"netw",		768, /*512,*/ /*1024,*/ /*1536,*/	network,
	"rnd ini",	768, /*650,*/	rand_init,
#ifdef PHOTURIS
	"keygen",	2048,	gendh,
	"key mgmt",	2048,	phot_proc,
#endif
	NULL,	0,	NULL
};

#if 0 /* Functions to be called on each clock tick */
void (*Cfunc[])() = {
#ifndef ZILOG /* Nick... not needed as RTC is in the touchscreen */
	pctick,	/* Call PC-specific stuff to keep time */
#endif
#if 0 /* Nick... it's too hard to get this working properly */
	sesflush,	/* Flush current session output */
#endif
#if 0 /* Nick.. ideally this should not be needed def ASY */
	asci_timer,
#endif
#if defined(ESCC) && defined(AX25)
	escc_timer,
#endif
	NULL /*,*/
};
#endif

/* Entry points for garbage collection */
void (*Gcollect[GCOLLECT_MAX])() = {
	tcp_garbage,
	ip_garbage,
	udp_garbage,
	/* st_garbage, */
#ifdef AX25
	lapb_garbage,
#endif
#ifdef NETROM
	nr_garbage,
#endif
	bufpool_garbage,
	inotab_garbage,
	of_garbage,
	os_garbage,
	thread_garbage,
	process_garbage,
#ifdef JOB_CONTROL
	session_garbage,
#endif
	arena_garbage,
	tty_minor_garbage,
	api_minor_garbage,
	mbuf_garbage, /* must be last, in case earlier callers mbuf_crunch() */
	NULL
};

/* Functions to be called at shutdown */
void (*Shutdown[SHUTDOWN_MAX])() = {
#if 0 /* Nick def ASY */
	fp_stop,
#endif
#ifdef ESCC
	escc_exit,
#endif
#ifdef SOUND
	sbshut,
#endif
#ifndef ZILOG /* Nick, it's now done afterwards, by the iostop() routine */
	uchtimer,	/* Unlink timer handler from timer chain */
#endif
	NULL,
};

#ifdef MAILBOX
void (*Listusers)(FILE *network) = listusers;
#else
#if 0 /* doesn't seem to be referenced */
void (*Listusers)(FILE *network) = NULL;
#endif
#endif /* MAILBOX */

#ifndef BOOTP
int WantBootp = 0;

int
bootp_validPacket(ip,bp)
struct ip *ip;
struct mbuf_s *bp;
{
	return 0;
}
#endif /* BOOTP */

/* Packet tracing stuff */
#ifdef TRACE
#include "dump/trace.h"
#include <libintl.h>
#include "po/messages.h"

#else	/* TRACE */

/* Stub for packet dump function */
void
dump(iface,direction,type,bp)
struct iface_s *iface;
int direction;
unsigned type;
struct mbuf_s *bp;
{
}
void
raw_dump(iface,direction,bp)
struct iface_s *iface;
int direction;
struct mbuf_s *bp;
{
}
#endif /* TRACE */

#ifndef LZW
#if 0 /* doesn't seem to be referenced */
void
lzwfree(up)
struct usock *up;
{
}
#endif
#endif

#ifdef AX25
/* Hooks for passing incoming AX.25 data frames to network protocols */
static void
axip(
struct iface_s *iface,
struct ax25_cb *axp,
uint8 *src,
uint8 *dest,
struct mbuf_s **bpp,
int mcast
){
	(void)ip_route(iface,bpp,mcast);
}

static void
axarp(
struct iface_s *iface,
struct ax25_cb *axp,
uint8 *src,
uint8 *dest,
struct mbuf_s **bpp,
int mcast
){
	(void)arp_input(iface,bpp);
}

#ifdef NETROM
static void
axnr(
struct iface_s *iface,
struct ax25_cb *axp,
uint8 *src,
uint8 *dest,
struct mbuf_s **bpp,
int mcast
){
	if(!mcast)
		nr_route(bpp,axp);
	else
		nr_nodercv(iface,src,bpp);
}

#endif /* NETROM */
#endif /* AX25 */

#if 0 /* Nick, it conflicts with internet/rip.c, formerly ndef	RIP */
/* Stub for routing timeout when RIP is not configured -- just remove entry */
void
rt_timeout(s)
void *s;
{
	struct route *stale = (struct route *)s;

	rt_drop(stale->target,stale->bits);
}
#endif

/* Stubs for Van Jacobsen header compression */
#if !defined(VJCOMPRESS) && defined(ASY)
struct slcompress *
slhc_init(rslots,tslots)
int rslots;
int tslots;
{
	return NULLSLCOMPR;
}
int
slhc_compress(comp, bpp, compress_cid)
struct slcompress *comp;
struct mbuf_s **bpp;
int compress_cid;
{
	return SL_TYPE_IP;
}
int
slhc_uncompress(comp, bpp)
struct slcompress *comp;
struct mbuf_s **bpp;
{
	return -1;	/* Can't decompress */
}
void
shlc_i_status(comp)
struct slcompress *comp;
{
}
void
shlc_o_status(comp)
struct slcompress *comp;
{
}
int
slhc_remember(comp, bpp)
struct slcompress *comp;
struct mbuf_s **bpp;
{
	return -1;
}
#endif /* !defined(VJCOMPRESS) && defined(ASY) */

#ifdef SERVERS
static int
dostart(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return subcmd(Startcmds,argc,argv,p);
}
static int
dostop(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return subcmd(Stopcmds,argc,argv,p);
}
#endif /* SERVERS */

#if 0 /* doesn't seem to be referenced */
dotest(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	int32 stime,ftime;
	char *bufa,*bufb;
	int i;

	bufa = malloc(1024);
	bufb = malloc(1024);
	stime = msclock();
	for(i=0;i<1000;i++)
		memcpy(bufa,bufb,1024);
	ftime = msclock();
	_printf(_("Time for 1000 1024-byte copies: %lu ms\n"),ftime - stime);
	return 0;
}
#endif

