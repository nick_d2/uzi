/* mbuf (message buffer) primitives
 * Copyright 1991 Phil Karn, KA9Q
 *
 * Apr 04	RPB	Po-ed most of the remaining strings
 */
#include <stdio.h>
/* Nick #include <dos.h> */	/* TEMP */
#include "asm.h" /* for SMALL_MBUF, MED_MBUF, LARGE_MBUF */
#include "nos/global.h"
#include "main/mbuf.h"
#include "kernel/dprintf.h"
#include "kernel/thread.h"
#include "kernel/usrmem.h"
#include <libintl.h>
#include "po/messages.h"

#ifdef MODULE
#define STATIC
extern int32 Allocmbufs;
extern int32 Freembufs;
extern int32 Cachehits;
extern unsigned long Msizes[16];
extern struct mbuf_s *Mbufcache[4];
#else
#define STATIC static
#define MODULE_Pushdowns
#define MODULE_Pushalloc
#define MODULE_Allocmbufs
#define MODULE_Freembufs
#define MODULE_Cachehits
#define MODULE_Msizes
#define MODULE_Mbufcache
#define MODULE_alloc_mbuf
#define MODULE_ambufw
#define MODULE_free_mbuf
#define MODULE_free_m
#define MODULE_free_p
/*#define MODULE_free_q*/
#define MODULE_len_p
#define MODULE_len_q
#define MODULE_trim_mbuf
#define MODULE_dup_p
#define MODULE_copy_p
#define MODULE_pullup
#define MODULE_extract
#define MODULE_append
#define MODULE_pushdown
/*#define MODULE_enqueue*/
/*#define MODULE_dequeue*/
#define MODULE_qdata
#define MODULE_pull32
#define MODULE_pull16
#define MODULE_pull8
/*#define MODULE_write_p*/
#define MODULE_mbuf_crunch
#define MODULE_mbufstat
#define MODULE_mbufsizes
#define MODULE_mbuf_garbage
#endif

#ifdef MODULE_Pushdowns
/* Nick STATIC */ int32 Pushdowns; /* Total calls to pushdown() */
#endif
#ifdef MODULE_Pushalloc
/* Nick STATIC */ int32 Pushalloc; /* Calls to pushalloc() that call malloc */
#endif
#ifdef MODULE_Allocmbufs
STATIC int32 Allocmbufs;	/* Calls to alloc_mbuf() */
#endif
#ifdef MODULE_Freembufs
STATIC int32 Freembufs;		/* Calls to free_mbuf() that actually free */
#endif
#ifdef MODULE_Cachehits
STATIC int32 Cachehits;		/* Hits on free mbuf cache */
#endif
#ifdef MODULE_Msizes
STATIC unsigned long Msizes[16];
#endif

#ifdef MODULE_Mbufcache
STATIC struct mbuf_s *Mbufcache[4];
#endif

/* Allocate mbuf with associated buffer of 'size' bytes */
#ifdef MODULE_alloc_mbuf
struct mbuf_s *
alloc_mbuf(uint16 size)
{
	struct mbuf_s *bp;
	unsigned int i;
	int i_state;

#if DEBUG >= 5
 _dprintf(5, _("\n%04x:%04x alloc_mbuf(0x%04x) "),
	 *(((unsigned int *)&size) + 2), *(((unsigned int *)&size) + 3),
	 (unsigned int)size);
#endif
	Allocmbufs++;

	/* Record the size of this request */
	if (size)
		{
		Msizes[ilog2(size)]++;
		}

	if(size <= SMALL_MBUF){
		i = 0;
		size = SMALL_MBUF;
	} else if(size <= MED_MBUF){
		i = 1;
		size = MED_MBUF;
	} else if(size <= LARGE_MBUF){
		i = 2;
		size = LARGE_MBUF;
	} else
		i = 3;

	i_state = dirps();
#if 1 /* blow away mbufs of size > LARGE_MBUF that were freed by free_m() */
	while((bp = Mbufcache[3]) != NULL){
 if (bp->object.type != 'D')
  {
  abyte('?');
  while (1)
   ;
  }
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
		Mbufcache[3] = bp->next;
#else
		Mbufcache[3] = bp->anext;
#endif
 bp->object.type = 0;
		restore(i_state);
		free(bp);
		disable();
	}
	/* leaves bp = NULL; */
#else
	bp = NULL;
#endif
	if(i < 3 && (bp = Mbufcache[i]) != NULL){
 if (bp->object.type != 'D')
  {
  abyte('?');
  while (1)
   ;
  }
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
		Mbufcache[i] = bp->next;
#else
		Mbufcache[i] = bp->anext;
#endif
		Cachehits++;
#if DEBUG >= 5
 _dprintf(5, _("= cache 0x%04x "), (unsigned int)bp);
#endif
	}
	restore(i_state);
	if(bp == NULL)
		bp = (struct mbuf_s *)malloc(size + sizeof(struct mbuf_s));
	if(bp == NULL)
		return NULL;

	/* Clear just the header portion */
	memset(bp,0,sizeof(struct mbuf_s));
 bp->object.type = 'N';
	if((bp->size = size) != 0)
		bp->data = (uint8 *)(bp + 1);
#if 0 /* don't do dup, replace it with a copy */
	bp->refcnt++;
#endif
	return bp;
}
#endif

/* Allocate mbuf, waiting if memory is unavailable */
#ifdef MODULE_ambufw
extern int Memwait;			/* Count of tasks waiting for memory */

struct mbuf_s *
ambufw(uint16 size)
{
	struct mbuf_s *bp;
	unsigned int i;
	int i_state;

#if DEBUG >= 5
 _dprintf(5, _("\n%04x:%04x ambufw(0x%04x) "),
	 *(((unsigned int *)&size) + 2), *(((unsigned int *)&size) + 3),
	 (unsigned int)size);
#endif
	Allocmbufs++;

	/* Record the size of this request */
	if (size)
		{
		Msizes[ilog2(size)]++;
		}

	if(size <= SMALL_MBUF){
		i = 0;
		size = SMALL_MBUF;
	} else if(size <= MED_MBUF){
		i = 1;
		size = MED_MBUF;
	} else if(size <= LARGE_MBUF){
		i = 2;
		size = LARGE_MBUF;
	} else
		i = 3;

	i_state = dirps();
	while ((bp = Mbufcache[3]) != NULL){
 if (bp->object.type != 'D')
  {
  abyte('?');
  while (1)
   ;
  }
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
		Mbufcache[3] = bp->next;
#else
		Mbufcache[3] = bp->anext;
#endif
 bp->object.type = 0;
		restore(i_state);
		free(bp);
		disable();
	}
	/* leaves bp = NULL; */
	if (i < 3 && (bp = Mbufcache[i]) != NULL){
 if (bp->object.type != 'D')
  {
  abyte('?');
  while (1)
   ;
  }
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
		Mbufcache[i] = bp->next;
#else
		Mbufcache[i] = bp->anext;
#endif
		Cachehits++;
#if DEBUG >= 5
 _dprintf(5, _("= cache 0x%04x "), (unsigned int)bp);
#endif
	}
	restore(i_state);
	if(bp == NULL)
 {
#if 1 /* expand out mallocw() subroutine inline, change failure diagnostic */
		while((bp = malloc(size + sizeof(struct mbuf_s))) == NULL){
			Memwait++;
			/* can't be po'd since _dprintf() re-enters malloc() */
			dprintf(0, "\n%02x:%04x ambufw(0x%04x) ",
					((unsigned char *)&size)[8],
					((unsigned int *)&size)[5], size);
			kwait(&Memwait);
			Memwait--;
		}
#else
		bp = (struct mbuf_s *)mallocw(size + sizeof(struct mbuf_s));
#endif
 }

	/* Clear just the header portion */
	memset(bp,0,sizeof(struct mbuf_s));
 bp->object.type = 'N';
	if((bp->size = size) != 0)
		bp->data = (uint8 *)(bp + 1);
#if 0 /* don't do dup, replace it with a copy */
	bp->refcnt++;
#endif
	return bp;
}
#endif

/* Decrement the reference pointer in an mbuf. If it goes to zero,
 * free all resources associated with mbuf.
 * Return pointer to next mbuf in packet chain
 */
#ifdef MODULE_free_mbuf
struct mbuf_s *
free_mbuf(struct mbuf_s **bpp)
{
	struct mbuf_s *bpnext;
	struct mbuf_s *bp;
	int i_state;

#if DEBUG >= 5
 _dprintf(5, _("\n%04x:%04x free_mbuf(0x%04x) "),
	 *(((unsigned int *)&bpp) + 2), *(((unsigned int *)&bpp) + 3),
	 (unsigned int)bpp);
#endif
	if (bpp == NULL)
		{
		return NULL;
		}

	bp = *bpp;
	*bpp = NULL;

	i_state = dirps();
	bpnext = free_m(bp);

	/* blow away mbufs of size > LARGE_MBUF that were freed by free_m() */
	while ((bp = Mbufcache[3]) != NULL)
		{
 if (bp->object.type != 'D')
  {
  abyte('?');
  while (1)
   ;
  }
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
		Mbufcache[3] = bp->next;
#else
		Mbufcache[3] = bp->anext;
#endif
 bp->object.type = 0;
		restore(i_state);
		free(bp);
		disable();
		}
	restore(i_state);

	return bpnext;
}
#endif

/* Same as free_mbuf(), but this one must be called with interrupts disabled.
 * Also doesn't call free() for size > LARGE_MBUF, uses Mbufcache[3] instead.
 * Argument is not indirect, not checked for validity, so bp must be non-NULL.
 */
#ifdef MODULE_free_m
struct mbuf_s *
free_m(struct mbuf_s *bp)
	{
	struct mbuf_s *bpnext;
#if 0 /* don't do dup, replace it with a copy */
	struct mbuf_s *bptmp;
#endif
	unsigned int i;

	if (bp == NULL)
		{
		return NULL;
		}
 if (bp->object.type != 'N')
  {
  abyte('~');
  while (1)
   ;
  }
	bpnext = bp->next;

#if 0 /* don't do dup, replace it with a copy */
	if(bp->dup != NULL){
		bptmp = bp->dup;
		bp->dup = NULL;	/* Nail it before we recurse */
		free_mbuf(&bptmp);	/* Follow indirection */
	}
	/* Decrement reference count. If it has gone to zero, free it. */
	if(--bp->refcnt <= 0){
#endif
	i = bp->size;
	if (i <= SMALL_MBUF)
		{
		i = 0;
		}
	else if (i <= MED_MBUF)
		{
		i = 1;
		}
	else if (i <= LARGE_MBUF)
		{
		i = 2;
		}
	else
		{
		i = 3;
		}

#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	bp->next = Mbufcache[i];
#else
	bp->anext = Mbufcache[i];
#endif
	Mbufcache[i] = bp;
 bp->object.type = 'D';

	Freembufs++;
#if 0 /* don't do dup, replace it with a copy */
	}
#endif

	return bpnext;
	}
#endif

/* Free packet (a chain of mbufs). Return pointer to next packet on queue,
 * if any
 */
#ifdef MODULE_free_p
struct mbuf_s *
free_p(struct mbuf_s **bpp)
{
	struct mbuf_s *bp;
#if 0 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	register struct mbuf_s *abp;
#endif

	if(bpp == NULL || (bp = *bpp) == NULL)
		return NULL;
#if 0 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	abp = bp->anext;
#endif
	while(bp != NULL)
		bp = free_mbuf(&bp);
	*bpp = NULL;
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	return NULL;
#else
	return abp;
#endif
}
#endif

/* Free entire queue of packets (of mbufs) */
#if 0 /*def MODULE_free_q*/
void
free_q(struct mbuf_s **q)
{
	struct mbuf_s *bp;

	while((bp = dequeue(q)) != NULL)
		free_p(&bp);
}
#endif

/* Count up the total number of bytes in a packet */
#ifdef MODULE_len_p
uint16
len_p(struct mbuf_s *bp)
{
	register uint16 cnt = 0;

	while(bp != NULL){
		cnt += bp->cnt;
		bp = bp->next;
	}
	return cnt;
}
#endif

/* Count up the number of packets in a queue */
#ifdef MODULE_len_q
uint16
len_q(struct mbuf_s *bp)
{
	register uint16 cnt;

#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	for(cnt=0;bp != NULL;cnt++,bp = bp->next)
#else
	for(cnt=0;bp != NULL;cnt++,bp = bp->anext)
#endif
		;
	return cnt;
}
#endif

/* Trim mbuf to specified length by lopping off end */
#ifdef MODULE_trim_mbuf
void
trim_mbuf(struct mbuf_s **bpp,uint16 length)
{
	register uint16 tot = 0;
	register struct mbuf_s *bp;

	if(bpp == NULL || *bpp == NULL)
		return;	/* Nothing to trim */

	if(length == 0){
		/* Toss the whole thing */
		free_p(bpp);
		return;
	}
	/* Find the point at which to trim. If length is greater than
	 * the packet, we'll just fall through without doing anything
	 */
	for( bp = *bpp; bp != NULL; bp = bp->next){
		if(tot + bp->cnt < length){
			tot += bp->cnt;
		} else {
			/* Cut here */
			bp->cnt = length - tot;
			free_p(&bp->next);
			bp->next = NULL;
			break;
		}
	}
}
#endif

/* Duplicate/enqueue/dequeue operations based on mbufs */

/* Duplicate first 'cnt' bytes of packet starting at 'offset'.
 * This is done without copying data; only the headers are duplicated,
 * but without data segments of their own. The pointers are set up to
 * share the data segments of the original copy. The return pointer is
 * passed back through the first argument, and the return value is the
 * number of bytes actually duplicated.
 */
#ifdef MODULE_dup_p
uint16
dup_p(
struct mbuf_s **hp,
/* Nick register */ struct mbuf_s *bp,
/* Nick register */ uint16 offset,
/* Nick register */ uint16 cnt
){
	struct mbuf_s *cp;
#if 0 /* don't do dup, replace it with a copy */
	uint16 tot;
#endif

#if DEBUG >= 5
 _dprintf(5, _("\n%04x:%04x dup_p(0x%04x) "),
	 *(((unsigned int *)&hp) + 2), *(((unsigned int *)&hp) + 3),
	 (unsigned int)bp);
#endif
	/* is this really necessary? */
	if(cnt == 0 || bp == NULL /*|| hp == NULL*/){
		/*if(hp != NULL)
			*hp = NULL;*/
		return 0;
	}

#if 1 /* don't do dup, replace it with a copy */
	cp = alloc_mbuf(cnt);
	if (cp == NULL)
		{
		return 0;
		}

	cnt = extract(bp, offset, cp->data, cnt);
	cp->cnt = cnt;

	*hp = cp;
	return cnt;
#else
	if((*hp = cp = alloc_mbuf(0)) == NULL){
		return 0;
	}
	/* Skip over leading mbufs that are smaller than the offset */
	while(bp != NULL && bp->cnt <= offset){
		offset -= bp->cnt;
		bp = bp->next;
	}
	if(bp == NULL){
		free_mbuf(&cp);
		*hp = NULL;
		return 0;	/* Offset was too big */
	}
	tot = 0;
	for(;;){
		/* Make sure we get the original, "real" buffer (i.e. handle the
		 * case of duping a dupe)
		 */
		if(bp->dup != NULL)
			cp->dup = bp->dup;
		else
			cp->dup = bp;

		/* Increment the duplicated buffer's reference count */
		cp->dup->refcnt++;

		cp->data = bp->data + offset;
		cp->cnt = min(cnt,bp->cnt - offset);
		offset = 0;
		cnt -= cp->cnt;
		tot += cp->cnt;
		bp = bp->next;
		if(cnt == 0 || bp == NULL || (cp->next = alloc_mbuf(0)) == NULL)
			break;
		cp = cp->next;
	}
	return tot;
#endif
}
#endif

/* Copy first 'cnt' bytes of packet into a new, single mbuf */
#ifdef MODULE_copy_p
struct mbuf_s *
copy_p(
/* Nick register */ struct mbuf_s *bp,
/* Nick register */ uint16 cnt
){
	register struct mbuf_s *cp;
	register uint8 *wp;
	register uint16 n;

	if(bp == NULL || cnt == 0 || (cp = alloc_mbuf(cnt)) == NULL)
		return NULL;
	wp = cp->data;
	while(cnt != 0 && bp != NULL){
		n = min(cnt,bp->cnt);
		memcpy(wp,bp->data,n);
		wp += n;
		cp->cnt += n;
		cnt -= n;
		bp = bp->next;
	}
	return cp;
}
#endif

/* Copy and delete "cnt" bytes from beginning of packet. Return number of
 * bytes actually pulled off
 */
#ifdef MODULE_pullup
uint16
pullup(
struct mbuf_s **bph,
void *buf,
uint16 cnt
){
	struct mbuf_s *bp;
	uint16 n,tot;
	uint8 *obp = buf;

	tot = 0;
	if(bph == NULL)
		return 0;
	while(cnt != 0 && (bp = *bph) != NULL){
		n = min(cnt,bp->cnt);
		if(obp != NULL){
#if 1
			usrput(obp, bp->data, n);
#else
			if(n == 1){	/* Common case optimization */
				*obp++ = *bp->data;
			} else if(n > 1){
				memcpy(obp,bp->data,n);
				obp += n;
			}
#endif
		}
		tot += n;
		cnt -= n;
		bp->data += n;
		bp->cnt -= n;
		if(bp->cnt == 0){
#if 0 /* Nick */
			/* If this is the last mbuf of a packet but there
			 * are others on the queue, return a pointer to
			 * the next on the queue. This allows pullups to
			 * to work on a packet queue
			 */
/* NOTE!  BUGGY, "anext" MUST BE GOT FROM FIRST MBUF OF PACKET NOT LAST??? */
			if(bp->next == NULL && bp->anext != NULL){
				*bph = bp->anext;
				free_mbuf(&bp);
			} else
#endif
				*bph = free_mbuf(&bp);
		}
	}
	return tot;
}
#endif

/* Copy data from within mbuf to user-provided buffer, starting at
 * 'offset' bytes from start of mbuf and copying no more than 'len'
 * bytes. Return actual number of bytes copied
 */
#ifdef MODULE_extract
uint16
extract(
struct mbuf_s *bp,
uint16 offset,
void *buf,
uint16 len
){
	uint8 *obp = buf;
	uint16 copied = 0;
	uint16 n;

	/* Skip over offset if greater than first mbuf(s) */
	while(bp != NULL && offset >= bp->cnt){
		offset -= bp->cnt;
		bp = bp->next;
	}
	while(bp != NULL && len != 0){
		n = min(len,bp->cnt - offset);	/* offset must be < bp->cnt */
#if 1
		usrput(obp,bp->data+offset,n);
#else
		memcpy(obp,bp->data+offset,n);
#endif
		copied += n;
		obp += n;
		len -= n;
		if(n + offset == bp->cnt)
			bp = bp->next;	/* Buffer exhausted, get next */
		offset = 0;		/* No more offset after first */
	}
	return copied;
}
#endif

/* Append mbuf to end of mbuf chain */
#ifdef MODULE_append
void
append(
struct mbuf_s **bph,
struct mbuf_s **bpp
){
	register struct mbuf_s *p;
#if 0 /* extra synchronization in case we're feeding an interrupt outq */
	int i_state;
#endif

	if(bph == NULL || bpp == NULL || *bpp == NULL)
		return;

#if 0 /* extra synchronization in case we're feeding an interrupt outq */
	i_state = dirps();
#endif
	if(*bph == NULL){
		/* First one on chain */
		*bph = *bpp;
	} else {
		for(p = *bph ; p->next != NULL ; p = p->next)
			;
		p->next = *bpp;
	}
#if 0 /* extra synchronization in case we're feeding an interrupt outq */
	restore(i_state);
#endif
	*bpp = NULL;	/* We've consumed it */
}
#endif

/* Insert specified amount of contiguous new space at the beginning of an
 * mbuf chain. If enough space is available in the first mbuf, no new space
 * is allocated. Otherwise a mbuf of the appropriate size is allocated and
 * tacked on the front of the chain.
 *
 * This operation is the logical inverse of pullup(), hence the name.
 */
#ifdef MODULE_pushdown
void
pushdown(struct mbuf_s **bpp,void *buf,uint16 size)
{
	struct mbuf_s *bp;

	Pushdowns++;
	if(bpp == NULL)
		return;
	/* Check that bp is real, that it hasn't been duplicated, and
	 * that it itself isn't a duplicate before checking to see if
	 * there's enough space at its front.
	 */
#if 1 /* don't do dup, replace it with a copy */
	if((bp = *bpp) != NULL && bp->data - (uint8 *)(bp+1) >= size){
#else
	if((bp = *bpp) != NULL && bp->refcnt == 1 && bp->dup == NULL
	 && bp->data - (uint8 *)(bp+1) >= size){
#endif
		/* No need to alloc new mbuf, just adjust this one */
		bp->data -= size;
		bp->cnt += size;
	} else {
		(*bpp) = ambufw(size);
		(*bpp)->next = bp;
		bp = *bpp;
		bp->cnt = size;
		Pushalloc++;
	}
	if(buf != NULL)
#if 1
		usrget(bp->data,buf,size);
#else
		memcpy(bp->data,buf,size);
#endif
}
#endif

/* Append packet to end of packet queue */
#if 0 /*def MODULE_enqueue*/
void
enqueue(
struct mbuf_s **q,
struct mbuf_s **bpp
){
	register struct mbuf_s *p;
	uint8 i_state;

	if(q == NULL || bpp == NULL || *bpp == NULL)
		return;
#if 0 /* slower, but much more efficient when many packets are on queue */
	mbuf_crunch(bpp);
#endif
	i_state = dirps();
	if(*q == NULL){
		/* List is empty, stick at front */
		*q = *bpp;
	} else {
		for(p = *q ; p->anext != NULL ; p = p->anext)
			;
		p->anext = *bpp;
	}
	*bpp = NULL;	/* We've consumed it */
	restore(i_state);
	ksignal(q,1);
}
#endif

/* Unlink a packet from the head of the queue */
#if 0 /*def MODULE_dequeue*/
struct mbuf_s *
dequeue(struct mbuf_s **q)
{
	register struct mbuf_s *bp;
	uint8 i_state;

	if(q == NULL)
		return NULL;
	i_state = dirps();
	if((bp = *q) != NULL){
		*q = bp->anext;
		bp->anext = NULL;
	}
	restore(i_state);
	return bp;
}
#endif

/* Copy user data into an mbuf */
#ifdef MODULE_qdata
struct mbuf_s *
qdata(void *data,uint16 cnt)
{
	register struct mbuf_s *bp;

	bp = alloc_mbuf(cnt);
	if (bp == NULL)
		{
		return NULL;
		}
#if 1
	usrget(bp->data,data,cnt);
#else
	memcpy(bp->data,data,cnt);
#endif
	bp->cnt = cnt;
	return bp;
}
#endif

/* Pull a 32-bit integer in host order from buffer in network byte order.
 * On error, return 0. Note that this is indistinguishable from a normal
 * return.
 */
#ifdef MODULE_pull32
int32
pull32(struct mbuf_s **bpp)
{
	uint8 buf[4];

	if(pullup(bpp,buf,4) != 4){
		/* Return zero if insufficient buffer */
		return 0;
	}
	return get32(buf);
}
#endif

/* Pull a 16-bit integer in host order from buffer in network byte order.
 * Return -1 on error
 */
#ifdef MODULE_pull16
long
pull16(struct mbuf_s **bpp)
{
	uint8 buf[2];

	if(pullup(bpp,buf,2) != 2){
		return -1;		/* Nothing left */
	}
	return get16(buf);
}
#endif

/* Pull single byte from mbuf */
#ifdef MODULE_pull8
int
pull8(struct mbuf_s **bpp)
{
	uint8 c;

	if(pullup(bpp,&c,1) != 1)
		return -1;		/* Nothing left */
	return c;
}
#endif

#if 0 /*def MODULE_write_p*/
int
write_p(FILE *fp,struct mbuf_s *bp)
{
	while(bp != NULL){
		if(fwrite(bp->data,1,bp->cnt,fp) != bp->cnt)
			return -1;
		bp = bp->next;
	}
	return 0;
}
#endif

/* Reclaim unused space in a mbuf chain. If the argument is a chain of mbufs
 * and/or it appears to have wasted space, copy it to a single new mbuf and
 * free the old mbuf(s). But refuse to move mbufs that merely
 * reference other mbufs, or that have other headers referencing them.
 *
 * Be extremely careful that there aren't any other pointers to
 * (or into) this mbuf, since we have no way of detecting them here.
 * This function is meant to be called only when free memory is in
 * short supply.
 */
#ifdef MODULE_mbuf_crunch
void
mbuf_crunch(struct mbuf_s **bpp)
{
	struct mbuf_s *bp = *bpp;
	struct mbuf_s *nbp;
#if 1 /* fix for problem introduced by the rounding up of mbuf sizes */
	uint16 size;
#endif

#if 0 /* don't do dup, replace it with a copy */
	if(bp->refcnt > 1 || bp->dup != NULL){
		/* Can't crunch, there are other refs */
		return;
	}
#endif
#if 1 /* fix for problem introduced by the rounding up of mbuf sizes */
	if (bp->next == NULL)
		{
		size = bp->cnt;
		if (size <= SMALL_MBUF)
			{
			size = SMALL_MBUF;
			}
		else if (size <= MED_MBUF)
			{
			size = MED_MBUF;
			}
		else if (size <= LARGE_MBUF)
			{
			size = LARGE_MBUF;
			}
		if (size == bp->size)
			{
			/* Nothing to be gained by crunching */
			return;
			}
		}
#else
	if(bp->next == NULL && bp->cnt == bp->size){
		/* Nothing to be gained by crunching */
		return;
	}
#endif
	if((nbp = copy_p(bp,len_p(bp))) == NULL){
		/* Copy failed due to lack of (contiguous) space */
		return;
	}
#if 0 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	nbp->anext = bp->anext;
#endif
	free_p(&bp);
	*bpp = nbp;
}
#endif

#ifdef MODULE_mbufstat
void
mbufstat(void)
{
#if 1 /* extra synchronization in case interrupts occur while reading */
	int i_state;
	unsigned long freembufs;
	struct mbuf_s *mbufcache[3];

	i_state = dirps();
	freembufs = Freembufs;
	mbufcache[0] = Mbufcache[0];
	mbufcache[1] = Mbufcache[1];
	mbufcache[2] = Mbufcache[2];
	restore(i_state);
#endif

	_printf(_("mbuf allocs %lu free cache hits %lu (%lu%%) mbuf frees %lu\n"),
#if 1 /* extra synchronization in case interrupts occur while reading */
	 Allocmbufs,Cachehits,100*Cachehits/Allocmbufs,freembufs);
#else
	 Allocmbufs,Cachehits,100*Cachehits/Allocmbufs,Freembufs);
#endif
	_printf(_("pushdown calls %lu pushdown calls to alloc_mbuf %lu\n"),
	 Pushdowns,Pushalloc);
	_printf(_("Free cache: small %u medium %u large %u\n"),
#if 1 /* extra synchronization in case interrupts occur while reading */
	 len_q(mbufcache[0]),len_q(mbufcache[1]),len_q(mbufcache[2]));
#else
	 len_q(Mbufcache[0]),len_q(Mbufcache[1]),len_q(Mbufcache[2]));
#endif
}
#endif

#ifdef MODULE_mbufsizes
void
mbufsizes(void)
{
	int i;

	_printf(_("Mbuf sizes:\n"));
	for(i=0;i<4;i++){
		_printf(_("N<=%5u:%7ld| N<=%5u:%7ld| N<=%5u:%7ld| N<=%5u:%7ld\n"),
		 1<<i,Msizes[i],16<<i,Msizes[i+4],
		 256<<i,Msizes[i+8],4096<<i,Msizes[i+12]);
	}
}
#endif

/* Mbuf garbage collection - return all mbufs on free cache to heap */
/* OLD note:  mbufs are not trashed when yellow anymore (except >LARGE_MBUF) */
#ifdef MODULE_mbuf_garbage
void
mbuf_garbage(int red)
{
	int i_state, i;
	struct mbuf_s *bp;

	/* Blow entire cache */
	i_state = dirps();
	for (i = 0 /*(red ? 0 : 3)*/; i < 4; i++){
		while((bp = Mbufcache[i]) != NULL){
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
			Mbufcache[i] = bp->next;
#else
			Mbufcache[i] = bp->anext;
#endif
 bp->object.type = 0;
			restore(i_state);
			free(bp);
			disable();
		}
	}
	restore(i_state);
}
#endif

