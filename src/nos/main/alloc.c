/* memory allocation routines
 * Copyright 1991 Phil Karn, KA9Q
 *
 * Adapted from alloc routine in K&R; memory statistics and interrupt
 * protection added for use with net package. Must be used in place of
 * standard Turbo-C library routines because the latter check for stack/heap
 * collisions. This causes erroneous failures because process stacks are
 * allocated off the heap.
 *
 * Apr 04 RPB     Po-ed most of the remaining strings
 */

#include <stdio.h>
#include "nos/global.h"
#include "driver/bufpool.h" /* for bufsync() */
#include "filesys/cinode.h" /* for i_sync() */
#include "filesys/filesys.h" /* for fsync() */
#include "kernel/dprintf.h"
#include "kernel/thread.h"
#include "main/cmdparse.h"
#include "main/config.h" /* for MTHRESH */
#include "main/logmsg.h"
#include "main/mbuf.h"
#include <libintl.h>
#include "po/messages.h"

unsigned long Memfail;			/* Count of allocation failures */
unsigned long Allocs;			/* Total allocations */
unsigned long Frees;			/* Total frees */
unsigned long Invalid;			/* Total calls to free with bad arg */

unsigned char Memwait;			/* Count of tasks waiting for memory */

static unsigned long YellowAlerts;	/* Yellow alerts sent to IP stack */
static unsigned long RedAlerts;		/* Red alerts sent to IP stack*/
static unsigned long Yellows;		/* Yellow alert garbage collections */
static unsigned long Reds;		/* Red alert garbage collections */

unsigned int Freebytes;			/* Unallocated heap, bytes */
unsigned int Freefrags;			/* Unallocated heap, fragments */
unsigned int Usedbytes;			/* Allocated heap, bytes */
unsigned int Usedfrags;			/* Allocated heap, fragments */
/*unsigned int Morecores;*/			/* Successful calls to sbrk() */

unsigned char bufpool_timeout; /* units of 1/5 second until forced bufsync() */
unsigned char memory_situation;	/* 0 = plenty spare, 1 = yellow, 2 = red */

/* Version of malloc() that waits if necessary for memory to become available */
void *
mallocw(nb)
size_t nb;
{
	register void *p;

#if DEBUG >= 6
	/* note, can't be po'd since _dprintf() would re-enter malloc() */
	dprintf(6, "\n%02x:%04x mallocw(0x%04x) ",
			((unsigned char *)&nb)[4], ((unsigned int *)&nb)[3],
			(unsigned int)nb);
#endif
	while((p = malloc(nb)) == NULL){
		Memwait++;

		/* can't be po'd since _dprintf() would re-enter malloc() */
		dprintf(0, "\n%02x:%04x mallocw(0x%04x) ",
				((unsigned char *)&nb)[8],
				((unsigned int *)&nb)[5], nb);

		kwait(&Memwait);
		Memwait--;
	}
	return p;
}

/* Version of calloc that waits if necessary for memory to become available */
void *
callocw(nelem,size)
unsigned nelem;	/* Number of elements */
unsigned size;	/* Size of each element */
{
	register unsigned int i;
	register char *cp;

#if DEBUG >= 6
	/* note, can't be po'd since _dprintf() would re-enter malloc() */
	dprintf(6, "\n%02x:%04x callocw(0x%04x, 0x%04x) ",
			((unsigned char *)&nelem)[4],
			((unsigned int *)&nelem)[3],
			(unsigned int)nelem, (unsigned int)size);
#endif
	i = nelem * size;
#if 1 /* expand out mallocw() subroutine inline, change failure diagnostic */
	while((cp = malloc(i)) == NULL){
		Memwait++;
		/* can't be po'd since _dprintf() re-enters malloc() */
		dprintf(0, "\n%02x:%04x callocw(0x%04x, 0x%04x) ",
				((unsigned char *)&size)[8],
				((unsigned int *)&size)[5], nelem, size);
		kwait(&Memwait);
		Memwait--;
	}
#else
	cp = mallocw(i);
#endif
	memset(cp,0,i);
	return cp;
}

extern void allock(void);

/* Return 0 if at least MTHRESH memory is available. Return 1 if
 * less than MTHRESH but more than MTHRESH/2 is available; i.e.,
 * if a yellow garbage collection should be performed. Return 2 if
 * less than MTHRESH/2 is available, i.e., a red collection should
 * be performed.
 */
int
availmem()
{
	void *p;

#if 0 /* this isn't adequate, due to severe fragmentation */
	if (Freebytes >= MTHRESH)
		return 0;	/* We're clearly OK */
#endif

	/* There's not enough on the heap; try calling malloc to see if
	 * it can get more from the system
	 */
/* abyte('a'); allock(); */
	p = malloc(MTHRESH);
/* abyte('b'); allock(); */
	if (p) {
/* abyte('c'); allock(); */
		free(p);
/* abyte('d'); allock(); */
		Allocs--;
		Frees--;
/* abyte('-'); */
		return 0;	/* Okay */
	}
	Memfail--;
/* abyte('e'); allock(); */
	p = malloc(MTHRESH / 2);
/* abyte('f'); allock(); */
	if (p) {
/* abyte('g'); allock(); */
		free(p);
/* abyte('h'); allock(); */
		Frees--;
		Allocs--;
 abyte('y');
		YellowAlerts++;
		return 1;	/* Yellow alert */
	}
	Memfail--;
/* abyte('i'); allock(); */
 abyte('r');
	RedAlerts++;
	return 2;		/* Red alert */
}

extern void allock(void);

int
domem(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	unsigned int freebytes, freefrags;
	unsigned int usedbytes, usedfrags;
	unsigned int heapbytes, heapfrags;
	unsigned int freecount, usedcount;
	/*unsigned int morecores;*/
	unsigned long allocs, frees;

 allock();
	freebytes = Freebytes;
	freefrags = Freefrags;
	usedbytes = Usedbytes;
	usedfrags = Usedfrags;
	heapbytes = Freebytes + Usedbytes;
	heapfrags = Freefrags + Usedfrags;
	freecount = Freefrags ? (Freebytes / Freefrags) : 0;
	usedcount = Usedfrags ? (Usedbytes / Usedfrags) : 0;
	/*morecores = Morecores;*/
	allocs = Allocs;
	frees = Frees;
	_printf(_("available %lu%% fragmentation %lu%%" /*" morecores %u"*/ "\n"),
			heapbytes ? (100L * freebytes / heapbytes) : 0,
			freecount ? (100L * usedcount / freecount) : 0 /*,
			morecores*/);
	_printf(_("free bytes %u fragments %u average %u\n"), freebytes,
			freefrags, freecount);
	_printf(_("used bytes %u fragments %u average %u\n"), usedbytes,
			usedfrags, usedcount);
	_printf(_("\n"));
	_printf(_("allocs %lu frees %lu (diff %lu) bad allocs %lu bad frees %lu\n"),
			allocs, frees, allocs - frees, Memfail, Invalid);
	_printf(_("alerts yellow %lu red %lu collections yellow %lu red %lu\n"),
			YellowAlerts, RedAlerts, Yellows, Reds);
	_printf(_("\n"));
	mbufstat();
	_printf(_("\n"));
	mbufsizes();
	return 0;
}

/* Background memory compactor, used when memory runs low */
void
gcollect(i,v1,v2)
int i;	/* Args not used */
void *v1;
void *v2;
{
	void (**fp)(int);
	int red;
#if 0 /*RPB*/
	unsigned int sec = 0;
#endif /*RPB*/

	for(;;){
		ppause(200L);	/* Run 5 times per second */

#if 0
 if (bufpool_timeout)
  {
  abyte('*');
  }
 else
  {
  abyte('-');
  }
#endif
		if (bufpool_timeout && (--bufpool_timeout) == (unsigned char)0)
			{
			i_sync(NULLDEV); /* write back all inodes for all devs */
			fsync(NULLDEV); /* write back superblocks for all devs */
			bufsync(NULLDEV, 0); /* write back all buffers for all devs */
			}

		/* If memory is low, collect some garbage. If memory is VERY
		 * low, invoke the garbage collection routines in "red" mode.
		 */
		memory_situation = availmem();
		switch(memory_situation){
		case 0:
			continue;	/* All is well */
		case 1:
			abyte('Y');
			red = 0;
			Yellows++;
			YellowAlerts--;
			break;
		case 2:
			abyte('R');
			red = 1;
			Reds++;
			RedAlerts--;
			break;
		}
		for(fp = Gcollect;*fp != NULL;fp++)
			(**fp)(red);
	}
}

