/* IP interface control and configuration routines
 * Copyright 1991 Phil Karn, KA9Q
 *
 * Apr 04       RPB     Po-ed most of the remaining strings
 */
#include <errno.h>
#include <stdio.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include "main/iface.h"
#include "internet/ip.h"
#include "internet/icmp.h"
#include "internet/netuser.h"
/*#include "ax25/ax25.h"*/
/*#include "x86/enet.h"*/
/*#include "x86/pktdrvr.h"*/
#include "main/cmdparse.h"
#include "main/commands.h"
#include "dump/trace.h"
#include "z80/escc.h" /* for Esccchan */
#include "kernel/thread.h" /* for kwait() */
#include "kernel/session.h" /* Nick */
#include "kernel/dprintf.h"
#include <libintl.h>
#include "po/messages.h"

#define IFCMDS_MAX 10

#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
#define HOPPER_ENTRIES 32

struct hopper_entry_s
	{
	struct iface_s *iface_p;
	struct mbuf_s *mbuf_p;
	};

struct hopper_queue_s
	{
	struct hopper_entry_s buf[HOPPER_ENTRIES]; /* Ring buffer */
	struct hopper_entry_s *wp;	/* Write pointer */
	struct hopper_entry_s *rp;	/* Read pointer */
	volatile unsigned int cnt;	/* Count of characters in buffer */
	unsigned int hiwat;		/* High water mark */
	unsigned long overrun;		/* Count of s/w fifo buffer overruns */
	};
#endif

#ifdef MODULE
#define STATIC
extern struct cmds Ifcmds[IFCMDS_MAX];
extern struct hopper_queue_s Hopper;
#else
#define STATIC static
#define MODULE_Hopper
#define MODULE_Ifaces
#define MODULE_Loopback
#define MODULE_Encap
/*#define MODULE_Noipaddr*/
#define MODULE_Ifcmds
#define MODULE_if_tx
#define MODULE_network
#define MODULE_net_route
#define MODULE_nu_send
#define MODULE_nu_output
#define MODULE_doifconfig
#define MODULE_ifipaddr
#define MODULE_iflinkadr
#define MODULE_ifbroad
#define MODULE_ifnetmsk
#define MODULE_ifencap
#define MODULE_setencap
#define MODULE_ifrxbuf
#define MODULE_ifmtu
#define MODULE_ifforw
#define MODULE_showiface
#define MODULE_dodetach
#define MODULE_if_detach
#define MODULE_iftxqlen
#define MODULE_if_lookup
#define MODULE_ismyaddr
#define MODULE_mask2width
#define MODULE_if_name
#define MODULE_bitbucket
#define MODULE_dodialer
#endif

#ifdef MODULE_Hopper
STATIC struct hopper_queue_s Hopper =
	{
		{
		{ NULL, NULL },
		{ NULL, NULL },
		{ NULL, NULL },
		{ NULL, NULL },
		{ NULL, NULL },
		{ NULL, NULL },
		{ NULL, NULL },
		{ NULL, NULL },
		{ NULL, NULL },
		{ NULL, NULL },
		{ NULL, NULL },
		{ NULL, NULL },
		{ NULL, NULL },
		{ NULL, NULL },
		{ NULL, NULL },
		{ NULL, NULL }
		},
	Hopper.buf,
	Hopper.buf,
	0,
	0,
	0
	};
#endif

STATIC void showiface(struct iface_s *ifp);
STATIC int mask2width(int32 mask);
STATIC int ifipaddr(int argc,char *argv[],void *p);
STATIC int iflinkadr(int argc,char *argv[],void *p);
STATIC int ifbroad(int argc,char *argv[],void *p);
STATIC int ifnetmsk(int argc,char *argv[],void *p);
STATIC int ifrxbuf(int argc,char *argv[],void *p);
STATIC int ifmtu(int argc,char *argv[],void *p);
STATIC int ifforw(int argc,char *argv[],void *p);
STATIC int ifencap(int argc,char *argv[],void *p);
STATIC int iftxqlen(int argc,char *argv[],void *p);

/*RPB*/
int dodial_exec(struct iface_s *ifp, int argc, char *argv[]);
/*RPB*/

/* Interface list header */
#ifdef MODULE_Ifaces
struct iface_s *Ifaces = &Loopback;
#endif

/* Loopback pseudo-interface */
#ifdef MODULE_Loopback
struct iface_s Loopback = {
#ifdef IPIP /* Nick */
	&Encap,		/* Link to next entry */
#else
	NULL,		/* Link to next entry */
#endif
	"lo" /*"loopback"*/,	/* name		*/
	0x7f000001L,	/* addr		127.0.0.1 */
	0xffffffffL,	/* broadcast	255.255.255.255 */
	0xffffffffL,	/* netmask	255.255.255.255 */
	MAXINT16,	/* mtu		No limit */
	0,		/* trace	*/
	NULL,	/* trfp		*/
	NULL,		/* forw		*/
	NULL,	/* rxproc	*/
	NULL,	/* txproc	*/
	NULL,	/* supv		*/
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
		{
			{
			{ 0, 0, NULL },
			{ 0, 0, NULL },
			{ 0, 0, NULL },
			{ 0, 0, NULL },
			{ 0, 0, NULL },
			{ 0, 0, NULL },
			{ 0, 0, NULL },
			{ 0, 0, NULL },
			{ 0, 0, NULL },
			{ 0, 0, NULL },
			{ 0, 0, NULL },
			{ 0, 0, NULL },
			{ 0, 0, NULL },
			{ 0, 0, NULL },
			{ 0, 0, NULL },
			{ 0, 0, NULL }
			},
		Loopback.outq.buf,
		Loopback.outq.buf,
		0,
		0,
		0
		},
#else
	NULL,	/* outq		*/
#endif
	0,		/* outlim	*/
	0,		/* txbusy	*/
	NULL,		/* dstate	*/
	NULL,		/* dtickle	*/
	NULL,		/* dstatus	*/
	0,		/* dev		*/
	NULL,		/* (*ioctl)	*/
	NULL,		/* (*iostatus)	*/
	NULL,		/* (*stop)	*/
	NULL,	/* hwaddr	*/
	NULL,		/* extension	*/
	0,		/* xdev		*/
	&Iftypes[0],	/* iftype	*/
	NULL,		/* (*send)	*/
	NULL,		/* (*output)	*/
	NULL,		/* (*raw)	*/
	NULL,		/* (*status)	*/
	NULL,		/* (*discard)	*/
	NULL,		/* (*echo)	*/
	0,		/* ipsndcnt	*/
	0,		/* rawsndcnt	*/
	0,		/* iprecvcnt	*/
	0,		/* rawrcvcnt	*/
	0,		/* lastsent	*/
	0,		/* lastrecv	*/
};
#endif

/* Encapsulation pseudo-interface */
#ifdef MODULE_Encap
struct iface_s Encap = {
	NULL,
	"encap",	/* name		*/
	INADDR_ANY,	/* addr		0.0.0.0 */
	0xffffffffL,	/* broadcast	255.255.255.255 */
	0xffffffffL,	/* netmask	255.255.255.255 */
	MAXINT16,	/* mtu		No limit */
	0,		/* trace	*/
	NULL,	/* trfp		*/
	NULL,		/* forw		*/
	NULL,	/* rxproc	*/
	NULL,	/* txproc	*/
	NULL,	/* supv		*/
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
		{
			{
			{ 0, 0, NULL },
			{ 0, 0, NULL },
			{ 0, 0, NULL },
			{ 0, 0, NULL },
			{ 0, 0, NULL },
			{ 0, 0, NULL },
			{ 0, 0, NULL },
			{ 0, 0, NULL },
			{ 0, 0, NULL },
			{ 0, 0, NULL },
			{ 0, 0, NULL },
			{ 0, 0, NULL },
			{ 0, 0, NULL },
			{ 0, 0, NULL },
			{ 0, 0, NULL },
			{ 0, 0, NULL }
			},
		Encap.outq.buf,
		Encap.outq.buf,
		0,
		0,
		0
		},
#else
	NULL,	/* outq		*/
#endif
	0,		/* outlim	*/
	0,		/* txbusy	*/
	NULL,		/* dstate	*/
	NULL,		/* dtickle	*/
	NULL,		/* dstatus	*/
	0,		/* dev		*/
	NULL,		/* (*ioctl)	*/
	NULL,		/* (*iostatus)	*/
	NULL,		/* (*stop)	*/
	NULL,	/* hwaddr	*/
	NULL,		/* extension	*/
	0,		/* xdev		*/
	&Iftypes[0],	/* iftype	*/
	ip_encap,	/* (*send)	*/
	NULL,		/* (*output)	*/
	NULL,		/* (*raw)	*/
	NULL,		/* (*status)	*/
	NULL,		/* (*discard)	*/
	NULL,		/* (*echo)	*/
	0,		/* ipsndcnt	*/
	0,		/* rawsndcnt	*/
	0,		/* iprecvcnt	*/
	0,		/* rawrcvcnt	*/
	0,		/* lastsent	*/
	0,		/* lastrecv	*/
};
#endif

#if 0 /*def MODULE_Noipaddr*/
char Noipaddr[] = "IP address field missing, and ip address not set\n";
#endif

#ifdef MODULE_Ifcmds
/* Nick has made this STATIC, although STATIC is null when MODULE defined */
STATIC struct cmds Ifcmds[IFCMDS_MAX] = {
	N_("broadcast"),	ifbroad,	0,	2,	NULL,
	N_("encapsulation"),	ifencap,	0,	2,	NULL,
	N_("forward"),		ifforw,		0,	2,	NULL,
	N_("ipaddress"),	ifipaddr,	0,	2,	NULL,
	N_("linkaddress"),	iflinkadr,	0,	2,	NULL,
	N_("mtu"),		ifmtu,		0,	2,	NULL,
	N_("netmask"),		ifnetmsk,	0,	2,	NULL,
	N_("txqlen"),		iftxqlen,	0,	2,	NULL,
	N_("rxbuf"),		ifrxbuf,	0,	2,	NULL,
	NULL,
};
#endif

/*
 * General purpose interface transmit task, one for each device that can
 * send IP datagrams. It waits on the interface's IP output queue (outq),
 * extracts IP datagrams placed there in priority order by ip_route(),
 * and sends them to the device's send routine.
 */
#ifdef MODULE_if_tx
void
if_tx(int dev,void *arg1,void *unused)
{
	struct mbuf_s *bp;	/* Buffer to send */
	struct iface_s *iface;	/* Pointer to interface control block */
	struct qhdr qhdr;

	iface = arg1;
	for(;;){
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
		while (iface->outq.cnt == 0)
			{
			/* cast shouldn't be needed */
			kwait((void *)&iface->outq.cnt);
			}
		iface->outq.cnt--;

		qhdr.tos = iface->outq.rp->tos;
		qhdr.gateway = iface->outq.rp->gateway;
		bp = iface->outq.rp->mbuf_p;
#if DEBUG >= 2
 dprintf(2, "o %04x %04x -> %02x %08lx %04x\n", iface, iface->outq.rp,
   qhdr.tos, qhdr.gateway, bp);
#endif

		iface->outq.rp++;
		if (iface->outq.rp >= iface->outq.buf + OUTPUT_ENTRIES)
			{
			iface->outq.rp = iface->outq.buf;
			}

		iface->txbusy = 1;
#else
/* abyte('A'); */
		while(iface->outq == NULL)
 {
/* abyte('B'); */
			kwait(&iface->outq);
 }

/* abyte('C'); */
		iface->txbusy = 1;
/* abyte('D'); */
		bp = dequeue(&iface->outq);
/* abyte('E'); */
		pullup(&bp,&qhdr,sizeof(qhdr));
/* abyte('F'); */
#endif
		if(iface->dtickle != NULL && (*iface->dtickle)(iface) == -1){
/* abyte('G'); */
#ifdef	notdef	/* Confuses some non-compliant hosts */
			struct ip ip;

			/* Link redial failed; bounce with unreachable */
			ntohip(&ip,&bp);
			icmp_output(&ip,bp,ICMP_DEST_UNREACH,ICMP_HOST_UNREACH,
			 NULL);
#endif
			free_p(&bp);
		} else {
/* abyte('H'); */
			(*iface->send)(&bp,iface,qhdr.gateway,qhdr.tos);
		}
/* abyte('I'); */
		iface->txbusy = 0;

		/* Let other tasks run, just in case send didn't block */
/* abyte('J'); */
		kwait(NULL);
	}
/* abyte('K'); */
}
#endif

/* Process packets in the Hopper */
#ifdef MODULE_network
void
network(int i,void *v1,void *v2)
{
	struct mbuf_s *bp;
	char i_state;
	struct iftype *ift;
	struct iface_s *ifp;

	for (;;){
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
		while (Hopper.cnt == 0)
			{
			/* cast shouldn't be needed */
			kwait((void *)&Hopper.cnt);
			}
		Hopper.cnt--;

		ifp = Hopper.rp->iface_p;
		bp = Hopper.rp->mbuf_p;
#if DEBUG >= 2
 dprintf(2, "H %04x -> %04x %04x\n", Hopper.rp, ifp, bp);
#endif

		Hopper.rp++;
		if (Hopper.rp >= Hopper.buf + HOPPER_ENTRIES)
			{
			Hopper.rp = Hopper.buf;
			}
#else
/* abyte('A'); */
		for(;;){
/* abyte('B'); */
			i_state = dirps();
			bp = Hopper;
			if(bp != NULL){
/* abyte('C'); */
				bp = dequeue(&Hopper);
/* abyte('D'); */
				restore(i_state);
				break;
			}
			restore(i_state);
			kwait(&Hopper);
		}
		/* Process the input packet */
/* abyte('E'); */
		pullup(&bp,&ifp,sizeof(ifp));
/* abyte('F'); */
#endif
		if(ifp != NULL){
			ifp->rawrecvcnt++;
			ifp->lastrecv = secclock();
			ift = ifp->iftype;
		} else {
			ift = &Iftypes[0];
		}
/* abyte('G'); */
		dump(ifp,IF_TRACE_IN,bp);

/* abyte('H'); */
		if(ift->rcvf != NULL)
 {
/* abyte('I'); */
			(*ift->rcvf)(ifp,&bp);
 }
		else
 {
/* abyte('J'); */
			free_p(&bp);	/* Nowhere to send it */
 }

		/* Let everything else run - this keeps the system from wedging
		 * when we're hit by a big burst of packets
		 */
/* abyte('K'); */
		kwait(NULL);
	}
}
#endif

/* put mbuf into Hopper for network task
 * returns 0 if OK
 */
#ifdef MODULE_net_route
unsigned int
net_route(struct iface_s *ifp,struct mbuf_s **bpp)
	{
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	if (Hopper.cnt >= HOPPER_ENTRIES)
		{
		Hopper.overrun++;

		free_p(bpp);
#if DEBUG >= 1
 dprintf(1, "net_route() drop\n");
#endif
		return EOVERFLOW;
		}

	Hopper.wp->iface_p = ifp;
	Hopper.wp->mbuf_p = *bpp;
#if DEBUG >= 2
 dprintf(2, "H %04x <- %04x %04x\n", Hopper.wp, ifp, *bpp);
#endif

	Hopper.wp++;
	if (Hopper.wp >= Hopper.buf + HOPPER_ENTRIES)
		{
		Hopper.wp = Hopper.buf;
		}
	Hopper.cnt++;

	/* cast shouldn't be needed */
	ksignal((void *)&Hopper.cnt, 0);
	if (Hopper.hiwat < Hopper.cnt)
		{
		Hopper.hiwat = Hopper.cnt;
		}

	*bpp = NULL;
#else
	pushdown(bpp,&ifp,sizeof(ifp));
	enqueue(&Hopper,bpp);
#endif
	return 0;
	}
#endif

/* Null send and output routines for interfaces without link level protocols */

#ifdef MODULE_nu_send
int
nu_send(struct mbuf_s **bpp,struct iface_s *ifp,int32 gateway,uint8 tos)
{
	return (*ifp->raw)(ifp,bpp);
}
#endif

#ifdef MODULE_nu_output
int
nu_output(struct iface_s *ifp,uint8 *dest,uint8 *src,uint16 type,struct mbuf_s **bpp)
{
	return (*ifp->raw)(ifp,bpp);
}
#endif

/* Set interface parameters */

#ifdef MODULE_doifconfig
int
doifconfig(int argc,char *argv[],void *p)
{
	struct iface_s *ifp;
	int i;

	if(argc < 2){
		for(ifp = Ifaces;ifp != NULL;ifp = ifp->next)
			showiface(ifp);
		return 0;
	}
	if((ifp = if_lookup(argv[1])) == NULL){
		_printf(_("Interface %s unknown\n"),argv[1]);
		return 1;
	}
	if(argc == 2){
		showiface(ifp);
		if(ifp->show != NULL){
			(*ifp->show)(ifp);
		}
		return 0;
	}
	if(argc == 3){
		_printf(_("Argument missing\n"));
		return 1;
	}
	for(i=2;i<argc-1;i+=2)
		subcmd(Ifcmds,3,&argv[i-1],ifp);

	return 0;
}
#endif

/* Set interface IP address */

#ifdef MODULE_ifipaddr
STATIC int
ifipaddr(int argc,char *argv[],void *p)
{
	struct iface_s *ifp = p;

	ifp->addr = resolve(argv[1]);
	return 0;
}
#endif


/* Set link (hardware) address */

#ifdef MODULE_iflinkadr
STATIC int
iflinkadr(int argc,char *argv[],void *p)
{
	struct iface_s *ifp = p;

	if(ifp->iftype == NULL || ifp->iftype->scan == NULL){
		_printf(_("Can't set link address\n"));
		return 1;
	}
	if(ifp->hwaddr != NULL)
		free(ifp->hwaddr);
	ifp->hwaddr = mallocw(ifp->iftype->hwalen);
	(*ifp->iftype->scan)(ifp->hwaddr,argv[1]);
	return 0;
}
#endif

/* Set interface broadcast address. This is actually done
 * by installing a private entry in the routing table.
 */

#ifdef MODULE_ifbroad
STATIC int
ifbroad(int argc,char *argv[],void *p)
{
	struct iface_s *ifp = p;
	struct route *rp;

	rp = rt_blookup(ifp->broadcast,32);
	if(rp != NULL && rp->iface == ifp)
		rt_drop(ifp->broadcast,32);
	ifp->broadcast = resolve(argv[1]);
	rt_add(ifp->broadcast,32,0L,ifp,1L,0L,1);
	return 0;
}
#endif

/* Set the network mask. This is actually done by installing
 * a routing entry.
 */

#ifdef MODULE_ifnetmsk
STATIC int
ifnetmsk(int argc,char *argv[],void *p)
{
	struct iface_s *ifp = p;
	struct route *rp;

	/* Remove old entry if it exists */
	rp = rt_blookup(ifp->addr & ifp->netmask,mask2width(ifp->netmask));
	if(rp != NULL)
		rt_drop(rp->target,rp->bits);

	ifp->netmask = htol(argv[1]);
	rt_add(ifp->addr,mask2width(ifp->netmask),0L,ifp,0L,0L,0);
	return 0;
}
#endif

/* Command to set interface encapsulation mode */

#ifdef MODULE_ifencap
STATIC int
ifencap(int argc,char *argv[],void *p)
{
	struct iface_s *ifp = p;

	if(setencap(ifp,argv[1]) != 0){
		_printf(_("Encapsulation mode '%s' unknown\n"),argv[1]);
		return 1;
	}
	return 0;
}
#endif
/* Function to set encapsulation mode */

#ifdef MODULE_setencap
int
setencap(struct iface_s *ifp,char *mode)
{
	struct iftype *ift;

	for(ift = &Iftypes[0];ift->name != NULL;ift++)
		if(strnicmp(ift->name,mode,strlen(mode)) == 0)
			break;
	if(ift->name == NULL)
		return -1;

	if(ifp != NULL){
		ifp->iftype = ift;
		ifp->send = ift->send;
		ifp->output = ift->output;
	}
	return 0;
}
#endif
/* Set interface receive buffer size */

#ifdef MODULE_ifrxbuf
STATIC int
ifrxbuf(int argc,char *argv[],void *p)
{
	return 0;	/* To be written */
}
#endif

/* Set interface Maximum Transmission Unit */

#ifdef MODULE_ifmtu
STATIC int
ifmtu(int argc,char *argv[],void *p)
{
	struct iface_s *ifp = p;

	ifp->mtu = atoi(argv[1]);
	return 0;
}
#endif

/* Set interface forwarding */

#ifdef MODULE_ifforw
STATIC int
ifforw(int argc,char *argv[],void *p)
{
	struct iface_s *ifp = p;

	ifp->forw = if_lookup(argv[1]);
	if(ifp->forw == ifp)
		ifp->forw = NULL;
	return 0;
}
#endif

/* Display the parameters for a specified interface */

#ifdef MODULE_showiface
STATIC void
showiface(struct iface_s *ifp)
{
	char tmp[25];

	_printf(_("%-10s IP addr %s MTU %u Link encap %s\n"),ifp->name,
	 inet_ntoa(ifp->addr),(int)ifp->mtu,
	 ifp->iftype != NULL ? ifp->iftype->name : "not set");
	if(ifp->iftype != NULL && ifp->iftype->format != NULL && ifp->hwaddr != NULL){
		_printf(_("           Link addr %s\n"),
		 (*ifp->iftype->format)(tmp,ifp->hwaddr));
	}
	_printf(_("           trace 0x%x netmask 0x%08lx broadcast %s\n"),
		ifp->trace,ifp->netmask,inet_ntoa(ifp->broadcast));
	if(ifp->forw != NULL)
		_printf(_("           output forward to %s\n"),ifp->forw->name);
	_printf(_("           sent: ip %lu tot %lu idle %s qlen %u"),
	 ifp->ipsndcnt,ifp->rawsndcnt,tformat(secclock() - ifp->lastsent),
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
		ifp->outq.cnt);
#else
		len_q(ifp->outq));
#endif
	if(ifp->outlim != 0)
		_printf(_("/%u"),ifp->outlim);
	if(ifp->txbusy)
		_printf(_(" BUSY"));
	_printf(_("\n"));
	_printf(_("           recv: ip %lu tot %lu idle %s\n"),
	 ifp->iprecvcnt,ifp->rawrecvcnt,tformat(secclock() - ifp->lastrecv));
}
#endif

/* Command to detach an interface */

#ifdef MODULE_dodetach
int
dodetach(int argc,char *argv[],void *p)
{
	register struct iface_s *ifp;

	if((ifp = if_lookup(argv[1])) == NULL){
		_printf(_("Interface %s unknown\n"),argv[1]);
		return 1;
	}
	if(if_detach(ifp) == -1)
		_printf(_("Can't detach loopback or encap interface\n"));
	return 0;
}
#endif

/* Detach a specified interface */
#ifdef MODULE_if_detach
int
if_detach(struct iface_s *ifp)
{
	struct iface_s *iftmp;
	struct route *rp,*rptmp;
	int i,j;
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	struct output_entry_s *op;
	unsigned int tmp, cnt;
#endif

#ifdef IPIP /* Nick */
	if(ifp == &Loopback || ifp == &Encap)
#else
	if(ifp == &Loopback)
#endif
		return -1;

	/* Drop all routes that point to this interface */
	if(R_default.iface == ifp)
		rt_drop(0L,0);	/* Drop default route */

	for(i=0;i<HASHMOD;i++){
		for(j=0;j<32;j++){
			for(rp = Routes[j][i];rp != NULL;rp = rptmp){
				/* Save next pointer in case we delete this entry */
				rptmp = rp->next;
				if(rp->iface == ifp)
					rt_drop(rp->target,rp->bits);
			}
		}
	}

	/* Unforward any other interfaces forwarding to this one */
	for(iftmp = Ifaces;iftmp != NULL;iftmp = iftmp->next){
		if(iftmp->forw == ifp)
			iftmp->forw = NULL;
	}

	/* Call device shutdown routine, if any */
	if(ifp->stop != NULL)
		{
		(*ifp->stop)(Esccchan[ifp->dev]);
		}

	killproc(ifp->rxproc);
	killproc(ifp->txproc);
	killproc(ifp->supv);

	/* Free allocated memory associated with this interface */
	if(ifp->name != NULL)
		free(ifp->name);
	if(ifp->hwaddr != NULL)
		free(ifp->hwaddr);

	/* Remove from interface list */
	if(ifp == Ifaces){
		Ifaces = ifp->next;
	} else {
		/* Search for entry just before this one
		 * (necessary because list is only singly-linked.)
		 */
		for(iftmp = Ifaces;iftmp != NULL ;iftmp = iftmp->next)
			if(iftmp->next == ifp)
				break;
		if(iftmp != NULL && iftmp->next == ifp)
			iftmp->next = ifp->next;
	}

#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	/* Drop any leftover output packets */
	op = ifp->outq.rp;
	cnt = ifp->outq.cnt;

	for (tmp = 0; tmp < cnt; tmp++)
		{
		free_p(&op->mbuf_p);

		op++;
		if (op >= ifp->outq.buf + OUTPUT_ENTRIES)
			{
			op = ifp->outq.buf;
			}
		}
#else
	/* Drop any leftover output packets */
	free_q(&ifp->outq); /* ADDED BY NICK!  MUST HAVE BEEN AN OVERSIGHT!! */
#endif

	/* Finally free the structure itself */
	free(ifp);
	return 0;
}
#endif

#ifdef MODULE_iftxqlen
STATIC int
iftxqlen(int argc,char *argv[],void *p)
{
	struct iface_s *ifp = p;

	setint(&ifp->outlim, _("TX queue limit"), argc, argv);
	return 0;
}
#endif

/* Given the ascii name of an interface, return a pointer to the structure,
 * or NULL if it doesn't exist
 */

#ifdef MODULE_if_lookup
struct iface_s *
if_lookup(char *name)
{
	register struct iface_s *ifp;

	for(ifp = Ifaces; ifp != NULL; ifp = ifp->next)
		if(strcmp(ifp->name,name) == 0)
			break;
	return ifp;
}
#endif

/* Return iface pointer if 'addr' belongs to one of our interfaces,
 * NULL otherwise.
 * This is used to tell if an incoming IP datagram is for us, or if it
 * has to be routed.
 */

#ifdef MODULE_ismyaddr
struct iface_s *
ismyaddr(int32 addr)
{
	register struct iface_s *ifp;

	if(addr == INADDR_ANY)
		return &Loopback;
	for(ifp = Ifaces; ifp != NULL; ifp = ifp->next)
		if(addr == ifp->addr)
			break;
	return ifp;
}
#endif

/* Given a network mask, return the number of contiguous 1-bits starting
 * from the most significant bit.
 */

#ifdef MODULE_mask2width
STATIC int
mask2width(int32 mask)
{
	int width,i;

	width = 0;
	for(i = 31;i >= 0;i--){
		if(!(mask & (1L << i)))
			break;
		width++;
	}
	return width;
}
#endif

/* return buffer with name + comment */

#ifdef MODULE_if_name
/*RPB*/
char *
if_name(struct iface_s *ifp,_char *comment)
{

#if 0
	char *result;
	result = mallocw(strlen(ifp->name) + strlen(comment) + 1);
	strcpy(result,ifp->name);
	strcat(result,comment);
	return result;
#else

	char *result;
	char *duptext;

	if ((duptext = _strdup(comment)) == NULL)
	{
		__getfail(comment);
		return NULL;
	}

	result = mallocw(strlen(ifp->name) + strlen(duptext) + 1);
	strcpy(result,ifp->name);
	strcat(result,duptext);
	return result;
#endif
/*RPB*/
}
#endif

/* Raw output routine that tosses all packets. Used by dialer, tip, etc */

#ifdef MODULE_bitbucket
int
bitbucket(struct iface_s *ifp,struct mbuf_s **bpp)
{
	free_p(bpp);
	return 0;
}
#endif
/*
 * dial <iface> <seconds> [device dependent args]	(begin autodialing)
 * dial <iface> 0	(stop autodialing)
 * dial <iface>	(display status)
 * -RPB-
 * dial <iface> exec <dialer subcommands>
 * -RPB-
 */

#ifdef MODULE_dodialer
int
dodialer(int argc,char *argv[],void *p)
{
	struct iface_s *ifp;
	int32 timeout;

	if((ifp = if_lookup(argv[1])) == NULL){
		_printf(_("Interface %s unknown\n"),argv[1]);
		return 1;
	}
	if(argc < 3){
		if(ifp->iftype->dstat != NULL)
			(*ifp->iftype->dstat)(ifp);
		return 0;
	}
	if(ifp->iftype->dinit == NULL){
		_printf(_("Dialing not supported on %s\n"),argv[1]);
		return 1;
	}
/*RPB*/
	if (_stricmp(argv[2], _("exec")) == 0)
		return (dodial_exec(ifp, argc-2, argv+2));
/*RPB*/
	timeout = atol(argv[2]) * 1000L;

	return (*ifp->iftype->dinit)(ifp,timeout,argc-3,argv+3);
}
#endif

