/* Routines for auditing mbuf consistency. Not used for some time, may
 * not be up to date.
 * Copyright 1991 Phil Karn, KA9Q
 */
#include <stdio.h>
#include "nos/global.h"
#include "kernel/thread.h"
#include "main/mbuf.h"
#include <libintl.h>
#include "po/messages.h"

extern uint8 _Uend;
extern int _STKRED;

union header {
	struct {
		union header *ptr;
		unsigned size;
	} s;
	long l;
};

void audit(struct mbuf_s *bp,char *file,int line);
static void audit_mbuf(struct mbuf_s *bp,char *file,int line);
static void dumpbuf(struct mbuf_s *bp);

/* Perform sanity checks on mbuf. Print any errors, return 0 if none,
 * nonzero otherwise
 */
void
audit(
struct mbuf_s *bp,
char *file,
int line
){
	register struct mbuf_s *bp1;

	for(bp1 = bp;bp1 != NULL; bp1 = bp1->next)
		audit_mbuf(bp1,file,line);
}

static void
audit_mbuf(
struct mbuf_s *bp,
char *file,
int line
){
	union header *blk;
	uint8 *bufstart,*bufend;
	uint16 overhead = sizeof(union header) + sizeof(struct mbuf_s);
	uint16 datasize;
	int errors = 0;
	uint8 *heapbot,*heaptop;

	if(bp == NULL)
		return;

	heapbot = &_Uend;
	heaptop = (uint8 *) -_STKRED;

	/* Does buffer appear to be a valid malloc'ed block? */
	blk = ((union header *)bp) - 1;
	if(blk->s.ptr != blk){
		_printf(_("Garbage bp %lx\n"),(long)bp);
		errors++;
	}
	if((datasize = blk->s.size*sizeof(union header) - overhead) != 0){
		/* mbuf has data area associated with it, verify that
		 * pointers are within it
		 */
		bufstart = (uint8 *)(bp + 1);
		bufend = bufstart + datasize;
		if(bp->data < bufstart){
			_printf(_("Data pointer before buffer\n"));
			errors++;
		}
		if(bp->data + bp->cnt > bufend){
			_printf(_("Data pointer + count past bounds\n"));
			errors++;
		}
	} else {
		/* Dup'ed mbuf, at least check that pointers are within
		 * heap area
		*/

		if(bp->data < heapbot
		 || bp->data + bp->cnt > heaptop){
			_printf(_("Data outside heap\n"));
			errors++;
		}
	}
	/* Now check link list pointers */
	if(bp->next != NULL && ((bp->next < (struct mbuf_s *)heapbot)
		 || bp->next > (struct mbuf_s *)heaptop)){
			_printf(_("next pointer out of limits\n"));
			errors++;
	}
#if 0 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	if(bp->anext != NULL && ((bp->anext < (struct mbuf_s *)heapbot)
		 || bp->anext > (struct mbuf_s *)heaptop)){
			_printf(_("anext pointer out of limits\n"));
			errors++;
	}
#endif
	if(errors != 0){
		dumpbuf(bp);
		_printf(_("PANIC: buffer audit failure in %s line %d\n"),file,line);
		fflush(stdout);
		for(;;)
			;
	}
	return;
}

static void
dumpbuf(struct mbuf_s *bp)
{
	union header *blk;
	if(bp == NULL){
		_printf(_("NULL BUFFER\n"));
		return;
	}
	blk = ((union header *)bp) - 1;
	_printf(_("bp %lx tot siz %u data %lx cnt %u next %lx" /*" anext %lx"*/ "\n"),
		(long)bp,blk->s.size * sizeof(union header),
		(long)bp->data,bp->cnt,
		(long)bp->next/*,(long)bp->anext*/);
}

