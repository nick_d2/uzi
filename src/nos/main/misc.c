/* Main-level NOS program:
 *  initialization
 *  keyboard processing
 *  generic user commands
 *
 * Copyright 1986-1995 Phil Karn, KA9Q
 *
 * Apr 04	RPB	Po-ed most of the remaining strings
 */
#include <stdio.h>
#include <string.h>
#ifdef JOB_CONTROL
#include <sgtty.h> /* for TIOC_WAIT_SESSION */
#endif
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <ctype.h>
#if defined(__TURBOC__) && defined(MSDOS)
#include <io.h>
#include <conio.h>
#endif
#include <stdarg.h>
#include "asm.h"
#include "nos/global.h"
#include "main/config.h" /* Nick for MTHRESH */
#include "main/mbuf.h"
#include "main/timer.h"
#include "main/iface.h"
#include "internet/ip.h"
#include "internet/tcp.h"
#include "internet/udp.h"
/*#include "ax25/ax25.h"*/
/*#include "ax25/kiss.h"*/
/*#include "x86/enet.h"*/
/*#include "netrom/netrom.h"*/
#include "client/ftpcli.h"
#include "client/telnet.h"
/*#include "driver/devtty.h"*/
#ifdef JOB_CONTROL
#include "kernel/session.h"
#endif
#include "nos/hardware.h"
#include "socket/usock.h"
#include "socket/socket.h"
#include "main/cmdparse.h"
#include "main/commands.h"
#include "main/daemon.h"
#include "main/devparam.h"
#include "internet/domain.h"
#include "main/files.h"
#include "main/main.h"
#include "server/remote.h"
#include "dump/trace.h"
/*#include "x86/display.h"*/
#include "driver/rtc.h"
#include "filesys/filesys.h" /* for fs_init() and fs_end() */
#include "filesys/xip.h"
#include "kernel/thread.h" /* also "kernel/process.h", "kernel/arena.h" */
#include "kernel/dprintf.h"
#include "z80/asci.h"
#include "z80/escc.h"
#include "main/logmsg.h"
#include "main/smsg.h"
#include "kernel/stack.h"
#include <libintl.h>
#include "po/messages.h"

#ifdef MODULE
#define STATIC
extern _char *Prompt;
extern FILE *Logfp;
extern time_t StartTime;
extern int Verbose;
/*extern unsigned int mon_lengths[];*/
extern char *Rempass; /* moved here from smisc.c */
#else
#define STATIC static
#define MODULE_Badhost
#define MODULE_Hostname
#define MODULE_Nospace
#define MODULE_Cmdline
#define MODULE_daemon_process_p
/*#define MODULE_driver_process_p*/
#define MODULE_main_exit
#define MODULE_Prompt
#define MODULE_Logfp
#define MODULE_StartTime
#define MODULE_Verbose
#define MODULE_Rempass
#define MODULE_Whitespace
#define MODULE_main_init
#define MODULE_net_prompt
#define MODULE_dorepeat
#define MODULE_dodelete
#define MODULE_dorename
#define MODULE_doexec
#define MODULE_dospawn
#define MODULE_doexit
#define MODULE_doshutdown
#define MODULE_dohostname
#define MODULE_dolog
#define MODULE_dohelp
#define MODULE_doattach
#define MODULE_doparam
#define MODULE_doremote
#define MODULE_dopage
#define MODULE_dosysdebug
#define MODULE_dosystrace
#define MODULE_donothing
#define MODULE_logmsg
/*#define MODULE_mon_lengths*/
/*#define MODULE_dodate*/
#define MODULE_smsg
#define MODULE_htoi
#define MODULE_htob
#define MODULE_readhex
#define MODULE_rip
#define MODULE_memcnt
#define MODULE_memxor
#define MODULE_strdupw
#define MODULE__strdupw
#define MODULE_strnicmp
#define MODULE_strtok
#define MODULE_isdelim
#define MODULE_put32
#define MODULE_put16
#define MODULE_get16
#define MODULE_get32
#define MODULE_ilog2
#endif

#ifdef MODULE_Badhost
_char *Badhost = N_("Unknown host %s\n");
#endif
#ifdef MODULE_Hostname
char *Hostname;
#endif
#ifdef MODULE_Nospace
_char *Nospace = N_("No space!!\n");	/* Generic malloc fail message */
#endif
#ifdef MODULE_Cmdline
char *Cmdline;				/* Copy of most recent command line */
#endif
#ifdef MODULE_daemon_process_p
struct process_s *daemon_process_p;
#endif
#if 0 /*def MODULE_driver_process_p*/
struct process_s *driver_process_p;
#endif
#ifdef MODULE_main_exit
int main_exit = FALSE;			/* from main program (flag) */
#endif

#ifdef MODULE_Prompt
STATIC _char *Prompt = N_("\x1b[01;32mnet> \x1b[00m");
#endif
#ifdef MODULE_Logfp
STATIC FILE *Logfp;
#endif
#ifdef MODULE_StartTime
STATIC time_t StartTime;		/* time that NOS was started */
#endif
#ifdef MODULE_Verbose
STATIC int Verbose;
#endif
#ifdef MODULE_Rempass
char *Rempass = "";	/* Remote access password (moved here from smisc.c) */
#endif
#ifdef MODULE_Whitespace
char Whitespace[WHITESPACE_MAX] = " \t\r\n";
#endif

#ifdef ZILOG
extern unsigned char __rom_serial_no[6];

extern unsigned int __stack_base;
extern unsigned int __stack_limit;
#endif

STATIC int htob(char c);
STATIC int isdelim(char c, char *delim);

#ifdef MODULE_main_init
int
main_init(argc,argv)
int argc;
char **argv;
{
#if 0
	FILE *fp;
#endif
	struct daemon *tp;
	int c;
#ifdef LARGEDATA
	long hinit = 102400;
	unsigned chunk;
	void **list;
#endif
#if 0
	struct arena_s *stack_arena_p;
#endif

 Verbose = 1;
#if 0
	/* create the mother of all threads */
	my_thread_p = thread_new();
	if (my_thread_p == NULL)
		{
		goto error;
		}
	my_thread_p->name = strdup("tick"); /*"main");*/
	if (my_thread_p->name == NULL)
		{
	error:
		_panic(_("can't create tick thread"));
		}
	/* bbr_value and cbar_value are not used inside the kernel */
	my_thread_p->stack = (unsigned short *)&__stack_base;
	my_thread_p->stksize = &__stack_limit - &__stack_base;
	my_thread_p->stack_limit = (unsigned int)&__stack_limit;
	my_thread_p->flags.active = 1;

	/* create the mother of all processes */
	daemon_process_p = process_new();
	if (daemon_process_p == NULL)
		{
		_panic(_("can't create daemon process"));
		}
	daemon_process_p->p_pid = 0;
	daemon_process_p->status = P_RUNNING;
	/* daemon_process_p->parent_p is null meaning orphaned */
	daemon_process_p->p_cprio = MAXTICKS;
	daemon_process_p->p_umask = 022;
	daemon_process_p->p_exitval = -1; /* will be inherited by all procs */
	my_thread_p->process_p = daemon_process_p;

	/* create the mother of all apartments */
	head_arena_p = arena_new();
	if (head_arena_p == NULL)
		{
		goto error1;
		}
	head_arena_p->size = 14; /* 56 kbytes */
	daemon_process_p->arena_p = head_arena_p;

	/* can now use arena_allocate() function */
	stack_arena_p = arena_allocate(1);
	if (stack_arena_p == NULL)
		{
	error1:
		_panic(_("can't create daemon arenas"));
		}
	stack_set(stack_arena_p, (union store *)KRNL_BA_START,
			(union store *)KRNL_CA1_START - 1);
	stack_set(stack_arena_p, (union store *)KRNL_CA1_START - 1,
			(union store *)(KRNL_BA_START + 1));

	/* note: BBR is already == 0, so the stack arena already appears */
	/* at 0xe000 ... in any case, we update BBR during every longjmp() */
	daemon_process_p->stack_arena_p = stack_arena_p;

	arena_ref(stack_arena_p);
	my_thread_p->stack_arena_p = stack_arena_p;
#endif

#if 0 /* this should print "EFFE E001" */
 ahexw(*(unsigned int *)KRNL_BA_START);
 abyte(' ');
 ahexw(*((unsigned int *)KRNL_CA1_START - 1));
 acrlf();
#endif

#if 0
	driver_process_p = process_fork(daemon_process_p, 1);
	if (driver_process_p == NULL)
		{
		_panic(_("can't create driver process"));
		}
#endif

#ifdef	PROCLOG /* formerly in mainproc() DOESN'T WORK, proclog NOT EXTERN! */
	/* could we use serial 0 for this? */
	proclog = fopen("proclog",APPEND_TEXT);
	proctrace = fopen("proctrace",APPEND_TEXT);
#endif

	StartTime = time(&StartTime);

	while ((c = getopt(argc, argv, "v" /*"f:s:d:bvh:"*/)) != EOF)
		{
		switch(c)
			{
#ifdef LARGEDATA
		case 'h':	/* Heap initialization */
			hinit = atol(optarg);
			break;
#endif
#if 0 /* now using MTHRESH directly */
		case 'm':	/* Memory threshold */
			Memthresh = atoi(optarg); /* atol(optarg); */
			break;
#endif
#ifdef __TURBOC__
		case 'b':	/* Use BIOS for screen output */
			directvideo = 0;
			break;
#endif
		case 'v':
			Verbose = 1;
			break;
			}
		}
	/* Get some memory on the heap so interrupt calls to malloc
	 * won't fail unnecessarily
	 */
#ifdef LARGEDATA
	list = calloc(sizeof(void *),(hinit / 32768L) + 1);
	for(c=0;hinit > 0;hinit -= chunk)
		{
		chunk = min(hinit,32768U);
		list[c++] = malloc(chunk);
		}
	while(c > 0)
		{
		free(list[--c]);
		}
	free(list);
#else
	/*free(malloc(Memthresh));*/ /* may be free(NULL); */
#endif

	kinit();
	/*ipinit();*/
	ioinit();
	/* sockinit(); */

 Hostname = strdupw("sn99999"); /* not allowed to run with Hostname = NULL */

#ifdef ZILOG /* Nick */
 for (c = 0; c < 4; c++)
  {
  if (__rom_serial_no[c] != ' ')
   {
   break;
   }
  }
 strcpy(Hostname + 2, __rom_serial_no + c);

	/* now done here, before opening stdin/stdout */
	/* note: this won't use _printf but may use _panic hence _dprintf */
	fs_init(0);

	if (open("/dev/tty1", O_RDWR) != 0 || fdopen(0, "rb") == NULL ||
			dup(0) != 1 || fdopen(1, "wb") == NULL ||
			dup(0) != 2 || fdopen(2, "wb") == NULL ||
			setvbuf(stderr, NULL, _IONBF, 0) != 0)
		{
		_panic(_("failed to open tty1"));
		}
#endif

	_printf(_(
	       "\n\x1b[01;32mHytech KA9Q NOS/UZI kernel\n"
	       "Version %s\n"
#ifdef ZILOG
	       "Compiled for Zilog Z8S180 CPU\x1b[00m\n"
#endif
#ifdef CPU386
	       "Compiled for Intel 386/486 CPU\n"
#endif
	       "\n"
	       "\x1b[01;37mCopyright 1986-1995 by Phil Karn, KA9Q\n"
	       "Copyright 2003-2004 by ND, RPB, Hytech\x1b[00m\n"
	       "\n"),
	       Version);
/* abyte('A'); */
	usercvt();

	/* Start background Daemons */
/* abyte('B'); */
	for(tp = Daemons; tp->name; tp++)
		{
/* abyte('C'); */
		if (process_thread_create(daemon_process_p, tp->name,
				tp->stksize, tp->fp, 0, NULL, NULL, 0) == NULL)
			{
			_fprintf(stderr, _("can't start daemon thread %s\n"),
					tp->name);
			}
/* abyte('D'); */

		}
#ifdef IPIP /* Nick */
	Encap.txproc = process_thread_create(daemon_process_p, "encap tx", 384 /*512*/, if_tx,0,&Encap,NULL,0);
#endif
/* abyte('E'); */
	return optind;
	}
#endif

#ifdef MODULE_net_prompt
void
net_prompt(argc,argv,p)
int argc;
void *argv;
void *p;
	{
	FILE *fp;
	char cmdbuf[256];
#ifdef JOB_CONTROL
	struct session_s *sp;

/* abyte('x'); */
	sp = newsession("main", COMMAND, 1);
#endif

	/* miscellaneous parameter, if given, is filename for profile cmds */
/* abyte('y'); */
	if (p)
		{
/* abyte('z'); */
		/* Pacify user, as it may be a long script */
		_printf(_("Processing %s\n"), p);

		/* Read startup file named on command line */
		fp = fopen((char *)p, READ_TEXT);
		if (fp == NULL)
			{
			_fprintf(stderr, _("Can't read config file "));
			perror(p);
			goto silly;
			}

		while(fgets(cmdbuf,sizeof(cmdbuf),fp) != NULL)
			{
			rip(cmdbuf);
			if (Cmdline)
				{
				free(Cmdline);
				}
			Cmdline = strdupw(cmdbuf);
			if (Verbose)
				{
				puts(cmdbuf);
				fflush(stdout);
				}
#ifdef JOB_CONTROL
 else
  {
  ioctl(1, TIOC_WAIT_SESSION); /* wait until output would have been allowed */
  }
#endif
			if (cmdparse(Cmds,cmdbuf,NULL) == -1)
				{
#ifdef JOB_CONTROL
				freesession(sp, 0);
#endif
				return;
				}
			fflush(stdout);
#ifdef JOB_CONTROL
 kwait(NULL); /* give command a chance to block our printing of the prompt */
#endif
			}
		fclose(fp);
		}
silly:

	/* Now loop forever, processing commands */
	for (;;)
		{
		_fputs(Prompt, stdout);
		fflush(stdout);
		if (fgets(cmdbuf,sizeof(cmdbuf),stdin) == NULL)
			{
#ifdef JOB_CONTROL
			freesession(sp, 0);
#endif
			return;
			}
		rip(cmdbuf);
		if (Cmdline)
			{
			free(Cmdline);
			}
		Cmdline = strdupw(cmdbuf);
		if (cmdparse(Cmds,cmdbuf,NULL/*Lastcurr*/) == -1)
			{
#ifdef JOB_CONTROL
			freesession(sp, 0);
#endif
			return;
			}
		fflush(stdout);
#ifdef JOB_CONTROL
 kwait(NULL); /* give command a chance to block our printing of the prompt */
#endif
		}
	}
#endif

#ifdef MODULE_dodelete
int
dodelete(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	int i;

	for(i=1;i < argc; i++){
		if(unlink(argv[i]) == -1){
			_printf(_("Can't delete %s"),argv[i]);
			_perror(_(""));
		}
	}
	return 0;
}
#endif

#ifdef MODULE_dorename
int
dorename(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	if(rename(argv[1],argv[2]) == -1){
		_printf(_("Can't rename %s"),argv[1]);
		_perror(_(""));
	}
	return 0;
}
#endif

#ifdef MODULE_doexec
int
doexec(argc,argv,p)
int argc;
char *argv[];
void *p;
	{
	int i;

	if (execve(argv[1], argv + 1, environ) == -1)
		{
		_fputs(_("can't exec "), stderr); /* TODO:  ALL LIKE THIS! */
		perror(argv[1]);
		}
	return 0;
	}
#endif

#ifdef MODULE_dospawn
int
dospawn(argc,argv,p)
int argc;
char *argv[];
void *p;
	{
#ifdef JOB_CONTROL
	newsession(Cmdline, SPAWN, 1);
#endif
	if (execve(argv[0], argv, environ) == -1)
		{
		_fputs(_("can't spawn "), stderr);
		perror(argv[0]);
		}
	return 0;
	}
#endif

#ifdef MODULE_doexit
int
doexit(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return -1;
}
#endif

#ifdef MODULE_doshutdown
int
doshutdown(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	int i;
	time_t StopTime;
#ifdef JOB_CONTROL
	struct session_s *sp;
#endif

	StopTime = time(&StopTime);
	main_exit = TRUE;	/* let everyone know we're out of here */
#ifdef JOB_CONTROL
	/* Alert each session task that we're aborting */
	for(i=0;i<SESSION_LIST;i++){
		if((sp = session_list[i]) == NULL || sp->refs != (unsigned char)0)
			continue;
		alert(sp->proc,EABORT);
		alert(sp->proc1,EABORT);
		alert(sp->proc2,EABORT);
	}
#endif
	reset_all();
	if(Dfile_updater != NULL)
		alert(Dfile_updater,0);	/* don't wait for timeout */
	for(i=0;i<100;i++)
		kwait(NULL);	/* Allow tasks to complete */
	shuttrace();
	logmsg(-1, _("NOS was stopped at %s"), ctime(&StopTime));
	if(Logfp){
#ifndef ZILOG
		fclose(Logfp);
#endif
		Logfp = NULL;
	}
	fs_end();
	iostop();
	exit(0);
	return 0;	/* To satisfy lint */
}
#endif

#ifdef MODULE_dohostname
int
dohostname(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	if(argc < 2)
		_printf(_("%s\n"),Hostname);
	else {
		struct iface_s *ifp;
		char *name;

		if((ifp = if_lookup(argv[1])) != NULL){
			if((name = resolve_a(ifp->addr, FALSE)) == NULL){
				_printf(_("Interface address not resolved\n"));
				return 1;
			} else {
				if(Hostname != NULL)
					free(Hostname);
				Hostname = name;

				/* remove trailing dot */
				if ( Hostname[strlen(Hostname)] == '.' ) {
					Hostname[strlen(Hostname)] = '\0';
				}
				_printf(_("Hostname set to %s\n"), name );
			}
		} else {
			if(Hostname != NULL)
				free(Hostname);
			Hostname = strdupw(argv[1]);
		}
	}
	return 0;
}
#endif

#ifdef MODULE_dolog
int
dolog(argc,argv,p)
int argc;
char *argv[];
void *p;
{
#ifndef ZILOG
	static char *logname;
#endif

	if(argc < 2)
		{
#ifdef ZILOG
		if(Logfp)
			{
			_puts(_("Logging on"));
			}
		else
			{
			_puts(_("Logging off"));
			}
#else
		if(Logfp)
			{
			_printf(_("Logging to %s\n"),logname);
			}
		else
			{
			_printf(_("Logging off\n"));
			}
#endif
		return 0;
		}
	if(Logfp)
		{
		logmsg(-1, _("NOS log closed"));
#ifndef ZILOG
		fclose(Logfp);
#endif
		Logfp = NULL;
#ifndef ZILOG
		free(logname);
		logname = NULL;
#endif
		}
	if(_strcmp(argv[1], _("stop")) != 0)
		{
#ifdef ZILOG /* for Hytech, trace output is sent to special SERIAL 0 handle */
/* it must be done this way so that it's accessible from any thread context */
		if(_strcmp(argv[1], _("start")) != 0)
			{
			_puts(_("Usage: log start|stop"));
			return 0;
			}

		Logfp = (FILE *)1;
#else
		logname = strdupw(argv[1]);
		Logfp = fopen(logname,APPEND_TEXT);
#endif
		logmsg(-1, _("NOS was started at %s"), ctime(&StartTime));
		}
	return 0;
}
#endif

#ifdef MODULE_dohelp
int
dohelp(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct cmds *cmdp;
	int i;
	char buf[66];

	_printf(_("Main commands:\n"));
	memset(buf,' ',sizeof(buf));
	buf[64] = '\n';
	buf[65] = '\0';
	for(i=0,cmdp = Cmds;cmdp->name != NULL;cmdp++,i = (i+1)%4){
		_strncpy(&buf[i*16], cmdp->name, _strlen(cmdp->name));
		if(i == 3){
			printf(buf);
			memset(buf,' ',sizeof(buf));
			buf[64] = '\n';
			buf[65] = '\0';
		}
	}
	if(i != 0)
		printf(buf);
	return 0;
}
#endif


/* Attach an interface
 * Syntax: attach <hw type> <I/O address> <vector> <mode> <label> <bufsize> [<speed>]
 */
#ifdef MODULE_doattach
int
doattach(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return subcmd(Attab,argc,argv,p);
}
#endif

/* Manipulate I/O device parameters */
#ifdef MODULE_doparam
int
doparam(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct iface_s *ifp;
	int param;
	int32 val;
	struct escc_s *escc;

	if((ifp = if_lookup(argv[1])) == NULL){
		_printf(_("Interface \"%s\" unknown\n"),argv[1]);
		return 1;
	}
	if(ifp->ioctl == NULL){
		_printf(_("Not supported\n"));
		return 1;
	}
	escc = Esccchan[ifp->dev];
	if(argc < 3){
		for(param=1;param<=16;param++){
			val = (*ifp->ioctl)(escc,param,FALSE,0L);
			if(val != -1)
				_printf(_("%s: %ld\n"),parmname(param),val);
		}
		return 0;
	}
	if((param = devparam(argv[2])) == -1){
		_printf(_("Unknown parameter %s\n"),argv[2]);
		return 1;
	}
	if(argc < 4){
		/* Read specific parameter */
		val = (*ifp->ioctl)(escc,param,FALSE,0L);
		if(val == -1){
			_printf(_("Parameter %s not supported\n"),argv[2]);
		} else {
			_printf(_("%s: %ld\n"),parmname(param),val);
		}
		return 0;
	}
	/* Set parameter */
	(*ifp->ioctl)(escc,param,TRUE,atol(argv[3]));
	return 0;
}
#endif

/* Generate system command packet. Synopsis:
 * remote [-p port#] [-k key] [-a hostname] <hostname> reset|exit|kickme
 */
#ifdef MODULE_doremote
int
doremote(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct sockaddr_in fsock;
	int s,c;
	uint8 *data,x;
	uint16 port,len;
	char *key = NULL;
	int klen;
	int32 addr = 0;
	char *cmd,*host;

	port = IPPORT_REMOTE;	/* Set default */
	optind = 1;		/* reinit getopt() */
	while((c = getopt(argc,argv,"a:p:k:s:")) != EOF){
		switch(c){
		case 'a':
			addr = resolve(optarg);
			break;
		case 'p':
			port = atoi(optarg);
			break;
		case 'k':
			key = optarg;
			klen = strlen(key);
			break;
#ifdef SERVERS /* Nick */
		case 's':
			Rempass = strdupw(optarg);
			return 0;	/* Only set local password */
#endif
		}
	}
	if(optind > argc - 2){
		_printf(_("Insufficient args\n"));
		return -1;
	}
	host = argv[optind++];
	cmd = argv[optind];
	if((s = socket(AF_INET,SOCK_DGRAM,0)) == -1){
		_perror(_("socket failed"));
		return 1;
	}
	len = 1;
	/* Did the user include a password or kickme target? */
	if(addr != 0)
		len += sizeof(int32);

	if(key != NULL)
		len += klen;

	if(len == 1)
		data = &x;
	else
		data = mallocw(len);

	fsock.sin_family = AF_INET;
	fsock.sin_addr.s_addr = resolve(host);
	fsock.sin_port = port;

	switch(cmd[0]){
	case 'r':
		data[0] = SYS_RESET;
		if(key != NULL)
			strncpy((char *)&data[1],key,klen);
		break;
	case 'e':
		data[0] = SYS_EXIT;
		if(key != NULL)
			strncpy((char *)&data[1],key,klen);
		break;
	case 'k':
		data[0] = KICK_ME;
		if(addr != 0)
			put32(&data[1],addr);
		break;
	default:
		_printf(_("Unknown command %s\n"),cmd);
		goto cleanup;
	}
	/* Form the command packet and send it */
	if(sendto(s,data,len,0,(struct sockaddr *)&fsock,sizeof(fsock)) == -1){
		_perror(_("sendto failed"));
		goto cleanup;
	}
cleanup:
	if(data != &x)
		free(data);
	close/*_s*/(s);
	return 0;
}
#endif

/* Execute a command with output piped to more */
#ifdef MODULE_dopage
int
dopage(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	FILE *fp;
	FILE *outsav;

	fp = tmpfile();
	outsav = stdout;
	stdout = fp;
	subcmd(Cmds,argc,argv,p);
	stdout = outsav;
	process_thread_create(daemon_process_p, "view",512,view,0,(void *)fp,NULL,0);	/* View closes fp */
	return 0;
}
#endif

/* Set kernel process debug flag */
#ifdef MODULE_dosysdebug
int
dosysdebug(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	setuchar(&Debug_level, _("system kernel debug"), argc, argv);
	return 0;
}
#endif

/* Set kernel process debug flag */
#ifdef MODULE_dosystrace
int
dosystrace(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	setuchar(&my_thread_p->process_p->p_traceme, _("system call trace"),
			argc, argv);
	return 0;
}
#endif

/* No-op command */
#ifdef MODULE_donothing
int
donothing(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return 0;
}
#endif

/* Log messages of the form
 * Tue Jan 31 00:00:00 1987 44.64.0.7:1003 open FTP
 */
#ifdef MODULE_logmsg
void
logmsg(int s, _char *fmt, ...)
{
	va_list ap;
	char *cp;
	time_t t;
	int i;
	struct sockaddr fsocket;

	if(Logfp == NULL)
		return;

	time(&t);
	cp = ctime(&t);
	rip(cp);
	i = SOCKSIZE;
#ifdef ZILOG
	_dprintf(0, _("%s"), cp);
	if (s >= 0 && getpeername(s, &fsocket, &i) != -1)
		_dprintf(0, _(" %s"), psocket(&fsocket));

	_dprintf(0, _(" - "));
	va_start(ap, fmt);
	_vdprintf(0, fmt, ap);
	va_end(ap);
	_dprintf(0, _("\n"));
#else
	_fprintf(Logfp, _("%s"), cp);
	if(getpeername(s, &fsocket, &i) != -1)
		_fprintf(Logfp, _(" %s"), psocket(&fsocket));

	_fprintf(Logfp, _(" - "));
	va_start(ap, fmt);
	_vfprintf(Logfp, fmt, ap);
	va_end(ap);
	_fprintf(Logfp, _("\n"));
	fflush(Logfp);
#endif

#ifdef MSDOS
	/* MS-DOS doesn't really flush files until they're closed */
	i = fileno(Logfp);
	if((i = dup(i)) != -1)
		{
		close(i);
		}
#endif
}
#endif

#if 0 /*def MODULE_mon_lengths*/
unsigned int mon_lengths [12] =
{
    31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
};
#endif

#if 0 /*def MODULE_dodate*/
int
dodate(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	int i, mon, year, i_state;
	time_t t;
	struct tm tmbuf;
	int32 secs;
	char *cp;

	if(argc < 2)
	{
		/* Show date */
		time(&t);
		cp = ctime(&t);
		rip(cp);
		_printf(_("%s\n"),cp);
		return;
	}
	else if (argc < 4)
	{
		_printf(_("Usage: date <day> <month> <year> [<hour> [<min> [<sec>]]]\n"));
		return;
	}

	/* Set date */
	mon = atoi(argv[2]);
	year = atoi(argv[3]);
	i = mon;
	i--;
	tmbuf.tm_yday = 0;
	while (i--)
		tmbuf.tm_yday += mon_lengths[i];
	tmbuf.tm_yday += atoi(argv[1]) - 1;
	tmbuf.tm_year = year - 1900;

	if (argc > 4)
	{
		tmbuf.tm_hour = atoi(argv[4]);

		if (argc > 5)
		{
			tmbuf.tm_min = atoi(argv[5]);

			if (argc > 6)
			{
				/* hh:mm:ss */
				tmbuf.tm_sec = atoi(argv[6]);
			}
			else
				/* hh:mm:00 */
				tmbuf.tm_sec = 0;
		}
		else
			/* hh:00:00 */
			tmbuf.tm_min = tmbuf.tm_sec = 0;
	}
	else
		/* 00:00:00 */
		tmbuf.tm_hour = tmbuf.tm_min = tmbuf.tm_sec = 0;

	if (mon >= 3 && __isleap(year))
	    /* Take extra day into account for remainder of year */
	    secs = (int32)tmbuf.tm_sec + (int32)(tmbuf.tm_min*60L) +
	           (int32)(tmbuf.tm_hour*3600L) + (int32)(tmbuf.tm_yday*86400L)+
	           (int32)((tmbuf.tm_year-70)*31536000L) +
                   (int32)(((tmbuf.tm_year-68)/4)*86400L) -
	           (int32)(((tmbuf.tm_year)/100)*86400L) +
                   (int32)(((tmbuf.tm_year+300)/400)*86400L);
	else
	    secs = (int32)tmbuf.tm_sec + (int32)(tmbuf.tm_min*60L) +
	           (int32)(tmbuf.tm_hour*3600L) + (int32)(tmbuf.tm_yday*86400L)+
	           (int32)((tmbuf.tm_year-70)*31536000L) +
                   (int32)(((tmbuf.tm_year-69)/4)*86400L) -
	           (int32)(((tmbuf.tm_year-1)/100)*86400L) +
                   (int32)(((tmbuf.tm_year+299)/400)*86400L);

	i_state = dirps();
	Clock = secs;
	restore(i_state);
}
#endif

/* Miscellaneous machine independent utilities
 * Copyright 1991 Phil Karn, KA9Q
 *
 * Apr 07       RPB     Changes needed for intl' of strings
 */

/* Select from an array of strings, or return ascii number if out of range */
#ifdef MODULE_smsg
char *
smsg(msgs,nmsgs,n)
_char *msgs[];
unsigned nmsgs,n;
	{
	static char buf[16];
	char *duptext;

	if(n < nmsgs && msgs[n] != NULL)
		{
		duptext = _strdup(msgs[n]);
		if (duptext == NULL)
			{
			__getfail(msgs[n]);
			return NULL;
			}

		return duptext;
		}

	_sprintf(buf,_("%u"),n);
	return buf;
	}
#endif

/* Convert hex-ascii to integer */
#ifdef MODULE_htoi
int
htoi(s)
char *s;
{
	int i = 0;
	char c;

	while((c = *s++) != '\0')
		{
		if(c == 'x')
			continue;	/* allow 0x notation */
		if('0' <= c && c <= '9')
			i = (i * 16) + (c - '0');
		else if('a' <= c && c <= 'f')
			i = (i * 16) + (c - 'a' + 10);
		else if('A' <= c && c <= 'F')
			i = (i * 16) + (c - 'A' + 10);
		else
			break;
		}
	return i;
	}
#endif

/* Convert single hex-ascii character to binary */
#ifdef MODULE_htob
STATIC int
htob(c)
char c;
	{
	if('0' <= c && c <= '9')
		return c - '0';
	else if('a' <= c && c <= 'f')
		return c - 'a' + 10;
	else if('A' <= c && c <= 'F')
		return c - 'A' + 10;
	else
		return -1;
	}
#endif

/* Read an ascii-encoded hex string, convert to binary and store in
 * output buffer. Return number of bytes converted
 */
#ifdef MODULE_readhex
int
readhex(out,in,size)
uint8 *out;
char *in;
int size;
	{
	int c,count;

	if(in == NULL)
		return 0;
	for(count=0;count < size;count++)
		{
		while(*in == ' ' || *in == '\t')
			in++;	/* Skip white space */
		if((c = htob(*in++)) == -1)
			break;	/* Hit non-hex character */
		out[count] = c << 4;	/* First nybble */
		while(*in == ' ' || *in == '\t')
			in++;	/* Skip white space */
		if((c = htob(*in++)) == -1)
			break;	/* Hit non-hex character */
		out[count] |= c;	/* Second nybble */
		}
	return count;
	}
#endif

/* replace terminating end of line marker(s) with null */
#ifdef MODULE_rip
void
rip(s)
char *s;
	{
	char *cp;

	if((cp = strchr(s,'\n')) != NULL)
		*cp = '\0';
	if((cp = strchr(s,'\r')) != NULL)
		*cp = '\0';
	}
#endif

/* Count the occurrances of 'c' in a buffer */
#ifdef MODULE_memcnt
int
memcnt(buf,c,size)
uint8 *buf;
uint8 c;
int size;
	{
	int cnt = 0;
	uint8 *icp;

	while(size != 0)
		{
		int change;

		if((icp = memchr(buf,c,size)) == NULL)
			break;	/* No more found */
		/* Advance the start of the next search to right after
		 * this character
		 */
		change = (int) (icp - buf + 1);
		buf += change;
		size -= change;
		cnt++;
		}
	return cnt;
	}
#endif

/* XOR block 'b' into block 'a' */
#ifdef MODULE_memxor
void
memxor(a,b,n)
uint8 *a,*b;
unsigned int n;
{
	while(n-- != 0)
		*a++ ^= *b++;
}
#endif

/* Copy a string to a malloc'ed buffer. Turbo C has this one in its
 * library, but it doesn't call mallocw() and can therefore return NULL.
 * NOS uses of strdupw() generally don't check for NULL, so they need this one.
 */
#ifdef MODULE_strdupw
extern int Memwait;			/* Count of tasks waiting for memory */

char *
strdupw(s)
const char *s;
	{
	char *out;
#if 0 /* expand out mallocw() subroutine inline, change failure diagnostic */
	int len;
#endif

	if(s == NULL)
 {
 abyte('|');
		return NULL;
 }
#if 1 /* expand out mallocw() subroutine inline, change failure diagnostic */
	while((out = strdup(s)) == NULL){
		Memwait++;
		/* can't be po'd since _dprintf() re-enters malloc() */
		dprintf(0, "\n%02x:%04x strdupw(0x%04x) ",
				((unsigned char *)&s)[8],
				((unsigned int *)&s)[5], s);
		kwait(&Memwait);
		Memwait--;
	}
#else
	len = strlen((char *)s); /* Nick added cast */
	out = mallocw(len+1);
	/* This is probably a tad faster than strcpy, since we know the len */
	memcpy(out,(char *)s,len); /* Nick added cast */
	out[len] = '\0';
#endif
	return out;
	}
#endif

#ifdef MODULE__strdupw
extern int Memwait;			/* Count of tasks waiting for memory */

char *
_strdupw(s)
_char *s;
	{
	char *out;
	int len;

	if(s == NULL)
 {
 abyte('_');
		return NULL;
 }
	while((out = _strdup(s)) == NULL){
		Memwait++;
		/* can't be po'd since _dprintf() re-enters malloc() */
		dprintf(0, "\n%02x:%04x _strdupw(0x%lx) ",
				((unsigned char *)&s)[10],
				((unsigned int *)&s)[6], s);
		kwait(&Memwait);
		Memwait--;
	}
	return out;
	}
#endif

/* Routines not needed for Turbo 2.0, but available for older libraries */

/* Case-insensitive string comparison */
#ifdef MODULE_strnicmp
int
strnicmp(a,b,n)
char *a,*b;
int n;
	{
	char a1,b1;

	while(n-- != 0 && (a1 = *a++) != '\0' && (b1 = *b++) != '\0')
		{
		if(a1 == b1)
			continue;	/* No need to convert */
		a1 = tolower(a1);
		b1 = tolower(b1);
		if(a1 == b1)
			continue;	/* NOW they match! */
		if(a1 > b1)
			return 1;
		if(a1 < b1)
			return -1;
		}
	return 0;
	}
#endif

#ifdef MODULE_strtok
char *
strtok(s1,s2)
char *s1;	/* Source string (first call) or NULL */
const char *s2;	/* Delimiter string */
	{
	static char *next;
	char *cp;
	char *tmp;

	if(s2 == NULL)
		return NULL;	/* Must give delimiter string */

	if(s1 != NULL)
		next = s1;		/* First call */

	if(next == NULL)
		return NULL;	/* No more */

	/* Find beginning of this token */
	for(cp = next;*cp != '\0' && isdelim(*cp,s2);cp++)
		;

	if(*cp == '\0')
		return NULL;	/* Trailing delimiters, no token */

	/* Save the beginning of this token, and find its end */
	tmp = cp;
	next = NULL;	/* In case we don't find another delim */
	for(;*cp != '\0';cp++){
		if (isdelim(*cp,s2))
			{
			*cp = '\0';
			next = cp + 1;	/* Next call will begin here */
			break;
			}
		}
	return tmp;
	}
#endif

#ifdef MODULE_isdelim
STATIC int
isdelim(c,delim)
char c;
char *delim;
	{
	char d;

	while((d = *delim++) != '\0')
		{
		if(c == d)
			return 1;
		}
	return 0;
	}
#endif

/* Host-network conversion routines, replaced on the x86 with
 * assembler code in pcgen.asm
 */

/* Put a long in host order into a char array in network order */
#ifdef MODULE_put32
uint8 *
put32(cp,x)
uint8 *cp;
int32 x;
	{
	*cp++ = x >> 24;
	*cp++ = x >> 16;
	*cp++ = x >> 8;
	*cp++ = x;
	return cp;
	}
#endif

/* Put a short in host order into a char array in network order */
#ifdef MODULE_put16
uint8 *
put16(cp,x)
uint8 *cp;
uint16 x;
	{
	*cp++ = x >> 8;
	*cp++ = x;
	return cp;
	}
#endif

#ifdef MODULE_get16
uint16
get16(cp)
uint8 *cp;
	{
	uint16 x;

	x = *cp++;
	x <<= 8;
	x |= *cp;
	return x;
	}
#endif

/* Machine-independent, alignment insensitive network-to-host long conversion */
#ifdef MODULE_get32
int32
get32(cp)
uint8 *cp;
	{
	int32 rval;

	rval = *cp++;
	rval <<= 8;
	rval |= *cp++;
	rval <<= 8;
	rval |= *cp++;
	rval <<= 8;
	rval |= *cp;
	return rval;
	}
#endif

/* Compute int(log2(x)) */
#ifdef MODULE_ilog2
int
ilog2(x)
uint16 x;
	{
	int n;

	for (n = 16; n; n--)
		{
		if(x & 0x8000)
			break;
		x <<= 1;
		}
	n--;
	return n;
	}
#endif

