/* This file MUST be compiled with the -zC_TEXT so that it will go
 * into the same code segment with the Borland C library. It
 * references the internal Borland function __vprinter(), which is
 * defined to be NICK_NEAR. It seems that the Borland linker does NOT detect
 * cross-segment calls to NICK_NEAR functions, so omitting this option will
 * cause the executable to crash!
 */

#include "nos/global.h"
#include "nos/stdio.h"
#include <stdarg.h>

int NICK_PASCAL NICK_NEAR __vprinter(
	unsigned NICK_PASCAL NICK_NEAR (*)(void *, unsigned, FILE *),
	FILE *, char *, void NICK__SS *);

static unsigned NICK_PASCAL NICK_NEAR
fputter(void *ptr, unsigned n, FILE *fp)
{
	return fwrite(ptr, 1, n, fp);
}

int
vfprintf(FILE *fp, char *fmt, va_list args)
{
	if(fp == NULL || fp->cookie != _COOKIE)
		return -1;
	return __vprinter(fputter, fp, fmt, (void NICK__SS *)args);
}

