/* General purpose software timer facilities
 * Copyright 1991 Phil Karn, KA9Q
 *
 * Apr 04       RPB     Po-ed most of the remaining strings
 */
#include <stdio.h>
#include <fcntl.h> /* for O_RDONLY */
#include "nos/global.h"
#include "main/timer.h"
#include "main/mbuf.h"
#include "main/commands.h"
#include "main/daemon.h"
#include "nos/hardware.h"
#include "socket/socket.h"
#include "driver/device.h" /* for device_find(), struct device_s */
#include "kernel/dprintf.h"
#include "kernel/process.h"
#include "kernel/thread.h"
#include "kernel/arena.h"
#include "main/main.h"
#include "main/files.h"
#include <libintl.h>
#include "po/messages.h"

/* Head of running timer chain.
 * The list of running timers is sorted in increasing order of expiration;
 * i.e., the first timer to expire is always at the head of the list.
 */
static struct timer *Timers;

static void t_alarm(void *x);

#if 1 /* Nick, eliminate killer task by putting it in timer.c */
extern struct mbuf_s *Killq; /* in kernel/kernel.c */
#endif

#if 1
int main_init(int argc, char **argv); /* in misc.c */
void net_prompt(int argc, void *argv, void *p); /* in misc.c */
#endif

/* Process that handles clock ticks */
void
timerproc(argc,argv,p)
int argc;
void *argv,*p;
{
	register struct timer *t;
	register struct timer *expired;
#if 0
	void (**vf)(void);
	int i_state;
#endif
	int32 clock;
	struct thread_s *thread_p;
	/*struct mbuf_s *bp;*/
	jmp_buf try_b;
#if 1
	unsigned int i;
	struct process_s *process_p;

	/* note that errors in main_init() do not return, just panic() */
	i = main_init(argc, argv);
/* abyte('a'); */

	/* then start a net> prompt, sending in the filename we obtained */
	process_p = process_fork(daemon_process_p, 1);
/* abyte('b'); */
	if (process_p == NULL)
		{
		goto error1;
		}
/* abyte('c'); */
	thread_p = process_thread_create(process_p, "main", 768, net_prompt,
			0, NULL, (i < argc) ? ((char **)argv)[i] : Startup, 0);
/* abyte('d'); */
	process_deref(process_p);
/* abyte('e'); */
	if (thread_p == NULL)
		{
	error1:
		_panic(_("can't fork main process\n"));
		}
/* abyte('f'); */
#endif

	/* start intercepting RTC packets from the touchscreen (ABUS1) */
	if (setjmp(try_b))
		{
		_printf(_("timer: can't open rtc!\n"));
		}
	else
		{
		device_find(try_b, 0x601)->open(try_b, MINOR(0x601));
		}

	for(;;){
/* abyte('A'); */
		/* Atomic read and decrement of Tick */
#if 0 /* there aren't any routines listed in Cfunc[] */
		for(;;){
/* abyte('U'); */
#endif
			while (Killq)
				{
/* abyte('V'); */
				/*bp = dequeue(&Killq);*/
/* abyte('W'); */
				pullup(&Killq/*&bp*/,&thread_p,sizeof(thread_p));
/* abyte('X'); */
				/*free_p(&bp);*/
/* abyte('Y'); */
				if (thread_p != my_thread_p) /* We're immortal */
					{
/* abyte('Z'); */
					killproc(thread_p);
					}
				}

/* abyte('B'); */
#if 0 /* there aren't any routines listed in Cfunc[] */
			i_state = dirps();
			if (Tick) {
/* abyte('!'); */
				break;
			}
#endif
			kwait(&Tick);
#if 0 /* there aren't any routines listed in Cfunc[] */
			restore(i_state);
		}

		do
			{
			Tick--;
			restore(i_state);

/* abyte('D'); */
			/* Call the functions listed in config.c */
			for(vf = Cfunc;*vf != NULL;vf++)
				{
/* abyte('E'); */
				(*vf)();
				}

			i_state = dirps();
			} while (Tick);
		restore(i_state);
#endif

/* abyte('G'); */
		if(Timers == NULL)
			continue;	/* No active timers, all done */

		/* Initialize null expired timer list */
		expired = NULL;
/* abyte('H'); */
		clock = rdclock();

		/* Move expired timers to expired list. Note use of
		 * subtraction and comparison to zero rather than the
		 * more obvious simple comparison; this avoids
		 * problems when the clock count wraps around.
		 */
/* abyte('I'); */
		while(Timers != NULL && (clock - Timers->expiration) >= 0){
/* abyte('J'); */
			if(Timers->next == Timers){
				_panic(_("timer loop at 0x%x\n"), Timers);
			}
			/* Save Timers since stop_timer will change it */
			t = Timers;
/* abyte('K'); */
			stop_timer(t);
/* abyte('L'); */
			t->state = TIMER_EXPIRE;
			/* Add to expired timer list */
			t->next = expired;
			expired = t;
		}
		/* Now go through the list of expired timers, removing each
		 * one and kicking the notify function, if there is one
		 */
/* abyte('M'); */
		while((t = expired) != NULL){
/* abyte('N'); */
			expired = t->next;
			if(t->func){
/* abyte('O'); */
				(*t->func)(t->arg);
/* abyte('o'); */
			}
		}
/* abyte('P'); */
	}
}
/* Start a timer */
void
start_timer(t)
struct timer *t;
{
	register struct timer *tnext;
	struct timer *tprev = NULL;

	if(t == NULL)
		return;
	if(t->state == TIMER_RUN)
		stop_timer(t);
	if(t->duration == 0)
		return;		/* A duration value of 0 disables the timer */

	t->expiration = rdclock() + t->duration;
/* dprintf(0, "t->expiration = %ld\n", t->expiration); */
	t->state = TIMER_RUN;

	/* Find right place on list for this guy. Once again, note use
	 * of subtraction and comparison with zero rather than direct
	 * comparison of expiration times.
	 */
	for(tnext = Timers;tnext != NULL;tprev=tnext,tnext = tnext->next){
		if((tnext->expiration - t->expiration) >= 0)
			break;
	}
	/* At this point, tprev points to the entry that should go right
	 * before us, and tnext points to the entry just after us. Either or
	 * both may be null.
	 */
	if(tprev == NULL)
		Timers = t;		/* Put at beginning */
	else
		tprev->next = t;

	t->next = tnext;
}
/* Stop a timer */
void
stop_timer(timer)
struct timer *timer;
{
	register struct timer *t;
	struct timer *tlast = NULL;

	if(timer == NULL || timer->state != TIMER_RUN)
		return;

	/* Verify that timer is really on list */
	for(t = Timers;t != NULL;tlast = t,t = t->next)
		if(t == timer)
			break;

	if(t == NULL)
		return;		/* Should probably panic here */

	/* Delete from active timer list */
	if(tlast != NULL)
		tlast->next = t->next;
	else
		Timers = t->next;	/* Was first on list */

	t->state = TIMER_STOP;
}
/* Return milliseconds remaining on this timer */
int32
read_timer(t)
struct timer *t;
{
	int32 remaining;

	if(t == NULL || t->state != TIMER_RUN)
		return 0;
	remaining = t->expiration - rdclock();
	if(remaining <= 0)
		return 0;	/* Already expired */
	else
		return remaining * MSPTICK;
}
void
set_timer(t,interval)
struct timer *t;
int32 interval;
{
	if(t == NULL)
		return;
	/* Round the interval up to the next full tick, and then
	 * add another tick to guarantee that the timeout will not
	 * occur before the interval is up. This is necessary because
	 * we're asynchronous with the system clock.
	 */
	if(interval != 0)
 {
		t->duration = 1 + (interval + MSPTICK - 1)/MSPTICK;
/* dprintf(0, "t->duration = %ld\n", t->duration); */
 }
	else
 {
		t->duration = 0;
 }
}
/* Delay process for specified number of milliseconds.
 * Normally returns 0; returns -1 if aborted by alarm.
 */
int
ppause(ms)
int32 ms;
{
	int val;

	if(my_thread_p == NULL || ms == 0)
		return 0;
	kalarm(ms);
	/* The actual event doesn't matter, since we'll be alerted */
	while(my_thread_p->alarm.state == TIMER_RUN){
		if((val = kwait(my_thread_p)) != 0)
			break;
	}
	kalarm(0L); /* Make sure it's stopped, in case we were killed */
	return (val == EALARM) ? 0 : -1;
}
static void
t_alarm(x)
void *x;
{
	alert((struct thread_s *)x,EALARM);
}
/* Send signal to current process after specified number of milliseconds */
void
kalarm(ms)
int32 ms;
{
	if(my_thread_p != NULL){
/* abyte('%'); */
		set_timer(&my_thread_p->alarm,ms);
/* abyte('^'); */
		my_thread_p->alarm.func = t_alarm;
		my_thread_p->alarm.arg = (char *)my_thread_p;
/* abyte('&'); */
		start_timer(&my_thread_p->alarm);
/* abyte('*'); */
	}
}
/* Convert time count in seconds to printable days:hr:min:sec format */
char *
tformat(t)
int32 t;
{
	static char buf[17],*cp;
	unsigned int days,hrs,mins,secs;
	int minus;

/* abyte('8'); */
	if(t < 0){
/* abyte('a'); */
		t = -t;
		minus = 1;
	} else
 {
/* abyte('b'); */
		minus = 0;
 }

/* abyte('c'); */
#if 0
 abyte(' ');
 for (cp = (char *)0x5b9; cp < (char *)0x5ec; cp++)
  {
  ahexb((unsigned int)(unsigned char)*cp);
  abyte(' ');
  }
#endif
	secs = t % 60;
/* abyte('d'); */
	t /= 60;
/* abyte('e'); */
	mins = t % 60;
/* abyte('f'); */
	t /= 60;
/* abyte('g'); */
	hrs = t % 24;
/* abyte('h'); */
	t /= 24;
/* abyte('i'); */
	days = t;
/* abyte('j'); */
	if(minus){
/* abyte('k'); */
		cp = buf+1;
		buf[0] = '-';
	} else
 {
/* abyte('l'); */
		cp = buf;
 }
/* abyte('m'); */
/* _dprintf(0,_("%u:%02u:%02u:%02u"),days,hrs,mins,secs); */
/* abyte('n'); */
	_sprintf(cp,_("%u:%02u:%02u:%02u"),days,hrs,mins,secs);

/* abyte('9'); */
	return buf;
}

