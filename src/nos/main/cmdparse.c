/* Parse command line, set up command arguments Unix-style, and call function.
 * Note: argument is modified (delimiters are overwritten with nulls)
 *
 * Copyright 1991 Phil Karn, KA9Q
 *
 * Improved error handling by Brian Boesch of Stanford University
 * Feb '91 - Bill Simpson
 *		bit16cmd for PPP
 * Mar '91 - Glenn McGregor
 *		handle string escaped sequences
 */
#include <stdio.h>
#ifdef ZILOG
#include <unistd.h> /* for execve() */
#include <sys/wait.h> /* for wait() */
#endif
#include "nos/global.h"
#include "main/cmdparse.h"
#include "kernel/dprintf.h"
#include "kernel/thread.h" /* for struct thread_s */
#include "kernel/process.h" /* for process_fork() etc */
#include <libintl.h>
#include "po/messages.h"

struct boolcmd {
	char *str;	/* Token */
	int val;	/* Value */
};

#define BOOLCMDS_MAX 15
static struct boolcmd Boolcmds[BOOLCMDS_MAX] = {
	"y",		1,	/* Synonyms for "true" */
	"yes",		1,
	"true",		1,
	"on",		1,
	"1",		1,
	"set",		1,
	"enable",	1,

	"n",		0,	/* Synonyms for "false" */
	"no",		0,
	"false",	0,
	"off",		0,
	"0",		0,
	"clear",	0,
	"disable",	0,
	NULL
};

static char *stringparse(char *line);

static char *
stringparse(line)
char *line;
{
	register char *cp = line;
	unsigned long num;

	while ( *line != '\0' && *line != '\"' ) {
		if ( *line == '\\' ) {
			line++;
			switch ( *line++ ) {
			case 'n':
				*cp++ = '\n';
				break;
			case 't':
				*cp++ = '\t';
				break;
			case 'v':
				*cp++ = '\v';
				break;
			case 'b':
				*cp++ = '\b';
				break;
			case 'r':
				*cp++ = '\r';
				break;
			case 'f':
				*cp++ = '\f';
				break;
			case 'a':
				*cp++ = '\a';
				break;
			case '\\':
				*cp++ = '\\';
				break;
			case '\?':
				*cp++ = '\?';
				break;
			case '\'':
				*cp++ = '\'';
				break;
			case '\"':
				*cp++ = '\"';
				break;
			case 'x':
				num = strtoul( --line, &line, 16 );
				*cp++ = (char) num;
				break;
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
				num = strtoul( --line, &line, 8 );
				*cp++ = (char) num;
				break;
			case '\0':
				return NULL;
			default:
				*cp++ = *(line - 1);
				break;
			};
		} else {
			*cp++ = *line++;
		}
	}

	if ( *line == '\"' )
		line++; 	/* skip final quote */
	*cp = '\0';		/* terminate string */
	return line;
}

int
cmdparse(cmds, line, p)
struct cmds cmds[];
register char *line;
void *p;
	{
	struct cmds *cmdp;
	char *argv[NARG + 1],*cp;
	char **pargv;
	int argc,i;
	char *duptext;
	struct process_s *process_p;
	struct thread_s *thread_p;

/* abyte('c'); */
	/* Remove cr/lf */
	rip(line);

	for (argc = 0; argc < (NARG + 1); argc++)
		{
		argv[argc] = NULL;
		}

	for (argc = 0; argc < NARG; )
		{
		register int qflag = FALSE;

		/* Skip leading white space */
		while (*line == ' ' || *line == '\t')
			{
			line++;
			}
		if (*line == '\0')
			{
			break;
			}
		/* return if comment character first non-white */
		if (argc == 0 && *line == '#' )
			{
/* abyte('C'); */
			return 0;
			}
		/* Check for quoted token */
		if (*line == '"')
			{
			line++;	/* Suppress quote */
			qflag = TRUE;
			}
		argv[argc++] = line;	/* Beginning of token */

		if(qflag)
			{
			/* Find terminating delimiter */
			line = stringparse(line);
			if (line == NULL)
				{
/* abyte('C'); */
				return -1;
				}
			}
		else
			{
			/* Find space or tab. If not present,
			 * then we've already found the last
			 * token.
			 */
			if ((cp = strchr(line,' ')) == NULL &&
					(cp = strchr(line,'\t')) == NULL)
				{
				break;
				}
			*cp++ = '\0';
			line = cp;
			}
		}

	if (argc < 1)
		{		/* empty command line */
/* abyte('C'); */
		return 0; /*-1;*/
		}

	/* Look up command in table; prefix matches are OK */
/* abyte('['); */
	for (cmdp = cmds; cmdp->name; cmdp++)
		{
/* abyte('.'); */
		if (_strncmp(argv[0], cmdp->name, strlen(argv[0])) == 0)
			{
			break;
			}
		}
/* abyte(']'); */

	/* for spawn command, cmdp->name == NULL */

	if (argc < cmdp->argcmin)
		{
		/* Insufficient arguments */
		_printf(_("Usage: "));
		_puts(cmdp->argc_errmsg);
/* abyte('C'); */
		return 0; /*-1;*/
		}

	/* in ftp client, rely on -1 return to detect command not in table */
	/* note that this can't happen at net> prompt, as we have spawn cmd */
	if(cmdp->func == NULL)
		{
/* abyte('C'); */
		return -1;
		}

	if(cmdp->stksize == 0)
		{
/* abyte('a'); */
#if 0
		i = (*cmdp->func)(argc,argv,p);
/* abyte('C'); */
		return i;
#else
		return (*cmdp->func)(argc,argv,p);
#endif
		}
	else
		{
/* abyte('b'); */
		/* Make private copy of argv and args,
		 * spawn off subprocess and return.
		 */
		pargv = (char **)callocw(argc + 1, sizeof(char *));
		for (i = 0; i < argc; i++)
			{
			pargv[i] = strdupw(argv[i]);
			}
		pargv[i] = NULL;

		duptext = cmdp->name ?
				_strdup(cmdp->name) : _strdup(_("spawn"));
		if (duptext == NULL)
			{
			__getfail(cmdp->name); /* argv[0] ?? */
			}
		else
			{
#ifdef JOB_CONTROL
			process_p = process_fork(my_thread_p->process_p, 1);
#else
			process_p = process_fork(my_thread_p->process_p, 0);
#endif
			if (process_p == NULL)
				{
				goto error1;
				}
			thread_p = process_thread_create(process_p, duptext,
					cmdp->stksize, cmdp->func,
					argc, pargv, p, 1);
			process_deref(process_p);
			if (thread_p == NULL)
				{
			error1:
				_printf(_("Not enough memory\n"));
				}
#ifndef JOB_CONTROL
 else
  {
  wait(&i);
  /* could return i down below, but don't */
  }
#endif
			free(duptext);
			}

/* abyte('C'); */
		return 0;
		}
	}

/* Call a subcommand based on the first token in an already-parsed line */
int
subcmd(tab,argc,argv,p)
struct cmds tab[];
int argc;
char *argv[];
void *p;
	{
	register struct cmds *cmdp;
	char **pargv;
	int i;
	char *duptext;
	struct process_s *process_p;
	struct thread_s *thread_p;

	/* Strip off first token and pass rest of line to subcommand */
	if (argc < 2)
		{
		if (argc < 1)
			{
			_printf(_("SUBCMD - Don't know what to do?\n"));
			}
		else
			{
			_printf(_("\"%s\" - takes at least one argument\n"),
					argv[0]);
			}
		return 0; /*-1;*/
		}

	argc--;
	argv++;

	for (cmdp = tab; cmdp->name; cmdp++)
		{
		if (_strncmp(argv[0], cmdp->name, strlen(argv[0])) == 0)
			{
			break;
			}
		}

	if (cmdp->name == NULL)
		{
		_printf(_("valid subcommands:"));
		for(cmdp = tab; cmdp->name != NULL; cmdp++)
			{
			_printf(_(" "));
			_printf(cmdp->name);
			}
		_printf(_("\n"));
		return 0; /*-1;*/
		}

	if(argc < cmdp->argcmin)
		{
		if (cmdp->argc_errmsg != NULL)
			{
			_printf(_("Usage: "));
			_puts(cmdp->argc_errmsg);
			}
		return 0; /*-1;*/
		}

	if (cmdp->stksize == 0)
		{
		return (*cmdp->func)(argc,argv,p);
		}
	else
		{
		/* Make private copy of argv and args */
		pargv = (char **)callocw(argc + 1, sizeof(char *));
		for(i = 0; i < argc; i++)
			{
			pargv[i] = strdupw(argv[i]);
			}
		pargv[i] = NULL;

		duptext = _strdup(cmdp->name);
		if (duptext == NULL)
			{
			__getfail(cmdp->name);
			}
		else
			{
#ifdef JOB_CONTROL
			process_p = process_fork(my_thread_p->process_p, 1);
#else
			process_p = process_fork(my_thread_p->process_p, 0);
#endif
			if (process_p == NULL)
				{
				goto error1;
				}
			thread_p = process_thread_create(process_p, duptext,
					cmdp->stksize, cmdp->func,
					argc, pargv, p, 1);
			process_deref(process_p);
			if (thread_p == NULL)
				{
			error1:
				_printf(_("Not enough memory\n"));
				}
#ifndef JOB_CONTROL
 else
  {
  wait(&i);
  /* could return i down below, but don't */
  }
#endif
			free(duptext);
			}

		return 0;
		}
	}

/* Subroutine for setting and displaying boolean flags */
int
setbool(var,label,argc,argv)
int *var;
_char *label;
int argc;
char *argv[];
{
	struct boolcmd *bc;

	if(argc < 2){
#if 1 /* Nick */
		_printf(label);
		_printf(_(": %s\n"),*var ? "on":"off");
#else
		_printf(_("%s: %s\n"),label,*var ? "on":"off");
#endif
		return 0;
	}
	for(bc = Boolcmds;bc->str != NULL;bc++){
#if 1 /* Nick */
		if(stricmp(argv[1],bc->str) == 0){
#else
		if(strcmpi(argv[1],bc->str) == 0){
#endif
			*var = bc->val;
			return 0;
		}
	}
	_printf(_("Valid options:"));
	for(bc = Boolcmds;bc->str != NULL;bc++)
		_printf(_(" %s"),bc->str);

	_printf(_("\n"));
	return 1;
}


/* Subroutine for setting and displaying bit values */
int
bit16cmd(bits,mask,label,argc,argv)
uint16 *bits;
uint16 mask;
_char *label;
int argc;
char *argv[];
{
	int doing = (*bits & mask);
	int result = setbool( &doing, label, argc, argv );

	if ( !result ) {
		if ( doing )
			*bits |= mask;
		else
			*bits &= ~mask;
	}
	return result;
}


/* Subroutine for setting and displaying long variables */
int
setlong(var,label,argc,argv)
long *var;
_char *label;
int argc;
char *argv[];
{
	if(argc < 2)
 {
#if 1 /* Nick */
		_printf(label);
		_printf(_(": %ld\n"), *var);
#else
		_printf(_("%s: %ld\n"),label,*var);
#endif
 }
	else
		*var = atol(argv[1]);

	return 0;
}
/* Subroutine for setting and displaying short variables */
int
setshort(var,label,argc,argv)
unsigned short *var;
_char *label;
int argc;
char *argv[];
{
	if(argc < 2)
 {
#if 1 /* Nick */
		_printf(label);
		_printf(_(": %u\n"), *var);
#else
		_printf(_("%s: %u\n"),label,*var);
#endif
 }
	else
		*var = atoi(argv[1]);

	return 0;
}
/* Subroutine for setting and displaying integer variables */
int
setint(var,label,argc,argv)
int *var;
_char *label;
int argc;
char *argv[];
{
	if(argc < 2)
 {
#if 1 /* Nick */
		_printf(label);
		_printf(_(": %d\n"), *var);
#else
		_printf(_("%s: %i\n"),label,*var);
#endif
 }
	else
		*var = atoi(argv[1]);

	return 0;
}

#if 0 /* doesn't seem to be referenced */
/* Subroutine for setting and displaying unsigned integer variables */
int
setuns(var,label,argc,argv)
unsigned *var;
_char *label;
int argc;
char *argv[];
{
	if(argc < 2)
 {
#if 1 /* Nick */
		_printf(label);
		_printf(_(": %u\n"), *var);
#else
		_printf(_("%s: %u\n"),label,*var);
#endif
 }
	else
		*var = atoi(argv[1]);

	return 0;
}
#endif

/* Nick: Subroutine for setting and displaying unsigned char variables */
int
setuchar(var,label,argc,argv)
unsigned char *var;
_char *label;
int argc;
char *argv[];
{
	if(argc < 2)
 {
#if 1 /* Nick */
		_printf(label);
		_printf(_(": %u\n"), *var);
#else
		_printf(_("%s: %u\n"),label,*var);
#endif
 }
	else
		*var = atoi(argv[1]);

	return 0;
}


