/* main.c by Nick for NOS/UZI project */

#include <stdio.h> /* for FILE */
#include "main/files.h" /* for Startup */
#include "main/daemon.h" /* for timerproc() */
#include "nos/global.h" /* for amess() etc */
#include "main/main.h" /* for daemon_process_p */
#include "kernel/thread.h" /* for process_thread_create(daemon_process_p, ) */
#include "kernel/dprintf.h"
#include "kernel/thread.h"
#include "kernel/process.h" /* for process_fork() */
#include "kernel/arena.h"
#include "kernel/stack.h"
#include <libintl.h>
#include "po/messages.h"

#ifndef MODULE
#define MODULE_main
#endif

#if 0
int main_init(int argc, char **argv); /* in misc.c */
void net_prompt(int argc, void *argv, void *p); /* in misc.c */
#endif

#ifdef MODULE_main
int main(unsigned int argc, unsigned char **argv)
	{
	unsigned int i;
#if 1
	struct arena_s *stack_arena_p;
#else
	struct process_s *process_p;
	struct thread_s *thread_p;
#endif

#ifdef ZILOG
	amess("kernel: parameters:");
	if (argc < 2)
		{
		amess(" none");
		}
	else
		{
		for (i = 1; i < argc; i++)
			{
			abyte(' ');
			amess(argv[i]);
			}
		}
	acrlf();

#if 0 /* temporary */
 amess("performing interrupt flag tests");
 acrlf();

 disable();
 if (istate())
  {
  enable();
  amess("disable() returns istate() = 1");
  acrlf();
  }

 enable();
 if (istate() == 0)
  {
  amess("disable() returns istate() = 0");
  acrlf();
  }

 disable();
 if (dirps())
  {
  enable();
  amess("disable() returns dirps() = 1");
  acrlf();
  }

 enable();
 if (dirps() == 0)
  {
  enable();
  amess("enable() returns dirps() = 0");
  acrlf();
  }

 restore(0);
 if (dirps())
  {
  enable();
  amess("restore(0) returns dirps() = 1");
  acrlf();
  }

 restore(1);
 if (dirps() == 0)
  {
  enable();
  amess("restore(1) returns dirps() = 0");
  acrlf();
  }

 enable();
 amess("completed interrupt flag tests\n\n"));
 acrlf();
#endif

#if 0
 amess("performing setjmp/longjmp tests");
 acrlf();
  {
  jmp_buf buf;
  int val;

  amess("setjmp ");
  val = setjmp(buf);
  ahexw(val);
  abyte(' ');
  if (val < 2)
   {
   amess("longjmp ");
   longjmp(buf, val + 1);
   }
  }
#endif

	acrlf();
#endif

#if 0
	/* create the mother of all threads */
	my_thread_p = thread_new();
	if (my_thread_p == NULL)
		{
		goto error;
		}
	my_thread_p->name = strdup("tick"); /*"main");*/
	if (my_thread_p->name == NULL)
		{
	error:
		_panic(_("can't create tick thread"));
		}
	/* bbr_value and cbar_value are not used inside the kernel */
	my_thread_p->stack = (unsigned short *)&__stack_base;
	my_thread_p->stksize = &__stack_limit - &__stack_base;
	my_thread_p->stack_limit = (unsigned int)&__stack_limit;
	my_thread_p->flags.active = 1;
#endif

	/* create the mother of all processes */
	daemon_process_p = process_new();
	if (daemon_process_p == NULL)
		{
		_panic(_("can't create daemon process"));
		}
	daemon_process_p->p_pid = 0;
	daemon_process_p->status = P_RUNNING;
	/* daemon_process_p->parent_p is null meaning orphaned */
	daemon_process_p->p_cprio = MAXTICKS;
	daemon_process_p->p_umask = 022;
	daemon_process_p->p_exitval = -1; /* will be inherited by all procs */
	my_thread_p->process_p = daemon_process_p;

	/* create the mother of all apartments */
	head_arena_p = arena_new();
	if (head_arena_p == NULL)
		{
		goto error1;
		}
	head_arena_p->size = 14; /* 56 kbytes */
	daemon_process_p->arena_p = head_arena_p;

	/* can now use arena_allocate() function */
	stack_arena_p = arena_allocate(1);
	if (stack_arena_p == NULL)
		{
	error1:
		_panic(_("can't create daemon arenas"));
		}
	stack_set(stack_arena_p, (union store *)KRNL_BA_START,
			(union store *)KRNL_CA1_START - 1);
	stack_set(stack_arena_p, (union store *)KRNL_CA1_START - 1,
			(union store *)(KRNL_BA_START + 1));

	/* note: BBR is already == 0, so the stack arena already appears */
	/* at 0xe000 ... in any case, we update BBR during every longjmp() */
	daemon_process_p->stack_arena_p = stack_arena_p;

#if 0
	arena_ref(stack_arena_p);
	my_thread_p->stack_arena_p = stack_arena_p;
#endif

#if 0 /* this should print "EFFE E001" */
 ahexw(*(unsigned int *)KRNL_BA_START);
 abyte(' ');
 ahexw(*((unsigned int *)KRNL_CA1_START - 1));
 acrlf();
#endif

#if 1
	/* create the mother of all threads */
	my_thread_p = process_thread_create(daemon_process_p, "tick", 768,
			timerproc, argc, argv, NULL, 0);
	if (my_thread_p == NULL)
		{
		_panic(_("can't create tick thread"));
		}

	Rdytab = NULL; /* it was == my_thread_p */
	longjmp(my_thread_p->env, 1);
#else
	/* process command line and initialize devices, this yields a */
	/* FILE * for the startup file specified on the command line */
	/* (if none, the FILE * refers to /etc/startup.net or is NULL) */

	/* note that errors in main_init() do not return, just panic() */
	i = main_init(argc, argv);

	/* then start a net> prompt, sending in the filename we obtained */
	process_p = process_fork(daemon_process_p, 1);
	if (process_p == NULL)
		{
		goto error1;
		}
	thread_p = process_thread_create(process_p, "main", 768, net_prompt,
			0, NULL, (i < argc) ? argv[i] : Startup, 0);
	process_deref(process_p);
	if (thread_p == NULL)
		{
	error1:
		_panic(_("can't fork main process\n"));
		}

	/* finally launch timerproc() so as not to waste main's stack */
	timerproc(0, NULL, NULL);
#endif

	return 0; /* we can never get here, see main_exit() instead */
	}
#endif

