#!/bin/sh
ls ax25/*.mk
grep "$1" ax25/*.mk
ls client/*.mk
grep "$1" client/*.mk
ls crt/*.mk
grep "$1" crt/*.mk
ls driver/*.mk
grep "$1" driver/*.mk
ls dump/*.mk
grep "$1" dump/*.mk
ls filesys/*.mk
grep "$1" filesys/*.mk
ls internet/*.mk
grep "$1" internet/*.mk
ls intl/*.mk
grep "$1" intl/*.mk
ls kernel/*.mk
grep "$1" kernel/*.mk
ls main/*.mk
grep "$1" main/*.mk
ls netrom/*.mk
grep "$1" netrom/*.mk
ls ppp/*.mk
grep "$1" ppp/*.mk
ls server/*.mk
grep "$1" server/*.mk
ls socket/*.mk
grep "$1" socket/*.mk
ls sys/*.mk
grep "$1" sys/*.mk
ls z80/*.mk
grep "$1" z80/*.mk
