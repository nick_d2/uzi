# auto.mk by Nick for Hytech NOS/UZI project

# -----------------------------------------------------------------------------

SUBDIRS=	po client crt driver dump filesys internet kernel ppp \
		server socket sys $(ARCH) iar intl main
#ax25 netrom

OUTPUTS=

driver_SOURCES= \
		po

dump_SOURCES=	po

client_SOURCES= \
		po

filesys_SOURCES= \
		po

internet_SOURCES= \
		po

ppp_SOURCES=	po

server_SOURCES= \
		po

socket_SOURCES= \
		po

$(ARCH)_SOURCES= \
		po

main_SOURCES=	po client crt driver dump internet kernel ppp filesys \
		server socket sys $(ARCH) iar intl
#ax25 netrom

# -----------------------------------------------------------------------------

