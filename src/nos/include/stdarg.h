#ifndef __STDARG_H
#define __STDARG_H

#ifndef __VARARGS_H
#ifdef HI_TECH_C		/* HTC-MSDOS STDARG.H */

typedef void *va_list[1];
#define _STACK_ALIGN	2
#define va_start(ap, parmn)	*ap = (char *)&parmn + ((sizeof(parmn)+_STACK_ALIGN-1)&~(_STACK_ALIGN-1))
#define va_arg(ap, type)	(*(*(type **)ap)++)
#define va_end(ap)

#else				/* STDARG.H (TC2.0) */

typedef void *va_list;
#if 1 /* Nick for IAR convention */
#define __VA_ALIGN 2
#define __VA_SIZEOF(parm) \
	((sizeof(parm) + (__VA_ALIGN - 1)) & ~(__VA_ALIGN - 1))
#define va_start(ap, parm) \
	((ap) = (char *)&(parm) + __VA_SIZEOF(parm))
#define va_arg(ap, type) \
	(*(type *)((*(char **)&(ap) += __VA_SIZEOF(type)) - __VA_SIZEOF(type)))
#define va_copy(dest, src) \
	((dest) = (src))
#define va_end(ap)
#else
#define va_start(ap, p) 	(ap = (char *) (&(p)+1))
#define va_arg(ap, type)	((type *) (((char *)ap) += sizeof(type)))[-1]
#define va_end(ap)
#endif

#endif
#endif /* __VARARGS_H */

#endif /* __STDARG_H */

