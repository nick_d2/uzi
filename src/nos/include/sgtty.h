#ifndef __SGTTY_H
#define __SGTTY_H

#define TIOCGETP 0
#define TIOCSETP 1
#define TIOCSETN  2
/**
#define TIOCEXCL  3
#define TIOCNXCL  4
#define TIOCHPCL  5
**/
#define TIOCFLUSH 6
#define TIOCGETC  7
#define TIOCSETC  8
#define TIOCTLSET 9
#define TIOCTLRES 10

/* Nick's innovations */
#define TIOC_CONNECT 11
#define TIOC_DISCONNECT 12
#define TIOC_NEW_SESSION 13
#define TIOC_FREE_SESSION 14
#define TIOC_WAIT_SESSION 15

struct tioc_new_session
	{
	unsigned int jid;
	unsigned char *name;
	unsigned int type;
	unsigned char makecur;
	};

#define XTABS	0006000
#define UNBUFF	0000100 /* Nick */
#define RAW	0000040
#define CRMOD   0000020
#define CRLF	0000020
#define ECHO	0000010
#define LCASE	0000004
#define CBREAK	0000002
#define COOKED  0000000
#define TNET	0000001 /* Nick (telnet support) */

struct sgttyb {
	char sg_ispeed, sg_ospeed;
	char sg_erase, sg_kill;
	int sg_flags;
};

struct tchars {
	char	t_intrc,t_quit,t_start,t_stop,t_eof;
};

#define stty(fd, s)	(ioctl(fd, TIOCSETP, s))
#define gtty(fd, s)	(ioctl(fd, TIOCGETP, s))

#endif /* __SGTTY_H */
