/*	setjmp.h

	Defines typedef and functions for setjmp/longjmp.

	Copyright (c) Borland International 1987,1988
	All Rights Reserved.
*/
#if 1 /* Nick __STDC__ */
#define _Cdecl
#else
#define _Cdecl	cdecl
#endif

#ifndef _SETJMP
#define _SETJMP
typedef struct {
	unsigned int	j_sp;
	unsigned int	j_bc;
	unsigned int	j_ix;
	unsigned int	j_iy;
#if 1 /* NOTE:  DUE TO REVISED set/longjmp_banked.S, BANKED MODEL ONLY!! */
	unsigned char	j_cs;
	unsigned char	j_dummy; /* saves high byte of exchange bc reg */
	unsigned int	j_ip;
#else
	unsigned int	j_ip;
	unsigned char	j_cs; /* this will be ignored for large model */
#endif
}	jmp_buf[1];

void	_Cdecl longjmp(jmp_buf jmpb, int retval);
int	_Cdecl setjmp(jmp_buf jmpb);
#endif

