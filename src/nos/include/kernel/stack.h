/* stack.h by Nick for NOS/UZI project */

#ifndef __STACK_H
#define __STACK_H

#include <sys/types.h>

#define	ALIGN		int
#define	NALIGN		1

union store {
	union store	*ptr;
	ALIGN		dummy[NALIGN];
	int		calloc;	/* calloc clears an array of integers */
};

union store *stack_get(struct arena_s *arena_p, union store *src);
void stack_set(struct arena_s *arena_p, union store *dest, union store *value);
void stack_allock(struct arena_s *arena_p);
void *stack_malloc(struct arena_s *arena_p, size_t nbytes);
void stack_free(struct arena_s *arena_p, void *ap);
void *stack_realloc(struct arena_s *arena_p, void *ap, size_t nbytes);

#endif

