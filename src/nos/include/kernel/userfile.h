/* userfile.h by Nick for NOS/UZI project */

#ifndef __USERFILE_H
#define __USERFILE_H

#include <setjmp.h>

#define USER_STREAM_LIST 8	/* maximum number of user streams */
#define USER_OBJECT_LIST 16	/* maximum number of user objects */

unsigned int uf_alloc(jmp_buf error);
struct object_s *uf_locate(jmp_buf error, unsigned int fd);
void uf_close(jmp_buf error, unsigned int fd);
size_t uf_read(jmp_buf error, unsigned int fd, void *data, size_t count);
size_t uf_write(jmp_buf error, unsigned int fd, void *data, size_t count);
void uf_fstat(jmp_buf error, unsigned int fd, struct stat *buf);
unsigned int uf_dup(jmp_buf error, unsigned int fd);
void uf_dup2(jmp_buf error, unsigned int fd, unsigned int fd_new);
void uf_ioctl(jmp_buf error, unsigned int fd, unsigned int request,
		void *data);

#endif /* __USERFILE_H */

