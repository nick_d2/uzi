/* session.h by Nick for NOS/UZI project */
/* contains the former session.h by Phil Karn KA9Q */
/* Apr 04	RPB	Changes for intl' of strings */

#ifndef __SESSION_H
#define __SESSION_H

#include <stdio.h>
#ifdef JOB_CONTROL
#include "driver/devtty.h" /* for struct ttystate */
#endif

#ifndef	_GLOBAL_H
#include "nos/global.h"
#endif

#ifndef	_FTPCLI_H
#include "client/ftpcli.h"
#endif

#ifndef	_TELNET_H
#include "client/telnet.h"
#endif

#define SESSION_LIST 20

#define	ALERT_EOF 1

/* Session control structure; only one entry is used at a time */
struct session_s
	{
	unsigned char refs; /* Nick, how many processes share this session */
#ifdef JOB_CONTROL
	unsigned char minor; /* Nick, which tty minor created this session */
#endif

	unsigned char index;
	enum
		{
		TELNET,
		FTP,
		AX25TNC,
		FINGER,
		PING,
		NRSESSION,
		COMMAND,
		VIEW,
		HOP,
		TIP,
		PPPPASS,
		DIAL,
		DQUERY,
		DCLIST,
		ITRACE,
		REPEAT,
		FAX,
		SPAWN /* Nick */
		} type;

	char *name;	/* Name of remote host */
	union
		{
		struct ftpcli *ftp;
		struct telnet *telnet;
		void *p;
		} cb;
	struct thread_s *proc;	/* Primary session process (e.g., tn recv) */
	struct thread_s *proc1;	/* Secondary session process (e.g., tn xmit) */
	struct thread_s *proc2;	/* Tertiary session process (e.g., upload) */
	FILE *network;		/* Primary network socket (control for FTP) */
	FILE *record;		/* Receive record file */
	FILE *upload;		/* Send file */
#ifdef JOB_CONTROL
	struct ttystate ttystate;
#endif
	int (*ctlproc)(int);	/* Upcall  for keyboard ctls */
	int (*inproc)(int);	/* Upcall for normal characters */
	struct session_s *parent;

#ifndef ZILOG
	enum
		{
		SCROLL_INBAND,
		SCROLL_LOCAL
		} scrollmode;	/* Cursor control key mode */
#endif
	};

#define SESTYPES_MAX 19 /* must be SPAWN+1 */
extern _char *Sestypes[SESTYPES_MAX]; /*RPB*/

#ifndef ZILOG
extern long Sfsize;			/* Size of scrollback file, lines */
#endif

extern char *Cmdline;			/* Last typed command line */
extern uint16 Lport;

extern struct session_s *session_list[SESSION_LIST];
extern struct session_s *my_session_p;

struct session_s *session_new(void);
void session_delete(struct session_s *process_p);
void session_ref(struct session_s *process_p);
void session_deref(struct session_s *process_p);
void session_garbage(int red);
void freesession(struct session_s *sp, int promptflag); /* Nick promptflag */
int keywait(char *prompt,int flush);
struct session_s *sessptr(char *cp);
struct session_s *newsession(char *name,int type,int makecur);
void sesflush(void);
void upload(int unused,void *sp1,void *p);

#endif /* __SESSION_H */

