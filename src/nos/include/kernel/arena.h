/* arena.h by Nick for NOS/UZI project */

#ifndef __ARENA_H
#define __ARENA_H

#define ARENA_LIST 20

struct arena_s
	{
	unsigned char refs;	/* how many processes share this arena item */
	struct arena_s *next; /* to item with next higher a_page, or NULL */

	unsigned char page;	/* starting 4 kbyte page, 0..63 */
	unsigned char size;	/* count of 4 kbyte pages, 1..64 */
	};

extern struct arena_s *arena_list[ARENA_LIST];
extern struct arena_s *head_arena_p;

struct arena_s *arena_new(void);
void arena_delete(struct arena_s *arena_p);
void arena_ref(struct arena_s *arena_p);
void arena_deref(struct arena_s *arena_p);
struct arena_s *arena_allocate(unsigned char size);
int arena_resize(struct arena_s *arena_p, unsigned char size);
void arena_garbage(int red);

#endif /* __ARENA_H */

