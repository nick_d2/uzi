/* dprintf.h by Nick for NOS/UZI project */

#ifndef __DPRINTF_H
#define __DPRINTF_H

#include <stdio.h> /* for FILE * (though it's really void *... look into!) */
#include <stdarg.h> /* for va_list */
#include <libintl.h> /* for _char... ZILOG only (Nick) */

/* note: this global variable must be stored in shared memory!! */
extern unsigned char Debug_level;

int panic(const char *format, ...);
int _panic(_char *format, ...);
int dprintf(unsigned char level, const char *format, ...);
int vdprintf(unsigned char level, const char *format, va_list arguments);
unsigned int dputter(void *ptr, unsigned int n, FILE *fp);

#endif /* __DPRINTF_H */

