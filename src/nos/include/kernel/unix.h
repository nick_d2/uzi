/* unix.h by Nick for NOS/UZI project
 *
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/* scall server, currently only for calls originating inside the kernel */

#include <stdarg.h>

/* defined in z80/unix_vec.S */
void unix_ret(unsigned int value);
void unix_run(void);

#define __UNIX___VECTOR 55

extern void (*__unix___vector[__UNIX___VECTOR])(jmp_buf error, va_list arg_p);

extern unsigned int unix_tick; /* must be initialized to zero */
extern unsigned int unix_ticks; /* must be initialized to zero */

unsigned int __unix(unsigned int request, ...);
void __unix_sync(jmp_buf error, va_list arg_p);
void __unix_utime(jmp_buf error, va_list arg_p);
void __unix_link(jmp_buf error, va_list arg_p);
void __unix_symlink(jmp_buf error, va_list arg_p);
void __unix_unlink(jmp_buf error, va_list arg_p);
void __unix_chdir(jmp_buf error, va_list arg_p);
void __unix_chroot(jmp_buf error, va_list arg_p);
void __unix_mknod(jmp_buf error, va_list arg_p);
void __unix_access(jmp_buf error, va_list arg_p);
void __unix_chmod(jmp_buf error, va_list arg_p);
void __unix_chown(jmp_buf error, va_list arg_p);
void __unix_stat(jmp_buf error, va_list arg_p);
void __unix_mount(jmp_buf error, va_list arg_p);
void __unix_umount(jmp_buf error, va_list arg_p);
void __unix_execve(jmp_buf error, va_list arg_p);
void __unix_open(jmp_buf error, va_list arg_p);
void __unix_close(jmp_buf error, va_list arg_p);
void __unix_read(jmp_buf error, va_list arg_p);
void __unix_write(jmp_buf error, va_list arg_p);
void __unix_lseek(jmp_buf error, va_list arg_p);
void __unix_fstat(jmp_buf error, va_list arg_p);
void __unix_falign(jmp_buf error, va_list arg_p);
void __unix_dup(jmp_buf error, va_list arg_p);
void __unix_dup2(jmp_buf error, va_list arg_p);
void __unix_ioctl(jmp_buf error, va_list arg_p);
void __unix_pipe(jmp_buf error, va_list arg_p);
void __unix_time(jmp_buf error, va_list arg_p);
void __unix_stime(jmp_buf error, va_list arg_p);
void __unix_getfsys(jmp_buf error, va_list arg_p);
void __unix_times(jmp_buf error, va_list arg_p);
void __unix_brk(jmp_buf error, va_list arg_p);
void __unix_sbrk(jmp_buf error, va_list arg_p);
void __unix_waitpid(jmp_buf error, va_list arg_p);
void __unix__exit(jmp_buf error, va_list arg_p);
void __unix_pause(jmp_buf error, va_list arg_p);
void __unix_signal(jmp_buf error, va_list arg_p);
void __unix_kill(jmp_buf error, va_list arg_p);
void __unix_alarm(jmp_buf error, va_list arg_p);
void __unix_getset(jmp_buf error, va_list arg_p);
void __unix_reboot(jmp_buf error, va_list arg_p);
void __unix_fork(jmp_buf error, va_list arg_p);
void __unix_socket(jmp_buf error, va_list arg_p);
void __unix_bind(jmp_buf error, va_list arg_p);
void __unix_listen(jmp_buf error, va_list arg_p);
void __unix_connect(jmp_buf error, va_list arg_p);
void __unix_accept(jmp_buf error, va_list arg_p);
void __unix_shutdown(jmp_buf error, va_list arg_p);
void __unix_socketpair(jmp_buf error, va_list arg_p);
void __unix_recv(jmp_buf error, va_list arg_p);
void __unix_recvfrom(jmp_buf error, va_list arg_p);
void __unix_send(jmp_buf error, va_list arg_p);
void __unix_sendto(jmp_buf error, va_list arg_p);
void __unix_getsockname(jmp_buf error, va_list arg_p);
void __unix_getpeername(jmp_buf error, va_list arg_p);

