/* fork.h by Nick for NOS/UZI project */

#ifndef __FORK_H
#define __FORK_H

#include <setjmp.h>

void fork_parent(jmp_buf error, unsigned int *pid_p);

#endif /* __FORK_H */

