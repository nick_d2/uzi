/* strace.h by Nick for NOS/UZI project */

#ifndef __STRACE_H
#define __STRACE_H

#include <stdarg.h>

#define TYPE_BIN 1
#define TYPE_STR 2
#define TYPE_OCT 3
#define TYPE_DEC 4
#define TYPE_HEX 5
#define TYPE_LNG 6

void strace_entry(unsigned int request, va_list arg_p);
void strace_exit(unsigned int request, va_list arg_p);
void strace_bomb(unsigned int request, unsigned int error);

#endif

