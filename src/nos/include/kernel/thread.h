/* thread.h by Nick for NOS/UZI project */
/* contains the former proc.h by Phil Karn KA9Q */

#ifndef __THREAD_H
#define __THREAD_H

#include <setjmp.h>
#include <signal.h> /* for NSIGS */
#include "main/timer.h" /* for struct timer */
#include "kernel/process.h" /* for struct process_s (please remove this later) */

#define THREAD_LIST 20

#define	SIGNAL_HASH 16		/* Number of wait table hash chains */
#define phash(event) ((unsigned int)event & (SIGNAL_HASH - 1))

/* Prepare for an exception signal and return 0. If after this macro
 * is executed any other process executes alert(thread_p, val), this will
 * invoke the exception and cause this macro to return a second time,
 * but with the return value 1. This cannot be a function since the stack
 * frame current at the time setjmp is called must still be current
 * at the time the signal is taken. Note use of comma operators to return
 * the value of setjmp as the overall macro expression value.
 */
#define	SETSIG(val) (my_thread_p->flags.sset=1, \
	my_thread_p->signo = (val), setjmp(my_thread_p->sig))

#define	stdin ((FILE *)my_thread_p->process_p->stream_p[0])
#define	stdout ((FILE *)my_thread_p->process_p->stream_p[1])
#define	stderr ((FILE *)my_thread_p->process_p->stream_p[2])

/* Kernel process control block */
struct thread_s
	{
	unsigned int c_sp;		/* must be at struct+0 for unixrun() */
	unsigned int c_cs;		/* must be at struct+4 for unixrun() */
	unsigned int stack_limit;	/* must be at struct+6 for unixrun() */
	unsigned char bbr_value;	/* must be at struct+8 for unixrun() */
	unsigned char cbar_value;	/* must be at struct+9 for unixrun() */

	struct process_s *process_p;

	struct thread_s *prev;	/* Process table pointers */
	struct thread_s *next;

	struct {
		unsigned int suspend:1;		/* Process is suspended */
		unsigned int waiting:1;		/* Process is waiting */
		unsigned int istate:1;		/* Process has interrupts enabled */
		unsigned int sset:1;		/* Process has set sig */
		unsigned int freeargs:1;	/* Free args on termination */
		unsigned int active:1;		/* ttab entry active (Nick) */
	} flags;
	jmp_buf env;		/* Process register state */
	jmp_buf sig;		/* State for alert signal */
	int signo;		/* Arg to alert to cause signal */
	void *event;		/* Wait event */
	uint16 *stack;		/* Process stack */
	unsigned stksize;	/* Size of same */
	char *name;		/* Arbitrary user-assigned name */
	int retval;		/* Return value from next kwait() */
	struct timer alarm;	/* Alarm clock timer */
	int iarg;		/* Copy of iarg */
	void *parg1;		/* Copy of parg1 */
	void *parg2;		/* Copy of parg2 */
	void (*startp)(int i, void * p, void *u); /* Copy of initial pc */
	int error;		/* Process' private errno */
	unsigned char sysio;	/* for usrget/usrput, was udata_p->u_sysio */
	struct arena_s *stack_arena_p;	/* small arena appearing at 0xe000 */
	};

extern struct thread_s *my_thread_p; 		/* Currently running thread */
extern struct thread_s *Rdytab;			/* Processes ready to run */
extern struct thread_s *Waittab[SIGNAL_HASH];	/* Waiting process list */
#if 0 /* out for now */
extern struct thread_s *Susptab;		/* Suspended processes */
#endif
extern struct thread_s *thread_list[THREAD_LIST];

#if 0 /* out for now */
extern int Stkchk;		/* Stack checking flag */
extern int Kdebug;		/* Control display of current task on screen */
#endif

struct thread_s *thread_new(void);
void thread_delete(struct thread_s *thread_p);
void thread_garbage(int red);

/* In kernel.c: */
void kinit(void);
void psetup(struct thread_s *thread_p);
void alert(struct thread_s *thread_p, int val);
void chname(struct thread_s *thread_p, char *newname);
void killproc(struct thread_s *thread_p);
void killself(void);
void mainproc(void);
void ksignal(void *event, int n);
void ksig(void *event, int n);
int kwait(void *event);
int setsig(int val);
#if 0 /* out for now */
void suspend(struct thread_s *thread_p);
void resume(struct thread_s *thread_p);
#endif
void delproc(struct thread_s *thread_p);
void addproc(struct thread_s *thread_p);

#endif /* __THREAD_H */

