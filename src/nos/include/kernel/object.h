/* object.h by Nick for NOS/UZI project */

#ifndef __OBJECT_H
#define __OBJECT_H

struct object_s
	{
	unsigned char type;	/* _FL_FILE or _FL_SOCK */
	unsigned char refs;	/* count of references to object */
	unsigned char busy;	/* acquired flag */
	};

void object_acquire(struct object_s *object_p);
void object_release(struct object_s *object_p);
void object_inc_refs(struct object_s *object_p);
unsigned char object_dec_refs(struct object_s *object_p);

#endif /* __OBJECT_H */

