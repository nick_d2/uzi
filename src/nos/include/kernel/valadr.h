/* valadr.h by Nick for NOS/UZI project */

#ifndef __VALADR_H
#define __VALADR_H

#include <setjmp.h>

void valadr(jmp_buf error, void *base, size_t size);

#endif /* __VALADR_H */

