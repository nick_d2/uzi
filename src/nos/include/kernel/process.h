/* process.h by Nick for NOS/UZI project */

#ifndef __PROCESS_H
#define __PROCESS_H

#include <setjmp.h>
#include <signal.h> /* for NSIGS */
#include <sys/types.h>
#include "kernel/arena.h"
/*#include "kernel/session.h"*/
#include "kernel/userfile.h" /* for USER_STREAM_LIST, USER_OBJECT_LIST */
#include "driver/bufpool.h" /* for dev_t */
#include "driver/rtc.h" /* for rtctime_t */

#define PROCESS_LIST 20

#define PRIO_MAX	19
#define PRIO_MIN	-20

#define TICKSPERSEC 50   /* Ticks per second */
#define MAXTICKS     4   /* Max ticks before swapping out (time slice)
                            default process time slice */
#if 0 /* temporary */
#define MAXBACK      3   /* Process time slice for tasks not connected
                            to the current tty */
#define MAXBACK2     2   /* Process time slice for background tasks */
#define MAXINTER     5   /* Process time slice for interactive tasks */
#endif

/* note: WNOHANG / WUNTRACED are provided by sys/wait.h */

#define P_EMPTY 0	/* Unused slot */
#define P_ZOMBIE 1	/* Exited. */
#define P_FORKING 2	/* In process of forking; do not mess with */
#define P_RUNNING 3	/* Currently running process */
#define P_READY 4	/* Runnable process */
#define P_SLEEP	5	/* Sleeping; can be awakened by signal */
#define P_XSLEEP 6	/* Sleeping, don't wake up for signal */
#define P_PAUSE 7	/* Sleeping for pause(); can wakeup for signal */
#define P_WAIT 8	/* Executed a wait() */
/*#define P_STOPED 9*/	/* Process is stoped */

/* structure of a unix process (nos processes are akin to threads) */
struct process_s
	{
	unsigned char refs;	/* how many threads share this process entry */
	unsigned char status;	/* Process status */

	struct arena_s *arena_p; /* normally one per process unless vforked */
	struct process_s *parent_p; /* Process parent's table entry */
	struct session_s *session_p; /* similar to notion of controlling tty */

	struct arena_s *stack_arena_p;	/* small arena appearing at 0xe000 */
	char p_tty;		/* Process' controlling tty minor # */

	unsigned int p_pid;		/* Process ID */
	unsigned int p_uid;

	unsigned int p_alarm;	/* Seconds until alarm goes off */
	unsigned int p_exitval;	/* Exit value */

/* Everything below here is overlaid by time info at exit */
	unsigned long p_zombie[2]; /* Don't allow sharing of mem after all!! */

	unsigned char p_cprio;	/* Process current priority */
	signed char p_nice;	/* Process nice value (-20 to 19) */

	unsigned short p_pending; /* Pending signals */
	unsigned short p_ignored; /* Ignored signals */

	unsigned char *p_break;	/* process break level */

/* everything below here has been moved from udata into the kernel_process */
	unsigned int p_gid;
	unsigned int p_euid;
	unsigned int p_egid;
	unsigned int p_umask;		/* umask: file creation mode mask */

	rtctime_t p_time;	/* Start time */

	struct core_inode_s *p_cwd;	/* Index into inode table of cwd. */
	struct core_inode_s *p_root;	/* Index into inode table of chroot target */

	int (*p_sigvec[NSIGS])(); /* Array of signal vectors */

	unsigned long p_utime;	/* Elapsed ticks in user mode */
	unsigned long p_stime;	/* Ticks in system mode */
	unsigned long p_cutime;	/* Total childrens ticks */
	unsigned long p_cstime;

	unsigned char p_traceme; /* added by Nick, see kernel/strace.c */

	struct object_s *stream_p[USER_STREAM_LIST]; /* process stream table */
	struct object_s *object_p[USER_OBJECT_LIST]; /* process object table */

	int p_cursig;		/* Signal currently being caught */
	};

#define UZIX "NOS/UZI"
#define VERSION Version
#define RELEASE "RELEASE"

#define HOST Hostname
#define MACHINE "MACHINE"

struct kdata_s
	{
	unsigned char k_name[14];	/* OS name */
	unsigned char k_version[8];	/* OS version */
	unsigned char k_release[8];	/* OS release */

	unsigned char k_host[14];	/* Host name */
	unsigned char k_machine[8];	/* Host machine */

	unsigned int k_tmem; 	/* System memory, in kbytes */
	unsigned int k_kmem; 	/* Kernel memory, in kbytes */
	};

/* process_getset() commands */
#define	GET_PID		0	/* get process id */
#define	GET_PPID	1	/* get parent process id */
#define	GET_UID		2	/* get user id */
#define	SET_UID		3	/* set user id */
#define	GET_EUID	4	/* get effective user id */
#define	GET_GID		5	/* get group id */
#define	SET_GID		6	/* set group id */
#define	GET_EGID	7	/* get effective group id */
#define	GET_PRIO	8	/* get process priority */
#define	SET_PRIO	9	/* set process priority */
#define	SET_UMASK	10	/* get/set umask */
#define	SET_TRACE	11	/* set trace flag */
#define	SET_DEBUG	12	/* set debug flag */

extern struct process_s *process_list[PROCESS_LIST];

/* Super() returns true if we are the superuser */
#define super() (my_thread_p->process_p->p_euid == 0)

struct process_s *process_new(void);
void process_delete(struct process_s *process_p);
void process_ref(struct process_s *process_p);
void process_deref(struct process_s *process_p);
struct process_s *process_find(jmp_buf error, unsigned int pid);
struct process_s *process_fork(struct process_s *parent_process_p,
		unsigned char daemon);
void process_garbage(int red);
void process_getfsys(jmp_buf error, dev_t devno, void *data);
void process_times(jmp_buf error, struct tms *buf);
void process_brk(jmp_buf error, void *addr);
void *process_sbrk(jmp_buf error, size_t incr);
unsigned int process_waitpid(jmp_buf error, unsigned int pid,
		unsigned int *statloc, unsigned int flag);
void process__exit(unsigned int val);
void process_pause(jmp_buf error);
unsigned long process_signal(jmp_buf error, signal_t sig, unsigned long func);
void process_kill(jmp_buf error, unsigned int pid, signal_t sig);
unsigned int process_alarm(unsigned int secs);
unsigned int process_getset(jmp_buf error, unsigned int what,
		unsigned int parm1, unsigned int parm2);
void process_reboot(jmp_buf error, unsigned int parm1, unsigned int parm2);
int process_fclose(struct process_s *process_p, struct object_s *object_p);
struct thread_s *process_thread_create(struct process_s *process_p,
		unsigned char *name, unsigned int stksize,
		void (*pc)(int argc, void *argv, void *p),
		int iarg, void *parg1, void *parg2, unsigned char freeargs);

#endif /* __PROCESS_H */

