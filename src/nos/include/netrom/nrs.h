#ifndef	_NRS_H
#define	_NRS_H

#ifndef	_GLOBAL_H
#include "nos/global.h"
#endif

#ifndef	_MBUF_H
#include "main/mbuf.h"
#endif

#ifndef	_IFACE_H
#include "main/iface.h"
#endif

#define NRS_MAX 5		/* Maximum number of Nrs channels */

/* SLIP definitions */
#define	NRS_ALLOC	40	/* Receiver allocation increment */

#define STX	0x02		/* frame start */
#define ETX 0x03		/* frame end */
#define DLE	0x10		/* data link escape */
#define NUL 0x0			/* null character */

/* packet unstuffing state machine */
#define NRS_INTER	0		/* in between packets */
#define NRS_INPACK	1		/* we've seen STX, and are in a the packet */
#define NRS_ESCAPE	2		/* we've seen a DLE while in NRS_INPACK */
#define NRS_CSUM	3		/* we've seen an ETX, and are waiting for the checksum */

/* net/rom serial protocol control structure */
struct nrs {
	uint8 state;		/* Receiver State control flag */
	unsigned char csum;	/* Accumulating checksum */
	struct mbuf_s *rbp;	/* Head of mbuf chain being filled */
	struct mbuf_s *rbp1;	/* Pointer to mbuf currently being written */
	uint8 *rcp;		/* Write pointer */
	uint16 rcnt;		/* Length of mbuf chain */
	struct mbuf_s *tbp;	/* Transmit mbuf being sent */
	long errors;		/* Checksum errors detected */
	long packets;		/* Number of packets received successfully */
	struct iface_s *iface;	/* Associated interface structure */
#if 0
	int (*send)(struct asy *,struct mbuf_s **);/* Routine to send mbufs */
	unsigned int (*getc)(struct asy *);/* Routine to fetch input chars */
#endif
};

extern struct nrs Nrs[];
/* In nrs.c: */
int nrs_free(struct iface_s *ifp);
int nrs_init(struct iface_s *ifp);
int nrs_raw(struct iface_s *iface,struct mbuf_s **bpp);
void nrs_recv(int dev,void *v1,void *v2);

#endif	/* _NRS_H */
