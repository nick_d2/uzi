#ifndef	_KISS_H
#define	_KISS_H

#ifndef	_MBUF_H
#include "main/mbuf.h"
#endif

#ifndef	_IFACE_H
#include "main/iface.h"
#endif

/* In kiss.c: */
int kiss_free(struct iface_s *ifp);
int kiss_raw(struct iface_s *iface,struct mbuf_s **data);
void kiss_recv(struct iface_s *iface,struct mbuf_s **bp);
int kiss_init(struct iface_s *ifp);
int32 kiss_ioctl(struct iface_s *iface,int cmd,int set,int32 val);
void kiss_recv(struct iface_s *iface,struct mbuf_s **bp);

#endif	/* _KISS_H */
