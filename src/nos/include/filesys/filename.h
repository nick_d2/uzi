/* filename.h by Nick for NOS/UZI project */

#ifndef __FILENAME_H
#define __FILENAME_H

#include <setjmp.h>
#include "filesys/cinode.h" /* for struct core_inode_s */

struct core_inode_s *_namei(jmp_buf error, unsigned char *name, unsigned char **rest,
		struct core_inode_s *strt, struct core_inode_s **parent);
struct core_inode_s *namei(jmp_buf error, unsigned char *uname, struct core_inode_s **parent,
		unsigned char follow);
struct core_inode_s *srch_dir(jmp_buf error, struct core_inode_s *wd, unsigned char *name);
struct core_inode_s *srch_mt(jmp_buf error, struct core_inode_s *ino);
void ch_link(jmp_buf error, struct core_inode_s *wd, unsigned char *oldname,
		unsigned char *newname, struct core_inode_s *nindex);
void filename(unsigned char *upath, unsigned char *name);
int namecomp(unsigned char *n1, unsigned char *n2);
struct core_inode_s *newfile(jmp_buf error, struct core_inode_s *pino, unsigned char *name);
void chany(jmp_buf error, unsigned char *path, unsigned int val1,
		unsigned int val2, unsigned char mode);
struct core_inode_s *n_creat(jmp_buf error, unsigned char *name, unsigned char new,
		mode_t mode);

#endif /* __FILENAME_H */

