/* execve.h by Nick for NOS/UZI project */

#ifndef __EXECVE_H
#define __EXECVE_H

#include <setjmp.h>
#include <sys/types.h>

/* Struct to hold arguments temporarily in execve */
typedef struct
	{
	int a_argc;
	int a_arglen;
	int a_envc;
	char a_buf[512-3*sizeof(int)];
	} argblk_t;

#define E_MAGIC 0xa6c9

#define E_FORMAT_LARGE 1
#define E_FORMAT_BANKED 2
#define E_FORMAT_KERNEL 3

typedef struct
	{
	unsigned short e_magic;
	unsigned short e_format;
	off_t e_size;
	unsigned short e_hsize;
	unsigned short e_idata;
	unsigned short e_entry;
	unsigned short e_udata;
	unsigned short e_stack;
	unsigned short e_break;
	} exehdr_t;

void fs_execve(jmp_buf error, unsigned char *path, unsigned char **argv,
		unsigned char **envp);
int wargs(char **argv, argblk_t *argbuf);
char *rargs(char *ptr, argblk_t *argbuf, int *cnt);

#endif /* __EXECVE_H */

