/* bitmap.h by Nick for NOS/UZI project */

#ifndef __BITMAP_H
#define __BITMAP_H

#include <setjmp.h>
#include <libintl.h> /* for _char */
#include "driver/bufpool.h" /* for dev_t, blkno_t */

extern _char *Badinomsg;

/* note: SUPERBLOCK is provided by z80/asm.h */
/* note: (D)(IN)DIRECTBLOCKS and TOTALREFBLOCKS are provided by z80/asm.h */

#define blockinodes(blk)	((blk) << DINODESPERBLOCKLOG)
#define devinodes(dev)		blockinodes((dev)->s_isize)
#define inodeblock(dev,ino)	(((ino) >> DINODESPERBLOCKLOG) + \
					(dev)->s_reserv)
#define inodeoffset(ino)	((ino) & (DINODESPERBLOCK-1))

ino_t i_alloc(jmp_buf error, dev_t devno);
void i_free(jmp_buf error, dev_t devno, ino_t ino);
blkno_t blk_alloc(jmp_buf error, dev_t devno, unsigned char dirty);
void blk_free(jmp_buf error, dev_t devno, blkno_t blk);
void validblk(dev_t devno, blkno_t num);
blkno_t bitmap_search(jmp_buf error, dev_t devno, int size,
		blkno_t start, blkno_t final);
void bitmap_reserve(jmp_buf error, dev_t devno, blkno_t blk,
		unsigned char size, unsigned char flag,
		blkno_t start, blkno_t final);
blkno_t bitmap_find(jmp_buf error, dev_t devno, blkno_t blk,
		unsigned char flag, unsigned char toggle,
		blkno_t start, blkno_t final);
unsigned char bitmap_get(jmp_buf error, dev_t devno, blkno_t blk,
		blkno_t start, blkno_t final);
unsigned char bitmap_set(jmp_buf error, dev_t devno, blkno_t blk,
		unsigned char flag, blkno_t start, blkno_t final);

#endif /* __BITMAP_H */

