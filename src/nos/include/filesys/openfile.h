/* openfile.h by Nick for NOS/UZI project */

#ifndef __OPENFILE_H
#define __OPENFILE_H

#include <setjmp.h>
#include <sys/types.h> /* for off_t */
#include "filesys/cinode.h" /* for struct core_inode_s */
#include "kernel/object.h" /* for struct object_s */

#define OPEN_FILE_LIST 16	/* maximum number of open files */

/* structure of open file table entry */
struct open_file_s
	{
	struct object_s object;	/* header with type, refs, busy */
	unsigned char o_access;	/* O_RDONLY, O_WRONLY, or O_RDWR */
	unsigned long o_ptr;		/* File position pointer */
	struct core_inode_s *o_inode;	/* Pointer into in-core inode table */
	};

extern struct open_file_s *open_file_list[OPEN_FILE_LIST];

struct open_file_s *of_alloc(jmp_buf error);
void of_ref(struct open_file_s *of_p);
void of_deref(struct open_file_s *of_p);
void of_truncate(struct open_file_s *of_p);
void of_garbage(int red);
struct open_file_s *of_locate(jmp_buf error, unsigned int fd);
unsigned int of_open(jmp_buf error, unsigned char *path, unsigned int flag,
		mode_t mode);
unsigned long of_lseek(jmp_buf error, unsigned int fd, unsigned long position,
		unsigned int flag);
void of_falign(jmp_buf error, unsigned int fd, unsigned int flag);
void of_pipe(jmp_buf error, unsigned int *fd_p);

#endif /* __OPENFILE_H */

