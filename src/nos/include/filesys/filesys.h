/* filesys.h by Nick for NOS/UZI project */

#ifndef __FILESYS_H
#define __FILESYS_H

#include <setjmp.h>
#include <libintl.h> /* for _char */
#include "driver/bufpool.h" /* for dev_t, blkno_t */
#include "filesys/cinode.h" /* for struct core_inode_s */
#include "driver/rtc.h" /* for rtctime_t */

#define FILE_SYSTEM_LIST 2	/* maximum number of filesystems */
#define SMOUNTED 12742	/* magic number to flag mounted filesystem (on disk) */

/* note: SUPERBLOCK is provided by z80/asm.h */

#if 0 /* remove support for the original UZI filesystem */
#define FSFREEBLOCKS	50
#define FSFREEINODES	50
#endif

/* device-resident super-block */
struct file_system_s
	{
	unsigned short s_mounted; /* signature */
	unsigned short s_reserv; /* # of first block of inodes */
	unsigned short s_isize;	/* # of inode's blocks */
	unsigned short s_fsize;	/* # of data's blocks */

	blkno_t s_tfree;	/* total free blocks */

	ino_t s_tinode;		/* total free inodes */

	rtctime_t s_time; 	/* last modification timestamp */

	blkno_t s_bitmap_inode;	/* disk byte position of free inode bitmap */
	blkno_t s_bitmap_block;	/* disk byte position of free block bitmap */
	blkno_t s_bitmap_immov;	/* disk byte position of immoveable bitmap */
	blkno_t s_bitmap_final;	/* byte position just after end of bitmaps */

/* Core-resident part */
	unsigned char s_fmod; 	/* filesystem modified */
	unsigned char s_ronly;	/* readonly filesystem */
	struct core_inode_s *s_mntpt;	/* mount point inode */
	dev_t s_dev;		/* device on which filesystem resides */
	};

extern dev_t Root_dev;		/* device_list number of root filesystem. */
extern struct core_inode_s *Root_ino;	/* Address of root dir in inode table */

extern struct file_system_s *file_system_list[FILE_SYSTEM_LIST];

extern _char *Baddevmsg;

void fs_init(dev_t boot_dev);
void fs_end(void);
struct file_system_s *findfs(dev_t devno);
struct file_system_s *getfs(dev_t devno);
void fmount(jmp_buf error, dev_t dev, struct core_inode_s *ino, unsigned int flag);
void fsync(dev_t dev);
void fs_sync(jmp_buf error);
void fs_utime(jmp_buf error, unsigned char *path, struct utimbuf *buf);
void fs_link(jmp_buf error, unsigned char *path, unsigned char *path_new);
void fs_symlink(jmp_buf error, unsigned char *path, unsigned char *path_new);
void fs_unlink(jmp_buf error, unsigned char *path);
void fs_chdir(jmp_buf error, unsigned char *path);
void fs_chroot(jmp_buf error, unsigned char *path);
void fs_mknod(jmp_buf error, unsigned char *path, mode_t mode, dev_t dev);
void fs_access(jmp_buf error, unsigned char *path, unsigned int mode);
void fs_chmod(jmp_buf error, unsigned char *path, mode_t mode);
void fs_chown(jmp_buf error, unsigned char *path, unsigned int owner,
		unsigned int group);
void fs_stat(jmp_buf error, unsigned char *path, struct stat *buf);
void fs_mount(jmp_buf error, unsigned char *path, unsigned char *path_dir,
		unsigned int flag);
void fs_umount(jmp_buf error, unsigned char *path);

#endif /* __FILESYS_H */

