/* cinode.h by Nick for NOS/UZI project */

#ifndef __CINODE_H
#define __CINODE_H

#include <setjmp.h>
#include <sys/stat.h> /* for S_IFMT */
#include <sys/types.h> /* for off_t */
#include "driver/bufpool.h" /* for dev_t, blkno_t */
#include "driver/rtc.h" /* for rtctime_t */

#define CORE_INODE_LIST 16	/* maximum entries in inode cache */
#define CMAGIC 24721	/* random number for cinode c_magic (in-core only) */

/* Flags for setftime() */
#define A_TIME 1
#define M_TIME 2
#define C_TIME 4

/* note: SUPERBLOCK is provided by z80/asm.h */
/* note: (D)(IN)DIRECTBLOCKS and TOTALREFBLOCKS are provided by z80/asm.h */

struct disk_inode_s
	{
	unsigned short i_mode;
	unsigned short i_nlink;
	unsigned short i_uid;
	unsigned short i_gid;
	unsigned long i_size;
	rtctime_t i_atime;
	rtctime_t i_mtime;
	rtctime_t i_ctime;
	blkno_t i_addr[TOTALREFBLOCKS];
	};		/* Exactly 64 bytes long!  (Don't break it!) */

/* note: DINODESPERBLOCK and DINODESPERBLOCKLOG are provided by z80/asm.h */

/* structure of in-core inode */
struct core_inode_s
	{
	unsigned short c_magic;	/* Used to check for corruption */
	unsigned char c_dirty;	/* Modified flag */
	dev_t c_dev;		/* Inode's device */
	ino_t c_num;		/* Inode's number */
	unsigned short c_refs; 	/* In-core reference count */
	unsigned char c_ro;	/* Read-only filesystem flag */
	struct disk_inode_s c_node;	/* disk inode copy */
	unsigned char c_busy;	/* Acquired flag */
	};

#define DEVNUM(ino)	((dev_t)((ino)->c_node.i_addr[0]))

/* Getmode() returns the inode kind */
#define _getmode(mode)	((mode) & S_IFMT)

extern struct core_inode_s *core_inode_list[CORE_INODE_LIST];

void i_ref(struct core_inode_s *ino);
void i_deref(struct core_inode_s *ino);
void i_acquire(struct core_inode_s *ino);
void i_release(struct core_inode_s *ino);
struct core_inode_s *i_open(jmp_buf error, dev_t devno, ino_t ino);
void wr_inode(jmp_buf error, struct core_inode_s *ino,
		unsigned char dirty_mask);
size_t readwritei(jmp_buf error, unsigned char write, struct core_inode_s *ino,
		void *data, size_t count, unsigned long position);
unsigned char isdevice(struct core_inode_s *ino);
void f_trunc(struct core_inode_s *ino);
blkno_t bmap(jmp_buf error, struct core_inode_s *ip, blkno_t bn,
		unsigned char rdflg);
mode_t getmode(struct core_inode_s *ino);
int getperm(struct core_inode_s *ino);
void setftim(struct core_inode_s *ino, unsigned char flag);
void magic(struct core_inode_s *ino, char *who);
void i_stat(struct core_inode_s *cinode_p, struct stat *buf);
void i_sync(dev_t dev);
void inotab_garbage(int red);

#endif /* __INODE_H */

