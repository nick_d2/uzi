/* xip.h by Nick for NOS/UZI project */

#ifndef __XIP_H
#define __XIP_H

#include <setjmp.h>
#include "filesys/filesys.h" /* for struct file_system_s, also "filesys/cinode.h" struct core_inode_s */

void xip_align(jmp_buf error, struct core_inode_s *ino);
void xip_ualign(jmp_buf error, struct core_inode_s *ino);
blkno_t *xip_examine(jmp_buf error, struct file_system_s *dev, struct core_inode_s *ino, off_t size,
		blkno_t *regioncount, blkno_t *blockcount);
blkno_t xip_align_chase(jmp_buf error, struct file_system_s *dev, struct core_inode_s *ino,
		blkno_t blk, blkno_t pos, blkno_t *region, blkno_t regions,
		blkno_t blocks);
blkno_t xip_align_bmap(jmp_buf error, struct core_inode_s *ino, blkno_t newno,
		blkno_t pos);
blkno_t xip_align_reverse(blkno_t blk, blkno_t *region, blkno_t regions,
		blkno_t blocks);
blkno_t xip_align_recurse(jmp_buf error, struct file_system_s *dev, struct core_inode_s *ino,
		unsigned char exclude, blkno_t *parent, char *dirty,
		unsigned char indirection, blkno_t *region,
		blkno_t regions, blkno_t blocks);

#endif /* __XIP_H */

