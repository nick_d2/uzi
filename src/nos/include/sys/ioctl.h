#ifndef _SYS_IOCTL_H
#define _SYS_IOCTL_H

#if 1 /* Nick moved this here from unix.h to make crt/utsname.c compile */
/* Info about a specific process, returned by sys_getfsys */
typedef struct s_pdata {
	int	u_pid;		/* Process PID */
	void/*ptab_t*/	*u_ptab;	/* Process table pointer */
	unsigned char	u_name[DIRECTNAMELEN];	/* Name invoked with */

	/* syscall's interface */
	unsigned char	u_insys;	/* True if in kernel now */
	unsigned char	u_callno;	/* syscall being executed */
	unsigned char	u_traceme;	/* Process tracing flag */

	/* filesystem/user info */
	unsigned char	u_uid; 		/* user id */
	unsigned char	u_gid; 		/* group id */
	unsigned char	u_euid; 	/* effective user id */
	unsigned char	u_egid; 	/* group user id */
	/*uzi*/time_t	u_time; 	/* Start time */

	/* process flow info */
	signal_t u_cursig;	/* Signal currently being caught */

	/* time info */
	/*uzi*/time_t	u_utime;	/* Elapsed ticks in user mode */
	/*uzi*/time_t	u_stime;	/* Ticks in system mode */
};

/* Info about kernel, returned by sys_getfsys */
typedef struct s_kdata {
	unsigned char	k_name[14];	/* OS name */
	unsigned char	k_version[8];	/* OS version */
	unsigned char	k_release[8];	/* OS release */

	unsigned char	k_host[14];	/* Host name */
	unsigned char	k_machine[8];	/* Host machine */

	int	k_tmem; 	/* System memory, in kbytes */
	int	k_kmem; 	/* Kernel memory, in kbytes */
};
#endif

typedef enum {
	GI_PTAB = -1,	/* processes table */
	GI_ITAB = -2,	/* inodes table */
	GI_BTAB = -3,	/* buffers table */
	GI_FTAB = -4,	/* filesystems table */
	GI_UDAT = -5,	/* process user data */
	GI_UTAB = -6,	/* current process table */
	GI_PDAT = -7,	/* process info */
	GI_KDAT = -8	/* kernel info */
} getinfo_t;

/* control of /dev/mem device */
#define MEM_INFO	0

#if 0 /* Nick, it conflicts with TIOCSETP etc, see c:\uzi\include\sgtty.h */
/* control of /dev/tty device */
#define TTY_COOKED	0	/* buffered */
#define TTY_RAW 	1	/* unbuffered, wait */
#define TTY_RAW_UNBUFF	2	/* unbuffered, no wait */
#endif

struct swap_mmread {
	unsigned char	mm[2];
	unsigned int	offset;
	unsigned int	size;
	unsigned char	*buf;
};

typedef struct {
	getinfo_t req;
	size_t	size;
	void	*ptr;
} info_t;
#endif

