/* socket.h by Nick for NOS/UZI project (contains NOS sockaddr.h by KA9Q) */

#ifndef _SYS_SOCKET_H
#define _SYS_SOCKET_H

#include <sys/types.h>

/* Berkeley format socket address structures. These things were rather
 * poorly thought out, but compatibility is important (or so they say).
 * Note that all the sockaddr variants must be of the same size, 16 bytes
 * to be specific. Although attempts have been made to account for alignment
 * requirements (notably in sockaddr_ax), porters should check each
 * structure.
 */

#define SOCKSIZE (sizeof(struct sockaddr))
#define MAXSOCKSIZE SOCKSIZE /* All sockets are of the same size for now */

#define AF_INET		0
#define AF_AX25		1
#define AF_NETROM	2
#define AF_LOCAL	3
/*#define NAF		4*/ /* this is now done in nos/config.h */

#define PF_INIT		AF_INET
#define PF_AX25		AF_AX25
#define PF_NETROM	AF_NETROM
#define PF_LOCAL	AF_LOCAL

#define SOCK_STREAM	0
#define SOCK_DGRAM	1
#define SOCK_RAW	2
#define SOCK_SEQPACKET	3

/* Generic socket address structure */
struct sockaddr
	{
	short sa_family;
	char sa_data[14];
	};

/* This is a structure for "historical" reasons (whatever they are) */
struct in_addr
	{
	unsigned long s_addr;
	};

/* Socket address, DARPA Internet style */
struct sockaddr_in
	{
	short sin_family;
	unsigned short sin_port;
	struct in_addr sin_addr;
	char sin_zero[8];
	};

int socket(int af, int type, int pf);
int bind(int fd, struct sockaddr *name, int namelen);
int listen(int fd, int backlog);
int connect(int fd, struct sockaddr *peername, int peernamelen);
int accept(int fd, struct sockaddr *peername, int *peernamelen);
int shutdown(int fd, int flag);
int socketpair(int af, int type, int pf, int *fd_p);
ssize_t recv(int fd, void *data, size_t count, int flag);
ssize_t recvfrom(int fd, void *data, size_t count, int flag,
		struct sockaddr *from, int *fromlen);
ssize_t send(int fd, void *data, size_t count, int flag);
ssize_t sendto(int fd, void *data, size_t count, int flag,
		struct sockaddr *to, int tolen);
int getsockname(int fd, struct sockaddr *name, int *namelen);
int getpeername(int fd, struct sockaddr *peername, int *peernamelen);

#endif /* _SYS_SOCKET_H */

