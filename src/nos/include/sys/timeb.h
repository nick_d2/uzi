/* timeb.h by Nick for UZI C library */

#ifndef __TIMEB_H
#define __TIMEB_H

struct timeb
	{
	time_t time; /* Seconds portion of the current time */
	unsigned short millitm; /* Millisecs portion of the current time */
	short timezone; /* The local timezone in minutes west of Greenwich */
	short dstflag; /* TRUE if Daylight Savings Time is in effect */
	};

int ftime(struct timeb *); /* LEGACY */

#endif

