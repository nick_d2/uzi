/* libintl.h by Nick for NOS/UZI project */

#ifndef __LIBINTL_H
#define	__LIBINTL_H

#include <stdio.h>
#include <stdarg.h>

#ifdef ZILOG

#ifndef __CHAR
#define __CHAR
typedef void _char(void);
#endif
#define gettext _gettext
#define _strdup(string) ((char *)_gettext(string))
int __dgetfail(unsigned char level, void (*address)(void));
int __fgetfail(FILE *stream, void (*address)(void));
int __getfail(void (*address)(void));
int _dprintf(unsigned char level, _char *format, ...);
int _vdprintf(unsigned char level, _char *format, va_list arguments);
int _fprintf(FILE *stream, _char *format, ...);
int _vfprintf(FILE *stream, _char *format, va_list arguments);
int _fputs(_char *string, FILE *stream);
int _printf(_char *format, ...);
int _vprintf(_char *format, va_list arguments);
int _puts(_char *string);
char *_strcpy(char *buffer, _char *string);
int _strlen(_char *string);
int _strncmp(const char *buffer, _char *string, size_t count);
int _strcmp(const char *buffer, _char *string);
char *_strncpy(char *buffer, _char *string, size_t count);
int _stricmp(const char *buffer, _char *string);
void _perror(_char *string);
int _sprintf(char *sp, _char *format, ...);

#else

#define _char const char
#define N_
#define _ gettext
#define _dprintf dprintf
#define _vdprintf vdprintf
#define _fprintf fprintf
#define _vfprintf vfprintf
#define _fputs fputs
#define _printf printf
#define _vprintf vprintf
#define _puts puts
#define _strcpy strcpy
#define _strdup strdup
#define _strlen strlen
#define _strncmp strncmp
#define _strncpy strncpy
/*RPB*/
#define _stricmp stricmp
#define _perror perror
#define _sprintf sprintf
/*RPB*/
#endif

const char *gettext(_char *string);

#endif /* __LIBINTL_H */

