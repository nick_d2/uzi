#ifndef	_SLIP_H
#define	_SLIP_H

#ifndef	_GLOBAL_H
#include "nos/global.h"
#endif

#ifndef _IFACE_H
#include "main/iface.h"
#endif

#ifndef _SLHC_H
#include "ppp/slhc.h"
#endif

#define SLIP_MAX 6		/* Maximum number of slip channels */

/* SLIP definitions */

/* Receiver allocation increment
 * Make this match the medium mbuf size in mbuf.h for best performance
 */
#define	SLIP_ALLOC	128

#define	FR_END		0300	/* Frame End */
#define	FR_ESC		0333	/* Frame Escape */
#define	T_FR_END	0334	/* Transposed frame end */
#define	T_FR_ESC	0335	/* Transposed frame escape */

/* Slip protocol control structure */
struct slip {
	struct iface_s *iface;
	uint8 escaped;		/* Receiver State control flag */
#define SLIP_FLAG	0x01	/* Last char was a frame escape */
#define SLIP_VJCOMPR	0x02	/* TCP header compression enabled */
	struct mbuf_s *rbp_head;	/* Head of mbuf chain being filled */
	struct mbuf_s *rbp_tail;	/* Pointer to mbuf currently being written */
	uint8 *rcp;		/* Write pointer */
	uint16 rcnt;		/* Length of mbuf chain */
	struct mbuf_s *tbp;	/* Transmit mbuf being sent */
	uint16 errors;		/* Receiver input errors */
	int type;		/* Protocol of input */
#if 0
	unsigned int (*recv)(struct asy *,struct mbuf_s **,size_t);
	unsigned int (*send)(struct asy *,struct mbuf_s **);
#endif
	struct slcompress *slcomp; /* TCP header compression table */
};

/* In slip.c: */
extern struct slip Slip[SLIP_MAX];

void asytxdone(int dev);
int slip_free(struct iface_s *ifp);
int slip_init(struct iface_s *ifp);
/* int slip_raw(struct iface_s *iface,struct mbuf_s **data); */
/* void slip_rx(int xdev,void *p1,void *p2); */
int slip_send(struct mbuf_s **bp,struct iface_s *iface,int32 gateway,uint8 tos);
int vjslip_send(struct mbuf_s **bp,struct iface_s *iface,int32 gateway,uint8 tos);
/* void slip_status(struct iface_s *iface); */

#endif	/* _SLIP_H */
