/* dialer definitions */

#ifndef _DIALER_H
#define _DIALER_H

/* Dynamic dialup params */
struct dialer_s {
	char *initfile;		/* Script to initialize modem */
	char *actfile;		/* Script to activate line */
	char *dropfile;		/* Script to drop line */
	char *ansfile;		/* Script to answer incoming call */
	struct timer idle;	/* Idle timeout timer */
	long originates;	/* Count of times we bring up the link */
	long answers;		/* Count of incoming calls */
	long localdrops;	/* Count of times we dropped the link */
	int dev;
};

int sd_init(struct iface_s *,int32,int,char **);
int sd_stat(struct iface_s *);
int dialer_kick(struct iface_s *ifp);
void sd_answer(int dev, void *p1, void *p2);

#endif /* _DIALER_H */

