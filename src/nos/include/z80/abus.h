/* Various I/O definitions specific to apibus I/O on the Hytech 1000/1500 */

#ifndef	_ABUS_H
#define	_ABUS_H

#include "z80/asm.h" /* for API_MAX */
#include "z80/async.h"
#include "nos/global.h"

/* Apibus controller control block */
struct abus_s {
	struct async_s async;
#if 0 /* this wouldn't work, abus_p may be different on closure */
	struct {		/* Previous configuration saved at startup */
		void INTERRUPT (*int1vec)(void);
				/* Original INT1 interrupt vector [bank:pc] */
		void INTERRUPT (*csiovec)(void);
				/* Original CSI/O interrupt vector [bank:pc] */
	} save;
#endif
};

extern struct abus_s Api[API_MAX];
extern unsigned char Api_cflag; /* apibus injected command byte */
extern unsigned char Api_txsti; /* apibus sticky counter for tx */
extern unsigned char Api_rxsti; /* apibus sticky counter for rx */
/*extern unsigned char Api_turbo;*/ /* counter for continuous polls */
extern unsigned char Api_count;
extern unsigned char Api_state;
extern unsigned char Api_dirn;
extern unsigned char Api_robin;
extern unsigned char Api_rxovr;
extern unsigned char Api_devok;
extern unsigned char Api_kick;

/* in abus.c */
int abus_init(int dev,struct iface_s *ifp, unsigned int bufsize, int trigchar);
unsigned long abus_ioctl(struct iface_s *ifp, int cmd, int set,
		unsigned long val);
/*int abus_open(char *name);*/
/*int abus_close(int dev);*/
int abus_stop(struct iface_s *ifp);
int doabusstat(int argc, char *argv[], void *p);

/* in abusvec.S */
void INTERRUPT int1_vec(void);
void INTERRUPT csio_vec(void);

#endif	/* _ABUS_H */

