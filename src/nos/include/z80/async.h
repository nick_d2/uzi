/* Various I/O definitions specific to asynch I/O on the IBM PC */

#ifndef	_ASY_H
#define	_ASY_H

#include <sys/types.h>

/* Asynch controller control block */
struct async_s {
	struct iface_s *iface;

	unsigned char *buf;		/* Ring buffer */
	unsigned int bufsize;		/* Size of ring buffer */
	unsigned char *wp;		/* Write pointer */
	unsigned char *rp;		/* Read pointer */
	volatile unsigned int cnt;	/* Count of characters in buffer */
	unsigned int hiwat;		/* High water mark */
	unsigned long overrun;		/* Count of s/w fifo buffer overruns */
	unsigned int trigchar;		/* Fifo trigger character */

	struct mbuf_s *sndq;
	void (*kick)(struct async_s *async_p);
};

unsigned int async_getc(struct async_s *async_p); /* PLEASE USE async_recv() */
unsigned int async_recv(struct async_s *async_p, struct mbuf_s **mbuf_pp,
		size_t cnt);
ssize_t async_read(struct async_s *async_p, void *buf, size_t cnt);
unsigned int async_send(struct async_s *async_p, struct mbuf_s **mbuf_pp);
ssize_t async_write(struct async_s *async_p, void *buf, size_t cnt);
void async_rx(struct async_s *async_p, unsigned char ch);
unsigned int async_tx(struct async_s *async_p);

#endif	/* _ASY_H */

