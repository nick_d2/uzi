/* Various I/O definitions specific to asynch I/O on the IBM PC */

#ifndef	_ASCI_H
#define	_ASCI_H

#include "z80/async.h"
#include "nos/global.h"

/* If you increase this, you must add additional interrupt vector
 * hooks in ascivec.S
 */
#define ASCI_MAX 2

/* Asynch controller control block */
struct asci_s {
	struct async_s async;	/* Header for all serial port devices */

	unsigned int addr;	/* Base I/O address */
	unsigned char vec;	/* Interrupt vector */
	unsigned long speed;	/* Line speed in bits per second */

	struct {		/* Previous configuration saved at startup */
		void INTERRUPT (*vec)(void);
				/* Original interrupt vector [cs:pc] */
		unsigned char mask; /* 8259 mask */
		unsigned char cntla; /* line control reg */
		unsigned char stat; /* Interrupt enable register */
		unsigned char cntlb; /* modem status bits */
	} save;

	unsigned char cntla;	/* Control register chan0 */
	unsigned char stat;	/* Status register chan0 */
	unsigned char cntlb;	/* Copy of current modem status register */
	unsigned char cts;	/* obey CTS lead for xmit flow control */
	unsigned char rlsd;	/* RLSD (CD) indicates line state */
	unsigned char chain;	/* Do interrupt vector chaining */

	unsigned long ints;	/* Count of modem status interrupts */
	unsigned long overrun;	/* Receiver hardware overrun errors */
};

extern struct asci_s *Asci[ASCI_MAX];

#if 0
struct fport {
	int base;
	int irq;
	struct asci_s *asci[4];	/* Pointers to regular asci entries */
	int iv;			/* Interrupt demux port */
};
extern int Nfport;
extern struct fport Fport[];

#define	BAUDCLK	115200L		/* PS=0, DR=0, 18.432 Mhz / (16 * 10) */
#endif

/* National 8250 asynch UART definitions */
/* Control/status register offsets from base address */
#define	CNTLA	0		/* Line control register */
#define	CNTLB	2		/* Modem status register */
#define	STAT	4		/* Line status / Interrupt enable register */
#define	TDR	6		/* Transmitter holding register */
#define	RDR	8		/* Receiver buffer register */
/*RPB*/
#define	EXCR0	0x12		/* ASCI0 extension control register */
/*RPB*/

#if 0
#define	DLL	0		/* Divisor latch LSB */
#define	DLM	1		/* Divisor latch MSB */
#define	IIR	2		/* Interrupt ident register */
#define	FCR	2		/* FIFO control register (16550A only) */
#define	MCR	4		/* Modem control register */
#endif

/* 8250 Line Control Register */
#define CNTLA_RE	0x40	/* Receive enable */
#define CNTLA_TE	0x20	/* Transmit enable */
#define CNTLA_RTS_CKA1D	0x10	/* =1 RTS off (chan0), =1 TEND0 select (chan1) *//*RPB added eoc*/
#define	CNTLA_7BITS	0x00	/* 7 bit words */
#define	CNTLA_8BITS	0x04	/* 8 bit words */
#define CNTLA_BIT_MASK	0x04	/* mask of bit size */
#define	CNTLA_PEN	0x02	/* Parity enable */
#define	CNTLA_NSB	0x01	/* Number of stop bits */
/*RPB*/
#define	CNTLA_RTS0	0x10	/* Request to send chan0 */
/*RPB*/


#if 0
#define	CNTLA_SP	0x20	/* Stick parity */
#define	CNTLA_SB	0x40	/* Set break */
#define	CNTLA_DLAB	0x80	/* Divisor Latch Access Bit */
#endif

/* 8250 Line Status Register */
#define	STAT_RDRF	0x80	/* Data ready */
#define	STAT_OVRN	0x40	/* Overrun error */
#define	STAT_PE		0x20	/* Parity error */
#define	STAT_FE		0x10	/* Framing error */
/*RPB*/
#define	STAT_DCD0	0x04	/* Data carrier detect chan0 */
/*RPB*/
#define STAT_TDRE	0x02	/* Transmitter line holding register empty */

#if 0
#define	STAT_BI		0x10	/* Break interrupt */
#define STAT_TSRE	0x40	/* Transmitter shift register empty */
#endif

/* 8250 Interrupt Identification Register */
#if 0
#define	IIR_IP		0x01	/* 0 if interrupt pending */

#define	IIR_MSTAT	0x00	/* Modem status interrupt */
#define	IIR_TDRE	0x02	/* Transmitter holding register empty int */
#define	IIR_RDA		0x04	/* Receiver data available interrupt */
#define	IIR_RLS		0x06	/* Receiver Line Status interrupt */
#define IIR_ID_MASK	0x06	/* Mask for interrupt ID */

#define IIR_FIFO_TIMEOUT 0x08	/* FIFO timeout interrupt pending - 16550A */
#define IIR_FIFO_ENABLED 0xc0	/* FIFO enabled (FCR0,1 = 1) - 16550A only */
#endif

/* 8250 interrupt enable register bits */
#define	STAT_RIE	0x08	/* Data available interrupt */
#define	STAT_TIE	0x01	/* Tx buffer empty interrupt */

#if 0
#define	STAT_RLS	0x04	/* Receive line status interrupt */
#define	STAT_MS		0x08	/* Modem status interrupt */
#endif

/* 8250 Modem control register */
#if 0
#define	MCR_DTR		0x01	/* Data Terminal Ready */
#define	MCR_RTS		0x02	/* Request to Send */
#define	MCR_RFR		MCR_RTS	/* Ready for Receiving (same as RTS) */
#define	MCR_OUT1	0x04	/* Out 1 (not used) */
#define	MCR_OUT2	0x08	/* Master interrupt enable (actually OUT 2) */
#define	MCR_LOOP	0x10	/* Loopback test mode */
#endif

/* 8250 Modem Status Register */
#define CNTLB_MPBT	0x80	/* =0 Send ordinary, =1 Send wakeup */
#define CNTLB_MP	0x40	/* =0 Normal mode, =1 Multiprocessor mode */
#define	CNTLB_PEO	0x10	/* =0 Even parity, =1 Odd parity select */

/*RPB*/
/*#if 0*/
#if 1
/*RPB*/
#define	CNTLB_DCTS	0x01	/* Delta Clear-to-Send */
#define	CNTLB_DDSR	0x02	/* Delta Data Set Ready */
#define	CNTLB_TERI	0x04	/* Trailing edge ring indicator */
#define	CNTLB_DRLSD	0x08	/* Delta Rx Line Signal Detect */
#define	CNTLB_CTS		0x10	/* Clear to send */
#define	CNTLB_DSR		0x20	/* Data set ready */
#define	CNTLB_RI		0x40	/* Ring indicator */
#define CNTLB_RLSD	0x80	/* Rx line signal detect */
/*RPB*/
#define	CNTLB_CTS_PS	0x20	/* rd: Clear to send chan0, wr: prescalar */
/*RPB*/
#endif

/*RPB*/
/* ASCI0 extension control register */
#define EXCR0_CTS0	0x20	/* 0= disable 1= enable */
#define EXCR0_DCD0	0x40	/* 0= disable 1= enable */
/*RPB*/

/* 16550A FIFO control register values */
#if 0
#define	FIFO_ENABLE	0x01	/* enable TX & RX fifo */
#define	FIFO_CLR_RX	0x02	/* clear RX fifo */
#define	FIFO_CLR_TX	0x04	/* clear TX fifo */
#define	FIFO_START_DMA	0x08	/* enable TXRDY/RXRDY pin DMA handshake */
#define FIFO_SIZE_1	0x00	/* RX fifo trigger levels */
#define FIFO_SIZE_4	0x40
#define FIFO_SIZE_8	0x80
#define FIFO_SIZE_14	0xC0
#define FIFO_SIZE_MASK	0xC0

#define FIFO_TRIGGER_LEVEL	FIFO_SIZE_4
#define FIFO_SETUP	(FIFO_ENABLE|FIFO_CLR_RX|FIFO_CLR_TX|FIFO_TRIGGER_LEVEL)

#define OUTPUT_FIFO_SIZE	16
#endif

struct asci_mode_s {
	char *name;
	int trigchar;
	int (*init)(struct iface_s *);
	int (*free)(struct iface_s *);
};
#define ASCI_MODE_MAX 4
extern struct asci_mode_s Asci_mode[ASCI_MODE_MAX];

/* in asci.c */
struct asci_s *asci_init(int dev, struct iface_s *ifp, int base, int irq,
		unsigned int bufsize, int trigchar, long speed, int cts,
		int rlsd, int chain);
unsigned long asci_ioctl(struct asci_s *asci_p, int cmd, int set,
		unsigned long val);
/*int asci_open(char *name);*/
/*int asci_close(int dev);*/
int asci_speed(struct asci_s *asci_p, unsigned long bps);
int asci_stop(struct asci_s *asci_p);
/*int get_rlsd_asci(int dev, int new_rlsd);*/
int doascistat(int argc, char *argv[], void *p);
void INTERRUPT asci_int(struct asci_s *asci_p);

/* In ascivec.S: */
void INTERRUPT asci0_vec(void);
void INTERRUPT asci1_vec(void);

#endif	/* _ASCI_H */

