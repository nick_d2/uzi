#ifndef __SIGNAL_H
#define __SIGNAL_H
#ifndef __TYPES_H
#include <sys/types.h>
#endif

#define NSIGS		16	/* Number of signals <= 16 */

#if 1 /* Nick, because the unix() system call needs integer parameters only */
#define __NOTASIGNAL 0
#define SIGHUP 1 		/* Hangup detected on controlling terminal
				   or death of a controlling process */
#define SIGINT 2 		/* Interrupt from keyboard */
#define SIGQUIT 3		/* Quit from keyboard */
#define SIGILL 4 		/* Illegal instruction */
#define SIGTRAP 5		/* Trace/breakpoint trap */
#define SIGIOT 6 		/* IOT trap. A synonym for SIGABRT */
#define SIGABRT SIGIOT		/* Abort signal from abort */
#define SIGUSR1 7		/* User's signal 1 */
#define SIGUSR2 8		/* User's signal 2 */
#define SIGKILL 9		/* Kill signal */
#define SIGPIPE 10		/* Broken pipe: write to pipe with no readers */
#define SIGALRM 11		/* Timer signal from alarm */
#define SIGTERM 12		/* Termination signal */
#define SIGURG 13			/* Urgent signal */
#define SIGCONT 14		/* Continue process */
#define SIGSTOP 15		/* Stop process */
#define __NUMOFSIGNALS SIGSTOP
/* this signals defined only for compatibility */
#define SIGBUS 16 		/* Bus error */
#define SIGFPE 17 		/* Floating point exception */
#define SIGSEGV 18		/* Invalid memory reference */
#define SIGSYS 19 		/* Bad argument to routine */
#define SIGTTIN 20
#define SIGTOUT 21
typedef int signal_t;
#else
/* signals values */
typedef enum {
	__NOTASIGNAL = 0,
	SIGHUP, 		/* Hangup detected on controlling terminal
				   or death of a controlling process */
	SIGINT, 		/* Interrupt from keyboard */
	SIGQUIT,		/* Quit from keyboard */
	SIGILL, 		/* Illegal instruction */
	SIGTRAP,		/* Trace/breakpoint trap */
	SIGIOT, 		/* IOT trap. A synonym for SIGABRT */
	SIGABRT = SIGIOT,	/* Abort signal from abort */
	SIGUSR1,		/* User's signal 1 */
	SIGUSR2,		/* User's signal 2 */
	SIGKILL,		/* Kill signal */
	SIGPIPE,		/* Broken pipe: write to pipe with no readers */
	SIGALRM,		/* Timer signal from alarm */
	SIGTERM,		/* Termination signal */
	SIGURG,			/* Urgent signal */
	SIGCONT,		/* Continue process */
	SIGSTOP,		/* Stop process */
#define __NUMOFSIGNALS SIGSTOP
	/* this signals defined only for compatibility */
	SIGBUS, 		/* Bus error */
	SIGFPE, 		/* Floating point exception */
	SIGSEGV,		/* Invalid memory reference */
	SIGSYS, 		/* Bad argument to routine */
	SIGTTIN,
	SIGTOUT
} signal_t;
#endif

#if __NUMOFSIGNALS > NSIGS
 error Too many signals defined
#endif

#define sigmask(sig) (1<<((sig)-1)) 	/* signal mask */

typedef uint sigset_t;			/* at least 16 bits */

/* Type of a signal handler.  */
#if 1 /* Nick, for IAR compiler large or banked memory model */
typedef long sig_t;
#else
typedef void (*sig_t) __P((signal_t));
#endif

#define SIG_DFL ((sig_t)0)	/* default signal handling */
#define SIG_IGN ((sig_t)1)	/* ignore signal */
#define SIG_ERR ((sig_t)-1)	/* error return from signal */

#if 0
struct sigaction {
	sig_t sa_handler;
	sigset_t sa_mask;
	uint sa_flags;
	void (*sa_restorer)( /*void*/ );
};
#endif

#if 0
extern char *sys_siglist[];
#endif

#endif
