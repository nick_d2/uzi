#ifndef	_PC_H
#define	_PC_H
#define _HARDWARE_H

#ifndef	_GLOBAL_H
#include "nos/global.h"
#endif

#define	NSW	10	/* Number of stopwatch "memories" */

#ifdef ZILOG
#define	HWTIRQ	10	/* IRQ for Z180 PRT channel 0 */
#else
#define HWTIRQ	0	/* IRQ for PC timer (not used yet, see "chtimer") */
#define	KBIRQ	1	/* IRQ for PC keyboard */
#endif

/* Extended keyboard codes for function keys */
#define	F1	59	/* Function key 1 */
#define	F2	60
#define	F3	61
#define	F4	62
#define	F5	63
#define	F6	64
#define	F7	65
#define	F8	66
#define	F9	67
#define	F10	68

#define	CURSHOM	71	/* Home key */
#define	CURSUP	72	/* Up arrow key */
#define	PAGEUP	73	/* Page up key */
#define	CURSLEFT 75	/* Cursor left key */
#define CURSRIGHT 77	/* Cursor right key */
#define	CURSEND	79	/* END key */
#define	CURSDWN	80	/* Down arrow key */
#define	PAGEDWN	81	/* Page down key */

#define	AF1	104	/* ALT-F1 */
#define	AF2	105
#define	AF3	106
#define	AF4	107
#define	AF5	108
#define	AF6	109
#define	AF7	110
#define	AF8	111
#define	AF9	112
#define	AF10	113
#define	AF11	139
#define	AF12	140

struct stopwatch {
	long calls;
	uint16 maxval;
	uint16 minval;
	int32 totval;
};
#if 0
extern struct stopwatch Sw[];
#endif

/* Interrupt stack defined in pcgen.asm */
/*extern uint16 Intstk[INTERRUPT_STACK >> 1];*/
/*extern uint16 Stktop[];*/	/* Top of interrupt stack */

/* List of functions to call at shutdown */
#define SHUTDOWN_MAX 2
extern void (*Shutdown[SHUTDOWN_MAX])();

#if 0
extern int Mtasker;	/* Type of multitasker, if any */
#endif

/* In n8250.c: */
void asy_timer(void);

/* In dos.c: */
extern unsigned int *Refcnt;
int _creat(const char *file, mode_t mode); /* Nick mode_t formerly int */
#ifndef SKIP_SYSCALLS
int _open(const char *file, int flag, ...); /* Nick added flag and ellipsis */
#endif
int dup(int fd);
int _close(int fd);
int _read(int fd, void *buf, size_t cnt);
int _write(int fd, const void *buf, size_t cnt);
long _lseek(int fd, long offset, int whence);

/* In dma.c: */
unsigned long dma_map(void *p,unsigned short len,int copy);
void dma_unmap(void *p,int copy);
int dis_dmaxl(int chan);
int ena_dmaxl(int chan);
unsigned long dmalock(void *p,unsigned short len);
unsigned long dmaunlock(unsigned long physaddr,unsigned short len);
void *dma_malloc(int32 *physaddr,unsigned short len);

/* In random.c: */
void rtype(uint16 c);

/* In escc.c: */
void escc_exit(void);
void escc_timer(void);

/* In pc.c: */
long bioscnt(void);
void clrbit(unsigned int port,char bits);
void ctick(void);
int32 divrem(int32 dividend,uint16 divisor);
void INTERRUPT (*getirq(unsigned int))(void);
int getmask(unsigned int irq);
int intcontext(void);
void ioinit(void);
void iostop(void);
void kbsave(int c);
int kbread(void);
int maskoff(unsigned int irq);
int maskon(unsigned int irq);
void pctick(void);
void setbit(unsigned int port,char bits);
int setirq(unsigned int irq,void INTERRUPT (*handler)(void));
void sysreset(void);
void systick(void);
void writebit(unsigned int port,char mask,int val);

#ifdef ZILOG
/* In z80gen.S: */
void INTERRUPT hwtick(void);
#else
/* In pcgen.S: */
void INTERRUPT btick(void);
#endif
void chktasker(void);
void chtimer(void INTERRUPT (*)());
int32 divrem(int32 dividend,uint16 divisor);
uint16 getss(void);
void giveup(void);
uint16 kbraw(void);
uint16 longdiv(uint16 divisor,int n,uint16 *dividend);
uint16 longmul(uint16 multiplier,int n,uint16 *multiplicand);
void INTERRUPT nullvec(void);
void INTERRUPT kbint(void);
void uchtimer(void);
uint16 clockbits(void);

/* In stopwatch.asm: */
void swstart(void);
uint16 stopval(void);

/* In sw.c: */
void swstop(int n);

#endif	/* _PC_H */

