#ifndef	_TRACE_H
#define	_TRACE_H

#include <libintl.h> /* for _char */

/* Apr 04	RPB	Changes for intl' of strings */

#ifndef	_MBUF_H
#include "main/mbuf.h"
#endif

#ifndef	_IFACE_H
#include "main/iface.h"
#endif

/* Definitions for packet dumping */

/* Table of trace subcommands */
struct tracecmd {
	_char *name;	/* Name of trace subcommand */ /*RPB*/
	int val;	/* New setting for these bits */
	int mask;	/* Mask of bits affected in trace word */
};

/* extern struct tracecmd Tracecmd[]; */	/* Defined in trace.c */

#if 0
/* List of address testing and tracing functions for each interface.
 * Entries are placed in this table by conditional compilation in main.c.
 */
struct trace {
	int (*addrtest)(struct iface_s *iface,struct mbuf_s *bp);
	void (*tracef)(FILE *,struct mbuf_s **,int);
};

extern struct trace Tracef[];
#endif

/* In trace.c: */
void dump(struct iface_s *ifp, int direction, struct mbuf_s *bp);
void raw_dump(struct iface_s *ifp, int direction, struct mbuf_s *bp);
void shuttrace(void);
void _trace_log(struct iface_s *ifp,_char *fmt, ...);
int _tprintf(struct iface_s *ifp, _char *fmt, ...); /*RPB*/
int _vtprintf(struct iface_s *ifp, _char *fmt, va_list ap);
int twrite(void *data, size_t count0, size_t count1, struct iface_s *ifp);
int tputc(int ch, struct iface_s *ifp);

/* In arcdump.c: */
void arc_dump(struct iface_s *ifp,struct mbuf_s **bpp,int check);
int arc_forus(struct iface_s *iface,struct mbuf_s *bp);

/* In arpdump.c: */
void arp_dump(struct iface_s *ifp,struct mbuf_s **bpp);

/* In ax25dump.c: */
void ax25_dump(struct iface_s *ifp,struct mbuf_s **bpp,int check);
int ax_forus(struct iface_s *iface,struct mbuf_s *bp);

/* In enetdump.c: */
void ether_dump(struct iface_s *ifp,struct mbuf_s **bpp,int check);
int ether_forus(struct iface_s *iface,struct mbuf_s *bp);

/* In icmpdump.c: */
void icmp_dump(struct iface_s *ifp,struct mbuf_s **bpp,int32 source,int32 dest,int check);

/* In ipdump.c: */
void ipip_dump(struct iface_s *ifp,struct mbuf_s **bpp,int32 source,int32 dest,int check);
void ip_dump(struct iface_s *ifp,struct mbuf_s **bpp,int check);

/* In kissdump.c: */
void ki_dump(struct iface_s *ifp,struct mbuf_s **bpp,int check);
int ki_forus(struct iface_s *iface,struct mbuf_s *bp);

/* In nrdump.c: */
void netrom_dump(struct iface_s *ifp,struct mbuf_s **bpp,int check);

/* In pppdump.c: */
void ppp_dump(struct iface_s *ifp,struct mbuf_s **bpp,int check);

/* In ripdump.c: */
void rip_dump(struct iface_s *ifp,struct mbuf_s **bpp);

/* In secdump.c: */
void esp_dump(struct iface_s *ifp,struct mbuf_s **bpp,int32 source,int32 dest,int check);
void ah_dump(struct iface_s *ifp,struct mbuf_s **bpp,int32 source,int32 dest,int check);

/* In slcompdump.c: */
void sl_dump(struct iface_s *ifp,struct mbuf_s **bpp,int check);
void vjcomp_dump(struct iface_s *ifp,struct mbuf_s **bpp,int unused);

/* In tcpdump.c: */
void tcp_dump(struct iface_s *ifp,struct mbuf_s **bpp,int32 source,int32 dest,int check);

/* In udpdump.c: */
void udp_dump(struct iface_s *ifp,struct mbuf_s **bpp,int32 source,int32 dest,int check);

#endif	/* _TRACE_H */
