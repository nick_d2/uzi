/* opensock.h by Nick for NOS/UZI project */

#ifndef __OPENSOCK_H
#define __OPENSOCK_H

#include <setjmp.h>
#include "socket/socket.h" /* for struct socklink */
#include "socket/usock.h" /* for struct usock */

#define OPEN_SOCK_LIST 16	/* maximum number of open sockets */

extern struct usock *open_sock_list[OPEN_SOCK_LIST];

struct usock *os_new(jmp_buf error, unsigned int af, unsigned int type,
		unsigned int pf);
void os_ref(struct usock *os_p);
void os_deref(struct usock *os_p);
void os_garbage(int red);
struct usock *os_locate(jmp_buf error, unsigned int fd);
unsigned int os_socket(jmp_buf error, unsigned int af, unsigned int type,
		unsigned int pf);
void os_bind(jmp_buf error, unsigned int fd, struct sockaddr *name,
		unsigned int namelen);
void os_listen(jmp_buf error, unsigned int fd, unsigned int backlog);
void os_connect(jmp_buf error, unsigned int fd, struct sockaddr *peername,
		unsigned int peernamelen);
unsigned int os_accept(jmp_buf error, unsigned int fd,
		struct sockaddr *peername, unsigned int *peernamelen);
void os_shutdown(jmp_buf error, unsigned int fd, unsigned int flag);
void os_socketpair(jmp_buf error, unsigned int af, unsigned int type,
		unsigned int pf, unsigned int *fd_p);
size_t os_recv(jmp_buf error, unsigned int fd, void *data, size_t count,
		unsigned int flag);
size_t os_recvfrom(jmp_buf error, unsigned int fd, void *data, size_t count,
		unsigned int flag, struct sockaddr *from,
		unsigned int *fromlen);
size_t os_send(jmp_buf error, unsigned int fd, void *data, size_t count,
		unsigned int flag);
size_t os_sendto(jmp_buf error, unsigned int fd, void *data, size_t count,
		unsigned int flag, struct sockaddr *to, unsigned int tolen);
void os_getsockname(jmp_buf error, unsigned int fd, struct sockaddr *name,
		unsigned int *namelen);
void os_getpeername(jmp_buf error, unsigned int fd, struct sockaddr *peername,
		unsigned int *peernamelen);

#endif /* __OPENSOCK_H */

