#ifndef	_SOCKET_H
#define	_SOCKET_H

/* Apr 04	RPB	Changes for intl' of strings */

#include <errno.h> /* just to pick up the socket related error numbers */
#include <sys/socket.h> /* Nick, has socket() etc prototypes, sockaddr.h */
#include "kernel/thread.h"

#ifndef	_GLOBAL_H
#include "nos/global.h"
#endif

#ifndef	_MBUF_H
#include "main/mbuf.h"
#endif

/* Local IP wildcard address */
#define	INADDR_ANY	0L

/* IP protocol numbers */
/* now in internet.h */

/* TCP port numbers */
#define	IPPORT_ECHO	7	/* Echo data port */
#define	IPPORT_DISCARD	9	/* Discard data port */
#define	IPPORT_FTPD	20	/* FTP Data port */
#define	IPPORT_FTP	21	/* FTP Control port */
#define IPPORT_TELNET	23	/* Telnet port */
#define IPPORT_SMTP	25	/* Mail port */
#define	IPPORT_MTP	57	/* Secondary telnet protocol */
#define	IPPORT_FINGER	79	/* Finger port */
#define	IPPORT_TTYLINK	87	/* Chat port */
#define IPPORT_POP	109	/* pop2 port */
#define	IPPORT_NNTP	119	/* Netnews port */
#define	IPPORT_LOGIN	513	/* BSD rlogin port */
#define	IPPORT_TERM	5000	/* Serial interface server port */

/* UDP port numbers */
#define	IPPORT_DOMAIN	53
#define	IPPORT_BOOTPS	67
#define	IPPORT_BOOTPC	68
#define	IPPORT_PHOTURIS	468	/* Photuris Key management */
#define	IPPORT_RIP	520
#define	IPPORT_REMOTE	1234	/* Pulled out of the air */
#define	IPPORT_BSR	5000	/* BSR X10 interface server port (UDP) */

#if 0
extern _char *Sock_errlist[];
#endif
extern int Axi_sock;	/* Socket listening to AX25 (there can be only one) */

/* fix this later */
/* int close_s(int s); */

/* In file sockutil.c: */
char *eolseq(int s);
/* void freesock(struct thread_s *pp); */
int settos(int s,int tos);
/* void sockinit(void); */
int sockkick(int s);
int socklen(int s,int rtx);
struct thread_s *sockowner(int s,struct thread_s *newowner);
/* int usesock(int s); */

char *psocket(void *p);
char *sockerr(int s);
char *sockstate(int s);

/* In file tcpsock.c: */
int start_tcp(uint16 port,char *name,void (*task)(),int stack);
int stop_tcp(uint16 port);

#endif	/* _SOCKET_H */
