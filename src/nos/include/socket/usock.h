#ifndef	_USOCK_H
#define	_USOCK_H

#include <sys/socket.h> /* Nick, instead of nos/sockaddr.h */
#include "kernel/object.h" /* for struct object_s */
#include "kernel/thread.h" /* can be removed later? */
#include "main/config.h" /* for NAF */

/* Apr 04	RPB	Changes for intl' of strings */

#ifndef	_MBUF_H
#include "main/mbuf.h"
#endif

#ifndef _TCP_H
#include "internet/tcp.h"
#endif

#ifndef _UDP_H
#include "internet/udp.h"
#endif

#ifndef _IP_H
#include "internet/ip.h"
#endif

/*#ifndef _NETROM_H*/
/*#include "netrom/netrom.h"*/
/*#endif*/

struct loc {
	struct usock *peer;
	struct receive_queue_s rcvq;
	struct mbuf_s *q;
	int hiwat;		/* Flow control point */
	int flags;
#define	LOC_SHUTDOWN	1
};
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
struct lod {
	struct usock *peer;
	struct receive_queue_s rcvq;
	int hiwat;		/* Flow control point */
	int flags;
};
#endif
#define	LOCDFLOW	5	/* dgram socket flow-control point, packets */
#define	LOCSFLOW	2048	/* stream socket flow control point, bytes */
#define	SOCKBASE	128	/* Start of socket indexes */

union sp {
        struct sockaddr *sa;
        struct sockaddr_in *in;
        struct sockaddr_ax *ax;
        struct sockaddr_nr *nr;
};
struct socklink {
	int type;		/* Socket type */
	int (*socket)(struct usock *,int);
	int (*bind)(struct usock *);
	int (*listen)(struct usock *,int);
	int (*connect)(struct usock *);
	int accept;
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
	int (*recv)(struct usock *,void *,int,struct sockaddr *,int *);
	int (*send)(struct usock *,void *,int,struct sockaddr *);
#else
	int (*recv)(struct usock *,struct mbuf_s **,struct sockaddr *,int *);
	int (*send)(struct usock *,struct mbuf_s **,struct sockaddr *);
#endif
	int (*qlen)(struct usock *,int);
	int (*kick)(struct usock *);
	int (*shut)(struct usock *,int);
	int (*close)(struct usock *);
	int (*check)(struct sockaddr *,int);
	char **error;
	char *(*state)(struct usock *);
	int (*status)(struct usock *);
	char *eol;
};
#define SOCKLINK_MAX 6
extern struct socklink Socklink[SOCKLINK_MAX];
union cb {
	struct tcb *tcb;
	struct ax25_cb *ax25;
	struct udp_cb *udp;
	struct raw_ip *rip;
	struct raw_nr *rnr;
	struct nr4cb *nr4;
	struct loc *local;
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	struct lod *lod;
#endif
	void *p;
};
/* User sockets */
struct usock {
	struct object_s object;	/* header with type, refs, busy */
	unsigned index;
	struct thread_s *owner;
	char noblock;
	enum {
		NOTUSED,
		TYPE_TCP,
		TYPE_UDP,
		TYPE_AX25I,
		TYPE_AX25UI,
		TYPE_RAW,
		TYPE_NETROML3,
		TYPE_NETROML4,
		TYPE_LOCAL_STREAM,
		TYPE_LOCAL_DGRAM /* Nick , */
	} type;
	struct socklink *sp;
#if 1
	struct usock *rdysock;	/* avoids the need to call itop(user) */
#else
	int rdysock;
#endif
	union cb cb;
	struct sockaddr *name;
	int namelen;
	struct sockaddr *peername;
	int peernamelen;
	uint8 errcodes[4];	/* Protocol-specific error codes */
	uint8 tos;		/* Internet type-of-service */
	int flag;		/* Mode flags, defined in socket.h */
};
extern char *(*Psock[NAF])(struct sockaddr *);
#if 0
extern char Badsocket[];
#endif
#define SOCKTYPES_MAX 10
extern _char *Socktypes[SOCKTYPES_MAX];
extern struct usock **Usock;
extern unsigned Nsock;
extern uint16 Lport;

/* struct usock *itop(int s); */
/* void st_garbage(int red); */

/* In axsocket.c: */
int so_ax_sock(struct usock *up,int protocol);
int so_ax_bind(struct usock *up);
int so_ax_listen(struct usock *up,int backlog);
int so_ax_conn(struct usock *up);
int so_ax_recv(struct usock *up,struct mbuf_s **bpp,struct sockaddr *from,
		int *fromlen);
int so_ax_send(struct usock *up,struct mbuf_s **bp,struct sockaddr *to);
int so_ax_qlen(struct usock *up,int rtx);
int so_ax_kick(struct usock *up);
int so_ax_shut(struct usock *up,int how);
int so_ax_close(struct usock *up);
int checkaxaddr(struct sockaddr *name,int namelen);
int so_axui_sock(struct usock *up,int protocol);
int so_axui_bind(struct usock *up);
int so_axui_conn(struct usock *up);
int so_axui_recv(struct usock *up,struct mbuf_s **bpp,struct sockaddr *from,
		int *fromlen);
int so_axui_send(struct usock *up,struct mbuf_s **bp,struct sockaddr *to);
int so_axui_qlen(struct usock *up,int rtx);
int so_axui_shut(struct usock *up,int how);
int so_axui_close(struct usock *up);
char *axpsocket(struct sockaddr *p);
char *axstate(struct usock *up);
int so_ax_stat(struct usock *up);


/* In ipsocket.c: */
int so_ip_sock(struct usock *up,int protocol);
int so_ip_conn(struct usock *up);
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
int so_ip_recv(struct usock *up,void *buf,int cnt,struct sockaddr *from,
		int *fromlen);
int so_ip_send(struct usock *up,void *buf,int cnt,struct sockaddr *to);
#else
int so_ip_recv(struct usock *up,struct mbuf_s **bpp,struct sockaddr *from,
		int *fromlen);
int so_ip_send(struct usock *up,struct mbuf_s **bp,struct sockaddr *to);
#endif
int so_ip_qlen(struct usock *up,int rtx);
int so_ip_close(struct usock *up);
int checkipaddr(struct sockaddr *name,int namelen);
char *ippsocket(struct sockaddr *p);

/* In locsocket.c: */
int so_los(struct usock *up,int protocol);
int so_lod(struct usock *up,int protocol);
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
int so_los_recv(struct usock *up,void *buf,int cnt,struct sockaddr *from,
		int *fromlen);
int so_lod_recv(struct usock *up,void *buf,int cnt,struct sockaddr *from,
		int *fromlen);
int so_los_send(struct usock *up,void *buf,int cnt,struct sockaddr *to);
int so_lod_send(struct usock *up,void *buf,int cnt,struct sockaddr *to);
#else
int so_los_recv(struct usock *up,struct mbuf_s **bpp,struct sockaddr *from,
		int *fromlen);
int so_lod_recv(struct usock *up,struct mbuf_s **bpp,struct sockaddr *from,
		int *fromlen);
int so_los_send(struct usock *up,struct mbuf_s **bp,struct sockaddr *to);
int so_lod_send(struct usock *up,struct mbuf_s **bp,struct sockaddr *to);
#endif
int so_los_qlen(struct usock *up,int rtx);
int so_lod_qlen(struct usock *up,int rtx);
int so_los_shut(struct usock *up,int how);
int so_lod_shut(struct usock *up,int how);
int so_los_close(struct usock *up);
int so_lod_close(struct usock *up);
char *lopsocket(struct sockaddr *p);
int so_loc_stat(struct usock *up);

/* In nrsocket.c: */
int so_n3_sock(struct usock *up,int protocol);
int so_n4_sock(struct usock *up,int protocol);
int so_n4_listen(struct usock *up,int backlog);
int so_n3_conn(struct usock *up);
int so_n4_conn(struct usock *up);
int so_n3_recv(struct usock *up,struct mbuf_s **bpp,struct sockaddr *from,
		int *fromlen);
int so_n4_recv(struct usock *up,struct mbuf_s **bpp,struct sockaddr *from,
		int *fromlen);
int so_n3_send(struct usock *up,struct mbuf_s **bp,struct sockaddr *to);
int so_n4_send(struct usock *up,struct mbuf_s **bp,struct sockaddr *to);
int so_n3_qlen(struct usock *up,int rtx);
int so_n4_qlen(struct usock *up,int rtx);
int so_n4_kick(struct usock *up);
int so_n4_shut(struct usock *up,int how);
int so_n3_close(struct usock *up);
int so_n4_close(struct usock *up);
int checknraddr(struct sockaddr *name,int namelen);
char *nrpsocket(struct sockaddr *p);
char *nrstate(struct usock *up);
int so_n4_stat(struct usock *up);

/* In tcpsock.c: */
int so_tcp(struct usock *up,int protocol);
int so_tcp_listen(struct usock *up,int backlog);
int so_tcp_conn(struct usock *up);
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
int so_tcp_recv(struct usock *up,void *buf,int cnt,struct sockaddr *from,
		int *fromlen);
int so_tcp_send(struct usock *up,void *buf,int cnt,struct sockaddr *to);
#else
int so_tcp_recv(struct usock *up,struct mbuf_s **bpp,struct sockaddr *from,
		int *fromlen);
int so_tcp_send(struct usock *up,struct mbuf_s **bp,struct sockaddr *to);
#endif
int so_tcp_qlen(struct usock *up,int rtx);
int so_tcp_kick(struct usock *up);
int so_tcp_shut(struct usock *up,int how);
int so_tcp_close(struct usock *up);
char *tcpstate(struct usock *up);
int so_tcp_stat(struct usock *up);

/* In udpsocket.c: */
int so_udp(struct usock *up,int protocol);
int so_udp_bind(struct usock *up);
int so_udp_conn(struct usock *up);
#if 1 /* now that we don't have to support recv_mbuf() or send_mbuf() */
int so_udp_recv(struct usock *up,void *buf,int cnt,struct sockaddr *from,
		int *fromlen);
int so_udp_send(struct usock *up,void *buf,int cnt,struct sockaddr *to);
#else
int so_udp_recv(struct usock *up,struct mbuf_s **bpp,struct sockaddr *from,
		int *fromlen);
int so_udp_send(struct usock *up,struct mbuf_s **bp,struct sockaddr *to);
#endif
int so_udp_qlen(struct usock *up,int rtx);
int so_udp_shut(struct usock *up,int how);
int so_udp_close(struct usock *up);
int so_udp_stat(struct usock *up);

/* Nick */
/* unsigned char deref_s(struct usock *up); */

#endif /* _USOCK_H */
