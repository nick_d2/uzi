/* SYSCALLS.H
 */
#ifndef __SYSCALLS_H
#define __SYSCALLS_H
#ifndef __TYPES_H
#include <sys/types.h>
#endif
#ifndef __SIGNAL_H
#include <signal.h>
#endif

#ifndef __VENDOR_H /* Nick */
#include <vendor.h> /* Nick */
#endif /* Nick */

#ifndef __DOS__

/*RPB*/
#if 0
extern int errno;
#endif
/*RPB*/

#ifndef __STDLIB_H
extern void _exit __P((int val));
#endif

extern int access __P((char *path, int mode));
extern int alarm __P((uint secs));
extern int brk __P((char *addr));
extern int chdir __P((char *dir));
extern int chroot __P((char *dir));
extern int chmod __P((char *path, mode_t mode));
extern int chown __P((char *path, int owner, int group));
extern int close __P((int uindex));
extern int creat __P((const char *name, mode_t mode));
extern int dup __P((int oldd));
extern int dup2 __P((int oldd, int newd));
extern int execve __P((char *name, char *argv[], char *envp[]));
extern int fork __P((void));
extern int fstat __P((int fd, struct stat *buf)); /* Nick void *buf)); */
#if 1 /* Nick free bitmap */
extern int falign __P((int fd, int parm));
#endif
extern int getfsys __P((int dev, void *buf));
extern int getgid __P((void));
extern int getpid __P((void));
extern int getppid __P((void));
extern int getuid __P((void));
extern int geteuid __P((void));
extern int getegid __P((void));
extern int getprio __P((void));
#ifndef SKIP_SYSCALLS
extern int ioctl __P((int fd, int request, ...));
#endif
extern int kill __P((int pid, int sig));
extern int link __P((const char *oldname, const char *newname));
extern int symlink __P((char *oldname, char *newname));
extern int mknod __P((char *name, mode_t mode, int dev));
extern int mkfifo __P((char *name, mode_t mode));
extern int mount __P((char *spec, char *dir, int rwflag));
#ifndef SKIP_SYSCALLS
extern int open __P((const char *name, int flag, ...)); /* Nick uint */
#endif
extern int pause __P((void));
extern int pipe __P((int fildes[2]));
extern int read __P((int d, void *buf, size_t nbytes));
extern int sbrk __P((uint incr));
extern off_t lseek __P((int file, off_t offset, int whence));
extern int setgid __P((int gid));
extern int setuid __P((int uid));
extern int setprio __P((int pid, char prio));
extern sig_t signal __P((signal_t sig, sig_t func));
extern int stat __P((char *path, struct stat *buf)); /* Nick void *buf)); */
extern int stime __P((time_t *tvec));
extern void sync __P((void)); /* Nick int sync __P((void)); */
extern time_t time __P((time_t *tvec));
extern int times __P((struct tms *buf));
extern int utime __P((char *path, struct utimbuf *buf));
extern int umask __P((int mask));
extern int umount __P((char *spec));
extern int unlink __P((const char *path));
extern int wait __P((int *statloc));
extern int write __P((int d, const void *buf, size_t nbytes));
extern int reboot __P((char p1, char p2));
extern int systrace __P((int onoff));
#if 1 /* Nick */
extern int sysdebug __P((int onoff));
#endif

extern int _access __P((char *path, int mode));
extern int _alarm __P((uint secs));
extern int _brk __P((char *addr));
extern int _chdir __P((char *dir));
extern int _chroot __P((char *dir));
extern int _chmod __P((char *path, mode_t mode));
extern int _chown __P((char *path, int owner, int group));
extern int _close __P((int uindex));
extern int _creat __P((const char *name, mode_t mode));
extern int _dup __P((int oldd));
extern int _dup2 __P((int oldd, int newd));
extern int _execve __P((char *name, char *argv[], char *envp[]));
extern int _fork __P((void));
extern int _fstat __P((int fd, struct stat *buf)); /* Nick void *buf)); */
#if 1 /* Nick free bitmap */
extern int _falign __P((int fd, int parm));
#endif
extern int _getfsys __P((int dev, void *buf));
extern int _getgid __P((void));
extern int _getpid __P((void));
extern int _getppid __P((void));
extern int _getuid __P((void));
extern int _geteuid __P((void));
extern int _getegid __P((void));
extern int _getprio __P((void));
#ifndef SKIP_SYSCALLS
extern int _ioctl __P((int fd, int request, ...));
#endif
extern int _kill __P((int pid, int sig));
extern int _link __P((const char *oldname, const char *newname));
extern int _symlink __P((char *oldname, char *newname));
extern int _mknod __P((char *name, mode_t mode, int dev));
extern int _mkfifo __P((char *name, mode_t mode));
extern int _mount __P((char *spec, char *dir, int rwflag));
#ifndef SKIP_SYSCALLS
extern int _open __P((const char *name, int flag, ...)); /* Nick uint */
#endif
extern int _pause __P((void));
extern int _pipe __P((int fildes[2]));
extern int _read __P((int d, void *buf, size_t nbytes));
extern int _sbrk __P((uint incr));
extern off_t _lseek __P((int file, off_t offset, int whence));
extern int _setgid __P((int gid));
extern int _setuid __P((int uid));
extern int _setprio __P((int pid, char prio));
extern sig_t _signal __P((signal_t sig, sig_t func));
extern int _stat __P((char *path, struct stat *buf)); /* Nick void *buf)); */
extern int _stime __P((time_t *tvec));
extern void _sync __P((void)); /* Nick int sync __P((void)); */
extern time_t _time __P((time_t *tvec));
extern int _times __P((struct tms *buf));
extern int _utime __P((char *path, struct utimbuf *buf));
extern int _umask __P((int mask));
extern int _umount __P((char *spec));
extern int _unlink __P((const char *path));
extern int _wait __P((int *statloc));
extern int _write __P((int d, const void *buf, size_t nbytes));
extern int _reboot __P((char p1, char p2));
extern int _systrace __P((int onoff));
#if 1 /* Nick */
extern int _sysdebug __P((int onoff));
#endif

/* ensure IAR doesn't use regparms */
extern unsigned int __unix __P((unsigned int rq, ...));

#ifdef UZIX_MODULE
extern int modulereg __P((uint sig, int (*func)()));
extern int moduledereg __P((uint sig));
extern int modulecall __P((uint sig, uint fcn, char *args, int argsize));
extern int modulereply __P((uint sig, int fcn, char *repladdr));
extern int modulesendreply __P((int pid, int fcn, char *repladdr, int replysize));
#endif

#else	/* running under MS-DOS */

#include "types.h"
#include <io.h>
#include <dir.h>
#include <alloc.h>
#include <errno.h>
#include <fcntl.h>
#include <process.h>

extern int alarm(uint secs);
extern int chown(char *path, int owner, int group);
extern int fork(void);
extern int getfsys(int dev, void *buf);
extern int getgid(void);
extern int getppid(void);
extern int getuid(void);
extern int geteuid(void);
extern int getegid(void);
extern int getprio(void);
extern int kill(int pid, int sig);
extern int link(const char *name1, const char *name2);
extern int mknod(char *name, mode_t mode, int dev);
extern int mount(char *spec, char *dir, int rwflag);
extern int pause(void);
extern int pipe(int fildes[2]);
extern int setgid(int gid);
extern int setuid(int uid);
extern int stime(time_t *tvec);
extern void sync(void); /* Nick int sync(void); */
extern time_t time(time_t *tvec);
extern int times(struct tms *buf);
extern int umount(char *spec);
extern int wait(int *statloc);
extern int reboot(char p1, char p2);
extern int systrace(int onoff);

#endif	/* __DOS__ */
#endif	/* __SYSCALLS_H */
