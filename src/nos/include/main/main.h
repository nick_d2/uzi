#ifndef _MAIN_H
#define _MAIN_H

#include "kernel/process.h" /* can be removed later? */
#include "kernel/thread.h" /* can be removed later? */

/* Apr 04	RPB	Changes for intl' of strings */

extern _char *Badhost; /*RPB*/
extern char *Hostname;
extern _char *Nospace; /*RPB*/		/* Generic malloc fail message */

extern struct thread_s *Cmdpp;
extern struct thread_s *Display;
extern struct process_s *daemon_process_p; /* Nick */
#define driver_process_p daemon_process_p
/*extern struct process_s *driver_process_p;*/ /* Nick */
extern int main_exit;			/* from main program (flag) */

#endif /* _MAIN_H */
