#ifndef	_IFACE_H
#define	_IFACE_H

/* Apr 04	RPB	Changes for intl' of strings */

#ifndef	_GLOBAL_H
#include "nos/global.h"
#endif

#ifndef	_MBUF_H
#include "main/mbuf.h"
#endif

#if 1 /* moved here from x86/pktdrvr.h */
/* Packet driver interface classes */
#define	CL_NONE		0
#define	CL_ETHERNET	1
#define	CL_PRONET_10	2
#define	CL_IEEE8025	3
#define	CL_OMNINET	4
#define	CL_APPLETALK	5
#define	CL_SERIAL_LINE	6
#define	CL_STARLAN	7
#define	CL_ARCNET	8
#define	CL_AX25		9
#define	CL_KISS		10
#define CL_IEEE8023	11
#define CL_FDDI 	12
#define CL_INTERNET_X25 13
#define CL_LANSTAR	14
#define CL_SLFP 	15
#define	CL_NETROM	16
#define CL_PPP		17
#define	CL_QTSO		18
#define NCLASS		19
#endif

#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
#define OUTPUT_ENTRIES 16

struct output_entry_s
	{
	uint8 tos;
	int32 gateway;
	struct mbuf_s *mbuf_p;
	};

struct output_queue_s
	{
	struct output_entry_s buf[OUTPUT_ENTRIES]; /* Ring buffer */
	struct output_entry_s *wp;	/* Write pointer */
	struct output_entry_s *rp;	/* Read pointer */
	volatile unsigned int cnt;	/* Count of characters in buffer */
	unsigned int hiwat;		/* High water mark */
	unsigned long overrun;		/* Count of s/w fifo buffer overruns */
	};
#endif

/* Interface encapsulation mode table entry. An array of these structures
 * are initialized in config.c with all of the information necessary
 * to attach a device.
 */
struct iftype {
	char *name;		/* Name of encapsulation technique */
	int (*send)(struct mbuf_s **,struct iface_s *,int32,uint8);
				/* Routine to send an IP datagram */
	int (*output)(struct iface_s *,uint8 *,uint8 *,uint16,struct mbuf_s **);
				/* Routine to send link packet */
	char *(*format)(char *,uint8 *);
				/* Function that formats addresses */
	int (*scan)(uint8 *,char *);
				/* Reverse of format */
	int type;		/* Type field for network process */
	int hwalen;		/* Length of hardware address, if any */
	void (*rcvf)(struct iface_s *,struct mbuf_s **);
				/* Function that handles incoming packets */
	int (*addrtest)(struct iface_s *,struct mbuf_s *);
				/* Function that tests incoming addresses */
	void (*trace)(struct iface_s *,struct mbuf_s **,int);
				/* Function that decodes protocol headers */
	int (*dinit)(struct iface_s *,int32,int,char **);
				/* Function to initialize demand dialing */
	int (*dstat)(struct iface_s *);
				/* Function to display dialer status */
};
#define IFTYPES_MAX 5
extern struct iftype Iftypes[IFTYPES_MAX];

/* Interface control structure */
struct iface_s {
	struct iface_s *next;	/* Linked list pointer */
	char *name;		/* Ascii string with interface name */

	int32 addr;		/* IP address */
	int32 broadcast;	/* Broadcast address */
	int32 netmask;		/* Network mask */

	uint16 mtu;		/* Maximum transmission unit size */

	uint16 trace;		/* Trace flags */
#define	IF_TRACE_OUT	0x01	/* Output packets */
#define	IF_TRACE_IN	0x10	/* Packets to me except broadcast */
#define	IF_TRACE_ASCII	0x100	/* Dump packets in ascii */
#define	IF_TRACE_HEX	0x200	/* Dump packets in hex/ascii */
#define	IF_TRACE_NOBC	0x1000	/* Suppress broadcasts */
#define	IF_TRACE_RAW	0x2000	/* Raw dump, if supported */
	FILE *trfp;		/* Stream to trace to */

	struct iface_s *forw;	/* Forwarding interface for output, if rx only */

	struct thread_s *rxproc; /* Receiver process, if any */
	struct thread_s *txproc; /* IP send process */
	struct thread_s *supv;	/* Supervisory process, if any */

#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	struct output_queue_s outq; /* IP datagram transmission queue */
#else
	struct mbuf_s *outq;	/* IP datagram transmission queue */
#endif
	int outlim;		/* Limit on outq length */
	int txbusy;		/* Transmitter is busy */

	void *dstate;		/* Demand dialer link state, if any */
	int (*dtickle)(struct iface_s *ifp);
				/* Function to tickle dialer, if any */
	void (*dstatus)(struct iface_s *ifp);
				/* Function to display dialer state, if any */

	/* device_list dependent */
	int dev;		/* Subdevice number to pass to send */
				/* To device -- control */
	unsigned long (*ioctl)(struct escc_s *escc, int cmd, int set,
			unsigned long val);
				/* From device -- when status changes */
	int (*iostatus)(struct iface_s *ifp, int cmd, int32 val);
				/* Call before detaching */
	int (*stop)(struct escc_s *escc);
	uint8 *hwaddr;		/* device_list hardware address, if any */

	/* Encapsulation dependent */
	void *edv;		/* Pointer to protocol extension block, if any */
	int xdev;		/* Associated Slip or Nrs channel, if any */
	struct iftype *iftype;	/* Pointer to appropriate iftype entry */

				/* Routine to send an IP datagram */
	int (*send)(struct mbuf_s **,struct iface_s *,int32,uint8);
			/* Encapsulate any link packet */
	int (*output)(struct iface_s *,uint8 *,uint8 *,uint16,struct mbuf_s **);
			/* Send raw packet */
	int (*raw)(struct iface_s *,struct mbuf_s **);
			/* Display status */
	void (*show)(struct iface_s *);

	int (*discard)(struct iface_s *,struct mbuf_s **);
	int (*echo)(struct iface_s *,struct mbuf_s **);

	/* Counters */
	int32 ipsndcnt; 	/* IP datagrams sent */
	int32 rawsndcnt;	/* Raw packets sent */
	int32 iprecvcnt;	/* IP datagrams received */
	int32 rawrecvcnt;	/* Raw packets received */
	int32 lastsent;		/* Clock time of last send */
	int32 lastrecv;		/* Clock time of last receive */
};
extern struct iface_s *Ifaces;	/* Head of interface list */
extern struct iface_s  Loopback;	/* Optional loopback interface */
extern struct iface_s  Encap;	/* IP-in-IP pseudo interface */

/* Header put on front of each packet sent to an interface */
struct qhdr {
	uint8 tos;
	int32 gateway;
};

#if 0
extern char Noipaddr[];
#endif

#if 0 /* remove anext member from mbufs and replace enqueue(), dequeue() */
extern struct mbuf_s *Hopper;
#endif

/* In iface.c: */
int bitbucket(struct iface_s *ifp,struct mbuf_s **bp);
int if_detach(struct iface_s *ifp);
struct iface_s *if_lookup(char *name);
char *if_name(struct iface_s *ifp,_char *comment);
void if_tx(int dev,void *arg1,void *unused);
struct iface_s *ismyaddr(int32 addr);
void network(int i,void *v1,void *v2);
int nu_send(struct mbuf_s **bpp,struct iface_s *ifp,int32 gateway,uint8 tos);
int nu_output(struct iface_s *,uint8 *,uint8 *,uint16,struct mbuf_s **);
int setencap(struct iface_s *ifp,char *mode);

/* In config.c: */
unsigned int net_route(struct iface_s *ifp,struct mbuf_s **bpp);

#endif	/* _IFACE_H */
