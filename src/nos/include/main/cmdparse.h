#ifndef	_CMDPARSE_H
#define	_CMDPARSE_H

#include <libintl.h> /* for _char... ZILOG only (Nick) */

#define	NARG		20	/* Max number of args to commands */

struct cmds {
	_char *name;		/* Name of command */
	int (*func)(int argc,char *argv[],void *p);
				/* Function to execute command */
	int stksize;		/* Size of stack if subprocess, 0 if synch */
	int  argcmin;		/* Minimum number of args */
	_char *argc_errmsg;	/* Message to print if insufficient args */
};

#define CMDS_MAX 40
#define ATTAB_MAX 2
extern struct cmds Cmds[CMDS_MAX],/*Startcmds[],Stopcmds[],*/Attab[ATTAB_MAX];

/* In cmdparse.c: */
int cmdparse(struct cmds cmds[], char *line, void *p);
int subcmd(struct cmds tab[], int argc, char *argv[], void *p);
int setbool(int *var, _char *label, int argc, char *argv[]);
int bit16cmd(uint16 *bits, uint16 mask, _char *label, int argc, char *argv[]);
int setint(int *var, _char *label, int argc, char *argv[]);
int setlong(int32 *var, _char *label, int argc, char *argv[]);
int setshort(unsigned short *var, _char *label, int argc, char *argv[]);
int setuns(unsigned *var, _char *label, int argc, char *argv[]);
/* Nick added this: */
int setuchar(unsigned char *var, _char *label, int argc, char *argv[]);

#endif	/* _CMDPARSE_H */
