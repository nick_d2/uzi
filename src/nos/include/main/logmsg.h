#ifndef __LOGMSG_H
#define __LOGMSG_H

#include <libintl.h>

void logmsg(int s, _char *fmt, ...);

#endif /* __LOGMSG_H */

