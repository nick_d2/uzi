#ifndef	_MBUF_H
#define	_MBUF_H

#include <stdio.h>
#include "nos/global.h"
#include "kernel/object.h"

extern unsigned Ibufsize;	/* Size of interrupt buffers to allocate */
extern int Nibufs;		/* Number of interrupt buffers to allocate */
extern long Pushdowns;		/* Total calls to pushdown() */
extern long Pushalloc;		/* Calls to pushdown that call malloc() */

/* Basic message buffer structure */
struct mbuf_s {
	struct object_s object;	/* Header containing refs and magic number */
	struct mbuf_s *next;	/* Links mbufs belonging to single packets */
#if 0 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	struct mbuf_s *anext;	/* Links packets on queues */
#endif
	uint16 size;		/* Size of associated data buffer */
#if 0 /* don't do dup, replace it with a copy */
	int refcnt;		/* Reference count */
	struct mbuf_s *dup;	/* Pointer to duplicated mbuf */
#endif
	uint8 *data;		/* Active working pointers */
	uint16 cnt;
};

#if 1 /* Nick */
#define	PULLCHAR(bpp) pullchar((bpp))
/*	((bpp) && *(bpp) && (*(bpp))->cnt > 1 ? \
		((*(bpp))->cnt--, *(*(bpp))->data++) : pullchar((bpp))) */
#else
#define	PULLCHAR(bpp)\
 ((bpp) != NULL && (*bpp) != NULL && (*bpp)->cnt > 1 ? \
 ((*bpp)->cnt--,*(*bpp)->data++) : pullchar(bpp))
#endif

/* In mbuf.c: */
struct mbuf_s *alloc_mbuf(uint16 size);
struct mbuf_s *free_mbuf(struct mbuf_s **bpp);
struct mbuf_s *free_m(struct mbuf_s *bp);

struct mbuf_s *ambufw(uint16 size);
struct mbuf_s *copy_p(struct mbuf_s *bp,uint16 cnt);
uint16 dup_p(struct mbuf_s **hp,struct mbuf_s *bp,uint16 offset,uint16 cnt);
uint16 extract(struct mbuf_s *bp,uint16 offset,void *buf,uint16 len);
struct mbuf_s *free_p(struct mbuf_s **bpp);
uint16 len_p(struct mbuf_s *bp);
void trim_mbuf(struct mbuf_s **bpp,uint16 length);
/*int write_p(FILE *fp,struct mbuf_s *bp);*/

/*struct mbuf_s *dequeue(struct mbuf_s **q);*/
/*void enqueue(struct mbuf_s **q,struct mbuf_s **bpp);*/
/*void free_q(struct mbuf_s **q);*/
uint16 len_q(struct mbuf_s *bp);

struct mbuf_s *qdata(void *data,uint16 cnt);
uint16 dqdata(struct mbuf_s *bp,void *buf,unsigned cnt);

void append(struct mbuf_s **bph,struct mbuf_s **bpp);
void pushdown(struct mbuf_s **bpp,void *buf,uint16 size);
uint16 pullup(struct mbuf_s **bph,void *buf,uint16 cnt);

#if 1 /* Nick */
#define	pullchar(bpp) pull8((bpp))
#else
#define	pullchar(x) pull8(x)
#endif
int pull8(struct mbuf_s **bpp);       /* returns -1 if nothing */
long pull16(struct mbuf_s **bpp);	/* returns -1 if nothing */
int32 pull32(struct mbuf_s **bpp);	/* returns  0 if nothing */

uint16 get16(uint8 *cp);
int32 get32(uint8 *cp);
uint8 *put16(uint8 *cp,uint16 x);
uint8 *put32(uint8 *cp,int32 x);

void iqstat(void);
void refiq(void);
void mbuf_crunch(struct mbuf_s **bpp);

void mbufsizes(void);
void mbufstat(void);
void mbuf_garbage(int red);

#define AUDIT(bp)       audit(bp,__FILE__,__LINE__)

#endif	/* _MBUF_H */
