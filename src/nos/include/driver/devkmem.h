/* devkmem.h by Nick for NOS/UZI project */

#ifndef __DEVKMEM_H
#define __DEVKMEM_H

#include <setjmp.h> /* for jmp_buf */
#include <stddef.h> /* for size_t */

extern struct device_char_s dev_kmem;

size_t dev_kmem_read(jmp_buf error, unsigned char minor,
		void *data, size_t count);

#endif /* __DEVKMEM_H */

