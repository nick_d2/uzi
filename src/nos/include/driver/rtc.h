/* rtc.h by Nick for NOS/UZI project */

#ifndef __RTC_H
#define __RTC_H

/* can contain either msdos or proper unix style time/date (soon!!) */
/* uzi name prefix avoids conflict when compiling for Windows, Linux etc */
typedef unsigned long rtctime_t;

extern rtctime_t Rtc;
extern unsigned char Rtc_protect;

rtctime_t rtc_time(void);
void sttime(rtctime_t *tvec);

#endif /* __RTC_H */

