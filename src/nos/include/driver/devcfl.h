/* devcfl.h by Nick for NOS/UZI project */

#ifndef __DEVCFL_H
#define __DEVCFL_H

#include <setjmp.h> /* for jmp_buf */
#include "driver/bufpool.h" /* for blkno_t */

extern struct device_block_s dev_cfl;

extern unsigned char *Cfl_data;
extern unsigned char *Cfl_message;
extern unsigned int Cfl_error;

void dev_cfl_open(jmp_buf error, unsigned char minor);
void dev_cfl_close(jmp_buf error, unsigned char minor);
void dev_cfl_read(jmp_buf error, unsigned char minor,
		void *data, blkno_t block);
void dev_cfl_write(jmp_buf error, unsigned char minor,
		void *data, blkno_t block);

#endif /* __DEVCFL_H */

