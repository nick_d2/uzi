/* devzero.h by Nick for NOS/UZI project */

#ifndef __DEVZERO_H
#define __DEVZERO_H

#include <setjmp.h> /* for jmp_buf */
#include <stddef.h> /* for size_t */

extern struct device_char_s dev_zero;

size_t dev_zero_read(jmp_buf error, unsigned char minor,
		void *data, size_t count);

#endif /* __DEVZERO_H */

