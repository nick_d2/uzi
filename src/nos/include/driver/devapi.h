/* devapi.h by Nick for NOS/UZI project */

#ifndef __DEVAPI_H
#define __DEVAPI_H

#include <setjmp.h> /* for jmp_buf */
#include <stddef.h> /* for size_t */
#include "driver/device.h" /* for struct device_char_s */
#include "kernel/thread.h" /* for struct thread_s */
#include "z80/asci.h" /* for struct dma, struct fifo */

struct api_minor_s
	{
	unsigned char refs;
	unsigned char minor;
	unsigned char nobl;
	struct thread_s *thread_p;
	struct mbuf_s *queue_p;
	};

#define API_MINOR_LIST 9

extern struct api_minor_s *api_minor_list[API_MINOR_LIST];
extern unsigned char api_minor_bufsize[API_MINOR_LIST];

extern struct device_char_s dev_api;

void dev_api_open(jmp_buf error, unsigned char minor);
void dev_api_close(jmp_buf error, unsigned char minor);
size_t dev_api_read(jmp_buf error, unsigned char minor,
		void *data, size_t count);
size_t dev_api_write(jmp_buf error, unsigned char minor,
		void *data, size_t count);
void dev_api_ioctl(jmp_buf error, unsigned char minor,
		unsigned int request, void *data);
void api_minor_thread(int argc, void *argv, void *p);
void api_minor_thread_lcd0(int argc, void *argv, void *p);
void api_minor_thread_cfl0(int argc, void *argv, void *p);
void api_minor_garbage(int red);

#endif /* __DEVAPI_H */

