/* devram.h by Nick for NOS/UZI project */

#ifndef __DEVRAM_H
#define __DEVRAM_H

#include <setjmp.h> /* for jmp_buf */
#include "driver/bufpool.h" /* for blkno_t */

extern struct device_block_s dev_ram;

void dev_ram_read(jmp_buf error, unsigned char minor,
		void *data, blkno_t block);
void dev_ram_write(jmp_buf error, unsigned char minor,
		void *data, blkno_t block);

#endif /* __DEVRAM_H */

