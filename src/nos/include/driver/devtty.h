/* devtty.h by Nick for NOS/UZI project */

#ifndef __DEVTTY_H
#define __DEVTTY_H

#include <setjmp.h> /* for jmp_buf */
#include <sgtty.h> /* for struct sgtty */
#include <stddef.h> /* for size_t */
#include "driver/device.h" /* for struct device_char_s */
#include "kernel/session.h" /* for struct session_s */
#include "kernel/thread.h" /* for struct thread_s */

#define TTY_MINOR_LIST 6
#define	TTY_LINE_SIZE 256

struct ttystate
	{
	unsigned int echo:1;	/* Keyboard local echoing? */
	unsigned int edit:1;	/* Local editing? */
	unsigned int crnl:1;	/* Translate input cr to lf? */
	unsigned int exnl:1;	/* Nick, expand output nl to cr/nl? */
	unsigned int nobl:1;	/* Nick, non blocking?  (Only affects read) */
	unsigned int ctrl:1;	/* Nick, skip input ctrl char processing? */
	unsigned int tnet:1;	/* Nick, skip telnet sequences in input */
	};

struct tty_minor_s
	{
	unsigned char refs;
	unsigned char minor;

	struct sgttyb param;
	struct tchars chars;

	struct ttystate state;

	unsigned char type;	/* _FL_ASY, _FL_SOCK */
	void *type_p;		/* -> struct asy, struct usock */

#ifndef JOB_CONTROL /* bit of a hack */
	unsigned char ctrl_c;
#else
	struct session_s *session_p;
	struct session_s *root_session_p;
	struct session_s *last_session_p;
#endif

	struct thread_s *thread_p;
	struct mbuf_s *queue_p;

	unsigned char *lp;	/* Pointer into same */
	unsigned char line[TTY_LINE_SIZE];
	};

extern struct tty_minor_s *tty_minor_list[TTY_MINOR_LIST];

extern struct device_char_s dev_tty;

void dev_tty_open(jmp_buf error, unsigned char minor);
void dev_tty_close(jmp_buf error, unsigned char minor);
size_t dev_tty_read(jmp_buf error, unsigned char minor,
		void *data, size_t count);
size_t dev_tty_write(jmp_buf error, unsigned char minor,
		void *data, size_t count);
void dev_tty_ioctl(jmp_buf error, unsigned char minor,
		unsigned int request, void *data);
void tty_minor_thread(int argc, void *argv, void *p);
void tty_minor_garbage(int red);
int ttydriv(struct tty_minor_s *tm_p, unsigned char c);
int tty_read(struct tty_minor_s *tm_p, void *data, size_t count);
int tty_write(struct tty_minor_s *tm_p, void *data, size_t count);
int tty_fputs(char *string, struct tty_minor_s *tm_p);
int _tty_fputs(_char *string, struct tty_minor_s *tm_p);
int get_tty(struct tty_minor_s *tm_p);
int put_tty(unsigned char c, struct tty_minor_s *tm_p);
#ifdef JOB_CONTROL
struct session_s *tty_focus(struct tty_minor_s *tm_p,
		struct session_s *process_p);
#endif

#endif /* __DEVTTY_H */

