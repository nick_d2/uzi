/* bufpool.h by Nick for NOS/UZI project */

#ifndef __BUFPOOL_H
#define __BUFPOOL_H

#include <setjmp.h>
#include "asm.h" /* "z80/asm.h" but we now rely on tradcpp -I../include/z80 */

/* BUFSIZE has been defined already by the C runtime library */
typedef unsigned short ino_t;	/* Can have 65536 inodes in fs */
typedef unsigned short blkno_t;	/* Can have 65536 BUFSIZE-byte blocks in fs */
typedef unsigned short dev_t;	/* device_list number */

#define MINOR(dev)		((uchar)(dev))
#define MAJOR(dev)		((uchar)((dev) >> 8))
#define MKDEV(major, minor)	(((uint)(uchar)(major) << 8) | (uchar)(minor))

#define NULLINO ((ino_t)0)
#define NULLBLK ((blkno_t)-1)
#define NULLDEV ((dev_t)-1)

/* structure of an in-core buffered disk block */
struct block_buffer_s
	{
#if 1
	unsigned char *bf_data;
#endif
#if 0
	unsigned char bf_data[BUFSIZE]; /* This MUST BE first ! */
#endif
	dev_t bf_dev; 	/* device of this block */
	blkno_t bf_blk; 	/* and block number on device */
	unsigned char bf_dirty;	/* buffer changed flag */
	unsigned char bf_busy;	/* buffer processing in progress */
	/*unsigned char bf_prio;*/	/* buffer must be in memory (for wargs) */
	unsigned int bf_time;	/* LRU time stamp */
	/*struct s_blkbuf *bf_next;*/ /* LRU free list pointer */
#if 0 /* trying this last, as an experiment to test the IAR aliasing fixes */
	unsigned char bf_data[BUFSIZE];
#endif
	};

#define BLOCK_BUFFER_LIST 8 /* max number of disk blocks in the buffer cache */

#if 1
extern struct block_buffer_s block_buffer_list[BLOCK_BUFFER_LIST];
#else
extern struct block_buffer_s *block_buffer_list[BLOCK_BUFFER_LIST];
#endif

extern unsigned int bufclock;		/* time-stamp counter for LRU */

extern unsigned int buf_hits;		/* buffer pool hits */
extern unsigned int buf_miss;		/* buffer pool misses */
extern unsigned int buf_flsh;		/* buffer pool flushes */

void blk_acquire(struct block_buffer_s *blkbuf_p);
void blk_release(struct block_buffer_s *blkbuf_p);
struct block_buffer_s *bread(jmp_buf error, dev_t dev, blkno_t blk,
		unsigned char rewrite);
void bfree(jmp_buf error, struct block_buffer_s *bp, unsigned char dirty);
struct block_buffer_s *zerobuf(jmp_buf error);
void bufsync(dev_t dev, unsigned char drop);
struct block_buffer_s *bfind(dev_t dev, blkno_t blk);
int bufdump(int argc, char *argv[], void *p);
void bufpool_garbage(int red);

#endif /* __BUFPOOL_H */

