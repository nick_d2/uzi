#ifndef __PATHS_H
#define __PATHS_H

#define _PATH_DEV	"/dev/"
#define _PATH_DEVNULL	"/dev/null"
#define _PATH_CONSOLE	"/dev/console"
#define _PATH_TTY	"/dev/tty"
#if 1 /* Nick */
#define _PATH_PRINTER	"/dev/printer"
#endif

#define _PATH_TMP	"/tmp/"
#define _PATH_UTMP	"/var/run/utmp"

#define _PATH_INIT	"/bin/init"
#define _PATH_LOGIN	"/bin/login"
#define _PATH_BSHELL	"/bin/sh"
#define _PATH_DEFPATH	".:/bin:/usr/bin:/etc:/usr/local/bin"

#if 1 /* Nick */
#define _PATH_LIBERR	"/lib/liberror.txt"
#else
#define _PATH_LIBERR	"/usr/lib/liberror.txt"
#endif

#define _PATH_TERMCAP	"/etc/termcap"
#define _PATH_PASSWD	"/etc/passwd"
#define _PATH_GROUP	"/etc/group"

#define _PATH_HOME	"/home"

#endif /* __PATHS_H */
