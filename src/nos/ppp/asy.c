/* Generic serial line interface routines
 * Copyright 1992 Phil Karn, KA9Q
 *
 * Apr 04	RPB	Po-ed most of the remaining strings
 */
#include <stdio.h>
#include "nos/global.h"
#include "kernel/thread.h"
#include "main/iface.h"
#include "internet/netuser.h"
#include "ppp/slhc.h"
#include "z80/asci.h"
/*#include "ax25/ax25.h"*/
/*#include "ax25/kiss.h"*/
/*#include "netrom/nrs.h"*/
/*#include "x86/pktdrvr.h"*/
#include "ppp/slip.h"
#include "ppp/ppp.h"
#include "main/commands.h"
#include "main/config.h" /* Nick for SLIP, PPP etc macros */
#include "main/main.h" /* for driver_process_p */
#include <libintl.h>
#include "po/messages.h"

static int asci_detach(struct iface_s *ifp);

/* Attach a serial interface to the system
 * argv[0]: hardware type, must be "asci"
 * argv[1]: I/O address, e.g., "0x3f8"
 * argv[2]: vector, e.g., "4", or "fp1" for port 1 on a 4port card
 * argv[3]: mode, may be:
 *		"slip" (point-to-point SLIP)
 *		"kissui" (AX.25 UI frame format in SLIP for raw TNC)
 *		"ax25ui" (same as kissui)
 *		"kissi" (AX.25 I frame format in SLIP for raw TNC)
 *		"ax25i" (same as kissi)
 *		"nrs" (NET/ROM format serial protocol)
 *		"ppp" (Point-to-Point Protocol, RFC1171, RFC1172)
 * argv[4]: interface label, e.g., "sl0"
 * argv[5]: receiver ring buffer size in bytes
 * argv[6]: maximum transmission unit, bytes
 * argv[7]: interface speed, e.g, "9600"
 * argv[8]: optional flags,
 *		'v' for Van Jacobson TCP header compression (SLIP only,
 *		    use ppp command for VJ compression with PPP);
 *		'c' for cts flow control
 *		'r' for rlsd (cd) detection
 */
int
asci_attach(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct iface_s *ifp;
	int dev;
	int trigchar = -1;
	int cts,rlsd;
	struct asci_mode_s *ap;
	char *cp;
	int base;
	int irq;
#if 0 /* is only needed for fourport implementation */
	struct asci_s *ascip;
#endif
	int i,n;
	int chain;
#ifdef SLIP
	int xdev;
#endif

	if(if_lookup(argv[4]) != NULL){
		_printf(_("Interface %s already exists\n"),argv[4]);
		return 0; /*-1;*/
	}
	if(setencap(NULL,argv[3]) == -1){
		_printf(_("Unknown encapsulation %s\n"),argv[3]);
		return 0; /*-1;*/
	}
	/* Find unused asci control block */
	for(dev=0;dev < ASCI_MAX;dev++){
		if(Asci[dev] == NULL || Asci[dev]->iface == NULL)
			break;
	}
	if(dev >= ASCI_MAX){
		_printf(_("Too many ascinch controllers\n"));
		return 0; /*-1;*/
	}
#if 0 /* is only needed for fourport implementation */
	ascip = &Asci[dev];
#endif

	base = htoi(argv[1]);
#if 0 /* is only needed for fourport implementation */
	if(*argv[2] == 's'){
		/* This is a port on a 4port card with shared interrupt */
		for(i=0;i<FPORT_MAX;i++){
			if(base >= Fport[i].base && base < Fport[i].base+32){
				n = (base - Fport[i].base) >> 3;
				Fport[i].asci[n] = ascip;
				break;
			}
		}
		if(i == FPORT_MAX){
			_printf(_("%x not a known 4port address\n"));
			return 0; /*-1;*/
		}
		irq = -1;
	} else
#endif
		irq = atoi(argv[2]);

	/* Create interface structure and fill in details */
	ifp = (struct iface_s *)callocw(1,sizeof(struct iface_s));
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	ifp->outq.rp = ifp->outq.buf;
	ifp->outq.wp = ifp->outq.buf;
#endif
#if 0 /* a good idea, but argv[8] is optional, sort this out later */
	if(argc > 9)
		{
		ifp->addr = resolve(argv[9]);
		}
	else
#endif
		{
		ifp->addr = Ip_addr;
		}
	ifp->name = strdupw(argv[4]);
	ifp->mtu = atoi(argv[6]);
	ifp->dev = dev;
	ifp->stop = asci_detach;

#ifdef SLIP
	if (stricmp(argv[3], "SLIP") == 0)
		{
		setencap(ifp,"SLIP");
		ifp->ioctl = asci_ioctl;

		for(xdev = 0;xdev < SLIP_MAX;xdev++)
			{
			if (Slip[xdev].iface == ifp ||
					Slip[xdev].iface == NULL)
				{
				break;
				}
			}

		Slip[xdev].type = CL_SERIAL_LINE;
		Slip[xdev].send = asci_send;
		Slip[xdev].get = get_asci;

#ifdef VJCOMPRESS
		if(argc > 8 && strchr(argv[8],'v') != NULL)
			{
			Slip[xdev].escaped |= SLIP_VJCOMPR;
			Slip[xdev].slcomp = slhc_init(16,16);
			}
#else
		Slip[xdev].slcomp = NULL;
#endif	/* VJCOMPRESS */

		if(slip_init(ifp) != 0)
			{
			_printf(_("%s: mode %s Init failed\n"),
					ifp->name,argv[3]);
			if_detach(ifp); /* should just free(ifp); instead? */
			return 0; /*-1;*/
			}
		}
	else
#endif
#ifdef PPP
	if (stricmp(argv[3], "PPP") == 0)
		{
		setencap(ifp,"PPP");
		ifp->ioctl = asci_ioctl;

#if 0 /* hmm.. where do we put these? need to modify ppp_init()... too hard? */
		Slip[xdev].type = CL_SERIAL_LINE;
		Slip[xdev].send = asci_send;
		Slip[xdev].get = get_asci;
#endif

		if(ppp_init(ifp) != 0)
			{
			_printf(_("%s: mode %s Init failed\n"),
					ifp->name,argv[3]);
			if_detach(ifp); /* should just free(ifp); instead? */
			return 0; /*-1;*/
			}
		}
	else
#endif
		{
		setencap(ifp,argv[3]);

		/* Look for the interface mode in the table */
		for(ap = Asci_mode;ap->name != NULL;ap++)
			{
			if(stricmp(argv[3],ap->name) == 0)
				{
				trigchar = ap->trigchar;
				if((*ap->init)(ifp) != 0)
					{
					_printf(_("%s: mode %s Init failed\n"),
							ifp->name,argv[3]);
					if_detach(ifp);
					/* should just free(ifp); instead? */
					return 0; /*-1;*/
					}
				break;
				}
			}
		if(ap->name == NULL)
			{
			_printf(_("Mode %s unknown for interface %s\n"),
					argv[3],argv[4]);
			if_detach(ifp); /* should just free(ifp); instead? */
			return 0; /*-1;*/
			}
		}

	/* Link in the interface */
	ifp->next = Ifaces;
	Ifaces = ifp;

	cts = rlsd = 0;
	if(argc > 8){
		if(strchr(argv[8],'c') != NULL)
			cts = 1;
		if(strchr(argv[8],'r') != NULL)
			rlsd = 1;
	}
	if(strchr(argv[2],'c') != NULL)
		chain = 1;
	else
		chain = 0;
	if (asci_init(dev,ifp,base,irq,(uint16)atol(argv[5]),
			trigchar,atol(argv[7]),cts,rlsd,chain) == NULL)
		{
		_printf(_("Not enough memory\n"));
		/* note: Asci[ifp->dev].iface is NULL so asci_stop() is a nop */
		if_detach(ifp);
		return 0; /*-1;*/
		}
	cp = if_name(ifp, _(" tx"));
	ifp->txproc = process_thread_create(driver_process_p, cp, 384 /*768*/,
			if_tx, 0, ifp, NULL, 0);
	free(cp);
	if (ifp->txproc == NULL)
		{
		_printf(_("Not enough memory\n"));
		/* note: Asci[ifp->dev].iface is true so asci_stop() is needed */
		if_detach(ifp);
		return 0; /*-1;*/
		}
	return 0;
}

static int
asci_detach(ifp)
struct iface_s *ifp;
{
	struct asci_mode_s *ap;
	struct asci_s *asci_p;

	if(ifp == NULL)
		{
		return -1;
		}

	asci_p = Asci[ifp->dev];
	if (asci_p->iface)
		{
		asci_stop(asci_p);
		}

	/* Call mode-dependent routine */
	for(ap = Asci_mode;ap->name != NULL;ap++)
		{
		if(ifp->iftype != NULL
		 && stricmp(ifp->iftype->name,ap->name) == 0
		 && ap->free != NULL)
			{
			(*ap->free)(ifp);
			}
		}

	return 0;
}

