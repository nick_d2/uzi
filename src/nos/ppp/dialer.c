/* -RPB-
 * Automatic line dialer for escc ports running SLIP, PPP, etc.
 * -RPB-
 *
 * Copyright 1991 Phil Karn, KA9Q
 *
 *	Mar '91	Bill Simpson & Glenn McGregor
 *		completely re-written;
 *		human readable control file;
 *		includes wait for string, and speed sense;
 *		dials immediately when invoked.
 *	May '91 Bill Simpson
 *		re-ordered command line;
 *		allow dial only;
 *		allow inactivity timeout without ping.
 *	Sep '91 Bill Simpson
 *		Check known DTR & RSLD state for redial decision
 *	Mar '92	Phil Karn
 *		autosense modem control stuff removed
 *		Largely rewritten to do demand dialing
 *	Nov '03 RPB
 *		Adapted for use with ESCC iso ASCI device
 *		Added an initialization script to the dialer command
 *		Added the dodial_exec routine
 *	Apr '04 RPB
 *		Po-ed most of the remaining strings
 */

#include <stdio.h>
#include <errno.h> /* Nick */
#include <ctype.h>
#include <stdlib.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include "main/timer.h"
#include "kernel/thread.h"
#include "main/iface.h"
#include "internet/netuser.h"
#include "z80/escc.h"
/*#include "driver/devtty.h"*/
#include "socket/socket.h"
#include "main/cmdparse.h"
#include "main/devparam.h"
#include "main/files.h"
#include "dump/trace.h"
#include "main/commands.h"
#include "ppp/dialer.h"
#include "ppp/ppp.h"
#include "main/main.h" /* for driver_process_p */
#include <libintl.h>
#include "po/messages.h"
#include "main/logmsg.h"

#define DIAL_CMDS_MAX 7

#ifdef MODULE
#define STATIC
extern struct cmds Dial_cmds[DIAL_CMDS_MAX];
#else
#define STATIC static
#define MODULE_Dial_cmds
#define MODULE_sd_init
#define MODULE_sd_stat
#define MODULE_dialer_kick
#define MODULE_dropline
#define MODULE_dropit
#define MODULE_sd_answer
#define MODULE_redial
#define MODULE_dodial_control
#define MODULE_dodial_send
#define MODULE_dodial_speed
#define MODULE_dodial_status
#define MODULE_dodial_wait
#define MODULE_dodial_exec
#endif

STATIC void dropline(void *p);
STATIC void dropit(int i, void *p, void *u);
STATIC int redial(struct iface_s *ifp, char *file);
STATIC int dodial_control(int argc, char *argv[], void *p);
STATIC int dodial_send(int argc, char *argv[], void *p);
STATIC int dodial_speed(int argc, char *argv[], void *p);
STATIC int dodial_status(int argc, char *argv[], void *p);
STATIC int dodial_wait(int argc, char *argv[], void *p);
STATIC int dodial_exec(struct iface_s *ifp, int argc, char *argv[]);

#ifdef MODULE_Dial_cmds
STATIC struct cmds Dial_cmds[DIAL_CMDS_MAX] = {
	N_(""),		donothing,	0, 0, N_(""),
	N_("control"),	dodial_control,	0, 2, N_("control up|down"),
	N_("send"),	dodial_send,	0, 2,
			N_("send \"string\" [<milliseconds>]"),
	N_("speed"),	dodial_speed,	0, 2, N_("speed <bps>"),
	N_("status"),	dodial_status,	0, 2,
			N_("status up|down|open|close|listen"),
	N_("wait"),	dodial_wait,	0, 2,
			N_("wait <milliseconds> [ \"string\" [speed] ]"),
	NULL,		NULL,		0, 0, N_("Unknown command"),
};
#endif

/* -RPB-
 * Set up demand dialing on an escc link. Called from dodialer command
 * in iface.c.
 * -RPB-
 */
#ifdef MODULE_sd_init
int
sd_init(ifp,timeout,argc,argv)
struct iface_s *ifp;
int32 timeout;
int argc;
char *argv[];
{
	struct escc_s *ap;
	struct dialer_s *dialer;
	char *cp;

	if (ifp->dev > Esccinfo.maxchan ||
			Esccchan[ifp->dev]->async.iface != ifp){
		/* "Can't happen" */
		_printf(_("Interface %s not escc port\n"),
		           Esccchan[ifp->dev]->async.iface->name);
		return 1;
	}
	ap = Esccchan[ifp->dev];
	if (timeout != 0 && argc < 3){
		_printf(_("Usage: dial <iface> <timeout> <initfile> <raisefile> <dropfile> <ringfile>\n"));
		_printf(_("       dial <iface> 0\n"));
		return 1;
	}
	if (!ap->rlsd){
		_printf(_("Must set 'r' flag at attach time\n"));
		return 1;
	}
	if (ifp->dstate != NULL){
		/* Get rid of the old dialer info */
		dialer = (struct dialer_s *)ifp->dstate;
		stop_timer(&dialer->idle);
		if (dialer->initfile != NULL){
			free(dialer->initfile);
			dialer->initfile = NULL;
		}
		if (dialer->actfile != NULL){
			free(dialer->actfile);
			dialer->actfile = NULL;
		}
		if (dialer->dropfile != NULL){
			free(dialer->dropfile);
			dialer->dropfile = NULL;
		}
		if (dialer->ansfile != NULL){
			free(dialer->ansfile);
			dialer->ansfile = NULL;
		}
		free(ifp->dstate);
		ifp->dstate = NULL;
	}
	killproc(ifp->supv);
	ifp->supv = NULL;

	dialer = (struct dialer_s *)calloc(1,sizeof(struct dialer_s));
	ifp->dstate = dialer;

	ifp->dtickle = dialer_kick;
	set_timer(&dialer->idle,timeout);
	dialer->idle.func = dropline;
	dialer->idle.arg = ifp;
	if (timeout != 0){
		dialer->initfile = strdupw(argv[0]);
		dialer->actfile = strdupw(argv[1]);
		dialer->dropfile = strdupw(argv[2]);
		dialer->ansfile = strdupw(argv[3]);

		if (dialer->ansfile){
			cp = if_name(ifp,_(" answerer"));
			ifp->supv = process_thread_create(driver_process_p,
					cp, 512, sd_answer, ifp->dev, ifp,
					NULL, 0);
		}
	}

	/* Execute modem initialization commands */
	if (dialer->initfile)
		redial(ifp,dialer->initfile);

	return 0;
}
#endif

/* Display status of ascinch dialer. Called from dodialer command in
 * iface.c when invoked without full args
 */
#ifdef MODULE_sd_stat
int
sd_stat(ifp)
struct iface_s *ifp;
{
	struct dialer_s *dialer = (struct dialer_s *)ifp->dstate;
	struct escc_s *ap;

	if (dialer == NULL){
		_printf(_("No dialer active on %s\n"),ifp->name);
		return 1;
	}

	ap = Esccchan[ifp->dev];
	_printf(_("%s: %s,"),ifp->name, ap->status & DCD ? "UP":"DOWN");
	_printf(_(" idle timer %lu/%lu sec\n"),read_timer(&dialer->idle)/1000L,
	  dur_timer(&dialer->idle)/1000L);
	if (dialer->initfile != NULL)
		_printf(_("init script: %s\n"),dialer->initfile);
	if (dialer->actfile != NULL)
		_printf(_("up script: %s\n"),dialer->actfile);
	if (dialer->dropfile != NULL)
		_printf(_("down script: %s\n"),dialer->dropfile);
	if (dialer->ansfile != NULL)
		_printf(_("answer script: %s\n"),dialer->ansfile);
	_printf(_("Calls originated %lu, Calls answered %lu\n"),
		dialer->originates,dialer->answers);
	_printf(_("Calls timed out %lu, carrier transitions %lu\n"),
		dialer->localdrops,ap->cdchanges);
	return 0;
}
#endif

/* Called by interface output routine just before sending each packet */
#ifdef MODULE_dialer_kick
int
dialer_kick(ifp)
struct iface_s *ifp;
{
	struct escc_s *ascip;
	struct dialer_s *dialer;

	dialer = (struct dialer_s *)ifp->dstate;
	ascip = Esccchan[ifp->dev];

	stop_timer(&dialer->idle);

	if (ascip->rlsd && !(ascip->status & DCD) && dialer->actfile != NULL){
		/* Line down, try a redial */
		dialer->originates++;

		if ((redial(ifp,dialer->actfile) != 0) ||
				!(ascip->status & DCD)){
			/* Redial failed, drop line and return failure */
			dialer->localdrops++;
			redial(ifp,dialer->dropfile);
			return -1;
		}
	}
	start_timer(&dialer->idle);
	return 0;
}
#endif

/* Called when idle line timer expires -- executes script to drop line */
#ifdef MODULE_dropline
STATIC void
dropline(p)
void *p;
{
	/* Fork this off to prevent wedging the timer task */
	process_thread_create(driver_process_p, "dropit", 512, dropit, 0,
			p, NULL, 0);
}
#endif

#ifdef MODULE_dropit
STATIC void
dropit(i,p,u)
int i;
void *p;
void *u;
{
	struct iface_s *ifp = p;
	struct escc_s *ap;
	struct dialer_s *dialer;

	dialer = (struct dialer_s *)ifp->dstate;
 	ap = Esccchan[ifp->dev];
	if (ap->status & DCD){
		dialer->localdrops++;
		redial(ifp,dialer->dropfile);	/* Drop only if still up */
	}
}
#endif

#ifdef MODULE_sd_answer
void
sd_answer(dev,p1,p2)
int dev;
void *p1,*p2;
{
	struct iface_s *ifp;
	struct dialer_s *dialer;
	struct escc_s *ascip;
	unsigned char changes = 1; /* means: if DCD high initially, dial immediately */
	unsigned char status;

	ifp = (struct iface_s *)p1;
	ascip = Esccchan[dev];

	dialer = (struct dialer_s *)ifp->dstate;
	if (dialer == NULL)
		return;	/* Can't happen */

	for(;;){
/*RPB*/
#if 0
		/*while ((ascip->msr & MSR_TERI) == 0)*/
		while (ascip->stat & STAT_DCD0)
			/*kwait(&ascip->msr);*/
			kwait(&ascip->stat);

		/*ascip->msr &= ~MSR_TERI;*/
		ascip->stat |= STAT_DCD0;
#else
		/*
		 * Since DCD is used to monitor "incoming calls"
		 * i.s.o. RI, this is not a real answerer as it was
		 * originally meant to be, but rather allows special
		 * actions the be undertaken on the raising of DCD by
		 * means of the answerer file script (e.g. re-init of
		 * data link layer, etc)
		 */
		while (!(changes && ascip->status & DCD))
		{
			status = ascip->status;
			kwait(&ascip->status);
			changes = status ^ ascip->status;
		}

		changes = 0;
#endif
/*RPB*/
		dialer->answers++;
		redial(ifp,dialer->ansfile);
	}
}
#endif

#if 1 /* instead of using suspend/resume */
void ppp_recv(int dev, void *p1, void *p2);
#endif

/* execute dialer commands
 * returns: -1 fatal error, 0 OK, 1 try again
 */
#ifdef MODULE_redial
STATIC int
redial(ifp,file)
struct iface_s *ifp;
char *file;
{
	char *inbuf;
	FILE *fp;
	int (*rawsave)(struct iface_s *,struct mbuf_s **);
	int result = 0;
#if 1 /* instead of using suspend/resume */
	char *ifn;
#endif

/*RPB*/
	if (file == NULL)
		return -1;
/*RPB*/

	if ((fp = fopen(file,READ_TEXT)) == NULL){
		if (ifp->trace & (IF_TRACE_IN|IF_TRACE_OUT))
			_tprintf(ifp,_("redial: can't read %s\n"),file);
		return -1;
	}
	/* Save output handler and temporarily redirect output to null */
	if (ifp->raw == bitbucket){
		if (ifp->trace & (IF_TRACE_IN|IF_TRACE_OUT))
			_tprintf(ifp,_("redial: tip or dialer already active on %s\n"),ifp->name);
		return -1;
	}

	if (ifp->trace & (IF_TRACE_IN|IF_TRACE_OUT))
		_tprintf(ifp,_("Commands to %s:\n"),ifp->name);

	/* Save output handler and temporarily redirect output to null */
	rawsave = ifp->raw;
	ifp->raw = bitbucket;

#if 1 /* instead of using suspend/resume */
	killproc(ifp->rxproc);
	ifp->rxproc = NULL;
#else
	/* Suspend the packet input driver. Note that the transmit driver
	 * is left running since we use it to send buffers to the line.
	 */
	suspend(ifp->rxproc);
#endif

	inbuf = mallocw(BUFSIZ);
	while (fgets(inbuf,BUFSIZ,fp) != NULL){
		rip(inbuf);
		logmsg(-1, _("%s dialer: %s"), ifp->name,inbuf);
		if (ifp->trace & (IF_TRACE_IN|IF_TRACE_OUT))
			_tprintf(ifp,_("%s\n"),inbuf);
		if ((result = cmdparse(Dial_cmds,inbuf,ifp)) != 0){
			break;
		}
	}
	free(inbuf);
	fclose(fp);

	if (result == 0){
		ifp->lastsent = ifp->lastrecv = secclock();
	}

	ifp->raw = rawsave;
#if 1 /* instead of using suspend/resume */
	ifn = if_name(ifp, _(" rx"));
	ifp->rxproc = process_thread_create(driver_process_p, ifn, 384,
			ppp_recv, ifp->dev, ifp, NULL, 0);
	free(ifn);
#else
	resume(ifp->rxproc);
#endif

	if (ifp->trace & (IF_TRACE_IN|IF_TRACE_OUT))
		_tprintf(ifp,_("\nDial %s complete\n"),ifp->name);

	return result;
}
#endif

#ifdef MODULE_dodial_control
STATIC int
dodial_control(argc,argv,p)
int argc;
char *argv[];
void *p;
	{
	struct iface_s *ifp;
	int param;
	unsigned long arg;
	struct escc_s *escc;

	ifp = p;
	if (ifp->ioctl == NULL)
		{
		return -1;
		}

	param = devparam(argv[1]);
	if (param == -1)
		{
		return -1;
		}

	arg = 0;
	if (argc > 2)
		{
		arg = atol(argv[2]);
		}

	escc = Esccchan[ifp->dev];
	(*ifp->ioctl)(escc, param, TRUE, arg);

	return 0;
	}
#endif

#ifdef MODULE_dodial_send
STATIC int
dodial_send(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct iface_s *ifp = p;
	struct mbuf_s *bp;
	struct async_s *async_p;

	async_p = &Esccchan[ifp->dev]->async;
#if 0 /* shouldn't need this feature */
	if (argc > 2){
		/* Send characters with inter-character delay
		 * (for dealing with prehistoric Micom switches that
		 * can't take back-to-back characters...yes, they
		 * still exist.)
		 */
		char *cp;
		int32 cdelay = atol(argv[2]);

		for(cp = argv[1];*cp != '\0';cp++){
			async_write(async_p,cp,1);
			ppause(cdelay);
		}
	} else {
#endif
#if 0 /* must be a mistake... bp has no value yet */
		if (ifp->trace & IF_TRACE_RAW)
			raw_dump( ifp, IF_TRACE_OUT, bp );
#endif
		async_write(async_p,argv[1],strlen(argv[1]));
#if 0 /* shouldn't need this feature */
	}
#endif
	return 0;
}
#endif

#ifdef MODULE_dodial_speed
STATIC int
dodial_speed(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct iface_s *ifp = p;
	struct escc_s *escc;

	escc = Esccchan[ifp->dev];
	if ( argc < 2 ) {
		if (ifp->trace & (IF_TRACE_IN|IF_TRACE_OUT))
			_tprintf(ifp,_("current speed = %u bps\n"),
					escc->speed);
		return 0;
	}
	escc_speed(escc, 16, atol(argv[1]));
	return 0;
}
#endif

#ifdef MODULE_dodial_status
STATIC int
dodial_status(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct iface_s *ifp = p;
	int param;

	if (ifp->iostatus == NULL)
		return -1;

	if ((param = devparam(argv[1])) == -1)
		return -1;

	(*ifp->iostatus)(ifp,param,atol(argv[2]));
	return 0;
}
#endif

#ifdef MODULE_dodial_wait
STATIC int
dodial_wait(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct iface_s *ifp = p;
	register int c = -1;
	char *cp;
	struct escc_s *escc;

	kalarm(atol(argv[1]));

	escc = Esccchan[ifp->dev];
	if (argc == 2){
		errno = 0;
		while ((c = async_getc(&escc->async)) != -1 && errno != EALARM){
			if (ifp->trace & IF_TRACE_IN){
				fputc(c,ifp->trfp);
				fflush(ifp->trfp);
			}
		}
		kalarm(0L);
		return 0;
	}
	/* argc > 2 */
	cp = argv[2];

	while (*cp != '\0' && (c = async_getc(&escc->async)) != -1){
		if (ifp->trace & IF_TRACE_IN){
			fputc(c,ifp->trfp);
			fflush(ifp->trfp);
		}
		if (*cp++ != c){
			cp = argv[2];
		}
	}
	if (argc > 3){
		unsigned long speed = 0;

		if (_stricmp(argv[3], _("speed")) != 0)
			return -1;

		while ((c = async_getc(&escc->async)) != -1){
			if (ifp->trace & IF_TRACE_IN){
				fputc(c,ifp->trfp);
				fflush(ifp->trfp);
			}
			if (isdigit(c)){
				speed *= 10;
				speed += c - '0';
			} else {
				kalarm(0L);
				escc_speed(escc, 16, speed);
				return 0;
			}
		}
	}
	kalarm(0L);
	return (c == -1);
}
#endif

/*
 * Execute a dialer subcommand
 */
#ifdef MODULE_dodial_exec
int
dodial_exec(struct iface_s *ifp, int argc, char *argv[])
{
	int (*rawsave)(struct iface_s *,struct mbuf_s **);
	int result;
#if 1 /* instead of using suspend/resume */
	char *ifn;
#endif

	/* Save output handler and temporarily redirect output to null */
	if (ifp->raw == bitbucket){
		if (ifp->trace & (IF_TRACE_IN|IF_TRACE_OUT))
			_tprintf(ifp,_("dodial_exec: tip or dialer already active on %s\n"),ifp->name);

		return -1;
	}

	if (ifp->trace & (IF_TRACE_IN|IF_TRACE_OUT))
		_tprintf(ifp,_("Commands to %s:\n"),ifp->name);

	/* Save output handler and temporarily redirect output to null */
	rawsave = ifp->raw;
	ifp->raw = bitbucket;

#if 1 /* instead of using suspend/resume */
	killproc(ifp->rxproc);
	ifp->rxproc = NULL;
#else
	/* Suspend the packet input driver. Note that the transmit driver
	 * is left running since we use it to send buffers to the line.
	 */
	suspend(ifp->rxproc);
#endif

	if ((result = subcmd(Dial_cmds, argc, argv, ifp)) == 0){
		ifp->lastsent = ifp->lastrecv = secclock();
	}

	ifp->raw = rawsave;
#if 1 /* instead of using suspend/resume */
	ifn = if_name(ifp, _(" rx"));
	ifp->rxproc = process_thread_create(driver_process_p, ifn, 384,
			ppp_recv, ifp->dev, ifp, NULL, 0);
	free(ifn);
#else
	resume(ifp->rxproc);
#endif

	return result;
}
#endif

