# auto.mk by Nick for Hytech NOS/UZI project

# -----------------------------------------------------------------------------

SUBDIRS=

OUTPUTS=	ppp.$(LIBEXT)

ppp_$(LIBEXT)_SOURCES= \
		dialer.c ppp.c pppcmd.c pppdump.c pppfsm.c \
		pppipcp.c ppplcp.c ppppap.c slhc.c slhcdump.c slip.c
#asy.c api.c sppp.c

dialer_c_MODULES= \
		Dial_cmds sd_init sd_stat dialer_kick dropline dropit \
		sd_answer redial dodial_control dodial_send dodial_speed \
		dodial_status dodial_wait dodial_exec

ppp_c_MODULES=	fcstab htonppp _ppp_log _ppp_error _ppp_skipped ppp_send \
		ppp_output ppp_raw ppp_recv ppp_proc ppp_iostatus ppp_discard \
		ppp_echo ppp_init ppp_free

pppfsm_c_MODULES= \
		fsmStates fsmCodes htoncnf ntohcnf ntohopt fsm_no_action \
		fsm_no_check _fsm_log fsm_timer fsm_send fsm_sendreq \
		fsm_sendtermreq fsm_sendtermack fsm_reset fsm_opening \
		fsm_proc fsm_timeout fsm_start fsm_down fsm_close fsm_init \
		fsm_free

pppipcp_c_MODULES= \
		ipcp_default ipcp_negotiate ipcp_option_length ipcp_constants \
		IPcpcmds IPcpside_cmds doppp_ipcp doipcp_local doipcp_open \
		doipcp_pool doipcp_remote doipcp_address doipcp_compress \
		doipcp_default ipcp_option ipcp_makeoptions ipcp_makereq \
		ipcp_check ipcp_request ipcp_ack ipcp_nak ipcp_reject \
		ipcp_reset ipcp_stopping ipcp_closing ipcp_opening \
		ipcp_addr_idle ipcp_poolnext ipcp_lookuppeer ipcp_starting \
		ipcp_free ipcp_init

ppplcp_c_MODULES= \
		lcp_default lcp_negotiate lcp_option_length lcp_constants \
		Lcpcmds Lcpside_cmds doppp_lcp dolcp_local dolcp_open \
		dolcp_remote dolcp_accm dolcp_acfc dolcp_auth dolcp_magic \
		dolcp_mru dolcp_pfc dolcp_default lcp_option lcp_makeoptions \
		lcp_makereq lcp_check lcp_request lcp_ack lcp_nak lcp_reject \
		ppp_ready lcp_reset lcp_starting lcp_stopping lcp_closing \
		lcp_opening lcp_free lcp_init

ppppap_c_MODULES= \
		pap_constants Papcmds doppp_pap dopap_user pap_monitor \
		pap_pwdlookup pap_verify stpcpy pap_makereq pap_shutdown \
		pap_opening pap_request pap_check pap_proc pap_timeout \
		pap_down pap_free pap_init pap_local pap_remote

slhc_c_MODULES=	slhc_init slhc_free encode decode slhc_compress \
		slhc_uncompress slhc_remember slhc_toss slhc_i_status \
		slhc_o_status

# -----------------------------------------------------------------------------

