/* Generic serial line interface routines
 * Copyright 1992 Phil Karn, KA9Q
 */
#include <stdio.h>
#include "nos/global.h"
#include "kernel/thread.h"
#include "main/iface.h"
#include "internet/netuser.h"
#include "ppp/slhc.h"
#include "z80/abus.h" /* also gets asci.h for struct dma, struct fifo */
/*#include "ax25/ax25.h"*/
/*#include "ax25/kiss.h"*/
/*#include "netrom/nrs.h"*/
/*#include "x86/pktdrvr.h"*/
#include "ppp/slip.h"
#include "ppp/ppp.h"
#include "main/commands.h"
#include "main/config.h" /* Nick for SLIP, PPP etc macros */
#include "main/main.h" /* for driver_process_p */
#include <libintl.h>
#include "po/messages.h"

static int api_detach(struct iface_s *ifp);

/* Attach an apibus interface to the system
 * argv[0]: hardware type, must be "abus"
 * argv[1]: index to an individual device 0-8
 * argv[2]: mode, may be:
 *		"slip" (point-to-point SLIP)
 *		"kissui" (AX.25 UI frame format in SLIP for raw TNC)
 *		"ax25ui" (same as kissui)
 *		"kissi" (AX.25 I frame format in SLIP for raw TNC)
 *		"ax25i" (same as kissi)
 *		"nrs" (NET/ROM format serial protocol)
 *		"ppp" (Point-to-Point Protocol, RFC1171, RFC1172)
 * argv[3]: interface label, e.g., "abus0"
 * argv[4]: receiver ring buffer size in bytes
 * argv[5]: maximum transmission unit, bytes
 */
int
api_attach(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct iface_s *ifp;
	int dev;
	int trigchar = -1;
	int cts,rlsd;
	struct asci_mode_s *ap;
	char *cp;
	int base;
	int irq;
	struct abus_s *apip;
	int i,n;
	int chain;
#ifdef SLIP
	int xdev;
#endif

	if(if_lookup(argv[3]) != NULL){
		_printf(_("Interface %s already exists\n"),argv[4]);
		return -1;
	}
	if(setencap(NULL,argv[2]) == -1){
		_printf(_("Unknown encapsulation %s\n"),argv[3]);
		return -1;
	}
#if 1
	dev = atoi(argv[1]);
#else
	/* Find unused api control block */
	for(dev=0;dev < API_MAX;dev++){
		if(Api[dev].iface == NULL)
			break;
	}
#endif
	if(dev >= API_MAX){
		_printf(_("Too many apibus controllers\n"));
		return -1;
	}
	apip = &Api[dev];

	/* Create interface structure and fill in details */
	ifp = (struct iface_s *)callocw(1,sizeof(struct iface_s));
#if 1 /* remove anext member from mbufs and replace enqueue(), dequeue() */
	ifp->outq.rp = ifp->outq.buf;
	ifp->outq.wp = ifp->outq.buf;
#endif
#if 0 /* a good idea, but argv[5] is optional, sort this out later */
	if(argc > 6)
		{
		ifp->addr = resolve(argv[6]);
		}
	else
#endif
		{
		ifp->addr = Ip_addr;
		}
	ifp->name = strdupw(argv[3]);
	ifp->mtu = atoi(argv[5]);
	ifp->dev = dev;
	ifp->stop = api_detach;

#ifdef SLIP
	if (stricmp(argv[2], "SLIP") == 0)
		{
		setencap(ifp,"SLIP");
		ifp->ioctl = api_ioctl;

		for(xdev = 0;xdev < SLIP_MAX;xdev++)
			{
			if (Slip[xdev].iface == ifp ||
					Slip[xdev].iface == NULL)
				{
				break;
				}
			}

		Slip[xdev].type = CL_SERIAL_LINE;
		Slip[xdev].send = api_send;
		Slip[xdev].get = get_api;

#ifdef VJCOMPRESS
		if(argc > 5 && strchr(argv[5],'v') != NULL)
			{
			Slip[xdev].escaped |= SLIP_VJCOMPR;
			Slip[xdev].slcomp = slhc_init(16,16);
			}
#else
		Slip[xdev].slcomp = NULL;
#endif	/* VJCOMPRESS */

		if(slip_init(ifp) != 0)
			{
			_printf(_("%s: mode %s Init failed\n"),
					ifp->name,argv[3]);
			if_detach(ifp); /* should just free(ifp); instead? */
			return -1;
			}
		}
	else
#endif
#ifdef PPP
	if (stricmp(argv[2], "PPP") == 0)
		{
		setencap(ifp,"PPP");
		ifp->ioctl = api_ioctl;

#if 0 /* hmm.. where do we put these? need to modify ppp_init()... too hard? */
		Slip[xdev].type = CL_SERIAL_LINE;
		Slip[xdev].send = api_send;
		Slip[xdev].get = get_api;
#endif

		if(ppp_init(ifp) != 0)
			{
			_printf(_("%s: mode %s Init failed\n"),
					ifp->name,argv[3]);
			if_detach(ifp); /* should just free(ifp); instead? */
			return -1;
			}
		}
	else
#endif
		{
		setencap(ifp,argv[2]);

		/* Look for the interface mode in the table */
		for(ap = Asci_mode;ap->name != NULL;ap++)
			{
			if(stricmp(argv[2],ap->name) == 0)
				{
				trigchar = ap->trigchar;
				if((*ap->init)(ifp) != 0)
					{
					_printf(_("%s: mode %s Init failed\n"),
					 ifp->name,argv[2]);
					if_detach(ifp);
					/* should just free(ifp); instead? */
					return -1;
					}
				break;
				}
			}
		if(ap->name == NULL)
			{
			_printf(_("Mode %s unknown for interface %s\n"),
					argv[2],argv[3]);
			if_detach(ifp); /* should just free(ifp); instead? */
			return -1;
			}
		}

	/* Link in the interface */
	ifp->next = Ifaces;
	Ifaces = ifp;

/* abyte('{'); */
	if (api_init(dev,ifp,trigchar,atol(argv[4]))) /* -1 means error */
		{
		_printf(_("Not enough memory\n"));
		/* note: Api[ifp->dev].iface is NULL so api_stop() is a nop */
		if_detach(ifp);
		return -1;
		}
/* abyte('}'); */
	cp = if_name(ifp, _(" tx"));
/* abyte('['); */
	ifp->txproc = process_thread_create(driver_process_p, cp, 384 /*768*/,
			if_tx, 0, ifp, NULL, 0);
/* abyte('|'); */
/* kwait(NULL); */
/* abyte(']'); */
	free(cp);
	if (ifp->txproc == NULL)
		{
		_printf(_("Not enough memory\n"));
		/* note: Asci[ifp->dev].iface is true so api_stop() is needed */
		if_detach(ifp);
		return -1;
		}
	return 0;
}

static int
api_detach(ifp)
struct iface_s *ifp;
{
	struct asci_mode_s *ap;

	if(ifp == NULL)
		return -1;

	/* note: Api[ifp->dev].iface may be NULL in which case this is a nop */
	api_stop(ifp);

	/* Call mode-dependent routine */
	for(ap = Asci_mode;ap->name != NULL;ap++)
		{
		if(ifp->iftype != NULL
		 && stricmp(ifp->iftype->name,ap->name) == 0
		 && ap->free != NULL)
			{
			(*ap->free)(ifp);
			}
		}

	return 0;
}

