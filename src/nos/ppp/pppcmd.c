/*
 *  PPPCMD.C	-- PPP related user commands
 *
 *	This implementation of PPP is declared to be in the public domain.
 *
 *	Jan 91	Bill_Simpson@um.cc.umich.edu
 *		Computer Systems Consulting Services
 *
 *	Acknowledgements and correction history may be found in PPP.C
 *
 *	Dec 03	RPB: added "ppp <iface> force <up|down>" command
 *		     Po-ed most of the remaining strings
 */

#include <stdio.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include "main/iface.h"
/*#include "x86/pktdrvr.h"*/
#include "ppp/ppp.h"
#include "ppp/pppfsm.h"
#include "ppp/ppplcp.h"
#include "ppp/ppppap.h"
#include "ppp/pppipcp.h"
#include "main/cmdparse.h"
#include <libintl.h>
#include "po/messages.h"

static struct iface_s *ppp_lookup(char *ifname);

static int doppp_quick(int argc, char *argv[], void *p);
static int doppp_trace(int argc, char *argv[], void *p);
/*RPB*/
static int doppp_force(int argc, char *argv[], void *p);
/*RPB*/

static int spot(uint16 work,uint16 want,uint16 will,uint16 mask);
static void genstat(struct ppp_s *ppp_p);
static void lcpstat(struct fsm_s *fsm_p);
static void papstat(struct fsm_s *fsm_p);
static void ipcpstat(struct fsm_s *fsm_p);

static int dotry_nak(int argc, char *argv[], void *p);
static int dotry_req(int argc, char *argv[], void *p);
static int dotry_terminate(int argc, char *argv[], void *p);

/* "ppp" subcommands */
#define PPPCMDS_MAX 7
static struct cmds Pppcmds[PPPCMDS_MAX] = {
	N_("ipcp"),	doppp_ipcp,	0,	0,	NULL,
	N_("lcp"),	doppp_lcp,	0,	0,	NULL,
	N_("pap"),	doppp_pap,	0,	0,	NULL,
	N_("quick"),	doppp_quick,	0,	0,	NULL,
	N_("trace"),	doppp_trace,	0,	0,	NULL,
/*RPB*/
	N_("force"),	doppp_force,	0,	2,	N_("force up|down"),
/*RPB*/
	NULL,
};

/* "ppp <iface> <ncp> try" subcommands */
#define PPPTRYCMDS_MAX 4
static struct cmds PppTrycmds[PPPTRYCMDS_MAX] = {
	N_("configure"),	dotry_req,	0,	0,	NULL,
	N_("failure"),		dotry_nak,	0,	0,	NULL,
	N_("terminate"),	dotry_terminate, 0,	0,	NULL,
	NULL,
};

#define PPPSTATUS_MAX 5
static _char *PPPStatus[PPPSTATUS_MAX] = {
	N_("Physical Line Dead"),
	N_("Establishment Phase"),
	N_("Authentication Phase"),
	N_("Network Protocol Phase"),
	N_("Termination Phase")
};

#define NCPSTATUS_MAX 7
static _char *NCPStatus[NCPSTATUS_MAX] = {
	N_("Closed"),
	N_("Listening -- waiting for remote host to attempt open"),
	N_("Starting configuration exchange"),
	N_("Remote host accepted our request; waiting for remote request"),
	N_("We accepted remote request; waiting for reply to our request"),
	N_("Opened"),
	N_("Terminate request sent to remote host")
};

int PPPtrace;
struct iface_s *PPPiface;  /* iface for trace */

/****************************************************************************/

static struct iface_s *
ppp_lookup(ifname)
char *ifname;
{
	register struct iface_s *ifp;

	if ((ifp = if_lookup(ifname)) == NULL) {
		_printf(_("%s: Interface unknown\n"),ifname);
		return(NULL);
	}
	if (ifp->iftype->type != CL_PPP) {
		_printf(_("%s: not a PPP interface\n"),ifp->name);
		return(NULL);
	}
	return(ifp);
}

/****************************************************************************/

int
doppp_commands(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct iface_s *ifp;

	if (argc < 2) {
		_printf( _("ppp <iface> required\n") );
		return 0; /*-1;*/
	}
	if ((ifp = ppp_lookup(argv[1])) == NULL)
		return 0; /*-1;*/

	if ( argc == 2 ) {
		ppp_show( ifp );
		return 0;
	}

	return subcmd(Pppcmds, argc - 1, &argv[1], ifp);
}


/* Close connection on PPP interface */
int
doppp_close(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct fsm_s *fsm_p = p;

	fsm_p->flags &= ~(FSM_ACTIVE | FSM_PASSIVE);

	fsm_close( fsm_p );
	return 0;
}


int
doppp_passive(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct fsm_s *fsm_p = p;

	fsm_p->flags &= ~FSM_ACTIVE;
	fsm_p->flags |= FSM_PASSIVE;

	fsm_start(fsm_p);
	return 0;
}


int
doppp_active(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct fsm_s *fsm_p = p;

	fsm_p->flags &= ~FSM_PASSIVE;
	fsm_p->flags |= FSM_ACTIVE;

	if ( fsm_p->state < fsmLISTEN ) {
		fsm_p->state = fsmLISTEN;
	}
	return 0;
}


static int
doppp_quick(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct iface_s *ifp = p;
	register struct ppp_s *ppp_p = ifp->edv;
	struct lcp_s *lcp_p = ppp_p->fsm[Lcp].pdv;
	struct ipcp_s *ipcp_p = ppp_p->fsm[IPcp].pdv;

	lcp_p->local.want.accm = 0L;
	lcp_p->local.want.negotiate |= LCP_N_ACCM;
	lcp_p->local.want.magic_number += (long)&lcp_p->local.want.magic_number;
	lcp_p->local.want.negotiate |= LCP_N_MAGIC;
	lcp_p->local.want.negotiate |= LCP_N_ACFC;
	lcp_p->local.want.negotiate |= LCP_N_PFC;

	ipcp_p->local.want.compression = PPP_COMPR_PROTOCOL;
	ipcp_p->local.want.slots = 16;
	ipcp_p->local.want.slot_compress = 1;
	ipcp_p->local.want.negotiate |= IPCP_N_COMPRESS;
	doppp_active( 0, NULL, &(ppp_p->fsm[IPcp]) );

	return 0;
}


/****************************************************************************/

void
ppp_show(ifp)
struct iface_s *ifp;
{
	register struct ppp_s *ppp_p = ifp->edv;

	genstat(ppp_p);
	if ( ppp_p->fsm[Lcp].pdv != NULL )
		lcpstat(&(ppp_p->fsm[Lcp]));
	if ( ppp_p->fsm[Pap].pdv != NULL )
		papstat(&(ppp_p->fsm[Pap]));
	if ( ppp_p->fsm[IPcp].pdv != NULL )
		ipcpstat(&(ppp_p->fsm[IPcp]));
}


static void
genstat(ppp_p)
register struct ppp_s *ppp_p;
{

/*RPB*/
#if 0
	_printf(_("%s"), PPPStatus[ppp_p->phase]);
#else
	_printf(PPPStatus[ppp_p->phase]);
#endif
/*RPB*/

	if (ppp_p->phase == pppREADY) {
		_printf(_("\t(open for %s)"),
			tformat(secclock() - ppp_p->upsince));
	}
	_printf(_("\n"));

	_printf(_("%10lu In,  %10lu Flags,%6u ME, %6u FE, %6u CSE, %6u other\n"),
		ppp_p->InRxOctetCount,
		ppp_p->InOpenFlag,
		ppp_p->InMemory,
		ppp_p->InFrame,
		ppp_p->InChecksum,
		ppp_p->InError);
	_printf(_("\t\t%6u Lcp,%6u Pap,%6u IPcp,%6u Unknown\n"),
		ppp_p->InNCP[Lcp],
		ppp_p->InNCP[Pap],
		ppp_p->InNCP[IPcp],
		ppp_p->InUnknown);
	_printf(_("%10lu Out, %10lu Flags,%6u ME, %6u Fail\n"),
		ppp_p->OutTxOctetCount,
		ppp_p->OutOpenFlag,
		ppp_p->OutMemory,
		ppp_p->OutError);
	_printf(_("\t\t%6u Lcp,%6u Pap,%6u IPcp\n"),
		ppp_p->OutNCP[Lcp],
		ppp_p->OutNCP[Pap],
		ppp_p->OutNCP[IPcp]);
}


static int
spot(work,want,will,mask)
uint16 work;
uint16 want;
uint16 will;
uint16 mask;
{
	char blot = ' ';
	int result = (work & mask);

	if ( !(will & mask) ) {
		blot = '*';
	} else if ( (want ^ work) & mask ) {
		blot = (result ? '+' : '-');
	}
	_printf( _("%c"), blot );
	return result;
}

static void
lcpstat(fsm_p)
struct fsm_s *fsm_p;
{
	struct lcp_s *lcp_p = fsm_p->pdv;
	struct lcp_value_s *localp = &(lcp_p->local.work);
	uint16  localwork = lcp_p->local.work.negotiate;
	uint16  localwant = lcp_p->local.want.negotiate;
	uint16  localwill = lcp_p->local.will_negotiate;
	struct lcp_value_s *remotep = &(lcp_p->remote.work);
	uint16  remotework = lcp_p->remote.work.negotiate;
	uint16  remotewant = lcp_p->remote.want.negotiate;
	uint16  remotewill = lcp_p->remote.will_negotiate;

/*RPB*/
#if 0
	_printf(_("LCP %s\n"),
		NCPStatus[fsm_p->state]);
#else
	_printf(_("LCP "));
	_printf(NCPStatus[fsm_p->state]);
	_printf(_("\n"));
#endif
/*RPB*/

	_printf(_("\t\t MRU\t ACCM\t\t AP\t PFC  ACFC Magic\n"));

	_printf(_("\tLocal:\t"));

	spot( localwork, localwant, localwill, LCP_N_MRU );
	_printf( _("%4d\t"), localp->mru );

	spot( localwork, localwant, localwill, LCP_N_ACCM );
	_printf( _("0x%08lx\t"), localp->accm );

	if ( spot( localwork, localwant, localwill, LCP_N_AUTHENT ) ) {
		switch ( localp->authentication ) {
		case PPP_PAP_PROTOCOL:
			_printf( _("Pap\t") );
			break;
		default:
			_printf( _("0x%04x\t"), localp->authentication);
			break;
		};
	} else {
		_printf( _("None\t") );
	}

	_printf( spot( localwork, localwant, localwill, LCP_N_PFC )
		  ? _("Yes ") : _("No  ") );
	_printf( spot( localwork, localwant, localwill, LCP_N_ACFC )
		  ? _("Yes ") : _("No  ") );

	spot( localwork, localwant, localwill, LCP_N_MAGIC );
	if ( localp->magic_number != 0L ) {
		_printf( _("0x%08lx\n"), localp->magic_number );
	} else {
		_printf ( _("unused\n") );
	}

	_printf(_("\tRemote:\t"));

	spot( remotework, remotewant, remotewill, LCP_N_MRU );
	_printf( _("%4d\t"), remotep->mru );

	spot( remotework, remotewant, remotewill, LCP_N_ACCM );
	_printf( _("0x%08lx\t"), remotep->accm );

	if ( spot( remotework, remotewant, remotewill, LCP_N_AUTHENT ) ) {
		switch ( remotep->authentication ) {
		case PPP_PAP_PROTOCOL:
			_printf( _("Pap\t") );
			break;
		default:
			_printf( _("0x%04x\t"), remotep->authentication);
			break;
		};
	} else {
		_printf( _("None\t") );
	}

	_printf( spot( remotework, remotewant, remotewill, LCP_N_PFC )
		  ? _("Yes ") : _("No  ") );
	_printf( spot( remotework, remotewant, remotewill, LCP_N_ACFC )
		  ? _("Yes ") : _("No  ") );

	spot( remotework, remotewant, remotewill, LCP_N_MAGIC );
	if ( remotep->magic_number != 0L ) {
		_printf( _("0x%08lx\n"), remotep->magic_number );
	} else {
		_printf( _("unused\n") );
	}
}


static void
papstat(fsm_p)
struct fsm_s *fsm_p;
{
	struct pap_s *pap_p = fsm_p->pdv;

/*RPB*/
#if 0
	_printf(_("PAP %s\n"),
		NCPStatus[fsm_p->state]);
#else
	_printf(_("PAP "));
	_printf(NCPStatus[fsm_p->state]);
	_printf(_("\n"));
#endif
/*RPB*/

	_printf( _("\tMessage: '%s'\n"), (pap_p->message == NULL) ?
		 "none" : pap_p->message );
}


static void
ipcpstat(fsm_p)
struct fsm_s *fsm_p;
{
	struct ipcp_s *ipcp_p = fsm_p->pdv;
	struct ipcp_value_s *localp = &(ipcp_p->local.work);
	uint16  localwork = ipcp_p->local.work.negotiate;
	struct ipcp_value_s *remotep = &(ipcp_p->remote.work);
	uint16  remotework = ipcp_p->remote.work.negotiate;

/*RPB*/
#if 0
	_printf(_("IPCP %s\n"),
		NCPStatus[fsm_p->state]);
#else
	_printf(_("IPCP "));
	_printf(NCPStatus[fsm_p->state]);
	_printf(_("\n"));
#endif
/*RPB*/
	_printf(_("\tlocal IP address: %s"),
		inet_ntoa(localp->address));
	_printf(_("  remote IP address: %s\n"),
		inet_ntoa(localp->other));

	if (localwork & IPCP_N_COMPRESS) {
		_printf(_("    In\tTCP header compression enabled:"
			" slots = %d, flag = 0x%02x\n"),
			localp->slots,
			localp->slot_compress);
		slhc_i_status(ipcp_p->slhcp);
	}

	if (remotework & IPCP_N_COMPRESS) {
		_printf(_("    Out\tTCP header compression enabled:"
			" slots = %d, flag = 0x%02x\n"),
			remotep->slots,
			remotep->slot_compress);
		slhc_o_status(ipcp_p->slhcp);
	}
}


/****************************************************************************/
/* Set timeout interval when waiting for response from remote peer */
int
doppp_timeout(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct fsm_s *fsm_p = p;
	struct timer *t = &(fsm_p->timer);

	if (argc < 2) {
		_printf(_("%d\n"),dur_timer(t)/1000L);
	} else {
		int x = (int)strtol( argv[1], NULL, 0 );

		if (x <= 0) {
			_printf(_("Timeout value %s (%d) must be > 0\n"),
				argv[1], x);
			return 0; /*-1;*/
		} else {
			set_timer(t, x * 1000L);
		}
	}
	return 0;
}


int
doppp_try(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return subcmd(PppTrycmds, argc, argv, p);
}


static int
dotry_nak(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct fsm_s *fsm_p = p;

	if (argc < 2) {
		_printf(_("%d\n"),fsm_p->try_nak);
	} else {
		int x = (int)strtol( argv[1], NULL, 0 );

		if (x <= 0) {
			_printf(_("Value %s (%d) must be > 0\n"),
				argv[1], x);
			return 0; /*-1;*/
		} else {
			fsm_p->try_nak = x;
		}
	}
	return 0;
}


static int
dotry_req(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct fsm_s *fsm_p = p;

	if (argc < 2) {
		_printf(_("%d\n"),fsm_p->try_req);
	} else {
		int x = (int)strtol( argv[1], NULL, 0 );

		if (x <= 0) {
			_printf(_("Value %s (%d) must be > 0\n"),
				argv[1], x);
			return 0; /*-1;*/
		} else {
			fsm_p->try_req = x;
		}
	}
	return 0;
}


static int
dotry_terminate(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct fsm_s *fsm_p = p;

	if (argc < 2) {
		_printf(_("%d\n"),fsm_p->try_terminate);
	} else {
		int x = (int)strtol( argv[1], NULL, 0 );

		if (x <= 0) {
			_printf(_("Value %s (%d) must be > 0\n"),
				argv[1], x);
			return 0; /*-1;*/
		} else {
			fsm_p->try_terminate = x;
		}
	}
	return 0;
}


static int
doppp_trace(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct iface_s *ifp = p;
	register struct ppp_s *ppp_p = ifp->edv;
	int tracing = ppp_p->trace;
	int result = setint(&tracing, _("PPP tracing"), argc, argv);

	ppp_p->trace = tracing;
	return result;
}

/*RPB*/
static int
doppp_force(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct iface_s *ifp = p;
	int param;

        if(ifp->iostatus == NULL)
                return 0; /*-1;*/

        if((param = devparam(argv[1])) == -1)
                return 0; /*-1;*/

        (*ifp->iostatus)(ifp,param,0);
        return 0;
}
/*RPB*/
