/* Simplified Point-to-Point Protocol
 * No negotiation, no address or ctl fields, 1-byte pids
 */
#include "nos/global.h"
#include "main/mbuf.h"
#include "main/iface.h"
#include "internet/ip.h"
#include "ppp/slhc.h"
#include "z80/escc.h"
#include "main/crc.h"
#include "main/ahdlc.h"
#include "ppp/sppp.h"
#include "dump/trace.h"
#include "main/main.h" /* for driver_process_p */
#include <libintl.h>
#include "po/messages.h"

static uint8 *putbyte(uint8 *cp,uint8 c);

int
sppp_init(ifp)
struct iface_s *ifp;
{
	char *ifn;

	ifp->ioctl = asy_ioctl;
	ifp->edv = slhc_init(16, 16);
	ifp->rxproc = process_thread_create(driver_process_p,
			ifn = if_name(ifp, _(" rx")), 512, sppp_rx,
			ifp->dev, ifp, NULL, 0);
	free(ifn);
	return 0;
}
int
sppp_free(ifp)
struct iface_s *ifp;
{
	free(ifp->edv);
	return 0;
}
/* Send an IP datagram using simplified PPP */
int
sppp_send(
struct mbuf_s **bpp,
struct iface_s *ifp,
int32 gateway,	/* Not used */
uint8 tos	/* Not used at this level */
){
	int type,protocol;
	struct slcompress *sp = ifp->edv;

	dump(ifp,IF_TRACE_OUT,*bpp);
	ifp->rawsndcnt++;
	ifp->lastsent = secclock();
	type = slhc_compress(sp,bpp,1);
	switch(type){
	default:	/* "Can't happen" */
	case SL_TYPE_IP:
		protocol = PPP_IP_PROTOCOL;
		break;
	case SL_TYPE_COMPRESSED_TCP:
		protocol = PPP_COMPR_PROTOCOL;
		break;
	case SL_TYPE_UNCOMPRESSED_TCP:
		protocol = PPP_UNCOMP_PROTOCOL;
		break;
	}
	return sppp_output(ifp,protocol,bpp);
}
int
sppp_output(
struct iface_s *iface,
int protocol,
struct mbuf_s **bpp
){
	struct mbuf_s *obp;
	uint8 *cp;
	int c;
	uint16 fcs;
	struct asy *asy_p;

	fcs = FCS_START;
	obp = ambufw(6+2*len_p(*bpp));	/* Allocate worst-case */
	cp = obp->data;
	*cp++ = HDLC_FLAG;
	cp = putbyte(cp,(char)protocol);
	fcs = FCS(fcs,protocol);
	while((c = PULLCHAR(bpp)) != -1){
		fcs = FCS(fcs,c);
		cp = putbyte(cp,c);
	}
	free_p(bpp);	/* Shouldn't be necessary */
	fcs ^= 0xffff;
	cp = putbyte(cp,fcs);
	cp = putbyte(cp,fcs >> 8);
	*cp++ = HDLC_FLAG;

	obp->cnt = cp - obp->data;

	asy_p = &Esccchan[iface->dev]->asy
	return async_send(asy_p,&obp);
}
/* Add a byte to the PPP output stream, byte stuffing for transparency */
static uint8 *
putbyte(cp,c)
uint8 *cp;
uint8 c;
{
	switch(c){
	case HDLC_FLAG:
	case HDLC_ESC_ASYNC:
		*cp++ = HDLC_ESC_ASYNC;
		*cp++ = c ^ HDLC_ESC_COMPL;
		break;
	default:
		*cp++ = c;
	}
	return cp;
}

/* Process simplified PPP line input */
void
sppp_rx(dev,p1,p2)
int dev;
void *p1;
void *p2;
{
	struct mbuf_s *bp;
	int c;
	struct ahdlc ahdlc;
	struct iface_s *ifp = (struct iface_s *)p1;
	struct slcompress *sp = ifp->edv;

	init_hdlc(&ahdlc,2048);
	asy_p = &Esccchan[dev]->asy;
	for(;;){
		c = async_getc(asy_p)
		if((bp = ahdlcrx(&ahdlc,c)) == NULL)
			continue;
		c = PULLCHAR(&bp);
		switch(c){	/* Turn compressed IP/TCP back to normal */
		case PPP_COMPR_PROTOCOL:
			if(slhc_uncompress(sp,&bp) <= 0)
				c = -1;	/* Failed; discard */
			else
				c = PPP_IP_PROTOCOL;
			break;
		case PPP_UNCOMP_PROTOCOL:
			if(slhc_remember(sp,&bp) <= 0)
				c = -1;	/* Failed; discard */
			else
				c = PPP_IP_PROTOCOL;
			break;
		}
		switch(c){
		case PPP_IP_PROTOCOL:
			net_route(ifp,&bp);
			break;
		case -1:
			break;
		default:	/* Toss all unknown types */
			free_p(&bp);
			break;
		}
	}
}

