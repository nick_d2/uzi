/*
 *  PPPFSM.C	-- PPP Finite State Machine
 *
 *	This implementation of PPP is declared to be in the public domain.
 *
 *	Jan 91	Bill_Simpson@um.cc.umich.edu
 *		Computer Systems Consulting Services
 *
 *	Acknowledgements and correction history may be found in PPP.C
 *
 *	Dec 03	RPB: removed bug in fsm_send()
 *		     Po-ed most of the remaining strings
 */

#include <stdio.h>
#include <ctype.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include "main/iface.h"
#include "ppp/ppp.h"
#include "ppp/pppfsm.h"
#include "ppp/ppplcp.h"
#include "dump/trace.h"
#include <libintl.h>
#include "po/messages.h"

#ifdef MODULE
#define STATIC
#else
#define STATIC static
#define MODULE_fsmStates
#define MODULE_fsmCodes
#define MODULE_htoncnf
#define MODULE_ntohcnf
#define MODULE_ntohopt
#define MODULE_fsm_no_action
#define MODULE_fsm_no_check
/*RPB*/
#define MODULE__fsm_log
/*RPB*/
#define MODULE_fsm_timer
#define MODULE_fsm_send
#define MODULE_fsm_sendreq
#define MODULE_fsm_sendtermreq
#define MODULE_fsm_sendtermack
#define MODULE_fsm_reset
#define MODULE_fsm_opening
#define MODULE_fsm_proc
#define MODULE_fsm_timeout
#define MODULE_fsm_start
#define MODULE_fsm_down
#define MODULE_fsm_close
#define MODULE_fsm_init
#define MODULE_fsm_free
#endif

#ifdef MODULE_fsmStates
_char *fsmStates[FSMSTATES_MAX] = {
	N_("Closed"),
	N_("Listen"),
	N_("Req Sent"),
	N_("Ack Rcvd"),
	N_("Ack Sent"),
	N_("Opened"),
	N_("TermSent")
};
#endif

#ifdef MODULE_fsmCodes
_char *fsmCodes[FSMCODES_MAX] = {
	NULL,
	N_("Config Req"),
	N_("Config Ack"),
	N_("Config Nak"),
	N_("Config Reject"),
	N_("Termin Req"),
	N_("Termin Ack"),
	N_("Code Reject"),
	N_("Protocol Reject"),
	N_("Echo Request"),
	N_("Echo Reply"),
	N_("Discard Request"),
};
#endif

STATIC int fsm_sendtermreq(struct fsm_s *fsm_p);
STATIC int fsm_sendtermack(struct fsm_s *fsm_p, byte_t id);

STATIC void fsm_timeout(void *vp);

STATIC void fsm_reset(struct fsm_s *fsm_p);
STATIC void fsm_opening(struct fsm_s *fsm_p);

/************************************************************************/
/* Convert header in host form to network form */

#ifdef MODULE_htoncnf
void
htoncnf(
struct config_hdr *cnf,
struct mbuf_s **bpp
){
	register uint8 *cp;

	/* Prepend bytes for LCP/IPCP header */
	pushdown(bpp, NULL,CONFIG_HDR_LEN);

	/* Load header with proper values */
	cp = (*bpp)->data;
	*cp++ = cnf->code;
	*cp++ = cnf->id;
	put16(cp, cnf->len);
}
#endif

/* Extract header from incoming packet */

#ifdef MODULE_ntohcnf
int
ntohcnf(cnf, bpp)
struct config_hdr *cnf;
struct mbuf_s **bpp;
{
	uint8 cnfb[CONFIG_HDR_LEN];

	if ( cnf == NULL )
		return -1;

	if ( pullup( bpp, cnfb, CONFIG_HDR_LEN ) < CONFIG_HDR_LEN )
		return -1;

        cnf->code = cnfb[0];
        cnf->id = cnfb[1];
	cnf->len = get16(&cnfb[2]);
	return 0;
}
#endif

/***************************************/
/* Extract configuration option header */

#ifdef MODULE_ntohopt
int
ntohopt(opt,bpp)
struct option_hdr *opt;
struct mbuf_s **bpp;
{
	uint8 optb[OPTION_HDR_LEN];

	if ( opt == NULL )
		return -1;

	if ( pullup( bpp, optb, OPTION_HDR_LEN ) < OPTION_HDR_LEN )
		return -1;

	opt->type = optb[0];
	opt->len = optb[1];
	return 0;
}
#endif


/************************************************************************/

#ifdef MODULE_fsm_no_action
void
fsm_no_action(fsm_p)
struct fsm_s *fsm_p;
{
	PPP_DEBUG_ROUTINES(_("fsm_no_action()"));
}
#endif

#ifdef MODULE_fsm_no_check
int
fsm_no_check(
struct fsm_s *fsm_p,
struct config_hdr *hdr,
struct mbuf_s **bpp
){
	PPP_DEBUG_ROUTINES(_("fsm_no_check()"));
	return 0;
}
#endif


/************************************************************************/

/*RPB*/
/* General log routine: intl version */

#ifdef MODULE__fsm_log
void
_fsm_log(fsm_p, comment)
struct fsm_s *fsm_p;
_char *comment;
{
	char *duptext1, *duptext2;

	if (PPPtrace > 1)
	{
		if ((duptext1 = _strdup(fsmStates[fsm_p->state])) == NULL)
		{
			__getfail(fsmStates[fsm_p->state]);
			return;
		}
		if ((duptext2 = _strdup(comment)) == NULL)
		{
			__getfail(comment);
			free(duptext1);
			return;
		}
		_trace_log(PPPiface, _("%s PPP/%s %-8s  %s"),
			fsm_p->ppp_p->iface->name,
			fsm_p->pdc->name,
			duptext1,
			duptext2);
		free(duptext1);
		free(duptext2);
	}
}
#endif
/*RPB*/


/************************************************************************/
/* Set a timer in case an expected event does not occur */

#ifdef MODULE_fsm_timer
void
fsm_timer(fsm_p)
struct fsm_s *fsm_p;
{
	PPP_DEBUG_ROUTINES(_("fsm_timer()"));

	start_timer( &(fsm_p->timer) );
}
#endif


/************************************************************************/
/* Send a packet to the remote host */

#ifdef MODULE_fsm_send
int
fsm_send(
struct fsm_s *fsm_p,
byte_t code,
byte_t id,
struct mbuf_s **data
){
	struct ppp_s *ppp_p = fsm_p->ppp_p;
	struct iface_s *iface = ppp_p->iface;
	struct config_hdr hdr;
	struct mbuf_s *bp;

	switch( hdr.code = code ) {
	case CONFIG_REQ:
	case TERM_REQ:
	case ECHO_REQ:
		/* Save ID field for match against replies from remote host */
		fsm_p->lastid = ppp_p->id;
		/* fallthru */
	case PROT_REJ:
	case DISCARD_REQ:
		/* Use a unique ID field value */
		hdr.id = ppp_p->id++;
		break;

	case CONFIG_ACK:
	case CONFIG_NAK:
	case CONFIG_REJ:
	case TERM_ACK:
	case CODE_REJ:
	case ECHO_REPLY:
		/* Use ID sent by remote host */
		hdr.id = id;
		break;

	default:
		/* we're in trouble */
/*RPB*/
	{
		char *duptext;

		if ((duptext = _strdup(fsmStates[fsm_p->state])) == NULL)
		{
			__getfail(fsmStates[fsm_p->state]);
			return -1;
		}
		_trace_log(PPPiface, _("%s PPP/%s %-8s;"
			" Send with bogus code: %d"),
			iface->name,
			fsm_p->pdc->name,
			duptext,
			code);
		free(duptext);
	}
/*RPB*/
		return -1;
	};

/*RPB*/
#if 0
	switch( code ) {
	case ECHO_REQ:
	case ECHO_REPLY:
	case DISCARD_REQ:
	{
		struct lcp_s *lcp_p = fsm_p->pdv;
		bp = ambufw(4);
		put32(bp->data, lcp_p->local.work.magic_number);
		*data = bp;
	}
	};

	hdr.len = len_p(*data) + CONFIG_HDR_LEN;

	/* Prepend header to packet data */
	htoncnf(&hdr,data);
#else
	/*
	 * BUG: there are more LCP packets which carry data than those
	 * three listed below. Catch them in the 'default" clause.
	 */
        switch( code ) {
        case ECHO_REQ:
        case ECHO_REPLY:
        case DISCARD_REQ:
        {
                struct lcp_s *lcp_p = fsm_p->pdv;
                bp = ambufw(4);
                put32(bp->data, lcp_p->local.work.magic_number);
                *data = bp;
                hdr.len = len_p(*data) + CONFIG_HDR_LEN;

                /* Prepend header to packet data */
                htoncnf(&hdr,data);
        }
	default:
        {
		if (data == NULL)
		{
			bp = ambufw(1);
			data = &bp;
			hdr.len = CONFIG_HDR_LEN;

			/* Prepend header to packet data */
			htoncnf(&hdr,data);
			trim_mbuf(data, CONFIG_HDR_LEN);
		}
		else
		{
			hdr.len = len_p(*data) + CONFIG_HDR_LEN;

			/* Prepend header to packet data */
			htoncnf(&hdr,data);
		}
        }
        };
#endif
/*RPB*/

	if (PPPtrace > 1) {
/*RPB*/
		char *duptext1, *duptext2;

		if ((duptext1 = _strdup(fsmStates[fsm_p->state])) == NULL)
		{
			__getfail(fsmStates[fsm_p->state]);
			return -1;
		}
		if ((duptext2 = _strdup(fsmCodes[code])) == NULL)
		{
			__getfail(fsmCodes[code]);
			free(duptext1);
			return -1;
		}
		_trace_log(PPPiface, _("%s PPP/%s %-8s;"
			" Sending %s, id: %d, len: %d"),
			iface->name,
			fsm_p->pdc->name,
			duptext1,
			duptext2,
			hdr.id,hdr.len);
		free(duptext1);
		free(duptext2);
/*RPB*/
	}

	ppp_p->OutNCP[fsm_p->pdc->fsmi]++;

	return( (*iface->output)
		(iface, NULL, NULL, fsm_p->pdc->protocol, data) );
}
#endif


/************************************************************************/
/* Send a configuration request */

#ifdef MODULE_fsm_sendreq
int
fsm_sendreq(fsm_p)
struct fsm_s *fsm_p;
{
	struct mbuf_s *bp;

	PPP_DEBUG_ROUTINES(_("fsm_sendreq()"));

	if ( fsm_p->retry <= 0 )
		return -1;

	fsm_p->retry--;
	fsm_timer(fsm_p);

	bp = (*fsm_p->pdc->makereq)(fsm_p);
	return( fsm_send(fsm_p, CONFIG_REQ, 0, &bp) );
}
#endif


/************************************************************************/
/* Send a termination request */

#ifdef MODULE_fsm_sendtermreq
STATIC int
fsm_sendtermreq(fsm_p)
struct fsm_s *fsm_p;
{
	PPP_DEBUG_ROUTINES(_("fsm_sendtermreq()"));

	if ( fsm_p->retry <= 0 )
		return -1;

	fsm_p->retry--;
	fsm_timer(fsm_p);
	return( fsm_send(fsm_p, TERM_REQ, 0, NULL) );
}
#endif


/************************************************************************/
/* Send Terminate Ack */

#ifdef MODULE_fsm_sendtermack
STATIC int
fsm_sendtermack(fsm_p,id)
struct fsm_s *fsm_p;
byte_t id;
{
	PPP_DEBUG_ROUTINES(_("fsm_sendtermack()"));

	return( fsm_send(fsm_p, TERM_ACK, id, NULL) );
}
#endif


/************************************************************************/
/* Reset state machine */

#ifdef MODULE_fsm_reset
STATIC void
fsm_reset(fsm_p)
struct fsm_s *fsm_p;
{
	PPP_DEBUG_ROUTINES(_("fsm_reset()"));

	fsm_p->state = (fsm_p->flags & (FSM_ACTIVE | FSM_PASSIVE))
			? fsmLISTEN : fsmCLOSED;
	fsm_p->retry = fsm_p->try_req;
	fsm_p->retry_nak = fsm_p->try_nak;

	(*fsm_p->pdc->reset)(fsm_p);
}
#endif


/************************************************************************/
/* Configuration negotiation complete */

#ifdef MODULE_fsm_opening
STATIC void
fsm_opening(fsm_p)
struct fsm_s *fsm_p;
{
	_fsm_log(fsm_p, _("Opened"));

	stop_timer(&(fsm_p->timer));

	(*fsm_p->pdc->opening)(fsm_p);
	fsm_p->state = fsmOPENED;
}
#endif


/************************************************************************/
/*			E V E N T   P R O C E S S I N G			*/
/************************************************************************/

/* Process incoming packet */

#ifdef MODULE_fsm_proc
void
fsm_proc(
struct fsm_s *fsm_p,
struct mbuf_s **bpp
){
	struct config_hdr hdr;

	PPPtrace = fsm_p->ppp_p->trace;
	PPPiface = fsm_p->ppp_p->iface;

	if ( ntohcnf(&hdr, bpp) == -1 )
		_fsm_log( fsm_p, _("short configuration packet") );

	if (PPPtrace > 1)
/*RPB*/
	{
		char *duptext1, *duptext2;

		if ((duptext1 = _strdup(fsmStates[fsm_p->state])) == NULL)
		{
			__getfail(fsmStates[fsm_p->state]);
			return;
		}
		if ((duptext2 = _strdup(fsmCodes[hdr.code])) == NULL)
		{
			__getfail(fsmCodes[hdr.code]);
			free(duptext1);
			return;
		}
		_trace_log(PPPiface, _("%s PPP/%s %-8s;"
			" Processing %s, id: %d, len: %d"),
			fsm_p->ppp_p->iface->name,
			fsm_p->pdc->name,
			duptext1,
			duptext2,
			hdr.id,	hdr.len);
		free(duptext1);
		free(duptext2);
	}
/*RPB*/

	hdr.len -= CONFIG_HDR_LEN;		/* Length includes envelope */
	trim_mbuf(bpp, hdr.len);		/* Trim off padding */

	switch(hdr.code) {
	case CONFIG_REQ:
		switch(fsm_p->state) {
		case fsmOPENED:		/* Unexpected event? */
			(*fsm_p->pdc->closing)(fsm_p);
			fsm_reset(fsm_p);
			/* fallthru */
		case fsmLISTEN:
			(*fsm_p->pdc->starting)(fsm_p);
			fsm_sendreq(fsm_p);
			/* fallthru */
		case fsmREQ_Sent:
		case fsmACK_Sent:	/* Unexpected event? */
			fsm_p->state =
			((*fsm_p->pdc->request)(fsm_p, &hdr, bpp) == 0)
				? fsmACK_Sent : fsmREQ_Sent;
			break;

		case fsmACK_Rcvd:
			if ((*fsm_p->pdc->request)(fsm_p, &hdr, bpp) == 0) {
				fsm_opening(fsm_p);
			} else {
				/* give peer time to respond */
				fsm_timer(fsm_p);
			}
			break;

		case fsmCLOSED:
			/* Don't accept any connections */
			fsm_sendtermack(fsm_p, hdr.id);
			/* fallthru */
		case fsmTERM_Sent:
			/* We are attempting to close connection; */
			/* wait for timeout to resend a Terminate Request */
			free_p(bpp);
			break;
		};
		break;

	case CONFIG_ACK:
		switch(fsm_p->state) {
		case fsmREQ_Sent:
			if ((*fsm_p->pdc->ack)(fsm_p, &hdr, bpp) == 0) {
				fsm_p->state = fsmACK_Rcvd;
			}
			break;

		case fsmACK_Sent:
			if ((*fsm_p->pdc->ack)(fsm_p, &hdr, bpp) == 0) {
				fsm_opening(fsm_p);
			}
			break;

		case fsmOPENED:		/* Unexpected event? */
			(*fsm_p->pdc->closing)(fsm_p);
			(*fsm_p->pdc->starting)(fsm_p);
			fsm_reset(fsm_p);
			/* fallthru */
		case fsmACK_Rcvd:	/* Unexpected event? */
			free_p(bpp);
			fsm_sendreq(fsm_p);
			fsm_p->state = fsmREQ_Sent;
			break;

		case fsmCLOSED:
		case fsmLISTEN:
			/* Out of Sync; kill the remote */
			fsm_sendtermack(fsm_p, hdr.id);
			/* fallthru */
		case fsmTERM_Sent:
			/* We are attempting to close connection; */
			/* wait for timeout to resend a Terminate Request */
			free_p(bpp);
			break;
		};
		break;

	case CONFIG_NAK:
		switch(fsm_p->state) {
		case fsmREQ_Sent:
		case fsmACK_Sent:
			/* Update our config request to reflect NAKed options */
			if ((*fsm_p->pdc->nak)(fsm_p, &hdr, bpp) == 0) {
				/* Send updated config request */
				fsm_sendreq(fsm_p);
			}
			break;

		case fsmOPENED:		/* Unexpected event? */
			(*fsm_p->pdc->closing)(fsm_p);
			(*fsm_p->pdc->starting)(fsm_p);
			fsm_reset(fsm_p);
			/* fallthru */
		case fsmACK_Rcvd:	/* Unexpected event? */
			free_p(bpp);
			fsm_sendreq(fsm_p);
			fsm_p->state = fsmREQ_Sent;
			break;

		case fsmCLOSED:
		case fsmLISTEN:
			/* Out of Sync; kill the remote */
			fsm_sendtermack(fsm_p, hdr.id);
			/* fallthru */
		case fsmTERM_Sent:
			/* We are attempting to close connection; */
			/* wait for timeout to resend a Terminate Request */
			free_p(bpp);
			break;
		};
		break;

	case CONFIG_REJ:
		switch(fsm_p->state) {
		case fsmREQ_Sent:
		case fsmACK_Sent:
			if((*fsm_p->pdc->reject)(fsm_p, &hdr, bpp) == 0) {
				fsm_sendreq(fsm_p);
			}
			break;

		case fsmOPENED:		/* Unexpected event? */
			(*fsm_p->pdc->closing)(fsm_p);
			(*fsm_p->pdc->starting)(fsm_p);
			fsm_reset(fsm_p);
			/* fallthru */
		case fsmACK_Rcvd:	/* Unexpected event? */
			free_p(bpp);
			fsm_sendreq(fsm_p);
			fsm_p->state = fsmREQ_Sent;
			break;

		case fsmCLOSED:
		case fsmLISTEN:
			/* Out of Sync; kill the remote */
			fsm_sendtermack(fsm_p, hdr.id);
			/* fallthru */
		case fsmTERM_Sent:
			/* We are attempting to close connection; */
			/* wait for timeout to resend a Terminate Request */
			free_p(bpp);
			break;
		};
		break;

	case TERM_REQ:
		_fsm_log(fsm_p, _("Peer requested Termination"));

		switch(fsm_p->state) {
		case fsmOPENED:
			fsm_sendtermack(fsm_p, hdr.id);
			(*fsm_p->pdc->closing)(fsm_p);
			(*fsm_p->pdc->stopping)(fsm_p);
			fsm_reset(fsm_p);
			break;

		case fsmACK_Rcvd:
		case fsmACK_Sent:
			fsm_p->state = fsmREQ_Sent;
			/* fallthru */
		case fsmREQ_Sent:
		case fsmTERM_Sent:
			/* waiting for timeout */
			/* fallthru */
		case fsmCLOSED:
		case fsmLISTEN:
			/* Unexpected, but make them happy */
			fsm_sendtermack(fsm_p, hdr.id);
			break;
		};
		break;

	case TERM_ACK:
		switch(fsm_p->state) {
		case fsmTERM_Sent:
			stop_timer(&(fsm_p->timer));

			_fsm_log(fsm_p, _("Terminated"));
			(*fsm_p->pdc->stopping)(fsm_p);
			fsm_reset(fsm_p);
			break;

		case fsmOPENED:
			/* Remote host has abruptly closed connection */
			_fsm_log(fsm_p, _("Terminated unexpectly"));
			(*fsm_p->pdc->closing)(fsm_p);
			fsm_reset(fsm_p);
			if ( fsm_sendreq(fsm_p) == 0 ) {
				fsm_p->state = fsmREQ_Sent;
			}
			break;

		case fsmACK_Sent:
		case fsmACK_Rcvd:
			fsm_p->state = fsmREQ_Sent;
			/* fallthru */
		case fsmREQ_Sent:
			/* waiting for timeout */
			/* fallthru */
		case fsmCLOSED:
		case fsmLISTEN:
			/* Unexpected, but no action needed */
			break;
		};
		break;

	case CODE_REJ:
		_trace_log(PPPiface,_("%s PPP/%s Code Reject;"
			" indicates faulty implementation"),
			fsm_p->ppp_p->iface->name,
			fsm_p->pdc->name);
		(*fsm_p->pdc->stopping)(fsm_p);
		fsm_reset(fsm_p);
		free_p(bpp);
		break;

	case PROT_REJ:
		_trace_log(PPPiface,_("%s PPP/%s Protocol Reject;"
			" please do not use this protocol"),
			fsm_p->ppp_p->iface->name,
			fsm_p->pdc->name);
		free_p(bpp);
		break;

	case ECHO_REQ:
		switch(fsm_p->state) {
		case fsmOPENED:
			fsm_send( fsm_p, ECHO_REPLY, hdr.id, bpp );
			break;

		case fsmCLOSED:
		case fsmLISTEN:
			/* Out of Sync; kill the remote */
			fsm_sendtermack(fsm_p, hdr.id);
			/* fallthru */
		case fsmREQ_Sent:
		case fsmACK_Rcvd:
		case fsmACK_Sent:
		case fsmTERM_Sent:
			/* ignore */
			free_p(bpp);
			break;
		};
		break;

	case ECHO_REPLY:
	case DISCARD_REQ:
	case QUALITY_REPORT:
		free_p(bpp);
		break;

	default:
		_trace_log(PPPiface,_("%s PPP/%s Unknown packet type: %d;"
			" Sending Code Reject"),
			fsm_p->ppp_p->iface->name,
			fsm_p->pdc->name,
			hdr.code);

		hdr.len += CONFIG_HDR_LEN;	/* restore length */
		htoncnf( &hdr, bpp );	/* put header back on */
		fsm_send( fsm_p, CODE_REJ, hdr.id, bpp );

		switch(fsm_p->state) {
		case fsmREQ_Sent:
		case fsmACK_Rcvd:
		case fsmACK_Sent:
		case fsmOPENED:
			fsm_p->state = fsmLISTEN;
			break;

		case fsmCLOSED:
		case fsmLISTEN:
		case fsmTERM_Sent:
			/* no change */
			break;
		};
		break;
	}
}
#endif


/************************************************************************/
/* Timeout while waiting for reply from remote host */

#ifdef MODULE_fsm_timeout
STATIC void
fsm_timeout(vp)
void *vp;
{
	struct fsm_s *fsm_p = (struct fsm_s *)vp;

	PPPtrace = fsm_p->ppp_p->trace;
	PPPiface = fsm_p->ppp_p->iface;

	_fsm_log( fsm_p, _("Timeout") );

	switch(fsm_p->state) {
	case fsmREQ_Sent:
	case fsmACK_Rcvd:
	case fsmACK_Sent:
		if (fsm_p->retry > 0) {
			fsm_sendreq(fsm_p);
			fsm_p->state = fsmREQ_Sent;
		} else {
			_fsm_log(fsm_p, _("Request retry exceeded"));
			fsm_reset(fsm_p);
		}
		break;

	case fsmTERM_Sent:
		if (fsm_p->retry > 0) {
			fsm_sendtermreq(fsm_p);
		} else {
			_fsm_log(fsm_p, _("Terminate retry exceeded"));
			(*fsm_p->pdc->stopping)(fsm_p);
			fsm_reset(fsm_p);
		}
		break;

	case fsmCLOSED:
	case fsmLISTEN:
	case fsmOPENED:
		/* nothing to do */
		break;
	}
}
#endif


/************************************************************************/
/*			I N I T I A L I Z A T I O N			*/
/************************************************************************/

/* Start FSM (after open event, and physical line up) */

#ifdef MODULE_fsm_start
void
fsm_start(fsm_p)
struct fsm_s *fsm_p;
{
	if ( fsm_p->pdv == NULL )
		return;

	PPPtrace = fsm_p->ppp_p->trace;
	PPPiface = fsm_p->ppp_p->iface;

	_fsm_log(fsm_p, _("Start"));

	if ( !(fsm_p->flags & (FSM_ACTIVE | FSM_PASSIVE)) )
		return;

	switch ( fsm_p->state ) {
	case fsmCLOSED:
	case fsmLISTEN:
	case fsmTERM_Sent:
		(*fsm_p->pdc->starting)(fsm_p);
		fsm_reset(fsm_p);

		if ( fsm_p->flags & FSM_ACTIVE ){
			fsm_sendreq(fsm_p);
			fsm_p->state = fsmREQ_Sent;
		}
		break;
	default:
		/* already started */
		break;
	};
}
#endif


/************************************************************************/
/* Physical Line Down Event */

#ifdef MODULE_fsm_down
void
fsm_down(fsm_p)
struct fsm_s *fsm_p;
{
	if ( fsm_p->pdv == NULL )
		return;

	PPPtrace = fsm_p->ppp_p->trace;
	PPPiface = fsm_p->ppp_p->iface;

	_fsm_log(fsm_p, _("Down"));

	switch ( fsm_p->state ) {
	case fsmREQ_Sent:
	case fsmACK_Rcvd:
	case fsmACK_Sent:
		stop_timer(&(fsm_p->timer));
		fsm_reset(fsm_p);
		break;

	case fsmOPENED:
		(*fsm_p->pdc->closing)(fsm_p);
		/* fallthru */
	case fsmTERM_Sent:
		fsm_reset(fsm_p);
		break;

	case fsmCLOSED:
	case fsmLISTEN:
		/* nothing to do */
		break;
	};
}
#endif


/************************************************************************/
/* Close the connection */

#ifdef MODULE_fsm_close
void
fsm_close(fsm_p)
struct fsm_s *fsm_p;
{
	if ( fsm_p->pdv == NULL )
		return;

	PPPtrace = fsm_p->ppp_p->trace;
	PPPiface = fsm_p->ppp_p->iface;

	_fsm_log(fsm_p, _("Close"));

	switch ( fsm_p->state ) {
	case fsmOPENED:
		(*fsm_p->pdc->closing)(fsm_p);
		/* fallthru */
	case fsmACK_Sent:
		fsm_p->retry = fsm_p->try_terminate;
		fsm_sendtermreq(fsm_p);
		fsm_p->state = fsmTERM_Sent;
		break;

	case fsmREQ_Sent:
	case fsmACK_Rcvd:
		/* simply wait for REQ timeout to expire */
		fsm_p->retry = 0;
		fsm_p->state = fsmTERM_Sent;
		break;

	case fsmLISTEN:
		fsm_p->state = fsmCLOSED;
		break;

	case fsmTERM_Sent:
	case fsmCLOSED:
		/* nothing to do */
		break;
	};
}
#endif


/************************************************************************/
/* Initialize the fsm for this protocol
 * Called from protocol _init
 */

#ifdef MODULE_fsm_init
void
fsm_init(fsm_p)
struct fsm_s *fsm_p;
{
	struct timer *t = &(fsm_p->timer);

	PPP_DEBUG_ROUTINES(_("fsm_init()"));

	fsm_p->try_req = fsm_p->pdc->try_req;
	fsm_p->try_nak = fsm_p->pdc->try_nak;
	fsm_p->try_terminate = fsm_p->pdc->try_terminate;
	fsm_reset(fsm_p);

	/* Initialize timer */
	t->func = (void (*)())fsm_timeout;
	t->arg = (void *)fsm_p;
	set_timer(t, fsm_p->pdc->timeout);
	fsm_timer(fsm_p);
	stop_timer(t);
}
#endif



#ifdef MODULE_fsm_free
void
fsm_free(fsm_p)
struct fsm_s *fsm_p;
{
	if ( fsm_p->pdv != NULL ) {
		(*fsm_p->pdc->free)(fsm_p);

		free( fsm_p->pdv );
		fsm_p->pdv = NULL;
	}
}
#endif


