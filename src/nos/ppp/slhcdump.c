#include <stdio.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include <libintl.h> /*RPB*/
#include "internet/internet.h"
#include "internet/ip.h"
#include "ppp/slhc.h"
#include "dump/trace.h"
#include "po/messages.h"

static uint16 decodeint(struct mbuf_s **bpp);

static uint16
decodeint(bpp)
struct mbuf_s **bpp;
{
	uint8 tmpbuf[2];

	pullup(bpp,tmpbuf,1);
	if (tmpbuf[0] == 0)
		pullup(bpp,tmpbuf,2);
	else {
	 	tmpbuf[1] = tmpbuf[0];
		tmpbuf[0] = 0;
	}
	return(get16(tmpbuf));
}

void
vjcomp_dump(ifp,bpp,unused)
struct iface_s *ifp;
struct mbuf_s **bpp;
int unused;
{
	uint8 changes;
	uint8 tmpbuf[2];

	if(bpp == NULL || *bpp == NULL)
		return;

	/* Dump compressed TCP/IP header */
	changes = pullchar(bpp);
	_tprintf(ifp, _("\tchanges: 0x%02x"),changes);
	if (changes & NEW_C) {
		pullup(bpp,tmpbuf,1);
		_tprintf(ifp, _("   connection: 0x%02x"),tmpbuf[0]);
	}
	pullup(bpp,tmpbuf,2);
	_tprintf(ifp, _("   TCP checksum: 0x%04x"),get16(tmpbuf));

	if (changes & TCP_PUSH_BIT)
		_tprintf(ifp, _("   PUSH"));
	_tprintf(ifp, _("\n"));

	switch (changes & SPECIALS_MASK) {
	case SPECIAL_I:
		_tprintf(ifp, _("\tdelta ACK and delta SEQ implied by length of data\n"));
		break;

	case SPECIAL_D:
		_tprintf(ifp, _("\tdelta SEQ implied by length of data\n"));
		break;

	default:
		if (changes & NEW_U) {
			_tprintf(ifp, _("\tUrgent pointer: 0x%02x"),decodeint(bpp));
		}
		if (changes & NEW_W)
			_tprintf(ifp, _("\tdelta WINDOW: 0x%02x"),decodeint(bpp));
		if (changes & NEW_A)
			_tprintf(ifp, _("\tdelta ACK: 0x%02x"),decodeint(bpp));
		if (changes & NEW_S)
			_tprintf(ifp, _("\tdelta SEQ: 0x%02x"),decodeint(bpp));
		break;
	};
	if (changes & NEW_I) {
		_tprintf(ifp, _("\tdelta ID: 0x%02x\n"),decodeint(bpp));
	} else {
		_tprintf(ifp, _("\tincrement ID\n"));
	}
}


/* dump serial line IP packet; may have Van Jacobson TCP header compression */
void
sl_dump(ifp,bpp,unused)
struct iface_s *ifp;
struct mbuf_s **bpp;
int unused;
{
	struct mbuf_s *bp, *tbp;
	unsigned char c;
	int len;

	bp = *bpp;
	c = bp->data[0];
	if (c & SL_TYPE_COMPRESSED_TCP) {
		_tprintf(ifp, _("serial line VJ Compressed TCP: len %3u\n"),
			len_p(*bpp));
		vjcomp_dump(ifp,bpp,0);
	} else if ( c >= SL_TYPE_UNCOMPRESSED_TCP ) {
		_tprintf(ifp, _("serial line VJ Uncompressed TCP: len %3u\n"),
			len = len_p(bp));
		/* Get our own copy so we can mess with the data */
		if ( (tbp = copy_p(bp, len)) == NULL )
			return;

		_tprintf(ifp, _("\tconnection ID = %d\n"),
			tbp->data[9]);	/* FIX THIS! */
		/* Restore the bytes used with Uncompressed TCP */
		tbp->data[0] &= 0x4f;		/* FIX THIS! */
		tbp->data[9] = TCP_PTCL;	/* FIX THIS! */
		/* Dump contents as a regular IP packet */
		ip_dump(ifp,&tbp,1);
		free_p(&tbp);
	} else {
		_tprintf(ifp, _("serial line IP: len: %3u\n"),len_p(*bpp));
		ip_dump(ifp,bpp,1);
	}
}


