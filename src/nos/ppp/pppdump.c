/*
 *	PPPDUMP.C
 *
 *	12-89	-- Katie Stevens (dkstevens@ucdavis.edu)
 *		   UC Davis, Computing Services
 *	PPP.08	05-90	[ks] improve tracing reports
 *	PPP.09  05-90	[ks] add UPAP packet reporting
 *	PPP.14	08-90	[ks] change UPAP to PAP for consistency with RFC1172
 *	PPP.15	09-90	[ks] update to KA9Q NOS v900828
 *	Jan 91	[Bill Simpson] small changes to match rewrite of PPP
 *	Aug 91	[Bill Simpson] fixed some buffer loss
 */
#include <stdio.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include "main/iface.h"
#include <libintl.h> /*RPB*/
#include "internet/internet.h"
#include "ppp/ppp.h"
#include "dump/trace.h"
#include "po/messages.h"

#ifdef TURBOC_SWITCH_BUG
#pragma option -G-
#endif

/* dump a PPP packet */
void
ppp_dump(ifp,bpp,unused)
struct iface_s *ifp;
struct mbuf_s **bpp;
int unused;
{
	struct ppp_hdr hdr;
	struct mbuf_s *tbp;

	_tprintf(ifp, _("PPP: len %3u\t"), len_p(*bpp));

	/* HDLC address and control fields may be compressed out */
	if ((byte_t)(*bpp)->data[0] != HDLC_ALL_ADDR) {
		_tprintf(ifp, _("(compressed ALL/UI)\t"));
	} else if ((byte_t)(*bpp)->data[1] != HDLC_UI) {
		_tprintf(ifp, _("(missing UI!)\t"));
	} else {
		/* skip address/control fields */
		pull16(bpp);
	}

	/* Initialize the expected header */
	hdr.addr = HDLC_ALL_ADDR;
	hdr.control = HDLC_UI;
	hdr.protocol = PULLCHAR(bpp);

	/* First byte of PPP protocol field may be compressed out */
	if ( hdr.protocol & 0x01 ) {
		_tprintf(ifp, _("compressed "));
	} else {
		hdr.protocol = (hdr.protocol << 8) | PULLCHAR(bpp);

		/* Second byte of PPP protocol field must be odd */
		if ( !(hdr.protocol & 0x01) ) {
			_tprintf(ifp, _("(not odd!) ") );
		}
	}

	_tprintf(ifp, _("protocol: "));
	switch(hdr.protocol){
		case PPP_IP_PROTOCOL:
			_tprintf(ifp, _("IP\n"));
			ip_dump(ifp,bpp,1);
			break;
		case PPP_IPCP_PROTOCOL:
			_tprintf(ifp, _("IPCP\n"));
			break;
		case PPP_LCP_PROTOCOL:
			_tprintf(ifp, _("LCP\n"));
			break;
		case PPP_PAP_PROTOCOL:
			_tprintf(ifp, _("PAP\n"));
			break;
		case PPP_COMPR_PROTOCOL:
			_tprintf(ifp, _("VJ Compressed TCP/IP\n"));
			vjcomp_dump(ifp,bpp,0);
			break;
		case PPP_UNCOMP_PROTOCOL:
			_tprintf(ifp, _("VJ Uncompressed TCP/IP\n"));
			/* Get our own copy so we can mess with the data */
			if ( (tbp = copy_p(*bpp, len_p(*bpp))) == NULL)
				return;

			_tprintf(ifp, _("\tconnection 0x%02x\n"),
				tbp->data[9]);		/* FIX THIS! */
			/* Restore the bytes used with Uncompressed TCP */
			tbp->data[9] = TCP_PTCL;	/* FIX THIS! */
			ip_dump(ifp,&tbp,1);
			free_p(&tbp);
			break;
		default:
			_tprintf(ifp, _("unknown 0x%04x\n"),hdr.protocol);
			break;
	}
}

#ifdef TURBOC_SWITCH_BUG
#pragma option -G
#endif
