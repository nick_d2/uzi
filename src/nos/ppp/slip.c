/* SLIP (Serial Line IP) encapsulation and control routines.
 * Copyright 1991 Phil Karn
 *
 * Van Jacobsen header compression hooks added by Katie Stevens, UC Davis
 *
 *	- Feb 1991	Bill_Simpson@um.cc.umich.edu
 *			reflect changes to header compression calls
 *			revise status display
 *	- Dec 2003	RPB: - adapted for use on escc i.s.o. asy
 *			     - added slip_iostatus()
 */
#include <stdio.h>
#include "nos/global.h"
#include "dump/trace.h"
#include "internet/ip.h"
#include "kernel/dprintf.h"
#include "main/devparam.h"
#include "main/iface.h"
#include "main/mbuf.h"
#include "ppp/slhc.h"
#include "ppp/slip.h"
/*#include "x86/pktdrvr.h"*/
#include "z80/escc.h"
#include "main/main.h" /* for driver_process_p */
#include <libintl.h>
#include "po/messages.h"

static int slip_raw(struct iface_s *iface,struct mbuf_s **data);
static struct mbuf_s *slip_encode(struct mbuf_s **bpp);
#if 0
static struct mbuf_s *slip_decode(struct slip *sp,uint8 c);
#endif
static void slip_rx(int xdev,void *p1,void *p2);
static void slip_status(struct iface_s *iface);

/* Slip level control structure */
struct slip Slip[SLIP_MAX];

static int slip_iostatus(struct iface_s *ifp, int command, int32 value);

int
slip_init(ifp)
struct iface_s *ifp;
{
	/* Do only hardware independant init here */
	int xdev;
	struct slip *sp;
	char *ifn;

	for(xdev = 0;xdev < SLIP_MAX;xdev++){
		sp = &Slip[xdev];
		if(sp->iface == ifp ||
		   sp->iface == NULL)
			break;
	}
	if(xdev >= SLIP_MAX) {
		_printf(_("Too many slip devices\n"));
		return -1;
	}

	ifp->iostatus = slip_iostatus;
	ifp->raw = slip_raw;
	ifp->show = slip_status;
	ifp->xdev = xdev;

	sp->iface = ifp;

	ifn = if_name(ifp, _(" rx"));
	ifp->rxproc = process_thread_create(driver_process_p, ifn,
			256 /*384*/, slip_rx, xdev, NULL, NULL, 0);
	free(ifn);

	return 0;
}

int
slip_free(ifp)
struct iface_s *ifp;
{
	struct slip *sp;

	sp = &Slip[ifp->xdev];
	if(sp->slcomp != NULL){
		slhc_free(sp->slcomp);
		sp->slcomp = NULL;
	}
	sp->iface = NULL;
	return 0;
}

/* Send routine for point-to-point slip, no VJ header compression */
int
slip_send(
struct mbuf_s **bpp,	/* Buffer to send */
struct iface_s *iface,	/* Pointer to interface control block */
int32 gateway,		/* Ignored (SLIP is point-to-point) */
uint8 tos
){
	if(iface == NULL){
		free_p(bpp);
		return -1;
	}
	return (*iface->raw)(iface,bpp);
}

/* Send routine for point-to-point slip, with VJ header compression */
int
vjslip_send(
struct mbuf_s **bpp,	/* Buffer to send */
struct iface_s *iface,	/* Pointer to interface control block */
int32 gateway,		/* Ignored (SLIP is point-to-point) */
uint8 tos
){
	register struct slip *sp;
	int type;

	if(iface == NULL){
		free_p(bpp);
		return -1;
	}
	sp = &Slip[iface->xdev];

	/* Attempt IP/ICP header compression */
	type = slhc_compress(sp->slcomp,bpp,TRUE);
	(*bpp)->data[0] |= type;

	return (*iface->raw)(iface,bpp);
}

/* Send a raw slip frame */
static int
slip_raw(
struct iface_s *iface,
struct mbuf_s **bpp
){
	struct mbuf_s *bp1;
	struct async_s *async_p;

	dump(iface,IF_TRACE_OUT,*bpp);
	iface->rawsndcnt++;
	iface->lastsent = secclock();
	if((bp1 = slip_encode(bpp)) == NULL){
		return -1;
	}
	if (iface->trace & IF_TRACE_RAW)
		raw_dump(iface,-1,bp1);

	async_p = &Esccchan[iface->dev]->async;
	return async_send(async_p,&bp1);
}

/* Encode a packet in SLIP format */
static
struct mbuf_s *
slip_encode(struct mbuf_s **bpp)
{
	struct mbuf_s *lbp;	/* Mbuf containing line-ready packet */
	register uint8 *cp;
	int c;

	/* Allocate output mbuf that's twice as long as the packet.
	 * This is a worst-case guess (consider a packet full of FR_ENDs!)
	 */
	lbp = alloc_mbuf((uint16)(2*len_p(*bpp) + 2));
	if(lbp == NULL){
		/* No space; drop */
		free_p(bpp);
		return NULL;
	}
	cp = lbp->data;

	/* Flush out any line garbage */
	*cp++ = FR_END;

	/* Copy input to output, escaping special characters */
	while((c = PULLCHAR(bpp)) != -1){
		switch(c){
		case FR_ESC:
			*cp++ = FR_ESC;
			*cp++ = T_FR_ESC;
			break;
		case FR_END:
			*cp++ = FR_ESC;
			*cp++ = T_FR_END;
			break;
		default:
			*cp++ = c;
		}
	}
	*cp++ = FR_END;
	lbp->cnt = cp - lbp->data;
	return lbp;
}

/* Process incoming bytes in SLIP format
 * When a buffer is complete, return it; otherwise NULL
 */
#if 0
static
struct mbuf_s *
slip_decode(sp,ch)
register struct slip *sp;
uint8 ch;		/* Incoming character */
{
	struct mbuf_s *bp;

	switch(ch){
	case FR_END:
		bp = sp->rbp_head;
		sp->rbp_head = NULL;
		if(sp->escaped){
			/* Treat this as an abort - discard frame */
			free_p(&bp);
			/*bp = NULL;*/
#if DEBUG >= 1
 dprintf(1, "slip_decode() abort\n");
#endif
		}
		sp->escaped &= ~SLIP_FLAG;
		return bp;	/* Will be NULL if empty frame */
	case FR_ESC:
		sp->escaped |= SLIP_FLAG;
		return NULL;
	}
	if(sp->escaped & SLIP_FLAG){
		/* Translate 2-char escape sequence back to original char */
		sp->escaped &= ~SLIP_FLAG;
		switch(ch){
		case T_FR_ESC:
			ch = FR_ESC;
			break;
		case T_FR_END:
			ch = FR_END;
			break;
		default:
			sp->errors++;
			break;
		}
	}
	/* We reach here with a character for the buffer;
	 * make sure there's space for it
	 */
	if(sp->rbp_head == NULL){
		/* Allocate first mbuf for new packet */
		if((sp->rbp_tail = sp->rbp_head = alloc_mbuf(SLIP_ALLOC)) == NULL)
 {
 abyte('!');
			return NULL; /* No memory, drop */
 }
		sp->rcp = sp->rbp_head->data;
	} else if(sp->rbp_tail->cnt == SLIP_ALLOC){
		/* Current mbuf is full; link in another */
		if((sp->rbp_tail->next = alloc_mbuf(SLIP_ALLOC)) == NULL){
			/* No memory, drop whole thing */
			free_p(&sp->rbp_head);
			sp->rbp_head = NULL;
#if DEBUG >= 1
 dprintf(1, "slip_decode() drop\n");
#endif
			return NULL;
		}
		sp->rbp_tail = sp->rbp_tail->next;
		sp->rcp = sp->rbp_tail->data;
	}
	/* Store the character, increment fragment and total
	 * byte counts
	 */
	*sp->rcp++ = ch;
	sp->rbp_tail->cnt++;
	return NULL;
}
#endif

/* Process SLIP line input */
static void
slip_rx(xdev,p1,p2)
int xdev;
void *p1;
void *p2;
	{
	unsigned int ch;
	struct mbuf_s *bp;
	register struct slip *sp;
	struct async_s *async_p;
#if 1
	struct mbuf_s *input_mbuf_p;
#endif

	sp = &Slip[xdev];
	async_p = &Esccchan[sp->iface->dev]->async;

#if 1
	while (async_recv(async_p, &input_mbuf_p, 0) == 0)
		{
		while (input_mbuf_p)
			{
			while (input_mbuf_p->cnt)
				{
				ch = *input_mbuf_p->data++;
				input_mbuf_p->cnt--;
#else
	while ((ch = async_getc(async_p)) != -1)
				{
#endif
#if 1
	switch(ch){
	case FR_END:
		bp = sp->rbp_head;
		sp->rbp_head = NULL;
		if(sp->escaped){
			/* Treat this as an abort - discard frame */
			free_p(&bp);
			/*bp = NULL;*/
#if DEBUG >= 1
 dprintf(1, "slip_rx() abort\n");
#endif
		}
		sp->escaped &= ~SLIP_FLAG;
#else
				bp = slip_decode(sp, ch);
#endif
				if (bp == NULL)
					{
					continue;	/* More to come */
					}

				if (sp->iface->trace & IF_TRACE_RAW)
					{
					raw_dump(sp->iface,IF_TRACE_IN,bp);
					}

				ch = *bp->data;
				if (ch & SL_TYPE_COMPRESSED_TCP)
					{
					if (slhc_uncompress(sp->slcomp, &bp) <=
							0)
						{
						free_p(&bp);
						sp->errors++;
						continue;
						}
					}
				else if (ch >= SL_TYPE_UNCOMPRESSED_TCP)
					{
					*bp->data = ch & 0x4f;
					if (slhc_remember(sp->slcomp, &bp) <=
							0)
						{
						free_p(&bp);
						sp->errors++;
						continue;
						}
					}
				net_route(sp->iface, &bp);

				/* Especially on slow machines, serial I/O can be quite
				 * compute intensive, so release the machine before we
				 * do the next packet.  This will allow this packet to
				 * go on toward its ultimate destination. [Karn]
				 */
				kwait(NULL);
#if 1
		continue;
	case FR_ESC:
		sp->escaped |= SLIP_FLAG;
		continue;
	}
	if(sp->escaped & SLIP_FLAG){
		/* Translate 2-char escape sequence back to original char */
		sp->escaped &= ~SLIP_FLAG;
		switch(ch){
		case T_FR_ESC:
			ch = FR_ESC;
			break;
		case T_FR_END:
			ch = FR_END;
			break;
		default:
			sp->errors++;
			break;
		}
	}
	/* We reach here with a character for the buffer;
	 * make sure there's space for it
	 */
	if(sp->rbp_head == NULL){
		/* Allocate first mbuf for new packet */
		if((sp->rbp_tail = sp->rbp_head = alloc_mbuf(SLIP_ALLOC)) == NULL)
 {
 abyte('!');
			continue; /* No memory, drop */
 }
		sp->rcp = sp->rbp_head->data;
	} else if(sp->rbp_tail->cnt == SLIP_ALLOC){
		/* Current mbuf is full; link in another */
		if((sp->rbp_tail->next = alloc_mbuf(SLIP_ALLOC)) == NULL){
			/* No memory, drop whole thing */
			free_p(&sp->rbp_head);
			sp->rbp_head = NULL;
#if DEBUG >= 1
 dprintf(1, "slip_rx() drop\n");
#endif
			continue;
		}
		sp->rbp_tail = sp->rbp_tail->next;
		sp->rcp = sp->rbp_tail->data;
	}
	/* Store the character, increment fragment and total
	 * byte counts
	 */
	*sp->rcp++ = ch;
	sp->rbp_tail->cnt++;
#endif
#if 1
				}
			input_mbuf_p = free_mbuf(&input_mbuf_p);
			}
#endif
		}

	if (sp->iface->rxproc == my_thread_p)
		{
		sp->iface->rxproc = NULL;
		}
	}

/* Show serial line status */
static void
slip_status(iface)
struct iface_s *iface;
{
	struct slip *sp;

	if (iface->xdev > SLIP_MAX)
		/* Must not be a SLIP device */
		return;

	sp = &Slip[iface->xdev];
	if (sp->iface != iface)
		/* Must not be a SLIP device */
		return;

	slhc_i_status(sp->slcomp);
	slhc_o_status(sp->slcomp);
}

/* Keep track of changes in I-O status */
/* (called through iface iostatus vector) */

static int
slip_iostatus(ifp, command, value)
struct iface_s *ifp;
int command;
int32 value;
{

        switch (command) {
        case PARAM_UP:
        case PARAM_DOWN:
        case PARAM_OPEN:
        case PARAM_CLOSE:
        case PARAM_LISTEN:
                return 0;
        };
        return -1;
}

