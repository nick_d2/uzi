/* IP header tracing routines
 * Copyright 1991 Phil Karn, KA9Q
 */
#include <stdio.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include <libintl.h> /*RPB*/
#include "internet/internet.h"
#include "main/iface.h"
#include "internet/ip.h"
#include "dump/trace.h"
#include "internet/netuser.h"
#include "kernel/session.h"
#include <libintl.h>
#include "po/messages.h"

static void ipldump(struct iface_s *ifp,struct ip *ip,struct mbuf_s **bpp,int check);

void
ip_dump(
struct iface_s *ifp,
struct mbuf_s **bpp,
int check
){
	struct ip ip;
	uint16 ip_len;
	uint16 csum;

	if(bpp == NULL || *bpp == NULL)
		return;

	/* Sneak peek at IP header and find length */
	ip_len = ((*bpp)->data[0] & 0xf) << 2;
	if(ip_len < IPLEN){
		_tprintf(ifp, _("IP: bad header\n"));
		return;
	}
	if(check && cksum(NULL,*bpp,ip_len) != 0)
		_tprintf(ifp, _("IP: CHECKSUM ERROR (%u)\n"),csum);

	ntohip(&ip,bpp);	/* Can't fail, we've already checked ihl */
	ipldump(ifp,&ip,bpp,check);
}

#ifdef IPIP
void
ipip_dump(
struct iface_s *ifp,
struct mbuf_s **bpp,
int32 source,
int32 dest,
int check
){
	ip_dump(ifp,bpp,check);
}
#endif

static void
ipldump(ifp,ip,bpp,check)
struct iface_s *ifp;
struct ip *ip;
struct mbuf_s **bpp;
int check;
{
	uint16 length;
	int i;

	/* Trim data segment if necessary. */
	length = ip->length - (IPLEN + ip->optlen);	/* Length of data portion */
	trim_mbuf(bpp,length);
	_tprintf(ifp, _("IP: len %u"),ip->length);
	_tprintf(ifp, _(" %s"),inet_ntoa(ip->source));
	_tprintf(ifp, _("->%s ihl %u ttl %u"),
		inet_ntoa(ip->dest),IPLEN + ip->optlen,ip->ttl);
	if(ip->tos != 0)
		_tprintf(ifp, _(" tos %u"),ip->tos);
	if(ip->offset != 0 || ip->flags.mf)
		_tprintf(ifp, _(" id %u offs %u"),ip->id,ip->offset);
	if(ip->flags.df)
		_tprintf(ifp, _(" DF"));
	if(ip->flags.mf){
		_tprintf(ifp, _(" MF"));
		check = 0;	/* Bypass host-level checksum verify */
	}
	if(ip->flags.congest){
		_tprintf(ifp, _(" CE"));
	}
	if(ip->offset != 0){
		tputc('\n',ifp);
		return;
	}
	for(i=0;Iplink[i].proto != 0;i++){
		if(Iplink[i].proto == ip->protocol){
			_tprintf(ifp, _(" prot %s\n"),Iplink[i].name);
			(*Iplink[i].dump)(ifp,bpp,ip->source,ip->dest,check);
			return;
		}
	}
	_tprintf(ifp, _(" prot %u\n"),ip->protocol);
}
/* Dump a locally sent or received IP datagram to the command interp session */
void
dumpip(iface,ip,bp,spi)
struct iface_s *iface;
struct ip *ip;
struct mbuf_s *bp;
int32 spi;
{
	struct mbuf_s *bpp;

 	if(iface != NULL){
		_printf(_("ip_recv(%s)"),iface->name);
		if(spi != 0)
			_printf(_(" spi %lx"),spi);
		_printf(_("\n"));
	} else
		_printf(_("ip_send\n"));

	dup_p(&bpp,bp,0,len_p(bp));
	ipldump(iface,ip,&bpp,1);
	free_p(&bpp);
	_printf(_("\n"));
}

