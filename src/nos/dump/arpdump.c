/* ARP packet tracing routines
 * Copyright 1991 Phil Karn, KA9Q
 * Apr 06      RPB     Changes needed for intl' of strings
 */
#include <stdio.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include "internet/arp.h"
#include "internet/netuser.h"
#include "dump/trace.h"
#include "main/smsg.h"
#include <libintl.h>
#include "po/messages.h"

void
arp_dump(ifp,bpp)
struct iface_s *ifp;
struct mbuf_s **bpp;
{
	struct arp arp;
	struct arp_type *at;
	int is_ip = 0;
	char tmp[25];
	char *duptext; /*RPB*/

	if(bpp == NULL || *bpp == NULL)
		return;
	_tprintf(ifp, _("ARP: len %d"),len_p(*bpp));
	if(ntoharp(&arp,bpp) == -1){
		_tprintf(ifp, _(" bad packet\n"));
		return;
	}
	if(arp.hardware < NHWTYPES)
		at = &Arp_type[arp.hardware];
	else
		at = NULL;

	/* Print hardware type in Ascii if known, numerically if not */
	duptext = smsg(Arptypes,NHWTYPES,arp.hardware); /*RPB*/
	_tprintf(ifp, _(" hwtype %s"), duptext); /*RPB*/
	free(duptext); /*RPB*/

	/* Print hardware length only if unknown type, or if it doesn't match
	 * the length in the known types table
	 */
	if(at == NULL || arp.hwalen != at->hwalen)
		_tprintf(ifp, _(" hwlen %u"),arp.hwalen);

	/* Check for most common case -- upper level protocol is IP */
	if(at != NULL && arp.protocol == at->iptype){
		_tprintf(ifp, _(" prot IP"));
		is_ip = 1;
	} else {
		_tprintf(ifp, _(" prot 0x%x prlen %u"),arp.protocol,arp.pralen);
	}
	switch(arp.opcode){
	case ARP_REQUEST:
		_tprintf(ifp, _(" op REQUEST"));
		break;
	case ARP_REPLY:
		_tprintf(ifp, _(" op REPLY"));
		break;
	case REVARP_REQUEST:
		_tprintf(ifp, _(" op REVERSE REQUEST"));
		break;
	case REVARP_REPLY:
		_tprintf(ifp, _(" op REVERSE REPLY"));
		break;
	default:
		_tprintf(ifp, _(" op %u"),arp.opcode);
		break;
	}
	_tprintf(ifp, _("\n"));
	_tprintf(ifp, _("sender"));
	if(is_ip)
		_tprintf(ifp, _(" IPaddr %s"),inet_ntoa(arp.sprotaddr));
	_tprintf(ifp, _(" hwaddr %s\n"),at->format(tmp,arp.shwaddr));

	_tprintf(ifp, _("target"));
	if(is_ip)
		_tprintf(ifp, _(" IPaddr %s"),inet_ntoa(arp.tprotaddr));
	_tprintf(ifp, _(" hwaddr %s\n"),at->format(tmp,arp.thwaddr));
}
