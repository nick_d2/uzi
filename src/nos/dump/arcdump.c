/* ARCNET trace routines
 * Copyright 1990 Russ Nelson
 */

#include <stdio.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include "nos/arcnet.h"
#include "dump/trace.h"
#include <libintl.h>
#include "po/messages.h"

void
arc_dump(ifp,bpp,check)
struct iface_s *ifp;
struct mbuf_s **bpp;
int check;	/* Not used */
{
	struct arc ahdr;
	char s[20],d[20];

	ntoharc(&ahdr,bpp);
	parc(s,ahdr.source);
	parc(d,ahdr.dest);
	_tprintf(ifp, _("Arcnet: len %u %s->%s"),ARCLEN + len_p(*bpp),s,d);

	switch(ahdr.type){
		case ARC_IP:
			_tprintf(ifp, _(" type IP\n"));
			ip_dump(fp,bpp,1);
			break;
		case ARC_ARP:
			_tprintf(ifp, _(" type ARP\n"));
			arp_dump(fp,bpp);
			break;
		default:
			_tprintf(ifp, _(" type 0x%x\n"),ahdr.type);
			break;
	}
}
int
arc_forus(iface,bp)
struct iface_s *iface;
struct mbuf_s *bp;
{
	return 1;
}
