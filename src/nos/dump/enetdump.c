/* Ethernet header tracing routines
 * Copyright 1991 Phil Karn, KA9Q
 */
#include <stdio.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include "x86/enet.h"
#include "dump/trace.h"
#include <libintl.h>
#include "po/messages.h"

void
ether_dump(
struct iface_s *ifp,
struct mbuf_s **bpp,
int check	/* Not used */
){
	struct ether ehdr;
	char s[20],d[20];

	ntohether(&ehdr,bpp);
	pether(s,ehdr.source);
	pether(d,ehdr.dest);
	_tprintf(ifp, _("Ether: len %u %s->%s"),ETHERLEN + len_p(*bpp),s,d);

	switch(ehdr.type){
		case IP_TYPE:
			_tprintf(ifp, _(" type IP\n"));
			ip_dump(fp,bpp,1);
			break;
		case REVARP_TYPE:
			_tprintf(ifp, _(" type REVARP\n"));
			arp_dump(fp,bpp);
			break;
		case ARP_TYPE:
			_tprintf(ifp, _(" type ARP\n"));
			arp_dump(fp,bpp);
			break;
		default:
			_tprintf(ifp, _(" type 0x%x\n"),ehdr.type);
			break;
	}
}
int
ether_forus(struct iface_s *iface,struct mbuf_s *bp)
{
	/* Just look at the multicast bit */

	if(bp->data[0] & 1)
		return 0;
	else
		return 1;
}
