/* UDP packet tracing
 * Copyright 1991 Phil Karn, KA9Q
 */
#include <stdio.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include "internet/netuser.h"
#include <libintl.h> /*RPB*/
#include "internet/internet.h"
#include "internet/udp.h"
#include "internet/ip.h"
#include "socket/socket.h"
#include "dump/trace.h"
#include "po/messages.h"

/* Dump a UDP header */
void
udp_dump(ifp,bpp,source,dest,check)
struct iface_s *ifp;
struct mbuf_s **bpp;
int32 source,dest;
int check;		/* If 0, bypass checksum verify */
{
	struct udp udp;
	struct pseudo_header ph;
	uint16 csum;

	if(bpp == NULL || *bpp == NULL)
		return;

	_tprintf(ifp, _("UDP:"));

	/* Compute checksum */
	ph.source = source;
	ph.dest = dest;
	ph.protocol = UDP_PTCL;
	ph.length = len_p(*bpp);
	if((csum = cksum(&ph,*bpp,ph.length)) == 0)
		check = 0;	/* No checksum error */

	ntohudp(&udp,bpp);

	_tprintf(ifp, _(" len %u"),udp.length);
	_tprintf(ifp, _(" %u->%u"),udp.source,udp.dest);
	if(udp.length > UDPHDR)
		_tprintf(ifp, _(" Data %u"),udp.length - UDPHDR);
	if(udp.checksum == 0)
		check = 0;
	if(check)
		_tprintf(ifp, _(" CHECKSUM ERROR (%u)"),csum);
	tputc('\n',ifp);

#ifdef RIP /* Nick */
	switch(udp.dest){
	case IPPORT_RIP:
		rip_dump(fp,bpp);
	}
#endif
}

