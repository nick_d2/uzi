/* RIP packet tracing
 * Copyright 1991 Phil Karn, KA9Q
 */
#include "nos/global.h"
#include "main/mbuf.h"
#include "internet/netuser.h"
#include "main/timer.h"
#include "internet/rip.h"
#include "dump/trace.h"
#include <libintl.h>
#include "po/messages.h"

void
rip_dump(ifp,bpp)
struct iface_s *ifp;
struct mbuf_s **bpp;
{
	struct rip_route entry;
	int i;
	int cmd,version;
	uint16 len;

	_tprintf(ifp, _("RIP: "));
	cmd = PULLCHAR(bpp);
	version = PULLCHAR(bpp);
	switch(cmd){
	case RIPCMD_REQUEST:
		_tprintf(ifp, _("REQUEST"));
		break;
	case RIPCMD_RESPONSE:
		_tprintf(ifp, _("RESPONSE"));
		break;
	default:
		_tprintf(ifp, _(" cmd %u"),cmd);
		break;
	}

	pull16(bpp);	/* remove one word of padding */

	len = len_p(*bpp);
	_tprintf(ifp, _(" vers %u entries %u:\n"),version,len / RIPROUTE);

	i = 0;
	while(len >= RIPROUTE){
		/* Pull an entry off the packet */
		pullentry(&entry,bpp);
		len -= RIPROUTE;

		if(entry.addr_fam != RIP_IPFAM) {
			/* Skip non-IP addresses */
			continue;
		}
		_tprintf(ifp, _("%-16s%-3u "),inet_ntoa(entry.target),entry.metric);
		if((++i % 3) == 0){
			tputc('\n',ifp);
		}
	}
	if((i % 3) != 0)
		tputc('\n',ifp);
}
