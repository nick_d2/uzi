/* Tracing routines for KISS TNC
 * Copyright 1991 Phil Karn, KA9Q
 */
#include "nos/global.h"
#include "main/mbuf.h"
#include "ax25/kiss.h"
#include "main/devparam.h"
#include "ax25/ax25.h"
#include "dump/trace.h"
#include <libintl.h>
#include "po/messages.h"

void
ki_dump(ifp,bpp,check)
struct iface_s *ifp;
struct mbuf_s **bpp;
int check;
{
	int type;
	int val;

	_tprintf(ifp, _("KISS: "));
	type = PULLCHAR(bpp);
	if(type == PARAM_DATA){
		_tprintf(ifp, _("Data\n"));
		ax25_dump(fp,bpp,check);
		return;
	}
	val = PULLCHAR(bpp);
	switch(type){
	case PARAM_TXDELAY:
		_tprintf(ifp, _("TX Delay: %lu ms\n"),val * 10L);
		break;
	case PARAM_PERSIST:
		_tprintf(ifp, _("Persistence: %u/256\n"),val + 1);
		break;
	case PARAM_SLOTTIME:
		_tprintf(ifp, _("Slot time: %lu ms\n"),val * 10L);
		break;
	case PARAM_TXTAIL:
		_tprintf(ifp, _("TX Tail time: %lu ms\n"),val * 10L);
		break;
	case PARAM_FULLDUP:
		_tprintf(ifp, _("Duplex: %s\n"),val == 0 ? "Half" : "Full");
		break;
	case PARAM_HW:
		_tprintf(ifp, _("Hardware %u\n"),val);
		break;
	case PARAM_RETURN:
		_tprintf(ifp, _("RETURN\n"));
		break;
	default:
		_tprintf(ifp, _("code %u arg %u\n"),type,val);
		break;
	}
}

int
ki_forus(iface,bp)
struct iface_s *iface;
struct mbuf_s *bp;
{
	struct mbuf_s *bpp;
	int i;

	if(bp->data[0] != PARAM_DATA)
		return 0;
	dup_p(&bpp,bp,1,AXALEN);
	i = ax_forus(iface,bpp);
	free_p(&bpp);
	return i;
}
