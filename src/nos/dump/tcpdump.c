/* TCP header tracing routines
 * Copyright 1991 Phil Karn, KA9Q
 */
#include <stdio.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include "internet/netuser.h"
#include <libintl.h> /*RPB*/
#include "internet/internet.h"
#include "internet/tcp.h"
#include "internet/ip.h"
#include "dump/trace.h"
#include "po/messages.h"

/* TCP segment header flags */
static char *Tcpflags[] = {
	"FIN",	/* 0x01 */
	"SYN",	/* 0x02 */
	"RST",	/* 0x04 */
	"PSH",	/* 0x08 */
	"ACK",	/* 0x10 */
	"URG",	/* 0x20 */
	"CE"	/* 0x40 */
};

/* Dump a TCP segment header. Assumed to be in network byte order */
void
tcp_dump(ifp,bpp,source,dest,check)
struct iface_s *ifp;
struct mbuf_s **bpp;
int32 source,dest;	/* IP source and dest addresses */
int check;		/* 0 if checksum test is to be bypassed */
{
	struct tcp seg;
	struct pseudo_header ph;
	uint16 csum;
	uint16 dlen;

	if(bpp == NULL || *bpp == NULL)
		return;

	/* Verify checksum */
	ph.source = source;
	ph.dest = dest;
	ph.protocol = TCP_PTCL;
	ph.length = len_p(*bpp);
	csum = cksum(&ph,*bpp,ph.length);

	ntohtcp(&seg,bpp);

	_tprintf(ifp, _("TCP: %u->%u Seq 0x%lx"),seg.source,seg.dest,seg.seq,seg.ack);
	if(seg.flags.ack)
		_tprintf(ifp, _(" Ack 0x%lx"),seg.ack);
	if(seg.flags.congest)
		_tprintf(ifp, _(" %s"),Tcpflags[6]);
	if(seg.flags.urg)
		_tprintf(ifp, _(" %s"),Tcpflags[5]);
	if(seg.flags.ack)
		_tprintf(ifp, _(" %s"),Tcpflags[4]);
	if(seg.flags.psh)
		_tprintf(ifp, _(" %s"),Tcpflags[3]);
	if(seg.flags.rst)
		_tprintf(ifp, _(" %s"),Tcpflags[2]);
	if(seg.flags.syn)
		_tprintf(ifp, _(" %s"),Tcpflags[1]);
	if(seg.flags.fin)
		_tprintf(ifp, _(" %s"),Tcpflags[0]);

	_tprintf(ifp, _(" Wnd %u"),seg.wnd);
	if(seg.flags.urg)
		_tprintf(ifp, _(" UP 0x%x"),seg.up);
	/* Print options, if any */
	if(seg.flags.mss)
		_tprintf(ifp, _(" MSS %u"),seg.mss);
	if(seg.flags.wscale)
		_tprintf(ifp, _(" WSCALE %u"),seg.wsopt);
	if(seg.flags.tstamp)
		_tprintf(ifp, _(" TSTAMP %lu TSECHO %lu"),seg.tsval,seg.tsecr);
	if((dlen = len_p(*bpp)) != 0)
		_tprintf(ifp, _(" Data %u"),dlen);
	if(check && csum != 0)
		_tprintf(ifp, _(" CHECKSUM ERROR (%u)"),csum);
	tputc('\n',ifp);
}

