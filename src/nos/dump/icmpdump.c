/* ICMP header tracing
 * Copyright 1991 Phil Karn, KA9Q
 * Apr 06	RPB     Changes needed for intl' of strings
 */
#include <stdio.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include <libintl.h> /*RPB*/
#include "internet/internet.h"
#include "internet/netuser.h"
#include "internet/icmp.h"
#include "dump/trace.h"
#include "main/smsg.h"
#include "internet/ip.h"
#include "po/messages.h"

/* Dump an ICMP header */
void
icmp_dump(ifp,bpp,source,dest,check)
struct iface_s *ifp;
struct mbuf_s **bpp;
int32 source,dest;
int check;		/* If 0, bypass checksum verify */
{
	struct icmp icmp;
	uint16 csum;
	char * duptext; /*RPB*/

	if(bpp == NULL || *bpp == NULL)
		return;
	csum = cksum(NULL,*bpp,len_p(*bpp));

	ntohicmp(&icmp,bpp);

	duptext = smsg(Icmptypes,ICMP_TYPES,icmp.type); /*RPB*/
	_tprintf(ifp, _("ICMP: type %s"), duptext); /*RPB*/
	free(duptext); /*RPB*/

	switch(icmp.type){
	case ICMP_DEST_UNREACH:
		duptext = smsg(Unreach,NUNREACH,icmp.code); /*RPB*/
		_tprintf(ifp, _(" code %s"), duptext); /*RPB*/
		free(duptext); /*RPB*/
		break;
	case ICMP_REDIRECT:
		duptext = smsg(Redirect,NREDIRECT,icmp.code); /*RPB*/
		_tprintf(ifp, _(" code %s"), duptext); /*RPB*/
		free(duptext); /*RPB*/
		_tprintf(ifp, _(" new gateway %s"),inet_ntoa(icmp.args.address));
		break;
	case ICMP_TIME_EXCEED:
		duptext = smsg(Exceed,NEXCEED,icmp.code); /*RPB*/
		_tprintf(ifp, _(" code %s"), duptext); /*RPB*/
		free(duptext); /*RPB*/
		break;
	case ICMP_PARAM_PROB:
		_tprintf(ifp, _(" pointer %u"),icmp.args.pointer);
		break;
	case ICMP_ECHO:
	case ICMP_ECHO_REPLY:
	case ICMP_INFO_RQST:
	case ICMP_INFO_REPLY:
	case ICMP_TIMESTAMP:
	case ICMP_TIME_REPLY:
		_tprintf(ifp, _(" id %u seq %u"),icmp.args.echo.id,icmp.args.echo.seq);
		break;
	case ICMP_IPSP:
		duptext = smsg(Said_icmp,NIPSP,icmp.code); /*RPB*/
		_tprintf(ifp, _(" %s"), duptext); /*RPB*/
		free(duptext); /*RPB*/
		break;
	}
	if(check && csum != 0){
		_tprintf(ifp, _(" CHECKSUM ERROR (%u)"),csum);
	}
	tputc('\n',ifp);
	/* Dump the offending IP header, if any */
	switch(icmp.type){
	case ICMP_DEST_UNREACH:
	case ICMP_TIME_EXCEED:
	case ICMP_PARAM_PROB:
	case ICMP_QUENCH:
	case ICMP_REDIRECT:
	case ICMP_IPSP:
		_tprintf(ifp, _("Returned "));
		ip_dump(ifp,bpp,0);
	}
}

