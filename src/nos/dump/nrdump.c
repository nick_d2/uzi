/* NET/ROM header tracing routines
 * Copyright 1991 Phil Karn, KA9Q
 */
#include <stdio.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include "netrom/netrom.h"
#include "netrom/nr4.h"
#include "dump/trace.h"
#include <libintl.h>
#include "po/messages.h"

/* Display NET/ROM network and transport headers */
void
netrom_dump(ifp,bpp,check)
struct iface_s *ifp;
struct mbuf_s **bpp;
int check;
{
	uint8 src[AXALEN],dest[AXALEN];
	char tmp[AXBUF];
	uint8 thdr[NR4MINHDR];
	register i;

	if(bpp == NULL || *bpp == NULL)
		return;
	/* See if it is a routing broadcast */
	if((*(*bpp)->data) == NR3NODESIG) {
		(void)PULLCHAR(bpp);		/* Signature */
		pullup(bpp,tmp,ALEN);
		tmp[ALEN] = '\0';
		_tprintf(ifp, _("NET/ROM Routing: %s\n"),tmp);
		for(i = 0;i < NRDESTPERPACK;i++) {
			if (pullup(bpp,src,AXALEN) < AXALEN)
				break;
			_tprintf(ifp, _("        %12s"),pax25(tmp,src));
			pullup(bpp,tmp,ALEN);
			tmp[ALEN] = '\0';
			_tprintf(ifp, _("%8s"),tmp);
			pullup(bpp,src,AXALEN);
			_tprintf(ifp, _("    %12s"),pax25(tmp,src));
			tmp[0] = PULLCHAR(bpp);
			_tprintf(ifp, _("    %3u\n"),(tmp[0]));
		}
		return;
	}
	/* Decode network layer */
	pullup(bpp,src,AXALEN);
	_tprintf(ifp, _("NET/ROM: %s"),pax25(tmp,src));

	pullup(bpp,dest,AXALEN);
	_tprintf(ifp, _("->%s"),pax25(tmp,dest));

	i = PULLCHAR(bpp);
	_tprintf(ifp, _(" ttl %d\n"),i);

	/* Read first five bytes of "transport" header */
	pullup(bpp,thdr,NR4MINHDR);
	switch(thdr[4] & NR4OPCODE){
 	case NR4OPPID:	/* network PID extension */
		if (thdr[0] == NRPROTO_IP && thdr[1] == NRPROTO_IP) {
 			ip_dump(fp,bpp,check) ;
			return;
		}
 		else
 			_tprintf(ifp, _("         protocol family %x, proto %x"),
			 thdr[0], thdr[1]) ;
 		break ;
	case NR4OPCONRQ:	/* Connect request */
		_tprintf(ifp, _("         conn rqst: ckt %d/%d"),(thdr[0]),(thdr[1]));
		i = PULLCHAR(bpp);
		_tprintf(ifp, _(" wnd %d"),i);
		pullup(bpp,src,AXALEN);
		_tprintf(ifp, _(" %s"),pax25(tmp,src));
		pullup(bpp,dest,AXALEN);
		_tprintf(ifp, _("@%s"),pax25(tmp,dest));
		break;
	case NR4OPCONAK:	/* Connect acknowledgement */
		_tprintf(ifp, _("         conn ack: ur ckt %d/%d my ckt %d/%d"),
		 thdr[0], thdr[1], thdr[2],thdr[3]);
		i = PULLCHAR(bpp);
		_tprintf(ifp, _(" wnd %d"),i);
		break;
	case NR4OPDISRQ:	/* Disconnect request */
		_tprintf(ifp, _("         disc: ckt %d/%d"),
		 thdr[0],thdr[1]);
		break;
	case NR4OPDISAK:	/* Disconnect acknowledgement */
		_tprintf(ifp, _("         disc ack: ckt %d/%d"),
		 thdr[0],thdr[1]);
		break;
	case NR4OPINFO:	/* Information (data) */
		_tprintf(ifp, _("         info: ckt %d/%d"),
		 thdr[0],thdr[1]);
		_tprintf(ifp, _(" txseq %d rxseq %d"),
		 thdr[2],thdr[3]);
		break;
	case NR4OPACK:	/* Information acknowledgement */
		_tprintf(ifp, _("         info ack: ckt %d/%d"),
		 thdr[0],thdr[1]);
		_tprintf(ifp, _(" txseq %d rxseq %d"),thdr[2],thdr[3]);
		break;
	default:
 		_tprintf(ifp, _("         unknown transport type %d"),
		 thdr[4] & 0x0f) ;
		break;
	}
	if(thdr[4] & NR4CHOKE)
		_tprintf(ifp, _(" CHOKE"));
	if(thdr[4] & NR4NAK)
		_tprintf(ifp, _(" NAK"));
	if(thdr[4] & NR4MORE)
		_tprintf(ifp, _(" MORE"));
	tputc('\n',ifp);
}

