/* Packet tracing - top level and generic routines, including hex/ascii
 * Copyright 1991 Phil Karn, KA9Q
 *
 * Apr 04	RPB	 Changes for intl' of strings
 */
#include <stdio.h>
#include <ctype.h>
#include <time.h>
#include "nos/global.h"
#include <stdarg.h>
#include "main/mbuf.h"
#include "main/iface.h"
/*#include "x86/pktdrvr.h"*/
#include "main/commands.h"
#include "dump/trace.h"
#ifdef JOB_CONTROL
#include "kernel/session.h"
#endif
#include "z80/asci.h"
#undef isprint
#define isprint(c) ((signed char)c >= 0x20)
#include "kernel/dprintf.h"
#include <libintl.h>
#include "po/messages.h"

static void ascii_dump(struct iface_s *ifp, struct mbuf_s **bpp);
static void ctohex(char *buf, uint16 c);
static void fmtline(struct iface_s *ifp, uint16 addr, uint8 *buf, uint16 len);
static void hex_dump(struct iface_s *ifp, struct mbuf_s **bpp);
static void showtrace(struct iface_s *ifp);

/* Redefined here so that programs calling dump in the library won't pull
 * in the rest of the package
 */
static _char *nospace = N_("No space!!\n");

static struct tracecmd Tracecmd[] = {
/*RPB*/
	N_("input"),		IF_TRACE_IN,	IF_TRACE_IN,
	N_("-input"),		0,		IF_TRACE_IN,
	N_("output"),		IF_TRACE_OUT,	IF_TRACE_OUT,
	N_("-output"),		0,		IF_TRACE_OUT,
	N_("broadcast"),	0,		IF_TRACE_NOBC,
	N_("-broadcast"),	IF_TRACE_NOBC,	IF_TRACE_NOBC,
	N_("raw"),		IF_TRACE_RAW,	IF_TRACE_RAW,
	N_("-raw"),		0,		IF_TRACE_RAW,
	N_("ascii"),		IF_TRACE_ASCII,	IF_TRACE_ASCII|IF_TRACE_HEX,
	N_("-ascii"),		0,		IF_TRACE_ASCII|IF_TRACE_HEX,
	N_("hex"),		IF_TRACE_HEX,	IF_TRACE_ASCII|IF_TRACE_HEX,
	N_("-hex"),		IF_TRACE_ASCII,	IF_TRACE_ASCII|IF_TRACE_HEX,
	N_("off"),		0,		0xffff,
/*RPB*/
	NULL,	0,		0
};

void
dump(
struct iface_s *ifp,
int direction,
struct mbuf_s *bp
)
	{
	struct mbuf_s *tbp;
	uint16 size;
	time_t timer;
	char *cp;
	struct iftype *ift;

	if (ifp == NULL || (ifp->trace & direction) == 0)
		{
		return;	/* Nothing to trace */
		}

	ift = ifp->iftype;
	switch (direction)
		{
	case IF_TRACE_IN:
		if ((ifp->trace & IF_TRACE_NOBC)
		 && ift != NULL
		 && (ift->addrtest != NULL)
		 && (*ift->addrtest)(ifp, bp) == 0)
			{
			return;		/* broadcasts are suppressed */
			}
		time(&timer);
		cp = ctime(&timer);
		cp[24] = '\0';
		_tprintf(ifp, _("\n%s - %s recv:\n"), cp, ifp->name);
		break;
	case IF_TRACE_OUT:
		time(&timer);
		cp = ctime(&timer);
		cp[24] = '\0';
		_tprintf(ifp, _("\n%s - %s sent:\n"), cp, ifp->name);
		break;
		}

	if (bp == NULL || (size = len_p(bp)) == 0)
		{
		_tprintf(ifp, _("empty packet!!\n"));
		return;
		}

	dup_p(&tbp, bp, 0, size);
	if (tbp == NULL)
		{
		_tprintf(ifp, nospace);
		return;
		}

	if (ift != NULL && ift->trace != NULL)
		(*ift->trace)(ifp, &tbp, 1);

	if (ifp->trace & IF_TRACE_ASCII)
		{
		/* Dump only data portion of packet in ascii */
		ascii_dump(ifp, &tbp);
		}
	else if (ifp->trace & IF_TRACE_HEX)
		{
		/* Dump entire packet in hex/ascii */
		free_p(&tbp);
		dup_p(&tbp, bp, 0, len_p(bp));
		if (tbp != NULL)
			{
			hex_dump(ifp, &tbp);
			}
		else
			_tprintf(ifp, nospace);
		}

	free_p(&tbp);
	}

/* Dump packet bytes, no interpretation */
void
raw_dump(ifp, direction, bp)
struct iface_s *ifp;
int direction;
struct mbuf_s *bp;
{
	struct mbuf_s *tbp;

	_tprintf(ifp, _("\n******* raw packet dump (%s)\n"),
	 ((direction & IF_TRACE_OUT) ? "send" : "recv"));
	dup_p(&tbp, bp, 0, len_p(bp));
	if (tbp != NULL)
		hex_dump(ifp, &tbp);
	else
		_tprintf(ifp, nospace);
	_tprintf(ifp, _("*******\n"));
	free_p(&tbp);
}

/* Dump an mbuf in hex */
static void
hex_dump(ifp, bpp)
struct iface_s *ifp;
register struct mbuf_s **bpp;
{
	uint16 n;
	uint16 address;
	uint8 buf[16];

	if (bpp == NULL || *bpp == NULL)
		return;

	address = 0;
	while ((n = pullup(bpp, buf, sizeof(buf))) != 0){
		fmtline(ifp, address, buf, n);
		address += n;
	}
}
/* Dump an mbuf in ascii */
static void
ascii_dump(ifp, bpp)
struct iface_s *ifp;
register struct mbuf_s **bpp;
{
	int c;
	register uint16 tot;

	if (bpp == NULL || *bpp == NULL)
		return;

	tot = 0;
	while ((c = PULLCHAR(bpp)) != -1){
		if ((tot & 0x3f /*% 64*/) == 0)
			_tprintf(ifp, _("%04x  "), tot);
		tputc(isprint(c) ? c : '.', ifp);
		if ((++tot & 0x3f /*% 64*/) == 0)
			_tprintf(ifp, _("\n"));
	}
	if ((tot & 0x3f /*% 64*/) != 0)
		_tprintf(ifp, _("\n"));
}

/* Print a buffer up to 16 bytes long in formatted hex with ascii
 * translation, e.g.,
 * 0000: 30 31 32 33 34 35 36 37 38 39 3a 3b 3c 3d 3e 3f  0123456789:;<=>?
 */
static void
fmtline(ifp, addr, buf, len)
struct iface_s *ifp;
uint16 addr;
uint8 *buf;
uint16 len;
{
	char line[80];
	char *aptr, *cptr;
	uint8 c;

	memset(line, ' ', sizeof(line));
	ctohex(line, (uint16)hibyte(addr));
	ctohex(line+2, (uint16)lobyte(addr));
	aptr = &line[6];
	cptr = &line[55];
	while (len-- != 0){
		c = *buf++;
		ctohex(aptr, (uint16)c);
		aptr += 3;
		*cptr++ = isprint(c) ? c : '.';
	}
	*cptr++ = '\n';
	twrite(line, 1, (unsigned)(cptr-line), ifp);
}

/* Convert byte to two ascii-hex characters */
static void
ctohex(buf, c)
register char *buf;
register uint16 c;
{
	static char hex[] = "0123456789abcdef";

	*buf++ = hex[hinibble(c)];
	*buf = hex[lonibble(c)];
}

/* Modify or displace interface trace flags */
int
dotrace(argc, argv, p)
int argc;
char *argv[];
void *p;
{
	struct iface_s *ifp;
	struct tracecmd *tp;
#ifdef JOB_CONTROL
	struct session_s *sp;
#endif

	if (argc < 2){
		for (ifp = Ifaces; ifp != NULL; ifp = ifp->next)
			showtrace(ifp);
		return 0;
	}
	if ((ifp = if_lookup(argv[1])) == NULL){
		_printf(_("Interface %s unknown\n"), argv[1]);
		return 1;
	}
	if (argc == 2){
		showtrace(ifp);
		return 0;
	}
	/* MODIFY THIS TO HANDLE MULTIPLE OPTIONS */
	if (argc >= 3){
		for (tp = Tracecmd;tp->name != NULL;tp++)
			if (_strncmp(argv[2], tp->name, strlen(argv[2])) == 0)
				break;
		if (tp->name != NULL)
			ifp->trace = (ifp->trace & ~tp->mask) | tp->val;
		else
			ifp->trace = htoi(argv[2]);
	}
#ifndef ZILOG /* now that trfp doesn't matter (using dprintf for output) */
	if (ifp->trfp != NULL){
		/* Close existing trace file */
		fclose(ifp->trfp);
		ifp->trfp = NULL;
	}
	if (argc >= 4){
		if ((ifp->trfp = fopen(argv[3], APPEND_TEXT)) == NULL){
			_printf(_("Can't write to %s\n"), argv[3]);
		}
	} else
	if (ifp->trace != 0){
#ifdef JOB_CONTROL
		/* Create trace session */
		sp = newsession(Cmdline, ITRACE, 1);
		sp->cb.p = NULL;
		sp->proc = sp->proc1 = sp->proc2 = NULL;
#endif
		ifp->trfp = stdout;
		showtrace(ifp);
		getchar();	/* Wait for the user to hit something */
		ifp->trace = 0;
		ifp->trfp = NULL;
#ifdef JOB_CONTROL
		freesession(sp, 0); /* doesn't keywait(NULL, 1); beforehand */
#endif
	}
#endif
	return 0;
}

/* Display the trace flags for a particular interface */
static void
showtrace(ifp)
register struct iface_s *ifp;
	{
	char *cp;

	if (ifp == NULL)
		{
		return;
		}

	_printf(_("%s:"), ifp->name);
	if (ifp->trace & (IF_TRACE_IN | IF_TRACE_OUT | IF_TRACE_RAW))
		{
		if (ifp->trace & IF_TRACE_IN)
			{
			_printf(_(" input"));
			}
		if (ifp->trace & IF_TRACE_OUT)
			{
			_printf(_(" output"));
			}

		if (ifp->trace & IF_TRACE_NOBC)
			{
			_printf(_(" - no broadcasts"));
			}

		if (ifp->trace & IF_TRACE_HEX)
			{
			_printf(_(" (Hex/ASCII dump)"));
			}
		else if (ifp->trace & IF_TRACE_ASCII)
			{
			_printf(_(" (ASCII dump)"));
			}
		else
			{
			_printf(_(" (headers only)"));
			}

		if (ifp->trace & IF_TRACE_RAW)
			{
			_printf(_(" Raw output"));
			}

#ifndef ZILOG /* haven't got fpname() with CRT stdio */
		if (ifp->trfp && (cp = fpname(ifp->trfp)) != NULL)
			{
			_printf(_(" trace file: %s"), cp);
			}
#endif

		_printf(_("\n"));
		}
	else
		{
		_printf(_(" tracing off\n"));
		}
	}

/* shut down all trace files */
void
shuttrace()
	{
	struct iface_s *ifp;

	for (ifp = Ifaces; ifp != NULL; ifp = ifp->next)
		{
		fclose(ifp->trfp);
		ifp->trfp = NULL;
		}
	}

/* Log messages of the form
 * Tue Jan 31 00:00:00 1987 44.64.0.7:1003 open FTP
 * RPB: intl version
 */
void
_trace_log(struct iface_s *ifp, _char *fmt, ...)
	{
	va_list ap;
	char *cp;
	time_t t;

	time(&t);
	cp = ctime(&t);
	rip(cp);
	_tprintf(ifp, _("%s - "), cp);
	va_start(ap, fmt);
	_vtprintf(ifp, fmt, ap);
	va_end(ap);
	_tprintf(ifp, _("\n"));
	}

int
_tprintf(struct iface_s *ifp, _char *fmt, ...) /*RPB*/
	{
	va_list ap;
	int ret;

	va_start(ap, fmt);
	ret = _vtprintf(ifp, fmt, ap);
	va_end(ap);

	return ret;
	}

int
_vtprintf(struct iface_s *ifp, _char *fmt, va_list ap)
	{
#ifdef ZILOG
	return _vdprintf(0, fmt, ap);
#else
	FILE *fp;

	fp = ifp->trfp;
	if (fp == NULL)
		{
		return 0;
		}
	return _vfprintf(fp, fmt, ap);
#endif
	}

int
twrite(void *data, size_t count0, size_t count1, struct iface_s *ifp)
	{
#ifdef ZILOG
	return dputter(data, count0 * count1, NULL);
#else
	FILE *fp;

	fp = ifp->trfp;
	if (fp == NULL)
		{
		return 0;
		}
	return fwrite(data, count0, count1, fp);
#endif
	}

int
tputc(int ch, struct iface_s *ifp)
	{
#ifdef ZILOG
	if (ch == '\n')
		{
		acrlf();
		}
	else
		{
		abyte(ch);
		}
	return ch;
#else
	FILE *fp;

	fp = ifp->trfp;
	if (fp == NULL)
		{
		return 0;
		}
	return putc(ch, fp);
#endif
	}

