/*
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 Implementation of system calls
**********************************************************/

#include <errno.h>
#include <string.h>
#include <sys/stat.h> /* for S_IOEXEC and others */
#include "kernel/dprintf.h"
#include "filesys/execve.h"
#include "filesys/filename.h" /* for namei() */
#include "filesys/filesys.h" /* for struct file_system_s, also "filesys/cinode.h" struct core_inode_s */
#include "kernel/process.h"
#include "kernel/thread.h" /* for chname() */
#include "kernel/arena.h"
#include "kernel/unix.h" /* for unix_run() */
#include "filesys/maxmin.h"
#include "kernel/usrmem.h"
#include "filesys/xip.h"
#include <libintl.h>
#include "po/messages.h"

#ifndef MODULE
#define MODULE_fs_execve
#define MODULE_wargs
#define MODULE_rargs
#endif

/* Implementation of system calls */

#ifdef MODULE_fs_execve
void
fs_execve(jmp_buf error, unsigned char *path, unsigned char **argv,
		unsigned char **envp)
	{
	jmp_buf try_b;
	unsigned int catch;
	int argc;
	register struct core_inode_s *ino;
	struct block_buffer_s *blkbuf_p;
	blkno_t blk;
	char **nargv, **nenvp;		/* In user space */
	argblk_t *abuf, *ebuf;
	int (**sigp)();
	char *progptr;			/* Original UZI */
	unsigned int entry, count, remain, offset;
	struct file_system_s *dev;
	dev_t devno;
	blkno_t i, pos, *region, regions, blocks;
	struct process_s *process_p;
	unsigned char size;
	struct arena_s *arena_p;
	unsigned char *name;
	unsigned char save_sysio;
#if 1
	struct object_s *object_p;
#endif

	save_sysio = my_thread_p->sysio;
	if (save_sysio)
		{
		progptr = NULL;
		name = path;
		}
	else
		{
		progptr = malloc(BUFSIZE);
		if (progptr == NULL)
			{
			longjmp(error, ENOMEM);
			}
		name = progptr;
#if 0
		if (ugets(path, name, BUFSIZE))
			{
			free(progptr);
			longjmp(error, ENAMETOOLONG);
			}
#else
		ugets(path, name, BUFSIZE);
#endif
		}
	chname(my_thread_p, name); /* for pretty ps display */

	my_thread_p->sysio = 1; /* pretend call is from kernel */

	catch = setjmp(try_b);
	if (catch)
		{
		my_thread_p->sysio = save_sysio;
		free(progptr); /* may be NULL */
		longjmp(error, catch);
		}

	ino = namei(try_b, name, NULL, 1);

	my_thread_p->sysio = save_sysio;
	free(progptr); /* may be NULL */

	if ((getperm(ino) & S_IOEXEC) == 0 ||
			(ino->c_node.i_mode &
				(S_IEXEC | S_IOEXEC | S_IGEXEC)) == 0)
		{
		i_deref(ino);
		longjmp(error, EACCES);
		}

	i_acquire(ino);

	catch = setjmp(try_b);
	if (catch)
		{
		goto error1;
		}

	if (S_ISREGALIGN(ino->c_node.i_mode) == 0)
		{
		longjmp(try_b, ENOREGALIGN);
		}

/* abyte('a'); */

	i_release(ino);
	setftim (ino, A_TIME);
	i_acquire(ino);
/* abyte('b'); */

	/* Read in the first block of the new program */
	blkbuf_p = bread(try_b, ino->c_dev, bmap(try_b, ino, 0, 1), 0);
/* abyte('c'); */

	region = NULL;
	abuf = NULL;
	ebuf = NULL;

	catch = setjmp(try_b);
	if (catch)
		{
		free(ebuf); /* may be NULL */
		free(abuf); /* may be NULL */
		free(region); /* may be NULL */
		if (setjmp(try_b) == 0)
			{
			bfree(try_b, blkbuf_p, 0);
			}
	error1:
		i_release(ino);
		i_deref(ino);
		longjmp(error, catch);
		}

	switch (((exehdr_t *)blkbuf_p->bf_data)->e_magic)
		{
	case E_MAGIC:
		break;
	case 0x2123: /* '#!' */
		longjmp(try_b, ESHELL);
	default:
		longjmp(try_b, ENOEXEC);
		}

	entry = ((exehdr_t *)blkbuf_p->bf_data)->e_format;
	switch (entry) /* temporary multiple use of variable */
		{
	case E_FORMAT_LARGE:
		break;
	case E_FORMAT_BANKED:
		/* prepare to access the given inode */
		devno = ino->c_dev;
		dev = getfs(devno);

		/* construct region list using shared subroutine */
		region = xip_examine(try_b, dev, ino, ino->c_node.i_size,
				&regions, &blocks);
		break;
	default:
		longjmp(try_b, ENOEXEC);
		}

	/* check the file's reported length against header */
	/* note: holes in the executable will cause a crash!! */
	if (((exehdr_t *)blkbuf_p->bf_data)->e_size > ino->c_node.i_size)
		{
		longjmp(try_b, ETOOSHORT);
		}

	/* can't skip more than one block for header */
	if (((exehdr_t *)blkbuf_p->bf_data)->e_hsize >= BUFSIZE)
		{
		longjmp(try_b, ENOEXEC);
		}

#if 0 /* Nick has temporarily removed CP/M support */
		   /* Set Flag to indicate type of Executable (UZI-CP/M) */
	if ((blkbuf_p->bf_data[3]=='U') && (blkbuf_p->bf_data[4]=='Z') && (blkbuf_p->bf_data[5]=='I'))
		uzicom = 1;
	else
		uzicom = 0;
#endif
/* abyte('d'); */

	/* Gather the arguments, and put them in temporary buffers. */
	/* Put environment in another buffer. */
	abuf = (argblk_t *)malloc(BUFSIZE);
	if (abuf == NULL)
		{
		longjmp(try_b, ENOMEM);
		}
	ebuf = (argblk_t *)malloc(BUFSIZE);
	if (ebuf == NULL)
		{
		longjmp(try_b, ENOMEM);
		}
/* abyte('e'); */

	if (wargs(argv, abuf) == 0 || wargs(envp, ebuf) == 0)
		{
		longjmp(try_b, E2BIG);
		}
/* abyte('f'); */

	/* cache this thread's process as we'll need it a few times */
	process_p = my_thread_p->process_p;

	/* calculate how many 4 kbyte pages needed, excluding basepage */
	size = ((abuf->a_argc + 1 + ebuf->a_argc + 1) * sizeof(char *) +
			abuf->a_arglen + ebuf->a_arglen +
			((exehdr_t *)blkbuf_p->bf_data)->e_break - 1) >>
			PAGE_LOG;

	/* resize the program's arena, if we are the only client and */
	/* space happens to be available, or detach and recreate it */
	/* (must be done like this, in case arena low or fragmented, */
	/* we also need to be careful we can return in case of error) */
	arena_p = process_p->arena_p;
/* ahexw((unsigned int)my_thread_p->process_p->arena_p->size); */
	if (arena_p->refs > 1 || arena_resize(arena_p, size) == 0)
		{
		arena_p = arena_allocate(size);
		if (arena_p == NULL)
			{
			longjmp(try_b, ENOMEM);
			}
		arena_deref(process_p->arena_p);
		process_p->arena_p = arena_p;
		}
/* ahexw((unsigned int)my_thread_p->process_p->arena_p->size); */

#if 1
	for (i = 0; i < USER_STREAM_LIST; i++)
		{
		object_p = process_p->stream_p[i];
		if (object_p)
			{
			process_fclose(process_p, object_p);
			}
		}
#else
	process_fclose(process_p, process_p->stream_p[0]); /* stdin */
	process_fclose(process_p, process_p->stream_p[1]); /* stdout */
	process_fclose(process_p, process_p->stream_p[2]); /* stderr */
#endif

	/* if the call came from within kernel, become user now */
	/* (because we use uput calls to copy executable to user) */
	my_thread_p->sysio = 0;

	/* Check setuid stuff here. No other changes needed in user data */
	if (ino->c_node.i_mode & S_ISUID)
		{
		process_p->p_euid = ino->c_node.i_uid;
		}

	if (ino->c_node.i_mode & S_ISGID)
		{
		process_p->p_egid = ino->c_node.i_gid;
		}

	/* We are definitely going to succeed with the exec,
	 * so we can start writing over the old program
	 */

	/* get break for program (not including argument data) */
	nargv = (char **)((exehdr_t *)blkbuf_p->bf_data)->e_break;

	/* read back arguments, determine position of environment */
	nenvp = (char **)rargs((char *)nargv, abuf, &argc);
	free(abuf);
	/* abuf = NULL; but don't bother, we can't fail now */

	/* read back environment, determine initial break address */
/* abyte('A'); */
	process_p->p_break = (unsigned char *)rargs((char *)nenvp, ebuf, NULL);

	/* zero out the program's udata and stack segments */
/* abyte('B'); */
	progptr = (char *)((exehdr_t *)blkbuf_p->bf_data)->e_udata;
/* abyte('C'); */
	remain = (unsigned int)nargv - ((exehdr_t *)blkbuf_p->bf_data)->e_udata;

/* abyte('D'); */
	memset(ebuf, 0, BUFSIZE); /* optimised for size, not speed */
/* abyte('E'); */
	uput(ebuf, (void *)USER_TABLE, 0x100); /* zero virtual memory table */

	while (remain)
		{
/* abyte('F'); */
		count = min_uint(remain, BUFSIZE);
		uput(ebuf, progptr, count);

		progptr += count;
		remain -= count;
		}

/* abyte('G'); */
	free(ebuf);
	/* ebuf = NULL; but don't bother, we can't fail now */

	/* if executable is banked, map it into virtual memory space */
/* abyte('H'); */
	if (entry == E_FORMAT_BANKED) /* entry = e_format field from header */
		{
		/* find initial location of program, in virtual memory */
		progptr = (char *)USER_TABLE;

		for (i = 0; i < regions; i++)
			{
			/* convert to page number, put in virt memory table */
#if 0 /* when we eventually change regions from 16 kbytes to 4 kbytes */
			uputc(0x40 +
				(region[i] >> (PAGE_LOG-BUFSIZELOG)) - 0xf,
				progptr++);
#else
 for (size = 0; size < 4; size++)
  {
  uputc(0x40 + (region[i] >> (PAGE_LOG-BUFSIZELOG)) + size - 0xf, progptr++);
  }
#endif
			}

		free(region);
		/* region = NULL; but don't bother, we can't fail now */
		}

	/* find out program's entry address, while blkbuf_p is still valid */
	entry = ((exehdr_t *)blkbuf_p->bf_data)->e_entry;

	/* Read in the rest of the program */
	/* we have already checked that offset is < BUFSIZE */
	progptr = (char *)((exehdr_t *)blkbuf_p->bf_data)->e_idata;
	offset = ((exehdr_t *)blkbuf_p->bf_data)->e_hsize;
	remain = ((exehdr_t *)blkbuf_p->bf_data)->e_udata - (unsigned int)progptr;
/* abyte('a'); */
/* ahexw(remain); */
/* acrlf(); */

	if (remain) /* first block is treated specially */
		{
		count = min_uint(remain, BUFSIZE - offset);
		uput(blkbuf_p->bf_data + offset, progptr, count);

		progptr += count;
		remain -= count;
/* abyte('b'); */
/* ahexw(remain); */
/* acrlf(); */
		}
	catch = setjmp(try_b);
	if (catch)
		{
		_panic(_("fs_execve() error %u"), catch);
		}
	bfree(try_b, blkbuf_p, 0);

	blk = 1;
	while (remain)
		{
		blkbuf_p = bread(try_b, ino->c_dev,
				bmap(try_b, ino, blk++, 1), 0);

		count = min_uint(remain, BUFSIZE);
		uput(blkbuf_p->bf_data, progptr, count);

		progptr += count;
		remain -= count;
/* abyte('c'); */
/* ahexw(remain); */
/* acrlf(); */

		bfree(try_b, blkbuf_p, 0);
		}
/* abyte('d'); */
/* ahexw(remain); */
/* acrlf(); */
/* abyte('j'); */

	i_release(ino);
	i_deref(ino);

	/* Turn off caught signals */
	for (sigp = process_p->p_sigvec; sigp < (process_p->p_sigvec + NSIGS); sigp++)
		{
		if (((long)*sigp) != SIG_IGN)
			{
			*sigp = (void (*)(signal_t))SIG_DFL;
			}
		}

	/* Jump into the program, first setting the stack
	 */

	/* push the arguments to main(), just below the argument data */
	uputw((unsigned int)nenvp, nargv - 1);
	uputw((unsigned int)nargv, nargv - 2);
	uputw(argc, nargv - 3);

	/* push address of main() as a return address (don't push bank) */
	uputw(entry, nargv - 4);
	my_thread_p->c_sp = (unsigned int)(nargv - 4);
	my_thread_p->c_cs = (unsigned int)USER_TABLE; /* arbitrary */

	/* calculate MMU settings, to be loaded by the unix_run() stub */
	my_thread_p->bbr_value =
			my_thread_p->process_p->arena_p->page - 1;
	my_thread_p->cbar_value =
			(my_thread_p->process_p->arena_p->size << 4) + 0x11;

/* abyte('m'); */
	unix_run();
	}
#endif

/* SN    TODO      max (1024) 512 bytes for argv
               and max  512 bytes for environ
*/


#ifdef MODULE_wargs
int
wargs(char **argv, argblk_t *argbuf)       /* argv in user space */
{
    register char *ptr;    /* Address of base of arg strings in user space */
    register unsigned char c; /* int      c; */
    register char *bufp;
    /* Nick unsigned ugetw(); due to IAR stricter checking */
    /* Nick char     ugetc(); as it's now implemented as a macro */
/* dprintf(0, "wargs 0x%x\n", argv); */

    argbuf->a_argc = 0;              /* Store argc in argbuf */
    bufp = argbuf->a_buf;

    while (ptr = (char *)ugetw (argv++))
    {
/* dprintf(0, "0x%x\n", ptr); */
        ++(argbuf->a_argc);          /* Store argc in argbuf. */
        do
        {
            *bufp++ = c = ugetc (ptr++);
/* if (c) */
/*  { */
/*  abyte(c); */
/*  } */
/* else */
/*  { */
/*  acrlf(); */
/*  } */
            if (bufp > argbuf->a_buf+500)
            {
                return 0;
            }
        }
        while (c);
    }
    argbuf->a_arglen = bufp - argbuf->a_buf;    /* Store total string size. */
    return 1;
}
#endif


#ifdef MODULE_rargs
char *
rargs(char *ptr, argblk_t *argbuf, int *cnt)
{
    int  argc;
    char **argv;         /* Address of users argv[], just below ptr */
    char *sptr;

    sptr = argbuf->a_buf;

    /* Move them into the users address space, from the bottom */
    argc = argbuf->a_argc;
    argv = (char **)ptr;
    ptr += (argc + 1) * sizeof(char *);

/* abyte('a'); */
/* ahexw((int)sptr); */
/* abyte(' '); */
/* ahexw((int)ptr); */
/* abyte(' '); */
/* ahexw((int)argbuf->a_arglen); */
    if (argbuf->a_arglen)
        uput (sptr, ptr, argbuf->a_arglen);
/* abyte('b'); */

    if (cnt)
/* { */
/* _dprintf(0, _("(argc = %d)\n"), argc); */
        *cnt = argc;
/* } */

    /* Set each element of argv[] to point to its argument string */
    while (argc--)
    {
/* abyte('c'); */
/* ahexw((int)ptr); */
/* abyte(' '); */
/* ahexw((int)argv); */
        uputw((unsigned int)ptr, argv++);
/* abyte('d'); */
        do
            ++ptr;
        while (*sptr++);
    }
/* abyte('e'); */
/* ahexw(0); */
/* abyte(' '); */
/* ahexw((int)argv); */
    /* add Null Pointer to end of array */
    uputw(0, argv);
/* abyte('f'); */
    return ptr;
}
#endif

