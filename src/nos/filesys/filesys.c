/*
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 Filesystem related routines
**********************************************************/

#include <errno.h>
#include <string.h>
#include "driver/device.h" /* for DTHRESH */
#include "kernel/dprintf.h"
#include "kernel/thread.h" /* for my_thread_p->process_p->object_p */
#include "filesys/filename.h" /* for namei() */
#include "filesys/filesys.h" /* also "filesys/cinode.h" struct core_inode_s i_sync() */
#include "kernel/userfile.h" /* for uf_close() */
#include "kernel/usrmem.h" /* for ugetc() */
#include "kernel/valadr.h"
#include "driver/rtc.h" /* for rtc_time() */
#include <libintl.h>
#include "po/messages.h"

#ifndef DEBUG
#define DEBUG 0
#endif

#ifndef MODULE
#define MODULE_Root_dev
#define MODULE_Root_ino
#define MODULE_file_system_list
#define MODULE_Baddevmsg
#define MODULE_fs_init
#define MODULE_fs_end
#define MODULE_findfs
#define MODULE_getfs
#define MODULE_fmount
#define MODULE_fsync
#define MODULE_fs_sync
#define MODULE_fs_utime
#define MODULE_fs_link
#define MODULE_fs_symlink
#define MODULE_fs_unlink
#define MODULE_fs_chdir
#define MODULE_fs_chroot
#define MODULE_fs_mknod
#define MODULE_fs_access
#define MODULE_fs_chmod
#define MODULE_fs_chown
#define MODULE_fs_stat
#define MODULE_fs_mount
#define MODULE_fs_umount
#endif

#ifdef MODULE_Root_dev
dev_t Root_dev;		/* Device number of root filesystem. */
#endif
#ifdef MODULE_Root_ino
struct core_inode_s *Root_ino;	/* Address of root dir in inode table */
#endif

#ifdef MODULE_file_system_list
struct file_system_s *file_system_list[FILE_SYSTEM_LIST];
#endif

/* Common messages. They are here to save memory */
#ifdef MODULE_Baddevmsg
_char *Baddevmsg = N_("%s: bad dev %u");
#endif

/* fs_init() originally had a boolean parameter "waitfordisk" */

#ifdef MODULE_fs_init
void
fs_init(dev_t boot_dev)
	{
	jmp_buf try_b;
	register struct file_system_s **fspp, *fsp;

/* abyte('e'); */
	/* device_init(); */
	/* actually, it's not really needed, so save a bit of space for now */

/* abyte('f'); */
	for (fspp = file_system_list; fspp < file_system_list + FILE_SYSTEM_LIST; fspp++)
		{
		fsp = calloc(sizeof(struct file_system_s), 1);
		if (fsp == NULL)
			{
			_panic(_("fs_init()"));
			}
		*fspp = fsp;
		}

/* abyte('g'); */
	Root_dev = boot_dev;
	if (Root_dev != NULLDEV)
		{
#if 0 /*def _MSX_DOS*/
		if (waitfordisk)
			{
			_printf(_("Insert disk and press RETURN: "));
			while (getchar() != '\n')
				;
			}
#endif

/* Debug_level = 3; */
		/* Mount the root device */
		if (setjmp(try_b))
			{
			_panic(_("no fsys"));
			}
		fmount(try_b, Root_dev, NULL, 0);

		if (setjmp(try_b))
			{
			_panic(_("no root"));
			}
		Root_ino = i_open(try_b, Root_dev, ROOTINODE);

		my_thread_p->process_p->p_cwd = Root_ino;
		my_thread_p->process_p->p_root = Root_ino;

		i_ref(Root_ino);
		}

	my_thread_p->process_p->p_time = rtc_time();
	}
#endif

#ifdef MODULE_fs_end
void
fs_end(void)
	{
	jmp_buf try_b;
	register int j;
	register struct open_file_s *oft_p;

	/* note: this was intended to catch files left open by ucp tasks */
	for (j = 0; j < USER_OBJECT_LIST; j++)
		{
		if (my_thread_p->process_p->object_p[j])
			{
			if (setjmp(try_b) == 0)
				{
				uf_close(try_b, j);
				}
			}
		}

	i_sync(NULLDEV); /* write back all inodes for all devs */
	fsync(NULLDEV); /* write back superblocks for all devs */
	bufsync(NULLDEV, 0); /* write back all buffers for all devs */
	}
#endif

/* Check the given device number, and return its address in the mount table.
 */
#ifdef MODULE_findfs
struct file_system_s *
findfs(dev_t devno)
{
	register struct file_system_s **fspp, *fsp;

	for (fspp = file_system_list; fspp < file_system_list + FILE_SYSTEM_LIST; fspp++)
		{
		fsp = *fspp;
#if 0 /* temp! */
 _dprintf(3, _("fsp 0x%x s_mounted 0x%x s_dev %d\n"), fsp, fsp->s_mounted, fsp->s_dev);
#endif
		if (fsp->s_mounted && fsp->s_dev == devno)
			{
			return fsp;
			}
		}
	return NULL;
}
#endif

/* Check the given device number, and return its address in the mount table.
 * Also time-stamp the superblock of dev, and mark it modified.
 * Used when freeing and allocating blocks and inodes.
 */

#ifdef MODULE_getfs
struct file_system_s *
getfs(dev_t devno)
{
	struct file_system_s *dev;

	dev = findfs(devno);
	if (dev == NULL)
		_panic(Baddevmsg, "getfs", devno);
	dev->s_time = rtc_time();	/* timestamp */
	dev->s_fmod = 1;		/* mark modified */
	return dev;
}
#endif

/* Fmount() places the given device in the mount table with mount point ino
 */

#ifdef MODULE_fmount
void
fmount(jmp_buf error, dev_t dev, struct core_inode_s *ino, unsigned int flag)
{
	jmp_buf try_b;
	unsigned int catch;
	register struct block_buffer_s *blkbuf_p;
	register struct file_system_s **fspp, *fsp;

#if DEBUG >= 2
	_dprintf(2, _("fmount(0x%x, 0x%x, 0x%x, %u) starting\n"),
			error, dev, ino, flag);
#endif
	device_find(error, dev); /* just validate dev */
	if (MAJOR(dev) >= DTHRESH)
		{
#if DEBUG >= 2
		_dprintf(2, _("fmount() error ENOTBLK\n"));
#endif
		longjmp(error, ENOTBLK);
		}

	/* Lookup free slot in mounted fs table */
	for (fspp = file_system_list; fspp < file_system_list + FILE_SYSTEM_LIST; fspp++)
		{
		fsp = *fspp;
		if (fsp->s_mounted && fsp->s_dev == dev)
			{
#if DEBUG >= 2
			_dprintf(2, _("fmount() error EBUSY\n"));
#endif
			longjmp(error, EBUSY);
			}
		}

	for (fspp = file_system_list; fspp < file_system_list + FILE_SYSTEM_LIST; fspp++)
		{
		fsp = *fspp;
		if (fsp->s_mounted == 0)
			{
			goto Ok;
			}
		}

#if DEBUG >= 2
	_dprintf(2, _("fmount() error ENOMEM\n"));
#endif
	longjmp(error, ENOMEM);

Ok:
	/* temporarily lock the slot */
	fsp->s_mounted = ~SMOUNTED;
	fsp->s_dev = dev;

	catch = setjmp(try_b);
	if (catch)
		{
		_dprintf(0, _("fmount() can't open device\n"));
		goto error1;
		}

	device_find(try_b, dev)->open(try_b, MINOR(dev));

#if 0 /* routine to search for the starting position within a FAT/FAT32 fs */
 {
 register int i;
 for (i = SUPERBLOCK; i < 0x400; ++i)
  {
  _dprintf(2, _("reading sector 0x%x\n"), i);
  if (setjmp(try_b) == 0)
   {
   blkbuf_p = bread(dev, i, 0);
   if (*(int *)blkbuf_p->bf_data == SMOUNTED)
    {
    _dprintf(2, _("formatted at sector 0x%x\n"), i - SUPERBLOCK);
    panic("stop");
    }
   bfree(blkbuf_p, 0);
   }
  }
 }
#endif

	/* read in superblock */
	catch = setjmp(try_b);
	if (catch)
		{
		_dprintf(0, _("fmount() can't read superblock\n"));
		goto error2;
		}

	blkbuf_p = bread(try_b, dev, SUPERBLOCK, 0);
	memcpy(fsp, blkbuf_p->bf_data, sizeof(struct file_system_s));
						/* rewrites s_mounted */

	/* fill-in in-core fields */
	fsp->s_ronly = flag;
	fsp->s_dev = dev;
	fsp->s_fmod = 0;

	bfree(try_b, blkbuf_p, 0);

	/* See if there really is a filesystem on the device */
	if (fsp->s_mounted != SMOUNTED || fsp->s_isize >= fsp->s_fsize) {
		_dprintf(0, _("fmount() corrupt superblock\n"));
		catch = ECORRUPTFS;
	error2:
		if (setjmp(try_b) == 0)
			{
			device_find(try_b, dev)->close(try_b, MINOR(dev));
			}
	error1:
		fsp->s_mounted = 0;	/* free fs table entry */
#if DEBUG >= 2
		_dprintf(2, _("fmount() error %u\n"), catch);
#endif
		longjmp(error, catch);
	}

	fsp->s_mntpt = ino; /* NULL if mounting as root filesystem */
	if (ino)
		{
		i_ref(ino); /* if mounting on a directory, lock it */
		}
#if DEBUG >= 2
	_dprintf(2, _("fmount() returning\n"));
#endif
}
#endif

/* fsync() syncing all superblocks
 * NOTE: SHOULD BE MODIFIED SO IT CAN RETURN AN ERRORCODE!!
 */

#ifdef MODULE_fsync
void
fsync(dev_t dev)
	{
	jmp_buf try_b;
	register struct file_system_s **fspp, *fsp;
	register struct block_buffer_s *blkbuf_p;

	/* Write out modified super blocks
	 * old: This fills the rest of the super block with zeros
	 * new: This preserves the remaining bytes of the superblock
	 */
	for (fspp = file_system_list; fspp < file_system_list + FILE_SYSTEM_LIST; fspp++)
		{
		fsp = *fspp;
		if (fsp->s_mounted && fsp->s_fmod &&
				(dev == NULLDEV || dev == fsp->s_dev))
			{
			/* don't zero bitmap data after the struct file_system_s */
			/* SQUELCH EXCEPTIONS FOR THE MOMENT (NOT NICE) */
			if (setjmp(try_b) == 0)
				{
				blkbuf_p = bread(try_b, fsp->s_dev,
						SUPERBLOCK, 0);
				fsp->s_fmod = 0;
				memcpy(blkbuf_p->bf_data, fsp, sizeof(*fsp));
				if (setjmp(try_b))
					{
					fsp->s_fmod = 1;
					}
				else
					{
					bfree(try_b, blkbuf_p, 1);
					}
				}
			}
		}
	}
#endif

#ifdef MODULE_fs_sync
void
fs_sync(jmp_buf error)
	{
	i_sync(NULLDEV); /* write back all inodes for all devs */
	fsync(NULLDEV); /* write back superblocks for all devs */
	bufsync(NULLDEV, 0); /* write back all buffers for all devs */
	}
#endif

#ifdef MODULE_fs_utime
void
fs_utime(jmp_buf error, unsigned char *path, struct utimbuf *buf)
	{
	jmp_buf try_b;
	unsigned int catch;
	struct utimbuf lcl;
	register struct core_inode_s *ino;

	if (buf)
		{
		uget((char *)buf, (char *)&lcl, sizeof(struct utimbuf));
		}
	else
		{
		lcl.actime = rtc_time();
		bcopy(&lcl.actime, &lcl.modtime, sizeof(time_t));
		}

	ino = namei(error, path, NULL, 1);
	if (ino->c_node.i_uid != my_thread_p->process_p->p_euid &&
			super() == 0)
		{
		i_deref(ino);
		longjmp(error, EPERM);
		}

	if (ino->c_ro)
		{
		i_deref(ino);
		longjmp(error, EROFS);
		}

	bcopy(&lcl.actime, &ino->c_node.i_atime, sizeof(time_t));
	bcopy(&lcl.modtime, &ino->c_node.i_mtime, sizeof(time_t));

	ino->c_dirty=1;
	i_deref(ino);
	}
#endif

#ifdef MODULE_fs_link
void
fs_link(jmp_buf error, unsigned char *path, unsigned char *path_new)
	{
	jmp_buf try_b;
	unsigned int catch;
	register struct core_inode_s *ino, *ino2;
	struct core_inode_s *parent2 = NULL;
	char fname[15];

	ino = namei(error, path, NULL, 1);

	catch = setjmp(try_b);
	if (catch)
		{
		goto error1;
		}

	if ((getperm(ino) & S_IOWRITE) == 0)
		{
		longjmp(try_b, EPERM);
		}

	/* Make sure path_new doesn't exist, and get its parent */
	ino2 = namei(try_b, path_new, &parent2, 1);
	if (ino2)
		{
		i_deref(ino2);
		i_deref(parent2);
		longjmp(try_b, EEXIST);
		}

	catch = setjmp(try_b);
	if (catch)
		{
		i_deref(parent2);
		goto error1;
		}

	if (ino->c_dev != parent2->c_dev)
		{
		/* cross-device link not allowed */
		longjmp(try_b, EXDEV);
		}

	filename(path_new, fname);
	ch_link(try_b, parent2, "", fname, ino);
	i_deref(parent2);

	setftim(ino, C_TIME);
	i_acquire(ino);

	catch = setjmp(try_b);
	if (catch)
		{
		i_release(ino);
	error1:
		i_deref(ino);
		longjmp(error, catch);
		}

	/* Update the link count */
	++ino->c_node.i_nlink;
	wr_inode(try_b, ino, 0);
	i_release(ino);
	i_deref(ino);
	}
#endif

#ifdef MODULE_fs_symlink
void
fs_symlink(jmp_buf error, unsigned char *path, unsigned char *path_new)
	{
	jmp_buf try_b;
	unsigned int catch;
	register struct core_inode_s *ino = NULL;
	mode_t mode;
	unsigned char *p;
	size_t count;

	mode = S_IFREG | (0666 & ~my_thread_p->process_p->p_umask);

	/* Create path_new as new IFREG node */
	ino = n_creat(error, path_new, 1, mode);

	/* fill in new file */
	p = path;
	count = 0;
	while (ugetc(p))
		{
		p++;
		count++;
		}

	catch = setjmp(try_b);
	if (catch)
		{
		goto error1;
		}

	/* write it */
	readwritei(try_b, 1, ino, path, count, 0);

	/* change mode to LNK */
	i_acquire(ino);
	ino->c_node.i_mode = (mode & S_UMASK) | S_IFLNK;

	catch = setjmp(try_b);
	if (catch)
		{
		i_release(ino);
	error1:
		i_deref(ino);
		/* in this case should unlink the created directory entry */
		longjmp(error, catch);
		}

	wr_inode(try_b, ino, 0);
	i_release(ino);
	i_deref(ino);
	}
#endif

#ifdef MODULE_fs_unlink
void
fs_unlink(jmp_buf error, unsigned char *path)
	{
	jmp_buf try_b;
	unsigned int catch;
	struct core_inode_s *pino;
	register struct core_inode_s *ino;
	char fname[15];

	ino = namei(error, path, &pino, 0);
	if (ino == NULL)
		{
		i_deref(pino);
		longjmp(error, ENOENT);
		}

	catch = setjmp(try_b);
	if (catch)
		{
		i_deref(ino);
		i_deref(pino);
		longjmp(error, catch);
		}

	if ((getperm(ino) & S_IOWRITE) == 0)
		{
		longjmp(try_b, EPERM);
		}

	/* Remove the directory entry */
	filename(path, fname);
	ch_link(try_b, pino, fname, "", NULL);
	i_deref(pino);

	/* Decrease the link count of the inode */
	i_acquire(ino);
	if (ino->c_node.i_nlink-- == 0)
		{
		ino->c_node.i_nlink = 1;
		_dprintf(0, _("fs_unlink(): bad nlink\n"));
		}
	i_release(ino);

	setftim(ino, C_TIME);
	i_deref(ino);
	}
#endif

#ifdef MODULE_fs_chdir
void
fs_chdir(jmp_buf error, unsigned char *path)
{
	register struct core_inode_s *cinode_p;

	cinode_p = namei(error, path, NULL, 1);
	if (getmode(cinode_p) != S_IFDIR)
		{
		i_deref(cinode_p);
		longjmp(error, ENOTDIR);
		}
	if ((getperm(cinode_p) & S_IOEXEC) == 0)
		{
		i_deref(cinode_p);
		longjmp(error, EPERM);
		}

	i_deref(my_thread_p->process_p->p_cwd);
	my_thread_p->process_p->p_cwd = cinode_p;
	}
#endif

#ifdef MODULE_fs_chroot
void
fs_chroot(jmp_buf error, unsigned char *path)
	{
	fs_chdir(error, path);

	i_deref(my_thread_p->process_p->p_root);
	my_thread_p->process_p->p_root = my_thread_p->process_p->p_cwd;
	i_ref(my_thread_p->process_p->p_root);
	}
#endif

#ifdef MODULE_fs_mknod
void
fs_mknod(jmp_buf error, unsigned char *path, mode_t mode, dev_t dev)
	{
	jmp_buf try_b;
	unsigned int catch;
	register struct core_inode_s *ino;

	mode &= (~my_thread_p->process_p->p_umask & S_UMASK) | S_IFMT;
	if (S_ISDEV(mode) && super() == 0)
		{
		/* only super able to create devs */
		longjmp(error, EPERM);
		}

	ino = n_creat(error, path, 1, mode);

	i_acquire(ino);
	catch = setjmp(try_b);
	if (catch)
		{
		i_release(ino);
		i_deref(ino);
		/* in this case should unlink the created directory entry */
		longjmp(error, catch);
		}

	/* Initialize device node */
	if (isdevice(ino))
		{
		DEVNUM(ino) = dev;
		ino->c_dirty = 1;
		}

	wr_inode(try_b, ino, 0);
	i_release(ino);
	i_deref(ino);
	}
#endif

#ifdef MODULE_fs_access
void
fs_access(jmp_buf error, unsigned char *path, unsigned int mode)
	{
	jmp_buf try_b;
	unsigned int catch;
	struct process_s *process_p;
	unsigned int euid, egid;
	register struct core_inode_s *cinode_p;

	mode &= 7;
	if (mode && ugetc(path) == 0)
		{
		longjmp(error, ENOENT);
		}

	process_p = my_thread_p->process_p;

	/* temporarily make effective id real id */
	euid = process_p->p_euid;
	egid = process_p->p_egid;

	process_p->p_euid = process_p->p_uid;
	process_p->p_egid = process_p->p_gid;

	catch = setjmp(try_b);
	if (catch)
		{
		process_p->p_euid = euid;
		process_p->p_egid = egid;
		longjmp(error, catch);
		}

	cinode_p = namei(try_b, path, NULL, 1);
	if (~getperm(cinode_p) & mode)
		{
		i_deref(cinode_p);
		longjmp(try_b, EPERM);
		}
	i_deref(cinode_p);

	process_p->p_euid = euid;
	process_p->p_egid = egid;
	}
#endif

#ifdef MODULE_fs_chmod
void
fs_chmod(jmp_buf error, unsigned char *path, mode_t mode)
	{
	chany(error, path, (unsigned int)mode, 0, 1);
	}
#endif

#ifdef MODULE_fs_chown
void
fs_chown(jmp_buf error, unsigned char *path, unsigned int owner,
		unsigned int group)
	{
	chany(error, path, owner, group, 0);
	}
#endif

#ifdef MODULE_fs_stat
void
fs_stat(jmp_buf error, unsigned char *path, struct stat *buf)
	{
	struct core_inode_s *cinode_p;

	valadr(error, buf, sizeof(struct stat));
	cinode_p = namei(error, path, NULL, 1);
	i_stat(cinode_p, buf);
	i_deref(cinode_p);
	}
#endif

#ifdef MODULE_fs_mount
void
fs_mount(jmp_buf error, unsigned char *path, unsigned char *path_dir,
		unsigned int flag)
	{
	jmp_buf try_b;
	unsigned int catch;
	register struct core_inode_s *sino, *dino, **dinopp;
	register dev_t dev;
	int sts = -1;
	struct file_system_s *fp;

	if (super() == 0)
		{
		longjmp(error, EPERM);
		}

	sino = namei(error, path, NULL, 1);

	catch = setjmp(try_b);
	if (catch)
		{
		goto error1;
		}

	if (getmode(sino) != S_IFBLK)
		{
		longjmp(try_b, ENOTBLK);
		}
	dev = DEVNUM(sino);

	dino = namei(try_b, path_dir, NULL, 1);

	catch = setjmp(try_b);
	if (catch)
		{
		i_deref(dino);
	error1:
		i_deref(sino);
		longjmp(error, catch);
		}

	if (getmode(dino) != S_IFDIR)
		{
		longjmp(error, ENOTDIR);
		}

	/* FIXME: the device will be officially opened in fmount() */
	device_find(try_b, dev)->open(try_b, MINOR(dev));
	device_find(try_b, dev)->close(try_b, MINOR(dev));

	fp = findfs(dev);
	if (fp || dino->c_refs != 1 || dino->c_num == ROOTINODE)
		{
		longjmp(try_b, EBUSY);
		}

	fmount(try_b, dev, dino, flag);
	i_deref(dino);
	i_deref(sino);
	}
#endif

#ifdef MODULE_fs_umount
void
fs_umount(jmp_buf error, unsigned char *path)
	{
	jmp_buf try_b;
	unsigned int catch;
	register struct core_inode_s *sino, *dino, **dinopp;
	register dev_t dev;
	struct file_system_s *fp;

	if (super() == 0)
		{
		longjmp(error, EPERM);
		}

	sino = namei(error, path, NULL, 1);

	catch = setjmp(try_b);
	if (catch)
		{
		i_deref(sino);
		longjmp(error, catch);
		}

	if (getmode(sino) != S_IFBLK)
		{
		longjmp(try_b, ENOTBLK);
		}

	dev = DEVNUM(sino);
	device_find(try_b, dev); /* just validate dev */

	fp = findfs(dev);
	if (fp == NULL || fp->s_mounted == 0)
		{
		longjmp(try_b, EINVAL);
		}

	for (dinopp = core_inode_list; dinopp < core_inode_list + CORE_INODE_LIST; dinopp++)
		{
		dino = *dinopp; /* need to check busy flag here?? */
		if (dino && dino->c_refs && dino->c_dev == dev)
			{
			longjmp(error, EBUSY);
			}
		}
	/* need to remove filesystem from table in case open occurs in sync? */

	dev = fp->s_dev;
	i_sync(dev); /* write back all inodes for dev */
	fsync(dev); /* write back superblock for dev */
	bufsync(dev, 1); /* write back all buffers for dev */
	if (setjmp(try_b) == 0)
		{
		device_find(try_b, dev)->close(try_b, MINOR(dev));
		}

	fp->s_mounted = 0;
	i_deref(fp->s_mntpt);
	i_deref(sino);
	}
#endif

