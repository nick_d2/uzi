/* maxmin.c by Nick for NOS/UZI implementation
 *
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 General utility subroutines
**********************************************************/

#include "filesys/maxmin.h"

#ifndef MODULE
#define MODULE_max_int
#define MODULE_max_long
#define MODULE_max_uint
#define MODULE_max_ulong
#define MODULE_min_int
#define MODULE_min_long
#define MODULE_min_uint
#define MODULE_min_ulong
#endif

#ifdef MODULE_max_int
int
max_int(a, b)
	int a;
	int b;
{
	return (a > b) ? a : b;
}
#endif

#ifdef MODULE_max_long
long
max_long(a, b)
	long a;
	long b;
{
	return (a > b) ? a : b;
}
#endif

#ifdef MODULE_max_uint
unsigned int
max_uint(a, b)
	unsigned int a;
	unsigned int b;
{
	return (a > b) ? a : b;
}
#endif

#ifdef MODULE_max_ulong
unsigned long
max_ulong(a, b)
	unsigned long a;
	unsigned long b;
{
	return (a > b) ? a : b;
}
#endif

#ifdef MODULE_min_int
int
min_int(a, b)
	int a;
	int b;
{
	return (a < b) ? a : b;
}
#endif

#ifdef MODULE_min_long
long
min_long(a, b)
	long a;
	long b;
{
	return (a < b) ? a : b;
}
#endif

#ifdef MODULE_min_uint
unsigned int
min_uint(a, b)
	unsigned int a;
	unsigned int b;
{
	return (a < b) ? a : b;
}
#endif

#ifdef MODULE_min_ulong
unsigned long
min_ulong(a, b)
	unsigned long a;
	unsigned long b;
{
	return (a < b) ? a : b;
}
#endif

