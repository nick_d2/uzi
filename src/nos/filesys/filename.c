/*
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 Filesystem related routines
**********************************************************/

#include <errno.h>
#include <string.h>
#include <sys/stat.h> /* for S_IFLNK */
#include "asm.h" /* for DIRECTPERBLOCK */
#include "kernel/dprintf.h"
#include "filesys/filename.h"
#include "filesys/filesys.h" /* for struct file_system_s, findfs(), "filesys/cinode.h" struct core_inode_s */
#include "kernel/usrmem.h"
#include <libintl.h>
#include "po/messages.h"

#ifndef DEBUG
#define DEBUG 0
#endif

#ifndef MODULE
#define MODULE__namei
#define MODULE_namei
#define MODULE_srch_dir
#define MODULE_srch_mt
#define MODULE_ch_link
#define MODULE_filename
#define MODULE_namecomp
#define MODULE_newfile
#define MODULE_chany
#define MODULE_n_creat
#endif

/* _namei() is given a string containing a path name,
 * and returns an inode table pointer.
 *
 * On failure, we longjmp out with an appropriate error code
 *
 * On success, we return the inode, *parent and *rest filled in
 * Note that success may involve returning a NULL inode pointer,
 * this means the parent was located but the file itself was not
 *
 * If a symlink is encountered, *rest points to the remaining path
 * Note that *parent and *rest may contain garbage before the call
 */

#ifdef MODULE__namei
struct core_inode_s *
_namei(jmp_buf error, unsigned char *name, unsigned char **rest,
		struct core_inode_s *strt, struct core_inode_s **parent)
	{
	jmp_buf try_b;
	unsigned int catch;
	struct core_inode_s *temp;
	register struct core_inode_s *ninode; /* next dir */
	register struct core_inode_s *wd;	/* the directory we are currently searching. */

#if DEBUG >= 3
	_dprintf(3, _("_namei(0x%x, 0x%x, 0x%x, 0x%x, 0x%x) starting\n"),
			error, name, rest, strt, parent);
#endif
/* abyte('a'); */
	ninode = wd = strt;
	i_ref(wd);
/* abyte('b'); */
	i_ref(ninode);

	catch = setjmp(try_b);
	if (catch)
		{
/* abyte('y'); */
		*parent = NULL;
/* abyte('z'); */
		i_deref(wd);
#if DEBUG >= 3
		_dprintf(3, _("_namei() error %u\n"), catch);
#endif
/* abyte('?'); */
		longjmp(error, catch);
		}

/* abyte('c'); */
	for (;;)
		{
/* abyte('d'); */
		if (ninode)
			{
/* abyte('e'); */
			magic(ninode, "_namei");
			/* See if we are at a mount point */
/* abyte('f'); */
			ninode = srch_mt(try_b, ninode);
			}

/* abyte('g'); */
		while (*name == '/')	/* Skip (possibly repeated) slashes */
			{
			++name;
			}
/* abyte('h'); */
		if (*name == 0) 	/* No more components of path? */
			{
			break;
			}

/* abyte('i'); */
		if (ninode == NULL)	/* path not found */
			{
			longjmp(try_b, ENOENT);
			}

/* abyte('j'); */
		if (getmode(ninode) == S_IFLNK)
			{
			break;		/* symlinks processed separately */
			}

/* abyte('k'); */
		i_deref(wd);		/* drop current dir */
/* abyte('l'); */
		wd = ninode;		/* switch to next inode */

		/* this node must be directory ... */
/* abyte('m'); */
		if (getmode(wd) != S_IFDIR)
			{
			longjmp(try_b, ENOTDIR);
			}

/* abyte('n'); */
		/* ... enabled for searching */
		if ((getperm(wd) & S_IOEXEC) == 0)
			{
			longjmp(try_b, EPERM);
			}

/* abyte('o'); */
		/* See if we are going up through a mount point */
		if (wd->c_num == ROOTINODE && wd->c_dev != Root_dev &&
/* NOTE: ABOVE SHOULD BE my_thread_p->process_p->p_root FIX THIS SOON!! */
		    name[1] == '.')
			{
/* abyte('p'); */
			/* switch to other fsys */
			temp = findfs(wd->c_dev)->s_mntpt;
/* abyte('q'); */
			i_deref(wd);
/* abyte('r'); */
			wd = temp;
			i_ref(wd);
			}

/* abyte('s'); */
		/* search for next part of path */
		ninode = srch_dir(try_b, wd, (char *)name);

/* abyte('t'); */
		while (*name != 0 && *name != '/')
			{
			++name; 	/* skip this part */
			}
/* abyte('u'); */
		}

/* abyte('v'); */
	*parent = wd;			/* store ref to parent */
/* abyte('w'); */
	*rest = name;
#if DEBUG >= 3
	_dprintf(3, _("_namei() returning 0x%x\n"), ninode);
#endif
/* abyte('x'); */
	return ninode;
	}
#endif

/* namei() is given a string containing a path name,
 * and returns an inode table pointer.
 * If it returns NULL, the file did not exist.
 * If the parent existed, and 'parent' is not null, 'parent'
 * will be filled in with the parents struct core_inode_s *.
 * Otherwise, 'parent' will be set to NULL.
 * If 'follow' is false then tail symlink is not opened
 */

#ifdef MODULE_namei
struct core_inode_s *
namei(jmp_buf error, unsigned char *uname, struct core_inode_s **parent,
		unsigned char follow)
	{
	jmp_buf try_b;
	unsigned int catch;
	register struct core_inode_s *ino;
	struct core_inode_s *parent1, *strt;
	unsigned char *buf, *buf1, *rest, *s;
	unsigned char cnt = 0;
	unsigned char *tb;
	unsigned char *name;
	unsigned char save_sysio;
	size_t count;

#if DEBUG >= 3
 	_dprintf(3, _("namei(0x%x, 0x%x, 0x%x, %u) starting\n"),
			error, uname, parent, follow);
#endif
/* abyte('A'); */
	save_sysio = my_thread_p->sysio;
	if (save_sysio)
		{
		tb = NULL;
		name = uname;
		}
	else
		{
		tb = malloc(BUFSIZE);
		if (tb == NULL)
			{
#if DEBUG >= 3
			_dprintf(3, _("namei() error ENOMEM\n"));
#endif
			longjmp(error, ENOMEM);
			}
		name = tb;
#if 0
		if (ugets(uname, name, BUFSIZE))
			{
			free(tb);
#if DEBUG >= 3
			_dprintf(3, _("namei() error ENAMETOOLONG\n"));
#endif
			longjmp(error, ENAMETOOLONG);
			}
#else
		ugets(uname, name, BUFSIZE);
#endif
		}

/* abyte('B'); */
	strt = (*name == '/') ?
			my_thread_p->process_p->p_root :
			my_thread_p->process_p->p_cwd;

	buf = NULL;
	buf1 = NULL;

/* abyte('C'); */
	for (;;)
		{
		catch = setjmp(try_b);
		if (catch)
			{
			/* in this case ino and parent1 have garbage */
			goto error1;
			}

/* abyte('D'); */
		ino = _namei(try_b, name, &rest, strt, &parent1);
		if (ino == NULL)
			{
			if (parent == NULL)
				{
				/* we want existent file */
				i_deref(parent1);
				longjmp(try_b, ENOENT);
				}

			/* existent parent is ok */
			*parent = parent1;
			break;
			}

/* abyte('E'); */
		if (getmode(ino) != S_IFLNK || (*rest == 0 && follow == 0))
			{
/* abyte('F'); */
			if (parent)
				{
				*parent = parent1;
				}
			else
				{
				i_deref(parent1);
				}
/* abyte('G'); */
			break;
			}

		catch = setjmp(try_b);
		if (catch)
			{
			i_deref(parent1); /* cannot be NULL */
			i_deref(ino); /* cannot be NULL */

		error1:
			free(buf); /* may be NULL */
			free(buf1); /* may be NULL */
			free(tb); /* may be NULL */

			my_thread_p->sysio = save_sysio;
#if DEBUG >= 3
			_dprintf(3, _("namei() error %u\n"), catch);
#endif
			longjmp(error, catch);
			}

		/* check symlink is readable */
/* abyte('H'); */
		if ((getperm(ino) & S_IOREAD) == 0)
			{
			longjmp(try_b, EPERM);
			}

/* abyte('I'); */
		if (buf == NULL)
			{
			buf = malloc(BUFSIZE);
			if (buf == NULL)
				{
				longjmp(try_b, ENOMEM);
				}
			}
/* abyte('J'); */
		if (*rest)
			{	/* need the temporary buffer */
			if (buf1 == NULL)
				{
				buf1 = malloc(BUFSIZE);
				if (buf1 == NULL)
					{
					longjmp(try_b, ENOMEM);
					}
				}
/* abyte('K'); */
			s = buf1;
			while ('\0' != (*s++ = *rest++))
				;
/* abyte('L'); */
			rest = buf1;
			}
		else
			{
/* abyte('M'); */
			rest = (unsigned char *)"";
					/* ! It's important - really "" ! */
			}

		/* readin symlink contents */
/* abyte('N'); */
		my_thread_p->sysio = 1;
/* abyte('O'); */
		count = readwritei(try_b, 0, ino, buf, BUFSIZE, 0);

/* abyte('P'); */
		/* check for buffer size */
		s = rest;
		while (*s++ != '\0')
			;

/* abyte('Q'); */
		if (count + (s - rest) >= (BUFSIZE - 1))
			{
			longjmp(try_b, ENOMEM);
			}

/* abyte('R'); */
		/* create substituted name */
		s = buf + count;
		if (*rest)
			{
			*s++ = '/';
			}

/* abyte('S'); */
		while ((*s++ = *rest++) != '\0')
			;

/* abyte('T'); */
		name = buf;
		strt = (*name == '/') ? Root_ino : parent1;
 /* NOTE:  THIS IS WRONG!  SHOULD USE my_thread_p->process_p->p_root!! */
/* abyte('U'); */
		if (++cnt == 13) /* recursion too deep */
			{
			longjmp(try_b, ELOOP);
			}

/* abyte('V'); */
		i_deref(parent1);
/* abyte('W'); */
		i_deref(ino);
/* abyte('X'); */
		}

/* abyte('Z'); */
	free(buf); /* may be NULL */
	free(buf1); /* may be NULL */
	free(tb); /* may be NULL */

	my_thread_p->sysio = save_sysio;

#if DEBUG >= 3
	_dprintf(3, _("namei() returning 0x%x\n"), ino);
#endif
/* abyte('/'); */
	return ino;
}
#endif

/* Srch_dir() is given a inode pointer of an open directory and a string
 * containing a filename, and searches the directory for the file.
 * If it exists, it opens it and returns the inode pointer, otherwise NULL.
 *
 * This depends on the fact that bmap will return unallocated blocks as
 * NULLBLK, and a partially allocated block will be padded with zeroes.
 */

#ifdef MODULE_srch_dir
struct core_inode_s *
srch_dir(jmp_buf error, struct core_inode_s *wd, unsigned char *name)
{
	jmp_buf try_b;
	unsigned int catch;
	ino_t ino;
	blkno_t db;
	unsigned int cb, ce, nblocks;
	register direct_t *bp;
	register struct block_buffer_s *blkbuf_p;

#if DEBUG >= 3
	_dprintf(3, _("srch_dir(0x%x, 0x%x, 0x%x) starting\n"),
			error, wd, name);
#endif
/* abyte('A'); */
	i_acquire(wd);
	catch = setjmp(try_b);
	if (catch)
		{
		i_release(wd);
#if DEBUG >= 3
		_dprintf(3, _("srch_dir() error %u a\n"), catch);
#endif
		longjmp(error, catch);
		}

/* abyte('B'); */
	nblocks = (unsigned int)(wd->c_node.i_size >> BUFSIZELOG);
						/* # of full blocks */
/* abyte('C'); */
	if ((unsigned int)wd->c_node.i_size & (BUFSIZE-1))
		++nblocks;			/* and part of last block */

/* abyte('D'); */
	cb = 0;
	while (cb < nblocks) {
/* abyte('E'); */
		/* map file block to fs */
		db = bmap(try_b, wd, cb, 1);
		if (db == NULLBLK)
			break;

/* abyte('F'); */
		blkbuf_p = bread(try_b, wd->c_dev, db, 0);

/* abyte('H'); */
		bp = (direct_t *)blkbuf_p->bf_data;
		ce = 0;
		while (ce < DIRECTPERBLOCK) {
/* abyte('I'); */
			if (namecomp(name, bp->d_name)) {
/* abyte('J'); */
				/* file name found */
				ino = bp->d_ino; /* inode of file */
/* abyte('K'); */
				bfree(try_b, blkbuf_p, 0);
/* abyte('L'); */
				i_release(wd);

				/* open the found file */
#if DEBUG >= 3
/* abyte('M'); */
				/* borrow wd!! */
				wd = i_open(error, wd->c_dev, ino);
				_dprintf(3, _("srch_dir() returning 0x%x, found\n"),
						wd);
/* abyte('N'); */
				return wd;
#else
/* abyte('O'); */
				return i_open(error, wd->c_dev, ino);
#endif
			}
/* abyte('P'); */
			++ce;
			++bp;
		}
/* abyte('Q'); */

		bfree(try_b, blkbuf_p, 0);
/* abyte('R'); */
		++cb;
	}
/* abyte('S'); */
	i_release(wd);

	/* file not found in this directory */
#if DEBUG >= 3
	_dprintf(3, _("srch_dir() returning NULL, not found\n"));
#endif
/* abyte('T'); */
	return NULL;
}
#endif

/* Srch_mt() sees if the given inode is a mount point.
 * If so it dereferences it, and references and returns a pointer
 * to the root of the mounted filesystem.
 *
 * Note:  If an error occurs, we know that ino has been dereferenced
 * Therefore the caller should not deref the inode its error handler
 */

#ifdef MODULE_srch_mt
struct core_inode_s *
srch_mt(jmp_buf error, struct core_inode_s *ino)
	{
	register struct file_system_s **fspp, *fsp;

	for (fspp = file_system_list; fspp < file_system_list + FILE_SYSTEM_LIST; fspp++)
		{
		fsp = *fspp;
		if (fsp->s_mounted && fsp->s_mntpt == ino)
			{
			i_deref(ino);
			return i_open(error, fsp->s_dev, ROOTINODE);
			}
		}
	return ino;
	}
#endif

/* Ch_link() modifies or makes a new entry in the directory for the name
 * and inode pointer given. The directory is searched for oldname.
 * When found, it is changed to newname, and it inode # is that of *nindex.
 *
 * An empty oldname ("") matches a unused slot.
 * A nindex NULL means an inode #0.
 *
 * An exit via longjmp() means there was no space left in the filesystem,
 * or a non-empty oldname was not found, or the user did not have write
 * permission.  If the function returns, the operation was successful.
 */

#ifdef MODULE_ch_link
void
ch_link(jmp_buf error, struct core_inode_s *wd, unsigned char *oldname,
		unsigned char *newname, struct core_inode_s *nindex)
	{
	jmp_buf try_b;
	unsigned int catch;
	direct_t curentry;
	int i;
	char save_sysio;
	size_t count;
	unsigned long position;

#if DEBUG >= 3
	_dprintf(3, _("ch_link(0x%x, 0x%x, \"%s\", \"%s\", 0x%x) starting\n"),
			error, wd, oldname, newname, nindex);
#endif
	/* Need the write permissions */
	if ((getperm(wd) & S_IOWRITE) == 0)
		{
#if DEBUG >= 3
		_dprintf(3, _("ch_link() error EPERM\n"));
#endif
		longjmp(error, EPERM);
		}

	save_sysio = my_thread_p->sysio;
	my_thread_p->sysio = 1;

	catch = setjmp(try_b);
	if (catch)
		{
		my_thread_p->sysio = save_sysio;
#if DEBUG >= 3
		_dprintf(3, _("ch_link() error %u\n"), catch);
#endif
		longjmp(error, catch);
		}

	/* Search the directory for the desired slot */
	position = 0;	/* from beginning */
	for (;;)
		{
		count =	readwritei(try_b, 0, wd,
				&curentry, sizeof(curentry), position);
		position += count;

		/* Read until EOF or name is found */
		if (count == 0 || namecomp((unsigned char *)oldname,
				curentry.d_name))
			{
			break;
			}
		}

	if (count == 0 && *oldname)
		{
		/* Rename and old entry not found */
		longjmp(error, ENOENT);
		}

	oldname = curentry.d_name;
	i = sizeof(curentry.d_name);
	while (--i >= 0)
		{
		if ('\0' != (*oldname++ = *newname))
			{
			++newname;
			}
		}
	curentry.d_ino = nindex ? nindex->c_num : 0;

	/* If an existing slot is being used,
	 * we must back up the file offset
	 */
	if (count)
		{
		position -= count;
		}

	/* write back updated directory */
	readwritei(try_b, 1, wd, &curentry, sizeof(curentry), position);

	/* Update directory length to next block - simple way to extend dir */
 /* IS THIS REALLY NECESSARY? HASN'T IT ALREADY BEEN DONE BY readwritei()?? */
	if ((unsigned int)wd->c_node.i_size & (BUFSIZE - 1))
		{
		wd->c_node.i_size = (wd->c_node.i_size & ~(BUFSIZE - 1)) +
				BUFSIZE;
		}
	setftim(wd, A_TIME | M_TIME | C_TIME); /* And sets c_dirty */

	my_thread_p->sysio = save_sysio;
#if DEBUG >= 3
	_dprintf(3, _("ch_link() returning\n"));
#endif
	}
#endif

/* Filename is given a path name in user space, and copies
 * the final component of it to name (in system space).
 */

#ifdef MODULE_filename
void
filename(unsigned char *upath, unsigned char *name)
{
	register unsigned char *buf;
	register unsigned char *ptr;

	buf = malloc(BUFSIZE);
	if (buf == NULL)
		{
		goto error;
		}

	if (ugets(upath, buf, BUFSIZE))
		{
		free(buf);
	error:
		*name = '\0';
		return;		  /* An access violation reading the name */
		}

	ptr = buf;
	while (*ptr)
		{
		++ptr;
		}

	/* Special case for "...name.../" */
	while (*ptr != '/' && ptr-- > buf)
		;
	ptr++;

	memcpy(name, ptr, DIRECTNAMELEN);
	free(buf);
}
#endif

/* Namecomp() compares two strings to see if they are the same file name.
 * It stops at DIRECTNAMELEN chars or a null or a slash.
 * It returns 0 for difference.
 */

#ifdef MODULE_namecomp
int
namecomp(unsigned char *n1, unsigned char *n2)
{
	unsigned char n = DIRECTNAMELEN;

#if DEBUG >= 5
 _dprintf(5, _("namecomp(\"%s\", \"%s\") starting\n"), n1, n2);
#endif

/*RPB*/
	if (strcmp(n1, "*") == 0 ||
	    strcmp(n1, "*.*") == 0)
		return 1;
/*RPB*/

	while (*n1 && *n1 != '/') {
		if (*n1++ != *n2++)
			goto NotEq;
		if (--n == 0)
 {
#if DEBUG >= 5
 _dprintf(5, _("namecomp() returning -1, error\n"));
#endif
			return (-1);	/* first name too long - ignore this */
 }
	}
	if (*n2 == '\0' || *n2 == '/')
 {
#if DEBUG >= 5
 _dprintf(5, _("namecomp() returning -1, match\n"));
#endif
		return 1;		/* names matched */
 }
NotEq:
#if DEBUG >= 5
 _dprintf(5, _("namecomp() returning 0, no match\n"));
#endif
	return 0;
}
#endif

/* Newfile() is given a pointer to a directory and a name, and
 * creates an entry in the directory for the name, dereferences
 * the parent, and returns a pointer to the new inode.
 *
 * It allocates an inode number, and creates a new entry in the inode
 * table for the new file, and initializes the inode table entry for
 * the new file.  The new file will have one reference, and 0 links to it.
 *
 * Better make sure there isn't already an entry with the same name.
 * Note:  The parent is deref'd even if we longjmp() with an exception.
 */

#ifdef MODULE_newfile
struct core_inode_s *
newfile(jmp_buf error, struct core_inode_s *pino, unsigned char *name)
{
	jmp_buf try_b;
	unsigned int catch;
	register struct core_inode_s *nindex;

#if DEBUG >= 3
	_dprintf(3, _("newfile(0x%x, 0x%x, \"%s\") starting\n"),
			error, pino, name);
#endif
	if ((getperm(pino) & S_IOWRITE) == 0)
		{
		i_deref(pino);
#if DEBUG >= 3
		_dprintf(3, _("newfile() error EPERM\n"));
#endif
		longjmp(error, EPERM);
		}

	catch = setjmp(try_b);
	if (catch)
		{
		/* can't create new inode */
		i_deref(pino);
#if DEBUG >= 3
		_dprintf(3, _("newfile() error %u a\n"), catch);
#endif
		longjmp(error, catch);
		}
	nindex = i_open(try_b, pino->c_dev, NULLINO);

	/* fill-in new inode */
	i_acquire(nindex);
	memset(&nindex->c_node, 0, sizeof(struct disk_inode_s));
	nindex->c_node.i_mode = S_IFREG; /* for the time being */
	nindex->c_node.i_nlink = 1;
	nindex->c_node.i_uid = my_thread_p->process_p->p_euid;
	nindex->c_node.i_gid = my_thread_p->process_p->p_egid;
	nindex->c_dirty = 2;
	if (setjmp(try_b) == 0)
		{
		wr_inode(try_b, nindex, 0);
		}
	i_release(nindex);

	catch = setjmp(try_b);
	if (catch)
		{
		/* can't enter new file to directory */
		i_deref(nindex);
		i_deref(pino);
#if DEBUG >= 3
		_dprintf(3, _("newfile() error %u b\n"), catch);
#endif
		longjmp(error, catch);
		}
	ch_link(try_b, pino, "", name, nindex);

	i_deref(pino);
#if DEBUG >= 3
	_dprintf(3, _("newfile() returning 0x%x\n"), nindex);
#endif
	return nindex;
}
#endif

#ifdef MODULE_chany
void
chany(jmp_buf error, unsigned char *path, unsigned int val1, unsigned int val2,
		unsigned char mode)
	{
	register struct core_inode_s *ino;

	ino = namei(error, path, NULL, 1);
	if (ino->c_node.i_uid != my_thread_p->process_p->p_euid &&
			super() == 0)
		{
		i_deref(ino);
		longjmp(error, EPERM);
		}

	if (mode)
		{
		/* file class can't be changed */
		ino->c_node.i_mode = (ino->c_node.i_mode & S_IFMT) |
				((mode_t)val1 & S_UMASK);
		}
	else
		{
		ino->c_node.i_uid = val1;
		ino->c_node.i_gid = val2;
		}

	setftim(ino, C_TIME);
	i_deref(ino);
}
#endif

/* create any kind of node */

#ifdef MODULE_n_creat
struct core_inode_s *
n_creat(jmp_buf error, unsigned char *name, unsigned char new, mode_t mode)
	{
	jmp_buf try_b;
	unsigned int catch;
	register struct core_inode_s *ino;
	struct core_inode_s *parent = NULL;
	char fname[15];

#if DEBUG >= 3
	_dprintf(3, _("n_creat(0x%x, 0x%x, %u, 0%o) starting\n"),
			error, name, (unsigned int)new, mode);
#endif
	ino = namei(error, name, &parent, 1);
	if (ino)
		{
		/* the file already exists */
		i_deref(parent); /* parent not needed */

		if (new)
			{
			/* caller wants a new one */
			i_deref(ino);
#if DEBUG >= 3
			_dprintf(3, _("n_creat() error EEXIST\n"));
#endif
			longjmp(error, EEXIST);
			}

		if (getmode(ino) == S_IFDIR)
			{
			/* can't rewrite directory */
			i_deref(ino);
#if DEBUG >= 3
			_dprintf(3, _("n_creat() error EISDIR\n"));
#endif
			longjmp(error, EISDIR);
			}

		if ((getperm(ino) & S_IOWRITE) == 0)
			{
			/* must have write access */
			i_deref(ino);
#if DEBUG >= 3
			_dprintf(3, _("n_creat() error EACCES\n"));
#endif
			longjmp(error, EACCES); /* EPERM?? */
			}

		i_acquire(ino);
		f_trunc(ino); /* don't of_truncate(); as don't have oft yet */
		i_release(ino);
		}
	else
		{
		/* must create the new file */
		filename(name, fname);

		/* note: use error, not try_b, as newfile() derefs parent */
		ino = newfile(error, parent, fname);

		i_acquire(ino);
		ino->c_node.i_mode = mode;
		i_release(ino);
		setftim(ino, A_TIME | M_TIME | C_TIME);

		/* The rest of the inode is initialized in newfile() */
		if (_getmode(mode) == S_IFDIR)
			{
			catch = setjmp(try_b);
			if (catch)
				{
				i_deref(ino);
				i_deref(parent);
#if DEBUG >= 3
				_dprintf(3, _("n_creat() error %u a\n"),
						catch);
#endif
				longjmp(error, catch);
				}

			ch_link(try_b, ino, "", ".", ino);
			i_acquire(ino);
			ino->c_node.i_size = 2*sizeof(direct_t);
			ino->c_node.i_nlink++;
			i_release(ino);

			ch_link(try_b, ino, "", "..", parent);
			i_acquire(parent);
			parent->c_node.i_nlink++;
			parent->c_dirty = 1;
			i_release(parent);
			}
		}

	i_acquire(ino);
	catch = setjmp(try_b);
	if (catch)
		{
		i_release(ino);
		i_deref(ino);
#if DEBUG >= 3
		_dprintf(3, _("n_creat() error %u b\n"), catch);
#endif
		longjmp(error, catch);
		}

	wr_inode(try_b, ino, 0);
	i_release(ino);

#if DEBUG >= 3
	_dprintf(3, _("n_creat() returning 0x%x\n"), ino);
#endif
	return ino;
	}
#endif

