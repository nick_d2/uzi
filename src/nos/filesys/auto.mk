# auto.mk by Nick for Hytech NOS/UZI project

# -----------------------------------------------------------------------------

SUBDIRS=

OUTPUTS=	filesys.$(LIBEXT)

filesys_$(LIBEXT)_SOURCES= \
		bitmap.c cinode.c execve.c filename.c filesys.c maxmin.c \
		openfile.c xip.c

bitmap_c_MODULES= \
		Badfsmsg Badinomsg i_alloc i_free blk_alloc blk_free validblk \
		bitmap_search bitmap_reserve bitmap_find bitmap_get bitmap_set

cinode_c_MODULES= \
		core_inode_list Refstoinode i_ref i_deref i_acquire i_release \
		i_open wr_inode readwritei isdevice freeblk f_trunc bmap \
		getmode getperm setftim magic i_stat i_sync inotab_garbage

execve_c_MODULES= \
		fs_execve wargs rargs

filename_c_MODULES= \
		_namei namei srch_dir srch_mt ch_link filename namecomp \
		newfile chany n_creat

filesys_c_MODULES= \
		Root_dev Root_ino file_system_list Baddevmsg fs_init fs_end findfs getfs \
		fmount fsync fs_sync fs_utime fs_link fs_symlink fs_unlink \
		fs_chdir fs_chroot fs_mknod fs_access fs_chmod fs_chown \
		fs_stat fs_mount fs_umount

maxmin_c_MODULES= \
		max_int max_long max_uint max_ulong \
		min_int min_long min_uint min_ulong

openfile_c_MODULES= \
		open_file_list of_alloc of_deref of_truncate of_garbage \
		of_locate of_open of_lseek of_falign of_pipe

xip_c_MODULES=	xip_align xip_ualign xip_examine xip_align_chase \
		xip_align_bmap xip_align_reverse xip_align_recurse

# -----------------------------------------------------------------------------

