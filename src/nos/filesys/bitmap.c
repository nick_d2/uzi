/*
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 Filesystem related routines
**********************************************************/

#include <errno.h>
#include <string.h>
#include "filesys/bitmap.h"
#include "kernel/dprintf.h"
#include "filesys/filesys.h" /* for struct file_system_s, findfs() */
#include "filesys/xip.h"
#include <libintl.h>
#include "po/messages.h"

#ifndef DEBUG
#define DEBUG 0
#endif

#ifdef MODULE
#define STATIC
extern _char *Badfsmsg;
#else
#define STATIC static
#define MODULE_Badfsmsg
#define MODULE_Badinomsg
#define MODULE_i_alloc
#define MODULE_i_free
#define MODULE_blk_alloc
#define MODULE_blk_free
#define MODULE_freeblk
#define MODULE_validblk
#define MODULE_bitmap_search
#define MODULE_bitmap_reserve
#define MODULE_bitmap_find
#define MODULE_bitmap_get
#define MODULE_bitmap_set
#endif

/* Common messages. They are here to save memory */
#ifdef MODULE_Badfsmsg
STATIC _char *Badfsmsg = N_("%s: fs of dev %u marked as bad\n");
#endif
#ifdef MODULE_Badinomsg
_char *Badinomsg = N_("%s: bad inode number 0x%x\n");
#endif

/* I_alloc() finds an unused inode number, and returns it,
 * or NULLINO if there are no more inodes available.
 */

#ifdef MODULE_i_alloc
ino_t
i_alloc(jmp_buf error, dev_t devno)
	{
	ino_t ino;
	struct file_system_s *dev;

#if DEBUG >= 4
	_dprintf(4, _("i_alloc(0x%x, 0x%x) starting\n"), error, devno);
#endif
	dev = getfs(devno);
	if (dev->s_ronly)
		{
#if DEBUG >= 4
		_dprintf(4, _("i_alloc() error EROFS\n"));
#endif
		longjmp(error, EROFS);
		}

	ino = bitmap_find(error, devno, 0, 0, 1, dev->s_bitmap_inode,
			dev->s_bitmap_block);
	if (ino <= ROOTINODE || ino >= devinodes(dev))
		{
#if DEBUG >= 4
		_dprintf(4, _("i_alloc() error ENOSPC\n"));
#endif
		longjmp(error, ENOSPC);
		}

	if (dev->s_tinode == 0 || ino <= ROOTINODE || ino >= devinodes(dev))
		{
		_dprintf(0, Badfsmsg, "i_alloc", devno);
		dev->s_mounted = ~SMOUNTED;	/* mark device as bad */
#if DEBUG >= 4
		_dprintf(4, _("i_alloc() error ECORRUPTFS\n"));
#endif
		longjmp(error, ECORRUPTFS);
		}

	dev->s_tinode--;
#if DEBUG >= 4
	_dprintf(4, _("i_alloc() returning %u\n"), ino);
#endif
	return ino;
	}
#endif

/* I_free() is given a device and inode number, and frees the inode.
 * It is assumed that there are no references to the inode in the
 * inode table or in the filesystem.
 */

#ifdef MODULE_i_free
void
i_free(jmp_buf error, dev_t devno, ino_t ino)
	{
	struct file_system_s *dev;

#if DEBUG >= 4
	_dprintf(4, _("i_free(0x%x, 0x%x, %u) starting\n"), error, devno, ino);
#endif
	dev = getfs(devno);
	if (ino <= ROOTINODE || ino >= devinodes(dev))
		{
		_panic(Badinomsg, "i_free", ino);
		}

	if (bitmap_set(error, devno, ino, 0, dev->s_bitmap_inode,
			dev->s_bitmap_block) != 1)
		{
		_dprintf(0, _("i_free() corrupt filesystem\n"));
#if DEBUG >= 4
 _dprintf(4, _("i_free() error ECORRUPTFS\n"));
#endif
		longjmp(error, ECORRUPTFS);
		}

	dev->s_tinode++;
#if DEBUG >= 4
 _dprintf(4, _("i_free() returning\n"));
#endif
	}
#endif

/* Blk_alloc() is given a device number, and allocates an unused block
 * from it. A returned block number of zero means no more blocks.
 * new: if the caller passes dirty = 1, don't write back immediately
 * new: if the caller passes dirty = 2, write back the zeros immediately
 * (the latter mode is used when allocating new indirect/dindirect blocks)
 */

#ifdef MODULE_blk_alloc
blkno_t
blk_alloc(jmp_buf error, dev_t devno, unsigned char dirty)
	{
	struct block_buffer_s *blkbuf_p;
	blkno_t newno;
	struct file_system_s *dev;

#if DEBUG >= 4
	_dprintf(4, _("blk_alloc(0x%x, 0x%x, %u) starting\n"),
			error, devno, (unsigned int)dirty);
#endif
	dev = getfs(devno);
	if (dev->s_ronly)
		{
#if DEBUG >= 3
		_dprintf(3, _("blk_alloc() error EROFS\n"));
#endif
		longjmp(error, EROFS);
		}

	newno = bitmap_find(error, devno, 0, 0, 1, dev->s_bitmap_block,
					    dev->s_bitmap_immov);
	if (newno < (dev->s_reserv + dev->s_isize) ||
			newno >= dev->s_fsize)
		{
#if DEBUG >= 3
		_dprintf(3, _("blk_alloc() error ENOSPC\n"));
#endif
		longjmp(error, ENOSPC);
		}

	validblk(devno, newno);
	if (dev->s_tfree == 0)
		{
		_dprintf(0, Badfsmsg, "blk_alloc", devno);
		dev->s_mounted = ~SMOUNTED;
#if DEBUG >= 3
		_dprintf(3, _("blk_alloc() error ECORRUPTFS\n"));
#endif
		longjmp(error, ECORRUPTFS);
		}
	dev->s_tfree--;

	/* Zero out the new block */
 /* at this point should catch errors and unreserve block.. fixme */
	blkbuf_p = bread(error, devno, newno, 2); /* 2 for zeroing */
	bfree(error, blkbuf_p, dirty); /* write back */

#if DEBUG >= 4
	_dprintf(4, _("blk_alloc() returning %u\n"), newno);
#endif
	return newno;
}
#endif

/* Blk_free() is given a device number and a block number,
 * and frees the block.
 */

#ifdef MODULE_blk_free
void
blk_free(jmp_buf error, dev_t devno, blkno_t blk)
	{
	struct file_system_s *dev;

#if DEBUG >= 4
	_dprintf(4, _("blk_free(0x%x, 0x%x, %u) starting\n"),
			error, devno, blk);
#endif
	if (blk == 0)
		{
#if DEBUG >= 4
		_dprintf(4, _("blk_free() returning, null\n"));
#endif
		return;
		}

	dev = getfs(devno);
	if (dev->s_ronly)
		{
#if DEBUG >= 4
		_dprintf(4, _("blk_free() error EROFS\n"));
#endif
		longjmp(error, EROFS);
		}

	validblk(devno, blk);
	if (bitmap_set(error, devno, blk, 0, dev->s_bitmap_block,
			dev->s_bitmap_immov) != 1)
		{
		_dprintf(0, _("blk_free() corrupt filesystem\n"));
#if DEBUG >= 4
 _dprintf(4, _("blk_free() error ECORRUPTFS\n"));
#endif
		longjmp(error, ECORRUPTFS);
		}

#if 1 /* temporary only... also unreserve the block if it was reserved */
	bitmap_set(error, devno, blk, 0, dev->s_bitmap_immov,
			dev->s_bitmap_final);
#endif

	dev->s_tfree++;
#if DEBUG >= 4
 _dprintf(4, _("blk_free() returning\n"));
#endif
	}
#endif

/* Validblk() panics if the given block number is not a valid data block
 * for the given device.
 */

#ifdef MODULE_validblk
void
validblk(dev_t devno, blkno_t num)
	{
	struct file_system_s *dev;

	dev = findfs(devno);
	if (dev == NULL)
		{
		_panic(_("validblk: dev 0x%x not mounted"), dev);
		}

	if (num < dev->s_isize + dev->s_reserv || num >= dev->s_fsize)
		{
		_panic(_("validblk: invalid block %u"), num);
		}
	}
#endif

#ifdef MODULE_bitmap_search
blkno_t
bitmap_search(jmp_buf error, dev_t devno, int size, blkno_t start, blkno_t final)
	{
	struct block_buffer_s *blkbuf_p;
	unsigned char *p;
	blkno_t i, j, lm, blk;
	unsigned long bits, mask; /* assumes REGION_BLOCKS <= 32 */

#if DEBUG >= 3
	_dprintf(3, _("bitmap_search(0x%x, 0x%x, %d, %u, %u) starting\n"),
			error, devno, size, start, final);
#endif
#if REGION_BLOCKS < 32
	/* validate the size parameter for extra safety */
	if (size < 1 || size > REGION_BLOCKS)
		{
#if DEBUG >= 3
		_dprintf(3, _("bitmap_search() error EINVAL\n"));
#endif
		longjmp(error, EINVAL);
		}

	/* form a bitstring of the needed size, for testing bitmap */
	mask = (1L << size) - 1;
#else
	/* validate the size parameter for extra safety */
	if (size < 1 || size > 32) /* REGION_BLOCKS > 32 bombs at runtime */
		{
#if DEBUG >= 3
		_dprintf(3, _("bitmap_search() error EINVAL\n"));
#endif
		longjmp(error, EINVAL);
		}

	/* form a bitstring of the needed size, for testing bitmap */
	if (size < 32)
		{
		mask = (1L << size) - 1;
		}
	else
		{
		mask = (unsigned long)-1; /* size is maximum 32 */
		}
#endif

	/* initialise bitbuffer, we need it to span bitmap blocks */
	bits = (unsigned long)-1;

	/* read bitmap, skipping unused bytes of reserved blocks */
	j = start;
	while (j < final)
		{
		/* calculate ending position after reading block */
		lm = min_uint((j + BUFSIZE) & ~(BUFSIZE - 1), final);

		/* read block and calculate starting relevant byte */
		blkbuf_p = bread(error, devno, j >> BUFSIZELOG, 0);
		p = blkbuf_p->bf_data + (j & (BUFSIZE - 1));

		/* ready to scan each byte looking for contiguous '0' bits */
		for (i = j; i < lm; i += (PAGE_BLOCKS >> 3)) /* can't be 0 ! */
			{
			bits = (bits >> PAGE_BLOCKS) |
			       (((unsigned long)*p) << (32 - PAGE_BLOCKS));

			if ((bits & mask) == 0)
				{
				/* all of the required blocks are unlocked */

				/* calculate which disk block it refers to */
				blk = ((i - start) << 3) - (32 - PAGE_BLOCKS);

				/* don't write it back, our caller will */
				bfree(error, blkbuf_p, 0);

#if DEBUG >= 3
				_dprintf(3, _("bitmap_search() returning %u\n"),
						blk);
#endif
				return blk;
				}

			p += PAGE_BLOCKS >> 3; /* can't be 0 */
			}

		/* nothing found this time, release block and loop */
		bfree(error, blkbuf_p, 0);

		/* bump counter to continue after data just read */
		j = lm;
		}

	/* no '0' string found within the bitmap limits */
#if DEBUG >= 3
	_dprintf(3, _("bitmap_search() error ENOSPC\n"));
#endif
	longjmp(error, ENOSPC);
	}
#endif

#ifdef MODULE_bitmap_reserve
void
bitmap_reserve(jmp_buf error, dev_t devno, blkno_t blk, unsigned char size,
				unsigned char flag, blkno_t start,
				blkno_t final)
	{
	blkno_t i, j, lm;
	struct block_buffer_s *blkbuf_p;
	unsigned char *p, mask;

#if DEBUG >= 3
	_dprintf(3, _("bitmap_reserve(0x%x, 0x%x, %u, %u, %u, %u, %u) starting\n"),
			error, devno, blk, (unsigned int)size,
			(unsigned int)flag, start, final);
#endif
	/* calculate byte position within bitmap for this block */
	j = start + (blk >> 3);
	if (j >= final)
		{
#if DEBUG >= 3
		_dprintf(3, _("bitmap_reserve() error EINVAL a\n"));
#endif
		longjmp(error, EINVAL);
		}

	/* read block and calculate starting relevant byte */
	blkbuf_p = bread(error, devno, j >> BUFSIZELOG, 0);
	p = blkbuf_p->bf_data + (j & (BUFSIZE - 1));

	/* calculate ending position after reading block */
	lm = min_uint((j + BUFSIZE) & ~(BUFSIZE - 1), final);

	/* ready to modify the required bits and write back */
	mask = flag ? 0xff : 0;
	while (size > 8)
		{
		*p++ = mask;

		j++;
		if (j >= lm)
			{
			/* gone past the end of the current bitmap block */
			if (j >= final)
				{
#if DEBUG >= 3
				_dprintf(3, _("bitmap_reserve() error EINVAL b\n"));
#endif
				longjmp(error, EINVAL);
				}

			/* need to flush the modifications done so far */
			bfree(error, blkbuf_p, 1);

			/* read next block, we always start at the beginning */
			blkbuf_p = bread(error, devno, j >> BUFSIZELOG, 0);
			p = blkbuf_p->bf_data;

			/* calculate ending position after reading block */
			lm = min_uint((j + BUFSIZE) & ~(BUFSIZE - 1), final);
			}

		size -= 8;
		}

	/* done all bitmap bytes, except the last (maybe partial) byte */
	mask = (1 << size) - 1;
	if (flag)
		{
		*p |= mask;
		}
	else
		{
		*p &= ~mask;
		}

	/* ready to write back the modified bitmap block */
	bfree(error, blkbuf_p, 1);
#if DEBUG >= 3
	_dprintf(3, _("bitmap_reserve() returning\n"));
#endif
	}
#endif

#ifdef MODULE_bitmap_find
blkno_t
bitmap_find(jmp_buf error, dev_t devno, blkno_t blk, unsigned char flag,
		unsigned char toggle, blkno_t start, blkno_t final)
	{
	unsigned char c, first;
	struct block_buffer_s *blkbuf_p;
	unsigned char *p, mask;
	blkno_t i, j, lm;

#if DEBUG >= 3
	_dprintf(3, _("bitmap_find(0x%x, 0x%x, %u, %u, %u, %u, %u) starting\n"),
			error, devno, blk, (unsigned int)flag,
			(unsigned int)toggle, start, final);
#endif
	/* set up to skip byte sized groups having opposite value */
	mask = flag ? 0 : 0xff;

	/* prepare to handle the first byte of the bitmap specially */
	first = ~((1 << (blk & 7)) - 1);

	/* read bitmap, skipping unused bytes of reserved blocks */
	j = start + (blk >> 3);
	while (j < final)
		{
		/* calculate ending position after reading block */
		lm = min_uint((j + BUFSIZE) & ~(BUFSIZE - 1), final);

		/* read block and calculate starting relevant byte */
		blkbuf_p = bread(error, devno, j >> BUFSIZELOG, 0);
		p = blkbuf_p->bf_data + (j & (BUFSIZE - 1));

		/* ready to scan each byte looking for a bit of 'flag' */
		for (i = j; i < lm; i++)
			{
			c = (*p ^ mask) & first; /* set bits being sought */
			first = 0xff; /* process full bytes from now on */

			if (c)
				{
				/* at least one of the 8 blocks is available */

				/* calculate which disk block it refers to */
				blk = (i - start) << 3;
				for (i = 0; i < 7; i++) /* don't check bit 7 */
					{
					if (c & (1 << i))
						{
						break;
						}
					}
				blk += i;

				if (toggle)
					{
					/* ready to toggle and write back */
					*p ^= 1 << i;
					bfree(error, blkbuf_p, 1);
					}
				else
					{
					bfree(error, blkbuf_p, 0);
					}

#if DEBUG >= 3
				_dprintf(3, _("bitmap_find() returning %u\n"),
						blk);
#endif
				return blk;
				}

			p++;
			}

		/* nothing found this time, release block and loop */
		bfree(error, blkbuf_p, 0);

		/* bump counter to continue after data just read */
		j = lm;
		}

	/* no 'flag' bit found within the bitmap limits */
	blk = (j - start) << 3;
#if DEBUG >= 3
	_dprintf(3, _("bitmap_find() returning %u, bitmap full\n"), blk);
#endif
	return blk; /* not an error, bitmap was really full */
	}
#endif

#ifdef MODULE_bitmap_get
unsigned char
bitmap_get(jmp_buf error, dev_t devno, blkno_t blk, blkno_t start,
		blkno_t final)
	{
	blkno_t i, j;
	struct block_buffer_s *blkbuf_p;
	unsigned char *p;

#if DEBUG >= 3
	_dprintf(3, _("bitmap_get(0x%x, 0x%x, %u, %u, %u) starting\n"),
			error, devno, blk, start, final);
#endif
	/* calculate byte position within bitmap for this block */
	j = start + (blk >> 3);
	if (j >= final)
		{
#if DEBUG >= 3
		_dprintf(3, _("bitmap_get() error EINVAL\n"));
#endif
		longjmp(error, EINVAL);
		}
	i = 1 << (blk & 7);

	/* read block and calculate starting relevant byte */
	blkbuf_p = bread(error, devno, j >> BUFSIZELOG, 0);
	p = blkbuf_p->bf_data + (j & (BUFSIZE - 1));

	/* check if the block is free or used (value of 0 or 1) */
	i = ((*p & i) != 0);

	bfree(error, blkbuf_p, 0);
#if DEBUG >= 3
	_dprintf(3, _("bitmap_get() returning %u\n"), (unsigned int)i);
#endif
	return (unsigned char)i;
	}
#endif

#ifdef MODULE_bitmap_set
unsigned char
bitmap_set(jmp_buf error, dev_t devno, blkno_t blk, unsigned char flag,
			blkno_t start, blkno_t final)
	{
	blkno_t i, j;
	struct block_buffer_s *blkbuf_p;
	unsigned char *p, mask;

#if DEBUG >= 3
	_dprintf(3, _("bitmap_set(0x%x, 0x%x, %u, %u, %u, %u) starting\n"),
			error, devno, blk, (unsigned int)flag, start, final);
#endif
	/* calculate byte position within bitmap for this block */
	j = start + (blk >> 3);
	if (j >= final)
		{
#if DEBUG >= 3
		_dprintf(3, _("bitmap_set() error EINVAL\n"));
#endif
		longjmp(error, EINVAL);
		}
	mask = 1 << (blk & 7);

	/* read block and calculate starting relevant byte */
	blkbuf_p = bread(error, devno, j >> BUFSIZELOG, 0);
	p = blkbuf_p->bf_data + (j & (BUFSIZE - 1));

	/* find the desired and the original values of the bit */
	i = (flag != 0);
	j = ((*p & mask) != 0);

	/* ready to modify the bit and write back (if necessary) */
	if (i != j)
		{
		*p ^= mask;
		bfree(error, blkbuf_p, 1);
		}
	else
		{
		bfree(error, blkbuf_p, 0);
		}

#if DEBUG >= 3
	_dprintf(3, _("bitmap_set() returning %u\n"), (unsigned int)j);
#endif
	return (unsigned char)j;
	}
#endif

