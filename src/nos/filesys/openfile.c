/*
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 Filesystem related routines
**********************************************************/

#include <errno.h>
#include <fcntl.h> /* for O_RDWR etc */
#include "driver/device.h" /* for device_find(), struct device_s */
#include "kernel/dprintf.h"
#include "filesys/filesys.h" /* for Root_dev */
#include "filesys/filename.h" /* for namei(), n_creat() */
#include "kernel/object.h" /* for object_dec_refs() */
#include "filesys/openfile.h"
#include "kernel/valadr.h"
#include <libintl.h>
#include "po/messages.h"

#ifndef DEBUG
#define DEBUG 0
#endif

#ifndef MODULE
#define MODULE_open_file_list
#define MODULE_of_alloc
#define MODULE_of_deref
#define MODULE_of_truncate
#define MODULE_of_garbage
#define MODULE_of_locate
#define MODULE_of_open
#define MODULE_of_lseek
#define MODULE_of_falign
#define MODULE_of_pipe

#endif

#ifdef MODULE_open_file_list
struct open_file_s *open_file_list[OPEN_FILE_LIST];
#endif

/* of_alloc() allocate entries in the open file table.
 */

#ifdef MODULE_of_alloc
struct open_file_s *
of_alloc(jmp_buf error)
	{
	struct open_file_s *of_p, **of_pp;

#if DEBUG >= 3
	_dprintf(3, _("of_alloc(0x%x) starting\n"), error);
#endif
	for (of_pp = open_file_list; of_pp < open_file_list + OPEN_FILE_LIST;
			of_pp++)
		{
		of_p = *of_pp;
		if (of_p && of_p->object.refs == 0)
			{
			memset(of_p, 0, sizeof(struct open_file_s));
			of_p->object.busy = 1;
			of_p->object.refs = 1;
#if DEBUG >= 3
			_dprintf(3, _("of_alloc() returning 0x%x, reuse\n"),
					of_p);
#endif
			return of_p;
			}
		}

	for (of_pp = open_file_list; of_pp < open_file_list + OPEN_FILE_LIST;
			of_pp++)
		{
		of_p = *of_pp;
		if (of_p == NULL)
			{
			of_p = calloc(sizeof(struct open_file_s), 1);
			if (of_p == NULL)
				{
#if DEBUG >= 3
				_dprintf(3, _("of_alloc() error ENOMEM\n"));
#endif
				longjmp(error, ENOMEM);
				}
			of_p->object.busy = 1;
			of_p->object.refs = 1;
			*of_pp = of_p;
#if DEBUG >= 3
			_dprintf(3, _("of_alloc() returning 0x%x, new\n"),
					of_p);
#endif
			return of_p;
			}
		}

#if DEBUG >= 3
	_dprintf(3, _("of_alloc() error ENFILE\n"));
#endif
	longjmp(error, ENFILE);
	}
#endif

/* of_deref() dereference (and possibly free attached inode)
 * entries in the open file table.
 */

#ifdef MODULE_of_deref
void
of_deref(struct open_file_s *of_p)
	{
	jmp_buf try_b;
	unsigned int catch;
	dev_t dev;
	struct core_inode_s *cinode_p;

#if DEBUG >= 3
	_dprintf(3, _("of_deref(0x%x) starting\n"), of_p);
#endif
	if (object_dec_refs(&of_p->object)) /* last reference? */
		{
		cinode_p = of_p->o_inode;
		if (cinode_p)
			{
			/* of_p->o_inode = NULL; */
			i_acquire(cinode_p);
			if (isdevice(cinode_p)
#if 1
					&& cinode_p->c_refs == 1
#endif
					)
				{
				dev = DEVNUM(cinode_p);
				if (setjmp(try_b) == 0)
					{
					device_find(try_b, dev)->close(try_b,
							MINOR(dev));
					}
				}
			i_release(cinode_p);
			i_deref(cinode_p);
			}
		/* object_release(&of_p->object); */
		}
#if DEBUG >= 3
	_dprintf(3, _("of_deref() returning, refs %u\n"), of_p->object.refs);
#endif
	}
#endif

/* truncate the file, if a regular one, to zero length */
/* note: the caller must call object_acquire() / object_release() */

#ifdef MODULE_of_truncate
void
of_truncate(struct open_file_s *of_p)
	{
	struct core_inode_s *cinode_p;

	cinode_p = of_p->o_inode;
	i_acquire(cinode_p);
	/* regular file? (or XIP aligned) */
	if (S_ISREGALIGN(cinode_p->c_node.i_mode))
		{
		/* Truncate the regular file to zero length */
		cinode_p->c_node.i_size = 0; /* for future use */
		f_trunc(cinode_p);
		}
	i_release(cinode_p);

	of_p->o_ptr = 0; /* reset only this oft pointer */
	}
#endif

#ifdef MODULE_of_garbage
/* open_file_list garbage collection - called by storage allocator when free
 * space runs low.  Zaps open file table entries with 0 reference count.
 */
void
of_garbage(int red)
	{
	struct open_file_s **of_pp, *of_p;

	for (of_pp = open_file_list; of_pp < open_file_list + OPEN_FILE_LIST;
			of_pp++)
		{
		of_p = *of_pp;
		if (of_p && of_p->object.refs == 0)
			{
			*of_pp = NULL;
			free(of_p);
			}
		}
	}
#endif

/* of_locate() returns the oft pointer associated with a user's
 * file descriptor, checking that it's a file and not a socket
 */

#ifdef MODULE_of_locate
struct open_file_s *
of_locate(jmp_buf error, unsigned int fd)
	{
	struct object_s *object_p;

#if DEBUG >= 3
	_dprintf(3, _("of_locate(0x%x, %u) starting\n"), error, fd);
#endif
	object_p = uf_locate(error, fd);
	if (object_p->type != _FL_FILE)
		{
		object_release(object_p);
#if DEBUG >= 3
		_dprintf(3, _("of_locate() error ESPIPE\n"));
#endif
		longjmp(error, ESPIPE);
		}
#if DEBUG >= 3
	_dprintf(3, _("of_locate() returning 0x%x\n"), object_p);
#endif
	return (struct open_file_s *)object_p;
	}
#endif

#ifdef MODULE_of_open
unsigned int
of_open(jmp_buf error, unsigned char *path, unsigned int flag, mode_t mode)
	{
	jmp_buf try_b;
	unsigned int catch;
	dev_t dev;
	struct open_file_s *oft_p;
	struct core_inode_s *ino;
	int spf;
	unsigned char perm;
	unsigned int fd;

	spf = flag;
	flag &= 0xff; /* special flags in high byte */
	if (flag > O_RDWR)
		{
		longjmp(error, EINVAL);
		}

	if (spf & O_SYMLINK)
		{
		spf = O_SYMLINK;
		}

	oft_p = of_alloc(error);
	oft_p->object.type = _FL_FILE;

	if (spf & O_CREAT)
		{
		/* creation of new file is allowed */
		if (spf & O_NEW)
			{
			/* create new file, it must not exist already */
			catch = setjmp(try_b);
			if (catch)
				{
				goto error1;
				}

			/* open inode for path */
			mode = S_IFREG | (mode & S_UMASK &
					~my_thread_p->process_p->p_umask);

			ino = n_creat(try_b, path, 1, mode);
			}
		else
			{
			/* create new file, or truncate if it already exists */
			catch = setjmp(try_b);
			if (catch)
				{
				/* it does not already exist */
				if (catch != ENOENT)
					{
					goto error1;
					}

				catch = setjmp(try_b);
				if (catch)
					{
					goto error1;
					}

				/* open inode for path */
				mode = S_IFREG | (mode & S_UMASK &
						~my_thread_p->process_p->
						p_umask);

				ino = n_creat(try_b, path, 0, mode);
				}
			else
				{
				/* in this case should use **parent to make */
				/* the possible creation step more efficient */
				ino = namei(try_b, path, NULL,
						(spf & O_SYMLINK) == 0);
				}
			}
		}
	else
		{
		/* creation of new file is not allowed */
		catch = setjmp(try_b);
		if (catch)
			{
			goto error1;
			}

		ino = namei(try_b, path, NULL, (spf & O_SYMLINK) == 0);
		}

	catch = setjmp(try_b);
	if (catch)
		{
		goto error2;
		}

	perm = getperm(ino);			/* check for permissions */
	if (((((unsigned char)flag == O_RDONLY) ||
			(unsigned char)flag == O_RDWR) &&
			!((unsigned char)perm & S_IOREAD)) ||
	    (((unsigned char)flag == O_WRONLY ||
			(unsigned char)flag == O_RDWR) &&
			!((unsigned char)perm & S_IOWRITE)))
		{
		longjmp(try_b, EPERM);
		}
	if (((unsigned char)flag == O_WRONLY ||
			(unsigned char)flag == O_RDWR) && (ino->c_ro))
		{
		longjmp(try_b, EROFS);
		}
	if ((spf & O_SYMLINK) && getmode(ino) != S_IFLNK)
		{
		longjmp(try_b, ESRCH);
		}
	if (getmode(ino) == S_IFDIR && flag != O_RDONLY)
		{ /* directories only for read */
		longjmp(try_b, EISDIR);
		}

	i_acquire(ino);

	catch = setjmp(try_b);
	if (catch)
		{
		goto error3;
		}

	if (isdevice(ino)
#if 1 /* Nick */
			&& ino->c_refs == 1
#endif
			)
		{
		dev = DEVNUM(ino);
		device_find(try_b, dev)->open(try_b, MINOR(dev));
		}

	catch = setjmp(try_b);
	if (catch)
		{
		if (setjmp(try_b) == 0)
			{
			device_find(try_b, dev)->close(try_b, MINOR(dev));
			}
	error3:
		i_release(ino);
	error2:
		i_deref(ino);
	error1:
		object_release(&oft_p->object);
		of_deref(oft_p);
		longjmp(error, catch);
		}

	fd = uf_alloc(try_b);

	/* can't fail now */
	oft_p->o_inode = ino;
	my_thread_p->process_p->object_p[fd] = &oft_p->object;
	if (spf & O_APPEND)
		{			/* posit to end of file */
		oft_p->o_ptr = ino->c_node.i_size;
		}
	else
		{
		oft_p->o_ptr = 0;	/* posit to start of file */
		}

	i_release(ino);

	/* creat must truncate file to 0 if it exists! */
	if ((spf & (O_CREAT|O_TRUNC)) == (O_CREAT|O_TRUNC))
		{
		/* hey! what if it was O_APPEND with O_TRUNC ?? */
		of_truncate(oft_p);
		}

	oft_p->o_access = flag;
	if (getmode(ino) == S_IFPIPE && oft_p->object.refs == 1)
		{
		object_release(&oft_p->object);
		kwait(ino);	/* sleep process if no writer of reader */
		}
	else
		{
		object_release(&oft_p->object);
		}

	return fd;
	}
#endif

#ifdef MODULE_of_lseek
unsigned long
of_lseek(jmp_buf error, unsigned int fd, unsigned long position,
		unsigned int flag)
	{
	jmp_buf try_b;
	unsigned int catch;
	struct open_file_s *oft_p;
	struct core_inode_s *cinode_p;

	oft_p = of_locate(error, fd);

	catch = setjmp(try_b);
	if (catch)
		{
		object_release(&oft_p->object);
		longjmp(error, catch);
		}

	cinode_p = oft_p->o_inode;
	if (getmode(cinode_p) == S_IFPIPE)
		{
		longjmp(try_b, ESPIPE);
		}

	switch (flag)
		{
	case SEEK_SET:
		break;
	case SEEK_CUR:
		position += oft_p->o_ptr;
		break;
	case SEEK_END:
		i_acquire(cinode_p);
		position += cinode_p->c_node.i_size;
		i_release(cinode_p);
		break;
	default:
		longjmp(try_b, EINVAL);
		}

	oft_p->o_ptr = position;
	object_release(&oft_p->object);

	return position;
	}
#endif

#ifdef MODULE_of_falign
void
of_falign(jmp_buf error, unsigned int fd, unsigned int flag)
	{
	jmp_buf try_b;
	unsigned int catch;
	struct open_file_s *oft_p;
	struct core_inode_s *cinode_p;

	oft_p = of_locate(error, fd);

	catch = setjmp(try_b);
	if (catch)
		{
		object_release(&oft_p->object);
		longjmp(error, catch);
		}

	switch (flag)
		{
	case XIP_ALIGN:
		xip_align(try_b, oft_p->o_inode);
		break;

	case XIP_UALIGN:
		xip_ualign(try_b, oft_p->o_inode);
		break;

	default:
		longjmp(try_b, EINVAL);
		}

	object_release(&oft_p->object);
	}
#endif

#ifdef MODULE_of_pipe
void
of_pipe(jmp_buf error, unsigned int *fd_p)
	{
	jmp_buf try_b;
	unsigned int catch;
	unsigned int fd1, fd2;
	struct open_file_s *oft_p1, *oft_p2;
	struct core_inode_s *cinode_p;

	valadr(error, fd_p, 2 * sizeof(unsigned int));
	cinode_p = i_open(error, Root_dev, NULLINO);

	catch = setjmp(try_b);
	if (catch)
		{
		i_deref(cinode_p);
		goto error1;
		}

	oft_p1 = of_alloc(try_b);
	oft_p1->object.type = _FL_FILE;
	oft_p1->o_access = O_RDONLY;
	oft_p1->o_ptr = 0;
	oft_p1->o_inode = cinode_p;

	object_release(&oft_p1->object);

	catch = setjmp(try_b);
	if (catch)
		{
		goto error2;
		}

	fd1 = uf_alloc(try_b);
	my_thread_p->process_p->object_p[fd1] = &oft_p1->object;

	catch = setjmp(try_b);
	if (catch)
		{
		goto error3;
		}

	oft_p2 = of_alloc(try_b);
	oft_p2->object.type = _FL_FILE;
	oft_p2->o_access = O_WRONLY;
	oft_p2->o_ptr = 0;
	oft_p2->o_inode = cinode_p;

	object_release(&oft_p2->object);
	i_ref(cinode_p);

	catch = setjmp(try_b);
	if (catch)
		{
		of_deref(oft_p2); /* calls i_deref() */
	error3:
		my_thread_p->process_p->object_p[fd1] = NULL;
	error2:
		of_deref(oft_p1); /* calls i_deref() */
	error1:
		longjmp(error, catch);
		}

	fd2 = uf_alloc(try_b); /* can't fail now */
	my_thread_p->process_p->object_p[fd2] = &oft_p2->object;

	/* No permissions necessary on pipes */
	i_acquire(cinode_p);
	cinode_p->c_node.i_mode = S_IFPIPE | 0777;
	cinode_p->c_node.i_nlink = 0; /* a pipe is not in any directory */
	i_release(cinode_p);

	usrput_unsigned_int(fd_p, fd1);
	usrput_unsigned_int(fd_p + 1, fd2);
	}
#endif

