/* xip.c by Nick for NOS/UZI project
 *
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 Filesystem related routines for XIP system
**********************************************************/

#include <errno.h>
#include <sys/stat.h> /* for S_IFMT and others */
#include "filesys/xip.h"
#include "kernel/process.h"
#include "filesys/maxmin.h"
#include "kernel/dprintf.h"
#include "filesys/bitmap.h"
#include <libintl.h>
#include "po/messages.h"

#ifndef DEBUG
#define DEBUG 0
#endif

#ifndef MODULE
#define MODULE_xip_align
#define MODULE_xip_ualign
#define MODULE_xip_examine
#define MODULE_xip_align_chase
#define MODULE_xip_align_bmap
#define MODULE_xip_align_reverse
#define MODULE_xip_align_recurse
#endif

#ifdef UTIL /* rather a hack indeed */
#define	EISALIGN	42		/* 42 File is already aligned */
#define	ENOALIGN	43		/* 43 File is not aligned */
#define ETOOSHORT	44		/* 44 File is too short, or hole */
#define ENOREGALIGN	45		/* 42 Not a regular or aligned file */
#define	ENOLCKFS	46		/* 45 Not a locking filesystem */
#define ECORRUPTFS	47		/* 46 Filesystem is corrupt */
#endif

#if 0
/* Returns true if the magic number of a superblock is ok ??? not used */
#define gooddev(dev)		((dev)->s_mounted == SMOUNTED)

#define blockinodes(blk)	((blk) << DINODESPERBLOCKLOG)
#define devinodes(dev)		blockinodes((dev)->s_isize)
#define inodeblock(dev,ino)	(((ino) >> DINODESPERBLOCKLOG) + (dev)->s_reserv)
#define inodeoffset(ino)	((ino) & DINODESPERBLOCKMASK)
#endif

#ifdef MODULE_xip_align
void
xip_align(jmp_buf error, struct core_inode_s *ino)
	{
	jmp_buf try_b;
	unsigned int catch;
	blkno_t i, j, k;
	blkno_t blk, pos, newno;
	blkno_t *region, regions, blocks;
	struct file_system_s *dev;
	dev_t devno;
	struct core_inode_s *other;
	int flag, exclude, indirection;
	unsigned long size;

#if DEBUG >= 3
	_dprintf(3, _("xip_align(0x%x, 0x%x) starting\n"), error, ino);
#endif
	/* prepare to access the given inode */
	i_acquire(ino);
	devno = ino->c_dev;
	dev = getfs(devno);

	if (dev->s_bitmap_immov >= dev->s_bitmap_final)
		{
		i_release(ino);
#if DEBUG >= 3
		_dprintf(3, _("xip_align() error ENOLCKFS\n"));
#endif
		longjmp(error, ENOLCKFS);
		}

	/* validate the filetype, it must be non-aligned */
	if ((ino->c_node.i_mode & S_IFMT) == S_IFALIGN)
		{
		i_release(ino);
#if DEBUG >= 3
		_dprintf(3, _("xip_align() error EISALIGN\n"));
#endif
		longjmp(error, EISALIGN);
		}
	if ((ino->c_node.i_mode & S_IFMT) != S_IFREG)
		{
		i_release(ino);
#if DEBUG >= 3
		_dprintf(3, _("xip_align() error ENOREGALIGN\n"));
#endif
		longjmp(error, ENOREGALIGN);
		}

	/* ensure the region list will fit in 512 bytes */
	size = ino->c_node.i_size;
	regions = (size + REGION_BYTES - 1) >> REGION_LOG;
	if (regions > (BUFSIZE / sizeof(blkno_t)))
		{
		i_release(ino);
#if DEBUG >= 3
		_dprintf(3, _("xip_align() error EFBIG\n"));
#endif
		longjmp(error, EFBIG);
		}

	/* ready to allocate the region list */
	region = (blkno_t *)malloc(BUFSIZE);
	if (region == NULL)
		{
		i_release(ino);
#if DEBUG >= 3
		_dprintf(3, _("xip_align() error ENOMEM\n"));
#endif
		longjmp(error, ENOMEM);
		}
	memset(region, 0, BUFSIZE);

	catch = setjmp(try_b);
	if (catch)
		{
		i_release(ino);
		goto xip_align_error;
		}

	/* calculate size in blocks, to resolve size of last region */
	blocks = (size + BUFSIZE - 1) >> BUFSIZELOG;

	/* allocate each region its physical memory address */
	pos = 0;
	for (i = 0; i < regions; i++)
		{
		/* is this a full region, or the last partial region? */
		k = min_uint(blocks - pos, REGION_BLOCKS);

		/* try to find contiguous aligned non-reserved blocks */
		j = bitmap_search(try_b, devno, k, dev->s_bitmap_immov,
				dev->s_bitmap_final);

		/* found a suitable spot for the region, snarf it */
		region[i] = j;
		bitmap_reserve(try_b, devno, j, k, 1, dev->s_bitmap_immov,
				dev->s_bitmap_final);

		/* calculate file position for start of next region */
		pos += REGION_BLOCKS;
		}

#if DEBUG > 1
	_dprintf(2, _("processing my inode %u\n"), ino->c_num);
#endif

	/* move the file's data into unoccupied blocks of each region */
	pos = 0;
	for (i = 0; i < regions; i++)
		{
		/* is this a full region, or the last partial region? */
		k = min_uint(blocks - pos, REGION_BLOCKS);

		/* loop through each block of the region individually */
		blk = region[i];
		for (j = 0; j < k; j++)
			{
			/* see if the target spot for this block is occupied */
			flag = bitmap_set(try_b, devno, blk, 1,
					dev->s_bitmap_block,
					dev->s_bitmap_immov);
			if (flag == 0)
				{
				/* no, move something in and possibly repeat */
				newno = xip_align_chase(try_b, dev, ino, blk,
						pos, region, regions, blocks);

				/* eventually a non-target block was freed */
				if (bitmap_set(try_b, devno, newno, 0,
						dev->s_bitmap_block,
						dev->s_bitmap_immov) != 1)
					{
					_dprintf(0, _("xip_align() corrupt filesystem a\n"));
					longjmp(try_b, ECORRUPTFS);
					}
				}

			blk++;
			pos++;
			}
		}
	i_release(ino);

	/* now, if any blocks remain to be swapped, traverse the entire */
	/* filesystem to find them, updating the filesystem as we move them */
	k = devinodes(dev);
	for (j = ROOTINODE; j < k; j++)
		{
		catch = setjmp(try_b);
		if (catch)
			{
			goto xip_align_error;
			}

		/* scan inode bitmap via utility routine, looking for '1' */
		j = bitmap_find(try_b, devno, j, 1, 0, dev->s_bitmap_inode,
				dev->s_bitmap_block);
		if (j < ROOTINODE || j >= k)
			{
			break; /* not found, means no more inodes to search */
			}

		other = i_open(try_b, devno, j);
		i_acquire(other);

#if DEBUG > 1
		_dprintf(2, _("processing other inode %u\n"), other->c_num);
#endif

		catch = setjmp(try_b);
		if (catch)
			{
			i_release(other);
			i_deref(other);
			goto xip_align_error;
			}

		/* check mode */
		flag = other->c_node.i_mode & S_IFMT;
		if (flag != S_IFALIGN && flag != S_IFREG &&
				flag != S_IFDIR && flag != S_IFLNK &&
				flag != S_IFBLK && flag != S_IFCHR)
			{
			i_release(other);
			i_deref(other);
			continue; /* inode's mode is bad, skip it, continue */
			}

		/* check blocks */
		if (flag == S_IFALIGN || flag == S_IFREG ||
				flag == S_IFDIR || flag == S_IFLNK)
			{
			/*pos = 0;*/
			/*inc = 1;*/
			indirection = 0;
			exclude = (ino == other) ? 2 : (flag == S_IFALIGN);
#if 1 /* under construction */
			for (i = ((exclude == 1) ? DIRECTBLOCKS : 0);
#else
			for (i = (exclude ? DIRECTBLOCKS : 0);
#endif
					i <= DIRECTBLOCKS+INDIRECTBLOCKS; i++)
				{
				if (i == DIRECTBLOCKS)
					{
					/*inc = BUFSIZE / sizeof(blkno_t);*/
					indirection = 1;
					}
				else if (i == DIRECTBLOCKS+INDIRECTBLOCKS)
					{
					/* inc doesn't matter in this case */
					indirection = 2;
					}
				blk = other->c_node.i_addr[i];
				if (blk)
					{
					pos = xip_align_recurse(try_b, dev,
							ino, exclude,
							&other->c_node.i_addr[i],
							&other->c_dirty, indirection,
							region, regions, blocks);

					/* move something into the freed spot, and possibly repeat */
					if (pos != (blkno_t)-1)
						{
						blk = xip_align_chase(try_b,
								dev, ino,
								blk, pos,
								region,
								regions,
								blocks);

						/* eventually a non-target block was freed, indicate this */
						if (bitmap_set(try_b, devno,
								blk, 0,
								dev->s_bitmap_block,
								dev->s_bitmap_immov)
								!= 1)
							{
							_dprintf(0, _("xip_align() corrupt filesystem b\n"));
							longjmp(try_b, ECORRUPTFS);
							}
						}
					}
				/*pos += inc;*/
				}
			}

		i_release(other);
		i_deref(other);
		}

	free(region);

	i_acquire(ino);
	ino->c_node.i_mode = (ino->c_node.i_mode & ~S_IFMT) | S_IFALIGN;
	ino->c_dirty = 2;
	i_release(ino);

	i_sync(ino->c_dev); /* write back all inodes for dev */
	bufsync(ino->c_dev, 0); /* write back all buffers for dev */
#if DEBUG >= 3
	_dprintf(3, _("xip_align() returning\n"));
#endif
	return;

xip_align_error:
	/* politely unreserve all regions reserved so far */
	for (i = 0; i < regions; i++)
		{
		/* did error occur partway through filling the list? */
		if (region[i] == 0)
			{
			break;
			}

		/* in this case always unreserve a full sized region */
		if (setjmp(try_b) == 0)
			{
			bitmap_reserve(try_b, devno, region[i],
				       REGION_BLOCKS, 0,
				       dev->s_bitmap_immov,
				       dev->s_bitmap_final);
			}
		}

	free(region);
#if DEBUG >= 3
	_dprintf(3, _("xip_align() error %u\n"), catch);
#endif
	longjmp(error, catch);
	}
#endif

#ifdef MODULE_xip_ualign
void
xip_ualign(jmp_buf error, struct core_inode_s *ino)
	{
	jmp_buf try_b;
	unsigned int catch;
	blkno_t i, j, pos;
	blkno_t *region, regions, blocks;
	struct file_system_s *dev;
	dev_t devno;
	unsigned long size;

#if DEBUG >= 3
	_dprintf(3, _("xip_ualign(0x%x, 0x%x) starting\n"), error, ino);
#endif
	/* prepare to access the given inode */
	i_acquire(ino);
	devno = ino->c_dev;
	dev = getfs(devno);

	catch = setjmp(try_b);
	if (catch)
		{
		i_release(ino);
#if DEBUG >= 3
		_dprintf(3, _("xip_ualign() error %u\n"), catch);
#endif
		longjmp(error, catch);
		}

	/* construct region list using subroutine shared with execve() */
	size = ino->c_node.i_size;
	region = xip_examine(try_b, dev, ino, size, &regions, &blocks);
	i_release(ino);

	/* unreserve all regions using the list */
	pos = 0;
	for (i = 0; i < regions; i++)
		{
		/* is this a full region, or the last partial region? */
		j = min_uint(blocks - pos, REGION_BLOCKS);

		/* unreserve the required number of blocks for region */
		if (setjmp(try_b) == 0)
			{
			bitmap_reserve(try_b, devno, region[i], j, 0,
					  dev->s_bitmap_immov,
					  dev->s_bitmap_final);
			}

		pos += REGION_BLOCKS;
		}

	free(region);

	i_acquire(ino);
	ino->c_node.i_mode = (ino->c_node.i_mode & ~S_IFMT) | S_IFREG;
	ino->c_dirty = 2;
	i_release(ino);

	i_sync(ino->c_dev); /* write back all inodes for dev */
	bufsync(ino->c_dev, 0); /* write back all buffers for dev */
#if DEBUG >= 3
	_dprintf(3, _("xip_ualign() returning\n"));
#endif
	}
#endif

#ifdef MODULE_xip_examine
blkno_t *
xip_examine(jmp_buf error, struct file_system_s *dev, struct core_inode_s *ino, off_t size,
		blkno_t *regions_p, blkno_t *blocsession_p)
	{
	jmp_buf try_b;
	unsigned int catch;
	blkno_t i, j, k;
	blkno_t blk, pos, /*inc,*/ newno;
	blkno_t *region, regions, blocks;

#if DEBUG >= 3
	_dprintf(3, _("xip_examine(0x%x, 0x%x, 0x%x, %ld, 0x%x, 0x%x) "
			"starting\n"), error, dev, ino, size, regions_p,
			blocsession_p);
#endif
	if (dev->s_bitmap_immov >= dev->s_bitmap_final)
		{
#if DEBUG >= 3
		_dprintf(3, _("xip_examine() error ENOLCKFS\n"));
#endif
		longjmp(error, ENOLCKFS);
		}

	/* validate the filetype, it must be aligned */
	if ((ino->c_node.i_mode & S_IFMT) == S_IFREG)
		{
#if DEBUG >= 3
		_dprintf(3, _("xip_examine() error ENOALIGN\n"));
#endif
		longjmp(error, ENOALIGN);
		}
	if ((ino->c_node.i_mode & S_IFMT) != S_IFALIGN)
		{
#if DEBUG >= 3
		_dprintf(3, _("xip_examine() error ENOREGALIGN\n"));
#endif
		longjmp(error, ENOREGALIGN);
		}

	/* ensure the region list will fit in 512 bytes */
	regions = (size + REGION_BYTES - 1) >> REGION_LOG;
	if (regions > (BUFSIZE / sizeof(blkno_t)))
		{
#if DEBUG >= 3
		_dprintf(3, _("xip_examine() error EFBIG\n"));
#endif
		longjmp(error, EFBIG);
		}

	/* ready to allocate the region list */
	region = (blkno_t *)malloc(BUFSIZE);
	if (region == NULL)
		{
#if DEBUG >= 3
		_dprintf(3, _("xip_examine() error ENOMEM\n"));
#endif
		longjmp(error, ENOMEM);
		}

	catch = setjmp(try_b);
	if (catch)
		{
		free(region);
#if DEBUG >= 3
		_dprintf(3, _("xip_examine() error %u\n"), catch);
#endif
		longjmp(error, catch);
		}

	/* calculate size in blocks, to resolve size of last region */
	blocks = (size + BUFSIZE - 1) >> BUFSIZELOG;

	/* find each region's existing physical memory address */
	pos = 0;
	for (i = 0; i < regions; i++)
		{
		/* is this a full region, or the last partial region? */
		k = min_uint(blocks - pos, REGION_BLOCKS);

		/* first block must be aligned, save its position in list */
		blk = bmap(try_b, ino, pos, 1);
		if ((blk & (PAGE_BLOCKS-1)) || blk == NULLBLK)
			{
			/* alignment bad */
			longjmp(try_b, EBADALIGN);
			}
		region[i] = blk;
		pos++;

		/* try to find and skip contiguous aligned blocks */
		for (j = 1; j < k; j++)
			{
			blk++;
			newno = bmap(try_b, ino, pos, 1);
			if (newno != blk)
				{
				/* alignment bad */
				longjmp(try_b, EBADALIGN);
				}
			pos++;
			}
		}

	*regions_p = regions;
	*blocsession_p = blocks;
#if DEBUG >= 3
 _dprintf(3, _("xip_examine() returning 0x%x (%u, %u)\n"), region, regions, blocks);
#endif
	return region;
	}
#endif

#ifdef MODULE_xip_align_chase
blkno_t
xip_align_chase(jmp_buf error, struct file_system_s *dev, struct core_inode_s *ino, blkno_t blk,
		blkno_t pos, blkno_t *region, blkno_t regions, blkno_t blocks)
	{
#if DEBUG >= 3
	_dprintf(3, _("xip_align_chase"
			"(0x%x, 0x%x, 0x%x, %u, %u, 0x%x, %u, %u) starting\n"),
			error, dev, ino, blk, pos, region, regions, blocks);
#endif
	/* check we have been passed a valid block number */
	if (blk < (dev->s_reserv + dev->s_isize) || blk >= dev->s_fsize)
		{
#if DEBUG >= 3
		_dprintf(3, _("xip_align_chase() error ECORRUPTFS a\n"));
#endif
		longjmp(error, ECORRUPTFS);
		}

	/* infinite loop to chase the just-freed block around the file */
	while (1)
		{
		/* find corresponding block in allocation table for ino */
		blk = xip_align_bmap(error, ino, blk, pos);

		/* check we have been passed a valid block number */
		if (blk < (dev->s_reserv + dev->s_isize) ||
				blk >= dev->s_fsize)
			{
#if DEBUG >= 3
			_dprintf(3, _("xip_align_chase() error ECORRUPTFS b\n"));
#endif
			longjmp(error, ECORRUPTFS);
			}

		/* see if the just-freed block was part of the target file */
		pos = xip_align_reverse(blk, region, regions, blocks);
		if (pos == (blkno_t)-1)
			{
#if DEBUG >= 3
			_dprintf(3, _("xip_align_chase() returning %u\n"),
					blk);
#endif
			return blk; /* success, return the just-freed block */
			}

		/* yes, we can move something into the just-freed block */
		}
	}
#endif

#ifdef MODULE_xip_align_bmap
blkno_t
xip_align_bmap(jmp_buf error, struct core_inode_s *ino, blkno_t newno, blkno_t pos)
	{
	jmp_buf try_b;
	unsigned int catch;
	dev_t devno;
	int i, j, sh;
	blkno_t blk;
	struct block_buffer_s *blkbuf_p, *other_blkbuf_p;

#if DEBUG >= 3
	_dprintf(3, _("xip_align_bmap(0x%x, 0x%x, %u) starting\n"),
			error, ino, pos);
#endif
	devno = ino->c_dev;

	/* blocks 0..DIRECTBLOCKS-1 are direct blocks */
	if (pos < DIRECTBLOCKS)
		{
		blk = ino->c_node.i_addr[pos];
		if (blk == 0)
			{
			/* hole in file */
#if DEBUG >= 3
			_dprintf(3, _("xip_align_bmap() error ETOOSHORT a\n"));
#endif
			longjmp(error, ETOOSHORT);
			}

		ino->c_node.i_addr[pos] = newno; /* move the block! */
		ino->c_dirty = 2;
		goto Ok;
		}

	/* addresses DIRECTBLOCKS and DIRECTBLOCKS+1 have single and double
	 * indirect blocks.
	 * The first step is to determine how many levels of indirection.
	 */
	pos -= DIRECTBLOCKS;
	sh = 0;
	j = INDIRECTBLOCKS+DINDIRECTBLOCKS;
	if (pos >= BUFSIZE / sizeof(blkno_t))	/* double indirect */
		{
		sh = 8; 			/* shift by 8 */
		pos -= BUFSIZE / sizeof(blkno_t);
		j -= INDIRECTBLOCKS;
		}

	/* fetch the address from the inode.
	 * Create the first indirect block if needed.
	 */
	blk = ino->c_node.i_addr[TOTALREFBLOCKS - j];
	if (blk == 0)
		{
		/* hole in file */
#if DEBUG >= 3
		_dprintf(3, _("xip_align_bmap() error ETOOSHORT b\n"));
#endif
		longjmp(error, ETOOSHORT);
		}

	/* fetch through the indirect blocks */
	while (j < INDIRECTBLOCKS+DINDIRECTBLOCKS) /* Nick <= */
		{
		blkbuf_p = bread(error, devno, blk, 0);

		i = (pos >> sh) & (BUFSIZE / sizeof(blkno_t) - 1);
		blk = ((blkno_t *)blkbuf_p->bf_data)[i];

		bfree(error, blkbuf_p, 0);

		if (blk == 0)
			{
			/* hole in file */
#if DEBUG >= 3
			_dprintf(3, _("xip_align_bmap() error ETOOSHORT c\n"));
#endif
			longjmp(error, ETOOSHORT);
			}
		sh -= 8;
		++j;
		}

	blkbuf_p = bread(error, devno, blk, 0);

	i = (pos >> sh) & (BUFSIZE / sizeof(blkno_t) - 1);
	blk = ((blkno_t *)blkbuf_p->bf_data)[i];
	if (blk == 0)
		{
		/* hole in file */
		if (setjmp(try_b) == 0)
			{
			bfree(try_b, blkbuf_p, 0);
			}
#if DEBUG >= 3
		_dprintf(3, _("xip_align_bmap() error ETOOSHORT d\n"));
#endif
		longjmp(error, ETOOSHORT);
		}

	((blkno_t *)blkbuf_p->bf_data)[i] = newno; /* move the block! */
	bfree(error, blkbuf_p, 1);

Ok:
	/* read the block, as we have to move the data also */
	blkbuf_p = bread(error, devno, blk, 0);

#if DEBUG >= 2
	_dprintf(2, _("getting block %u -> %u\n"), blk, newno);
#endif

	/* really move the block by manipulating cache entries */
	other_blkbuf_p = bfind(devno, newno); /* hanging around in cache? */
	if (other_blkbuf_p)
		{
		other_blkbuf_p->bf_blk = blk; /* swap, for minimal overhead */
		other_blkbuf_p->bf_dirty = 0; /* try to avoid writing back */
		other_blkbuf_p->bf_busy = 0; /* no need to call blk_release() */
		/* because ksignal(Bufpool, 0) is done in bfree() */
		}

	/* revise the original cache entry to show the new block */
	blkbuf_p->bf_blk = newno; /* original value is in variable blk */
	bfree(error, blkbuf_p, 1);

#if DEBUG >= 3
	_dprintf(3, _("xip_align_bmap() returning %u\n"), blk);
#endif
	return blk;
	}
#endif

#ifdef MODULE_xip_align_reverse
blkno_t
xip_align_reverse(blkno_t blk, blkno_t *region, blkno_t regions,
		blkno_t blocks)
	{
	blkno_t i, k;
	blkno_t pos;

#if DEBUG > 3
	_dprintf(4, _("xip_align_reverse(%u, 0x%x, %u, %u) starting\n"),
			blk, region, regions, blocks);
#endif
	/* perform a reverse lookup of blk via the region list */
	pos = 0;
	for (i = 0; i < regions; i++)
		{
		/* is this a full region, or the last partial region? */
		k = min_uint(blocks - pos, REGION_BLOCKS);

		/* see if the given blk falls within this region */
		if (blk >= region[i] && blk < (region[i] + k))
			{
			/* yes, return the computed logical pos in file */
			pos += blk - region[i];
#if DEBUG > 3
			_dprintf(4, _("xip_align_reverse() "
					"returning %u, found\n"), pos);
#endif
			return pos;
			}

		/* calculate file position for start of next region */
		pos += REGION_BLOCKS;
		}

	/* the given blk was not found in any region of the list */
#if DEBUG > 3
	_dprintf(4, _("xip_align_reverse() returning -1, not found\n"));
#endif
	return -1;
	}
#endif

#ifdef MODULE_xip_align_recurse
blkno_t
xip_align_recurse(jmp_buf error, struct file_system_s *dev, struct core_inode_s *ino,
		unsigned char exclude, blkno_t *parent, char *dirty,
		unsigned char indirection, blkno_t *region, blkno_t regions,
		blkno_t blocks)
	{
	jmp_buf try_b;
	unsigned int catch;
	dev_t devno;
	int i, reserved;
	blkno_t blk, newno;
	struct block_buffer_s *blkbuf_p, *other_blkbuf_p;
	int moveable;
	blkno_t tempblk;
	char tempdirty;
	blkno_t pos, chase;

#if DEBUG > 3
	_dprintf(4, _("xip_align_recurse"
			"(0x%x, 0x%x, 0x%x, %u, 0x%x, 0x%x, %u, 0x%x, %u, %u) "
			"starting\n"), error, dev, ino, (unsigned int)exclude,
			parent, dirty, (unsigned int)indirection, region,
			regions, blocks);
#endif
	/* check we have been passed a valid block number */
	blk = *parent;
	if (blk < (dev->s_reserv + dev->s_isize) || blk >= dev->s_fsize)
		{
		/* filesystem is corrupt, invalid block */
#if DEBUG >= 3
		_dprintf(3, _("xip_align_recurse() error ECORRUPTFS\n"));
#endif
		longjmp(error, ECORRUPTFS);
		}

	/* see if this block is intended for the target file */
#if 0 /* not possible, see "exclude ? DIRECTBLOCKS : 0" above */
	if (exclude && indirection == 0)
		{
		/* it's some other aligned file on the drive */
		moveable = 0;
		}
	else
#endif
#if 0 /* under construction */
	if (exclude == 2 && indirection == 0)
		{
		/* it's the target file */
		chase = xip_align_reverse(blk, region, regions, blocks);
		moveable = (chase != (blkno_t)-1);
		}
	else
#endif
		{
		chase = xip_align_reverse(blk, region, regions, blocks);
		moveable = (chase != (blkno_t)-1);
		}

	/* early out, if the block doesn't need to be accessed at all */
	if (moveable == 0 && indirection == 0)
		{
		/* no action is needed (no move and not indirect) */
#if DEBUG > 3
		_dprintf(4, _("xip_align_recurse() "
				"returning -1, no action, ok\n"));
#endif
		return (blkno_t)-1;
		}

	/* need to read the block, we might modify it, and/or relocate it */
	devno = dev->s_dev; /* don't forget! */
	blkbuf_p = bread(error, devno, blk, 0);

	catch = setjmp(try_b);
	if (catch)
		{
		if (setjmp(try_b) == 0)
			{
			bfree(try_b, blkbuf_p, 0);
			}
#if DEBUG >= 3
		_dprintf(3, _("xip_align_recurse() error %u\n"), catch);
#endif
		longjmp(error, catch);
		}

	/* if it's a single or double indirect block, traverse recursively */
#if 1 /* under construction */
	if (indirection && (exclude != 1 || indirection > 1))
#else
	if (indirection && (exclude == 0 || indirection > 1))
#endif
		{
		for (i = 0; i < BUFSIZE / sizeof(blkno_t); i++)
			{
			newno = ((blkno_t *)blkbuf_p->bf_data)[i];
			if (newno)
				{
				if (exclude == 2) /* it's the target file */
					{
 /* NOTE! THIS IS VERY NON-IDEAL, IT WAS NEEDED TO AVOID A DEADLOCK PROBLEM */
					tempblk = newno;
					tempdirty = 0;

					bfree(error, blkbuf_p, 0);

					pos = xip_align_recurse(error, dev,
						ino, exclude,
						&tempblk, &tempdirty,
						indirection - 1,
						region, regions, blocks);

					blkbuf_p = bread(error, devno, blk, 0);

					if (tempdirty)
						{
						((blkno_t *)blkbuf_p->bf_data)[i] = tempblk;
						blkbuf_p->bf_dirty = tempdirty;
						}
					}
				else
					{
					pos = xip_align_recurse(try_b, dev,
						ino, exclude,
						(blkno_t *)blkbuf_p->bf_data + i,
						&blkbuf_p->bf_dirty, indirection-1,
						region, regions, blocks);
					}

				/* move something into the freed spot, and possibly repeat */
				if (pos != (blkno_t)-1)
					{
					if (exclude == 2) /* it's the target file */
						{
 /* NOTE! THIS IS VERY NON-IDEAL, IT WAS NEEDED TO AVOID A DEADLOCK PROBLEM */
						bfree(error, blkbuf_p, 0);

						newno = xip_align_chase(error,
							dev, ino, newno, pos,
							region, regions,
							blocks);

						blkbuf_p = bread(error, devno,
								blk, 0);
						}
					else
						{
						newno = xip_align_chase(try_b,
							dev, ino, newno, pos,
							region, regions,
							blocks);
						}

					/* eventually a non-target block was freed, indicate this */
					if (bitmap_set(try_b, devno, newno, 0,
						dev->s_bitmap_block,
						dev->s_bitmap_immov) != 1)
						{
						longjmp(try_b, ECORRUPTFS);
						}
					}
				}
			/*pos += newno;*/
			}
		}

	/* if this block is in the way of the target file, move it */
	if (moveable)
		{
		/* find an unoccupied block (fixme: can't be reserved) */
		newno = bitmap_find(try_b, devno, 0, 0, 1, dev->s_bitmap_block,
				dev->s_bitmap_immov);
		if (newno < (dev->s_reserv + dev->s_isize) ||
				newno >= dev->s_fsize)
			{
			longjmp(try_b, ENOSPC);
			}

		/* revise parent's allocation entry to show the new block */
		*parent = newno;

		/* inform parent that its allocation entries were changed */
		*dirty = 1;

#if DEBUG >= 2
		_dprintf(2, _("putting block %u -> %u\n"), blk, newno);
#endif
		/* really move the block by manipulating cache entries */
		other_blkbuf_p = bfind(devno, newno); /* hanging around in cache? */
		if (other_blkbuf_p)
			{
			other_blkbuf_p->bf_blk = blk; /* swap, for minimal overhead */
			other_blkbuf_p->bf_dirty = 0; /* try to avoid writing back */
			other_blkbuf_p->bf_busy = 0; /* no need to call blk_release() */
			/* because ksignal(Bufpool, 0) is done in bfree() */
			}

		/* revise the original cache entry to show the new block */
		blkbuf_p->bf_blk = newno; /* original value is in variable blk */
		bfree(error, blkbuf_p, 2);

#if DEBUG > 3
		_dprintf(4, _("xip_align_recurse() returning %u, moved, ok\n"),
				chase);
#endif
		return chase; /* indicate which spot was just freed */
		}

	bfree(error, blkbuf_p, 0);
#if DEBUG > 3
	_dprintf(4, _("xip_align_recurse() returning -1, ok\n"));
#endif
	return (blkno_t)-1; /* successfully traversed block and its children */
	}
#endif

