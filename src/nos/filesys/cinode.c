/*
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 Filesystem related routines
**********************************************************/

#include <errno.h>
#include <string.h>
#include "filesys/bitmap.h" /* for Badinomsg and others */
#include "driver/bufpool.h" /* for bread(), bwrite() */
#include "driver/device.h" /* for device_find(), struct device_s */
#include "filesys/cinode.h"
#include "kernel/dprintf.h"
#include "filesys/filesys.h" /* for struct file_system_s, findfs() */
#include "driver/rtc.h" /* for rtc_time() */
#include "nos/global.h" /* for ksignal() */
#include <libintl.h>
#include "po/messages.h"

#ifndef DEBUG
#define DEBUG 0
#endif

#ifdef MODULE
#define STATIC
extern _char *Refstoinode;
#else
#define STATIC static
#define MODULE_core_inode_list
#define MODULE_Refstoinode
#define MODULE_i_ref
#define MODULE_i_deref
#define MODULE_i_acquire
#define MODULE_i_release
#define MODULE_i_open
#define MODULE_wr_inode
#define MODULE_readwritei
#define MODULE_isdevice
#define MODULE_freeblk
#define MODULE_f_trunc
#define MODULE_bmap
#define MODULE_getmode
#define MODULE_getperm
#define MODULE_setftim
#define MODULE_magic
#define MODULE_i_stat
#define MODULE_i_sync
#define MODULE_inotab_garbage
#endif

#ifdef MODULE_core_inode_list
struct core_inode_s *core_inode_list[CORE_INODE_LIST];
#endif

/* Common messages. They are here to save memory */
#ifdef MODULE_Refstoinode
STATIC _char *Refstoinode = N_("%s refs to inode 0x%x\n");
#endif

STATIC void freeblk(dev_t dev, blkno_t blk, unsigned char level);

/* I_ref() increases the reference count of the given inode table entry.
 */

#ifdef MODULE_i_ref
void
i_ref(struct core_inode_s *ino)
{
#if DEBUG >= 3
 _dprintf(3, _("i_ref(0x%x) starting\n"), ino);
#endif

	i_acquire(ino);
	magic(ino, "i_ref");
#if 1
	if (ino->c_refs == 0xffff)
		{
		_panic(Refstoinode, "too many", ino);
		}
	ino->c_refs++;
#else
	if (++ino->c_refs >= 12 * CORE_INODE_LIST) /* Arbitrary limit */
		_panic(Refstoinode, "too many", ino);
#endif
	i_release(ino);
#if DEBUG >= 3
 _dprintf(3, _("i_ref() returning, refs %u\n"), ino->c_refs);
#endif
}
#endif

/* I_deref() decreases the reference count of an inode, and frees it
 * from the table if there are no more references to it.  If it also
 * has no links, the inode itself and its blocks (if not a device) are freed.
 */

#ifdef MODULE_i_deref
void
i_deref(struct core_inode_s *ino)
	{
	jmp_buf try_b;

#if DEBUG >= 3
 _dprintf(3, _("i_deref(0x%x) starting\n"), ino);
#endif
	i_acquire(ino);
	magic(ino, "i_deref");
	if (ino->c_refs == 0)
		{
		_panic(Refstoinode, "no", ino);
		}
	if (_getmode(ino->c_node.i_mode) == S_IFPIPE)
		{
		ksignal(ino, 0);
		}

	/* If the inode has no links and no refs,
	 * it must have its blocks freed.
	 */
	if (--ino->c_refs == 0)
		{
		if (ino->c_node.i_nlink == 0)
			{
			ino->c_node.i_size = 0;
			f_trunc(ino);
			/* and also freeing this inode */
			ino->c_node.i_mode = 0;
			if (setjmp(try_b) == 0)
				{
				i_free(try_b, ino->c_dev, ino->c_num);
				}
			}
		/* If the inode was modified, we must write it back to disk. */
		if (ino->c_dirty)
			{
			if (setjmp(try_b) == 0)
				{
				wr_inode(try_b, ino, 0);
				}
			}
		}

	i_release(ino);
#if DEBUG >= 3
 _dprintf(3, _("i_deref() returning, refs %u\n"), ino->c_refs);
#endif
	}
#endif

/* I_acquire() waits to acquire the inode's lock and then returns.
 */

#ifdef MODULE_i_acquire
void
i_acquire(struct core_inode_s *ino)
{
	/* unsigned int i_state; */

#if DEBUG >= 4
 _dprintf(4, _("i_acquire(0x%x) starting\n"), ino);
#endif
	/* i_state = dirps(); */
	while (ino->c_busy)
		{
		/* restore(i_state); */
		kwait(core_inode_list);
		/* i_state = dirps(); */
		}
	ino->c_busy = 1;
	/* restore(i_state); */
#if DEBUG >= 4
 _dprintf(4, _("i_acquire() returning\n"));
#endif
}
#endif

/* I_release() unlocks the inode, it is assumed to have been locked.
 */

#ifdef MODULE_i_release
void
i_release(struct core_inode_s *ino)
{
#if DEBUG >= 4
 _dprintf(4, _("i_release(0x%x) starting\n"), ino);
#endif
	ino->c_busy = 0;
	ksignal(core_inode_list, 0);
#if DEBUG >= 4
 _dprintf(4, _("i_release() returning\n"));
#endif
}
#endif

/* I_open() is given an inode number and a device number, and makes an entry
 * in the inode table for them, or increases it reference count if it is
 * already there.
 *
 * An inode == NULLINO means a newly allocated inode.
 */

#ifdef MODULE_i_open
struct core_inode_s *
i_open(jmp_buf error, dev_t devno, ino_t ino)
	{
	jmp_buf try_b;
	unsigned int catch;
	struct file_system_s *dev;
	struct block_buffer_s *blkbuf_p;
	struct core_inode_s *ip, *cinode_p;
	unsigned char i, newn, mode0;
	unsigned char refs0;
	static struct core_inode_s **nextipp = core_inode_list; /* rover ptr */

#if DEBUG >= 3
	_dprintf(3, _("i_open(0x%x, 0x%x, %u) starting\n"), error, devno, ino);
#endif
	dev = findfs(devno);
	if (dev == NULL)
		{
		_panic(Baddevmsg, "i_open", devno);
		}

	newn = 0;
	if (ino == NULLINO)		/* Wanted a new one */
		{
		ino = i_alloc(error, devno);
		++newn;
		}

#if 0 /* handy for troubleshooting fmount() / bdread() problems */
 _dprintf(2, _("ino = %u\n"), ino);
 _dprintf(2, _("dev -> 0x%x\n"), dev);
 _dprintf(2, _("devinodes(dev) = %u\n"), devinodes(dev));
#endif
	if (ino < ROOTINODE || ino >= devinodes(dev))
		{
		_dprintf(0, Badinomsg, "i_open", ino);
#if DEBUG >= 3
		_dprintf(3, _("i_open() error ECORRUPTFS"));
#endif
		longjmp(error, ECORRUPTFS);
		}

	/* search inode table for available entry */
silly:
/* _dprintf(2, _("{ ")); */
	for (i = 0; i < CORE_INODE_LIST; i++)
		{
		ip = *nextipp++;
		if (nextipp >= core_inode_list + CORE_INODE_LIST)
			{
			nextipp = core_inode_list;
/* _dprintf(2, _("%u "), ip - core_inode_list); */
			}
		if (ip && ip->c_dev == devno && ip->c_num == ino)
			{
/* _dprintf(2, _("} ")); */
			if (ip->c_busy)
				{
				kwait(core_inode_list);
				goto silly;
				}
			ip->c_busy = 1;
			cinode_p = ip;
			goto found;	/* really found */
			}
		}
/* _dprintf(2, _("} ")); */

	cinode_p = NULL;
/* _dprintf(2, _("{ ")); */
	for (i = 0; i < CORE_INODE_LIST; i++)
		{
		ip = *nextipp++;
		if (nextipp >= core_inode_list + CORE_INODE_LIST)
			{
			nextipp = core_inode_list;
/* _dprintf(2, _("%u "), ip - core_inode_list); */
			}
		if (ip && ip->c_busy == 0 && ip->c_refs == 0)
			{
			ip->c_busy = 1;
			cinode_p = ip;	/* candidate for discarding */
			goto ffree;
			}
		}
/* _dprintf(2, _("} ")); */

	/* No unrefed buffers in inode table */
	for (i = 0; i < CORE_INODE_LIST; i++)
		{
		if (core_inode_list[i] == NULL)
			{
			cinode_p = calloc(sizeof(struct core_inode_s), 1);
			if (cinode_p == NULL)
				{
				break; /* causes ENOMEM error */
				}
			cinode_p->c_busy = 1;
			core_inode_list[i] = cinode_p;
			goto ffree;
			}
		}

	/* no unrefed slots in inode table, or no memory */
#if DEBUG >= 3
	_dprintf(3, _("i_open() error ENOMEM"));
#endif
	longjmp(error, ENOMEM);

ffree:
/* _dprintf(2, _("reading(%u, %u) "), inodeblock(dev, ino), inodeoffset(ino)); */
	/* discard oldest? inode from table and read the inode from disk */
	catch = setjmp(try_b);
	if (catch)
		{
		i_release(cinode_p);
#if DEBUG >= 3
	_dprintf(3, _("i_open() error %u"), catch);
#endif
		longjmp(error, catch);
		}

	blkbuf_p = bread(try_b, devno, inodeblock(dev, ino), 0);
	memcpy(&cinode_p->c_node,
			(struct disk_inode_s *)blkbuf_p->bf_data + inodeoffset(ino),
			sizeof(struct disk_inode_s));
	bfree(try_b, blkbuf_p, 0);

	/* fill-in in-core parts of inode */
	cinode_p->c_magic = CMAGIC;
	cinode_p->c_dev = devno;
	cinode_p->c_num = ino;
	cinode_p->c_ro = dev->s_ronly;

found:
/* _dprintf(2, _("found(%u, 0%o) "), cinode_p->c_node.i_nlink, cinode_p->c_node.i_mode); */
	mode0 = (_getmode(cinode_p->c_node.i_mode) == 0);
	/* need to check for the special case of working on an erased file */
	refs0 = (cinode_p->c_refs == 0) && (cinode_p->c_node.i_nlink == 0);
	if (newn)	/* RO fs can't do i_alloc()! */
		{
		/* newly allocated disk inode must be clean */
		if (!refs0 || !mode0)
			{
			goto badino;
			}
		}
	else
		{
		/* and vice versa */
		 if (refs0 || mode0)
			{
		badino:
			i_release(cinode_p);
			_dprintf(0, Badinomsg, "i_open (disk)", ino);
#if DEBUG >= 3
			_dprintf(3, _("i_open() error ECORRUPTFS"));
#endif
			longjmp(error, ECORRUPTFS);
			}
		}

	magic(cinode_p, "i_open");
	if (cinode_p->c_refs == 0xffff)
		{
		_panic(Refstoinode, "too many", cinode_p);
		}
	cinode_p->c_refs++;

	i_release(cinode_p);
#if DEBUG >= 3
	_dprintf(3, _("i_open() returning 0x%x\n"), cinode_p);
#endif
	return cinode_p;
	}
#endif

/* Wr_inode() writes out the given inode in the inode table out to disk,
 * and resets its dirty bit.
 * Attention! Immediate writing may be delayed by dirty_mask!
 * Caller must acquire the inode beforehand and release it afterwards
 */

#ifdef MODULE_wr_inode
void
wr_inode(jmp_buf error, struct core_inode_s *cinode_p, unsigned char dirty_mask)
	{
	jmp_buf try_b;
	unsigned int catch;
	struct block_buffer_s *blkbuf_p;

#if DEBUG >= 3
	_dprintf(3, _("wr_cinode(0x%x, 0x%x) starting\n"), error, cinode_p);
#endif
	magic(cinode_p, "wr_inode");
	if (cinode_p->c_dirty && cinode_p->c_ro == 0)
		{
		blkbuf_p = bread(error, cinode_p->c_dev,
				inodeblock(findfs(cinode_p->c_dev),
				cinode_p->c_num), 0);
		memcpy((struct disk_inode_s *)blkbuf_p->bf_data +
				inodeoffset(cinode_p->c_num),
				&cinode_p->c_node, sizeof(struct disk_inode_s));
		/* write immediately, unless dirty_mask set before call */
		/* shouldn't this be dirty_mask ? 2 : cinode_p->c_dirty ???? */
		bfree(error, blkbuf_p, dirty_mask ? 1 : cinode_p->c_dirty);
		}

	cinode_p->c_dirty = 0; /* unmark modif flag */
#if DEBUG >= 3
	_dprintf(3, _("wr_inode() returning\n"));
#endif
	}
#endif

/* readwritei() implements reading/writing from/to any kind of inode
 */

#ifdef MODULE_readwritei
size_t
readwritei(jmp_buf error, unsigned char write, struct core_inode_s *ino,
		void *data, size_t count, unsigned long position)
	{
	jmp_buf try_b;
	unsigned int catch;
	struct device_char_s *device_char_p;
	register size_t amount, todo, uoff, tdo;
	register struct block_buffer_s *blkbuf_p;
	register blkno_t pblk, ublk;
	dev_t dev;
	unsigned char *base, ispipe;

#if DEBUG >= 3
	_dprintf(3, _("readwritei(0x%x, %u, 0x%x, 0x%x, 0x%x, 0x%lx) "
			"starting\n"), error, (unsigned int)write, ino,
			data, count, position);
#endif
/* while (1) */
/*  ; */
	i_acquire(ino);
	if (write && ino->c_ro)
		{
		i_release(ino);
#if DEBUG >= 3
		_dprintf(3, _("readwritei() error EROFS\n"));
#endif
		longjmp(error, EROFS);
		}

	dev = ino->c_dev;
	tdo = 0; /* amount done so far */
	base = data;
	ispipe = 0;

	switch (_getmode(ino->c_node.i_mode))
		{
	case S_IFCHR:	/* character device */
		/* note: the DEVNUM macro uses ino->c_node.i_addr[0] rather */
		/* than ino->c_dev which is the device containing the pseudo */
		/* file - we are reusing the variable dev because it's safe */
		dev = DEVNUM(ino);
		i_release(ino);

		device_char_p = (struct device_char_s *)
				device_find(error, dev);
/* abyte('{'); */
		count = (write ? device_char_p->write : device_char_p->read)
				(error, MINOR(dev), base, count);
/* abyte('}'); */
#if 0
		position += count;
#endif
		break;
	case S_IFLNK:	/* sym link */
		if (write)
			{
			i_release(ino);
#if DEBUG >= 3
			_dprintf(3, _("readwritei() error ENODEV\n"));
#endif
			longjmp(error, ENODEV);
			}
	case S_IFDIR:	/* directory */
	case S_IFREG:	/* regular file */
	case S_IFALIGN:	/* aligned regular file */
		todo = count;
		if (!write)
			{
			/* See if offset is beyond end of file */
			if (position >= ino->c_node.i_size)
				{
				i_release(ino);

				count = 0;
				break;
				}

			/* See if end of file will limit read */
			if ((position + todo) > ino->c_node.i_size)
				{
				todo = (unsigned int)(ino->c_node.i_size -
						position);
				}

			count = todo;
			}
		goto loop;
	case S_IFPIPE:	/* interprocess pipe */
		++ispipe;
		if (write)
			{	/* write side */
			while ((todo = count) >
					(DIRECTBLOCKS * BUFSIZE) -
					(unsigned int)ino->c_node.i_size)
				{
				if (ino->c_refs == 1)
					{
					/* No readers - broken pipe */
#if 0 /* temporary!!!! */
					ssig(my_thread_p->process_p, SIGPIPE);
#endif
#if DEBUG >= 3
					_dprintf(3, _("readwritei() error EPIPE\n"));
#endif
					longjmp(error, EPIPE);
					}
				/* Sleep if full pipe */
				i_release(ino);
				kwait(ino);	/* P_SLEEP */
				i_acquire(ino);
				}
			}
		else
			{	/* read side */
			while ((unsigned int)ino->c_node.i_size == 0)
				{
				if (ino->c_refs == 1)	/* No writers */
					{
					break;
					}
				/* Sleep if empty pipe */
				i_release(ino);
				kwait(ino);		/* P_SLEEP */
				i_acquire(ino);
				}
			todo = min_uint(count, ino->c_node.i_size);
			count = todo;
			}
		goto loop;
	case S_IFBLK:	/* block device */
		dev = DEVNUM(ino);
		todo = count;

	loop:	/* blockdev/directory/regular/pipe */
		catch = setjmp(try_b);
		if (catch)
			{
			/* if a partial operation was done before the error */
			/* was detected, squelch the error (we should also */
			/* save it and return it next time, but we will rely */
			/* on a likelihood that the error will recur anyway */
			if (tdo)
				{
				goto done;
				}

			i_release(ino);

			/* if (!write && isdevice(ino)) we should */
			/* possibly translate error codes so that */
			/* "out of disk bounds" looks like EOF?? */
#if DEBUG >= 3
			_dprintf(3, _("readwritei() error %u\n"), catch);
#endif
			longjmp(error, catch);
			}

		while (todo)
			{
			ublk = (unsigned int)(position >> BUFSIZELOG);
			uoff = (unsigned int)position & (BUFSIZE - 1);
			pblk = bmap(try_b, ino, ublk, write == 0);

			if (!write)
				{
				/* Nick added isdevice(ino) to allow access */
				/* to block 0 via open handle to /dev/hdX */
				blkbuf_p = (pblk == NULLBLK &&
						isdevice(ino) == 0) ?
						zerobuf(NULL) :
						bread(try_b, dev, pblk, 0);
				}

			amount = min_uint(todo, BUFSIZE - uoff);
			if (write)
				{
#if 0
				if (pblk == NULLBLK && isdevice(ino) == 0)
					{
					/* No space to make more blocks */
					return;
					}
#endif

				/* If we are writing an entire block, we don't
				 * care about its previous contents
				 */
				blkbuf_p = bread(try_b, dev, pblk,
						(amount == BUFSIZE));

				/* new usrput routine handles the sysio test */
				usrget(blkbuf_p->bf_data + uoff, base, amount);

				/* should be bfree(try_b, blkbuf_p, 1); ???? */
				bfree(try_b, blkbuf_p, 2);
				}
			else
				{
				/* new usrput routine handles the sysio test */
				usrput(base, blkbuf_p->bf_data + uoff, amount);
				bfree(try_b, blkbuf_p, 0);
				}
			base += amount;
			tdo += amount;
			position += amount;
			todo -= amount;
			if (ispipe)
				{
				if (ublk >= DIRECTBLOCKS)
					{
					position &= (BUFSIZE - 1);
					}
				ino->c_node.i_size += write ? amount : -amount;
				/* Wake up any readers/writers */
				ksignal(ino, 0);
				}
			}

	done:
		count = tdo;

		/* Update size if file grew */
		if (write && ispipe == 0)
			{
			if (position > ino->c_node.i_size)
				{
				ino->c_node.i_size = position;
				ino->c_dirty = 2;
				}
			}

		i_release(ino);
		break;
	default:
		i_release(ino);
#if DEBUG >= 3
		_dprintf(3, _("readwritei() error ENODEV\n"));
#endif
		longjmp(error, ENODEV);
		}
#if DEBUG >= 3
	_dprintf(3, _("readwritei() returning 0x%x\n"), count);
#endif
	return count;
}
#endif

/* isdevice() returns true if ino points to a device
 * old: !!! assume S_IFBLK == S_IFCHR|any
 * new: checks for S_IFBLK and S_IFCHR separately
 * Caller must acquire the inode beforehand and release it afterwards
 */

#ifdef MODULE_isdevice
unsigned char
isdevice(struct core_inode_s *ino)
{
#if 1 /* Nick, to avoid conflict with S_IFALIGN and other new filetypes */
	return S_ISDEV(ino->c_node.i_mode);
#else
	return (ino->c_node.i_mode & S_IFCHR) != 0;
#endif
}
#endif

/* Freeblk() - free (possible recursive for (double)inderect) block
 * Caller must acquire the inode beforehand and release it afterwards
 * NOTE: THIS SHOULD BE FIXED SO IT CAN RETURN ERRORS!!!!!
 * THEN WE SHOULD ALSO MAKE IT CHECK blk_free() RETURN CODE!!
 */

#ifdef MODULE_freeblk
STATIC void
freeblk(dev_t dev, blkno_t blk, unsigned char level)
	{
	jmp_buf try_b;
	register struct block_buffer_s *blkbuf_p;
	register unsigned int j;

#if DEBUG >= 3
 _dprintf(3, _("freeblk(0x%x, %u, %u) starting\n"), dev, blk, (unsigned int)level);
#endif
	if (blk)
		{
		if (level)
			{ /* INDIRECT/DINDIRECT block */
			--level;
			/* SQUELCH EXCEPTIONS FOR THE MOMENT (NOT NICE) */
			if (setjmp(try_b) == 0)
				{
				blkbuf_p = bread(try_b, dev, blk, 0);
				j = BUFSIZE/sizeof(blkno_t);
				while (j)
					{
					freeblk(dev, ((blkno_t *)blkbuf_p->
							bf_data)[--j], level);
					}
				bfree(try_b, blkbuf_p, 0);
				}
			}
		/* SQUELCH EXCEPTIONS FOR THE MOMENT (NOT NICE) */
		if (setjmp(try_b) == 0)
			{
			blk_free(try_b, dev, blk);
			}
		}
#if DEBUG >= 3
 _dprintf(3, _("freeblk() returning\n"));
#endif
	}
#endif

/* F_trunc() frees all the blocks associated with the file,
 * if it is a disk file.
 * need enhancement for truncating to given size
 * Caller must acquire the inode beforehand and release it afterwards
 */

#ifdef MODULE_f_trunc
void
f_trunc(struct core_inode_s *ino)
{
	struct disk_inode_s *ip = &ino->c_node;
	blkno_t *blk = ip->i_addr;
	register unsigned char j;

#if DEBUG >= 3
 _dprintf(3, _("f_trunc(0x%x) starting\n"), ino);
#endif

	/* i_acquire(ino); */
	/* First deallocate the double indirect blocks */
	freeblk(ino->c_dev, blk[TOTALREFBLOCKS-1], 2);
	/* Also deallocate the indirect blocks */
	freeblk(ino->c_dev, blk[TOTALREFBLOCKS-1-DINDIRECTBLOCKS], 1);
	/* Finally, free the direct blocks */
	j = 0;
	while (j != DIRECTBLOCKS) {
		freeblk(ino->c_dev, *blk++, 0);
		j++;
	}
	memset(ip->i_addr, 0, sizeof(blkno_t)*TOTALREFBLOCKS);
	ino->c_dirty = 2;
	ip->i_size = 0;
	/* i_release(ino); */
#if DEBUG >= 3
 _dprintf(3, _("f_trunc() returning\n"));
#endif
}
#endif

/* Bmap() defines the structure of file system storage by returning the
 * physical block number on a device given the inode and the logical
 * block number in a file.  Can return NULLBLK only if rdflag != 0.
 *
 * The block is zeroed if created.
 * Caller must acquire the inode beforehand and release it afterwards
 */

#ifdef MODULE_bmap
blkno_t
bmap(jmp_buf error, struct core_inode_s *ip, blkno_t bn, unsigned char rdflg)
	{
	jmp_buf try_b;
	unsigned int catch;
	dev_t dev;
	register unsigned int i, j, sh;
	register struct block_buffer_s *blkbuf_p;
	register blkno_t *np, nb;

#if DEBUG >= 3
	_dprintf(3, _("bmap(0x%x, 0x%x, %u, %u) starting\n"),
			error, ip, bn, (unsigned int)rdflg);
#endif
	if (isdevice(ip))	/* block devices */
		{
#if DEBUG >= 3
		_dprintf(3, _("bmap() returning %u, direct\n"), bn);
#endif
		return bn;	/* map directly */
		}
	dev = ip->c_dev;

	/* blocks 0..DIRECTBLOCKS-1 are direct blocks */
	if (bn < DIRECTBLOCKS)
		{
		np = &ip->c_node.i_addr[bn];
		nb = *np;
		if (nb == 0)
			{
			/* block not allocated yet */
			if (rdflg)
				{
#if DEBUG >= 3
				_dprintf(3, _("bmap() returning NULLBLK a\n"));
#endif
				return NULLBLK;
				}
			nb = blk_alloc(error, dev, 1); /* write back later */
			*np = nb;
			ip->c_dirty = 2;
			}
		goto Ok;
		}

	/* addresses DIRECTBLOCKS and DIRECTBLOCKS+1 have single and double
	 * indirect blocks.
	 * The first step is to determine how many levels of indirection.
	 */
	bn -= DIRECTBLOCKS;
	sh = 0;
	j = INDIRECTBLOCKS+DINDIRECTBLOCKS;
	if (bn >= BUFSIZE/sizeof(blkno_t))	/* double indirect */
		{
		sh = 8; 			/* shift by 8 */
		bn -= BUFSIZE/sizeof(blkno_t);
		j -= INDIRECTBLOCKS;
		}

	/* fetch the address from the inode.
	 * Create the first indirect block if needed.
	 */
	np = &ip->c_node.i_addr[TOTALREFBLOCKS - j];
	nb = *np;
	if (nb == 0)
		{
		if (rdflg)
			{
#if DEBUG >= 3
			_dprintf(3, _("bmap() returning NULLBLK b\n"));
#endif
			return NULLBLK;
			}
		nb = blk_alloc(error, dev, 2); /* write back now */
		*np = nb;
		ip->c_dirty = 2;
		}

	/* fetch through the indirect blocks */
	while (j <= INDIRECTBLOCKS+DINDIRECTBLOCKS)
		{
		blkbuf_p = bread(error, dev, nb, 0);
		np = (blkno_t *)blkbuf_p->bf_data;
		i = (bn >> sh) & (BUFSIZE/sizeof(blkno_t)-1);
		nb = np[i];
		if (nb)
			{
			bfree(error, blkbuf_p, 0);
			}
		else
			{
			if (rdflg)
				{
				bfree(error, blkbuf_p, 0);
#if DEBUG >= 3
				_dprintf(3, _("bmap() returning NULLBLK c\n"));
#endif
				return NULLBLK;
				}
			catch = setjmp(try_b);
			if (catch)
				{
				if (setjmp(try_b) == 0)
					{
					bfree(try_b, blkbuf_p, 0);
					}
#if DEBUG >= 3
				_dprintf(3, _("bmap() error %u\n"), catch);
#endif
				longjmp(error, catch);
				}
			nb = blk_alloc(try_b, dev,
#if 1
					(j == INDIRECTBLOCKS+DINDIRECTBLOCKS) ?
					1 : 2 /* if indirect, write back now */
#else
					1 /* write back later */
#endif
					);
			np[i] = nb;
			bfree(error, blkbuf_p, 1);
			}
		sh -= 8;
		j++;
		}
Ok:
#if DEBUG >= 3
 _dprintf(3, _("bmap() returning %u\n"), nb);
#endif
	return nb;
	}
#endif

/* Getmode() looks at the given inode and the effective user/group ids, and
 * returns the effective modeissions in the low-order 3 bits.
 */

#ifdef MODULE_getmode
mode_t
getmode(struct core_inode_s *ino)
{
	mode_t mode;

#if DEBUG >= 3
 _dprintf(3, _("getmode(0x%x) starting\n"), ino);
#endif
	i_acquire(ino);
	mode = _getmode(ino->c_node.i_mode);
	i_release(ino);
#if DEBUG >= 3
 _dprintf(3, _("getmode() returning 0%o\n"), mode);
#endif
	return mode;
}
#endif

/* Getperm() looks at the given inode and the effective user/group ids, and
 * returns the effective permissions in the low-order 3 bits.
 */

#ifdef MODULE_getperm
int
getperm(struct core_inode_s *ino)
{
	mode_t mode;

#if DEBUG >= 3
 _dprintf(3, _("getperm(0x%x) starting\n"), ino);
#endif
	i_acquire(ino);
	mode = ino->c_node.i_mode;
	if (super())
		{
		mode |= S_ISDEV(mode) ? ((mode >> 3) | (mode >> 6)) : 7;
		}
	else
		{
		if (ino->c_node.i_uid == my_thread_p->process_p->p_euid)
			{
			mode >>= 6; /* owner */
			}
		else if (ino->c_node.i_gid ==
				my_thread_p->process_p->p_egid)
			{
			mode >>= 3; /* group */
			}
		}
	mode &= 7;
	i_release(ino);
#if DEBUG >= 3
 _dprintf(3, _("getperm() returning %u\n"), mode);
#endif
	return mode;
}
#endif

/* Setftim() sets the times of the given inode, according to the flags
 */

#ifdef MODULE_setftim
void
setftim(struct core_inode_s *ino, unsigned char flag)
{
	/*static*/ time_t t;

#if DEBUG >= 3
 _dprintf(3, _("setftim(0x%x, %u) starting\n"), ino, flag);
#endif
	i_acquire(ino);
	t = rtc_time();
	if (flag & A_TIME)	ino->c_node.i_atime = t;
	if (flag & M_TIME)	ino->c_node.i_mtime = t;
	if (flag & C_TIME)	ino->c_node.i_ctime = t;
	ino->c_dirty |= 1; /* means not very important to write it back */
	i_release(ino);
#if DEBUG >= 3
 _dprintf(3, _("setftim() returning\n"));
#endif
}
#endif

/* magic() check inode against corruption
 * Caller must acquire the inode beforehand and release it afterwards
 */

#ifdef MODULE_magic
void
magic(struct core_inode_s *ino, char *who) /* Nick added who */
{
#if DEBUG >= 3
 _dprintf(3, _("magic(0x%x, \"%s\") starting\n"), ino, who);
#endif
	if (ino->c_magic != CMAGIC)
		_panic(_("%s: corrupted inode 0x%x"), who, ino); /* Nick added who */
#if DEBUG >= 3
 _dprintf(3, _("magic() returning\n"));
#endif
}
#endif

/* utility routine for fs_stat() and uf_fstat() */

#ifdef MODULE_i_stat
void
i_stat(struct core_inode_s *cinode_p, struct stat *buf)
	{
	/* ensure fields are copied individually, struct core_inode_s may change */
	i_acquire(cinode_p);
	usrput_unsigned_int(&buf->st_dev, cinode_p->c_dev);
	usrput_unsigned_int(&buf->st_ino, cinode_p->c_num);
	usrput_unsigned_int(&buf->st_mode, cinode_p->c_node.i_mode);
	usrput_unsigned_int(&buf->st_nlink, cinode_p->c_node.i_nlink);
	usrput_unsigned_int(&buf->st_uid, cinode_p->c_node.i_uid);
	usrput_unsigned_int(&buf->st_gid, cinode_p->c_node.i_gid);
	usrput_unsigned_int(&buf->st_rdev, DEVNUM(cinode_p));
	usrput(&buf->st_size, &cinode_p->c_node.i_size, sizeof(unsigned long));
	usrput(&buf->st_atime, &cinode_p->c_node.i_atime,
			3 * sizeof(rtctime_t));
	i_release(cinode_p);
	}
#endif

/* i_sync() syncing all inodes
 */

#ifdef MODULE_i_sync
void
i_sync(dev_t dev)
	{
	jmp_buf try_b;
	register struct core_inode_s **inopp, *ino;

	/* Write out modified inodes */
	for (inopp = core_inode_list; inopp < core_inode_list + CORE_INODE_LIST; inopp++)
		{
		ino = *inopp;
		if (ino && ino->c_refs)
			{
			i_acquire(ino);
			magic(ino, "i_sync");
			if (ino->c_dirty &&
					(dev == NULLDEV || dev == ino->c_dev))
				{
				/* note: dirty_mask is given as 1, so that */
				/* several adjacent inodes can be written in */
				/* one go (we will call bufsync after this!) */
				if (setjmp(try_b) == 0)
					{
					wr_inode(try_b, ino, 1);
					}
				}
			i_release(ino);
			}
		}
	}
#endif

#ifdef MODULE_inotab_garbage
/* core_inode_list garbage collection - called by storage allocator when free space
 * runs low.  Non-referenced (cached) inodes are discarded.  They will never
 * be dirty, because they will have been synced when released earlier.
 */
void
inotab_garbage(int red)
	{
	register struct core_inode_s **inopp, *ino;

	for (inopp = core_inode_list; inopp < core_inode_list + CORE_INODE_LIST; inopp++)
		{
		ino = *inopp;
		if (ino && ino->c_refs == 0)
			{
			*inopp = NULL;
			free(ino);
			}
		}
	}
#endif

