/* devnull.c by Nick for NOS/UZI project */

#include <errno.h>
#include "driver/device.h" /* for struct device_char_s */
#include "driver/devnull.h" /* this will include devio.h */
#include "nos/global.h" /* for copyr() */
#include <libintl.h>
#include "po/messages.h"

#ifndef DEBUG
#define DEBUG 0
#endif

#ifndef MODULE
#define MODULE_dev_null
#define MODULE_dev_null_init
#define MODULE_dev_null_read
#define MODULE_dev_null_write
#define MODULE_dev_null_ioctl
#endif

#ifdef MODULE_dev_null
struct device_char_s dev_null =
	{
		{
		1,			/* minors */
		dev_null_init,		/* init */
		dev_null_init,		/* open */
		dev_null_init,		/* close */
		dev_null_ioctl		/* ioctl */
		},
	dev_null_read,		/* read */
	dev_null_write		/* write */
	};
#endif

#ifdef MODULE_dev_null_init
void
dev_null_init(jmp_buf error, unsigned char minor)
	{
	/* do nothing, success */
	}
#endif

#ifdef MODULE_dev_null_read
size_t
dev_null_read(jmp_buf error, unsigned char minor, void *data, size_t count)
	{
	return 0; /* do nothing, success */
	}
#endif

#ifdef MODULE_dev_null_write
size_t
dev_null_write(jmp_buf error, unsigned char minor, void *data, size_t count)
	{
	return count; /* do nothing, complete */
	}
#endif

#ifdef MODULE_dev_null_ioctl
void
dev_null_ioctl(jmp_buf error, unsigned char minor,
		unsigned int request, void *data)
	{
	longjmp(error, EINVAL);
	}
#endif

