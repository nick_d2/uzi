/* rtc.c by Nick for NOS/UZI project
 *
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 Real time clock I/O routines
**********************************************************/

#include <errno.h>
#include "kernel/process.h" /* for super() */
#include "driver/rtc.h"
#include "z80/abus.h"
#include "nos/global.h"

#ifndef DEBUG
#define DEBUG 0
#endif

#ifndef MODULE
#define MODULE_Rtc
#define MODULE_Rtc_protect
#define MODULE_rtc_time
#define MODULE_rtc_stime
#endif

#ifdef MODULE_Rtc
rtctime_t Rtc = 0x28210000L; /* 00:00:00 sat 01 jan 2000 */
#endif

#ifdef MODULE_Rtc_protect
unsigned char Rtc_protect;
#endif

#ifdef MODULE_rtc_time
rtctime_t
rtc_time(void)
	{
	/* read wall clock, kept updated by struct abus_s_minor_shread_lcd0() */
	return Rtc;
	}
#endif

#ifdef MODULE_rtc_stime
void
rtc_stime(jmp_buf error, rtctime_t time)
	{
	unsigned char message[8];

	if (super() == 0)
		{
		longjmp(error, EPERM);
		}

	/* set wall clock, it may become corrupted if we kwait() */
	Rtc = time;
	Rtc_protect = 2; /* skip next 2 rtc updates so we don't go backwards */

	/* ensure lcd0 device is attached (iface is just a flag) */
	if (Api[1].async.iface == NULL)
		{
		return;
		}

	/* construct message for lcd0, assuming Rtc unchanged */
	message[0] = 0x1b;
	message[1] = 'T';
	message[2] = (Rtc >> 11) & 0x1f;	/* hour */
	message[3] = (Rtc >> 5) & 0x3f;		/* mins */
	message[4] = (Rtc << 1) & 0x3f;		/* secs */
	message[5] = (Rtc >> 21) & 0xf;		/* month */
	message[6] = (Rtc >> 16) & 0x1f;	/* day */
	message[7] = (Rtc >> 25) & 0x3f;	/* year */

	/* send message to lcd0, we don't care about errors */
	async_write(&Api[1].async, message, 8);
	}
#endif

