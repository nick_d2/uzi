/* devzero.c by Nick for NOS/UZI project */

#include <errno.h>
#include "driver/device.h" /* for struct device_char_s */
#include "driver/devnull.h" /* this will include devio.h */
#include "driver/devzero.h"
#include "nos/global.h" /* for copyr() */
#include <libintl.h>
#include "po/messages.h"

#ifndef DEBUG
#define DEBUG 0
#endif

#ifndef MODULE
#define MODULE_dev_zero
#define MODULE_dev_zero_read
#endif

#ifdef MODULE_dev_zero
struct device_char_s dev_zero =
	{
		{
		1,			/* minors */
		dev_null_init,		/* init */
		dev_null_init,		/* open */
		dev_null_init,		/* close */
		dev_null_ioctl		/* ioctl */
		},
	dev_zero_read,		/* read */
	dev_null_write		/* write */
	};
#endif

#ifdef MODULE_dev_zero_read
size_t
dev_zero_read(jmp_buf error, unsigned char minor, void *data, size_t count)
	{
	/* for now this is the same as dev_null but want to zero user buffer */
	return 0; /* do nothing, success */
	}
#endif

