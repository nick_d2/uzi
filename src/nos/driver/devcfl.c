/* devcfl.c by Nick for NOS/UZI project */

#include <errno.h>
#include "driver/device.h" /* for struct device_block_s */
#include "kernel/dprintf.h"
#include "driver/devnull.h" /* this will include devio.h */
#include "driver/devcfl.h"
#include "nos/global.h" /* for copyr() */
#include "z80/abus.h"
#include <libintl.h>
#include "po/messages.h"

#ifndef DEBUG
#define DEBUG 0
#endif

#ifdef MODULE
#define STATIC
extern unsigned char Cfl_refs;
extern unsigned char Cfl_busy;
#else
#define STATIC static
#define MODULE_dev_cfl
#define MODULE_Cfl_refs
#define MODULE_Cfl_busy
#define MODULE_Cfl_data
#define MODULE_Cfl_message
#define MODULE_Cfl_error
#define MODULE_dev_cfl_open
#define MODULE_dev_cfl_close
#define MODULE_dev_cfl_read
#define MODULE_dev_cfl_write
#endif

#ifdef MODULE_dev_cfl
struct device_block_s dev_cfl =
	{
		{
		1,			/* minors */
		dev_null_init,		/* init */
		dev_cfl_open,		/* open */
		dev_cfl_close,		/* close */
		dev_null_ioctl		/* ioctl */
		},
	dev_cfl_read,		/* read */
	dev_cfl_write		/* write */
	};
#endif

#ifdef MODULE_Cfl_refs
STATIC unsigned char Cfl_refs;
#endif

#ifdef MODULE_Cfl_busy
STATIC unsigned char Cfl_busy;
#endif

#ifdef MODULE_Cfl_data
unsigned char *Cfl_data;
#endif

#ifdef MODULE_Cfl_message
unsigned char *Cfl_message;
#endif

#ifdef MODULE_Cfl_error
unsigned int Cfl_error;
#endif

#ifdef MODULE_dev_cfl_open
void
dev_cfl_open(jmp_buf error, unsigned char minor)
	{
#if DEBUG >= 3
	_dprintf(3, _("dev_cfl_open(0x%x, %u) starting\n"), error,
			(unsigned int)minor);
#endif
	if (Cfl_refs == (unsigned char)0)
		{
		device_find(error, 0x606)->open(error, MINOR(0x606));
		}

	/* assert(Cfl_refs != (unsigned char)0xff); */
	Cfl_refs++;
#if DEBUG >= 3
	_dprintf(3, _("dev_cfl_open() returning\n"));
#endif
	}
#endif

#ifdef MODULE_dev_cfl_close
void
dev_cfl_close(jmp_buf error, unsigned char minor)
	{
	jmp_buf try_b;

#if DEBUG >= 3
	_dprintf(3, _("dev_cfl_close(0x%x, %u) starting\n"), error,
			(unsigned int)minor);
#endif
	if (Cfl_refs != (unsigned char)0)
		{
		Cfl_refs--;
		if (Cfl_refs == (unsigned char)0)
			{
			if (setjmp(try_b) == 0)
				{
				device_find(try_b, 0x606)->close(try_b,
						MINOR(0x606));
				}
			}
		}
#if DEBUG >= 3
	_dprintf(3, _("dev_cfl_close() returning\n"));
#endif
	}
#endif

#ifdef MODULE_dev_cfl_read
void
dev_cfl_read(jmp_buf error, unsigned char minor, void *data, blkno_t block)
	{
	unsigned long temp;
	unsigned char retry;
	static unsigned char message[7]; /* cannot be in stack segment! */
	unsigned int i;
	unsigned char sysio_save;

#if DEBUG >= 3
	_dprintf(3, _("dev_cfl_read(0x%x, %u, 0x%x, %u) starting\n"),
			error, (unsigned int)minor, data, block);
#endif
	if (block >= HD1_SIZE)
		{
		goto error1;
		}
	temp = (unsigned long)block + HD1_START;

	sysio_save = my_thread_p->sysio;
	my_thread_p->sysio = 1;

	while (Cfl_busy)
		{
		kwait(&Cfl_busy);
		}
	Cfl_busy = 1;

	message[0] = 0x1b;
	message[1] = 'R';
	message[2] = (unsigned char)temp;
	message[3] = (unsigned char)(temp >> 8);
	message[4] = (unsigned char)(temp >> 16);
	message[5] = (unsigned char)(temp >> 24);
	message[6] = ~(message[2] + message[3] + message[4] + message[5]);

	for (retry = 0; retry < 3; retry++)
		{
#if DEBUG >= 2
		_dprintf(2, _("cfl read 0x%lx "), temp);
#endif
		/* Cfl_data must be filled in before Cfl_message */
		Cfl_data = data;
		Cfl_message = message;

		if (async_write(&Api[6].async, message, 7) != 7)
			{
			/* disaster has struck, no point retrying here */
			Cfl_message = NULL;
			Cfl_data = NULL;
			goto error2;
			}

		/* command has been sent, wait for response via devapi.c */
		kalarm(1000L);
		while (Cfl_message)
			{
			if (kwait(&Cfl_message) == EALARM)
				{
				/* woken by signal */
#if DEBUG >= 2
				_dprintf(2, _("timeout "));
#endif
				goto abort;
				}
			}
		kalarm(0L);

		if (Cfl_error)
			{
#if DEBUG >= 2
			_dprintf(2, _("error %u "), Cfl_error);
#endif
			goto abort;
			}

		Cfl_data = NULL;
#if DEBUG >= 2
		_dprintf(2, _("ok\n"));
#endif

		Cfl_busy = 0;
		ksignal(&Cfl_busy, 1);

		my_thread_p->sysio = sysio_save;
#if DEBUG >= 3
		_dprintf(3, _("dev_cfl_read() returning\n"));
#endif
		return;

	abort:
		Cfl_message = NULL;
		Cfl_data = NULL;
#if DEBUG >= 2
		_dprintf(2, _("retry %u\n"), (unsigned int)retry);
#endif
		}

error2:
	Cfl_busy = 0;
	ksignal(&Cfl_busy, 1);

	my_thread_p->sysio = sysio_save;

error1:
#if DEBUG >= 3
	_dprintf(3, _("dev_cfl_read() error EIO\n"));
#endif
	longjmp(error, EIO);
	}
#endif

#ifdef MODULE_dev_cfl_write
void
dev_cfl_write(jmp_buf error, unsigned char minor, void *data, blkno_t block)
	{
	unsigned long temp;
	unsigned char retry, *p;
	static unsigned char message[7]; /* cannot be in stack segment! */
	unsigned int i;
	unsigned char sysio_save;

#if DEBUG >= 3
	_dprintf(3, _("dev_cfl_write(0x%x, %u, 0x%x %u) starting\n"),
			error, (unsigned int)minor, data, block);
#endif
	if (block >= HD1_SIZE)
		{
		goto error1;
		}
	temp = (unsigned long)block + HD1_START;

	sysio_save = my_thread_p->sysio;
	my_thread_p->sysio = 1;

	while (Cfl_busy)
		{
		kwait(&Cfl_busy);
		}
	Cfl_busy = 1;

	message[0] = 0x1b;
	message[1] = 'W';
	message[2] = (unsigned char)temp;
	message[3] = (unsigned char)(temp >> 8);
	message[4] = (unsigned char)(temp >> 16);
	message[5] = (unsigned char)(temp >> 24);

	retry = message[2] + message[3] + message[4] + message[5];
	p = data;
	for (i = 0; i < BUFSIZE; i++)
		{
		retry += *p++;
		}
	message[6] = ~retry;

	for (retry = 0; retry < 3; retry++)
		{
#if DEBUG >= 2
		_dprintf(2, _("cfl write 0x%lx "), temp);
#endif
		Cfl_message = message;

		if (async_write(&Api[6].async, message, 6) != 6 ||
				async_write(&Api[6].async, data, BUFSIZE) != BUFSIZE ||
				async_write(&Api[6].async, message + 6, 1) != 1)
			{
			/* disaster has struck, no point retrying here */
			Cfl_message = NULL;
			goto error2;
			}

		/* command has been sent, wait for response via devapi.c */
		kalarm(1000L);
		while (Cfl_message)
			{
			if (kwait(&Cfl_message) == EALARM)
				{
				/* woken by signal */
#if DEBUG >= 2
				_dprintf(2, _("timeout "));
#endif
				goto abort;
				}
			}
		kalarm(0L);

		if (Cfl_error)
			{
#if DEBUG >= 2
			_dprintf(2, _("error %u "), Cfl_error);
#endif
			goto abort;
			}
#if DEBUG >= 2
		_dprintf(2, _("ok\n"));
#endif

		Cfl_busy = 0;
		ksignal(&Cfl_busy, 1);

		my_thread_p->sysio = sysio_save;
#if DEBUG >= 3
		_dprintf(3, _("dev_cfl_write() returning\n"));
#endif
		return;

	abort:
		Cfl_message = NULL;
#if DEBUG >= 2
		_dprintf(2, _("retry %u\n"), (unsigned int)retry);
#endif
		}

error2:
	Cfl_busy = 0;
	ksignal(&Cfl_busy, 1);

	my_thread_p->sysio = sysio_save;

error1:
#if DEBUG >= 3
	_dprintf(3, _("dev_cfl_write() error EIO\n"));
#endif
	longjmp(error, EIO);
	}
#endif

