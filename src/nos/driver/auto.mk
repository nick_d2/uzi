# auto.mk by Nick for Hytech NOS/UZI project

# -----------------------------------------------------------------------------

SUBDIRS=

OUTPUTS=	driver.$(LIBEXT)

driver_$(LIBEXT)_SOURCES= \
		bufpool.c devapi.c devcfl.c device.c devkmem.c devnull.c \
		devram.c devtty.c devzero.c rtc.c

bufpool_c_MODULES= \
		block_buffer_list bufclock buf_hits buf_miss buf_flsh \
		blk_acquire blk_release bread bfree zerobuf bufsync bfind \
		freebuf freebuf_expand freebuf_nondirty freebuf_dirty \
		freebuf_oldest bufdump bufpool_garbage

devapi_c_MODULES= \
		dev_api api_minor_list api_minor_bufsize dev_api_open \
		dev_api_close dev_api_read dev_api_write dev_api_ioctl \
		api_minor_thread api_minor_thread_lcd0 api_minor_thread_cfl0 \
		api_minor_garbage

devcfl_c_MODULES= \
		dev_cfl Cfl_refs Cfl_busy Cfl_data Cfl_message Cfl_error \
		dev_cfl_open dev_cfl_close dev_cfl_read dev_cfl_write

device_c_MODULES= \
		device_list device_init device_find

devkmem_c_MODULES= \
		dev_kmem dev_kmem_read

devnull_c_MODULES= \
		dev_null dev_null_init dev_null_read dev_null_write \
		dev_null_ioctl

devram_c_MODULES= \
		dev_ram dev_ram_read dev_ram_write

devtty_c_MODULES= \
		dev_tty tty_minor_list tty_minor_bufsize dev_tty_open \
		dev_tty_close dev_tty_read dev_tty_write dev_tty_ioctl \
		tty_minor_thread tty_minor_garbage ttydriv tty_read tty_write \
		tty_fputs _tty_fputs get_tty put_tty
#tty_focus

devzero_c_MODULES= \
		dev_zero dev_zero_read

rtc_c_MODULES=	Rtc Rtc_protect rtc_time rtc_stime

# -----------------------------------------------------------------------------

