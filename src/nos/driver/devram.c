/* devram.c by Nick for NOS/UZI project */

#include <errno.h>
#include "nos/global.h" /* for copyr() */
#include "driver/device.h" /* for struct device_block_s */
#include "driver/devnull.h" /* this will include devio.h */
#include "driver/devram.h"
#include "kernel/dprintf.h"
#include <libintl.h>
#include "po/messages.h"

#ifndef DEBUG
#define DEBUG 0
#endif

#ifndef MODULE
#define MODULE_dev_ram
#define MODULE_dev_ram_read
#define MODULE_dev_ram_write
#endif

#ifdef MODULE_dev_ram
struct device_block_s dev_ram =
	{
		{
		1,			/* minors */
		dev_null_init,		/* init */
		dev_null_init,		/* open */
		dev_null_init,		/* close */
		dev_null_ioctl		/* ioctl */
		},
	dev_ram_read,		/* read */
	dev_ram_write		/* write */
	};
#endif

#ifdef MODULE_dev_ram_read
void
dev_ram_read(jmp_buf error, unsigned char minor, void *data, blkno_t block)
	{
	unsigned long temp;

#if DEBUG >= 3
	_dprintf(3, _("dev_ram_read(0x%x, %u, 0x%x, %u) starting\n"),
			error, (unsigned int)minor, data, block);
#endif
	if (block >= HD0_SIZE)
		{
#if DEBUG >= 3
		_dprintf(3, _("dev_ram_read() error EIO\n"));
#endif
		longjmp(error, EIO);
		}
	temp = (unsigned long)block + HD0_START;

#if DEBUG >= 2
	_dprintf(2, _("ram read 0x%lx "), temp);
#endif
	copyr(temp << BUFSIZELOG, (unsigned long)data, BUFSIZE);
#if DEBUG >= 2
	_dprintf(2, _("ok\n"));
#endif
#if DEBUG >= 3
	_dprintf(3, _("dev_ram_read() returning\n"));
#endif
	}
#endif

#ifdef MODULE_dev_ram_write
void
dev_ram_write(jmp_buf error, unsigned char minor, void *data, blkno_t block)
	{
	unsigned long temp;

#if DEBUG >= 3
	_dprintf(3, _("dev_ram_write(0x%x, %u, 0x%x, %u) starting\n"),
			error, (unsigned int)minor, data, block);
#endif
	if (block >= HD0_SIZE)
		{
#if DEBUG >= 3
		_dprintf(3, _("dev_ram_write() error EIO\n"));
#endif
		longjmp(error, EIO);
		}
	temp = (unsigned long)block + HD0_START;

#if DEBUG >= 2
	_dprintf(2, _("ram write 0x%lx "), temp);
#endif
	copyr((unsigned long)data, temp << BUFSIZELOG, BUFSIZE);
#if DEBUG >= 2
	_dprintf(2, _("ok\n"));
#endif
#if DEBUG >= 3
	_dprintf(3, _("dev_ram_write() returning\n"));
#endif
	}
#endif

