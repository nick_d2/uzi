/*
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

#include <errno.h>
#include "driver/device.h"
#include "driver/devram.h"
#include "driver/devcfl.h"
#include "driver/devnull.h"
#include "driver/devzero.h"
#include "driver/devkmem.h"
#include "driver/devtty.h"
#include "driver/devapi.h"
#include "kernel/dprintf.h"
#include <libintl.h>
#include "po/messages.h"

#ifndef MODULE
#define MODULE_device_list
#define MODULE_device_init
#define MODULE_device_find
#endif

#ifdef MODULE_device_list
struct device_s *device_list[DEVICE_LIST] =
	{
	&dev_ram.device,		/* major 0 (block) */
	&dev_cfl.device,		/* major 1 (block) */
	&dev_null.device,		/* major 2 (char) */
	&dev_zero.device,		/* major 3 (char) */
	&dev_kmem.device,		/* major 4 (char) */
	&dev_tty.device,		/* major 5 (char) */
	&dev_api.device			/* major 6 (char) */
	};
#endif

#ifdef MODULE_device_init
void
device_init(void)
	{
	jmp_buf try_b;
	unsigned int catch;
	register struct device_s **device_pp, *device_p;
	unsigned char minor, minors;

	for (device_pp = device_list; device_pp < device_list + DEVICE_LIST;
			device_pp++)
		{
		catch = setjmp(try_b);
		if (catch)
			{
			_panic(_("device_init() major %u minor %u error %u"),
					device_pp - device_list, minor, catch);
			}
		device_p = *device_pp;
		minors = device_p->minors;
		for (minor = 0; minor < minors; minor++)
			{
			device_p->init(try_b, minor);
			}
		}
	}
#endif

#ifdef MODULE_device_find
struct device_s *
device_find(jmp_buf error, dev_t dev)
	{
	unsigned char major;
	register struct device_s *device_p;

/* abyte('.'); */
/* ahexw((unsigned int)&dev); */
	major = MAJOR(dev);
	if (major < DEVICE_LIST)
		{
		device_p = device_list[major];
		if (MINOR(dev) < device_p->minors)
			{
			return device_p;
			}
		}
	longjmp(error, ENODEV);
	}
#endif

