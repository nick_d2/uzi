/*
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 Buffer pool management routines
**********************************************************/

#include <errno.h>
#include "nos/global.h" /* for ksignal() and kwait() */
#include "driver/bufpool.h"
#include "driver/device.h" /* for device_find(), struct device_block_s */
#include "filesys/filesys.h" /* for Baddevmsg */
#include "kernel/dprintf.h"
#include "main/mbuf.h"
#include <libintl.h>
#include "po/messages.h"

#ifndef DEBUG
#define DEBUG 0
#endif

#ifdef MODULE
#define STATIC
#else
#define STATIC static
#define MODULE_block_buffer_list
#define MODULE_bufclock
#define MODULE_buf_hits
#define MODULE_buf_miss
#define MODULE_buf_flsh
#define MODULE_blk_acquire
#define MODULE_blk_release
#define MODULE_bread
#define MODULE_bfree
#define MODULE_zerobuf
#define MODULE_bufsync
#define MODULE_bfind
#define MODULE_freebuf
#define MODULE_freebuf_expand
#define MODULE_freebuf_nondirty
#define MODULE_freebuf_dirty
#define MODULE_freebuf_oldest
#define MODULE_bufdump
#define MODULE_bufpool_garbage
#endif

#ifdef MODULE_block_buffer_list
#if 1
struct block_buffer_s block_buffer_list[BLOCK_BUFFER_LIST];
#else
struct block_buffer_s *block_buffer_list[BLOCK_BUFFER_LIST];
#endif
#endif

#ifdef MODULE_bufclock
unsigned int bufclock;		/* time-stamp counter for LRU */
#endif

#ifdef MODULE_buf_hits
unsigned int buf_hits;		/* buffer pool hits */
#endif
#ifdef MODULE_buf_miss
unsigned int buf_miss;		/* buffer pool misses */
#endif
#ifdef MODULE_buf_flsh
unsigned int buf_flsh;		/* buffer pool flushes */
#endif

extern unsigned char bufpool_timeout; /* please see alloc.c gcollect() */
/*extern unsigned char memory_situation;*/ /* please see alloc.c availmem() */

STATIC struct block_buffer_s *freebuf(jmp_buf error, dev_t dev);
STATIC struct block_buffer_s *freebuf_expand(void);
STATIC struct block_buffer_s *freebuf_nondirty(dev_t dev);
STATIC struct block_buffer_s *freebuf_dirty(dev_t dev);
STATIC struct block_buffer_s *freebuf_oldest(void);

/* Buffer pool management and generic device I/O */

/* The high-level interface is through bread() and bfree().
 *
 * Bread() is given a device and block number, and a rewrite flag.
 * If rewrite is 0, the block is actually read if it is not already
 * in the buffer pool. If rewrite is set, it is assumed that the caller
 * plans to rewrite the entire contents of the block, and it will
 * not be read in, but only have a buffer named after it. Also if
 * rewrite great then 1 bread() zeroes allocated buffer.
 *
 * Bfree() is given a buffer pointer and a dirty flag.
 * The dirty flag ored with buffer bf_dirty and if result is 0, the buffer
 * is made available for further use, else the buffer is marked "dirty", and
 * it will eventually be written out to disk.  If the flag is 2, the buffer
 * will be immediately written out to disk.
 *
 * Zerobuf() returns a buffer of zeroes not belonging to any device.
 * It must be bfree'd after use, and must not be dirty.
 * It is used when a read() wants to read an unallocated block of a file.
 *
 * Bufsync() write outs all dirty blocks
 *
 * Note that a pointer to a buffer structure is the same as a
 * pointer to the data.  This is very important.
 */

/* blk_acquire() waits to acquire the block's lock and then returns.
 * NOTE!  NOT REALLY ALLOWED TO USE THIS, BECAUSE blkbuf_p MIGHT POINT
 * TO INVALID MEMORY BY THE TIME WE WAKE UP, SEE bufpool_garbage() !!!
 */

#ifdef MODULE_blk_acquire
void
blk_acquire(struct block_buffer_s *blkbuf_p)
{
	/* unsigned int i_state; */

#if DEBUG >= 3
 _dprintf(3, _("blk_acquire(0x%x) starting\n"), blkbuf_p);
#endif
	/* i_state = dirps(); */
	while (blkbuf_p->bf_busy)
		{
		/* restore(i_state); */
		kwait(block_buffer_list);
		/* i_state = dirps(); */
		}
	blkbuf_p->bf_busy = 1;
	/* restore(i_state); */
#if DEBUG >= 3
 _dprintf(3, _("blk_acquire() returning\n"));
#endif
}
#endif

/* blk_release() unlocks the block, it is assumed to have been locked.
 */

#ifdef MODULE_blk_release
void
blk_release(struct block_buffer_s *blkbuf_p)
{
#if DEBUG >= 3
 _dprintf(3, _("blk_release(0x%x) starting\n"), blkbuf_p);
#endif
	blkbuf_p->bf_busy = 0;
	ksignal(block_buffer_list, 0);
#if DEBUG >= 3
 _dprintf(3, _("blk_release() returning\n"));
#endif
}
#endif

/* read block blk from device dev, return buffer with this block */
#ifdef MODULE_bread
struct block_buffer_s *
bread(jmp_buf error, dev_t dev, blkno_t blk, unsigned char rewrite)
{
	jmp_buf try_b;
	unsigned int catch;
	register struct block_buffer_s *blkbuf_p;

#if DEBUG >= 3
	_dprintf(3, _("bread(0x%x, 0x%x, %u, %u) starting\n"),
		error, dev, blk, rewrite);
#endif

/* abyte('a'); */
	blkbuf_p = bfind(dev, blk);
/* abyte('b'); */
	if (blkbuf_p)
		{
/* abyte('d'); */
		++buf_hits;
/* abyte('e'); */
		goto Done;
		}
/* abyte('f'); */
	++buf_miss;
	/* block not found in pool - allocate free buffer for them */
/* abyte('g'); */
	blkbuf_p = freebuf(NULL, dev); /* this will wait if none available */
/* abyte('r'); */
	blkbuf_p->bf_busy = 1;
/* abyte('h'); */
	blkbuf_p->bf_dev = dev;
/* abyte('i'); */
	blkbuf_p->bf_blk = blk;
	/* If rewrite is set, we are about to write over the entire block,
	 * so we don't need the previous contents
	 */
/* abyte('j'); */
	if (rewrite == 0) {		/* we must read block data */
/* abyte('k'); */
		catch = setjmp(try_b);
		if (catch)
			{
/* abyte('l'); */
			if (setjmp(try_b) == 0)
				{
				bfree(try_b, blkbuf_p, 0);
				}
#if DEBUG >= 3
			_dprintf(3, _("bread() error %u\n"), catch);
#endif
			longjmp(error, catch);
			}
		((struct device_block_s *)device_find(try_b, dev))->
				read(try_b, MINOR(dev),
				blkbuf_p->bf_data, blkbuf_p->bf_blk);
/* abyte('n'); */
	}
Done:
/* abyte('o'); */
	if (rewrite > 1)		/* we need really zeroed block */
		{
/* abyte('p'); */
		memset(blkbuf_p->bf_data, 0, BUFSIZE);
/* abyte('q'); */
		}
/* abyte('s'); */
	blkbuf_p->bf_time = ++bufclock;	/* Time stamp it */
#if DEBUG >= 3
	_dprintf(3, _("bread() returning 0x%x\n"), blkbuf_p);
#endif
/* abyte('t'); */
	return blkbuf_p;
}
#endif

/* free not needed now buffer */

#ifdef MODULE_bfree
void
bfree(jmp_buf error, struct block_buffer_s *blkbuf_p, unsigned char dirty)
{
	jmp_buf try_b;
	unsigned int catch;
	dev_t dev;

#if DEBUG >= 3
	_dprintf(3, _("bfree(0x%x, 0x%x, %u) starting\n"),
			error, blkbuf_p, dirty);
#endif
	blkbuf_p->bf_dirty |= dirty;
	if (blkbuf_p->bf_dirty && blkbuf_p->bf_dev == NULLDEV)
		{
		_panic(_("attempt to write zerobuf"));
		}
	if (blkbuf_p->bf_dirty >= 2)
		{ /* Extra dirty */
		dev = blkbuf_p->bf_dev;
		catch = setjmp(try_b);
		if (catch)
			{
			blk_release(blkbuf_p);
#if DEBUG >= 3
			_dprintf(3, _("bfree() error %u\n"), catch);
#endif
			longjmp(error, catch);
			}
		((struct device_block_s *)device_find(try_b, dev))->
				write(try_b, MINOR(dev),
				blkbuf_p->bf_data, blkbuf_p->bf_blk);
		blkbuf_p->bf_dirty = 0;
		}
	else if (blkbuf_p->bf_dirty >= 1)
		{
		/* i_state = dirps(); */
/* abyte('|'); */
		bufpool_timeout = 25;
		/* restore(i_state); */
		}
	if (blkbuf_p->bf_busy == 0)
		{
		_dprintf(0, _("bfree(): not acquired\n"));
		}
	blk_release(blkbuf_p);
#if DEBUG >= 3
	_dprintf(3, _("bfree() returning\n"));
#endif
}
#endif

/* Returns a free zeroed buffer with no associated device.
 * It is essentially a malloc for the kernel. Free it with bfree().
 * This procedure able to throw ENOMEM only if error != NULL
 */

#ifdef MODULE_zerobuf
struct block_buffer_s *
zerobuf(jmp_buf error)
	{
	static int blk = 0;
	register struct block_buffer_s *blkbuf_p;

#if DEBUG >= 3
	_dprintf(3, _("zerobuf(0x%x) starting\n"), error);
#endif
	blkbuf_p = freebuf(error, NULLDEV);
	blkbuf_p->bf_dev = NULLDEV;
	blkbuf_p->bf_blk = ++blk;
	blkbuf_p->bf_busy = 1;
	blkbuf_p->bf_time = ++bufclock;
	memset(blkbuf_p->bf_data, 0, BUFSIZE);
#if DEBUG >= 3
	_dprintf(3, _("zerobuf() returning 0x%x\n"), blkbuf_p);
#endif
	return blkbuf_p;
	}
#endif

/* flush all dirty buffers  */
/* if dev != NULLDEV, only flush buffers for this device */
/* if drop != 0, all buffers for (all) dev are deallocated */
/* (dirty buffers are flushed before being deallocated) */

#ifdef MODULE_bufsync
void
bufsync(dev_t fdev, unsigned char drop)
	{
	jmp_buf try_b;
	unsigned int catch;
	dev_t dev;
#if 1
	struct block_buffer_s *blkbuf_p;
	struct mbuf_s *mbuf_p;
#else
	struct block_buffer_s **blkbuf_pp, *blkbuf_p;
#endif

#if DEBUG >= 3
	_dprintf(3, _("bufsync(0x%x, %d) starting\n"), fdev, drop);
#endif
restart:
#if 1
	for (blkbuf_p = block_buffer_list;
			blkbuf_p < block_buffer_list + BLOCK_BUFFER_LIST;
			blkbuf_p++)
		{
		if (blkbuf_p->bf_data)
#else
	for (blkbuf_pp = block_buffer_list;
			blkbuf_pp < block_buffer_list + BLOCK_BUFFER_LIST;
			blkbuf_pp++)
		{
		blkbuf_p = *blkbuf_pp;
		if (blkbuf_p)
#endif
			{
			if (blkbuf_p->bf_busy)
				{
				kwait(block_buffer_list);
				goto restart;
				}
			blkbuf_p->bf_busy = 1;
			dev = blkbuf_p->bf_dev;
			if (dev != NULLDEV && (fdev == NULLDEV || fdev == dev))
				{
				if (blkbuf_p->bf_dirty)
					{
					blkbuf_p->bf_busy = 1;
					catch = setjmp(try_b);
					if (catch)
						{
						_dprintf(0, _("bufsync() error %u\n"), catch);
						}
					else
						{
						((struct device_block_s *)device_find(try_b, dev))->
								write(try_b, MINOR(dev),
								blkbuf_p->bf_data,
								blkbuf_p->bf_blk);
						blkbuf_p->bf_dirty = 0;
						}
					blk_release(blkbuf_p);
					}
				/* extra logic to flush cache after unmounting */
				if (drop)
					{
#if 1
					mbuf_p = (struct mbuf_s *)
							blkbuf_p->bf_data - 1;
					free_mbuf(&mbuf_p);
					blkbuf_p->bf_data = NULL;
#else
					free(blkbuf_p);
					*blkbuf_pp = NULL;
#endif
					}
				}
			blk_release(blkbuf_p);
			}
		}
#if DEBUG >= 3
	_dprintf(3, _("bufsync() returning\n"));
#endif
	}
#endif


#ifdef MODULE_bfind
struct block_buffer_s *
bfind(dev_t dev, blkno_t blk)
{
#if 1
	struct block_buffer_s *blkbuf_p;
#else
	struct block_buffer_s **blkbuf_pp, *blkbuf_p;
#endif

#if DEBUG >= 3
	_dprintf(3, _("bfind(0x%x, %u) starting\n"), dev, blk);
#endif

restart:
#if 1
	for (blkbuf_p = block_buffer_list;
			blkbuf_p < block_buffer_list + BLOCK_BUFFER_LIST;
			blkbuf_p++)
		{
		if (blkbuf_p->bf_data &&
#else
	for (blkbuf_pp = block_buffer_list;
			blkbuf_pp < block_buffer_list + BLOCK_BUFFER_LIST;
			blkbuf_pp++)
		{
		blkbuf_p = *blkbuf_pp;
        	if (blkbuf_p &&
#endif
				blkbuf_p->bf_dev == dev &&
				blkbuf_p->bf_blk == blk)
			{
			if (blkbuf_p->bf_busy)
				{
				kwait(block_buffer_list);
				goto restart;
				}
#if DEBUG >= 3
			_dprintf(3, _("bfind() returning 0x%x, found (index %u)\n"),
#if 1
					blkbuf_p, blkbuf_p - block_buffer_list);
#else
					blkbuf_p, blkbuf_pp - block_buffer_list);
#endif
#endif
			blkbuf_p->bf_busy = 1; /* acquire it */
			return blkbuf_p;
			}
		}
#if DEBUG >= 3
	_dprintf(3, _("bfind() returning NULL, not found\n"));
#endif
	return NULL;
}
#endif

/* find free buffer in pool or freeing oldest
 * This procedure able to throw ENOMEM only if error != NULL
 */

#ifdef MODULE_freebuf
STATIC struct block_buffer_s *
freebuf(jmp_buf error, dev_t fdev)
	{
	jmp_buf try_b;
	unsigned int catch;
	dev_t dev;
	struct block_buffer_s *blkbuf_p;

#if DEBUG >= 3
	_dprintf(3, _("freebuf(0x%x, 0x%x) starting\n"), error, fdev);
#endif
	/* Try to find a non-busy buffer and
	 * write out the data if it is dirty
	 */
restart:
	while (1)
		{
		switch (availmem()) /*memory_situation)*/
			{
		case 0: /* plenty */
			if (MAJOR(fdev) == 1) /* only for compactflash */
				{
				blkbuf_p = freebuf_expand();
				if (blkbuf_p == NULL)
					{
					blkbuf_p = freebuf_nondirty(fdev);
					if (blkbuf_p == NULL)
						{
						blkbuf_p = freebuf_dirty(fdev);
						}
					}
				break;
				}
			/* otherwise fall thru */

		case 1: /* yellow */
/* abyte('('); */
			blkbuf_p = freebuf_nondirty(fdev);
/* abyte(')'); */
			if (blkbuf_p == NULL)
				{
				blkbuf_p = freebuf_dirty(fdev);
				if (blkbuf_p == NULL)
					{
					blkbuf_p = freebuf_expand();
					}
				}
			break;

		case 2: /* red */
			blkbuf_p = freebuf_dirty(fdev);
			if (blkbuf_p == NULL)
				{
				blkbuf_p = freebuf_nondirty(fdev);
				if (blkbuf_p == NULL)
					{
					blkbuf_p = freebuf_expand();
					}
				}
			break;
			}

		/* once created by freebuf_expand(), buffers are dedicated */
		/* to a particular device, except when reassigned by this */
		if (blkbuf_p == NULL)
			{
			blkbuf_p = freebuf_oldest();
			}

		/* if found, by any of the above means, return it to caller */
		if (blkbuf_p)
			{
			break;
			}

		/* otherwise need to continue infinite loop and try again */
		if (error)
			{
#if DEBUG >= 3
			_dprintf(3, _("freebuf() error ENOMEM\n"));
#endif
			longjmp(error, ENOMEM);
			}
		kwait(block_buffer_list); /* wait for buffer */
		}

	/* can only get here with blkbuf_p non-NULL (ie. found) */
	if (blkbuf_p->bf_dirty)
		{
		dev = blkbuf_p->bf_dev;
		if (dev == NULLDEV)
			{
			_panic(_("attempt to write zerobuf"));
			}
		++buf_flsh;
		blkbuf_p->bf_busy = 1;
		catch = setjmp(try_b);
		if (catch)
			{
			blk_release(blkbuf_p);
			_dprintf(0, _("freebuf() error %u\n"), catch);
			if (error)
				{
				longjmp(error, catch);
				}
			/* something has gone drastically wrong, but we're */
			/* not allowed to return NULL - so we'll probably */
			/* keep finding the same block and trying to write */
			/* it, if error persists we're in an infinite loop */
			kwait(NULL);
			goto restart;
			}
		((struct device_block_s *)device_find(try_b, dev))->
				write(try_b, MINOR(dev),
				blkbuf_p->bf_data, blkbuf_p->bf_blk);
		blkbuf_p->bf_dirty = 0;
		blk_release(blkbuf_p);
		}
#if DEBUG >= 3
	_dprintf(3, _("freebuf() returning 0x%x\n"), blkbuf_p);
#endif
	return blkbuf_p;
	}
#endif

#ifdef MODULE_freebuf_expand
STATIC struct block_buffer_s *
freebuf_expand(void)
	{
#if 1
	struct block_buffer_s *blkbuf_p;
	struct mbuf_s *mbuf_p;
#else
	struct block_buffer_s **blkbuf_pp, *blkbuf_p;
#endif

	/* first a quick pass to see if we can reuse a NULLDEV entry */
#if 1
	for (blkbuf_p = block_buffer_list;
			blkbuf_p < block_buffer_list + BLOCK_BUFFER_LIST;
			blkbuf_p++)
		{
		if (blkbuf_p->bf_data &&
#else
	for (blkbuf_pp = block_buffer_list;
			blkbuf_pp < block_buffer_list + BLOCK_BUFFER_LIST;
			blkbuf_pp++)
		{
		blkbuf_p = *blkbuf_pp;
		if (blkbuf_p &&
#endif
				!blkbuf_p->bf_busy &&
				blkbuf_p->bf_dev == NULLDEV)
			{
#if DEBUG >= 2
			_dprintf(2, _("slot %u nulldev\n"),
#if 1
					blkbuf_p - block_buffer_list);
#else
					blkbuf_pp - block_buffer_list);
#endif
#endif
			return blkbuf_p;
			}
		}

	/* now look for a slot which doesn't have memory allocated yet */
#if 1
	for (blkbuf_p = block_buffer_list;
			blkbuf_p < block_buffer_list + BLOCK_BUFFER_LIST;
			blkbuf_p++)
		{
		if (blkbuf_p->bf_data == NULL)
			{
			mbuf_p = alloc_mbuf(BUFSIZE);
			if (mbuf_p == NULL)
				{
				break; /* can't expand, no memory */
				}
			blkbuf_p->bf_data = (unsigned char *)(mbuf_p + 1);
#else
	for (blkbuf_pp = block_buffer_list;
			blkbuf_pp < block_buffer_list + BLOCK_BUFFER_LIST;
			blkbuf_pp++)
		{
		blkbuf_p = *blkbuf_pp;
		if (blkbuf_p == NULL)
			{
			blkbuf_p = calloc(sizeof(struct block_buffer_s), 1);
			if (blkbuf_p == NULL)
				{
				break; /* can't expand, no memory */
				}
			*blkbuf_pp = blkbuf_p;
#endif
#if DEBUG >= 2
			_dprintf(2, _("slot %u expand\n"),
#if 1
					blkbuf_p - block_buffer_list);
#else
					blkbuf_pp - block_buffer_list);
#endif
#endif
			return blkbuf_p;
			}
		}

	return NULL;
	}
#endif

#ifdef MODULE_freebuf_nondirty
STATIC struct block_buffer_s *
freebuf_nondirty(dev_t dev)
	{
#if 1
	struct block_buffer_s *blkbuf_p;
#else
	struct block_buffer_s **blkbuf_pp, *blkbuf_p;
#endif
	unsigned int diff, oldtime;
	struct block_buffer_s *oldest;
#if DEBUG >= 2
	unsigned char slot;
#endif

	oldest = NULL;
	oldtime = 0;
#if 1
	for (blkbuf_p = block_buffer_list;
			blkbuf_p < block_buffer_list + BLOCK_BUFFER_LIST;
			blkbuf_p++)
		{
		if (blkbuf_p->bf_data &&
#else
	for (blkbuf_pp = block_buffer_list;
			blkbuf_pp < block_buffer_list + BLOCK_BUFFER_LIST;
			blkbuf_pp++)
		{
		blkbuf_p = *blkbuf_pp;
		if (blkbuf_p &&
#endif
				!blkbuf_p->bf_busy &&
				!blkbuf_p->bf_dirty &&
				blkbuf_p->bf_dev == dev)
			{
			diff = bufclock - blkbuf_p->bf_time;
			if (diff >= oldtime)
				{
#if DEBUG >= 2
#if 1
				slot = blkbuf_p - block_buffer_list;
#else
				slot = blkbuf_pp - block_buffer_list;
#endif
#endif
				oldest = blkbuf_p;
				oldtime = diff;
				}
			}
		}

#if DEBUG >= 2
	if (oldest)
		{
		_dprintf(2, _("slot %u nondirty\n"), slot);
		}
#endif
	return oldest;
	}
#endif

#ifdef MODULE_freebuf_dirty
STATIC struct block_buffer_s *
freebuf_dirty(dev_t dev)
	{
#if 1
	struct block_buffer_s *blkbuf_p;
#else
	struct block_buffer_s **blkbuf_pp, *blkbuf_p;
#endif
	unsigned int diff, oldtime;
	struct block_buffer_s *oldest;
#if DEBUG >= 2
	unsigned char slot;
#endif

	oldest = NULL;
	oldtime = 0;
#if 1
	for (blkbuf_p = block_buffer_list;
			blkbuf_p < block_buffer_list + BLOCK_BUFFER_LIST;
			blkbuf_p++)
		{
		if (blkbuf_p->bf_data &&
#else
	for (blkbuf_pp = block_buffer_list;
			blkbuf_pp < block_buffer_list + BLOCK_BUFFER_LIST;
			blkbuf_pp++)
		{
		blkbuf_p = *blkbuf_pp;
		if (blkbuf_p &&
#endif
				!blkbuf_p->bf_busy &&
				blkbuf_p->bf_dirty &&
				blkbuf_p->bf_dev == dev)
			{
			diff = bufclock - blkbuf_p->bf_time;
			if (diff >= oldtime)
				{
#if DEBUG >= 2
#if 1
				slot = blkbuf_p - block_buffer_list;
#else
				slot = blkbuf_pp - block_buffer_list;
#endif
#endif
				oldest = blkbuf_p;
				oldtime = diff;
				}
			}
		}

#if DEBUG >= 2
	if (oldest)
		{
		_dprintf(2, _("slot %u dirty\n"), slot);
		}
#endif
	return oldest;
	}
#endif

#ifdef MODULE_freebuf_oldest
STATIC struct block_buffer_s *
freebuf_oldest(void)
	{
#if 1
	struct block_buffer_s *blkbuf_p;
#else
	struct block_buffer_s **blkbuf_pp, *blkbuf_p;
#endif
	unsigned int diff, oldtime;
	struct block_buffer_s *oldest;
#if DEBUG >= 2
	unsigned char slot;
#endif

	oldest = NULL;
	oldtime = 0;
#if 1
	for (blkbuf_p = block_buffer_list;
			blkbuf_p < block_buffer_list + BLOCK_BUFFER_LIST;
			blkbuf_p++)
		{
		if (blkbuf_p->bf_data &&
#else
	for (blkbuf_pp = block_buffer_list;
			blkbuf_pp < block_buffer_list + BLOCK_BUFFER_LIST;
			blkbuf_pp++)
		{
		blkbuf_p = *blkbuf_pp;
		if (blkbuf_p &&
#endif
				!blkbuf_p->bf_busy)
			{
			diff = bufclock - blkbuf_p->bf_time;
			if (diff >= oldtime)
				{
#if DEBUG >= 2
#if 1
				slot = blkbuf_p - block_buffer_list;
#else
				slot = blkbuf_pp - block_buffer_list;
#endif
#endif
				oldest = blkbuf_p;
				oldtime = diff;
				}
			}
		}

#if DEBUG >= 2
	if (oldest)
		{
		_dprintf(2, _("slot %u oldest\n"), slot);
		}
#endif
	return oldest;
	}
#endif

/* dump buffers info for debug */
#ifdef MODULE_bufdump
int
bufdump(argc,argv,p)
int argc;
char *argv[];
void *p;
{
#if 1
	struct block_buffer_s *blkbuf_p;
#else
	struct block_buffer_s **blkbuf_pp, *blkbuf_p;
#endif

	_printf(_("Buf hits/miss/flsh: %u/%u/%u\n"
		/*"Bufs:\t"*/ "dev\tblock\tDB" /*"P"*/ "\ttime\t(clock=%d)\n"),
		buf_hits, buf_miss, buf_flsh, bufclock);
#if 1
	for (blkbuf_p = block_buffer_list;
			blkbuf_p < block_buffer_list + BLOCK_BUFFER_LIST;
			blkbuf_p++)
		{
		if (blkbuf_p->bf_data)
#else
	for (blkbuf_pp = block_buffer_list;
			blkbuf_pp < block_buffer_list + BLOCK_BUFFER_LIST;
			blkbuf_pp++)
		{
		blkbuf_p = *blkbuf_pp;
		if (blkbuf_p)
#endif
			{
			_printf(_("%04x\t%5u\t%c%c\t%u\n"),
					blkbuf_p->bf_dev, blkbuf_p->bf_blk,
					blkbuf_p->bf_dirty ? 'd' : '-',
					blkbuf_p->bf_busy ? 'b' : '-',
					blkbuf_p->bf_time);
			}
		}
	buf_miss = buf_hits = buf_flsh = 0;
	return 0;
}
#endif

#ifdef MODULE_bufpool_garbage
/* bufpool garbage collection - called by storage allocator when free
 * space runs low.  Non-dirty buffers are discarded.  If the situation
 * is red, dirty buffers are flushed to disk, and then discarded also.
 */

/* note: this should be merged with bufsync() by passing drop == red? */

void
bufpool_garbage(int red)
	{
	jmp_buf try_b;
	unsigned int catch;
#if 1
	struct block_buffer_s *blkbuf_p;
	struct mbuf_s *mbuf_p;
#else
	struct block_buffer_s **blkbuf_pp, *blkbuf_p;
#endif
	dev_t dev;

#if 1
	for (blkbuf_p = block_buffer_list;
			blkbuf_p < block_buffer_list + BLOCK_BUFFER_LIST;
			blkbuf_p++)
		{
		if (blkbuf_p->bf_data &&
#else
	for (blkbuf_pp = block_buffer_list;
			blkbuf_pp < block_buffer_list + BLOCK_BUFFER_LIST;
			blkbuf_pp++)
		{
		blkbuf_p = *blkbuf_pp;
		if (blkbuf_p &&
#endif
				blkbuf_p->bf_busy == 0)
			{
			if (blkbuf_p->bf_dirty)
				{
#if 0
				if (red == 0)
					{
					continue;
					}
#endif
				dev = blkbuf_p->bf_dev;
				if (dev == NULLDEV)
					{
					_panic(_("attempt to write zerobuf"));
					}
				buf_flsh++;
				blkbuf_p->bf_busy = 1;
				catch = setjmp(try_b);
				if (catch)
					{
					blk_release(blkbuf_p);
					_dprintf(0, _("bufpool_garbage() error %u\n"), catch);
					continue;
					}
				((struct device_block_s *)
						device_find(try_b, dev))->
						write(try_b, MINOR(dev),
						blkbuf_p->bf_data,
						blkbuf_p->bf_blk);
				blkbuf_p->bf_dirty = 0;
				blk_release(blkbuf_p);
				}
#if 1
			mbuf_p = (struct mbuf_s *)blkbuf_p->bf_data - 1;
			free_mbuf(&mbuf_p);
			blkbuf_p->bf_data = NULL;
#else
			*blkbuf_pp = NULL;
			free(blkbuf_p);
#endif
			/*ksignal(block_buffer_list, 0);*/
			}
		}
	}
#endif

