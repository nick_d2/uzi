/* devapi.c by Nick for NOS/UZI project */

#include <errno.h>
#include <sgtty.h>
#include <string.h>
#include "driver/device.h" /* for struct device_char_s */
#include "kernel/dprintf.h"
#include "kernel/thread.h" /* for my_thread_p */
#include "driver/devnull.h" /* this will include devio.h */
#include "driver/devapi.h"
#include "driver/devcfl.h" /* for Cfl_data, Cfl_message, Cfl_thread */
#include "kernel/usrmem.h"
#include "nos/global.h" /* for copyr() */
#include "z80/abus.h"
#include "main/iface.h"
#include "main/main.h" /* for driver_process_p */
#include <libintl.h>
#include "po/messages.h"

#ifndef DEBUG
#define DEBUG 0
#endif

#ifdef MODULE
#define STATIC
extern unsigned char api_minor_bufsize[API_MINOR_LIST];
#else
#define STATIC static
#define MODULE_dev_api
#define MODULE_api_minor_list
#define MODULE_api_minor_bufsize
#define MODULE_dev_api_open
#define MODULE_dev_api_close
#define MODULE_dev_api_read
#define MODULE_dev_api_write
#define MODULE_dev_api_ioctl
#define MODULE_api_minor_thread
#define MODULE_api_minor_thread_lcd0
#define MODULE_api_minor_thread_cfl0
#define MODULE_api_minor_garbage
#endif

#ifdef MODULE_dev_api
struct device_char_s dev_api =
	{
		{
		API_MINOR_LIST,		/* minors */
		dev_null_init,		/* init */
		dev_api_open,		/* open */
		dev_api_close,		/* close */
		dev_api_ioctl		/* ioctl */
		},
	dev_api_read,		/* read */
	dev_api_write		/* write */
	};
#endif

#ifdef MODULE_api_minor_list
struct api_minor_s *api_minor_list[API_MINOR_LIST];
#endif

#ifdef MODULE_api_minor_bufsize
STATIC unsigned char api_minor_bufsize[API_MINOR_LIST] =
	{
	0x80,			/* 0 loadcell */
	0x80,			/* 1 operator lcd */
	0x80,			/* 2 customer lcd */
	0x80,			/* 3 */
	0x80,			/* 4 thermal printer */
	0x80,			/* 5 */
	0x80,			/* 6 compactflash */
	0x80,			/* 7 */
	0x80			/* 8 */
	};
#endif

#ifdef MODULE_dev_api_open
void
dev_api_open(jmp_buf error, unsigned char minor)
	{
	struct api_minor_s **am_pp, *am_p;
	char thread_name[0x10];

#if DEBUG >= 3
	_dprintf(3, _("dev_api_open(0x%x, %u) starting\n"), error,
			(unsigned int)minor);
#endif
	am_pp = api_minor_list + minor;
	am_p = *am_pp;
	if (am_p == NULL)
		{
		am_p = malloc(sizeof(struct api_minor_s));
		if (am_p == NULL)
			{
/* abyte('b'); */
			goto error1;
			}

		memset(am_p, 0, sizeof(struct api_minor_s));
		am_p->minor = minor;

		*am_pp = am_p;
		}

	if (am_p->refs == (unsigned char)0)
		{
		if (Api[minor].async.iface) /* port is already attached */
			{
			goto error2;
			}

		if (abus_init(minor, &Loopback, /* ifp not applicable, kludge */
				api_minor_bufsize[minor], -1)) /* -1 means error */
			{
		error2:
#if DEBUG >= 3
			_dprintf(3, _("dev_api_open() error ENODEV\n"));
#endif
			longjmp(error, ENODEV);
			}

		sprintf(thread_name, "api%u rx", (unsigned int)minor);
		am_p->thread_p = process_thread_create(driver_process_p,
				thread_name, 128 /*256*/,
				(minor == 1) ? api_minor_thread_lcd0 :
				((minor == 6) ? api_minor_thread_cfl0 :
				api_minor_thread),
				0, NULL, am_p, 0);
		if (am_p->thread_p == NULL)
			{
/* abyte('c'); */
		error1:
#if DEBUG >= 3
			_dprintf(3, _("dev_api_open() error ENOMEM\n"));
#endif
			longjmp(error, ENOMEM);
			}
		}

	/* assert(am_p->refs != (unsigned char)0xff); */
	am_p->refs++;
#if DEBUG >= 3
	_dprintf(3, _("dev_api_open() returning\n"));
#endif
	}
#endif

#ifdef MODULE_dev_api_close
void
dev_api_close(jmp_buf error, unsigned char minor)
	{
	struct api_minor_s *am_p;

#if DEBUG >= 3
	_dprintf(3, _("dev_api_close(0x%x, %u) starting\n"), error,
			(unsigned int)minor);
#endif
	am_p = api_minor_list[minor];
	if (am_p && am_p->refs != (unsigned char)0)
		{
		am_p->refs--;
/* dprintf(0, "minor %d --refs %d\n", minor, am_p->refs); */
		if (am_p->refs == (unsigned char)0)
			{
			if (am_p->thread_p)
				{
				killproc(am_p->thread_p);
				}
			free_p(&am_p->queue_p);
			/* temporary kludge to avoid changing api.c */
			Loopback.dev = minor;
			abus_stop(&Loopback);
			}
		}
#if DEBUG >= 3
	_dprintf(3, _("dev_api_close() returning\n"));
#endif
	}
#endif

#ifdef MODULE_dev_api_read
size_t
dev_api_read(jmp_buf error, unsigned char minor, void *data, size_t count)
	{
	struct api_minor_s *am_p;
#if 0
	struct mbuf_s *bp;
	void *temp;
#endif

#if DEBUG >= 3
	_dprintf(3, _("dev_api_read(0x%x, %u, 0x%x, 0x%x) starting\n"),
			error, (unsigned int)minor, data, count);
#endif
	if (count == 0)
		{
#if DEBUG >= 3
		_dprintf(3, _("dev_api_read() returning 0\n"));
#endif
		return 0;
		}

	am_p = api_minor_list[minor];
	while (am_p->queue_p == 0)
		{
		if (am_p->nobl)
			{
#if DEBUG >= 3
			_dprintf(3, _("dev_api_read() error EWOULDBLOCK\n"));
#endif
			longjmp(error, EWOULDBLOCK);
			}

		if (kwait(am_p))
			{
#if DEBUG >= 3
			_dprintf(3, _("dev_api_read() error EINTR\n"));
#endif
			longjmp(error, EINTR);
			}
		}

#if 1
	count = pullup(&am_p->queue_p, data, count);
#else
	if (my_thread_p->sysio)
		{
		bp = NULL;
		temp = data;
		}
	else
		{
		bp = alloc_mbuf(count);
		if (bp == NULL)
			{
			longjmp(error, ENOMEM);
			}
		/* bp->cnt = count; */
		temp = bp->data;
		}

	count = pullup(&am_p->queue_p, temp, count);
	if (bp) /* my_thread_p->sysio == 0 */
		{
		usrput(data, temp, count);
		free_mbuf(&bp);
		}
#endif

#if DEBUG >= 3
	_dprintf(3, _("dev_api_read() returning 0x%x\n"), count);
#endif
	return count;
	}
#endif

#ifdef MODULE_dev_api_write
size_t
dev_api_write(jmp_buf error, unsigned char minor, void *data, size_t count)
	{
	struct mbuf_s *bp;
	void *temp;
	unsigned char sysio_save;

#if DEBUG >= 3
	_dprintf(3, _("dev_api_write(0x%x, %u, 0x%x, 0x%x) starting\n"),
			error, (unsigned int)minor, data, count);
#endif
	if (count == 0)
		{
#if DEBUG >= 3
		_dprintf(3, _("dev_api_write() returning 0\n"));
#endif
		return 0;
		}

	bp = NULL;
	temp = data;
	if (my_thread_p->sysio == 0)
		{
#if 1
		bp = qdata(data, count);
		if (bp == NULL)
			{
			longjmp(error, ENOMEM);
			}
#else
		bp = alloc_mbuf(count);
		if (bp == NULL)
			{
			longjmp(error, ENOMEM);
			}
		usrget(bp->data, temp, count);
		/* bp->cnt = count; */
#endif
		temp = bp->data;
		}

	sysio_save = my_thread_p->sysio;
	my_thread_p->sysio = 1;

	count = async_write(&Api[minor].async, temp, count);

	my_thread_p->sysio = sysio_save;
	free_mbuf(&bp);

	if (count == -1)
		{
#if DEBUG >= 3
		_dprintf(3, _("dev_api_write() error EIO\n"));
#endif
		longjmp(error, EIO);
		}
#if DEBUG >= 3
	_dprintf(3, _("dev_api_write() returning 0x%x\n"), count);
#endif
	return count;
	}
#endif

#ifdef MODULE_dev_api_ioctl
void
dev_api_ioctl(jmp_buf error, unsigned char minor,
		unsigned int request, void *data)
	{
	struct api_minor_s *am_p;
	struct sgttyb param;

#if DEBUG >= 3
	_dprintf(3, _("dev_api_ioctl(0x%x, %u, %u, 0x%x) starting\n"),
			error, (unsigned int)minor, request, data);
#endif
	am_p = api_minor_list[minor];
	switch (request)
		{
	case TIOCGETP:
		param.sg_flags = RAW;
		if (am_p->nobl)
			{
			param.sg_flags |= UNBUFF;
			}
		uput(&param, data, sizeof(struct sgttyb));
		break;
	case TIOCSETP:
		uget(data, &param, sizeof(struct sgttyb));
		am_p->nobl = ((param.sg_flags & UNBUFF) != 0);
		break;
#if 0
	case TIOCGETC:
		uput(&am_p->chars, data, sizeof(struct tchars));
		break;
	case TIOCSETC:
		uget(data, &am_p->chars, sizeof(struct tchars));
		break;
#if 0 /* look into this */
	case TIOCSETN:
		uput(&apiinq[minor].q_count, data, 2);
		break;
#endif
#endif
	case TIOCFLUSH:
		free_p(&am_p->queue_p);
		break;
#if 0
	case TIOCTLSET:
		am_p->state.ctrl = 1;
		break;
	case TIOCTLRES:
		am_p->state.ctrl = 0;
		break;
#endif
	default:
#if DEBUG >= 3
		_dprintf(3, _("dev_api_ioctl() error EINVAL\n"));
#endif
		longjmp(error, EINVAL);
		}
#if DEBUG >= 3
	_dprintf(3, _("dev_api_ioctl() returning\n"));
#endif
	}
#endif

#ifdef MODULE_api_minor_thread
void
api_minor_thread(int argc, void *argv, void *p)
	{
	unsigned int ch;
	struct mbuf_s *bp;

	while ((ch = async_getc(&Api[((struct api_minor_s *)p)->minor].async)) !=
			-1)
		{
		/* perform touchscreen special processing here */
		bp = qdata(&ch, 1);
		if (bp)
			{
			append(&((struct api_minor_s *)p)->queue_p, &bp);
			ksignal(p, 0); /* wake up all processes (important) */
			}

#if 0 /* temporarily out, as there's never much data at a time */
		/* Especially on slow machines, serial I/O can be quite
		 * compute intensive, so release the machine before we
		 * do the next line of input (keeps memory use down) */
		kwait(NULL);
#endif
		}

	if (((struct api_minor_s *)p)->thread_p == my_thread_p)
		{
		((struct api_minor_s *)p)->thread_p = NULL;
		}
	}
#endif

#ifdef MODULE_api_minor_thread_lcd0
void
api_minor_thread_lcd0(int argc, void *argv, void *p)
	{
	unsigned int ch;
	struct mbuf_s *bp;
	unsigned char valid, state;
	unsigned char message[8];

	state = 0;
	while ((ch = async_getc(&Api[((struct api_minor_s *)p)->minor].async)) !=
			-1)
		{
		valid = 0;
		switch (state)
			{
		case 0:
			if (ch == 0x1b)
				{
				message[state++] = ch;
				}
			else
				{
				valid = 1;
				}
			break;

		case 1:
			if (ch == 'T')
				{
				message[state++] = ch;
				}
			else
				{
				valid = 1;
				state = 0;
				}
			break;

		case 2: /* hour */
		case 3: /* mins */
		case 4: /* secs */
		case 5: /* month */
		case 6: /* day */
			message[state++] = ch;
			break;

		case 7: /* year-1980 */
			message[state] = ch;
			state = 0;

			/* skip 2 updates after setting the rtc time */
			/* allows for pipelining in the abus interface */
			if (Rtc_protect)
				{
				Rtc_protect--;
				break;
				}

			Rtc = (unsigned long)
					(((unsigned int)message[4] >> 1) |
				     	((unsigned int)message[3] << 5) |
				     	((unsigned int)message[2] << 11)) |
					((unsigned long)
					(((unsigned int)message[6]) |
					((unsigned int)message[5] << 5) |
					((unsigned int)message[7] << 9)) <<
					16);

#if 0			/* record start time when first rtc packet comes */
			if (StartTime == 0)
				{
				StartTime = Rtc;
				}
#endif

			break;
			}

		if (valid)
			{
			bp = qdata(&ch, 1);
			if (bp)
				{
				append(&((struct api_minor_s *)p)->queue_p, &bp);
				ksignal(p, 0); /* wake up all processes */
				}
#if 0 /* temporarily out, as there's never much data at a time */
			kwait(NULL);
#endif
			}
		}

	if (((struct api_minor_s *)p)->thread_p == my_thread_p)
		{
		((struct api_minor_s *)p)->thread_p = NULL;
		}
	}
#endif

#ifdef MODULE_api_minor_thread_cfl0
void
api_minor_thread_cfl0(int argc, void *argv, void *p)
	{
	unsigned int ch;
	struct mbuf_s *bp;
	unsigned char valid, state, check, there;
	unsigned char message[7];
	unsigned int count, error;
	unsigned char *data;
#if 0
 unsigned char statex = -1;
#endif

	state = 0;
	there = '1';
	while ((ch = async_getc(&Api[((struct api_minor_s *)p)->minor].async)) !=
			-1)
		{
#if 0
 if (state != statex)
  {
  abyte((unsigned int)state + '0');
  statex = state;
  }
#endif
		valid = 0;
		switch (state)
			{
		case 0:
			if (ch == 0x1b)
				{
				message[state++] = ch;
				}
			else
				{
				valid = 1;
				}
			break;

		case 1:
			switch (ch)
				{
			case 'R':
			case 'W':
			case 'r':
			case 'w':
				message[state++] = ch;
				check = 0xff;
				count = 0;
				error = 0;
				break;
			default:
				valid = 1;
				state = 0;
				break;
				}
			break;

		case 2: /* lba 0-7 */
		case 3: /* lba 8-15 */
		case 4: /* lba 16-23 */
			message[state++] = ch;
			check -= ch;
			break;

		case 5: /* lba 24-31 */
			message[state++] = ch;
			check -= ch;
			switch (message[1])
				{
			case 'R':
				if (Cfl_data == NULL)
					{
					/* no request anymore */
					state = 6;
					break;
					}
				/* user wants to read */
				state = 7;
				data = Cfl_data;
				break;
			case 'W':
				/* user wants to write */
				state = 8;
				break;
			case 'r':
			case 'w':
				/* error notification */
				state = 9;
				message[1] &= 0xdf; /* uppercase for memcmp */
				break;
				}
			break;

		case 6:	/* ignore data followed by checksum */
			if (count < BUFSIZE)
				{
				count++;
				break;
				}
			state = 0;
			break;

		case 7: /* read data followed by checksum */
			if (count < BUFSIZE)
				{
				if (data != Cfl_data)
					{
					/* user wants to abort */
					state = 6;
					count++;
					}
				else
					{
					data[count++] = ch;
					check -= ch;
					}
				break;
				}
			/* fall thru */

		case 8: /* write checksum */
/* acrlf(); */
/* ahexw(ch); */
/* acrlf(); */
/* ahexw(check); */
/* acrlf(); */
/* ahexw((unsigned int)Cfl_message); */
/* acrlf(); */
/* for (state = 0; state < 6; state++) */
/*  { */
/*  ahexb(message[state]); */
/*  } */
/* acrlf(); */
/* for (state = 0; state < 6; state++) */
/*  { */
/*  ahexb(Cfl_message[state]); */
/*  } */
/* acrlf(); */
			if (ch == check && Cfl_message &&
					memcmp(message, Cfl_message, 6)	== 0)
				{
				Cfl_error = error;
				Cfl_message = NULL;
				ksignal(&Cfl_message, 1);
				}
			state = 0;
			break;

		case 9:	/* error notification */
			state = 8;
			error = (unsigned int)ch + 1;
			check -= ch;
			break;
			}

		if (valid)
			{
#if 1
			switch (ch)
				{
			case '0':
			case '1':
				if (ch != there)
					{
					there = ch;
					_dprintf(0, (ch == '0') ?
					_("\nplease reinsert card: ") :
					_("thank you\n"));
					}
				break;
				}
#endif
#if 0
			abyte(ch);
#endif
#if 0
			bp = qdata(&ch, 1);
			if (bp)
				{
				append(&((struct api_minor_s *)p)->queue_p, &bp);
				ksignal(p, 0); /* wake up all processes */
				}
#if 0 /* temporarily out, as there's never much data at a time */
			kwait(NULL);
#endif
#endif
			}
		}

	if (((struct api_minor_s *)p)->thread_p == my_thread_p)
		{
		((struct api_minor_s *)p)->thread_p = NULL;
		}
	}
#endif

#ifdef MODULE_api_minor_garbage
/* api_minor garbage collection - called by storage allocator when free
 * space runs low.  Buffers for non-open api minor numbers are freed.
 */
void
api_minor_garbage(int red)
	{
	struct api_minor_s **am_pp, *am_p;

	for (am_pp = api_minor_list; am_pp < api_minor_list + API_MINOR_LIST;
			am_pp++)
		{
		am_p = *am_pp;
		if (am_p && am_p->refs == 0)
			{
			*am_pp = NULL;
			free(am_p);
			}
		}
	}
#endif

