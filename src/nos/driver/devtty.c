/* devtty.c by Nick for NOS/UZI project */

#include <errno.h>
#include <string.h>
#include <sgtty.h>
#include "nos/global.h" /* for copyr() */
#include "driver/device.h" /* for struct device_char_s */
#include "driver/devnull.h" /* this will include devio.h */
#include "driver/devtty.h"
#include "kernel/dprintf.h"
#ifdef JOB_CONTROL
#include "kernel/session.h" /* temporary */
#endif
#include "kernel/thread.h" /* for my_thread_p */
#include "kernel/usrmem.h"
#include "socket/opensock.h" /* for os_deref(), struct usock */
#include "z80/asci.h"
#include "main/main.h" /* for driver_process_p */
#include <libintl.h>
#include "po/messages.h"

#ifndef DEBUG
#define DEBUG 0
#endif

#define	OFF	0
#define	ON	1

#ifdef MODULE
#define STATIC
extern unsigned char tty_minor_bufsize[TTY_MINOR_LIST];
#else
#define STATIC static
#define MODULE_dev_tty
#define MODULE_tty_minor_list
#define MODULE_tty_minor_bufsize
#define MODULE_dev_tty_open
#define MODULE_dev_tty_close
#define MODULE_dev_tty_read
#define MODULE_dev_tty_write
#define MODULE_dev_tty_ioctl
#define MODULE_tty_minor_thread
#define MODULE_tty_minor_garbage
#define MODULE_ttydriv
#define MODULE_tty_read
#define MODULE_tty_write
#define MODULE_tty_fputs
#define MODULE__tty_fputs
#define MODULE_get_tty
#define MODULE_put_tty
#ifdef JOB_CONTROL
#define MODULE_tty_focus
#endif
#endif

#ifdef MODULE_dev_tty
struct device_char_s dev_tty =
	{
		{
		TTY_MINOR_LIST,		/* minors */
		dev_null_init,		/* init */
		dev_tty_open,		/* open */
		dev_tty_close,		/* close */
		dev_tty_ioctl		/* ioctl */
		},
	dev_tty_read,		/* read */
	dev_tty_write		/* write */
	};
#endif

#ifdef MODULE_tty_minor_list
struct tty_minor_s *tty_minor_list[TTY_MINOR_LIST];
#endif

#ifdef MODULE_tty_minor_bufsize
STATIC unsigned char tty_minor_bufsize[TTY_MINOR_LIST] =
	{
	0x80,			/* serial 0 */
	0x80			/* serial 1 */
	};
#endif

#ifdef MODULE_dev_tty_open
void
dev_tty_open(jmp_buf error, unsigned char minor)
	{
	struct tty_minor_s **tm_pp, *tm_p;
	struct asci_s *asci_p;
	char thread_name[0x10];

#if DEBUG >= 3
	_dprintf(3, _("dev_tty_open(0x%x, %u) starting\n"), error,
			(unsigned int)minor);
#endif
	tm_pp = tty_minor_list + minor;
	tm_p = *tm_pp;
	if (tm_p == NULL)
		{
		tm_p = malloc(sizeof(struct tty_minor_s));
		if (tm_p == NULL)
			{
/* abyte('b'); */
			goto error1;
			}

		memset(tm_p, 0, sizeof(struct tty_minor_s));
		tm_p->minor = minor;

		*tm_pp = tm_p;
		}

	if (tm_p->refs == (unsigned char)0)
		{
		tm_p->param.sg_flags = COOKED|CRMOD|ECHO;
		tm_p->state.echo = 1;
		tm_p->state.edit = 1;
		tm_p->state.crnl = 1;
		tm_p->state.exnl = 1;
		tm_p->state.ctrl = 0;
		tm_p->state.tnet = 0;

		tm_p->type = 0;
		tm_p->type_p = 0;

#ifndef JOB_CONTROL /* bit of a hack */
		tm_p->ctrl_c = 0;
#else
		tm_p->session_p = NULL;
		tm_p->root_session_p = NULL;
		tm_p->last_session_p = NULL;
#endif

		tm_p->thread_p = NULL;
		tm_p->queue_p = NULL;

		tm_p->lp = tm_p->line;

		if (minor < ASCI_MAX)
			{
			asci_p = Asci[minor];
			if (asci_p == NULL || asci_p->async.iface == NULL)
				{
				asci_p = asci_init(minor,
						&Loopback, /* ifp n/a kludge */
						minor, /* i/o base is 0/1 */
						15 + minor, /* irq is 15/16 */
						tty_minor_bufsize[minor],
						-1, 115200 /*9600*/, 0, 0, 0);
				if (asci_p == NULL)
					{
#if DEBUG >= 3
					_dprintf(3, _("dev_tty_open() error ENODEV\n"));
#endif
					longjmp(error, ENODEV);
					}
				}
			tm_p->type_p = asci_p;
			tm_p->type = _FL_ASCI;

			sprintf(thread_name, "tty%u rx", (unsigned int)minor);
			tm_p->thread_p =
					process_thread_create(driver_process_p,
					thread_name, 256, /*128,*/
					tty_minor_thread, 0, NULL, tm_p, 0);
			if (tm_p->thread_p == NULL)
				{
/* abyte('c'); */
			error1:
#if DEBUG >= 3
				_dprintf(3, _("dev_tty_open() error ENOMEM\n"));
#endif
				longjmp(error, ENOMEM);
				}
			}
		}

	/* assert(tm_p->refs != (unsigned char)0xff); */
	tm_p->refs++;
#if DEBUG >= 3
	_dprintf(3, _("dev_tty_open() returning\n"));
#endif
	}
#endif

#ifdef MODULE_dev_tty_close
void
dev_tty_close(jmp_buf error, unsigned char minor)
	{
	struct tty_minor_s *tm_p;

#if DEBUG >= 3
	_dprintf(3, _("dev_tty_close(0x%x, %u) starting\n"), error,
			(unsigned int)minor);
#endif
	tm_p = tty_minor_list[minor];
	if (tm_p && tm_p->refs != (unsigned char)0)
		{
		tm_p->refs--;
		if (tm_p->refs == (unsigned char)0)
			{
			if (tm_p->thread_p)
				{
				killproc(tm_p->thread_p);
				}
			free_p(&tm_p->queue_p);

			switch (tm_p->type)
				{
			case _FL_ASCI:
				asci_stop((struct asci_s *)tm_p->type_p);
				break;

			case _FL_SOCK:
				os_deref((struct usock *)tm_p->type_p);
				break;

			default:
				_panic(_("dev_tty_close() unknown object"));
				}
			}
		}
#if DEBUG >= 3
	_dprintf(3, _("dev_tty_close() returning\n"));
#endif
	}
#endif

#ifdef MODULE_dev_tty_read
size_t
dev_tty_read(jmp_buf error, unsigned char minor, void *data, size_t count)
	{
	struct tty_minor_s *tm_p;
#if 0
	struct mbuf_s *bp;
	void *temp;
#endif

#if DEBUG >= 3
	_dprintf(3, _("dev_tty_read(0x%x, %u, 0x%x, 0x%x) starting\n"),
			error, (unsigned int)minor, data, count);
#endif
	if (count == 0)
		{
#if DEBUG >= 3
		_dprintf(3, _("dev_tty_read() returning 0\n"));
#endif
		return 0;
		}

	tm_p = tty_minor_list[minor];
#ifndef JOB_CONTROL /* bit of a hack */
	if (tm_p->ctrl_c)
		{
		tm_p->ctrl_c = 0;
		alert(my_thread_p, EALARM);
		}

	while (tm_p->queue_p == 0)
#else
	while (tm_p->queue_p == 0 ||
			(my_thread_p->process_p->session_p &&
			my_thread_p->process_p->session_p !=
			tm_p->session_p))
#endif
		{
		if (tm_p->thread_p == NULL)
			{
#if DEBUG >= 3
			_dprintf(3, _("dev_tty_read() error EIO\n"));
#endif
			longjmp(error, EIO);
			}

		if (tm_p->state.nobl)
			{
#if DEBUG >= 3
			_dprintf(3, _("dev_tty_read() error EWOULDBLOCK\n"));
#endif
			longjmp(error, EWOULDBLOCK);
			}

#ifdef JOB_CONTROL
		if (kwait(NULL)) /* keep polling (rather inefficient) */
#else
		if (kwait(tm_p))
#endif
			{
#if DEBUG >= 3
			_dprintf(3, _("dev_tty_read() error EINTR\n"));
#endif
			longjmp(error, EINTR);
			}

#ifndef JOB_CONTROL /* bit of a hack */
		if (tm_p->ctrl_c)
			{
			tm_p->ctrl_c = 0;
			alert(my_thread_p, EALARM);
			}
#endif
		}

#if 1
	count = pullup(&tm_p->queue_p, data, count);
#else
	if (my_thread_p->sysio)
		{
		bp = NULL;
		temp = data;
		}
	else
		{
		bp = alloc_mbuf(count);
		if (bp == NULL)
			{
			longjmp(error, ENOMEM);
			}
		/* bp->cnt = count; */
		temp = bp->data;
		}

	count = pullup(&tm_p->queue_p, temp, count);
	if (bp) /* my_thread_p->sysio == 0 */
		{
		usrput(data, temp, count);
		free_mbuf(&bp);
		}
#endif

#if DEBUG >= 3
	_dprintf(3, _("dev_tty_read() returning 0x%x\n"), count);
#endif
	return count;
	}
#endif

#ifdef MODULE_dev_tty_write
size_t
dev_tty_write(jmp_buf error, unsigned char minor, void *data, size_t count)
	{
	struct tty_minor_s *tm_p;
	struct mbuf_s *bp;
	void *temp;
	char *start, *found;
	size_t part;
	unsigned char sysio_save;

#if DEBUG >= 3
	_dprintf(3, _("dev_tty_write(0x%x, %u, 0x%x, 0x%x) starting\n"),
			error, (unsigned int)minor, data, count);
#endif
	if (count == 0)
		{
#if DEBUG >= 3
		_dprintf(3, _("dev_tty_write() returning 0\n"));
#endif
		return 0;
		}

	tm_p = tty_minor_list[minor];
#ifdef JOB_CONTROL
 /* got focus? */
 while (my_thread_p->process_p->session_p &&
		my_thread_p->process_p->session_p != tm_p->session_p)
  {
  if (tm_p->thread_p == NULL)
   {
   goto error1;
   }

  if (kwait(NULL)) /* no, keep polling (rather inefficient) */
   {
#if DEBUG >= 3
   _dprintf(3, _("dev_tty_write() error EINTR\n"));
#endif
   longjmp(error, EINTR);
   }
  }
#endif
	if (tm_p->thread_p == NULL)
		{
		goto error1;
		}

	bp = NULL;
	temp = data;
	if (my_thread_p->sysio == 0)
		{
#if 1
		bp = qdata(data, count);
		if (bp == NULL)
			{
			longjmp(error, ENOMEM);
			}
#else
		bp = alloc_mbuf(count);
		if (bp == NULL)
			{
			longjmp(error, ENOMEM);
			}
		usrget(bp->data, temp, count);
		/* bp->cnt = count; */
#endif
		temp = bp->data;
		}
	start = (char *)temp;

	sysio_save = my_thread_p->sysio;
	my_thread_p->sysio = 1;

	if (tm_p->state.exnl && (found = memchr(start, '\n', count)) != NULL)
		{
		do
			{
			while ((part = found - start) != 0)
				{
/* abyte('a'); */
				part = tty_write(tm_p, start, part);
				if (part == -1)
					{
					goto error2;
					}
				start += part;
				count -= part;
				}
			put_tty('\r', tm_p);

			found = memchr(start + 1, '\n', count - 1);
			} while (found);
		}

/* abyte('b'); */
	part = tty_write(tm_p, start, count);
	if (part == -1)
		{
	error2:
		my_thread_p->sysio = sysio_save;
		free_mbuf(&bp);
	error1:
#if DEBUG >= 3
		_dprintf(3, _("dev_tty_write() error EIO\n"));
#endif
		longjmp(error, EIO);
		}
	else
		{
		/* add count already printed by newline loop */
		part += start - (char *)temp;
		}

	my_thread_p->sysio = sysio_save;
	free_mbuf(&bp);

#if DEBUG >= 3
	_dprintf(3, _("dev_tty_write() returning 0x%x\n"), part);
#endif
	return part;
	}
#endif

#ifdef MODULE_dev_tty_ioctl
void
dev_tty_ioctl(jmp_buf error, unsigned char minor,
		unsigned int request, void *data)
	{
	struct tty_minor_s *tm_p;
	int fd;
	struct object_s *object_p;
	struct usock *up;
	struct socklink *sp;
	char thread_name[0x10];
#ifdef JOB_CONTROL
	struct session_s *session_p;
#endif
	int i;

#if DEBUG >= 3
	_dprintf(3, _("dev_tty_ioctl(0x%x, %u, %u, 0x%x) starting\n"),
			error, (unsigned int)minor, request, data);
#endif
	tm_p = tty_minor_list[minor];
	switch (request)
		{
	case TIOCGETP:
		tm_p->param.sg_flags &= ~(RAW|ECHO|CRMOD|UNBUFF);
		if (tm_p->state.tnet)
			{
			tm_p->param.sg_flags |= TNET;
			}
		if (tm_p->state.echo)
			{
			tm_p->param.sg_flags |= ECHO;
			}
		if (tm_p->state.edit == 0)
			{
			tm_p->param.sg_flags |= RAW;
			}
		if (tm_p->state.crnl)
			{
			tm_p->param.sg_flags |= CRMOD;
			}
		if (tm_p->state.nobl)
			{
			tm_p->param.sg_flags |= UNBUFF;
			}
		uput(&tm_p->param, data, sizeof(struct sgttyb));
		break;

	case TIOCSETP:
		uget(data, &tm_p->param, sizeof(struct sgttyb));
		tm_p->state.tnet = ((tm_p->param.sg_flags & TNET) != 0);
		tm_p->state.echo = ((tm_p->param.sg_flags & ECHO) != 0);
		tm_p->state.edit = ((tm_p->param.sg_flags & RAW) == 0);
		tm_p->state.crnl = tm_p->state.exnl =
				((tm_p->param.sg_flags & CRMOD) != 0);
		tm_p->state.nobl = ((tm_p->param.sg_flags & UNBUFF) != 0);
/* XTABS ?? */
		break;

	case TIOCGETC:
		uput(&tm_p->chars, data, sizeof(struct tchars));
		break;

	case TIOCSETC:
		uget(data, &tm_p->chars, sizeof(struct tchars));
		break;

#if 0 /* look into this */
	case TIOCSETN:
		uput(&ttyinq[minor].q_count, data, 2);
		break;
#endif

	case TIOCFLUSH:
		free_p(&tm_p->queue_p);
		tm_p->lp = tm_p->line;
		break;

	case TIOCTLSET:
		tm_p->state.ctrl = 1;
		break;

	case TIOCTLRES:
		tm_p->state.ctrl = 0;
		break;

	case TIOC_CONNECT:
		if (minor < ASCI_MAX)
			{
			goto error1;
			}

		if (tm_p->type == _FL_SOCK)
			{
#if DEBUG >= 3
			_dprintf(3, _("dev_tty_ioctl() error EISCONN\n"));
#endif
			longjmp(error, EISCONN);
			}

/* abyte('a'); */
		up = os_locate(error, usrget_unsigned_int(data));
/* abyte('b'); */
		object_release(&up->object);
/* abyte('c'); */
		object_inc_refs(&up->object);
/* abyte('d'); */
 /* NOTE: inc_refs should be changed so it doesn't acquire/release itself */

		tm_p->type_p = up;
		tm_p->type = _FL_SOCK;

/* abyte('e'); */
		sprintf(thread_name, "vty%u rx",
				(unsigned int)minor - ASCI_MAX);
/* abyte('f'); */
		tm_p->thread_p = process_thread_create(driver_process_p,
				thread_name, 512, /*256,*/
				tty_minor_thread, 0, NULL, tm_p, 0);
/* abyte('g'); */
		if (tm_p->thread_p == NULL)
			{
/* abyte('h'); */
			tm_p->type = 0;
			tm_p->type_p = NULL;

 /* NOTE: deref should be changed so it doesn't acquire/release itself */
			os_deref(up);

		error2:
#if DEBUG >= 3
			_dprintf(3, _("dev_tty_ioctl() error ENOMEM\n"));
#endif
			longjmp(error, ENOMEM);
			}
/* abyte('i'); */
		break;

	case TIOC_DISCONNECT:
		if (minor < ASCI_MAX)
			{
			goto error1;
			}

		if (tm_p->type != _FL_SOCK)
			{
#if DEBUG >= 3
			_dprintf(3, _("dev_tty_ioctl() error ENOTCONN\n"));
#endif
			longjmp(error, ENOTCONN);
			}

		if (tm_p->thread_p)
			{
			killproc(tm_p->thread_p);
			tm_p->thread_p = NULL;
			}
		free_p(&tm_p->queue_p);

		up = (struct usock *)tm_p->type_p;
		tm_p->type = 0;
		tm_p->type_p = NULL;

 /* NOTE: deref should be changed so it doesn't acquire/release itself */
		os_deref(up);
		break;

#ifdef JOB_CONTROL
	case TIOC_NEW_SESSION:
		session_p = session_new();
		if (session_p == NULL)
			{
			goto error2;
			}
		session_p->minor = tm_p->minor;

		session_p->type = ((struct tioc_new_session *)data)->type;
		if(((struct tioc_new_session *)data)->name != NULL)
			{
			session_p->name = strdupw(
					((struct tioc_new_session *)data)->
					name);
			}
		session_p->proc = my_thread_p;

		memcpy(&session_p->ttystate, &tm_p->state, sizeof(struct ttystate));

		session_p->parent = tm_p->session_p;
		if (((struct tioc_new_session *)data)->makecur)
			{
			if (tm_p->root_session_p == NULL)
				{
				tm_p->root_session_p = session_p;
				}
			if (tm_p->last_session_p == NULL)
				{
				tm_p->last_session_p = session_p;
				}
			tty_focus(tm_p, session_p);
			}

		if (my_thread_p->process_p->session_p)
			{
			session_deref(my_thread_p->process_p->session_p);
			}
		my_thread_p->process_p->session_p = session_p;

		((struct tioc_new_session *)data)->jid = (unsigned int)session_p;
		break;

	case TIOC_FREE_SESSION:
		session_p = my_thread_p->process_p->session_p;
		my_thread_p->process_p->session_p = session_p->parent;

		if (session_p->parent)
			{
			session_ref(session_p->parent);
			}
		if (session_p)
			{
			session_deref(session_p);
			}
		break;

	case TIOC_WAIT_SESSION:
#if 1 /* temporary */
 /* got focus? */
 while (my_thread_p->process_p->session_p &&
		my_thread_p->process_p->session_p != tm_p->session_p)
  {
  if (tm_p->thread_p == NULL)
   {
#if DEBUG >= 3
   _dprintf(3, _("dev_tty_ioctl() error EIO\n"));
#endif
   longjmp(error, EIO);
   }

  if (kwait(NULL)) /* no, keep polling (rather inefficient) */
   {
#if DEBUG >= 3
   _dprintf(3, _("dev_tty_ioctl() error EINTR\n"));
#endif
   longjmp(error, EINTR);
   }
  }
#endif
		break;
#endif

	default:
	error1:
#if DEBUG >= 3
		_dprintf(3, _("dev_tty_ioctl() error EINVAL\n"));
#endif
		longjmp(error, EINVAL);
		}
#if DEBUG >= 3
	_dprintf(3, _("dev_tty_ioctl() returning\n"));
#endif
	}
#endif

#ifdef MODULE_tty_minor_thread
void
tty_minor_thread(int argc, void *argv, void *p)
	{
	int ch, opt;
	unsigned char buf[3];
	struct mbuf_s *bp;

	while ((ch = get_tty((struct tty_minor_s *)p)) != -1)
		{
		if (ch == IAC && ((struct tty_minor_s *)p)->state.tnet)
			{
			ch = get_tty((struct tty_minor_s *)p);
			if (ch == -1)
				{
				break;
				}

			/* Negotiation options */
			if (ch > 250 && ch < 255)
				{
				opt = get_tty((struct tty_minor_s *)p);
				if (opt == -1)
					{
					break;
					}

				/* Note that all are denied */
				buf[0] = IAC;
				buf[2] = opt;
				switch (ch)
					{
				case WILL:
				case WONT:
					buf[1] = DONT;
					tty_write((struct tty_minor_s *)p, buf, 3);
					break;
				case DO:
				case DONT:
					buf[1] = WONT;
					tty_write((struct tty_minor_s *)p, buf, 3);
					break;
					}

				continue;
				}

			/* Telnet special characters */
			else if (ch > 240)
				{
				switch (ch)
					{
				case IAC:
					/* IAC escape */
					break;
				case T_IP:
					_tty_fputs(_("**interrupt**\n"),
							(struct tty_minor_s *)p);
					continue;
				case T_EOF:
				case T_BRK:
				case T_ABORT:
					/* Terminate session */
					goto mybreak;
				case T_SUSP:
				default:
					_tty_fputs(_("**not supported**\n"),
							(struct tty_minor_s *)p);
					continue;
					}
				}

			/* character is < 240, or is an undoubled IAC escape */
			}

		if ((ch = ttydriv((struct tty_minor_s *)p, ch)) != 0)
			{
#ifndef JOB_CONTROL /* bit of a hack */
			if (ch == -1)
				{
				((struct tty_minor_s *)p)->ctrl_c = 1;
				ksignal(p, 0); /* wake up all processes (important) */
				}
			else
				{
#endif
				/* A single line of input is ready to hand to client */
				bp = qdata(((struct tty_minor_s *)p)->line, ch);
				if (bp)
					{
					append(&((struct tty_minor_s *)p)->queue_p, &bp);
					ksignal(p, 0); /* wake up all processes (important) */
					}
#ifndef JOB_CONTROL /* ksignal doesn't do anything if readers kwait(NULL); */
				}
#endif

#if 0 /* temporarily out, as there's never much data at a time */
			/* Especially on slow machines, serial I/O can be quite
			 * compute intensive, so release the machine before we
		 	 * do the next line of input (keeps memory use down) */
			kwait(NULL);
#endif
/* abyte('g'); */
			}
/* abyte('h'); */
		}
/* abyte('<'); */

mybreak:
	if (((struct tty_minor_s *)p)->thread_p == my_thread_p)
		{
		((struct tty_minor_s *)p)->thread_p = NULL;
		}
/* abyte('@'); */
	}
#endif

#ifdef MODULE_tty_minor_garbage
/* tty_minor garbage collection - called by storage allocator when free
 * space runs low.  Buffers for non-open tty minor numbers are freed.
 */
void
tty_minor_garbage(int red)
	{
	struct tty_minor_s **tm_pp, *tm_p;

	for (tm_pp = tty_minor_list; tm_pp < tty_minor_list + TTY_MINOR_LIST;
			tm_pp++)
		{
		tm_p = *tm_pp;
		if (tm_p && tm_p->refs == 0)
			{
			*tm_pp = NULL;
			free(tm_p);
			}
		}
	}
#endif

/* Accept characters from the incoming tty buffer and process them
 * (if in cooked mode) or just pass them directly (if in raw mode).
 *
 * Echoing (if enabled) is direct to the raw terminal. This requires
 * recording (if enabled) of locally typed info to be done by the session
 * itself so that edited output instead of raw input is recorded.
 *
 * Returns the number of cooked characters ready to be read from the buffer.
 */
#ifdef MODULE_ttydriv
int
ttydriv(tm_p, c)
struct tty_minor_s *tm_p;
unsigned char c;
	{
	int j;

#if DEBUG >= 4
 _dprintf(4, _("ttydriv(0x%x, 0x%x) starting\n"), sp, (int)c);
#endif

#if 1 /* Nick, now done here as we are only called from devtty.c */
	if (tm_p->state.ctrl == 0)
		{
		switch (c)
			{
#ifdef JOB_CONTROL
		case 1: /* c-a */
			if (tm_p->state.echo)
				{
				_tty_fputs(tm_p->state.exnl ?
						_("^A\r\n") : _("^A\n"), tm_p);
				}
			/* tm_p->lp = tm_p->line; */
#if 1 /* temporary */
			if (tm_p->session_p == tm_p->root_session_p &&
					tm_p->last_session_p != NULL)
				{
				tty_focus(tm_p, tm_p->last_session_p);
				}
#else
			sgrpsig(tm_p->minor, SIGTSTP);
#endif
			return 0;

		case 2: /* c-b */
			if (tm_p->state.echo)
				{
				_tty_fputs(tm_p->state.exnl ?
						_("^B\r\n") : _("^B\n"), tm_p);
				}
			/* tm_p->lp = tm_p->line; */
#if 1 /* temporary */
			for(j = tm_p->session_p->index - 1; j != tm_p->session_p->index; j--)
				{
				if (j == -1)
					{
					j = SESSION_LIST - 1;
					}
				if (session_list[j] != NULL &&
						session_list[j]->refs &&
						session_list[j]->minor ==
						tm_p->minor)
					{
					tty_focus(tm_p, session_list[j]);
					break;
					}
				}
#else
			sgrpsig(tm_p->minor, SIGTSTP);
#endif
			return 0;
#endif

		case 3: /* c-c */
			if (tm_p->state.echo)
				{
				_tty_fputs(tm_p->state.exnl ?
						_("^C\r\n") : _("^C\n"), tm_p);
				}
			tm_p->lp = tm_p->line;
#ifndef JOB_CONTROL /* bit of a hack */
			return -1;
#else
#if 1 /* temporary */
			alert(tm_p->session_p->proc, EABORT);
#else
			sgrpsig(tm_p->minor, SIGQUIT);
#endif
			return 0;
#endif

#ifdef JOB_CONTROL
		case 0xe: /* c-n */
			if (tm_p->state.echo)
				{
				_tty_fputs(tm_p->state.exnl ?
						_("^N\r\n") : _("^N\n"), tm_p);
				}
			/* tm_p->lp = tm_p->line; */
#if 1 /* temporary */
			for (j = tm_p->session_p->index + 1; j != tm_p->session_p->index; j++)
				{
				if (j >= SESSION_LIST)
					{
					j = 0;
					}
				if (session_list[j] != NULL &&
						session_list[j]->refs &&
						session_list[j]->minor ==
						tm_p->minor)
					{
					tty_focus(tm_p, session_list[j]);
					break;
					}
				}
#else
			sgrpsig(tm_p->minor, SIGTSTP);
#endif
			return 0;

		case 0x1a: /* c-z */
			if (tm_p->state.echo)
				{
				_tty_fputs(tm_p->state.exnl ?
						_("^Z\r\n") : _("^Z\n"), tm_p);
				}
			/* tm_p->lp = tm_p->line; */
#if 1 /* temporary */
			if(tm_p->session_p != tm_p->root_session_p)
				{
				tm_p->last_session_p = tty_focus(tm_p,
						tm_p->root_session_p);
				}
#else
			sgrpsig(tm_p->minor, SIGTSTP);
#endif
			return 0;
#endif
			}
		}
#endif

#if 1
	if (tm_p->state.edit == 0)
		{
#else
	switch(tm_p->state.edit)
		{
	case OFF:
#endif
		/* Editing is off; add character to buffer
		 * and return the number of characters in it (probably 1)
		 */
		*tm_p->lp++ = c;
		if (tm_p->state.echo)
			{
			put_tty(c, tm_p);
			}
		j = tm_p->lp - tm_p->line;
		tm_p->lp = tm_p->line;
#if DEBUG >= 4
 _dprintf(4, _("ttydriv() returning 0x%x a\n"), j);
#endif
		return j;
#if 1
		}
	else
		{
#else
	case ON:
#endif
		/* Perform cooked-mode line editing */
		switch(c)
			{
#if 0 /* temporary as Windows Telnet seems to send CR/LF for CR keypress */
		case '\r':	/* CR and LF both terminate the line */
		case '\n':
#else
		case '\n':	/* CR and LF both terminate the line */
 break;
		case '\r':
#endif
			if (tm_p->state.crnl)
				{
				*tm_p->lp++ = '\n';
				}
			else
				{
				*tm_p->lp++ = c;
				}
			if (tm_p->state.echo)
				{
				if (tm_p->state.exnl)
					{
					put_tty('\r', tm_p);
					}
				put_tty('\n', tm_p);
 /*fflush(tm_p->minor);*/ /* shouldn't be necessary (use _IOLBF!!) */
				}
			j = tm_p->lp - tm_p->line;
			tm_p->lp = tm_p->line;
#if DEBUG >= 4
 _dprintf(4, _("ttydriv() returning 0x%x b\n"), j);
#endif
			return j;
		case DEL:
		case '\b':	/* Character delete */
			if (tm_p->lp != tm_p->line)
				{
				tm_p->lp--;
				if (tm_p->state.echo)
					{
					_tty_fputs(_("\b \b"), tm_p);
					}
				}
			break;
		case CTLR:	/* print line buffer */
			if (tm_p->state.echo)
				{
				_tty_fputs(tm_p->state.exnl ?
						_("^R\r\n") : _("^R\n"), tm_p);
				tty_write(tm_p, tm_p->line,
						tm_p->lp - tm_p->line);
				}
			break;
		case CTLU:	/* Line kill */
			while(tm_p->state.echo && tm_p->lp-- != tm_p->line)
				{
				_tty_fputs(_("\b \b"), tm_p);
				}
			tm_p->lp = tm_p->line;
			break;
		default:	/* Ordinary character */
			*tm_p->lp++ = c;

			/* ^Z apparently hangs the terminal emulators under
			 * DoubleDos and Desqview. I REALLY HATE having to patch
			 * around other people's bugs like this!!!
			 */
			if (tm_p->state.echo &&
#if !defined(ZILOG) && !defined(AMIGA)
			 c != CTLZ &&
#endif
			 tm_p->lp - tm_p->line < TTY_LINE_SIZE-1)
				{
				put_tty(c, tm_p);
				}
			else if (tm_p->lp - tm_p->line >= TTY_LINE_SIZE-1)
				{
				put_tty('\007', tm_p); /* Beep */
				tm_p->lp--;
				}
			break;
			}
#if 0
		break;
#endif
		}
#if DEBUG >= 4
 _dprintf(4, _("ttydriv() returning 0\n"));
#endif
	return 0;
	}
#endif

#ifdef MODULE_tty_read
int tty_read(struct tty_minor_s *tm_p, void *data, size_t count)
	{
	struct usock *up;
	struct socklink *sp;

	switch (tm_p->type)
		{
	case _FL_ASCI:
		return async_read((struct async_s *)tm_p->type_p, data, count);

	case _FL_SOCK:
		up = (struct usock *)tm_p->type_p;

		/* Find vector table for the protocol in use */
		sp = up->sp;

		/* Fail if no recv routine */
		if(sp->recv == NULL)
			{
			/*longjmp(error, EOPNOTSUPP);*/
			goto Err2;
			}

		return (*sp->recv)(up, data, count, NULL, (int *)NULL);


	default:
		_panic(_("tty_read() unknown object"));
		}

	/*longjmp(error, ENOSYS);*/ /* EOPNOTSUPP? */
Err2:
	return -1;
	}
#endif

#ifdef MODULE_tty_write
int tty_write(struct tty_minor_s *tm_p, void *data, size_t count)
	{
	struct usock *up;
	struct socklink *sp;
	struct sockaddr peername;
	int peernamelen;

/* abyte('c'); */
	switch (tm_p->type)
		{
	case _FL_ASCI:
/* abyte('d'); */
		return async_write((struct async_s *)tm_p->type_p, data, count);

	case _FL_SOCK:
/* abyte('e'); */
		up = (struct usock *)tm_p->type_p;

		/* Find vector table for the protocol in use */
		sp = up->sp;

		/* Expand out getpeername subroutine inline */
		if(up->peername == NULL)
			{
			/*longjmp(error, ENOTCONN);*/
			goto Err2;
			}
		peernamelen = min_uint(MAXSOCKSIZE, up->peernamelen);
		memcpy(&peername, up->peername, peernamelen);

		/* Fail if no send routine (shouldn't happen) */
		if(sp->send == NULL)
			{
			/*longjmp(error, EOPNOTSUPP);*/
			goto Err2;
			}

		/* Always got remote address, check (maybe redundant) */
		if(sp->check != NULL &&
				(*sp->check)(&peername, peernamelen) == -1)
			{
			/*longjmp(error, EAFNOSUPPORT);*/
			goto Err2;
			}

		return (*sp->send)(up, data, count, &peername);


	default:
		_panic(_("tty_write() unknown object"));
		}

	/*longjmp(error, ENOSYS);*/ /* EOPNOTSUPP? */
Err2:
	return -1;
	}
#endif

#ifdef MODULE_tty_fputs
int tty_fputs(char *string, struct tty_minor_s *tm_p)
	{
	return tty_write(tm_p, string, strlen(string));
	}
#endif

#ifdef MODULE__tty_fputs
int _tty_fputs(_char *string, struct tty_minor_s *tm_p)
	{
	char *duptext;
	int result;

	duptext = _strdup(string);
	if (duptext == NULL)
		{
		return 0; /*__fgetfail(stream, string);*/
		}

	result = tty_fputs(duptext, tm_p);

	free(duptext);
	return result;
	}
#endif

/* Blocking read from tty line
 * Returns character or -1 if aborting
 */
#ifdef MODULE_get_tty
int
get_tty(tm_p)
struct tty_minor_s *tm_p;
	{
	unsigned char ch;
	/*size_t count;*/

/* ch = 0xaa; */
	if (/*(count =*/ tty_read(tm_p, &ch, 1)/*)*/ == 1)
		{
/* abyte(' '); */
/* ahexw((unsigned int)ch); */
		return ch;
		}
	else
		{
/* abyte(' '); */
/* ahexw(-1); */
		return -1; /*count;*/
		}
	}
#endif

/* Blocking write to tty line
 * Returns character or -1 if aborting
 */
#ifdef MODULE_put_tty
int
put_tty(ch, tm_p)
unsigned char ch;
struct tty_minor_s *tm_p;
	{
	/*size_t count;*/

	if (/*(count =*/ tty_write(tm_p, &ch, 1)/*)*/ == 1)
		{
		return ch;
		}
	else
		{
		return -1; /*count;*/
		}
	}
#endif

#ifdef JOB_CONTROL
#ifdef MODULE_tty_focus
struct session_s *
tty_focus(struct tty_minor_s *tm_p, struct session_s *session_p)
	{
	/* int i_state; */
	struct session_s *other_session_p;

#if DEBUG >= 3
	dprintf(3, "\n%02x:%04x tty_focus(0x%x, 0x%x) = 0x%x ",
			((unsigned char *)&tm_p)[4],
			((unsigned int *)&tm_p)[3], tm_p, session_p,
			tm_p->session_p);
#endif
	/* i_state = dirps(); */
	if (tm_p->session_p)
		{
		memcpy(&tm_p->session_p->ttystate, &tm_p->state,
				sizeof(struct ttystate));
		}
	other_session_p = tm_p->session_p;

	tm_p->session_p = session_p;
	if (tm_p->session_p)
		{
		memcpy(&tm_p->state, &tm_p->session_p->ttystate,
				sizeof(struct ttystate));
		}
	/* restore(i_state); */

	free_p(&tm_p->queue_p);
	tm_p->lp = tm_p->line;

#ifndef ZILOG
	alert(Display,1); /* make sure the display reflects the change */
#endif
	return other_session_p;
	}
#endif
#endif

