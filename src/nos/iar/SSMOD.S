; SSMOD.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?SS_MOD_L02
		rseg	RCODE
rcode_base:
		public	?SS_MOD_L02
?SS_MOD_L02	equ	rcode_base+00000000h
		extern	?S_FIND_SIGN_L02
		extern	?S_DIVMOD_L02
		defb	0F5h,0E5h,0C5h,03Eh,001h,0CDh
		defw	LWRD ?S_FIND_SIGN_L02
		defb	0F5h,0CDh
		defw	LWRD ?S_DIVMOD_L02
		defb	0F1h,0EBh,0B7h,020h,007h,0AFh,093h
		defb	05Fh,03Eh,000h,09Ah,057h,0C1h,0E1h
		defb	0F1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
