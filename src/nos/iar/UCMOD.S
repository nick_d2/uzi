; UCMOD.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?UC_MOD_L01
		rseg	RCODE
rcode_base:
		public	?UC_MOD_L01
?UC_MOD_L01	equ	rcode_base+00000000h
		extern	?C_DIVMOD_L01
		defb	0C5h,0D5h,04Fh,0CDh
		defw	LWRD ?C_DIVMOD_L01
		defb	07Ah,0D1h,0C1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
