; SCMOD.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?SC_MOD_L01
		rseg	RCODE
rcode_base:
		public	?SC_MOD_L01
?SC_MOD_L01	equ	rcode_base+00000000h
		extern	?C_DIVMOD_L01
		extern	?C_FIND_SIGN_L01
		defb	0C5h,0D5h,04Fh,03Eh,001h,0CDh
		defw	LWRD ?C_FIND_SIGN_L01
		defb	0F5h,0CDh
		defw	LWRD ?C_DIVMOD_L01
		defb	0F1h,0B7h,07Ah,020h,002h,0EDh,044h
		defb	0D1h,0C1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
