; LDEC.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?L_DEC_L03
		rseg	RCODE
rcode_base:
		public	?L_DEC_L03
?L_DEC_L03	equ	rcode_base+00000000h
		defb	0F5h,07Dh,0B4h,02Bh,020h,001h,00Bh
		defb	0F1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
