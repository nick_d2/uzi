; SLSH.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?S_LSH_L02
		rseg	RCODE
rcode_base:
		public	?S_LSH_L02
?S_LSH_L02	equ	rcode_base+00000000h
		defb	004h,005h,0C8h,0EBh,029h,010h,0FDh
		defb	0EBh,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
