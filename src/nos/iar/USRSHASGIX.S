; USRSHASGIX.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?US_RSHASG_IX_L12
		rseg	RCODE
rcode_base:
		public	?US_RSHASG_IX_L12
?US_RSHASG_IX_L12	equ	rcode_base+00000000h
		extern	?US_RSH_L02
		defb	0D5h,0DDh,0E5h,0D1h,0CDh
		defw	LWRD ?US_RSH_L02
		defb	0D5h,0DDh,0E1h,0EBh,0D1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
