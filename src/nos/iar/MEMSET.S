; MEMSET.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?MEMSET_L11
		rseg	RCODE
rcode_base:
		public	?MEMSET_L11
?MEMSET_L11	equ	rcode_base+00000000h
		defb	0F5h,0E5h,0D5h,078h,0B1h,028h,00Ch
		defb	07Dh,012h,00Bh,078h,0B1h,028h,005h
		defb	06Bh,062h,013h,0EDh,0B0h,0D1h,0E1h
		defb	0F1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
