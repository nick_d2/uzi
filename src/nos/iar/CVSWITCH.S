; CVSWITCH.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?C_V_SWITCH_L06
		rseg	RCODE
rcode_base:
		public	?C_V_SWITCH_L06
?C_V_SWITCH_L06	equ	rcode_base+00000000h
		extern	?V_SWITCH_END_L06
		defb	0E3h,0F5h,0C5h,04Eh,023h,046h,023h
		defb	07Bh,0EDh,0B1h,020h,002h,023h,023h
		defb	009h,0C3h
		defw	LWRD ?V_SWITCH_END_L06
		endmod

; -----------------------------------------------------------------------------

	end
