/* Generic ARCnet constants and templates */

#ifndef	_ARCNET_H
#define	_ARCNET_H

#ifndef	_GLOBAL_H
#include "nos/global.h"
#endif

#ifndef	_MBUF_H
#include "main/mbuf.h"
#endif

#ifndef	_IFACE_H
#include "main/iface.h"
#endif

#define	AADDR_LEN	1
/* Format of an ARCnet header */
struct arc {
	uint8 source[AADDR_LEN];
	uint8 dest[AADDR_LEN];
	uint8 type;
};
#define	ARCLEN	3

/* ARCnet broadcast address */
extern uint8 ARC_bdcst[];

/* ARCnet type fields */
#define	ARC_IP		0xf0	/* Type field for IP */
#define	ARC_ARP		0xf1	/* Type field for ARP */

/* In file arcnet.c: */
void htonarc(struct arc *arc,struct mbuf_s **data);
int ntoharc(struct arc *arc,struct mbuf_s **bpp);
char *parc(char *out,uint8 *addr);
int garc(uint8 *out,char *cp);
int anet_send(struct mbuf_s **bp,struct iface_s *iface,int32 gateway,uint8 tos);
int anet_output(struct iface_s *iface,uint8 dest[],uint8 source[],uint16 type,
	struct mbuf_s **data);
void aproc(struct iface_s *iface,struct mbuf_s **bp);

#endif	/* _ARCNET_H */
