/* AX25 control commands
 * Copyright 1991 Phil Karn, KA9Q
 */
#include <stdio.h>
#include "nos/global.h"
#include "main/mbuf.h"
#include "main/timer.h"
#include "kernel/thread.h"
#include "main/iface.h"
#include "ax25/ax25.h"
#include "ax25/lapb.h"
#include "main/cmdparse.h"
#include "socket/socket.h"
#include "server/mailbox.h"
#ifdef JOB_CONTROL
#include "kernel/session.h"
#endi
#include "driver/devtty.h" /* for struct ttystate */
#include "netrom/nr4.h"
#include "main/commands.h"
#include <libintl.h>
#include "po/messages.h"

static int axdest(struct iface_s *ifp);
static int axheard(struct iface_s *ifp);
static void axflush(struct iface_s *ifp);
static int doaxflush(int argc,char *argv[],void *p);
static int doaxirtt(int argc,char *argv[],void *p);
static int doaxkick(int argc,char *argv[],void *p);
static int doaxreset(int argc,char *argv[],void *p);
static int doaxroute(int argc,char *argv[],void *p);
static int doaxstat(int argc,char *argv[],void *p);
static int doaxwindow(int argc,char *argv[],void *p);
static int doblimit(int argc,char *argv[],void *p);
static int dodigipeat(int argc,char *argv[],void *p);
static int domaxframe(int argc,char *argv[],void *p);
static int domycall(int argc,char *argv[],void *p);
static int don2(int argc,char *argv[],void *p);
static int dopaclen(int argc,char *argv[],void *p);
static int dopthresh(int argc,char *argv[],void *p);
static int dot3(int argc,char *argv[],void *p);
static int doversion(int argc,char *argv[],void *p);

char *Ax25states[] = {
	"",
	"Disconn",
	"Listening",
	"Conn pend",
	"Disc pend",
	"Connected",
	"Recovery",
};

/* Ascii explanations for the disconnect reasons listed in lapb.h under
 * "reason" in ax25_cb
 */
char *Axreasons[] = {
	"Normal",
	"DM received",
	"Timeout"
};

static struct cmds Axcmds[] = {
	N_("blimit"),	doblimit,	0, 0, NULL,
	N_("destlist"),	doaxdest,	0, 0, NULL,
	N_("digipeat"),	dodigipeat,	0, 0, NULL,
	N_("flush"),	doaxflush,	0, 0, NULL,
	N_("heard"),	doaxheard,	0, 0, NULL,
	N_("irtt"),	doaxirtt,	0, 0, NULL,
	N_("kick"),	doaxkick,	0, 2, N_("ax25 kick <axcb>"),
	N_("maxframe"),	domaxframe,	0, 0, NULL,
	N_("mycall"),	domycall,	0, 0, NULL,
	N_("paclen"),	dopaclen,	0, 0, NULL,
	N_("pthresh"),	dopthresh,	0, 0, NULL,
	N_("reset"),	doaxreset,	0, 2, N_("ax25 reset <axcb>"),
	N_("retry"),	don2,		0, 0, NULL,
	N_("route"),	doaxroute,	0, 0, NULL,
	N_("status"),	doaxstat,	0, 0, NULL,
	N_("t3"),	dot3,		0, 0, NULL,
	N_("version"),	doversion,	0, 0, NULL,
	N_("window"),	doaxwindow,	0, 0, NULL,
	NULL,
};

#ifdef JOB_CONTROL
static int keychar(int c);
#endif

/* Multiplexer for top-level ax25 command */
int
doax25(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return subcmd(Axcmds,argc,argv,p);
}

int
doaxheard(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct iface_s *ifp;

	if(argc > 1){
		if((ifp = if_lookup(argv[1])) == NULL){
			_printf(_("Interface %s unknown\n"),argv[1]);
			return 1;
		}
		if(ifp->output != ax_output){
			_printf(_("Interface %s not AX.25\n"),argv[1]);
			return 1;
		}
		axheard(ifp);
		return 0;
	}
	for(ifp = Ifaces;ifp != NULL;ifp = ifp->next){
		if(ifp->output != ax_output)
			continue;	/* Not an ax.25 interface */
		if(axheard(ifp) == EOF)
			break;
	}
	return 0;
}
static int
axheard(ifp)
struct iface_s *ifp;
{
	struct lq *lp;
	char tmp[AXBUF];

	if(ifp->hwaddr == NULL)
		return 0;
	_printf(_("%s:\n"),ifp->name);
	_printf(_("Station   Last heard           Pkts\n"));
	for(lp = Lq;lp != NULL;lp = lp->next){
		if(lp->iface != ifp)
			continue;
		_printf(_("%-10s%-17s%8lu\n"),pax25(tmp,lp->addr),
		 tformat(secclock() - lp->time),lp->currxcnt);
	}
	return 0;
}
int
doaxdest(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct iface_s *ifp;

	if(argc > 1){
		if((ifp = if_lookup(argv[1])) == NULL){
			_printf(_("Interface %s unknown\n"),argv[1]);
			return 1;
		}
		if(ifp->output != ax_output){
			_printf(_("Interface %s not AX.25\n"),argv[1]);
			return 1;
		}
		axdest(ifp);
		return 0;
	}
	for(ifp = Ifaces;ifp != NULL;ifp = ifp->next){
		if(ifp->output != ax_output)
			continue;	/* Not an ax.25 interface */
		if(axdest(ifp) == EOF)
			break;
	}
	return 0;
}
static int
axdest(ifp)
struct iface_s *ifp;
{
	struct ld *lp;
	struct lq *lq;
	char tmp[AXBUF];

	if(ifp->hwaddr == NULL)
		return 0;
	_printf(_("%s:\n"),ifp->name);
	_printf(_("Station   Last ref         Last heard           Pkts\n"));
	for(lp = Ld;lp != NULL;lp = lp->next){
		if(lp->iface != ifp)
			continue;

		_printf(_("%-10s%-17s"),
		 pax25(tmp,lp->addr),tformat(secclock() - lp->time));

		if(addreq(lp->addr,ifp->hwaddr)){
			/* Special case; it's our address */
			_printf(_("%-17s"),tformat(secclock() - ifp->lastsent));
		} else if((lq = al_lookup(ifp,lp->addr,0)) == NULL){
			_printf(_("%-17s"),"");
		} else {
			_printf(_("%-17s"),tformat(secclock() - lq->time));
		}
		_printf(_("%8lu\n"),lp->currxcnt);
	}
	return 0;
}
static int
doaxflush(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct iface_s *ifp;

	for(ifp = Ifaces;ifp != NULL;ifp = ifp->next){
		if(ifp->output != ax_output)
			continue;	/* Not an ax.25 interface */
		axflush(ifp);
	}
	return 0;
}
static void
axflush(ifp)
struct iface_s *ifp;
{
	struct lq *lp,*lp1;
	struct ld *ld,*ld1;

	ifp->rawsndcnt = 0;
	for(lp = Lq;lp != NULL;lp = lp1){
		lp1 = lp->next;
		free(lp);
	}
	Lq = NULL;
	for(ld = Ld;ld != NULL;ld = ld1){
		ld1 = ld->next;
		free(ld);
	}
	Ld = NULL;
}

static
doaxreset(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct ax25_cb *axp;

	axp = (struct ax25_cb *)ltop(htol(argv[1]));
	if(!ax25val(axp)){
		printf(_("Not a valid control block\n"));
		return 1;
	}
	reset_ax25(axp);
	return 0;
}

/* Display AX.25 link level control blocks */
static
doaxstat(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	register struct ax25_cb *axp;
	char tmp[AXBUF];

	if(argc < 2){
		_printf(_("&AXB      Snd-Q   Rcv-Q   Remote    State\n"));
		for(axp = Ax25_cb;axp != NULL; axp = axp->next){
			_printf(_("%9p %-8d%-8d%-10s%s\n"),
			 axp,
			 len_q(axp->txq),len_p(axp->rxq),
			 pax25(tmp,axp->remote),
			 Ax25states[axp->state]);
		}
		return 0;
	}
	axp = (struct ax25_cb *)ltop(htol(argv[1]));
	if(!ax25val(axp)){
		printf(_("Not a valid control block\n"));
		return 1;
	}
	st_ax25(axp);
	return 0;
}
/* Dump one control block */
void
st_ax25(axp)
register struct ax25_cb *axp;
{
	char tmp[AXBUF];

	if(axp == NULL)
		return;
	_printf(_("     &AXB Remote   RB V(S) V(R) Unack P Retry State\n"));

	_printf(_("%9p %-9s%c%c"),axp,pax25(tmp,axp->remote),
	 axp->flags.rejsent ? 'R' : ' ',
	 axp->flags.remotebusy ? 'B' : ' ');
	_printf(_(" %4d %4d"),axp->vs,axp->vr);
	_printf(_(" %02u/%02u %u"),axp->unack,axp->maxframe,axp->proto);
	_printf(_(" %02u/%02u"),axp->retries,axp->n2);
	_printf(_(" %s\n"),Ax25states[axp->state]);

	_printf(_("srtt = %lu mdev = %lu "),axp->srt,axp->mdev);
	_printf(_("T1: "));
	if(run_timer(&axp->t1))
		_printf(_("%lu"),read_timer(&axp->t1));
	else
		_printf(_("stop"));
	_printf(_("/%lu ms; "),dur_timer(&axp->t1));

	_printf(_("T3: "));
	if(run_timer(&axp->t3))
		_printf(_("%lu"),read_timer(&axp->t3));
	else
		_printf(_("stop"));
	_printf(_("/%lu ms\n"),dur_timer(&axp->t3));

}

/* Display or change our AX.25 address */
static
domycall(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	char tmp[AXBUF];

	if(argc < 2){
		_printf(_("%s\n"),pax25(tmp,Mycall));
		return 0;
	}
	if(setcall(Mycall,argv[1]) == -1)
		return -1;
	return 0;
}

/* Control AX.25 digipeating */
static
dodigipeat(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return setbool(&Digipeat, _("Digipeat"), argc, argv);
}
/* Set limit on retransmission backoff */
static
doblimit(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return setlong(&Blimit, _("blimit"), argc, argv);
}
static
doversion(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return setshort(&Axversion, _("AX25 version"), argc, argv);
}

static
doaxirtt(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return setlong(&Axirtt, _("Initial RTT (ms)"), argc, argv);
}

/* Set idle timer */
static
dot3(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return setlong(&T3init, _("Idle poll timer (ms)"), argc, argv);
}

/* Set retry limit count */
static
don2(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return setshort(&N2, _("Retry limit"), argc, argv);
}
/* Force a retransmission */
static
doaxkick(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct ax25_cb *axp;

	axp = (struct ax25_cb *)ltop(htol(argv[1]));
	if(!ax25val(axp)){
		printf(_("Not a valid control block\n"));
		return 1;
	}
	kick_ax25(axp);
	return 0;
}
/* Set maximum number of frames that will be allowed in flight */
static
domaxframe(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return setshort(&Maxframe, _("Window size (frames)"), argc, argv);
}

/* Set maximum length of I-frame data field */
static
dopaclen(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return setshort(&Paclen, _("Max frame length (bytes)"), argc, argv);
}
/* Set size of I-frame above which polls will be sent after a timeout */
static
dopthresh(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return setshort(&Pthresh, _("Poll threshold (bytes)"), argc, argv);
}

/* Set high water mark on receive queue that triggers RNR */
static
doaxwindow(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	return setshort(&Axwindow, _("AX25 receive window (bytes)"),
	       argc, argv);
}
/* End of ax25 subcommands */

/* Initiate interactive AX.25 connect to remote station */
int
doconnect(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	struct sockaddr_ax fsocket;
#ifdef JOB_CONTROL
	struct session_s *sp;
#else
	FILE *network;
#endif
	int ndigis,i,s;
	uint8 digis[MAXDIGIS][AXALEN];
	uint8 target[AXALEN];

	/* If digipeaters are given, put them in the routing table */
	if(argc > 3){
		setcall(target,argv[2]);
		ndigis = argc - 3;
		if(ndigis > MAXDIGIS){
			_printf(_("Too many digipeaters\n"));
			return 1;
		}
		for(i=0;i<ndigis;i++){
			if(setcall(digis[i],argv[i+3]) == -1){
				_printf(_("Bad digipeater %s\n"),argv[i+3]);
				return 1;
			}
		}
		if(ax_add(target,AX_LOCAL,digis,ndigis) == NULL){
			_printf(_("Route add failed\n"));
			return 1;
		}
	}
#ifdef JOB_CONTROL
	/* Allocate a session descriptor */
	if((sp = newsession(Cmdline,AX25TNC,1)) == NULL){
		_printf(_("Too many sessions\n"));
		return 1;
	}
	sp->inproc = keychar;	/* Intercept ^C */
#endif
	if((s = socket(AF_AX25,SOCK_STREAM,0)) == -1){
		_printf(_("Can't create socket\n"));
#ifdef JOB_CONTROL
		freesession(sp, 1); /* does keywait(NULL, 1); beforehand */
#endif
		return 1;
	}
	fsocket.sax_family = AF_AX25;
	setcall(fsocket.ax25_addr,argv[2]);
	strncpy(fsocket.iface,argv[1],ILEN);
#ifdef JOB_CONTROL
	sp->network = fdopen(s,"r+t");
	setvbuf(sp->network,NULL,_IOLBF,BUFSIZ);
#else
	network = fdopen(s,"r+t");
	setvbuf(network,NULL,_IOLBF,BUFSIZ);
#endif
	if(SETSIG(EABORT)){
#ifdef JOB_CONTROL
		freesession(sp, 1); /* does keywait(NULL, 1); beforehand */
#endif
		return 1;
	}
#ifdef JOB_CONTROL
	return tel_connect(sp, (struct sockaddr *)&fsocket, sizeof(struct sockaddr_ax));
#else
	return tel_connect(network, (struct sockaddr *)&fsocket, sizeof(struct sockaddr_ax));
#endif
}

/* Display and modify AX.25 routing table */
static int
doaxroute(argc,argv,p)
int argc;
char *argv[];
void *p;
{
	char tmp[AXBUF];
	int i,ndigis;
	register struct ax_route *axr;
	uint8 target[AXALEN],digis[MAXDIGIS][AXALEN];

	if(argc < 2){
		_printf(_("Target    Type   Digipeaters\n"));
		for(axr = Ax_routes;axr != NULL;axr = axr->next){
			_printf(_("%-10s%-6s"),pax25(tmp,axr->target),
			 axr->type == AX_LOCAL ? "Local":"Auto");
			for(i=0;i<axr->ndigis;i++){
				_printf(_(" %s"),pax25(tmp,axr->digis[i]));
			}
			_printf(_("\n"));
		}
		return 0;
	}
	if(argc < 3){
		_printf(_("Usage: ax25 route add <target> [digis...]\n"));
		_printf(_("       ax25 route drop <target>\n"));
		return 1;
	}
	if(setcall(target,argv[2]) == -1){
		_printf(_("Bad target %s\n"),argv[2]);
		return 1;
	}
	switch(argv[1][0]){
	case 'a':	/* Add route */
		if(argc < 3){
			_printf(_("Usage: ax25 route add <target> [digis...]\n"));
			return 1;
		}
		ndigis = argc - 3;
		if(ndigis > MAXDIGIS){
			_printf(_("Too many digipeaters\n"));
			return 1;
		}
		for(i=0;i<ndigis;i++){
			if(setcall(digis[i],argv[i+3]) == -1){
				_printf(_("Bad digipeater %s\n"),argv[i+3]);
				return 1;
			}
		}
		if(ax_add(target,AX_LOCAL,digis,ndigis) == NULL){
			_printf(_("Failed\n"));
			return 1;
		}
		break;
	case 'd':	/* Drop route */
		if(ax_drop(target) == -1){
			_printf(_("Not in table\n"));
			return 1;
		}
		break;
	default:
		_printf(_("Unknown command %s\n"),argv[1]);
		return 1;
	}
	return 0;
}

#ifdef JOB_CONTROL
static int
keychar(c)
int c;
{
	if(c != CTLC)
		return 1;	/* Ignore all but ^C */

#if 1
	_printf(_("^C\n"));
	fflush(stdout);
	alert(my_thread_p, EABORT);
#else
	_fprintf(my_session_p->output, _("^C\n"));
	alert(my_session_p->proc,EABORT);
#endif
	return 0;
}
#endif

