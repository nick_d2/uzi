# auto.mk by Nick for Hytech NOS/UZI project

# -----------------------------------------------------------------------------

SUBDIRS=

OUTPUTS=	messages.po

messages_po_SOURCES= \
		../init/align.c ../init/bd.c ../init/boot.c ../init/cat.c \
		../init/chgrp.c ../init/chmod.c ../init/chown.c ../init/cp.c \
		../init/echo.c ../init/fsck.c ../init/init.c ../init/login.c \
		../init/ls.c ../init/mkdir.c ../init/mkfs.c ../init/mknod.c \
		../init/more.c ../init/mount.c ../init/touch.c

# -----------------------------------------------------------------------------

