#!/bin/sh
rm -frv ../lib/crt0_banked.asm ../lib/crt0_banked.lst ../lib/crt0_banked.rel
rm -frv ../lib/crt0_large.asm ../lib/crt0_large.lst ../lib/crt0_large.rel
rm -frv crt/*.i crt/build
rm -frv ../lib/crt_banked.lib ../lib/crt_banked_lib
rm -frv ../lib/crt_large.lib ../lib/crt_large_lib
rm -frv sys/*.i sys/build
rm -frv ../lib/sys_banked.lib ../lib/sys_banked_lib
rm -frv ../lib/sys_large.lib ../lib/sys_large_lib
rm -frv init/*.i init/build
rm -frv po/messages.po po/build ../lib/messages.lib ../lib/messages_lib
rm -frv simple/*.i simple/build
rm -frv sh/msh/*.i sh/msh/build
rm -frv sh/po/messages.po sh/po/build sh/lib/messages.lib sh/lib/messages_lib
