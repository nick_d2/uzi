# /uzi/src/make.d/host.mk by Nick for Hytech NOS/UZI project

# Enter with TOPSRCDIR = relative path to current directory, which will be
# blank if the project is at the same level as this host.mk file, for example
# /uzi/src/simple has an "include ../make.d/host.mk" with TOPSRCDIR = blank

# -----------------------------------------------------------------------------

TOPSRCDIR:=	$(TOPSRCDIR)../
TOPMAKDIR=	$(TOPSRCDIR)make.d/

# -----------------------------------------------------------------------------

BUILD=		build
ARCH=		z80

MODEL=		banked
MODEL_SWITCH=	-mb
MODEL_LINKER=	$(ECHO) "-bl RCODE=0x1100"; \
		$(ECHO) "-bl CODE=0xf000,0x10000"; \
		$(ECHO) "-bc CODE=0x1000";

#MODEL=		large
#MODEL_SWITCH=	-ml
#MODEL_LINKER=	$(ECHO) "-bl RCODE=0x1100";

HOST:=		$(ARCH)

include $(TOPMAKDIR)arch.mk

# -----------------------------------------------------------------------------

