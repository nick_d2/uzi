# /uzi/src/make.d/top_defs.mk by Nick for Hytech NOS/UZI project

# Enter with TOPSRCDIR = relative path to /uzi/src top level source directory

# -----------------------------------------------------------------------------

TOPDIR=		$(TOPSRCDIR)../
TOPBINDIR=	$(TOPDIR)bin/
TOPINCDIR=	$(TOPDIR)include/
TOPLIBDIR=	$(TOPDIR)lib/

# -----------------------------------------------------------------------------

AR=		ar
AS=		as
CAT=		cat
CC=		cc
CP=		cp -f
CPP=		cpp
CPROTO=		cproto
ECHO=		echo
LD=		ld
LN=		ln -sf
MKDIR=		mkdir -p
MV=		mv -f
RM=		rm -f
RMDIR=		rmdir -p
SED=		sed
TEE=		tee
TEST=		test

ARFLAGS=
ASFLAGS=
CFLAGS=
CPPFLAGS=
LDFLAGS=
DEFINES=	__STDC__ NDEBUG PC_UZIX_TARGET
INCDIRS=	$(TOPINCDIR) $(TOPINCDIR)$(ARCH)
LIBDIRS=	$(TOPLIBDIR)
STDLIBS=

ASMEXT=		as
BINEXT=		bin
EXEEXT=
IHXEXT=		ihx
INTEXT=		i
LBREXT=		ar
LIBEXT=		a
LNKEXT=		ld
OBJEXT=		o
TMPEXT=		tmp

BINOUT=		$(TOPBINDIR)
EXEOUT=		$(TOPBINDIR)$(MODEL:%=%/)
HDROUT=		$(TOPINCDIR)
LIBOUT=		$(TOPLIBDIR)
TMPOUT=		$(BUILD:%=%/)$(ARCH:%=%/)

# -----------------------------------------------------------------------------

include $(TOPMAKDIR)$(ARCH:%=%/)arch_defs.mk

# -----------------------------------------------------------------------------

