# /uzi/src/make.d/z80/arch_defs.mk by Nick for Hytech NOS/UZI project

# -----------------------------------------------------------------------------

AR=		lib-z80
AS=		as-z80
CC=		iccz80
CPP=		tradcpp
LD=		link-z80
LN=		$(CP)

ASFLAGS+=	-l -o -z
CFLAGS+=	-S -A -v1 -s7 -g $(MODEL_SWITCH)
LDFLAGS+=	-i -m -u -z
STDLIBS+=	crt_$(MODEL).$(LIBEXT) sys_$(MODEL).$(LIBEXT) iar.$(LIBEXT)

ASMEXT=		asm
IHXEXT=		i86
LBREXT=		lbr
LIBEXT=		lib
LNKEXT=		lnk
OBJEXT=		rel

TMPOUT=		$(BUILD:%=%/)$(ARCH:%=%/)$(MODEL:%=%/)

# -----------------------------------------------------------------------------

