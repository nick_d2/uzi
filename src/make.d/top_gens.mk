# /uzi/src/make.d/top_gens.mk by Nick for Hytech NOS/UZI project

# -----------------------------------------------------------------------------

APPEND=		$(if $(strip $2), $(eval $(strip $1)+=$(strip $2)))
ASSIGN=		$(if $(strip $2), $(eval $(strip $1)=$(strip $2)))
CANON=		$(strip $(subst .,_,$(subst /,_,$1)))
CHOOSE=		$(strip $(if $(strip $1), $1, $2))

define CLASS
$(strip \
	$(if \
		$(filter \
			all $(PHONIES), \
			$1 \
		), \
		.phony, \
		$(if \
			$(filter \
				$(SUBDIRS), \
				$1 \
			), \
			.d, \
			$(suffix \
				$1 \
			) \
		) \
	) \
)
endef

GENERATE=	$(call RECURSE, all,, $(OUTPUTS) $(SUBDIRS),,)

define INHERIT
$(if \
	$(call \
		$1 \
	),, \
	$(call \
		ASSIGN, \
		$1, \
		$(call \
			$2 \
		) \
	) \
)
endef

define INHERITS
$(foreach \
	VARIABLE, \
	$1, \
	$(call \
		INHERIT, \
		$(strip \
			$2 \
		)$(VARIABLE), \
		$(strip \
			$3 \
		)$(VARIABLE) \
	) \
)
endef

# -----------------------------------------------------------------------------

define DEPENDS
$(if \
	$(call \
		$2 \
	),, \
	$(call \
		ASSIGN, \
		$2, \
		$(call \
			GUESS$(call \
				CANON, \
				$(call \
					CLASS, \
					$1 \
				) \
			), \
			$1 \
		) \
	) \
) \
$(call \
	CHOOSE, \
	$(call \
		DERIVE$(call \
			CANON, \
			$(call \
				CLASS, \
				$1 \
			) \
		), \
		$(call \
			$2 \
		) \
	), \
	$(call \
		$2 \
	) \
)
endef

define MUNGE
$(strip \
	$(filter \
		all %.all $(SUBDIRS) .%, \
		$1 \
	) \
	$(filter \
		%.c %.S %.hin %.po %.txt, \
		$(filter-out \
			.%, \
			$1 \
		) \
	) \
	$(patsubst \
		%, \
		$(TMPOUT)%, \
		$(notdir \
			$(filter \
%.$(ASMEXT) %.$(INTEXT) %.$(LBREXT) %.$(LNKEXT) %.$(OBJEXT) %.$(TMPEXT) \
%.$(IHXEXT) %.l, \
				$(filter-out \
					.%, \
					$1 \
				) \
			) \
		) \
	) \
	$(patsubst \
		%, \
		$(BINOUT)%, \
		$(filter \
			%.$(BINEXT), \
			$(filter-out \
				.%, \
				$1 \
			) \
		) \
	) \
	$(patsubst \
		%, \
		$(LIBOUT)%, \
		$(filter \
			%.$(LIBEXT), \
			$(filter-out \
				.%, \
				$1 \
			) \
		) \
	) \
	$(patsubst \
		%, \
		$(HDROUT)%, \
		$(filter \
			%.h, \
			$(filter-out \
				.%, \
				$1 \
			) \
		) \
	) \
	$(patsubst \
		%, \
		$(EXEOUT)%, \
		$(strip \
			$(filter-out \
				all $(PHONIES) $(SUBDIRS) .% \
				%.c %.S %.hin %.h %.po %.txt \
%.$(ASMEXT) %.$(INTEXT) %.$(LBREXT) %.$(LNKEXT) %.$(OBJEXT) %.$(TMPEXT) \
%.$(IHXEXT) %.$(BINEXT) %.$(LIBEXT) %.l, \
				$1 \
			) \
		) \
	) \
)
endef

define RECURSE
$(call \
	INHERITS, \
ARFLAGS ASFLAGS CFLAGS CPPFLAGS LDFLAGS DEFINES INCDIRS LIBDIRS STDLIBS, \
	$(call \
		CANON, \
		$1 \
	)_, \
	$2 \
) \
$(if \
	$5, \
	$(call \
		APPEND, \
		$(call \
			CANON, \
			$1 \
		)_DEFINES, \
		$5 \
	) \
) \
$(eval \
	$(call \
		TEMPLATE$(call \
			CANON, \
			$(call \
				CLASS, \
				$1 \
			) \
		), \
		$1, \
		$(strip \
			$3 $4 \
		) \
	) \
) \
$(foreach \
	SOURCE, \
	$3, \
	$(if \
		$(call \
			$(call \
				CANON, \
				$(SOURCE) \
			)_CLIENTS \
		), \
		$(call \
			APPEND, \
			$(call \
				CANON, \
				$(SOURCE) \
			)_CLIENTS, \
			$1 \
		), \
		$(call \
			ASSIGN, \
			$(call \
				CANON, \
				$(SOURCE) \
			)_CLIENTS, \
			$1 \
		) \
		$(call \
			RECURSE, \
			$(SOURCE), \
			$(call \
				CANON, \
				$1 \
			)_, \
			$(if \
				$(call \
					$(call \
						CANON, \
						$(SOURCE) \
					)_MODSRCS \
				),, \
				$(call \
					DEPENDS, \
					$(SOURCE), \
					$(call \
						CANON, \
						$(SOURCE) \
					)_SOURCES \
				) \
			), \
			$(call \
				$(call \
					CANON, \
					$(SOURCE) \
				)_MODSRCS \
			), \
			$(call \
				$(call \
					CANON, \
					$(SOURCE) \
				)_MODDEFS \
			) \
		) \
	) \
)
endef

# -----------------------------------------------------------------------------

define DERIVE$(call CANON, $(EXEEXT))
$(foreach \
	DEPEND, \
	$1, \
	$(if \
		$(call \
			$(call \
				CANON, \
				$(DEPEND) \
			)_MODULES \
		), \
		$(foreach \
			MODULE, \
			$(call \
				$(call \
					CANON, \
					$(DEPEND) \
				)_MODULES \
			), \
			$(MODULE).$(OBJEXT) \
			$(call \
				ASSIGN, \
				$(call \
					CANON, \
					$(MODULE).$(OBJEXT) \
				)_MODSRCS, \
				$(DEPEND) \
				$(filter \
					%.h %.hin, \
					$1 \
				) \
			) \
			$(call \
				ASSIGN, \
				$(call \
					CANON, \
					$(MODULE).$(OBJEXT) \
				)_MODDEFS, \
				MODULE MODULE_$(MODULE) \
			) \
		), \
		$(patsubst \
			%.c, \
			%.$(OBJEXT), \
			$(patsubst \
				%.S, \
				%.$(OBJEXT), \
				$(DEPEND) \
			) \
		) \
	) \
)
endef

define GUESS$(call CANON, $(EXEEXT))
$(firstword \
	$(wildcard \
		$(call \
			MUNGE, \
			$(1:%$(EXEEXT)=%.c) \
		) \
		$(call \
			MUNGE, \
			$(1:%$(EXEEXT)=%.S) \
		) \
		$(call \
			MUNGE, \
			$(1:%$(EXEEXT)=%.$(OBJEXT)) \
		) \
	) \
)
endef

# -----------------------------------------------------------------------------

define DERIVE_$(BINEXT)
$(foreach \
	DEPEND, \
	$1, \
	$(if \
		$(call \
			$(call \
				CANON, \
				$(DEPEND) \
			)_MODULES \
		), \
		$(foreach \
			MODULE, \
			$(call \
				$(call \
					CANON, \
					$(DEPEND) \
				)_MODULES \
			), \
			$(MODULE).$(OBJEXT) \
			$(call \
				ASSIGN, \
				$(call \
					CANON, \
					$(MODULE).$(OBJEXT) \
				)_MODSRCS, \
				$(DEPEND) \
				$(filter \
					%.h %.hin, \
					$1 \
				) \
			) \
			$(call \
				ASSIGN, \
				$(call \
					CANON, \
					$(MODULE).$(OBJEXT) \
				)_MODDEFS, \
				MODULE MODULE_$(MODULE) \
			) \
		), \
		$(patsubst \
			%.c, \
			%.$(OBJEXT), \
			$(patsubst \
				%.S, \
				%.$(OBJEXT), \
				$(DEPEND) \
			) \
		) \
	) \
)
endef

define GUESS_$(BINEXT)
$(firstword \
	$(wildcard \
		$(call \
			MUNGE, \
			$(1:%.$(BINEXT)=%.c) \
		) \
		$(call \
			MUNGE, \
			$(1:%.$(BINEXT)=%.S) \
		) \
		$(call \
			MUNGE, \
			$(1:%.$(BINEXT)=%.$(OBJEXT)) \
		) \
	) \
)
endef

# -----------------------------------------------------------------------------

define GUESS_$(INTEXT)
$(firstword \
	$(wildcard \
		$(call \
			MUNGE, \
			$(1:%.$(INTEXT)=%.c) \
		) \
		$(call \
			MUNGE, \
			$(1:%.$(INTEXT)=%.S) \
		) \
		$(call \
			MUNGE, \
			$(1:%.$(INTEXT)=%.$(INTEXT)) \
		) \
	) \
) \
$(firstword \
	$(wildcard \
		$(call \
			MUNGE, \
			$(1:%.$(INTEXT)=%.hin) \
		) \
		$(call \
			MUNGE, \
			$(1:%.$(INTEXT)=%.h) \
		) \
	) \
)
endef

define TEMPLATE_$(INTEXT)
$(foreach \
	SOURCE, \
	$(filter \
		%.c, \
		$2 \
	), \
	$(call \
		TEMPLATE_$(INTEXT)_c, \
		$1, \
		$(SOURCE), \
		$(filter-out \
			$(SOURCE), \
			$2 \
		) \
	) \
) \
$(foreach \
	SOURCE, \
	$(filter \
		%.S, \
		$2 \
	), \
	$(call \
		TEMPLATE_$(INTEXT)_S, \
		$1, \
		$(SOURCE), \
		$(filter-out \
			$(SOURCE), \
			$2 \
		) \
	) \
)
endef

define TEMPLATE_$(INTEXT)_c

$(call MUNGE, $1): $(strip $(call MUNGE, $2) $(call MUNGE, $3))
$(if $(TMPOUT),	$(TEST) -d $(TMPOUT) || $(MKDIR) $(TMPOUT))
	$(CPP) $(strip $($(call CANON, $1)_CPPFLAGS) \
$(foreach DEFINE, $($(call CANON, $1)_DEFINES), -D$(DEFINE)) \
$(foreach INCDIR, $($(call CANON, $1)_INCDIRS), -I$(INCDIR))) \
$(call MUNGE, $2) $(call MUNGE, $1)

endef

define TEMPLATE_$(INTEXT)_S

$(call MUNGE, $1): $(strip $(call MUNGE, $2) $(call MUNGE, $3))
$(if $(TMPOUT),	$(TEST) -d $(TMPOUT) || $(MKDIR) $(TMPOUT))
	$(CPP) $(strip $($(call CANON, $1)_CPPFLAGS) \
$(foreach DEFINE, $($(call CANON, $1)_DEFINES), -D$(DEFINE)) \
$(foreach INCDIR, $($(call CANON, $1)_INCDIRS), -I$(INCDIR))) \
$(call MUNGE, $2) $(call MUNGE, $1)

endef

# -----------------------------------------------------------------------------

define DERIVE_$(LIBEXT)
$(foreach \
	DEPEND, \
	$1, \
	$(if \
		$(call \
			$(call \
				CANON, \
				$(DEPEND) \
			)_MODULES \
		), \
		$(foreach \
			MODULE, \
			$(call \
				$(call \
					CANON, \
					$(DEPEND) \
				)_MODULES \
			), \
			$(MODULE).$(OBJEXT) \
			$(call \
				ASSIGN, \
				$(call \
					CANON, \
					$(MODULE).$(OBJEXT) \
				)_MODSRCS, \
				$(DEPEND) \
				$(filter \
					%.h %.hin, \
					$1 \
				) \
			) \
			$(call \
				ASSIGN, \
				$(call \
					CANON, \
					$(MODULE).$(OBJEXT) \
				)_MODDEFS, \
				MODULE MODULE_$(MODULE) \
			) \
		), \
		$(patsubst \
			%.c, \
			%.$(OBJEXT), \
			$(patsubst \
				%.S, \
				%.$(OBJEXT), \
				$(DEPEND) \
			) \
		) \
	) \
)
endef

define GUESS_$(LIBEXT)
$(firstword \
	$(wildcard \
		$(call \
			MUNGE, \
			$(1:%.$(LIBEXT)=%.c) \
		) \
		$(call \
			MUNGE, \
			$(1:%.$(LIBEXT)=%.S) \
		) \
		$(call \
			MUNGE, \
			$(1:%.$(LIBEXT)=%.$(OBJEXT)) \
		) \
	) \
)
endef

# -----------------------------------------------------------------------------

define DERIVE_$(OBJEXT)
$(patsubst \
	%.hin, \
	%.h, \
	$1 \
)
endef

define GUESS_$(OBJEXT)
$(firstword \
	$(wildcard \
		$(call \
			MUNGE, \
			$(1:%.$(OBJEXT)=%.c) \
		) \
		$(call \
			MUNGE, \
			$(1:%.$(OBJEXT)=%.S) \
		) \
		$(call \
			MUNGE, \
			$(1:%.$(OBJEXT)=%.$(OBJEXT)) \
		) \
	) \
) \
$(firstword \
	$(wildcard \
		$(call \
			MUNGE, \
			$(1:%.$(OBJEXT)=%.hin) \
		) \
		$(call \
			MUNGE, \
			$(1:%.$(OBJEXT)=%.h) \
		) \
	) \
)
endef

define TEMPLATE_$(OBJEXT)
$(foreach \
	SOURCE, \
	$(filter \
		%.c, \
		$2 \
	), \
	$(call \
		TEMPLATE_$(OBJEXT)_c, \
		$1, \
		$(SOURCE), \
		$(filter-out \
			$(SOURCE), \
			$2 \
		) \
	) \
) \
$(foreach \
	SOURCE, \
	$(filter \
		%.S, \
		$2 \
	), \
	$(call \
		TEMPLATE_$(OBJEXT)_S, \
		$1, \
		$(SOURCE), \
		$(filter-out \
			$(SOURCE), \
			$2 \
		) \
	) \
)
endef

# -----------------------------------------------------------------------------

define TEMPLATE_d

$(call MUNGE, $1): $(call MUNGE, $2)
	+$(MAKE) -C $(call MUNGE, $1)

.PHONY: $(call MUNGE, $1)

endef

# -----------------------------------------------------------------------------

define GUESS_h
$(firstword \
	$(wildcard \
		$(call \
			MUNGE, \
			$(1:%.h=%.c) \
		) \
	) \
)
$(firstword \
	$(wildcard \
		$(call \
			MUNGE, \
			$(1:%.h=%.hin) \
		) \
	) \
)
endef

define TEMPLATE_h

$(call MUNGE, $1): $(call MUNGE, $2)
$(if $(TMPOUT),	$(TEST) -d $(TMPOUT) || $(MKDIR) $(TMPOUT))
	( \
echo "/* $(strip $1) generated from $(strip $2) */"; \
echo "/* please do not edit this file, edit the source files instead */"; \
echo ""; \
echo -n "#ifndef __"; \
echo "$(strip $1)" | \
	$(SED) -e "y/abcdefghijklmnopqrstuvwxyz./ABCDEFGHIJKLMNOPQRSTUVWXYZ_/"; \
echo -n "#define __"; \
echo "$(strip $1)" | \
	$(SED) -e "y/abcdefghijklmnopqrstuvwxyz./ABCDEFGHIJKLMNOPQRSTUVWXYZ_/"; \
echo ""; \
echo -n "/* -------------------------------------"; \
echo "------------------------------------ */"; \
( \
$(foreach HINDEP, $(filter %.hin, $2), $(CAT) $(call MUNGE, $(HINDEP));) \
) | $(TEE) $(call MUNGE, $1); \
$(CPROTO) -E "$(CPP) $(strip $($(call CANON, $1)_CPPFLAGS) \
$(foreach DEFINE, $($(call CANON, $1)_DEFINES), -D$(DEFINE)) \
$(foreach INCDIR, $($(call CANON, $1)_INCDIRS), -I$(INCDIR)))" \
$(call MUNGE, $(filter %.c, $2)); \
echo ""; \
echo -n "/* -------------------------------------"; \
echo "------------------------------------ */"; \
echo ""; \
echo -n "#endif /* __"; \
echo -n "$(strip $1)" | \
	$(SED) -e "y/abcdefghijklmnopqrstuvwxyz./ABCDEFGHIJKLMNOPQRSTUVWXYZ_/"; \
echo " */"; \
echo ""; ) > $(call MUNGE, cproto.tmp)
	$(MV) $(call MUNGE, cproto.tmp) $(call MUNGE, $1)

endef

# -----------------------------------------------------------------------------

define DERIVE_po
$(patsubst \
	%.c, \
	%.i, \
	$1 \
)
endef

define GUESS_po
$(firstword \
	$(wildcard \
		$(call \
			MUNGE, \
			$(1:%.po=%.c) \
		) \
		$(call \
			MUNGE, \
			$(1:%.po=%.S) \
		) \
	) \
)
endef

define TEMPLATE_po

$(call MUNGE, $1): $(call MUNGE, $2)
$(if $(TMPOUT),	$(TEST) -d $(TMPOUT) || $(MKDIR) $(TMPOUT))
	( \
$(foreach INTDEP, \
	$(filter %.$(INTEXT), $2), $(ECHO) "c2po $(call MUNGE, $(INTDEP))";) \
) > $(call MUNGE, $(1:%.po=%.txt))
	restool $(call MUNGE, $(1:%.po=%.txt)) $(call MUNGE, $1) \
$(HDROUT)po/$(1:%.po=%.h) $(LIBOUT)$(1:%.po=%.$(LIBEXT))

endef

# -----------------------------------------------------------------------------

define TEMPLATE_phony

$(call MUNGE, $1): $(call MUNGE, $2)

.PHONY: $(call MUNGE, $1)

endef

# -----------------------------------------------------------------------------

include $(TOPMAKDIR)$(ARCH:%=%/)arch_gens.mk

# -----------------------------------------------------------------------------

