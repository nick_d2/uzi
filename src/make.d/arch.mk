# /uzi/src/make.d/arch.mk by Nick for Hytech NOS/UZI project

# Enter with ARCH = the architecture currently being compiled (win, z80, etc)
# Enter with TOPSRCDIR = relative path to /uzi/src top level source directory

# -----------------------------------------------------------------------------

include $(TOPMAKDIR)top_defs.mk
include $(TOPMAKDIR)top_gens.mk

# -----------------------------------------------------------------------------

