# /uzi/src/make.d/z80/arch_defs.mk by Nick for Hytech NOS/UZI project

# -----------------------------------------------------------------------------

AR=		lib
AS=		masm
CC=		cl
CPP=		cl /P
LD=		link
LN=		$(CP)

#ASFLAGS+=	-l -o -z
CFLAGS+=	/Zi
#LDFLAGS+=	-i -m -u -z
#STDLIBS+=	crt_$(MODEL).$(LIBEXT) sys_$(MODEL).$(LIBEXT) iar.$(LIBEXT)

ASMEXT=		asm
EXEEXT=		.exe
LBREXT=		lbr
LIBEXT=		lib
LNKEXT=		lnk
OBJEXT=		obj

TMPOUT=		$(BUILD:%=%/)$(ARCH:%=%/)

# -----------------------------------------------------------------------------

