# /uzi/src/make.d/z80/arch_gens.mk by Nick for Hytech NOS/UZI project

# -----------------------------------------------------------------------------

define TEMPLATE$(call CANON, $(EXEEXT))

$(call MUNGE, $1): $(call MUNGE, $(filter-out %.h, $2))
$(if $(EXEOUT),	$(TEST) -d $(EXEOUT) || $(MKDIR) $(EXEOUT))
	( \
$(ECHO) "/debug"; \
$(ECHO) "/subsystem:console"; \
$(foreach LIBDIR, $(LIBDIRS), $(ECHO) "\"/libpath:$(LIBDIR)\"";) \
$(foreach LIBDEP, $(filter %.$(LIBEXT), $2), $(ECHO) "$(LIBDEP)";) \
$(foreach STDLIB, $(STDLIBS), $(ECHO) "$(STDLIB)";) \
$(ECHO) "/out:$(call MUNGE, $1)"; \
$(foreach OBJDEP, \
	$(filter %.$(OBJEXT), $2), $(ECHO) "$(call MUNGE, $(OBJDEP))";) \
) > $(call MUNGE, $(1:%$(EXEEXT)=%.$(LNKEXT)))
	$(LD) $(strip $($(call CANON, $1)_LDFLAGS)) \
@$(call MUNGE, $(1:%$(EXEEXT)=%.$(LNKEXT)))

endef

# -----------------------------------------------------------------------------

define TEMPLATE_$(BINEXT)

$(call MUNGE, $1): $(call MUNGE, $(filter-out %.h, $2))
$(if $(BINOUT),	$(TEST) -d $(BINOUT) || $(MKDIR) $(BINOUT))
	( \
$(foreach LIBDIR, $(LIBDIRS), $(ECHO) "-k $(LIBDIR)";) \
$(foreach LIBDEP, $(filter %.$(LIBEXT), $2), $(ECHO) "-l $(LIBDEP)";) \
$(foreach STDLIB, $(STDLIBS), $(ECHO) "-l $(STDLIB)";) \
$(ECHO) "-o $(call MUNGE, $(1:%.$(BINEXT)=%.$(IHXEXT)))"; \
$(ECHO) "-bl RCODE=0"; \
$(ECHO) "-bl ICODE=0x1100"; \
$(ECHO) "-bl CODE=0x80f000,0x10000"; \
$(ECHO) "-bc CODE=0x1000"; \
$(ECHO) "-w"; \
$(foreach OBJDEP, \
	$(filter %.$(OBJEXT), $2), $(ECHO) "$(call MUNGE, $(OBJDEP))";) \
) > $(call MUNGE, $(1:%.$(BINEXT)=%.$(LNKEXT)))
	$(LD) $(strip $($(call CANON, $1)_LDFLAGS) -f) \
$(call MUNGE, $(1:%.$(BINEXT)=%.$(LNKEXT)))
	ihex2bin $(call MUNGE, $(1:%.$(BINEXT)=%.$(IHXEXT))) $(call MUNGE, $1)

endef

# -----------------------------------------------------------------------------

define TEMPLATE_$(LIBEXT)

$(call MUNGE, $1): $(call MUNGE, $(filter-out %.h, $2))
$(if $(LIBOUT),	$(TEST) -d $(LIBOUT) || $(MKDIR) $(LIBOUT))
	( \
$(ECHO) "-o $(call MUNGE, $1)"; \
$(foreach OBJDEP, \
	$(filter %.$(OBJEXT), $2), $(ECHO) "$(call MUNGE, $(OBJDEP))";) \
) > $(call MUNGE, $(1:%.$(LIBEXT)=%.$(LBREXT)))
	$(AR) $(strip $($(call CANON, $1)_ARFLAGS) -f) \
$(call MUNGE, $(1:%.$(LIBEXT)=%.$(LBREXT)))

endef

# -----------------------------------------------------------------------------

define TEMPLATE_$(OBJEXT)_c

$(call MUNGE, $1): $(strip $(call MUNGE, $2) $(call MUNGE, $3))
$(if $(TMPOUT),	$(TEST) -d $(TMPOUT) || $(MKDIR) $(TMPOUT))
	$(CC) $(strip $($(call CANON, $1)_CPPFLAGS) \
$(foreach DEFINE, $($(call CANON, $1)_DEFINES), -D$(DEFINE)) \
$(foreach INCDIR, $($(call CANON, $1)_INCDIRS), -I$(INCDIR))) \
$($(call CANON, $1)_CFLAGS) /c $(call MUNGE, $2)
	$(MV) $(patsubst %.c, %.$(OBJEXT), $(call MUNGE, $2)) $(call MUNGE, $1)

endef

define TEMPLATE_$(OBJEXT)_S

$(call MUNGE, $1): $(strip $(call MUNGE, $2) $(call MUNGE, $3))
$(if $(TMPOUT),	$(TEST) -d $(TMPOUT) || $(MKDIR) $(TMPOUT))
	$(CPP) $(strip $($(call CANON, $1)_CPPFLAGS) \
$(foreach DEFINE, $($(call CANON, $1)_DEFINES), -D$(DEFINE)) \
$(foreach INCDIR, $($(call CANON, $1)_INCDIRS), -I$(INCDIR))) \
$(call MUNGE, $2) $(call MUNGE, $(1:%.$(OBJEXT)=%.$(ASMEXT)))
	$(AS) $($(call CANON, $1)_ASFLAGS) \
$(call MUNGE, $(1:%.$(OBJEXT)=%.$(ASMEXT)))

endef

# -----------------------------------------------------------------------------

