cl -Zi -I. -I..\kernel\uzi -DVAX -DNROFF -c n1.c
@if errorlevel 1 goto failure
cl -Zi -I. -I..\kernel\uzi -DVAX -DNROFF -c n2.c
@if errorlevel 1 goto failure
cl -Zi -I. -I..\kernel\uzi -DVAX -DNROFF -c n3.c
@if errorlevel 1 goto failure
cl -Zi -I. -I..\kernel\uzi -DVAX -DNROFF -c n4.c
@if errorlevel 1 goto failure
cl -Zi -I. -I..\kernel\uzi -DVAX -DNROFF -c n5.c
@if errorlevel 1 goto failure
cl -Zi -I. -I..\kernel\uzi -DVAX -DNROFF -c n6.c
@if errorlevel 1 goto failure
cl -Zi -I. -I..\kernel\uzi -DVAX -DNROFF -c n7.c
@if errorlevel 1 goto failure
cl -Zi -I. -I..\kernel\uzi -DVAX -DNROFF -c n8.c
@if errorlevel 1 goto failure
cl -Zi -I. -I..\kernel\uzi -DVAX -DNROFF -c n9.c
@if errorlevel 1 goto failure
cl -Zi -I. -I..\kernel\uzi -DVAX -DNROFF -c n10.c
@if errorlevel 1 goto failure
cl -Zi -I. -I..\kernel\uzi -DVAX -DNROFF -c ni.c
@if errorlevel 1 goto failure
cl -Zi -I. -I..\kernel\uzi -DVAX -DNROFF -c nii.c
@if errorlevel 1 goto failure
cl -Zi -I. -I..\kernel\uzi -DVAX -DNROFF -c ntab.c
@if errorlevel 1 goto failure
cl -Zi -I. -I..\kernel\uzi -DVAX -DNROFF -c hytab.c
@if errorlevel 1 goto failure
cl -Zi -I. -I..\kernel\uzi -DVAX -DNROFF -c suftab.c
@if errorlevel 1 goto failure
link @nroff.w32
@if errorlevel 1 goto failure
copy nroff.exe ..\bin

iccz80 -S -w -mb -v1 -z -A -I..\..\include\ -DNROFF n1
@if errorlevel 1 goto failure
del n1.r01
as-z80 -l -o n1.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\include\ -DNROFF n2
@if errorlevel 1 goto failure
del n2.r01
as-z80 -l -o n2.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\include\ -DNROFF n3
@if errorlevel 1 goto failure
del n3.r01
as-z80 -l -o n3.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\include\ -DNROFF n4
@if errorlevel 1 goto failure
del n4.r01
as-z80 -l -o n4.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\include\ -DNROFF n5
@if errorlevel 1 goto failure
del n5.r01
as-z80 -l -o n5.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\include\ -DNROFF n6
@if errorlevel 1 goto failure
del n6.r01
as-z80 -l -o n6.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\include\ -DNROFF n7
@if errorlevel 1 goto failure
del n7.r01
as-z80 -l -o n7.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\include\ -DNROFF n8
@if errorlevel 1 goto failure
del n8.r01
as-z80 -l -o n8.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\include\ -DNROFF n9
@if errorlevel 1 goto failure
del n9.r01
as-z80 -l -o n9.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\include\ -DNROFF n10
@if errorlevel 1 goto failure
del n10.r01
as-z80 -l -o n10.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\include\ -DNROFF ni
@if errorlevel 1 goto failure
del ni.r01
as-z80 -l -o ni.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\include\ -DNROFF nii
@if errorlevel 1 goto failure
del nii.r01
as-z80 -l -o nii.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\include\ -DNROFF ntab
@if errorlevel 1 goto failure
del ntab.r01
as-z80 -l -o ntab.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\include\ -DNROFF hytab
@if errorlevel 1 goto failure
del hytab.r01
as-z80 -l -o hytab.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\include\ -DNROFF suftab
@if errorlevel 1 goto failure
del suftab.r01
as-z80 -l -o suftab.s01
@if errorlevel 1 goto failure

link-z80 -f nroff
@if errorlevel 1 goto failure
ihex2bin -l nroff.i86 ..\..\bin\banked\nroff
@if errorlevel 1 goto failure

@echo SUCCESS
@goto done
:failure
@echo FAILURE
:done

