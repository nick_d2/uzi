rem iccz80 -S -w -ml -v1 -z -A -I..\..\include\ tab37
rem @if errorlevel 1 goto failure
rem del tab37.r01
rem as-z80 -l -o tab37.s01
rem @if errorlevel 1 goto failure

as-z80 -l -o tab37.asm
@if errorlevel 1 goto failure

link-z80 -f tab37
@if errorlevel 1 goto failure
ihex2bin tab37.i86 tab37
@if errorlevel 1 goto failure

@echo SUCCESS
@goto done
:failure
@echo FAILURE
:done

