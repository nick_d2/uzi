; LMUL.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?L_MUL_L03
		rseg	RCODE
rcode_base:
		public	?L_MUL_L03
?L_MUL_L03	equ	rcode_base+00000000h
		defb	0E3h,0D5h,0DDh,0E5h,0DDh,021h,000h
		defb	000h,0DDh,039h,0DDh,056h,009h,0DDh
		defb	074h,009h,0DDh,05Eh,008h,0DDh,075h
		defb	008h,0D5h,0C5h,0F5h,0DDh,06Eh,004h
		defb	062h,0EDh,06Ch,0DDh,056h,005h,0EDh
		defb	05Ch,019h,059h,0DDh,056h,007h,0EDh
		defb	05Ch,019h,0DDh,05Eh,006h,050h,0EDh
		defb	05Ch,019h,0E5h,0DDh
		defb	05Eh,004h,0DDh,056h,0FEh,0EDh,05Ch
		defb	0DDh,06Eh,005h,0DDh,066h,007h,0EDh
		defb	06Ch,019h,0DDh,046h,006h,0EDh,04Ch
		defb	009h,0E5h,0DDh,05Eh,004h,0DDh,056h
		defb	007h,0EDh,05Ch,0DDh,06Eh,005h,0DDh
		defb	066h,006h,0EDh,06Ch,0AFh,019h,017h
		defb	0F5h,0DDh,05Eh,004h,0DDh,056h,006h
		defb	0EDh,05Ch,04Ah,006h
		defb	000h,0AFh,009h,017h,055h,0C1h,04Ch
		defb	0E1h,009h,084h,0C1h,081h,04Dh,047h
		defb	0EBh,0F1h,0D1h,0D1h,0DDh,0E1h,0D1h
		defb	033h,033h,033h,033h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
