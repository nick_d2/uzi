; frexp.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	frexp
		rseg	CODE
code_base:
		public	frexp
frexp		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?BANK_FAST_LEAVE_L08
		extern	?BANK_LEAVE_32_L08
		extern	?ENT_PARM_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_PARM_DIRECT_L09
		defb	079h,0B0h,020h,00Fh,0DDh,06Eh,00Ah
		defb	0DDh,066h,00Bh,077h,023h,077h,04Fh
		defb	069h,047h,060h,018h,022h,079h,007h
		defb	078h,017h,0D6h,07Eh,04Fh,007h,09Fh
		defb	047h,0DDh,06Eh,00Ah,0DDh,066h,00Bh
		defb	071h,023h,070h,0DDh,04Eh,004h,0DDh
		defb	046h,005h,0EBh,0CBh,0B0h,0CBh,0B9h
		defb	03Eh,03Fh,0B0h,047h
		defb	0C3h
		defw	LWRD ?BANK_LEAVE_32_L08
		endmod

; -----------------------------------------------------------------------------

	end
