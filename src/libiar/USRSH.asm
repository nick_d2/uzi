; USRSH.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?US_RSH_L02
		rseg	RCODE
rcode_base:
		public	?US_RSH_L02
?US_RSH_L02	equ	rcode_base+00000000h
		defb	004h,005h,0C8h,0CBh,03Ah,0CBh,01Bh
		defb	010h,0FAh,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
