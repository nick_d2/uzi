; malloc.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	malloc
                             00000000 "busy" [0070]    00000001 "next" [0066]
		rseg	CODE
code_base:
		extern	_heap_of_memory
		extern	_last_heap_object
		public	_make_new_mem_hole
_make_new_mem_hole	equ	code_base+00000000h
		extern	_top_of_heap
		public	malloc
malloc		equ	code_base+00000047h
		extern	?CL64180B_4_06_L00
		extern	?SS_CMP_L02
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	000h,000h,0FDh,0E5h,0DDh,06Eh,002h
		defb	0DDh,066h,003h,0E5h,0FDh,0E1h,0FDh
		defb	06Eh,001h,0FDh,066h,002h,0A7h,0EDh
		defb	042h,04Dh,044h,021h,003h,000h,0CDh
		defw	LWRD ?SS_CMP_L02
		defb	030h,01Fh,0FDh,05Eh,001h,0FDh,056h
		defb	002h,0DDh,06Eh,004h,0FDh,075h,001h
		defb	0DDh,066h,005h,0FDh,074h,002h,0E5h
		defb	0FDh,0E1h,0FDh,036h,000h,000h,0FDh
		defb	073h,001h,0FDh,072h,002h,0FDh,0E1h
		defb	0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0FEh,0FFh,0FDh,0E5h,0DDh,04Eh,002h
		defb	0DDh,046h,003h,003h,003h,003h,0C5h
		defb	0EDh,04Bh
		defw	LWRD _last_heap_object
		defb	02Ah
		defw	LWRD _top_of_heap
		defb	0A7h,0EDh,042h,0C1h,0A7h,0EDh,042h
		defb	038h,02Dh,0FDh,02Ah
		defw	LWRD _last_heap_object
		defb	0FDh,036h,000h,001h,0DDh,06Eh,002h
		defb	0DDh,066h,003h,023h,023h,023h,0EDh
		defb	04Bh
		defw	LWRD _last_heap_object
		defb	009h,022h
		defw	LWRD _last_heap_object
		defb	0FDh,075h,001h,0FDh,074h,002h,0DDh
		defb	04Eh,002h,0DDh,046h,003h,02Ah
		defw	LWRD _last_heap_object
		defb	0A7h,0EDh,042h,018h,05Fh,0EDh,05Bh
		defw	LWRD _heap_of_memory
		defb	0EDh,04Bh
		defw	LWRD _last_heap_object
		defb	06Bh,062h,0A7h,0EDh,042h,030h,04Dh
		defb	06Bh,062h,0E5h,0FDh,0E1h,0AFh,0B6h
		defb	020h,03Ch,0FDh,06Eh,001h,0FDh,066h
		defb	002h,0EDh,052h,001h,0FDh,0FFh,009h
		defb	0DDh,04Eh,002h,0DDh,046h,003h,0A7h
		defb	0EDh,042h,038h,025h,06Bh,062h,023h
		defb	023h,023h,0DDh,075h,0FEh,0DDh,074h
		defb	0FFh,0FDh,036h,000h
		defb	001h,009h,04Dh,044h,0FDh,0E5h,0D1h
		defb	03Eh
		defb	BYTE3 code_base+0000000h
		defb	021h
		defw	LWRD code_base+00000h
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0DDh,06Eh,0FEh,0DDh,066h,0FFh,018h
		defb	00Bh,0FDh,05Eh,001h,0FDh,056h,002h
		defb	018h,0A8h,021h,000h,000h,0FDh,0E1h
		defb	0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
