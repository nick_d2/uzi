; putchar2.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	putchar2
		rseg	CODE
code_base:
		public	putchar
putchar		equ	code_base+00000000h
		public	?C_PUTCHAR
?C_PUTCHAR	equ	code_base+00000003h
		public	?DBG_2T
?DBG_2T		equ	00000000h
		extern	?BANK_FAST_LEAVE_L08
		defb	0C5h,04Bh,042h,000h,000h,000h,0C1h
		defb	06Bh,062h,0C3h
		defw	LWRD ?BANK_FAST_LEAVE_L08
		endmod

; -----------------------------------------------------------------------------

	end
