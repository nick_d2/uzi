; SMUL.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?S_MUL_L02
		rseg	RCODE
rcode_base:
		public	?S_MUL_L02
?S_MUL_L02	equ	rcode_base+00000000h
		defb	0F5h,0C5h,0E5h,078h,042h,051h,067h
		defb	06Bh,0EDh,04Ch,0EDh,06Ch,0EDh,05Ch
		defb	079h,085h,082h,057h,0E1h,0C1h,0F1h
		defb	0C9h
		endmod

; -----------------------------------------------------------------------------

	end
