; SFINDSIGN.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?S_FIND_SIGN_L02
		rseg	RCODE
rcode_base:
		public	?S_FIND_SIGN_L02
?S_FIND_SIGN_L02	equ	rcode_base+00000000h
		defb	0CBh,078h,028h,009h,0AFh,091h,04Fh
		defb	03Eh,000h,098h,047h,03Eh,001h,0CBh
		defb	07Ah,0C8h,0F5h,0AFh,093h,05Fh,03Eh
		defb	000h,09Ah,057h,0F1h,0EEh,001h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
