; LXORASG.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?L_XORASG_L03
		rseg	RCODE
rcode_base:
		public	?L_XORASG_L03
?L_XORASG_L03	equ	rcode_base+00000000h
		defb	0F5h,07Eh,0ABh,077h,05Fh,023h,07Eh
		defb	0AAh,077h,057h,023h,07Eh,0A9h,077h
		defb	04Fh,023h,07Eh,0A8h,077h,047h,02Bh
		defb	02Bh,02Bh,0F1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
