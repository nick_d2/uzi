; exit1.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	exit1
		rseg	RCODE
rcode_base:
		public	exit
exit		equ	rcode_base+00000001h
		public	?C_EXIT
?C_EXIT		equ	rcode_base+00000001h
		public	?C_FUNCALL
?C_FUNCALL	equ	rcode_base+00000000h
		public	?DBG_1
?DBG_1		equ	00000000h
		defb	000h,000h,000h,000h,018h,0FBh
		endmod

; -----------------------------------------------------------------------------

	end
