; BFCORASG.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?BF_C_ORASG_L10
		rseg	RCODE
rcode_base:
		public	?BF_C_ORASG_L10
?BF_C_ORASG_L10	equ	rcode_base+00000000h
		extern	?BF_C_SHIFT_UP_L10
		extern	?BF_C_RET_VAL_L10
		defb	0EBh,0E3h,0C5h,0D5h,05Eh,023h,056h
		defb	023h,0E3h,0F5h,0CDh
		defw	LWRD ?BF_C_SHIFT_UP_L10
		defb	047h,07Eh,0B0h,0C3h
		defw	LWRD ?BF_C_RET_VAL_L10
		endmod

; -----------------------------------------------------------------------------

	end
