; abort.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	abort
		rseg	CODE
code_base:
		public	abort
abort		equ	code_base+00000000h
		extern	exit
		extern	?CL64180B_4_06_L00
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		defb	0D5h,011h,001h,000h,03Eh
		defb	BYTE3 exit
		defb	021h
		defw	LWRD exit
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0D1h,0C3h
		defw	LWRD ?BANK_FAST_LEAVE_L08
		endmod

; -----------------------------------------------------------------------------

	end
