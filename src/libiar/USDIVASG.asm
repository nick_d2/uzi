; USDIVASG.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?US_DIVASG_L02
		rseg	RCODE
rcode_base:
		public	?US_DIVASG_L02
?US_DIVASG_L02	equ	rcode_base+00000000h
		extern	?US_DIV_L02
		defb	05Eh,023h,056h,0CDh
		defw	LWRD ?US_DIV_L02
		defb	072h,02Bh,073h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
