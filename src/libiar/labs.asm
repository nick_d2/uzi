; labs.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	labs
		rseg	CODE
code_base:
		public	labs
labs		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?L_NEG_L03
		extern	?BANK_FAST_LEAVE_L08
		extern	?BANK_LEAVE_32_L08
		extern	?ENT_PARM_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_PARM_DIRECT_L09
		defb	0CBh,078h,0EBh,028h,003h,0CDh
		defw	LWRD ?L_NEG_L03
		defb	0C3h
		defw	LWRD ?BANK_LEAVE_32_L08
		endmod

; -----------------------------------------------------------------------------

	end
