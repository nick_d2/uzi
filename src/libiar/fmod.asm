; fmod.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	fmod
		rseg	CODE
code_base:
		extern	floor
		public	fmod
fmod		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?F_MUL_L04
		extern	?F_DIV_L04
		extern	?F_SUB_L04
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?BANK_LEAVE_32_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0FCh,0FFh,0DDh,07Eh,00Ch,0DDh,0B6h
		defb	00Dh,020h,006h,04Fh,069h,047h,060h
		defb	018h,067h,0DDh,06Eh,00Ch,0DDh,066h
		defb	00Dh,0E5h,0DDh,06Eh,00Ah,0DDh,066h
		defb	00Bh,0E5h,0DDh,06Eh,002h,0DDh,066h
		defb	003h,0CDh
		defw	LWRD ?F_DIV_L04
		defb	0CBh,078h,05Dh,054h,028h,014h,0CBh
		defb	0B8h,03Eh
		defb	BYTE3 floor
		defb	021h
		defw	LWRD floor
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	079h,0B0h,028h,00Eh,078h,0EEh,080h
		defb	047h,018h,008h,03Eh
		defb	BYTE3 floor
		defb	021h
		defw	LWRD floor
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0DDh,075h,0FCh,0DDh,074h,0FDh,0DDh
		defb	06Eh,00Ch,0DDh,066h,00Dh,0E5h,0DDh
		defb	06Eh,00Ah,0DDh,066h,00Bh,0E5h,0DDh
		defb	06Eh,0FCh,0DDh,066h,0FDh,0CDh
		defw	LWRD ?F_MUL_L04
		defb	0C5h,0E5h,0DDh,04Eh,004h,0DDh,046h
		defb	005h,0DDh,06Eh,002h,0DDh,066h,003h
		defb	0CDh
		defw	LWRD ?F_SUB_L04
		defb	0C3h
		defw	LWRD ?BANK_LEAVE_32_L08
		endmod

; -----------------------------------------------------------------------------

	end
