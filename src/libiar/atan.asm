; atan.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	atan
		rseg	CODE
code_base:
		extern	__satan
		public	atan
atan		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?BANK_LEAVE_32_L08
		extern	?ENT_PARM_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_PARM_DIRECT_L09
		defb	0CBh,078h,03Eh
		defb	BYTE3 __satan
		defb	021h
		defw	LWRD __satan
		defb	020h,005h,0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	018h,00Bh,0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	079h,0B0h,028h,004h,078h,0EEh,080h
		defb	047h,0C3h
		defw	LWRD ?BANK_LEAVE_32_L08
		endmod

; -----------------------------------------------------------------------------

	end
