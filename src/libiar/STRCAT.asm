; STRCAT.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?STRCAT_L11
		rseg	RCODE
rcode_base:
		public	?STRCAT_L11
?STRCAT_L11	equ	rcode_base+00000000h
		defb	0E5h,0D5h,0C5h,0F5h,0AFh,047h,04Fh
		defb	0EDh,0B1h,02Bh,0EBh,0BEh,0EDh,0A0h
		defb	020h,0FBh,0F1h,0C1h,0D1h,0E1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
