; FDEC.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?F_DEC_L04
		rseg	RCODE
rcode_base:
		public	?F_DEC_L04
?F_DEC_L04	equ	rcode_base+00000000h
		extern	?F_ADD_L04
		defb	0C5h,0E5h,001h,080h,0BFh,021h,000h
		defb	000h,0CDh
		defw	LWRD ?F_ADD_L04
		defb	0C9h
		endmod

; -----------------------------------------------------------------------------

	end
