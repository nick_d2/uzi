; LIBVERSION.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?LIB_VERSION_L00
		public	?CL64180B_4_06_L00
?CL64180B_4_06_L00	equ	00000000h
		public	?CL64180L_4_06_L00 ; Nick
?CL64180L_4_06_L00	equ	00000000h ; Nick
		endmod

; -----------------------------------------------------------------------------

	end
