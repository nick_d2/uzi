; strcatlc.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	strcatlc
		rseg	CODE
code_base:
		public	strcat
strcat		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_PARM_DIRECT_L09
		extern	?STRCAT_L11
		defb	0CDh
		defw	LWRD ?ENT_PARM_DIRECT_L09
		defb	059h,050h,0DDh,06Eh,002h,0DDh,066h
		defb	003h,0CDh
		defw	LWRD ?STRCAT_L11
		defb	0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
