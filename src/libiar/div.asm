; div.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	div
                             00000000 "quot" [0066]    00000002 "rem" [0066]
		rseg	CODE
code_base:
		public	div
div		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?SS_DIV_L02
		extern	?S_MUL_L02
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0FCh,0FFh,0DDh,04Eh,00Ah,0DDh,046h
		defb	00Bh,0DDh,05Eh,004h,0DDh,056h,005h
		defb	0CDh
		defw	LWRD ?SS_DIV_L02
		defb	0DDh,073h,0FCh,0DDh,072h,0FDh,0CDh
		defw	LWRD ?S_MUL_L02
		defb	0DDh,06Eh,004h,0DDh,066h,005h,0A7h
		defb	0EDh,052h,0DDh,075h,0FEh,0DDh,074h
		defb	0FFh,021h,000h,000h,039h,0DDh,05Eh
		defb	002h,0DDh,056h,003h,001h,004h,000h
		defb	0D5h,0EDh,0B0h,0E1h,0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
