; strrchr.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	strrchr
		rseg	CODE
code_base:
		public	strrchr
strrchr		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	000h,000h,0FDh,0E5h,0DDh,05Eh,002h
		defb	0DDh,056h,003h,0FDh,021h,000h,000h
		defb	06Bh,062h,0AFh,0B6h,028h,00Ch,07Eh
		defb	0DDh,0BEh,004h,020h,003h,0E5h,0FDh
		defb	0E1h,013h,018h,0EEh,0FDh,0E5h,0E1h
		defb	0FDh,0E1h,0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
