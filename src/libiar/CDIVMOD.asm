; CDIVMOD.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?C_DIVMOD_L01
		rseg	RCODE
rcode_base:
		public	?C_DIVMOD_L01
?C_DIVMOD_L01	equ	rcode_base+00000000h
		defb	011h,009h,000h,0CBh,011h,01Dh,0C8h
		defb	0CBh,012h,07Ah,090h,038h,0F6h,057h
		defb	018h,0F3h
		endmod

; -----------------------------------------------------------------------------

	end
