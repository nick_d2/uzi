; ldexp.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	ldexp
		rseg	CODE
code_base:
		extern	errno
		public	ldexp
ldexp		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?SS_CMP_L02
		extern	?BANK_FAST_LEAVE_L08
		extern	?BANK_LEAVE_32_L08
		extern	?ENT_AUTO_DIRECT_L09
		extern	?__daddexp_L11
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	000h,000h,0FDh,0E5h,079h,0B0h,028h
		defb	046h,079h,007h,078h,017h,0D6h,07Eh
		defb	04Fh,007h,09Fh,047h,0DDh,06Eh,00Ah
		defb	0DDh,066h,00Bh,009h,0E5h,0FDh,0E1h
		defb	001h,006h,07Fh,0DDh,06Eh,00Ah,0DDh
		defb	066h,00Bh,03Eh,080h,0ACh,067h,0EDh
		defb	042h,038h,00Eh,001h,083h,07Fh,0FDh
		defb	0E5h,0E1h,03Eh,080h
		defb	0ACh,067h,0EDh,042h,030h,01Ah,021h
		defb	022h,000h,022h
		defw	LWRD errno
		defb	0CDh
		defw	LWRD code_base+00099h
		defb	030h,008h,0DDh,0CBh,005h,07Eh,028h
		defb	02Ah,018h,023h,001h,000h,000h,069h
		defb	060h,018h,03Ch,0CDh
		defw	LWRD code_base+00099h
		defb	038h,00Bh,0FDh,0E5h,0C1h,021h,080h
		defb	000h,0CDh
		defw	LWRD ?SS_CMP_L02
		defb	030h,019h,021h,022h,000h,022h
		defw	LWRD errno
		defb	0DDh,0CBh,005h,07Eh,028h,005h,001h
		defb	07Fh,0FFh,018h,003h,001h,07Fh,07Fh
		defb	021h,0FFh,0FFh,018h,013h,0DDh,04Eh
		defb	004h,0DDh,046h,005h,0DDh,05Eh,002h
		defb	0DDh,056h,003h,0DDh,06Eh,00Ah,0CDh
		defw	LWRD ?__daddexp_L11
		defb	0EBh,0FDh,0E1h,0C3h
		defw	LWRD ?BANK_LEAVE_32_L08
		defb	0DDh,04Eh,00Ah,0DDh,046h,00Bh,021h
		defb	000h,001h,0C3h
		defw	LWRD ?SS_CMP_L02
		endmod

; -----------------------------------------------------------------------------

	end
