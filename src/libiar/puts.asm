; puts.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	puts
		rseg	CODE
code_base:
		extern	putchar
		public	puts
puts		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_PARM_DIRECT_L09
		defb	0DDh,0E5h,0D5h,0DDh,0E1h,0AFh,0DDh
		defb	0B6h,000h,028h,018h,0DDh,05Eh,000h
		defb	016h,000h,03Eh
		defb	BYTE3 putchar
		defb	021h
		defw	LWRD putchar
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	07Dh,0B4h,0DDh,023h,020h,0E7h,021h
		defb	0FFh,0FFh,018h,00Eh,011h,00Ah,000h
		defb	03Eh
		defb	BYTE3 putchar
		defb	021h
		defw	LWRD putchar
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	021h,001h,000h,0DDh,0E1h,0C3h
		defw	LWRD ?BANK_FAST_LEAVE_L08
		endmod

; -----------------------------------------------------------------------------

	end
