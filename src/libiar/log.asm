; log.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	log
		rseg	CODE
code_base:
		extern	errno
		extern	frexp
		public	log
log		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?SL_CMP_L03
		extern	?F_MUL_L04
		extern	?F_DIV_L04
		extern	?F_ADD_L04
		extern	?F_CMP_L04
		extern	?SL_TO_F_L04
		extern	?F_MULASG_L04
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_LEAVE_32_L08
		extern	?ENT_AUTO_DIRECT_L09
		extern	?__daddexp_L11
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0FAh,0FFh,0C5h,0DDh,06Eh,002h,0DDh
		defb	066h,003h,0E5h,001h,000h,000h,069h
		defb	060h,0CDh
		defw	LWRD ?F_CMP_L04
		defb	038h,020h,0DDh,07Eh,004h,0DDh,0B6h
		defb	005h,020h,00Ah,02Eh,022h,022h
		defw	LWRD errno
		defb	001h,07Fh,0FFh,018h,008h,02Eh,021h
		defb	022h
		defw	LWRD errno
		defb	001h,07Fh,07Fh,021h,0FFh,0FFh,0C3h
		defw	LWRD code_base+00111h
		defb	039h,0E5h,0DDh,04Eh,004h,0DDh,046h
		defb	005h,0DDh,05Eh,002h,0DDh,056h,003h
		defb	03Eh
		defb	BYTE3 frexp
		defb	021h
		defw	LWRD frexp
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0F1h,0DDh,075h,002h,0DDh,074h,003h
		defb	0DDh,071h,004h,0DDh,070h,005h,011h
		defb	035h,03Fh,0D5h,011h,0F3h,004h,0D5h
		defb	0CDh
		defw	LWRD ?SL_CMP_L03
		defb	030h,015h,02Eh,001h,0CDh
		defw	LWRD ?__daddexp_L11
		defb	0DDh,071h,004h,0DDh,070h,005h,0DDh
		defb	06Eh,0FAh,0DDh,066h,0FBh,02Bh,0DDh
		defb	075h,0FAh,021h,080h,03Fh,0E5h,021h
		defb	000h,000h,0E5h,0DDh,06Eh,002h,0DDh
		defb	066h,003h,0CDh
		defw	LWRD ?F_ADD_L04
		defb	0C5h,0E5h,021h,080h,0BFh,0E5h,021h
		defb	000h,000h,0E5h,0DDh,04Eh,004h,0DDh
		defb	046h,005h,0DDh,06Eh,002h,0DDh,066h
		defb	003h,0CDh
		defw	LWRD ?F_ADD_L04
		defb	0CDh
		defw	LWRD ?F_DIV_L04
		defb	0DDh,075h,002h,0DDh,074h,003h,0DDh
		defb	071h,004h,0DDh,070h,005h,0C5h,0E5h
		defb	0CDh
		defw	LWRD ?F_MUL_L04
		defb	0DDh,075h,0FCh,0DDh,074h,0FDh,0DDh
		defb	071h,0FEh,0DDh,070h,0FFh,021h,008h
		defb	000h,039h,0E5h,001h,0D4h,03Eh,0C5h
		defb	001h,022h,092h,0C5h,0CDh
		defw	LWRD code_base+00114h
		defb	0C5h,0E5h,001h,02Ah,03Fh,021h,0DDh
		defb	09Bh,0CDh
		defw	LWRD ?F_ADD_L04
		defb	0C5h,0E5h,0CDh
		defw	LWRD code_base+00114h
		defb	0C5h,0E5h,001h,000h,040h,06Ah,061h
		defb	0CDh
		defw	LWRD ?F_ADD_L04
		defb	0EBh,0E1h,0CDh
		defw	LWRD ?F_MULASG_L04
		defb	0C5h,0D5h,0DDh,06Eh,0FAh,07Dh,007h
		defb	09Fh,067h,044h,04Ch,0CDh
		defw	LWRD ?SL_TO_F_L04
		defb	0C5h,0E5h,001h,031h,03Fh,021h,018h
		defb	072h,0CDh
		defw	LWRD ?F_MUL_L04
		defb	0CDh
		defw	LWRD ?F_ADD_L04
		defb	0C3h
		defw	LWRD ?BANK_LEAVE_32_L08
		defb	0DDh,04Eh,0FEh,0DDh,046h,0FFh,0DDh
		defb	06Eh,0FCh,0DDh,066h,0FDh,0C3h
		defw	LWRD ?F_MUL_L04
		endmod

; -----------------------------------------------------------------------------

	end
