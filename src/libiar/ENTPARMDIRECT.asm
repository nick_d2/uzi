; ENTPARMDIRECT.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?ENT_PARM_DIRECT_L09
		rseg	RCODE
rcode_base:
		public	?ENT_PARM_DIRECT_L09
?ENT_PARM_DIRECT_L09	equ	rcode_base+00000000h
		defb	0E1h,0C5h,0D5h,0DDh,0E5h,0DDh,021h
		defb	000h,000h,0DDh,039h,0E9h
		endmod

; -----------------------------------------------------------------------------

	end
