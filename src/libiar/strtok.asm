; strtok.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	strtok
		rseg	CODE
code_base:
		rseg	CSTR
cstr_base:
		rseg	IDATA0
idata0_base:
		rseg	CDATA0
cdata0_base:
		extern	strcspn
		extern	strspn
		public	strtok
strtok		equ	cdata0_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0FCh,0FFh,0DDh,07Eh,002h,0DDh,0B6h
		defb	003h,028h,008h,0DDh,06Eh,002h,0DDh
		defb	066h,003h,018h,003h,02Ah
    t_rel_p16            (0B) 0000
		defb	0DDh,075h,0FCh,0DDh,074h,0FDh,0EBh
		defb	03Eh
		defb	BYTE3 strspn
		defb	021h
		defw	LWRD strspn
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	04Dh,044h,021h,000h,000h,039h,07Eh
		defb	081h,077h,023h,07Eh,088h,077h,0DDh
		defb	06Eh,0FCh,067h,0AFh,0B6h,020h,00Ah
		defb	021h
    t_rel_p16            (01) 0000
		defb	022h
    t_rel_p16            (0B) 0000
		defb	06Fh,067h,018h,035h,0DDh,04Eh,004h
		defb	0DDh,046h,005h,0EBh,03Eh
		defb	BYTE3 strcspn
		defb	021h
		defw	LWRD strcspn
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0DDh,04Eh,0FCh,0DDh,046h,0FDh,009h
		defb	0DDh,075h,0FEh,0DDh,074h,0FFh,0AFh
		defb	0B6h,028h,00Ah,023h,0DDh,075h,0FEh
		defb	0DDh,074h,0FFh,02Bh,036h,000h,0DDh
		defb	06Eh,0FEh,0DDh,066h,0FFh,022h
    t_rel_p16            (0B) 0000
		defb	069h,060h,0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
    t_org_rel            (01) 00000000
		defb	000h
    t_org_rel            (0B) 00000000
    t_org_rel            (0B) 00000002
    t_org_rel            (0C) 00000000
    t_rel_p16            (01) 0000
		endmod

; -----------------------------------------------------------------------------

	end
