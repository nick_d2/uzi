; LNOT.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?L_NOT_L03
		rseg	RCODE
rcode_base:
		public	?L_NOT_L03
?L_NOT_L03	equ	rcode_base+00000000h
		defb	0F5h,07Dh,02Fh,06Fh,07Ch,02Fh,067h
		defb	079h,02Fh,04Fh,078h,02Fh,047h,0F1h
		defb	0C9h
		endmod

; -----------------------------------------------------------------------------

	end
