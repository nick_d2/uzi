; floor.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	floor
		rseg	CODE
code_base:
		public	floor
floor		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?SL_CMP_L03
		extern	?F_SUB_L04
		extern	?F_TO_L_L04
		extern	?SL_TO_F_L04
		extern	?F_ADDASG_L04
		extern	?BANK_FAST_LEAVE_L08
		extern	?BANK_LEAVE_32_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0FCh,0FFh,021h,018h,04Bh,0E5h,021h
		defb	080h,096h,0E5h,0DDh,06Eh,002h,0DDh
		defb	066h,003h,0CBh,0B8h,0CDh
		defw	LWRD ?SL_CMP_L03
		defb	038h,005h,0DDh,046h,005h,018h,05Eh
		defb	0DDh,0CBh,005h,07Eh,0DDh,046h,005h
		defb	028h,04Fh,0CBh,0B8h,0DDh,070h,005h
		defb	0CDh
		defw	LWRD ?F_TO_L_L04
		defb	0CDh
		defw	LWRD ?SL_TO_F_L04
		defb	0DDh,075h,0FCh,0DDh,074h,0FDh,0DDh
		defb	071h,0FEh,0DDh,070h,0FFh,0C5h,0E5h
		defb	0DDh,04Eh,004h,0DDh,046h,005h,0DDh
		defb	06Eh,002h,0DDh,066h,003h,0CDh
		defw	LWRD ?F_SUB_L04
		defb	079h,0B0h,028h,00Dh,021h,000h,000h
		defb	039h,001h,080h,03Fh,011h,000h,000h
		defb	0CDh
		defw	LWRD ?F_ADDASG_L04
		defb	0DDh,04Eh,0FEh,079h,0DDh,046h,0FFh
		defb	0DDh,06Eh,0FCh,0DDh,066h,0FDh,0B0h
		defb	028h,00Ch,078h,0EEh,080h,047h,018h
		defb	006h,0CDh
		defw	LWRD ?F_TO_L_L04
		defb	0CDh
		defw	LWRD ?SL_TO_F_L04
		defb	0C3h
		defw	LWRD ?BANK_LEAVE_32_L08
		endmod

; -----------------------------------------------------------------------------

	end
