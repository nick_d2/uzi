; BFSCEXT.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?BF_SC_EXT_L10
		rseg	RCODE
rcode_base:
		public	?BF_SC_EXT_L10
?BF_SC_EXT_L10	equ	rcode_base+00000000h
		defb	006h,080h,07Ah,0A0h,020h,004h,0CBh
		defb	028h,018h,0F8h,079h,0A0h,0C8h,079h
		defb	0B0h,04Fh,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
