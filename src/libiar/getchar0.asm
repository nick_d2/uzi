; getchar0.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	getchar0
		rseg	CODE
code_base:
		public	getchar
getchar		equ	code_base+00000000h
		public	?C_GETCHAR
?C_GETCHAR	equ	code_base+00000001h
		public	?DBG_0T
?DBG_0T		equ	00000000h
		extern	?BANK_FAST_LEAVE_L08
		defb	0C5h,069h,060h,0C1h,0C3h
		defw	LWRD ?BANK_FAST_LEAVE_L08
		endmod

; -----------------------------------------------------------------------------

	end
