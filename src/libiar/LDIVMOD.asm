; LDIVMOD.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?L_DIVMOD_L03
		rseg	RCODE
rcode_base:
		public	?L_DIVMOD_L03
?L_DIVMOD_L03	equ	rcode_base+00000000h
		defb	0F5h,021h,000h,000h,001h,000h,000h
		defb	0DDh,036h,0F8h,021h,018h,002h,019h
		defb	037h,0DDh,0CBh,0FCh,016h,0DDh,0CBh
		defb	0FDh,016h,0DDh,0CBh,0FEh,016h,0DDh
		defb	0CBh,0FFh,016h,0DDh,035h,0F8h,028h
		defb	02Eh,0CBh,011h,0CBh,010h,0CBh,015h
		defb	0CBh,014h,0EDh,052h,038h,0DDh,020h
		defb	010h,078h,0DDh,096h
		defb	007h,038h,0D5h,020h,008h,079h,0DDh
		defb	096h,006h,038h,0CDh,018h,004h,079h
		defb	0DDh,096h,006h,04Fh,078h,0DDh,09Eh
		defb	007h,047h,030h,0C1h,02Bh,0A7h,018h
		defb	0BDh,0F1h,0E5h,060h,069h,0C1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
