; USMOD.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?US_MOD_L02
		rseg	RCODE
rcode_base:
		public	?US_MOD_L02
?US_MOD_L02	equ	rcode_base+00000000h
		extern	?S_DIVMOD_L02
		defb	0F5h,0E5h,0CDh
		defw	LWRD ?S_DIVMOD_L02
		defb	0EBh,0E1h,0F1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
