; BFUSRSHASG.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?BF_US_RSHASG_L10
		rseg	RCODE
rcode_base:
		public	?BF_US_RSHASG_L10
?BF_US_RSHASG_L10	equ	rcode_base+00000000h
		extern	?US_RSH_L02
		extern	?BF_MASKED_LD_L10
		extern	?BF_S_RET_VAL_L10
		defb	0EBh,0E3h,0DDh,0E5h,0D5h,0E5h,0DDh
		defb	0E1h,023h,023h,023h,0E3h,0E5h,0F5h
		defb	0C5h,0CDh
		defw	LWRD ?BF_MASKED_LD_L10
		defb	0CDh
		defw	LWRD ?US_RSH_L02
		defb	0C3h
		defw	LWRD ?BF_S_RET_VAL_L10
		endmod

; -----------------------------------------------------------------------------

	end
