; strncmp.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	strncmp
		rseg	CODE
code_base:
		public	strncmp
strncmp		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_PARM_DIRECT_L09
		defb	0FDh,0E5h,0DDh,0E5h,0C5h,0DDh,0E1h
		defb	0D5h,0FDh,0E1h,021h,008h,000h,039h
		defb	07Eh,023h,0B6h,028h,01Ch,0FDh,07Eh
		defb	000h,0DDh,0BEh,000h,0DDh,023h,020h
		defb	012h,0FDh,07Eh,000h,0B7h,0FDh,023h
		defb	028h,01Dh,02Bh,04Eh,023h,046h,00Bh
		defb	070h,02Bh,071h,018h,0DBh,02Bh,07Eh
		defb	023h,0B6h,028h,00Dh
		defb	0DDh,04Eh,0FFh,006h,000h,060h,0FDh
		defb	06Eh,000h,0EDh,042h,018h,002h,06Fh
		defb	067h,0DDh,0E1h,0FDh,0E1h,0C3h
		defw	LWRD ?BANK_FAST_LEAVE_L08
		endmod

; -----------------------------------------------------------------------------

	end
