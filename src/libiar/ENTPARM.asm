; ENTPARM.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?ENT_PARM_L09
		public	?ENT_PARM_L09
?ENT_PARM_L09	equ	00000008h
		extern	?ENT_PARM_DIRECT_L09
    t_org_abs            S86=0000 00000000
    t_org_abs            S86=0000 00000008
		defb	0C3h
		defw	LWRD ?ENT_PARM_DIRECT_L09
		endmod

; -----------------------------------------------------------------------------

	end
