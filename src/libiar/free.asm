; free.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	free
		rseg	CODE
code_base:
		extern	_heap_of_memory
		extern	_last_heap_object
		public	free
free		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0FAh,0FFh,0FDh,0E5h,02Ah
		defw	LWRD _heap_of_memory
		defb	0DDh,075h,0FCh,0DDh,074h,0FDh,0EDh
		defb	05Bh
		defw	LWRD _heap_of_memory
		defb	0DDh,07Eh,002h,0DDh,0B6h,003h,0CAh
		defw	LWRD code_base+000E0h
		defb	0EDh,04Bh
		defw	LWRD _last_heap_object
		defb	06Bh,062h,0A7h,0EDh,042h,0D2h
		defw	LWRD code_base+000E0h
		defb	06Bh,062h,023h,023h,023h,0DDh,04Eh
		defb	002h,0DDh,046h,003h,0A7h,0EDh,042h
		defb	0C2h
		defw	LWRD code_base+000D0h
		defb	0EBh,0E5h,0FDh,0E1h,0AFh,0B6h,0CAh
		defw	LWRD code_base+000E0h
		defb	0DDh,06Eh,0FCh,0DDh,066h,0FDh,0DDh
		defb	075h,0FEh,0DDh,074h,0FFh,0FDh,06Eh
		defb	001h,0FDh,066h,002h,0EDh,04Bh
		defw	LWRD _last_heap_object
		defb	0EDh,042h,030h,008h,0FDh,06Eh,001h
		defb	0FDh,066h,002h,018h,003h,0FDh,0E5h
		defb	0E1h,0DDh,075h,0FAh,0DDh,074h,0FBh
		defb	0DDh,06Eh,0FCh,0DDh,066h,0FDh,0AFh
		defb	0B6h,020h,02Ch,0DDh,06Eh,0FEh,0DDh
		defb	066h,0FFh,023h,0E5h,0DDh,06Eh,0FAh
		defb	0DDh,066h,0FBh,0B6h,028h,008h,0FDh
		defb	05Eh,001h,0FDh,056h
		defb	002h,018h,004h,023h,05Eh,023h,056h
		defb	0E1h,073h,023h,072h,0DDh,06Eh,0FEh
		defb	0DDh,066h,0FFh,0E5h,0FDh,0E1h,018h
		defb	018h,0DDh,06Eh,0FAh,0DDh,066h,0FBh
		defb	0AFh,0B6h,020h,00Ah,023h,046h,0FDh
		defb	070h,001h,023h,066h,0FDh,074h,002h
		defb	0FDh,036h,000h,000h,0FDh,06Eh,001h
		defb	0FDh,066h,002h,0EDh
		defb	04Bh
		defw	LWRD _last_heap_object
		defb	0EDh,042h,020h,016h,0FDh,022h
		defw	LWRD _last_heap_object
		defb	018h,010h,0DDh,073h,0FCh,0DDh,072h
		defb	0FDh,0EBh,023h,046h,023h,066h,068h
		defb	0EBh,0C3h
		defw	LWRD code_base+0001Dh
		defb	0FDh,0E1h,0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
