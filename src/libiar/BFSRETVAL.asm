; BFSRETVAL.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?BF_S_RET_VAL_L10
		rseg	RCODE
rcode_base:
		public	?BF_S_RET_VAL_L10
?BF_S_RET_VAL_L10	equ	rcode_base+0000000Ch
		public	?BF_S_RET_VAL2_L10
?BF_S_RET_VAL2_L10	equ	rcode_base+00000000h
		extern	?BF_MASKED_ST_L10
		extern	?BF_SS_EXT_L10
		extern	?SS_RSH_L02
		extern	?US_RSH_L02
		defb	0CDh
		defw	LWRD ?BF_MASKED_ST_L10
		defb	0D1h,07Bh,0A1h,05Fh,07Ah,0A0h,057h
		defb	018h,003h,0CDh
		defw	LWRD ?BF_MASKED_ST_L10
		defb	0DDh,04Eh,000h,0CBh,079h,028h,01Dh
		defb	079h,0E6h,00Fh,047h,0CBh,071h,028h
		defb	00Ch,0CDh
		defw	LWRD ?BF_SS_EXT_L10
		defb	0CDh
		defw	LWRD ?SS_RSH_L02
		defb	042h,04Bh,0E1h,0C3h
 defw rcode_base+34h ;    t_push_rel           (0000) 00000034
		defb	0CDh
		defw	LWRD ?US_RSH_L02
		defb	042h,04Bh,0E1h,0C3h
 defw rcode_base+34h ;    t_push_rel           (0000) 00000034
		defb	0C1h,0F1h,0E1h,0D1h,0DDh,0E1h,0EBh
		defb	0E3h,0EBh,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
