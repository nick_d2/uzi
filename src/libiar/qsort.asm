; qsort.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	qsort
		rseg	CODE
code_base:
		rseg	IDATA0
idata0_base:
		rseg	CDATA0
cdata0_base:
    t_symbol_def         PUBLIC_REL (000B) 00000000 '_qbuf'
		public	qsort
qsort		equ	cdata0_base+00000281h
		extern	?CL64180B_4_06_L00
		extern	?S_MUL_L02
		extern	?SS_CMP_L02
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_AUTO_DIRECT_L09
		extern	?LEAVE_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0FAh,0FFh,0FDh,0E5h,0DDh,06Eh,00Ah
		defb	0DDh,066h,00Bh,0E5h,0FDh,0E1h,0DDh
		defb	04Eh,008h,0DDh,046h,009h,0DDh,06Eh
		defb	004h,0DDh,066h,005h,0CDh
		defw	LWRD ?SS_CMP_L02
		defb	0D2h
		defw	LWRD cdata0_base+001FDh
		defb	069h,060h,0DDh,04Eh,004h,0DDh,046h
		defb	005h,009h,0EBh,0CBh,02Ah,0CBh,01Bh
		defb	0FDh,0E5h,0C1h,0CDh
		defw	LWRD cdata0_base+00276h
		defb	0DDh,075h,0FCh,0DDh,074h,0FDh,0DDh
		defb	04Eh,004h,0DDh,046h,005h,0DDh,071h
		defb	0FAh,0DDh,070h,0FBh,0DDh,04Eh,008h
		defb	0DDh,046h,009h,0DDh,071h,0FEh,0DDh
		defb	070h,0FFh,0EDh,04Bh
    t_rel_p16            (0B) 0000
		defb	0A7h,0EDh,042h,028h,013h,0FDh,0E5h
		defb	0C1h,0EDh,05Bh
    t_rel_p16            (0B) 0000
		defb	0DDh,06Eh,0FCh,0DDh,066h,0FDh,079h
		defb	0B0h,028h,002h,0EDh,0B0h,0DDh,04Eh
		defb	0FAh,0DDh,046h,0FBh,0CDh
		defw	LWRD cdata0_base+00234h
		defb	028h,00Eh,0FDh,0E5h,0C5h,0CDh
		defw	LWRD cdata0_base+0026Dh
		defb	0D1h,0C1h,079h,0B0h,028h,002h,0EDh
		defb	0B0h,0CDh
		defw	LWRD cdata0_base+0026Dh
		defb	0EDh,04Bh
    t_rel_p16            (0B) 0000
		defb	0A7h,0EDh,042h,028h,012h,0FDh,0E5h
		defb	0C1h,0FDh,0E5h,0CDh
		defw	LWRD cdata0_base+0025Bh
		defb	02Ah
    t_rel_p16            (0B) 0000
		defb	0C1h,079h,0B0h,028h,002h,0EDh,0B0h
		defb	02Ah
    t_rel_p16            (0B) 0000
		defb	0DDh,075h,0FCh,0DDh,074h,0FDh,0CDh
		defw	LWRD cdata0_base+00202h
		defb	0D2h
		defw	LWRD cdata0_base+0015Eh
		defb	0DDh,04Eh,0FCh,0DDh,046h,0FDh,0C5h
		defb	0DDh,05Eh,0FEh,0DDh,056h,0FFh,0CDh
		defw	LWRD cdata0_base+0021Ch
		defb	0C1h,0CDh
		defw	LWRD cdata0_base+00211h
		defb	030h,00Fh,0DDh,06Eh,0FEh,0DDh,066h
		defb	0FFh,02Bh,0DDh,075h,0FEh,0DDh,074h
		defb	0FFh,018h,0DBh,0CDh
		defw	LWRD cdata0_base+0026Dh
		defb	0E5h,0DDh,04Eh,0FEh,0DDh,046h,0FFh
		defb	0CDh
		defw	LWRD cdata0_base+0024Bh
		defb	0E1h,0A7h,0EDh,042h,028h,01Ah,0FDh
		defb	0E5h,0C1h,0FDh,0E5h,0CDh
		defw	LWRD cdata0_base+0025Bh
		defb	0D5h,0DDh,04Eh,0FEh,0DDh,046h,0FFh
		defb	0CDh
		defw	LWRD cdata0_base+00273h
		defb	0D1h,0C1h,079h,0B0h,028h,002h,0EDh
		defb	0B0h,0CDh
		defw	LWRD cdata0_base+00202h
		defb	030h,01Bh,0DDh,04Eh,0FCh,0DDh,046h
		defb	0FDh,0C5h,0EBh,0CDh
		defw	LWRD cdata0_base+0021Ch
		defb	0C1h,0CDh
		defw	LWRD cdata0_base+00211h
		defb	038h,00Ah,0DDh,034h,0FAh,020h,0E5h
		defb	0DDh,034h,0FBh,018h,0E0h,0DDh,04Eh
		defb	0FEh,0DDh,046h,0FFh,0CDh
		defw	LWRD cdata0_base+00273h
		defb	0E5h,0DDh,04Eh,0FAh,0DDh,046h,0FBh
		defb	0CDh
		defw	LWRD cdata0_base+0024Bh
		defb	0E1h,0A7h,0EDh,042h,028h,01Ah,0FDh
		defb	0E5h,0C1h,0FDh,0E5h,0DDh,05Eh,0FEh
		defb	0DDh,056h,0FFh,0CDh
		defw	LWRD cdata0_base+00261h
		defb	0D5h,0CDh
		defw	LWRD cdata0_base+0026Dh
		defb	0D1h,0C1h,079h,0B0h,028h,002h,0EDh
		defb	0B0h,0C3h
		defw	LWRD cdata0_base+000B0h
		defb	04Dh,044h,0CDh
		defw	LWRD cdata0_base+00234h
		defb	028h,015h,0FDh,0E5h,0C1h,0FDh,0E5h
		defb	0CDh
		defw	LWRD cdata0_base+0025Bh
		defb	0DDh,06Eh,0FCh,0DDh,066h,0FDh,0C1h
		defb	079h,0B0h,028h,002h,0EDh,0B0h,0DDh
		defb	04Eh,0FAh,0DDh,046h,0FBh,0DDh,06Eh
		defb	008h,0DDh,066h,009h,0A7h,0EDh,042h
		defb	04Dh,044h,0DDh,05Eh,004h,0DDh,056h
		defb	005h,0DDh,06Eh,0FAh,0DDh,066h,0FBh
		defb	0A7h,0EDh,052h,0CDh
		defw	LWRD ?SS_CMP_L02
		defb	0DDh,06Eh,00Eh,0E5h,0DDh,06Eh,00Ch
		defb	0DDh,066h,00Dh,0E5h,0FDh,0E5h,030h
		defb	026h,0DDh,06Eh,0FAh,0DDh,066h,0FBh
		defb	02Bh,0E5h,04Bh,042h,0DDh,05Eh,002h
		defb	0DDh,056h,003h,0CDh
		defw	LWRD cdata0_base+00000h
		defb	0E1h,0E1h,0E1h,0E1h,0DDh,06Eh,0FAh
		defb	0DDh,066h,0FBh,023h,0DDh,075h,004h
		defb	0DDh,074h,005h,018h,028h,0DDh,06Eh
		defb	008h,0DDh,066h,009h,0E5h,0DDh,04Eh
		defb	0FAh,0DDh,046h,0FBh,003h,0DDh,05Eh
		defb	002h,0DDh,056h,003h,0CDh
		defw	LWRD cdata0_base+00000h
		defb	0E1h,0E1h,0E1h,0E1h,0DDh,06Eh,0FAh
		defb	0DDh,066h,0FBh,02Bh,0DDh,075h,008h
		defb	0DDh,074h,009h,0C3h
		defw	LWRD cdata0_base+00010h
		defb	0FDh,0E1h,0C3h
		defw	LWRD ?LEAVE_DIRECT_L09
		defb	0DDh,04Eh,0FEh,0DDh,046h,0FFh,0DDh
		defb	06Eh,0FAh,0DDh,066h,0FBh,0C3h
		defw	LWRD ?SS_CMP_L02
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	04Dh,044h,021h,000h,000h,0C3h
		defw	LWRD ?SS_CMP_L02
		defb	0FDh,0E5h,0C1h,0CDh
		defw	LWRD ?S_MUL_L02
		defb	0DDh,06Eh,002h,0DDh,066h,003h,019h
		defb	0EBh,0DDh,07Eh,00Eh,0DDh,06Eh,00Ch
		defb	0DDh,066h,00Dh,0C9h,0FDh,0E5h,0D1h
		defb	0CDh
		defw	LWRD ?S_MUL_L02
		defb	0DDh,06Eh,002h,0DDh,066h,003h,019h
		defb	0DDh,04Eh,0FCh,0DDh,046h,0FDh,0A7h
		defb	0EDh,042h,0C9h,0FDh,0E5h,0D1h,0CDh
		defw	LWRD ?S_MUL_L02
		defb	0DDh,06Eh,002h,0DDh,066h,003h,019h
		defb	04Dh,044h,0C9h,0DDh,05Eh,0FAh,0DDh
		defb	056h,0FBh,0CDh
		defw	LWRD ?S_MUL_L02
		defb	0DDh,06Eh,002h,0DDh,066h,003h,019h
		defb	0EBh,0C9h,0DDh,04Eh,0FAh,0DDh,046h
		defb	0FBh,0FDh,0E5h,0D1h,0CDh
		defw	LWRD ?S_MUL_L02
		defb	0DDh,06Eh,002h,0DDh,066h,003h,019h
		defb	0C9h,0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	080h,0FFh,0DDh,05Eh,00Ah,0DDh,056h
		defb	00Bh,021h,080h,000h,0A7h,0EDh,052h
		defb	038h,02Ah,021h,000h,000h,039h,022h
    t_rel_p16            (0B) 0000
		defb	0DDh,06Eh,00Eh,0E5h,0DDh,06Eh,00Ch
		defb	0DDh,066h,00Dh,0E5h,06Bh,062h,0D5h
		defb	021h,0FFh,0FFh,009h,0E5h,001h,000h
		defb	000h,0DDh,05Eh,002h,0DDh,056h,003h
		defb	0CDh
		defw	LWRD cdata0_base+00000h
		defb	0E1h,0E1h,0E1h,0E1h,0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
    t_org_rel            (0B) 00000000
    t_org_rel            (0B) 00000002
    t_org_rel            (0C) 00000000
		defb	000h,000h
		endmod

; -----------------------------------------------------------------------------

	end
