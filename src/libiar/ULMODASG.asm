; ULMODASG.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?UL_MODASG_L03
		rseg	RCODE
rcode_base:
		public	?UL_MODASG_L03
?UL_MODASG_L03	equ	rcode_base+00000000h
		extern	?UL_MOD_L03
		extern	?L_END_MULDIVASG_L03
		defb	0C5h,0D5h,05Eh,023h,056h,023h,04Eh
		defb	023h,046h,0EBh,0CDh
		defw	LWRD ?UL_MOD_L03
		defb	0C3h
		defw	LWRD ?L_END_MULDIVASG_L03
		endmod

; -----------------------------------------------------------------------------

	end
