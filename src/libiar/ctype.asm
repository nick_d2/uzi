; ctype.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	ctype
		rseg	CODE
code_base:
		rseg	CONST
const_base:
    t_symbol_def         PUBLIC_REL (0004) 00000000 '_Small_Ctype'
		extern	?CL64180B_4_06_L00
    t_org_rel            (04) 00000000
		defb	000h,020h,020h,020h,020h,020h,020h
		defb	020h,020h,020h,028h,028h,028h,028h
		defb	028h,020h,020h,020h,020h,020h,020h
		defb	020h,020h,020h,020h,020h,020h,020h
		defb	020h,020h,020h,020h,020h,088h,010h
		defb	010h,010h,010h,010h,010h,010h,010h
		defb	010h,010h,010h,010h,010h,010h,010h
		defb	004h,004h,004h,004h
		defb	004h,004h,004h,004h,004h,004h,010h
		defb	010h,010h,010h,010h,010h,010h,041h
		defb	041h,041h,041h,041h,041h,001h,001h
		defb	001h,001h,001h,001h,001h,001h,001h
		defb	001h,001h,001h,001h,001h,001h,001h
		defb	001h,001h,001h,001h,010h,010h,010h
		defb	010h,010h,010h,042h,042h,042h,042h
		defb	042h,042h,002h,002h
		defb	002h,002h,002h,002h,002h,002h,002h
		defb	002h,002h,002h,002h,002h,002h,002h
		defb	002h,002h,002h,002h,010h,010h,010h
		defb	010h,020h
		endmod

; -----------------------------------------------------------------------------

	end
