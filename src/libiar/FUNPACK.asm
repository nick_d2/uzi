; FUNPACK.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?F_UNPACK_L04
		rseg	RCODE
rcode_base:
		public	?F_UNPACK_L04
?F_UNPACK_L04	equ	rcode_base+00000000h
		defb	0AFh,067h,0CBh,021h,0CBh,010h,017h
		defb	037h,0CBh,019h,068h,041h,04Ah,053h
		defb	01Eh,000h,0C5h,0D5h,0DDh,05Eh,002h
		defb	0DDh,056h,003h,0DDh,04Eh,004h,0DDh
		defb	046h,005h,0CBh,021h,0CBh,010h,0CBh
		defb	014h,037h,0CBh,019h,0DDh,071h,005h
		defb	0DDh,072h,004h,0DDh,073h,003h,0DDh
		defb	036h,002h,000h,04Fh
		defb	0ACh,0CBh,019h,017h,0CBh,01Ch,017h
		defb	060h,0D1h,0C1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
