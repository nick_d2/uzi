; SLSHASGDEprim.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?S_LSHASG_DE_prim_L12
		rseg	RCODE
rcode_base:
		public	?S_LSHASG_DE_prim_L12
?S_LSHASG_DE_prim_L12	equ	rcode_base+00000000h
		defb	0D9h,0B7h,028h,006h,0EBh,029h,03Dh
		defb	020h,0FCh,0EBh,0D5h,0D9h,0E1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
