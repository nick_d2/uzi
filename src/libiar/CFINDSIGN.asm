; CFINDSIGN.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?C_FIND_SIGN_L01
		rseg	RCODE
rcode_base:
		public	?C_FIND_SIGN_L01
?C_FIND_SIGN_L01	equ	rcode_base+00000000h
		defb	0CBh,078h,028h,005h,0AFh,090h,047h
		defb	03Eh,001h,0CBh,079h,0C8h,0F5h,0AFh
		defb	091h,04Fh,0F1h,0EEh,001h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
