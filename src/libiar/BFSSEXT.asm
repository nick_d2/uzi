; BFSSEXT.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?BF_SS_EXT_L10
		rseg	RCODE
rcode_base:
		public	?BF_SS_EXT_L10
?BF_SS_EXT_L10	equ	rcode_base+00000000h
		defb	0E5h,0C5h,0DDh,06Eh,001h,0DDh,066h
		defb	002h,006h,080h,00Eh,000h,07Ch,0A0h
		defb	020h,00Ah,07Dh,0A1h,020h,006h,0CBh
		defb	028h,0CBh,019h,018h,0F2h,07Ah,0A4h
		defb	0A0h,020h,008h,07Bh,0A5h,0A1h,028h
		defb	006h,07Bh,0B1h,05Fh,07Ah,0B0h,057h
		defb	0C1h,0E1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
