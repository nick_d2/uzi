; memsetlc.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	memsetlc
		rseg	CODE
code_base:
		public	memset
memset		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_PARM_DIRECT_L09
		extern	?MEMSET_L11
		defb	0CDh
		defw	LWRD ?ENT_PARM_DIRECT_L09
		defb	0DDh,04Eh,00Ah,0DDh,046h,00Bh,0DDh
		defb	06Eh,004h,0CDh
		defw	LWRD ?MEMSET_L11
		defb	0EBh,0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
