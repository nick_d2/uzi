; FADDSUB.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?F_ADD_SUB_L04
		rseg	RCODE
rcode_base:
		public	?F_SUB_L04
?F_SUB_L04	equ	rcode_base+00000000h
		public	?F_ADD_L04
?F_ADD_L04	equ	rcode_base+00000014h
		extern	?F_OVERFLOW_L04
		extern	?F_UNDERFLOW_L04
		extern	?F_ROUND_L04
		extern	?F_NO_PACK_L04
		extern	?F_PACK_L04
		extern	?F_UNPACK_L04
		defb	0EBh,0E3h,0DDh,0E5h,0DDh,021h,002h
		defb	000h,0DDh,039h,0E5h,0F5h,0DDh,07Eh
		defb	005h,0EEh,080h,06Fh,018h,00Fh,0EBh
		defb	0E3h,0DDh,0E5h,0DDh,021h,002h,000h
		defb	0DDh,039h,0E5h,0F5h,0DDh,06Eh,005h
		defb	078h,0E6h,07Fh,0B1h,020h,015h,045h
		defb	0DDh,04Eh,004h,0DDh,056h,003h,0DDh
		defb	05Eh,002h,07Dh,0E6h
		defb	07Fh,06Fh,0B1h,020h,001h,045h,0C3h
		defw	LWRD ?F_NO_PACK_L04
		defb	07Dh,0E6h,07Fh,0DDh,0B6h,004h,0CAh
		defw	LWRD ?F_NO_PACK_L04
		defb	0C5h,0D5h,078h,0E6h,07Fh,067h,0DDh
		defb	075h,005h,07Dh,0E6h,07Fh,047h,069h
		defb	0DDh,04Eh,004h,0EDh,042h,020h,009h
		defb	0EBh,0DDh,046h,003h,0DDh,04Eh,002h
		defb	0EDh,042h,0D1h,038h,01Dh,0E1h,0DDh
		defb	07Eh,003h,0DDh,072h,003h,057h,0DDh
		defb	07Eh,002h,0DDh,073h,002h,05Fh,0DDh
		defb	046h,005h,0DDh,074h
		defb	005h,0DDh,04Eh,004h,0DDh,075h,004h
		defb	018h,001h,0C1h,0CDh
		defw	LWRD ?F_UNPACK_L04
		defb	0F5h,07Ch,095h,0FEh,01Ah,038h,00Fh
		defb	0DDh,046h,005h,0DDh,04Eh,004h,0DDh
		defb	056h,003h,0DDh,05Eh,002h,0C3h
		defw	LWRD ?F_PACK_L04
		defb	0FEh,008h,038h,010h,0D6h,008h,06Fh
		defb	0AFh,0BBh,05Ah,028h,002h,0CBh,0C3h
		defb	051h,048h,047h,07Dh,018h,0ECh,0B7h
		defb	028h,00Fh,0CBh,038h,0CBh,019h,0CBh
		defb	01Ah,0CBh,01Bh,030h,002h,0CBh,0C3h
		defb	03Dh,020h,0F1h,0DDh,0CBh,0F9h,056h
		defb	020h,029h,0DDh,07Eh,002h,083h,05Fh
		defb	0DDh,07Eh,003h,08Ah
		defb	057h,0DDh,07Eh,004h,089h,04Fh,0DDh
		defb	07Eh,005h,088h,047h,030h,010h,0CBh
		defb	018h,0CBh,019h,0CBh,01Ah,0CBh,01Bh
		defb	030h,002h,0CBh,0C3h,024h,0CAh
		defw	LWRD ?F_OVERFLOW_L04
		defb	0C3h
		defw	LWRD ?F_ROUND_L04
		defb	0DDh,07Eh,002h,093h,05Fh,0DDh,07Eh
		defb	003h,09Ah,057h,0DDh,07Eh,004h,099h
		defb	04Fh,0DDh,07Eh,005h,098h,047h,0B1h
		defb	0B2h,0B3h,0CAh
		defw	LWRD ?F_UNDERFLOW_L04
		defb	0CBh,078h,020h,00Eh,025h,0CAh
		defw	LWRD ?F_UNDERFLOW_L04
		defb	0CBh,023h,0CBh,012h,0CBh,011h,0CBh
		defb	010h,018h,0EEh,0C3h
		defw	LWRD ?F_ROUND_L04
		endmod

; -----------------------------------------------------------------------------

	end
