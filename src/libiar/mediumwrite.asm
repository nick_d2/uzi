; mediumwrite.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	_medium_write
		rseg	CODE
code_base:
		rseg	CSTR
cstr_base:
		public	_medium_write
_medium_write	equ	cstr_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?SS_CMP_L02
		extern	?UL_MOD_L03
		extern	?UL_DIVASG_L03
		extern	?L_NEGASG_L03
		extern	?C_V_SWITCH_L06
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	05Ch,0FFh,0FDh,0E5h,0DDh,06Eh,002h
		defb	0DDh,066h,003h,0E5h,0FDh,0E1h,021h
		defb	004h,000h,039h,0AFh,077h,023h,077h
		defb	0FDh,046h,000h,078h,0FEh,025h,0FDh
		defb	023h,028h,022h,0AFh,0B0h,020h,00Dh
		defb	021h,004h,000h,039h,046h,023h,066h
		defb	068h,0FDh,0E1h,0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		defb	0DDh,04Eh,00Eh,0DDh,046h,00Fh,05Fh
		defb	0CDh
		defw	LWRD cstr_base+00701h
		defb	039h,034h,023h,020h,0D6h,018h,019h
		defb	0FDh,07Eh,000h,0FEh,025h,020h,015h
		defb	0DDh,04Eh,00Eh,0DDh,046h,00Fh,01Eh
		defb	025h,0CDh
		defw	LWRD cstr_base+00701h
		defb	039h,034h,023h,0FDh,023h,020h,0BBh
		defb	034h,018h,0B8h,021h,002h,000h,039h
		defb	0E5h,021h,022h,000h,039h,04Dh,044h
		defb	0E1h,071h,023h,070h,021h,00Ch,000h
		defb	039h,071h,023h,070h,021h,016h,000h
		defb	039h,036h
    t_push_rel           (0001) 00000000
    t_pop_8             
		defb	023h,036h
    t_rel_fi_8           (01) 0000
		defb	021h,01Dh,000h,039h,036h,000h,023h
		defb	036h,000h,021h,01Ah,000h,039h,036h
		defb	000h,02Bh,02Bh,036h,000h,0FDh,05Eh
		defb	000h,0CDh
		defw	LWRD ?C_V_SWITCH_L06
		defb	005h,000h,020h,023h,02Bh,02Dh,030h
		defw	LWRD cstr_base+000D4h
		defw	LWRD cstr_base+000CBh
		defw	LWRD cstr_base+000BDh
		defw	LWRD cstr_base+000B3h
		defw	LWRD cstr_base+000C4h
		defw	LWRD cstr_base+000ABh
		defb	021h,018h,000h,039h,0AFh,0B6h,020h
		defb	01Dh,0FDh,046h,000h,021h,018h,000h
		defb	039h,070h,018h,013h,021h,01Ah,000h
		defb	039h,034h,018h,00Ch,021h,01Eh,000h
		defb	039h,034h,018h,005h,021h,01Dh,000h
		defb	039h,034h,0FDh,023h,018h,0BEh,0FDh
		defb	07Eh,000h,0FEh,02Ah,021h,010h,000h
		defb	039h,020h,033h,0E5h
		defb	0DDh,06Eh,010h,0DDh,066h,011h,04Eh
		defb	059h,023h,046h,050h,013h,013h,072h
		defb	02Bh,073h,069h,060h,04Eh,023h,046h
		defb	0E1h,071h,023h,070h,0CBh,078h,028h
		defb	011h,021h,010h,000h,039h,0AFh,091h
		defb	077h,023h,03Eh,000h,09Eh,077h,021h
		defb	01Ah,000h,039h,034h,0FDh,023h,018h
		defb	03Dh,0AFh,077h,023h
		defb	077h,0FDh,07Eh,000h,0FEh,030h,038h
		defb	032h,03Eh,039h,0FDh,0BEh,000h,038h
		defb	02Bh,021h,010h,000h,039h,0E5h,0FDh
		defb	04Eh,000h,006h,000h,021h,0D0h,0FFh
		defb	009h,0E5h,021h,014h,000h,039h,046h
		defb	023h,066h,068h,029h,04Dh,044h,029h
		defb	029h,009h,04Dh,044h,0E1h,009h,04Dh
		defb	044h,0E1h,071h,023h
		defb	070h,0FDh,023h,018h,0C7h,021h,01Ah
		defb	000h,039h,0AFh,0B6h,028h,005h,023h
		defb	023h,023h,036h,000h,0FDh,07Eh,000h
		defb	0FEh,02Eh,020h,068h,0FDh,07Eh,001h
		defb	0FEh,02Ah,021h,006h,000h,039h,0FDh
		defb	023h,020h,01Eh,0E5h,0DDh,06Eh,010h
		defb	0DDh,066h,011h,04Eh,059h,023h,046h
		defb	050h,013h,013h,072h
		defb	02Bh,073h,069h,060h,04Eh,023h,046h
		defb	0E1h,071h,023h,070h,0FDh,023h,018h
		defb	046h,0AFh,077h,023h,077h,0FDh,07Eh
		defb	000h,0FEh,030h,038h,03Bh,03Eh,039h
		defb	0FDh,0BEh,000h,038h,034h,021h,006h
		defb	000h,039h,0E5h,0FDh,04Eh,000h,006h
		defb	000h
		defb	021h,0D0h,0FFh,009h,0E5h,021h,00Ah
		defb	000h,039h,046h,023h,066h,068h,029h
		defb	04Dh,044h,029h,029h,009h,04Dh,044h
		defb	0E1h,009h,04Dh,044h,0E1h,071h,023h
		defb	070h,0FDh,023h,018h,0C7h,021h,006h
		defb	000h,039h,036h,0FFh,023h,036h,0FFh
		defb	021h,01Bh,000h,039h,036h,000h,023h
		defb	036h,000h,0FDh,07Eh
                         00FE4C2804FE6C2003341806FE6820042B34FD23FD4600211900397058FD23CD
		defw	LWRD ?C_V_SWITCH_L06
		defb	010h,000h,000h,045h,047h,058h,063h
		defb	064h,065h,066h,067h,069h,06Eh,06Fh
		defb	070h,073h,075h,078h
		defw	LWRD cstr_base+005F0h
		defw	LWRD cstr_base+0036Dh
		defw	LWRD cstr_base+00376h
		defw	LWRD cstr_base+002BAh
		defw	LWRD cstr_base+00376h
		defw	LWRD cstr_base+00356h
		defw	LWRD cstr_base+00235h
		defw	LWRD cstr_base+003FCh
		defw	LWRD cstr_base+005C8h
		defw	LWRD cstr_base+005C8h
		defw	LWRD cstr_base+005C8h
		defw	LWRD cstr_base+003FCh
		defw	LWRD cstr_base+00298h
		defw	LWRD cstr_base+00376h
		defw	LWRD cstr_base+005C8h
		defw	LWRD cstr_base+005C8h
		defw	LWRD cstr_base+005EEh
		defb	0AFh,020h,03Bh,023h,023h,028h,009h
		defb	0B6h,020h,034h,023h,0B6h,028h,030h
		defb	018h,004h,023h,0B6h,028h,02Ah,0DDh
		defb	06Eh,010h,0DDh,066h,011h,04Eh,059h
		defb	023h,046h,050h,013h,013h,072h,02Bh
		defb	073h,069h,060h,07Eh,023h,066h,06Fh
		defb	0E5h,021h,006h,000h,039h,05Eh,023h
		defb	056h,07Ah,007h,09Fh
		defb	04Fh,041h,0E1h,073h,023h,072h,023h
		defb	018h,01Fh,0DDh,06Eh,010h,0DDh,066h
		defb	011h,04Eh,059h,023h,046h,050h,013h
		defb	013h,072h,02Bh,073h,069h,060h,07Eh
		defb	023h,066h,06Fh,0E5h,021h,006h,000h
		defb	039h,04Eh,023h,046h,0E1h,071h,023h
		defb	070h,0C3h
		defw	LWRD cstr_base+00018h
		defb	0DDh,06Eh,010h,0DDh,066h,011h,04Eh
		defb	059h,023h,046h,050h,013h,013h,072h
		defb	02Bh,073h,00Ah,021h,020h,000h,039h
		defb	077h,021h,00Ch,000h,039h,034h,023h
		defb	020h,001h,034h,0C3h
		defw	LWRD cstr_base+0060Ch
		defb	021h,002h,000h,039h,0E5h,0DDh,06Eh
		defb	010h,0DDh,066h,011h,04Eh,059h,023h
		defb	046h,050h,013h,013h,072h,02Bh,073h
		defb	069h,060h,04Eh,023h,046h,0E1h,071h
		defb	023h,070h,079h,0B0h,020h,009h,021h
		defb	002h,000h,039h,036h
    t_push_rel           (0001) 00000011
    t_pop_8             
		defb	023h,036h
    t_rel_fi_8           (01) 0011
		defb	021h,006h,000h,039h,023h,0CBh,07Eh
		defb	028h,006h,02Bh,036h,010h,023h,036h
		defb	027h,021h,00Eh,000h,039h,0AFh,077h
		defb	023h,077h,021h,002h,000h,039h,04Eh
		defb	023h,046h,003h,070h,02Bh,071h,00Bh
		defb	00Ah,0B7h,028h,01Fh,021h,006h,000h
		defb	039h,04Eh,023h,046h,021h,00Eh,000h
		defb	039h,056h,023h,066h
		defb	06Ah,0CDh
		defw	LWRD ?SS_CMP_L02
		defb	030h,00Bh,021h,00Eh,000h,039h,034h
		defb	023h,020h,0D4h,034h,018h,0D1h,021h
		defb	00Ch,000h,039h,0E5h,021h,004h,000h
		defb	039h,04Eh,023h,046h,00Bh,070h,02Bh
		defb	071h,0E1h,071h,023h,070h,021h,002h
		defb	000h,039h,0E5h,021h,010h,000h,039h
		defb	04Eh,023h,046h,0E1h,07Eh,091h,077h
		defb	023h,07Eh,098h,077h
		defb	0C3h
		defw	LWRD cstr_base+0060Ch
		defb	021h,01Eh,000h,039h,0AFh,0B6h,028h
		defb	00Fh,021h,006h,000h,039h,07Eh,023h
		defb	0B6h,020h,006h,02Bh,034h,023h,020h
		defb	001h,034h,021h,016h,000h,039h,036h
    t_push_rel           (0001) 00000020
    t_pop_8             
		defb	023h,036h
    t_rel_fi_8           (01) 0020
		defb	078h,0FEh,070h,028h,003h,0AFh,028h
		defb	00Fh,0DDh,06Eh,010h,0DDh,066h,011h
		defb	04Eh,059h,023h,046h,050h,013h,013h
		defb	018h,025h,021h,01Ch,000h,039h,0B6h
		defb	0DDh,06Eh,010h,0DDh,066h,011h,04Eh
		defb	059h,023h,046h,050h,013h,013h,028h
		defb	011h,013h,013h,072h,02Bh,073h,069h
		defb	060h,05Eh,023h,056h
		defb	023h,04Eh,023h,046h,0EBh,018h,00Bh
		defb	072h,02Bh,073h,00Ah,06Fh,003h,00Ah
		defb	067h,001h,000h,000h,0E5h,021h,00Ah
		defb	000h,039h,0D1h,073h,023h,072h,023h
		defb	071h,023h,070h,021h,019h,000h,039h
		defb	07Eh,0FEh,06Fh,020h,008h,001h,000h
		defb	000h,021h,008h,000h,018h,00Eh,0FEh
		defb	075h,020h,004h,02Eh
		defb	00Ah,018h,002h,02Eh,010h,001h,000h
		defb	000h,061h,0E5h,02Eh,014h,039h,0D1h
		defb	073h,023h,072h,023h,071h,023h,070h
		defb	023h,023h,023h,070h,0C3h
		defw	LWRD cstr_base+00488h
		defb	0AFh,028h,02Fh,0DDh,06Eh,010h,0DDh
		defb	066h,011h,04Eh,059h,023h,046h,050h
		defb	013h,013h,013h,013h,072h,02Bh,073h
		defb	069h,060h,05Eh,023h,056h,023h,04Eh
		defb	023h,046h,0EBh,0E5h,021h,00Ah,000h
		defb	039h,0D1h,073h,023h,072h,023h,071h
		defb	023h,070h,0CBh,078h,028h,04Fh,018h
		defb	041h,023h,023h,023h
		defb	0B6h,0DDh,06Eh,010h,0DDh,066h,011h
		defb	04Eh,059h,023h,046h,050h,013h,013h
		defb	028h,011h,013h,013h,072h,02Bh,073h
		defb	069h,060h,05Eh,023h,056h,023h,04Eh
		defb	023h,046h,0EBh,018h,00Ch,072h,02Bh
		defb	073h,00Ah,06Fh,003h,00Ah,067h,007h
		defb	09Fh,04Fh,041h,0E5h,021h,00Ah,000h
		defb	039h,0D1h,073h,023h
		defb	072h,023h,071h,023h,070h,0CBh,078h
		defb	028h,00Ch,02Bh,02Bh,02Bh,0CDh
		defw	LWRD ?L_NEGASG_L03
		defb	021h,018h,000h,039h,036h,02Dh,021h
		defb	012h,000h,039h,0AFh,036h,00Ah,023h
		defb	077h,023h,077h,023h,077h,021h,002h
		defb	000h,039h,0E5h,021h,022h,000h,039h
		defb	001h,085h,000h,009h,04Dh,044h,0E1h
		defb	071h,023h,070h,021h,00Ch,000h,039h
		defb	071h,023h,070h,021h,008h,000h,039h
		defb	07Eh,023h,0B6h,023h
		defb	0B6h,023h,0B6h,028h,002h,03Eh,001h
		defb	021h,01Fh,000h,039h,077h,021h,006h
		defb	000h,039h,07Eh,023h,0B6h,020h,007h
		defb	021h,01Fh,000h,039h,0B6h,028h,04Eh
		defb	021h,012h,000h,039h,04Eh,023h,046h
		defb	023h,05Eh,023h,056h,0D5h,0C5h,021h
		defb	00Ch,000h,039h,05Eh,023h,056h,023h
		defb	04Eh,023h,046h,0EBh
		defb	0CDh
		defw	LWRD ?UL_MOD_L03
		defb	0E5h,021h,018h,000h,039h,04Eh,023h
		defb	046h,0E1h,009h,046h,021h,002h,000h
		defb	039h,05Eh,023h,056h,01Bh,072h,02Bh
		defb	073h,0EBh,070h,021h,008h,000h,039h
		defb	0E5h,021h,014h,000h,039h,05Eh,023h
		defb	056h,023h,04Eh,023h,046h,0E1h,0CDh
		defw	LWRD ?UL_DIVASG_L03
		defb	07Bh,0B2h,0B1h,0B0h,020h,0B2h,021h
		defb	006h,000h,039h,023h,0CBh,07Eh,028h
		defb	02Dh,021h,01Dh,000h,039h,0AFh,0B6h
		defb	028h,025h,021h,006h,000h,039h,0E5h
		defb	021h,01Ah,000h,039h,0AFh,0B6h,028h
		defb	004h,00Eh,001h,018h,001h,04Fh,006h
		defb	000h,021h,012h,000h,039h,056h,023h
		defb	066h,06Ah,0A7h,0EDh
		defb	042h,04Dh,044h,0E1h,071h,023h,070h
		defb	021h,002h,000h,039h,04Eh,023h,046h
		defb	021h,00Ch,000h,039h,056h,023h,066h
		defb	06Ah,0A7h,0EDh,042h,0E5h,021h,008h
		defb	000h,039h,04Eh,023h,046h,0E1h,0CDh
		defw	LWRD ?SS_CMP_L02
		defb	030h,011h,021h,002h,000h,039h,04Eh
		defb	023h,046h,00Bh,070h,02Bh,071h,069h
		defb	060h,036h,030h,018h,0CFh,021h,01Eh
		defb	000h,039h,0AFh,0B6h,028h,043h,023h
		defb	0AFh,0B6h,028h,03Eh,021h,019h,000h
		defb	039h,07Eh,0FEh,078h,028h,004h,0FEh
		defb	058h,020h,011h,021h,002h,000h,039h
		defb	04Eh,023h,046h,00Bh
		defb	070h,02Bh,071h,0C5h,047h,0E1h,070h
		defb	018h,011h,0FEh,06Fh,020h,062h,021h
		defb	002h,000h,039h,07Eh,023h,066h,06Fh
		defb	07Eh,0FEh,030h,028h,055h,021h,002h
		defb	000h,039h,04Eh,023h,046h,00Bh,070h
		defb	02Bh,071h,069h,060h,036h,030h,018h
		defb	044h,021h,002h,000h,039h,036h
    t_push_rel           (0001) 00000031
    t_pop_8             
		defb	023h,036h
    t_rel_fi_8           (01) 0031
		defb	021h,00Ch,000h,039h,036h
    t_push_rel           (0001) 00000031
    t_pop_8             
		defb	023h,036h
    t_rel_fi_8           (01) 0031
		defb	02Bh,07Eh,023h,066h,06Fh,0AFh,0B6h
		defb	028h,029h,021h,00Ch,000h,039h,034h
		defb	023h,020h,0EFh,034h,018h,0ECh,0FDh
		defb	02Bh,021h,002h,000h,039h,036h
    t_push_rel           (0001) 00000054
    t_pop_8             
		defb	023h,036h
    t_rel_fi_8           (01) 0054
		defb	021h,00Ch,000h,039h,036h
    t_push_rel           (0001) 00000054
    t_pop_8             
		defb	023h,036h
    t_rel_fi_8           (01) 0054
		defb	02Bh,07Eh,0C6h,003h,077h,023h,07Eh
		defb	0CEh,000h,077h,021h,006h,000h,039h
		defb	0E5h,021h,004h,000h,039h,04Eh,023h
		defb	046h,021h,00Eh,000h,039h,056h,023h
		defb	066h,06Ah,0A7h,0EDh,042h,04Dh,044h
		defb	0E1h,071h,023h,070h,021h,010h,000h
		defb	039h,056h,023h,066h,06Ah,0CDh
		defw	LWRD ?SS_CMP_L02
		defb	021h,00Eh,000h,030h,007h,039h,0AFh
		defb	077h,023h,077h,018h,02Dh,039h,0E5h
		defb	021h,01Ah,000h,039h,0AFh,0B6h,028h
		defb	004h,00Eh,001h,018h,001h,04Fh,006h
		defb	000h,021h,012h,000h,039h,023h,066h
		defb	06Ah,0A7h,0EDh,042h,0E5h,021h,00Ah
		defb	000h,039h,04Eh,023h,046h,0E1h,0A7h
		defb	0EDh,042h,04Dh,044h
		defb	0E1h,071h,023h,070h,021h,01Ah,000h
		defb	039h,0AFh,0B6h,020h,01Ah,021h,00Eh
		defb	000h,039h,04Eh,023h,046h,00Bh,070h
		defb	02Bh,071h,0CBh,078h,020h,00Bh,0CDh
		defw	LWRD cstr_base+006F9h
		defb	039h,034h,023h,020h,0E9h,034h,018h
		defb	0E6h,021h,018h,000h,039h,0AFh,0B6h
		defb	028h,010h,0DDh,04Eh,00Eh,0DDh,046h
		defb	00Fh,05Fh,0CDh
		defw	LWRD cstr_base+00701h
		defb	039h,034h,023h,020h,001h,034h,021h
		defb	006h,000h,039h,04Eh,023h,046h,00Bh
		defb	070h,02Bh,071h,0CBh,078h,020h,01Eh
		defb	0DDh,04Eh,00Eh,0DDh,046h,00Fh,021h
		defb	002h,000h,039h,05Eh,023h,056h,013h
		defb	072h,02Bh,073h,01Bh,01Ah,05Fh,0CDh
		defw	LWRD cstr_base+00701h
		defb	039h,034h,023h,020h,0D5h,018h,0D2h
		defb	021h,01Ah,000h,039h,0AFh,0B6h,028h
		defb	01Ah,021h,00Eh,000h,039h,04Eh,023h
		defb	046h,00Bh,070h,02Bh,071h,0CBh,078h
		defb	020h,00Bh,0CDh
		defw	LWRD cstr_base+006F9h
		defb	039h,034h,023h,020h,0E9h,034h,018h
		defb	0E6h,0C3h
		defw	LWRD cstr_base+00018h
		defb	0DDh,04Eh,00Eh,0DDh,046h,00Fh,01Eh
		defb	020h,0DDh,07Eh,00Ch,0DDh,06Eh,00Ah
		defb	0DDh,066h,00Bh,0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	021h,004h,000h,0C9h
    t_org_rel            (01) 00000000
		defb	030h,031h,032h,033h,034h,035h,036h
		defb	037h,038h,039h,041h,042h,043h,044h
		defb	045h,046h,000h,028h,06Eh,075h,06Ch
		defb	06Ch,020h,070h,06Fh,069h,06Eh,074h
		defb	065h,072h,029h,000h,030h,031h,032h
		defb	033h,034h,035h,036h,037h,038h,039h
		defb	061h,062h,063h,064h,065h,066h,000h
		defb	046h,04Ch,04Fh,041h
		defb	054h,053h,03Fh,020h,077h,072h,06Fh
		defb	06Eh,067h,020h,066h,06Fh,072h,06Dh
		defb	061h,074h,074h,065h,072h,020h,069h
		defb	06Eh,073h,074h,061h,06Ch,06Ch,065h
		defb	064h,021h,000h,03Fh,03Fh,03Fh,000h
		endmod

; -----------------------------------------------------------------------------

	end
