; SCRSH.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?SC_RSH_L01
		rseg	RCODE
rcode_base:
		public	?SC_RSH_L01
?SC_RSH_L01	equ	rcode_base+00000000h
		defb	004h,005h,0C8h,0CBh,02Fh,010h,0FCh
		defb	0C9h
		endmod

; -----------------------------------------------------------------------------

	end
