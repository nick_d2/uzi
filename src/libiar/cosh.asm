; cosh.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	cosh
		rseg	CODE
code_base:
		public	cosh
cosh		equ	code_base+00000000h
		extern	errno
		extern	exp
		extern	?CL64180B_4_06_L00
		extern	?SL_CMP_L03
		extern	?F_MUL_L04
		extern	?F_DIV_L04
		extern	?F_ADD_L04
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?BANK_LEAVE_32_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0FCh,0FFh,0DDh,06Eh,002h,0DDh,066h
		defb	003h,0CBh,0B8h,0DDh,070h,005h,0EBh
		defb	03Eh
		defb	BYTE3 exp
		defb	021h
		defw	LWRD exp
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0C5h,0E5h,001h,000h,03Fh,069h,061h
		defb	0CDh
		defw	LWRD ?F_MUL_L04
		defb	0DDh,075h,0FCh,0DDh,074h,0FDh,0DDh
		defb	071h,0FEh,0DDh,070h,0FFh,021h,0FFh
		defb	040h,0E5h,021h,029h,05Ch,0E5h,0DDh
		defb	04Eh,004h,0DDh,046h,005h,0DDh,06Eh
		defb	002h,0DDh,066h,003h,0CDh
		defw	LWRD ?SL_CMP_L03
		defb	030h,02Ah,0DDh,06Eh,0FEh,0DDh,066h
		defb	0FFh,0E5h,0DDh,06Eh,0FCh,0DDh,066h
		defb	0FDh,0E5h,001h,080h,03Eh,021h,000h
		defb	000h,0CDh
		defw	LWRD ?F_DIV_L04
		defb	0C5h,0E5h,0DDh,04Eh,0FEh,0DDh,046h
		defb	0FFh,0DDh,06Eh,0FCh,0DDh,066h,0FDh
		defb	0CDh
		defw	LWRD ?F_ADD_L04
		defb	018h,02Dh,021h,031h,043h,0E5h,021h
		defb	018h,072h,0E5h,0DDh,06Eh,002h,0DDh
		defb	066h,003h,0CDh
		defw	LWRD ?SL_CMP_L03
		defb	030h,00Eh,0DDh,04Eh,0FEh,0DDh,046h
		defb	0FFh,0DDh,06Eh,0FCh,0DDh,066h,0FDh
		defb	018h,00Ch,021h,022h,000h,022h
		defw	LWRD errno
		defb	001h,07Fh,07Fh,021h,0FFh,0FFh,0C3h
		defw	LWRD ?BANK_LEAVE_32_L08
		endmod

; -----------------------------------------------------------------------------

	end
