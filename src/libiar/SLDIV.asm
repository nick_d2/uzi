; SLDIV.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?SL_DIV_L03
		rseg	RCODE
rcode_base:
		public	?SL_DIV_L03
?SL_DIV_L03	equ	rcode_base+00000000h
		extern	?L_FIND_SIGN_L03
		extern	?L_DIVMOD_L03
		extern	?L_NOT_L03
		extern	?L_INC_L03
		defb	0EBh,0E3h,0F5h,0DDh,0E5h,0DDh,021h
		defb	000h,000h,0DDh,039h,0C5h,0D5h,0DDh
		defb	056h,009h,0DDh,074h,009h,0DDh,05Eh
		defb	008h,0DDh,075h,008h,0AFh,0CDh
		defw	LWRD ?L_FIND_SIGN_L03
		defb	0CDh
		defw	LWRD ?L_DIVMOD_L03
		defb	0E1h,0C1h,0B7h,0CCh
		defw	LWRD ?L_NOT_L03
		defb	0C4h
		defw	LWRD ?L_INC_L03
		defb	0DDh,0E1h,0F1h,0D1h,033h,033h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
