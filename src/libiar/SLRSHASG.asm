; SLRSHASG.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?SL_RSHASG_L03
		rseg	RCODE
rcode_base:
		public	?SL_RSHASG_L03
?SL_RSHASG_L03	equ	rcode_base+00000000h
		extern	?SL_RSH_L03
		extern	?L_END_ASG_L03
		defb	05Eh,023h,056h,023h,04Eh,023h,046h
		defb	0EBh,0CDh
		defw	LWRD ?SL_RSH_L03
		defb	0C3h
		defw	LWRD ?L_END_ASG_L03
		endmod

; -----------------------------------------------------------------------------

	end
