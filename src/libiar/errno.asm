; errno.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	errno
		rseg	CODE
code_base:
		rseg	UDATA0
udata0_base:
    t_symbol_def         PUBLIC_REL (000A) 00000000 'errno'
		extern	?CL64180B_4_06_L00
    t_org_rel            (0A) 00000000
    t_org_rel            (0A) 00000002
		endmod

; -----------------------------------------------------------------------------

	end
