; atof.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	atof
		rseg	CODE
code_base:
		extern	_Small_Ctype
		public	atof
atof		equ	code_base+00000000h
		extern	atoi
		extern	?CL64180B_4_06_L00
		extern	?SS_CMP_L02
		extern	?F_MUL_L04
		extern	?F_ADD_L04
		extern	?SL_TO_F_L04
		extern	?F_MULASG_L04
		extern	?F_DIVASG_L04
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_LEAVE_32_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0F4h,0FFh,0FDh,0E5h,0DDh,06Eh,002h
		defb	0DDh,066h,003h,0E5h,0FDh,0E1h,0DDh
		defb	036h,0FEh,000h,0FDh,04Eh,000h,006h
		defb	000h,021h
		defw	LWRD _Small_Ctype 0001
		defb	009h,0CBh,05Eh,028h,004h,0FDh,023h
		defb	018h,0EFh,0FDh,07Eh,000h,0FEh,02Dh
		defb	020h,006h,0DDh,036h,0FEh,001h,018h
		defb	007h,0FDh,07Eh,000h,0FEh,02Bh,020h
		defb	002h,0FDh,023h,0AFh,0DDh,077h,0F4h
		defb	0DDh,077h,0F5h,0DDh,077h,0F6h,0DDh
		defb	077h,0F7h,0FDh,04Eh,000h,006h,000h
		defb	021h
		defw	LWRD _Small_Ctype 0001
		defb	009h,0CBh,056h,028h,023h,0CDh
		defw	LWRD code_base+00173h
		defb	0C5h,0E5h,021h,020h,041h,0E5h,021h
		defb	000h,000h,0E5h,0CDh
		defw	LWRD code_base+00182h
		defb	0CDh
		defw	LWRD ?F_ADD_L04
		defb	0DDh,075h,0F4h,0DDh,074h,0F5h,0DDh
		defb	071h,0F6h,0DDh,070h,0F7h,0FDh,023h
		defb	018h,0D0h,0AFh,0DDh,077h,0FAh,0DDh
		defb	077h,0FBh,0DDh,036h,0FCh,080h,0DDh
		defb	036h,0FDh,03Fh,0FDh,07Eh,000h,0FEh
		defb	02Eh,020h,03Dh,0FDh,04Eh,001h,006h
		defb	000h,021h
		defw	LWRD _Small_Ctype 0001
		defb	009h,0CBh,056h,0FDh,023h,028h,02Eh
		defb	0CDh
		defw	LWRD code_base+00173h
		defb	0C5h,0E5h,021h,020h,041h,0E5h,021h
		defb	000h,000h,0E5h,0CDh
		defw	LWRD code_base+00182h
		defb	0CDh
		defw	LWRD ?F_ADD_L04
		defb	0DDh,075h,0F4h,0DDh,074h,0F5h,0DDh
		defb	071h,0F6h,0DDh,070h,0F7h,021h,008h
		defb	000h,039h,001h,020h,041h,011h,000h
		defb	000h,0CDh
		defw	LWRD ?F_MULASG_L04
		defb	018h,0C3h,0FDh,04Eh,000h,021h
		defw	LWRD _Small_Ctype 0001
		defb	009h,0CBh,046h,028h,008h,0FDh,046h
		defb	000h,0CBh,0E8h,078h,018h,003h,0FDh
		defb	07Eh,000h,0FEh,065h,020h,059h,0FDh
		defb	023h,0FDh,0E5h,0D1h,03Eh
		defb	BYTE3 atoi
		defb	021h
		defw	LWRD atoi
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0DDh,075h,0F8h,0DDh,074h,0F9h,0DDh
		defb	0CBh,0F9h,07Eh,028h,017h,021h,002h
		defb	000h,039h,001h,020h,041h,011h,000h
		defb	000h,0CDh
		defw	LWRD ?F_DIVASG_L04
		defb	0DDh,034h,0F8h,020h,0E8h,0DDh,034h
		defb	0F9h,018h,0E3h,0DDh,04Eh,0F8h,0DDh
		defb	046h,0F9h,021h,000h,000h,0CDh
		defw	LWRD ?SS_CMP_L02
		defb	030h,01Bh,023h,023h,039h,001h,020h
		defb	041h,011h,000h,000h,0CDh
		defw	LWRD ?F_MULASG_L04
		defb	0DDh,06Eh,0F8h,0DDh,066h,0F9h,02Bh
		defb	0DDh,075h,0F8h,0DDh,074h,0F9h,018h
		defb	0D7h,021h,002h,000h,039h,0DDh,04Eh
		defb	0FCh,0DDh,046h,0FDh,0DDh,05Eh,0FAh
		defb	0DDh,056h,0FBh,0CDh
		defw	LWRD ?F_DIVASG_L04
		defb	0AFh,0DDh,0B6h,0FEh,028h,00Bh,0EBh
		defb	079h,0B0h,028h,012h,078h,0EEh,080h
		defb	047h,018h,00Ch,0DDh,04Eh,0F6h,0DDh
		defb	046h,0F7h,0DDh,06Eh,0F4h,0DDh,066h
		defb	0F5h,0FDh,0E1h,0C3h
		defw	LWRD ?BANK_LEAVE_32_L08
		defb	0FDh,04Eh,000h,021h,0D0h,0FFh,009h
		defb	07Ch,007h,09Fh,04Fh,041h,0C3h
		defw	LWRD ?SL_TO_F_L04
		defb	0DDh,04Eh,0F6h,0DDh,046h,0F7h,0DDh
		defb	06Eh,0F4h,0DDh,066h,0F5h,0C3h
		defw	LWRD ?F_MUL_L04
		endmod

; -----------------------------------------------------------------------------

	end
