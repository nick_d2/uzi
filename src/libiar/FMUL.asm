; FMUL.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?F_MUL_L04
		rseg	RCODE
rcode_base:
		public	?F_MUL_L04
?F_MUL_L04	equ	rcode_base+00000000h
		extern	?F_UNPACK_L04
		extern	?F_OVERFLOW_L04
		extern	?F_UNDERFLOW_L04
		extern	?F_ROUND_L04
		defb	0EBh,0E3h,0DDh,0E5h,0DDh,021h,002h
		defb	000h,0DDh,039h,0E5h,0F5h,0CDh
		defw	LWRD ?F_UNPACK_L04
		defb	0CBh,03Fh,0CBh,03Fh,0F5h,0AFh,0BDh
		defb	0CAh
		defw	LWRD ?F_UNDERFLOW_L04
		defb	0B4h,0CAh
		defw	LWRD ?F_UNDERFLOW_L04
		defb	0D6h,07Fh,038h,006h,085h,0DAh
		defw	LWRD ?F_OVERFLOW_L04
		defb	018h,004h,085h,0D2h
		defw	LWRD ?F_UNDERFLOW_L04
		defb	067h,03Eh,080h,0A8h,0B1h,0B2h,020h
		defb	00Bh,0DDh,046h,005h,0DDh,04Eh,004h
		defb	0DDh,056h,003h,018h,029h,03Eh,080h
		defb	0DDh,0AEh,005h,0DDh,0B6h,004h,0DDh
		defb	0B6h,003h,028h,01Ch,07Ch,0C5h,0D5h
		defb	0CDh
 defw rcode_base+6ah ;    t_push_rel           (0000) 0000006A
		defb	0F1h,0F1h,0CBh,03Dh,030h,010h,024h
		defb	0CAh
		defw	LWRD ?F_OVERFLOW_L04
		defb	0CBh,018h,0CBh,019h,0CBh,01Ah,0CBh
		defb	01Bh,030h,002h,0CBh,0C3h,0C3h
		defw	LWRD ?F_ROUND_L04
		defb	0F5h,060h,0DDh,056h,005h,06Ah,0EDh
		defb	06Ch,0E5h,060h,0DDh,06Eh,004h,0EDh
		defb	06Ch,059h,0EDh,05Ch,0AFh,019h,0CEh
		defb	000h,0F5h,0E5h,060h,0DDh,06Eh,003h
		defb	0EDh,06Ch,059h,0DDh,056h,004h,0EDh
		defb	05Ch,0AFh,019h,0CEh,000h,0DDh,046h
		defb	0F5h,050h,0DDh,05Eh,005h,0EDh,05Ch
		defb	019h,0CEh,000h,0F5h
		defb	0E5h,069h,0DDh,066h,003h,0EDh,06Ch
		defb	050h,0DDh,05Eh,004h,0EDh,05Ch,0AFh
		defb	019h,0CEh,000h,0DDh,04Eh,003h,0EDh
		defb	04Ch,0DDh,071h,002h,048h,006h,000h
		defb	009h,0DDh,075h,0F4h,04Ch,0CEh,000h
		defb	047h,0E1h,009h,04Dh,0D1h,07Ah,0CEh
		defb	000h,057h,05Ch,0E1h,019h,045h,0D1h
		defb	07Ah,0CEh,000h,057h
		defb	05Ch,0E1h,019h,0E5h,0C5h,0D1h,0C1h
		defb	0E1h,0CBh,013h,0CBh,012h,0CBh,011h
		defb	0CBh,010h,0CBh,015h,0DDh,07Eh,0F4h
		defb	0DDh,0B6h,002h,0C8h,0CBh,0C3h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
