; SVSWITCH.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?S_V_SWITCH_L06
		rseg	RCODE
rcode_base:
		public	?S_V_SWITCH_L06
?S_V_SWITCH_L06	equ	rcode_base+00000000h
		extern	?V_SWITCH_END_L06
		defb	0E3h,0F5h,0C5h,04Eh,023h,046h,023h
		defb	078h,0B1h,028h,00Dh,00Bh,07Bh,096h
		defb	023h,020h,0F5h,07Ah,096h,020h,0F1h
		defb	023h,023h,023h,009h,009h,0C3h
		defw	LWRD ?V_SWITCH_END_L06
		endmod

; -----------------------------------------------------------------------------

	end
