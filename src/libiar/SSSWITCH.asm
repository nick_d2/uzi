; SSSWITCH.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?S_S_SWITCH_L06
		rseg	RCODE
rcode_base:
		public	?S_S_SWITCH_L06
?S_S_SWITCH_L06	equ	rcode_base+00000000h
		extern	?S_SWITCH_END_L06
		defb	0E3h,0F5h,0D5h,07Bh,096h,023h,05Fh
		defb	07Ah,09Eh,023h,057h,07Bh,096h,023h
		defb	07Ah,09Eh,023h,0C3h
		defw	LWRD ?S_SWITCH_END_L06
		endmod

; -----------------------------------------------------------------------------

	end
