; atoi.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	atoi
		rseg	CODE
code_base:
		extern	_Small_Ctype
		public	atoi
atoi		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0FEh,0FFh,0FDh,0E5h,0DDh,05Eh,002h
		defb	0DDh,056h,003h,0DDh,036h,0FEh,000h
		defb	06Bh,062h,04Eh,006h,000h,021h
		defw	LWRD _Small_Ctype 0001
		defb	009h,0CBh,05Eh,028h,003h,013h,018h
		defb	0F0h,06Bh,062h,07Eh,0FEh,02Dh,020h
		defb	007h,013h,0DDh,036h,0FEh,001h,018h
		defb	006h,07Eh,0FEh,02Bh,020h,001h,013h
		defb	0FDh,021h,000h,000h,06Bh,062h,04Eh
		defb	006h,000h,021h
		defw	LWRD _Small_Ctype 0001
		defb	009h,0CBh,056h,028h,019h,06Bh,062h
		defb	013h,04Eh,0C5h,0FDh,0E5h,0E1h,029h
		defb	04Dh,044h,029h,029h,009h,0C1h,009h
		defb	001h,0D0h,0FFh,009h,0E5h,0FDh,0E1h
		defb	018h,0DAh,0AFh,0DDh,0B6h,0FEh,028h
		defb	00Ah,0FDh,0E5h,0C1h,021h,000h,000h
		defb	0EDh,042h,018h,003h,0FDh,0E5h,0E1h
		defb	0FDh,0E1h,0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
