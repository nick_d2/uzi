; MONITORBANKLEAVE32.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?MONITOR_BANK_LEAVE_32_L09
		rseg	RCODE
rcode_base:
		public	?MONITOR_BANK_LEAVE_32_L09
?MONITOR_BANK_LEAVE_32_L09	equ	rcode_base+00000000h
		extern	?BANK_FAST_LEAVE_L08
		defb	0F1h,0DDh,0F9h,0DDh,0E1h,0D1h,033h
		defb	033h,0E2h
		defw	LWRD ?BANK_FAST_LEAVE_L08
		defb	0FBh,0C3h
		defw	LWRD ?BANK_FAST_LEAVE_L08
		endmod

; -----------------------------------------------------------------------------

	end
