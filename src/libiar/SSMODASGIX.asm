; SSMODASGIX.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?SS_MODASG_IX_L12
		rseg	RCODE
rcode_base:
		public	?SS_MODASG_IX_L12
?SS_MODASG_IX_L12	equ	rcode_base+00000000h
		extern	?SS_MOD_L02
		defb	0D5h,0DDh,0E5h,0D1h,0CDh
		defw	LWRD ?SS_MOD_L02
		defb	0D5h,0EBh,0DDh,0E1h,0D1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
