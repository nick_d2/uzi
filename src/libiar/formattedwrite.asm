; formattedwrite.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	_formatted_write
		rseg	CODE
code_base:
		rseg	CSTR
cstr_base:
		public	_formatted_write
_formatted_write	equ	cstr_base+00000465h
		extern	?CL64180B_4_06_L00
		extern	?SS_MOD_L02
		extern	?SS_CMP_L02
		extern	?SS_DIVASG_L02
		extern	?UL_MOD_L03
		extern	?SL_CMP_L03
		extern	?UL_DIVASG_L03
		extern	?L_NEGASG_L03
		extern	?F_TO_L_L04
		extern	?SL_TO_F_L04
		extern	?F_NEGASG_L04
		extern	?F_MULASG_L04
		extern	?F_DIVASG_L04
		extern	?F_SUBASG_L04
		extern	?C_V_SWITCH_L06
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?ENT_AUTO_DIRECT_L09
		extern	?LEAVE_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0F6h,0FFh,0FDh,0E5h,0DDh,06Eh,00Ah
		defb	0DDh,066h,00Bh,0DDh,075h,0F6h,0DDh
		defb	074h,0F7h,0AFh,0DDh,077h,0F8h,0DDh
		defb	077h,0F9h,021h,080h,03Fh,0E5h,06Fh
		defb	067h,0E5h,0CDh
		defw	LWRD cstr_base+0045Ch
		defb	038h,04Dh,021h,0BAh,051h,0E5h,021h
		defb	0B7h,043h,0E5h,0CDh
		defw	LWRD cstr_base+00456h
		defb	038h,01Ah,021h,00Eh,000h,039h,001h
		defb	015h,050h,011h,0F9h,002h,0CDh
		defw	LWRD ?F_DIVASG_L04
		defb	021h,004h,000h,039h,0C6h,00Ah,077h
		defb	030h,0DDh,023h,034h,018h,0D9h,021h
		defb	020h,041h,0E5h,021h,000h,000h,0E5h
		defb	0CDh
		defw	LWRD cstr_base+00456h
		defb	038h,017h,021h,00Eh,000h,039h,001h
		defb	020h,041h,011h,000h,000h,0CDh
		defw	LWRD ?F_DIVASG_L04
		defb	0DDh,034h,0F8h,020h,0E1h,0DDh,034h
		defb	0F9h,018h,0DCh,018h,062h,079h,0B0h
		defb	028h,05Eh,0DDh,06Eh,004h,0DDh,066h
		defb	005h,0E5h,0DDh,06Eh,002h,0DDh,066h
		defb	003h,0E5h,001h,0DBh,02Eh,021h,0FFh
		defb	0E6h,0CDh
		defw	LWRD ?SL_CMP_L03
		defb	038h,01Ch,021h,00Eh,000h,039h,001h
		defb	015h,050h,011h,0F9h,002h,0CDh
		defw	LWRD ?F_MULASG_L04
		defb	021h,004h,000h,039h,07Eh,0D6h,00Ah
		defb	077h,023h,07Eh,0DEh,000h,077h,018h
		defb	0CBh,021h,080h,03Fh,0E5h,021h,000h
		defb	000h,0E5h,0CDh
		defw	LWRD cstr_base+00456h
		defb	030h,01Ch,021h,00Eh,000h,039h,001h
		defb	020h,041h,011h,000h,000h,0CDh
		defw	LWRD ?F_MULASG_L04
		defb	0DDh,06Eh,0F8h,0DDh,066h,0F9h,02Bh
		defb	0DDh,075h,0F8h,0DDh,074h,0F9h,018h
		defb	0D7h,0AFh,0DDh,0B6h,00Eh,028h,04Ch
		defb	0DDh,04Eh,008h,0DDh,046h,009h,0DDh
		defb	06Eh,0F8h,0DDh,066h,0F9h,0CDh
		defw	LWRD ?SS_CMP_L02
		defb	030h,01Eh,001h,0FCh,07Fh,03Eh,080h
		defb	0ACh,067h,0EDh,042h,038h,013h,021h
		defb	014h,000h,039h,07Eh,0DDh,096h,0F8h
		defb	077h,023h,07Eh,0DDh,09Eh,0F9h,077h
		defb	0DDh,036h,00Ch,000h,0DDh,06Eh,008h
		defb	0DDh,066h,009h,02Bh,0DDh,075h,008h
		defb	0DDh,074h,009h,0AFh,0DDh,0B6h,010h
		defb	028h,006h,0DDh,036h
		defb	00Eh,000h,018h,004h,0DDh,036h,010h
		defb	001h,0AFh,0DDh,0B6h,00Ch,028h,00Ah
		defb	0AFh,0DDh,077h,0FEh,0DDh,077h,0FFh
		defb	0C3h
		defw	LWRD cstr_base+001E4h
		defb	0DDh,0CBh,0F9h,07Eh,0CAh
		defw	LWRD cstr_base+001D4h
		defb	0DDh,06Eh,0F6h,0DDh,066h,0F7h,023h
		defb	0DDh,075h,0F6h,0DDh,074h,0F7h,02Bh
		defb	036h,030h,0DDh,06Eh,008h,0DDh,066h
		defb	009h,0DDh,075h,0FAh,0DDh,074h,0FBh
		defb	07Dh,0B4h,020h,005h,0DDh,0B6h,010h
		defb	028h,010h,0DDh,06Eh,0F6h,0DDh,066h
		defb	0F7h,023h,0DDh,075h,0F6h,0DDh,074h
		defb	0F7h,02Bh,036h,02Eh
		defb	0AFh,0DDh,077h,0FCh,0DDh,077h,0FDh
		defb	0DDh,04Eh,0FCh,0DDh,046h,0FDh,00Bh
		defb	0DDh,071h,0FCh,0DDh,070h,0FDh,0DDh
		defb	06Eh,0F8h,0DDh,066h,0F9h,0CDh
		defw	LWRD ?SS_CMP_L02
		defb	030h,027h,0DDh,07Eh,008h,0DDh,0B6h
		defb	009h,028h,01Fh,0DDh,06Eh,0F6h,0DDh
		defb	066h,0F7h,023h,0DDh,075h,0F6h,0DDh
		defb	074h,0F7h,02Bh,036h,030h,0DDh,06Eh
		defb	008h,0DDh,066h,009h,02Bh,0DDh,075h
		defb	008h,0DDh,074h,009h,018h,0C1h,0DDh
		defb	07Eh,0FAh,0DDh,046h,0FBh,02Fh,04Fh
		defb	078h,02Fh,047h,0CDh
		defw	LWRD ?SS_CMP_L02
		defb	0DAh
		defw	LWRD cstr_base+00345h
		defb	0DDh,036h,0FEh,001h,0DDh,036h,0FFh
		defb	000h,018h,010h,0DDh,04Eh,0F8h,0DDh
		defb	046h,0F9h,06Fh,067h,0EDh,042h,0DDh
		defb	075h,0FEh,0DDh,074h,0FFh,0DDh,06Eh
		defb	0FEh,0DDh,066h,0FFh,0DDh,075h,0FCh
		defb	0DDh,074h,0FDh,0DDh,04Eh,0FCh,0DDh
		defb	046h,0FDh,0DDh,06Eh,008h,0DDh,066h
		defb	009h,0CDh
		defw	LWRD ?SS_CMP_L02
		defb	038h,077h,021h,00Eh,000h,039h,0E5h
		defb	0DDh,04Eh,004h,0DDh,046h,005h,0DDh
		defb	06Eh,002h,0DDh,066h,003h,0CDh
		defw	LWRD ?F_TO_L_L04
		defb	0DDh,075h,0FAh,07Ch,007h,09Fh,04Fh
		defb	041h,0CDh
		defw	LWRD ?SL_TO_F_L04
		defb	0EBh,0E1h,0CDh
		defw	LWRD ?F_SUBASG_L04
		defb	021h,00Eh,000h,039h,001h,020h,041h
		defb	011h,000h,000h,0CDh
		defw	LWRD ?F_MULASG_L04
		defb	0DDh,06Eh,0F6h,0DDh,066h,0F7h,023h
		defb	0DDh,075h,0F6h,0DDh,074h,0F7h,02Bh
		defb	0DDh,07Eh,0FAh,0C6h,030h,077h,0DDh
		defb	06Eh,0FCh,0DDh,066h,0FDh,023h,0DDh
		defb	075h,0FCh,0DDh,074h,0FDh,02Bh,07Dh
		defb	0B4h,020h,098h,0DDh,07Eh,008h,0DDh
		defb	0B6h,009h,020h,005h,0DDh,0B6h,010h
		defb	028h,08Bh,0DDh,06Eh
		defb	0F6h,0DDh,066h,0F7h,023h,0DDh,075h
		defb	0F6h,0DDh,074h,0F7h,02Bh,036h,02Eh
		defb	0C3h
		defw	LWRD cstr_base+001F0h
		defb	021h,0A0h,040h,0E5h,021h,000h,000h
		defb	0E5h,0CDh
		defw	LWRD cstr_base+00456h
		defb	0DAh
		defw	LWRD cstr_base+00345h
		defb	0DDh,06Eh,0F6h,0DDh,066h,0F7h,02Bh
		defb	0E5h,0FDh,0E1h,0DDh,036h,0FAh,001h
		defb	0DDh,036h,0FBh,000h,0FDh,07Eh,000h
		defb	0FEh,02Eh,028h,022h,0FDh,07Eh,000h
		defb	0DDh,086h,0FAh,0FDh,077h,000h,0FEh
		defb	03Ah,020h,00Eh,0FDh,036h,000h,030h
		defb	0DDh,036h,0FAh,001h,0DDh,036h,0FBh
		defb	000h,018h,007h,0AFh
		defb	0DDh,077h,0FAh,0DDh,077h,0FBh,0FDh
		defb	0E5h,0C1h,0DDh,06Eh,00Ah,0DDh,066h
		defb	00Bh,0A7h,0EDh,042h,0FDh,02Bh,038h
		defb	0C7h,0DDh,07Eh,0FAh,0DDh,0B6h,0FBh
		defb	028h,06Ch,0AFh,0DDh,0B6h,00Ch,0DDh
		defb	06Eh,0F6h,0DDh,066h,0F7h,028h,036h
		defb	0E5h,0FDh,0E1h,0FDh,0E5h,0C1h,0DDh
		defb	06Eh,00Ah,0DDh,066h
		defb	00Bh,0A7h,0EDh,042h,030h,01Bh,0FDh
		defb	07Eh,0FFh,0FEh,02Eh,020h,00Ah,0FDh
		defb	046h,0FEh,0FDh,070h,000h,0FDh,02Bh
		defb	018h,006h,0FDh,046h,0FFh,0FDh,070h
		defb	000h,0FDh,02Bh,018h,0D7h,0DDh,034h
		defb	0F8h,020h,027h,0DDh,034h,0F9h,018h
		defb	022h,023h,0DDh,075h,0F6h,0DDh,074h
		defb	0F7h,0E5h,0FDh,0E1h
		defb	0FDh,0E5h,0C1h,0DDh,06Eh,00Ah,0DDh
		defb	066h,00Bh,0A7h,0EDh,042h,030h,00Ah
		defb	0FDh,046h,0FFh,0FDh,070h,000h,0FDh
		defb	02Bh,018h,0E8h,0DDh,06Eh,00Ah,0DDh
		defb	066h,00Bh,036h,031h,0AFh,0DDh,0B6h
		defb	00Eh,028h,01Fh,0DDh,06Eh,0F6h,0DDh
		defb	066h,0F7h,02Bh,07Eh,0FEh,030h,020h
		defb	008h,0DDh,075h,0F6h
		defb	0DDh,074h,0F7h,018h,0ECh,07Eh,0FEh
		defb	02Eh,020h,006h,0DDh,075h,0F6h,0DDh
		defb	074h,0F7h,0AFh,0DDh,0B6h,00Ch,0CAh
		defw	LWRD cstr_base+0044Bh
		defb	0DDh,06Eh,0F6h,0DDh,066h,0F7h,077h
		defb	0DDh,0CBh,0F9h,07Eh,023h,023h,0DDh
		defb	075h,0F6h,0DDh,074h,0F7h,02Bh,028h
		defb	015h,036h,02Dh,021h,000h,000h,0DDh
		defb	04Eh,0F8h,0DDh,046h,0F9h,0EDh,042h
		defb	0DDh,075h,0F8h,0DDh,074h,0F9h,018h
		defb	002h,036h,02Bh,021h,002h,000h,039h
		defb	07Eh,0C6h,00Ah,077h
		defb	023h,07Eh,0CEh,000h,077h,0AFh,0DDh
		defb	077h,0FAh,0DDh,077h,0FBh,0DDh,034h
		defb	0FAh,020h,003h,0DDh,034h,0FBh,0DDh
		defb	06Eh,0F6h,0DDh,066h,0F7h,023h,0DDh
		defb	075h,0F6h,0DDh,074h,0F7h,02Bh,001h
		defb	00Ah,000h,0DDh,05Eh,0F8h,0DDh,056h
		defb	0F9h,0CDh
		defw	LWRD ?SS_MOD_L02
		defb	07Bh,0C6h,030h,077h,021h,004h,000h
		defb	039h,0CDh
		defw	LWRD ?SS_DIVASG_L02
		defb	07Bh,0B2h,020h,0CFh,001h,002h,080h
		defb	0DDh,06Eh,0FAh,0DDh,066h,0FBh,078h
		defb	0ACh,067h,0EDh,042h,038h,0BFh,0DDh
		defb	06Eh,0FAh,0DDh,066h,0FBh,0DDh,075h
		defb	0FCh,0DDh,074h,0FDh,0DDh,04Eh,0FAh
		defb	0DDh,046h,0FBh,021h,000h,000h,0CDh
		defw	LWRD ?SS_CMP_L02
		defb	030h,032h,0DDh,04Eh,0FCh,0DDh,046h
		defb	0FDh,0DDh,06Eh,0FAh,0DDh,066h,0FBh
		defb	0A7h,0EDh,042h,001h,0F5h,0FFh,009h
		defb	0DDh,04Eh,0F6h,0DDh,046h,0F7h,009h
		defb	0E5h,0DDh,05Eh,0FAh,0DDh,056h,0FBh
		defb	069h,060h,0A7h,0EDh,052h,046h,0E1h
		defb	070h,01Bh,0DDh,073h,0FAh,0DDh,072h
		defb	0FBh,018h,0C0h,023h
		defb	023h,039h,07Eh,0D6h,00Ah,077h,023h
		defb	07Eh,0DEh,000h,077h,0DDh,06Eh,0F6h
		defb	0DDh,066h,0F7h,0FDh,0E1h,0C3h
		defw	LWRD ?LEAVE_DIRECT_L09
		defb	0DDh,04Eh,004h,0DDh,046h,005h,0DDh
		defb	06Eh,002h,0DDh,066h,003h,0C3h
		defw	LWRD ?SL_CMP_L03
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	058h,0FFh,0FDh,0E5h,0DDh,06Eh,002h
		defb	0DDh,066h,003h,0E5h,0FDh,0E1h,021h
		defb	006h,000h,039h,0AFh,077h,023h,077h
		defb	0FDh,046h,000h,021h,01Ch,000h,039h
		defb	070h,078h,0FEh,025h,0FDh,023h,028h
		defb	022h,0AFh,0B0h,020h,00Dh,021h,006h
		defb	000h,039h,046h,023h,066h,068h,0FDh
		defb	0E1h,0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		defb	0DDh,04Eh,00Eh,0DDh,046h,00Fh,05Fh
		defb	0CDh
		defw	LWRD cstr_base+00C65h
		defb	039h,034h,023h,020h,0D1h,018h,019h
		defb	0FDh,07Eh,000h,0FEh,025h,020h,015h
		defb	0DDh,04Eh,00Eh,0DDh,046h,00Fh,01Eh
		defb	025h,0CDh
		defw	LWRD cstr_base+00C65h
		defb	039h,034h,023h,0FDh,023h,020h,0B6h
		defb	034h,018h,0B3h,021h,002h,000h,039h
		defb	0E5h,021h,026h,000h,039h,04Dh,044h
		defb	0E1h,071h,023h,070h,021h,00Eh,000h
		defb	039h,071h,023h,070h,021h,01Ah,000h
		defb	039h,036h
    t_push_rel           (0001) 00000000
    t_pop_8             
		defb	023h,036h
    t_rel_fi_8           (01) 0000
		defb	021h,021h,000h,039h,036h,000h,023h
		defb	036h,000h,02Bh,02Bh,02Bh,036h,000h
		defb	02Bh,02Bh,036h,000h,0FDh,05Eh,000h
		defb	0CDh
		defw	LWRD ?C_V_SWITCH_L06
		defb	005h,000h,020h,023h,02Bh,02Dh,030h
		defw	LWRD cstr_base+0053Dh
		defw	LWRD cstr_base+00534h
		defw	LWRD cstr_base+00526h
		defw	LWRD cstr_base+0051Ch
		defw	LWRD cstr_base+0052Dh
		defw	LWRD cstr_base+00514h
		defb	021h,01Dh,000h,039h,0AFh,0B6h,020h
		defb	01Dh,0FDh,046h,000h,021h,01Dh,000h
		defb	039h,070h,018h,013h,021h,01Fh,000h
		defb	039h,034h,018h,00Ch,021h,022h,000h
		defb	039h,034h,018h,005h,021h,021h,000h
		defb	039h,034h,0FDh,023h,018h,0BEh,0FDh
		defb	07Eh,000h,0FEh,02Ah,021h,010h,000h
		defb	039h,020h,033h,0E5h
		defb	0DDh,06Eh,010h,0DDh,066h,011h,04Eh
		defb	059h,023h,046h,050h,013h,013h,072h
		defb	02Bh,073h,069h,060h,04Eh,023h,046h
		defb	0E1h,071h,023h,070h,0CBh,078h,028h
		defb	011h,021h,010h,000h,039h,0AFh,091h
		defb	077h,023h,03Eh,000h,09Eh,077h,021h
		defb	01Fh,000h,039h,034h,0FDh,023h,018h
		defb	03Dh,0AFh,077h,023h
		defb	077h,0FDh,07Eh,000h,0FEh,030h,038h
		defb	032h,03Eh,039h,0FDh,0BEh,000h,038h
		defb	02Bh,021h,010h,000h,039h,0E5h,0FDh
		defb	04Eh,000h,006h,000h,021h,0D0h,0FFh
		defb	009h,0E5h,021h,014h,000h,039h,046h
		defb	023h,066h,068h,029h,04Dh,044h,029h
		defb	029h,009h,04Dh,044h,0E1h,009h,04Dh
		defb	044h,0E1h,071h,023h
		defb	070h,0FDh,023h,018h,0C7h,021h,01Fh
		defb	000h,039h,0AFh,0B6h,028h,004h,023h
		defb	023h,036h,000h,0FDh,07Eh,000h,0FEh
		defb	02Eh,020h,068h,0FDh,07Eh,001h,0FEh
		defb	02Ah,021h,004h,000h,039h,0FDh,023h
		defb	020h,01Eh,0E5h,0DDh,06Eh,010h,0DDh
		defb	066h,011h,04Eh,059h,023h,046h,050h
		defb	013h,013h,072h,02Bh
		defb	073h,069h,060h,04Eh,023h,046h,0E1h
		defb	071h,023h,070h,0FDh,023h,018h,046h
		defb	0AFh,077h,023h,077h,0FDh,07Eh,000h
		defb	0FEh,030h,038h,03Bh,03Eh,039h,0FDh
		defb	0BEh,000h,038h,034h,021h,004h,000h
		defb	039h,0E5h,0FDh,04Eh,000h,006h,000h
		defb	021h
		defb	0D0h,0FFh,009h,0E5h,021h,008h,000h
		defb	039h,046h,023h,066h,068h,029h,04Dh
		defb	044h,029h,029h,009h,04Dh,044h,0E1h
		defb	009h,04Dh,044h,0E1h,071h,023h,070h
		defb	0FDh,023h,018h,0C7h,021h,004h,000h
		defb	039h,036h,0FFh,023h,036h,0FFh,021h
		defb	020h,000h,039h,036h,000h,02Bh,02Bh
		defb	036h,000h,0FDh,07Eh
                         00FE4C2804FE6C2003341807FE682005232334FD23FD4600211C00397058FD23CD
		defw	LWRD ?C_V_SWITCH_L06
		defb	010h,000h,000h,045h,047h,058h,063h
		defb	064h,065h,066h,067h,069h,06Eh,06Fh
		defb	070h,073h,075h,078h
		defw	LWRD cstr_base+00B58h
		defw	LWRD cstr_base+007D9h
		defw	LWRD cstr_base+007E2h
		defw	LWRD cstr_base+00728h
		defw	LWRD cstr_base+007E2h
		defw	LWRD cstr_base+007C2h
		defw	LWRD cstr_base+0069Fh
		defw	LWRD cstr_base+00869h
		defw	LWRD cstr_base+00A35h
		defw	LWRD cstr_base+00A4Fh
		defw	LWRD cstr_base+00A51h
		defw	LWRD cstr_base+00869h
		defw	LWRD cstr_base+00706h
		defw	LWRD cstr_base+007E2h
		defw	LWRD cstr_base+00A35h
		defw	LWRD cstr_base+00A51h
		defw	LWRD cstr_base+00B56h
		defb	0AFh,020h,03Fh,028h,00Eh,021h,020h
		defb	000h,039h,0B6h,020h,036h,02Bh,02Bh
		defb	0B6h,028h,031h,018h,005h,023h,023h
		defb	0B6h,028h,02Ah,0DDh,06Eh,010h,0DDh
		defb	066h,011h,04Eh,059h,023h,046h,050h
		defb	013h,013h,072h,02Bh,073h,069h,060h
		defb	07Eh,023h,066h,06Fh,0E5h,021h,008h
		defb	000h,039h,05Eh,023h
		defb	056h,07Ah,007h,09Fh,04Fh,041h,0E1h
		defb	073h,023h,072h,023h,018h,01Fh,0DDh
		defb	06Eh,010h,0DDh,066h,011h,04Eh,059h
		defb	023h,046h,050h,013h,013h,072h,02Bh
		defb	073h,069h,060h,07Eh,023h,066h,06Fh
		defb	0E5h,021h,008h,000h,039h,04Eh,023h
		defb	046h,0E1h,071h,023h,070h,0C3h
		defw	LWRD cstr_base+0047Dh
		defb	0DDh,06Eh,010h,0DDh,066h,011h,04Eh
		defb	059h,023h,046h,050h,013h,013h,072h
		defb	02Bh,073h,00Ah,021h,024h,000h,039h
		defb	077h,021h,00Eh,000h,039h,034h,023h
		defb	020h,001h,034h,0C3h
		defw	LWRD cstr_base+00B74h
		defb	021h,002h,000h,039h,0E5h,0DDh,06Eh
		defb	010h,0DDh,066h,011h,04Eh,059h,023h
		defb	046h,050h,013h,013h,072h,02Bh,073h
		defb	069h,060h,04Eh,023h,046h,0E1h,071h
		defb	023h,070h,079h,0B0h,020h,009h,021h
		defb	002h,000h,039h,036h
    t_push_rel           (0001) 00000011
    t_pop_8             
		defb	023h,036h
    t_rel_fi_8           (01) 0011
		defb	021h,004h,000h,039h,023h,0CBh,07Eh
		defb	028h,006h,02Bh,036h,010h,023h,036h
		defb	027h,021h,00Ch,000h,039h,0AFh,077h
		defb	023h,077h,021h,002h,000h,039h,04Eh
		defb	023h,046h,003h,070h,02Bh,071h,00Bh
		defb	00Ah,0B7h,028h,01Dh,023h,023h,04Eh
		defb	023h,046h,021h,00Ch,000h,039h,056h
		defb	023h,066h,06Ah,0CDh
		defw	LWRD ?SS_CMP_L02
		defb	030h,00Bh,021h,00Ch,000h,039h,034h
		defb	023h,020h,0D6h,034h,018h,0D3h,021h
		defb	00Eh,000h,039h,0E5h,021h,004h,000h
		defb	039h,04Eh,023h,046h,00Bh,070h,02Bh
		defb	071h,0E1h,071h,023h,070h,021h,002h
		defb	000h,039h,0E5h,021h,00Eh,000h,039h
		defb	04Eh,023h,046h,0E1h,07Eh,091h,077h
		defb	023h,07Eh,098h,077h
		defb	0C3h
		defw	LWRD cstr_base+00B74h
		defb	021h,022h,000h,039h,0AFh,0B6h,028h
		defb	00Fh,021h,004h,000h,039h,07Eh,023h
		defb	0B6h,020h,006h,02Bh,034h,023h,020h
		defb	001h,034h,021h,01Ah,000h,039h,036h
    t_push_rel           (0001) 00000020
    t_pop_8             
		defb	023h,036h
    t_rel_fi_8           (01) 0020
		defb	078h,0FEh,070h,028h,003h,0AFh,028h
		defb	00Fh,0DDh,06Eh,010h,0DDh,066h,011h
		defb	04Eh,059h,023h,046h,050h,013h,013h
		defb	018h,025h,021h,01Eh,000h,039h,0B6h
		defb	0DDh,06Eh,010h,0DDh,066h,011h,04Eh
		defb	059h,023h,046h,050h,013h,013h,028h
		defb	011h,013h,013h,072h,02Bh,073h,069h
		defb	060h,05Eh,023h,056h
		defb	023h,04Eh,023h,046h,0EBh,018h,00Bh
		defb	072h,02Bh,073h,00Ah,06Fh,003h,00Ah
		defb	067h,001h,000h,000h,0E5h,021h,00Ah
		defb	000h,039h,0D1h,073h,023h,072h,023h
		defb	071h,023h,070h,021h,01Ch,000h,039h
		defb	07Eh,0FEh,06Fh,020h,008h,001h,000h
		defb	000h,021h,008h,000h,018h,00Eh,0FEh
		defb	075h,020h,004h,02Eh
		defb	00Ah,018h,002h,02Eh,010h,001h,000h
		defb	000h,061h,0E5h,02Eh,018h,039h,0D1h
		defb	073h,023h,072h,023h,071h,023h,070h
		defb	021h,01Dh,000h,039h,070h,0C3h
		defw	LWRD cstr_base+008F4h
		defb	0AFh,028h,02Fh,0DDh,06Eh,010h,0DDh
		defb	066h,011h,04Eh,059h,023h,046h,050h
		defb	013h,013h,013h,013h,072h,02Bh,073h
		defb	069h,060h,05Eh,023h,056h,023h,04Eh
		defb	023h,046h,0EBh,0E5h,021h,00Ah,000h
		defb	039h,0D1h,073h,023h,072h,023h,071h
		defb	023h,070h,0CBh,078h,028h,04Eh,018h
		defb	040h,023h,023h,0B6h
		defb	0DDh,06Eh,010h,0DDh,066h,011h,04Eh
		defb	059h,023h,046h,050h,013h,013h,028h
		defb	011h,013h,013h,072h,02Bh,073h,069h
		defb	060h,05Eh,023h,056h,023h,04Eh,023h
		defb	046h,0EBh,018h,00Ch,072h,02Bh,073h
		defb	00Ah,06Fh,003h,00Ah,067h,007h,09Fh
		defb	04Fh,041h,0E5h,021h,00Ah,000h,039h
		defb	0D1h,073h,023h,072h
		defb	023h,071h,023h,070h,0CBh,078h,028h
		defb	00Ch,02Bh,02Bh,02Bh,0CDh
		defw	LWRD ?L_NEGASG_L03
		defb	021h,01Dh,000h,039h,036h,02Dh,021h
		defb	016h,000h,039h,0AFh,036h,00Ah,023h
		defb	077h,023h,077h,023h,077h,021h,002h
		defb	000h,039h,0E5h,021h,026h,000h,039h
		defb	001h,085h,000h,009h,04Dh,044h,0E1h
		defb	071h,023h,070h,021h,00Eh,000h,039h
		defb	071h,023h,070h,021h,008h,000h,039h
		defb	07Eh,023h,0B6h,023h
		defb	0B6h,023h,0B6h,028h,002h,03Eh,001h
		defb	021h,023h,000h,039h,077h,021h,004h
		defb	000h,039h,07Eh,023h,0B6h,020h,007h
		defb	021h,023h,000h,039h,0B6h,028h,04Eh
		defb	021h,016h,000h,039h,04Eh,023h,046h
		defb	023h,05Eh,023h,056h,0D5h,0C5h,021h
		defb	00Ch,000h,039h,05Eh,023h,056h,023h
		defb	04Eh,023h,046h,0EBh
		defb	0CDh
		defw	LWRD ?UL_MOD_L03
		defb	0E5h,021h,01Ch,000h,039h,04Eh,023h
		defb	046h,0E1h,009h,046h,021h,002h,000h
		defb	039h,05Eh,023h,056h,01Bh,072h,02Bh
		defb	073h,0EBh,070h,021h,008h,000h,039h
		defb	0E5h,021h,018h,000h,039h,05Eh,023h
		defb	056h,023h,04Eh,023h,046h,0E1h,0CDh
		defw	LWRD ?UL_DIVASG_L03
		defb	07Bh,0B2h,0B1h,0B0h,020h,0B2h,021h
		defb	004h,000h,039h,023h,0CBh,07Eh,028h
		defb	02Dh,021h,021h,000h,039h,0AFh,0B6h
		defb	028h,025h,021h,004h,000h,039h,0E5h
		defb	021h,01Fh,000h,039h,0AFh,0B6h,028h
		defb	004h,00Eh,001h,018h,001h,04Fh,006h
		defb	000h,021h,012h,000h,039h,056h,023h
		defb	066h,06Ah,0A7h,0EDh
		defb	042h,04Dh,044h,0E1h,071h,023h,070h
		defb	021h,002h,000h,039h,04Eh,023h,046h
		defb	021h,00Eh,000h,039h,056h,023h,066h
		defb	06Ah,0A7h,0EDh,042h,0E5h,021h,006h
		defb	000h,039h,04Eh,023h,046h,0E1h,0CDh
		defw	LWRD ?SS_CMP_L02
		defb	030h,011h,021h,002h,000h,039h,04Eh
		defb	023h,046h,00Bh,070h,02Bh,071h,069h
		defb	060h,036h,030h,018h,0CFh,021h,022h
		defb	000h,039h,0AFh,0B6h,028h,043h,023h
		defb	0AFh,0B6h,028h,03Eh,021h,01Ch,000h
		defb	039h,07Eh,0FEh,078h,028h,004h,0FEh
		defb	058h,020h,011h,021h,002h,000h,039h
		defb	04Eh,023h,046h,00Bh
		defb	070h,02Bh,071h,0C5h,047h,0E1h,070h
		defb	018h,011h,0FEh,06Fh,020h,01Ch,021h
		defb	002h,000h,039h,07Eh,023h,066h,06Fh
		defb	07Eh,0FEh,030h,028h,00Fh,021h,002h
		defb	000h,039h,04Eh,023h,046h,00Bh,070h
		defb	02Bh,071h,069h,060h,036h,030h,0C3h
		defw	LWRD cstr_base+00B74h
		defb	078h,0D6h,002h,077h,021h,00Ch,000h
		defb	039h,036h,001h,021h,004h,000h,039h
		defb	07Eh,023h,0B6h,020h,00Fh,02Bh,036h
		defb	001h,023h,077h,018h,008h,036h,000h
		defb	021h,00Ch,000h,039h,0AFh,077h,021h
		defb	004h,000h,039h,023h,0CBh,07Eh,028h
		defb	006h,02Bh,036h,006h,023h,036h,000h
		defb	0AFh,0DDh,06Eh,010h
		defb	0DDh,066h,011h,04Eh,059h,023h,046h
		defb	050h,013h,013h,013h,013h,072h,02Bh
		defb	073h,069h,060h,05Eh,023h,056h,023h
		defb	04Eh,023h,046h,0EBh,0E5h,021h,014h
		defb	000h,039h,0D1h,073h,023h,072h,023h
		defb	071h,023h,070h,028h,006h,0CBh,078h
		defb	028h,012h,018h,004h,0CBh,078h,028h
		defb	00Ch,02Bh,02Bh,02Bh
		defb	0CDh
		defw	LWRD ?F_NEGASG_L04
		defb	021h,01Dh,000h,039h,036h,02Dh,021h
		defb	00Eh,000h,039h,0E5h,021h,024h,000h
		defb	039h,04Eh,0C5h,021h,010h,000h,039h
		defb	04Eh,0C5h,021h,022h,000h,039h,04Eh
		defb	0C5h,021h,00Ah,000h,039h,0E5h,021h
		defb	01Ah,000h,039h,04Eh,023h,046h,0E1h
		defb	07Eh,081h,077h,023h,07Eh,088h,077h
		defb	02Bh,06Eh,067h,0E5h
		defb	021h,00Eh,000h,039h,04Eh,023h,046h
		defb	0C5h,021h,01Eh,000h,039h,05Eh,023h
		defb	056h,023h,04Eh,023h,046h,0CDh
		defw	LWRD cstr_base+00000h
		defb	0F1h,0F1h,0F1h,0F1h,0F1h,04Dh,044h
		defb	0E1h,071h,023h,070h,021h,021h,000h
		defb	039h,0AFh,0B6h,028h,074h,021h,004h
		defb	000h,039h,0E5h,021h,01Fh,000h,039h
		defb	0AFh,0B6h,028h,004h,00Eh,001h,018h
		defb	001h,04Fh,006h,000h,021h,012h,000h
		defb	039h,056h,023h,066h,06Ah,0A7h,0EDh
		defb	042h,04Dh,044h,0E1h
		defb	071h,023h,070h,021h,002h,000h,039h
		defb	04Eh,023h,046h,021h,00Eh,000h,039h
		defb	056h,023h,066h,06Ah,0A7h,0EDh,042h
		defb	0E5h,021h,006h,000h,039h,04Eh,023h
		defb	046h,0E1h,0CDh
		defw	LWRD ?SS_CMP_L02
		defb	030h,02Fh,021h,002h,000h,039h,04Eh
		defb	023h,046h,00Bh,070h,02Bh,071h,069h
		defb	060h,036h,030h,018h,0CFh,0FDh,02Bh
		defb	021h,002h,000h,039h,036h
    t_push_rel           (0001) 00000031
    t_pop_8             
		defb	023h,036h
    t_rel_fi_8           (01) 0031
		defb	021h,00Eh,000h,039h,036h
    t_push_rel           (0001) 00000031
    t_pop_8             
		defb	023h,036h
    t_rel_fi_8           (01) 0031
		defb	02Bh,07Eh,0C6h,003h,077h,023h,07Eh
		defb	0CEh,000h,077h,021h,004h,000h,039h
		defb	0E5h,02Bh,02Bh,04Eh,023h,046h,021h
		defb	010h,000h,039h,056h,023h,066h,06Ah
		defb	0A7h,0EDh,042h,04Dh,044h,0E1h,071h
		defb	023h,070h,021h,010h,000h,039h,056h
		defb	023h,066h,06Ah,0CDh
		defw	LWRD ?SS_CMP_L02
		defb	021h,00Ch,000h,030h,007h,039h,0AFh
		defb	077h,023h,077h,018h,02Dh,039h,0E5h
		defb	021h,01Fh,000h,039h,0AFh,0B6h,028h
		defb	004h,00Eh,001h,018h,001h,04Fh,006h
		defb	000h,021h,012h,000h,039h,023h,066h
		defb	06Ah,0A7h,0EDh,042h,0E5h,021h,008h
		defb	000h,039h,04Eh,023h,046h,0E1h,0A7h
		defb	0EDh,042h,04Dh,044h
		defb	0E1h,071h,023h,070h,021h,01Fh,000h
		defb	039h,0AFh,0B6h,020h,01Ah,021h,00Ch
		defb	000h,039h,04Eh,023h,046h,00Bh,070h
		defb	02Bh,071h,0CBh,078h,020h,00Bh,0CDh
		defw	LWRD cstr_base+00C5Dh
		defb	039h,034h,023h,020h,0E9h,034h,018h
		defb	0E6h,021h,01Dh,000h,039h,0AFh,0B6h
		defb	028h,010h,0DDh,04Eh,00Eh,0DDh,046h
		defb	00Fh,05Fh,0CDh
		defw	LWRD cstr_base+00C65h
		defb	039h,034h,023h,020h,001h,034h,021h
		defb	004h,000h,039h,04Eh,023h,046h,00Bh
		defb	070h,02Bh,071h,0CBh,078h,020h,01Ch
		defb	0DDh,04Eh,00Eh,0DDh,046h,00Fh,02Bh
		defb	02Bh,05Eh,023h,056h,013h,072h,02Bh
		defb	073h,01Bh,01Ah,05Fh,0CDh
		defw	LWRD cstr_base+00C65h
		defb	039h,034h,023h,020h,0D7h,018h,0D4h
		defb	021h,01Fh,000h,039h,0AFh,0B6h,028h
		defb	01Ah,021h,00Ch,000h,039h,04Eh,023h
		defb	046h,00Bh,070h,02Bh,071h,0CBh,078h
		defb	020h,00Bh,0CDh
		defw	LWRD cstr_base+00C5Dh
		defb	039h,034h,023h,020h,0E9h,034h,018h
		defb	0E6h,0C3h
		defw	LWRD cstr_base+0047Dh
		defb	0DDh,04Eh,00Eh,0DDh,046h,00Fh,01Eh
		defb	020h,0DDh,07Eh,00Ch,0DDh,06Eh,00Ah
		defb	0DDh,066h,00Bh,0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	021h,006h,000h,0C9h
    t_org_rel            (01) 00000000
		defb	030h,031h,032h,033h,034h,035h,036h
		defb	037h,038h,039h,041h,042h,043h,044h
		defb	045h,046h,000h,028h,06Eh,075h,06Ch
		defb	06Ch,020h,070h,06Fh,069h,06Eh,074h
		defb	065h,072h,029h,000h,030h,031h,032h
		defb	033h,034h,035h,036h,037h,038h,039h
		defb	061h,062h,063h,064h,065h,066h,000h
		defb	03Fh,03Fh,03Fh,000h
		endmod

; -----------------------------------------------------------------------------

	end
