; rand.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	rand
		rseg	CODE
code_base:
		rseg	IDATA0
idata0_base:
		rseg	CDATA0
cdata0_base:
    t_symbol_def         PUBLIC_REL (000B) 00000000 '__next_rand'
		public	rand
rand		equ	cdata0_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?L_MUL_L03
		extern	?BANK_FAST_LEAVE_L08
		defb	0C5h,0D5h,021h,0C6h,041h,0E5h,021h
		defb	06Dh,04Eh,0E5h,0EDh,04Bh
    t_rel_p16            (0B) 0002
		defb	02Ah
    t_rel_p16            (0B) 0000
		defb	0CDh
		defw	LWRD ?L_MUL_L03
		defb	0C5h,0E5h,021h,039h,030h,0C1h,009h
		defb	0EBh,021h,000h,000h,0C1h,0EDh,04Ah
		defb	04Dh,044h,0EBh,022h
    t_rel_p16            (0B) 0000
		defb	0EDh,043h
    t_rel_p16            (0B) 0002
		defb	069h,060h,0CBh,0BCh,0D1h,0C1h,0C3h
		defw	LWRD ?BANK_FAST_LEAVE_L08
    t_org_rel            (0B) 00000000
    t_org_rel            (0B) 00000004
    t_org_rel            (0C) 00000000
		defb	001h,000h,000h,000h
		endmod

; -----------------------------------------------------------------------------

	end
