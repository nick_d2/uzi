; SSDIV.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?SS_DIV_L02
		rseg	RCODE
rcode_base:
		public	?SS_DIV_L02
?SS_DIV_L02	equ	rcode_base+00000000h
		extern	?S_FIND_SIGN_L02
		extern	?S_DIVMOD_L02
		defb	0F5h,0C5h,0E5h,0AFh,0CDh
		defw	LWRD ?S_FIND_SIGN_L02
		defb	0F5h,0CDh
		defw	LWRD ?S_DIVMOD_L02
		defb	0F1h,0B7h,028h,003h,013h,018h,006h
		defb	07Bh,02Fh,05Fh,07Ah,02Fh,057h,0E1h
		defb	0C1h,0F1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
