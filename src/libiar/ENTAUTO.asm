; ENTAUTO.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?ENT_AUTO_L09
		public	?ENT_AUTO_L09
?ENT_AUTO_L09	equ	00000010h
		extern	?ENT_AUTO_DIRECT_L09
    t_org_abs            S86=0000 00000000
    t_org_abs            S86=0000 00000010
		defb	0C3h
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		endmod

; -----------------------------------------------------------------------------

	end
