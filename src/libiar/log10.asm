; log10.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	log10
		rseg	CODE
code_base:
		extern	errno
		extern	log
		public	log10
log10		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?F_MUL_L04
		extern	?F_CMP_L04
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?BANK_LEAVE_32_L08
		extern	?ENT_PARM_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_PARM_DIRECT_L09
		defb	0C5h,0D5h,001h,000h,000h,069h,060h
		defb	0CDh
		defw	LWRD ?F_CMP_L04
		defb	038h,01Fh,0DDh,07Eh,004h,0DDh,0B6h
		defb	005h,020h,00Ah,02Eh,022h,022h
		defw	LWRD errno
		defb	001h,07Fh,0FFh,018h,008h,02Eh,021h
		defb	022h
		defw	LWRD errno
		defb	001h,07Fh,07Fh,021h,0FFh,0FFh,018h
		defb	019h,0DDh,04Eh,004h,0DDh,046h,005h
		defb	03Eh
		defb	BYTE3 log
		defb	021h
		defw	LWRD log
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0C5h,0E5h,001h,0DEh,03Eh,021h,0D9h
		defb	05Bh,0CDh
		defw	LWRD ?F_MUL_L04
		defb	0C3h
		defw	LWRD ?BANK_LEAVE_32_L08
		endmod

; -----------------------------------------------------------------------------

	end
