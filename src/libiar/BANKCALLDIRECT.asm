; BANKCALLDIRECT.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

	module	?BANK_CALL_DIRECT_L08

 .if 1
BBR	equ	39h
 .endif

	rseg	RCODE

	public	?BANK_CALL_DIRECT_L08

?BANK_CALL_DIRECT_L08:
 .if 1 ; virtual memory
	push	hl
	ld	h,80h
	ld	l,a			; hl -> 8000h + entry a value
	ld	a,(hl)			; translate bbr via virt memory table
;	pop	hl
;
;	or	a			; check for unused virt memory pages
;	ret	z			; just to be extra paranoid
 .endif

 .if 1
;	push	hl
	in0	h,(BBR)
	out0	(BBR),a
	ex	(sp),hl
	jp	(hl)
 .else
	defb	0E5h,0EDh,020h,039h,0EDh,039h,039h
	defb	0E3h,0E9h
 .endif

	endmod

; -----------------------------------------------------------------------------

	end
