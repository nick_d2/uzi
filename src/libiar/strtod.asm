; strtod.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	strtod
		rseg	CODE
code_base:
		extern	_Small_Ctype
		extern	abs
		extern	errno
		public	strtod
strtod		equ	code_base+000000F1h
		extern	?CL64180B_4_06_L00
		extern	?SS_CMP_L02
		extern	?SL_CMP_L03
		extern	?F_MUL_L04
		extern	?F_DIV_L04
		extern	?F_ADD_L04
		extern	?SL_TO_F_L04
		extern	?F_MULASG_L04
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_LEAVE_32_L08
		extern	?ENT_AUTO_DIRECT_L09
		extern	?LEAVE_32_L09
		extern	?US_RSHASG_IY_L12
		defb	0FDh,0E5h,0DDh,0E5h,0F5h,0F5h,0F5h
		defb	0F5h,0D5h,0DDh,0E1h,021h,004h,000h
		defb	039h,0AFh,077h,023h,077h,023h,036h
		defb	020h,023h,036h,041h,0EBh,0CDh
		defw	LWRD code_base+000E9h
		defb	079h,0B0h,020h,005h,001h,080h,03Fh
		defb	018h,01Bh,0DDh,0E5h,0E1h,0CDh
		defw	LWRD code_base+000E9h
		defb	059h,050h,001h,000h,000h,0A7h,0EDh
		defb	042h,020h,011h,0EBh,001h,080h,03Fh
		defb	0EDh,042h,020h,009h,001h,020h,041h
		defb	021h,000h,000h,0C3h
		defw	LWRD code_base+000D7h
		defb	0DDh,0E5h,0D1h,03Eh
		defb	BYTE3 abs
		defb	021h
		defw	LWRD abs
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0E5h,0FDh,0E1h,021h,000h,000h,039h
		defb	0AFh,077h,023h,077h,023h,036h,080h
		defb	023h,036h,03Fh,0FDh,0E5h,0E1h,0CBh
		defb	045h,028h,014h,021h,000h,000h,039h
		defb	0E5h,021h,006h,000h,039h,05Eh,023h
		defb	056h,023h,04Eh,023h,046h,0E1h,0CDh
		defw	LWRD ?F_MULASG_L04
		defb	0FDh,0E5h,0E1h,03Eh,001h,0ADh,0B4h
		defb	028h,017h,021h,004h,000h,039h,0E5h
		defb	05Eh,023h,056h,023h,04Eh,023h,046h
		defb	0E1h,0CDh
		defw	LWRD ?F_MULASG_L04
		defb	006h,001h,0CDh
		defw	LWRD ?US_RSHASG_IY_L12
		defb	018h,0C5h,0DDh,0E5h,0E1h,0CDh
		defw	LWRD code_base+000E9h
		defb	0CBh,078h,028h,023h,021h,000h,000h
		defb	039h,04Eh,023h,046h,023h,05Eh,023h
		defb	056h,0D5h,0C5h,001h,080h,03Fh,021h
		defb	000h,000h,0CDh
		defw	LWRD ?F_DIV_L04
		defb	0E5h,021h,002h,000h,039h,0D1h,073h
		defb	023h,072h,023h,071h,023h,070h,021h
		defb	000h,000h,039h,05Eh,023h,056h,023h
		defb	04Eh,023h,046h,0EBh,0F1h,0F1h,0F1h
		defb	0F1h,0DDh,0E1h,0FDh,0E1h,0C9h,06Bh
		defb	062h,04Eh,006h,000h,021h,0D0h,0FFh
		defb	009h,07Ch,007h,09Fh,04Fh,041h,0C3h
		defw	LWRD ?SL_TO_F_L04
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0EEh,0FFh,0FDh,0E5h,0DDh,05Eh,002h
		defb	0DDh,056h,003h,0DDh,036h,0FCh,000h
		defb	0FDh,021h,000h,000h,0AFh,0DDh,077h
		defb	0F4h,0DDh,077h,0F5h,0DDh,077h,0F2h
		defb	0DDh,077h,0F3h,0DDh,077h,0EEh,0DDh
		defb	077h,0EFh,0DDh,077h,0F0h,0DDh,077h
		defb	0F1h,06Bh,062h,04Eh,006h,000h,021h
		defw	LWRD _Small_Ctype 0001
		defb	009h,0CBh,05Eh,028h,003h,013h,018h
		defb	0F0h,06Bh,062h,07Eh,0FEh,02Dh,028h
		defb	005h,07Eh,0FEh,02Bh,020h,004h,013h
		defb	07Eh,018h,002h,03Eh,02Bh,0DDh,077h
		defb	0FEh,06Bh,062h,07Eh,0FEh,030h,020h
		defb	007h,013h,0DDh,036h,0FCh,001h,018h
		defb	0F2h,06Bh,062h,04Eh,006h,000h,021h
		defw	LWRD _Small_Ctype 0001
		defb	009h,0CBh,056h,028h,02Fh,0FDh,023h
		defb	0FDh,0E5h,0C1h,021h,006h,000h,0CDh
		defw	LWRD ?SS_CMP_L02
		defb	038h,01Fh,0CDh
		defw	LWRD code_base+000E0h
		defb	0C5h,0E5h,021h,020h,041h,0E5h,021h
		defb	000h,000h,0E5h,0CDh
		defw	LWRD code_base+0040Dh
		defb	0CDh
		defw	LWRD ?F_ADD_L04
		defb	0DDh,075h,0EEh,0DDh,074h,0EFh,0DDh
		defb	071h,0F0h,0DDh,070h,0F1h,013h,018h
		defb	0C4h,001h,026h,080h,0FDh,0E5h,0E1h
		defb	078h,0ACh,067h,0EDh,042h,0D2h
		defw	LWRD code_base+003D7h
		defb	021h,008h,000h,039h,0FDh,0E5h,0C1h
		defb	07Eh,081h,077h,023h,07Eh,088h,077h
		defb	06Bh,062h,07Eh,0FEh,02Eh,020h,047h
		defb	013h,06Bh,062h,04Eh,006h,000h,021h
		defw	LWRD _Small_Ctype 0001
		defb	009h,0CBh,056h,028h,039h,001h,006h
		defb	080h,0DDh,06Eh,0F4h,0DDh,066h,0F5h
		defb	078h,0ACh,067h,0EDh,042h,030h,0E2h
		defb	0DDh,034h,0F4h,020h,003h,0DDh,034h
		defb	0F5h,0CDh
		defw	LWRD code_base+000E0h
		defb	0C5h,0E5h,021h,020h,041h,0E5h,021h
		defb	000h,000h,0E5h,0CDh
		defw	LWRD code_base+0040Dh
		defb	0CDh
		defw	LWRD ?F_ADD_L04
		defb	0DDh,075h,0EEh,0DDh,074h,0EFh,0DDh
		defb	071h,0F0h,0DDh,070h,0F1h,018h,0B9h
		defb	021h,008h,000h,039h,0FDh,0E5h,0C1h
		defb	07Eh,091h,077h,023h,07Eh,098h,077h
		defb	0DDh,073h,0FAh,0DDh,072h,0FBh,06Bh
		defb	062h,07Eh,0FEh,065h,028h,005h,07Eh
		defb	0FEh,045h,020h,079h,013h,06Bh,062h
		defb	07Eh,0FEh,02Dh,028h
		defb	005h,07Eh,0FEh,02Bh,020h,004h,013h
		defb	07Eh,018h,002h,03Eh,02Bh,0DDh,077h
		defb	0FDh,06Bh,062h,04Eh,006h,000h,021h
		defw	LWRD _Small_Ctype 0001
		defb	009h,0CBh,056h,020h,008h,0DDh,05Eh
		defb	0FAh,0DDh,056h,0FBh,018h,04Eh,06Bh
		defb	062h,04Eh,006h,000h,021h
		defw	LWRD _Small_Ctype 0001
		defb	009h,0CBh,056h,028h,031h,0DDh,04Eh
		defb	0F2h,0DDh,046h,0F3h,021h,026h,000h
		defb	0CDh
		defw	LWRD ?SS_CMP_L02
		defb	038h,023h,06Bh,062h,013h,04Eh,006h
		defb	000h,021h,0D0h,0FFh,009h,0E5h,0DDh
		defb	06Eh,0F2h,0DDh,066h,0F3h,029h,04Dh
		defb	044h,029h,029h,009h,04Dh,044h,0E1h
		defb	009h,0DDh,075h,0F2h,0DDh,074h,0F3h
		defb	018h,0C2h,06Bh,062h,04Eh,006h,000h
		defb	021h
		defw	LWRD _Small_Ctype 0001
		defb	009h,0CBh,056h,028h,003h,013h,018h
		defb	0F0h,0FDh,02Bh,0FDh,0E5h,0C1h,021h
		defb	006h,000h,039h,07Eh,081h,077h,023h
		defb	07Eh,088h,077h,0DDh,04Eh,0F2h,047h
		defb	021h,026h,000h,0CDh
		defw	LWRD ?SS_CMP_L02
		defb	0DDh,07Eh,0FDh,030h,00Dh,0FEh,02Bh
		defb	0CAh
		defw	LWRD code_base+003D7h
		defb	02Eh,022h,022h
		defw	LWRD errno
		defb	0C3h
		defw	LWRD code_base+003B1h
		defb	0FEh,02Bh,020h,00Bh,0FDh,0E5h,0C1h
		defb	0DDh,06Eh,0F2h,0DDh,066h,0F3h,018h
		defb	003h,0FDh,0E5h,0E1h,0A7h,0EDh,042h
		defb	0DDh,04Eh,0F4h,0DDh,046h,0F5h,0A7h
		defb	0EDh,042h,0DDh,075h,0F2h,0DDh,074h
		defb	0F3h,0DDh,04Eh,0F0h,0DDh,046h,0F1h
		defb	0DDh,071h,0F8h,0DDh,070h,0F9h,0D5h
		defb	021h,004h,000h,039h
		defb	0E5h,0DDh,05Eh,0F2h,0DDh,056h,0F3h
		defb	0CDh
		defw	LWRD code_base+00000h
		defb	0EBh,0E1h,0CDh
		defw	LWRD ?F_MULASG_L04
		defb	0D1h,0DDh,07Eh,0F8h,0DDh,0B6h,0F9h
		defb	028h,009h,0DDh,07Eh,0F0h,0DDh,0B6h
		defb	0F1h,0CAh
		defw	LWRD code_base+003B1h
		defb	0DDh,0CBh,0F1h,07Eh,020h,01Ah,0DDh
		defb	06Eh,0F0h,0DDh,066h,0F1h,0E5h,0DDh
		defb	06Eh,0EEh,0DDh,066h,0EFh,0E5h,001h
		defb	07Fh,07Fh,021h,0FFh,0FFh,0CDh
		defw	LWRD ?SL_CMP_L03
		defb	0DAh
		defw	LWRD code_base+003D7h
		defb	0DDh,07Eh,0F8h,0DDh,0B6h,0F9h,028h
		defb	017h,021h,080h,000h,0E5h,06Ch,0E5h
		defb	0DDh,04Eh,0F0h,0DDh,046h,0F1h,0DDh
		defb	06Eh,0EEh,0DDh,066h,0EFh,0C
h,0  h
		defb	0  h,0t_h,0abh,0s_h,0vah,0r h,0  h
		defb	0  h,0  h,0  h,0  h,0 Dh
		defw	LWRD ?SL_CMP_L03
		defb	038h,05Fh,0FDh,023h,0FDh,0E5h,0E1h
		defb	07Dh,0B4h,020h,028h,0DDh,07Eh,0F4h
		defb	0DDh,0B6h,0F5h,020h,020h,0DDh,0B6h
		defb	0FCh,020h,01Bh,0DDh,07Eh,004h,0DDh
		defb	0B6h,005h,028h,060h,0DDh,06Eh,002h
		defb	0DDh,066h,003h,0E5h,0DDh,06Eh,004h
		defb	0DDh,066h,005h,0C1h,071h,023h,070h
		defb	018h,04Dh,0DDh,07Eh
		defb	004h,0DDh,0B6h,005h,028h,009h,0DDh
		defb	06Eh,004h,0DDh,066h,005h,073h,023h
		defb	072h,0DDh,07Eh,0FEh,0FEh,02Bh,0DDh
		defb	04Eh,0F0h,0DDh,046h,0F1h,0DDh,06Eh
		defb	0EEh,0DDh,066h,0EFh,028h,061h,079h
		defb	0B0h,028h,05Dh,078h,0EEh,080h,047h
		defb	018h,057h,0DDh,07Eh,004h,0DDh,0B6h
		defb	005h,028h,011h,0DDh
		defb	06Eh,002h,0DDh,066h,003h,0E5h,0DDh
		defb	06Eh,004h,0DDh,066h,005h,0C1h,071h
		defb	023h,070h,021h,022h,000h,022h
		defw	LWRD errno
		defb	001h,000h,000h,069h,060h,018h,031h
		defb	0DDh,07Eh,004h,0DDh,0B6h,005h,028h
		defb	011h,0DDh,06Eh,002h,0DDh,066h,003h
		defb	0E5h,0DDh,06Eh,004h,0DDh,066h,005h
		defb	0C1h,071h,023h,070h,021h,022h,000h
		defb	022h
		defw	LWRD errno
		defb	0DDh,07Eh,0FEh,0FEh,02Bh,020h,005h
		defb	001h,07Fh,07Fh,018h,003h,001h,07Fh
		defb	0FFh,021h,0FFh,0FFh,0FDh,0E1h,0C3h
		defw	LWRD ?BANK_LEAVE_32_L08
		defb	0DDh,04Eh,0F0h,0DDh,046h,0F1h,0DDh
		defb	06Eh,0EEh,0DDh,066h,0EFh,0C3h
		defw	LWRD ?F_MUL_L04
		endmod

; -----------------------------------------------------------------------------

	end
