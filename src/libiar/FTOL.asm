; FTOL.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?F_TO_L_L04
		rseg	RCODE
rcode_base:
		public	?F_TO_L_L04
?F_TO_L_L04	equ	rcode_base+00000000h
		extern	?L_NOT_L03
		extern	?L_INC_L03
		defb	0F5h,0D5h,0CBh,020h,0CBh,013h,0CBh
		defb	079h,028h,002h,0CBh,0C0h,0CBh,0F9h
		defb	078h,0D6h,07Fh,030h,007h,001h,000h
		defb	000h,060h,069h,018h,041h,0FEh,020h
		defb	038h,00Dh,001h,000h,080h,061h,069h
		defb	0CBh,043h,020h,034h,00Bh,02Bh,018h
		defb	030h,047h,03Eh,01Fh,090h,051h,04Ch
		defb	065h,02Eh,000h,028h
		defb	01Ah,0D6h,008h,038h,007h,06Ch,061h
		defb	04Ah,016h,000h,018h,0F5h,0C6h,008h
		defb	028h,00Bh,047h,0CBh,03Ah,0CBh,019h
		defb	0CBh,01Ch,0CBh,01Dh,010h,0F6h,042h
		defb	0CBh,043h,028h,006h,0CDh
		defw	LWRD ?L_NOT_L03
		defb	0CDh
		defw	LWRD ?L_INC_L03
		defb	0D1h,0F1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
