; vprintf.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	vprintf
		rseg	CODE
code_base:
		extern	_formatted_write
		extern	putchar
		public	vprintf
vprintf		equ	code_base+00000010h
		extern	?CL64180B_4_06_L00
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_PARM_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_PARM_DIRECT_L09
		defb	016h,000h,03Eh
		defb	BYTE3 putchar
		defb	021h
		defw	LWRD putchar
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		defb	0CDh
		defw	LWRD ?ENT_PARM_DIRECT_L09
		defb	0C5h,021h,000h,000h,0E5h,00Eh
		defb	BYTE3 code_base+0000000h
		defb	0C5h,021h
		defw	LWRD code_base+00000h
		defb	0E5h,03Eh
		defb	BYTE3 _formatted_write
		defb	021h
		defw	LWRD _formatted_write
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0F1h,0F1h,0F1h,0F1h,0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
