; SMULASG.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?S_MULASG_L02
		rseg	RCODE
rcode_base:
		public	?S_MULASG_L02
?S_MULASG_L02	equ	rcode_base+00000000h
		extern	?S_MUL_L02
		defb	05Eh,023h,056h,0CDh
		defw	LWRD ?S_MUL_L02
		defb	072h,02Bh,073h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
