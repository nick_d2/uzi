; scanf.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	scanf
		rseg	CODE
code_base:
		extern	_formatted_read
		extern	gets
		public	scanf
scanf		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	07Ch,0FFh,0FDh,0E5h,0FDh,021h,000h
		defb	000h,021h,002h,000h,039h,0E5h,021h
		defb	008h,000h,039h,04Dh,044h,0E1h,0CDh
		defw	LWRD code_base+0009Dh
		defb	07Dh,0B4h,028h,077h,021h,004h,000h
		defb	039h,0E5h,021h,092h,000h,039h,001h
		defb	002h,000h,009h,04Dh,044h,0E1h,071h
		defb	023h,070h,021h,002h,000h,039h,07Eh
		defb	023h,066h,06Fh,0AFh,0B6h,028h,01Dh
		defb	021h,004h,000h,039h,0E5h,021h,092h
		defb	000h,039h,04Dh,044h,021h,004h,000h
		defb	039h,0EBh,03Eh
		defb	BYTE3 _formatted_read
		defb	021h
		defw	LWRD _formatted_read
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0F1h,04Dh,044h,0FDh,009h,0DDh,06Eh
		defb	00Ah,0DDh,066h,00Bh,0AFh,0B6h,028h
		defb	022h,021h,002h,000h,039h,07Eh,023h
		defb	066h,06Fh,0AFh,0B6h,020h,016h,021h
		defb	002h,000h,039h,0E5h,021h,008h,000h
		defb	039h,04Dh,044h,0E1h,0CDh
		defw	LWRD code_base+0009Dh
		defb	07Dh,0B4h,020h,003h,0DDh,077h,080h
		defb	0DDh,06Eh,00Ah,0DDh,066h,00Bh,0AFh
		defb	0B6h,020h,0A1h,0FDh,0E5h,0E1h,018h
		defb	003h,021h,0FFh,0FFh,0FDh,0E1h,0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		defb	071h,023h,070h,059h,050h,03Eh
		defb	BYTE3 gets
		defb	021h
		defw	LWRD gets
		defb	0C3h
		defw	LWRD ?BANK_CALL_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
