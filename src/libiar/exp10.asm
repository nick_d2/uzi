; exp10.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	exp10
		rseg	CODE
code_base:
		extern	errno
		extern	exp
		public	exp10
exp10		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?SL_CMP_L03
		extern	?F_MUL_L04
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?BANK_LEAVE_32_L08
		extern	?ENT_PARM_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_PARM_DIRECT_L09
		defb	0CBh,0B8h,0C5h,0D5h,001h,018h,042h
		defb	021h,000h,000h,0CDh
		defw	LWRD ?SL_CMP_L03
		defb	030h,018h,02Eh,022h,022h
		defw	LWRD errno
		defb	0DDh,0CBh,005h,07Eh,028h,005h,04Ch
		defb	069h,044h,018h,023h,001h,07Fh,07Fh
		defb	021h,0FFh,0FFh,018h,01Bh,021h,013h
		defb	040h,0E5h,021h,08Eh,05Dh,0E5h,0DDh
		defb	04Eh,004h,0DDh,046h,005h,0EBh,0CDh
		defw	LWRD ?F_MUL_L04
		defb	0EBh,03Eh
		defb	BYTE3 exp
		defb	021h
		defw	LWRD exp
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0C3h
		defw	LWRD ?BANK_LEAVE_32_L08
		endmod

; -----------------------------------------------------------------------------

	end
