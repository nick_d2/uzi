; VSWITCHEND.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?V_SWITCH_END_L06
		rseg	RCODE
rcode_base:
		public	?V_SWITCH_END_L06
?V_SWITCH_END_L06	equ	rcode_base+00000000h
		defb	009h,009h,07Eh,023h,066h,06Fh,0C1h
		defb	0F1h,0E3h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
