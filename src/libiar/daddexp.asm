; daddexp.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?__daddexp_L11
		rseg	RCODE
rcode_base:
		public	?__daddexp_L11
?__daddexp_L11	equ	rcode_base+00000000h
		defb	0F5h,079h,007h,078h,017h,085h,0CBh
		defb	03Fh,0F5h,06Fh,078h,0E6h,080h,0B5h
		defb	047h,0CBh,021h,0F1h,0CBh,019h,0F1h
		defb	0C9h
		endmod

; -----------------------------------------------------------------------------

	end
