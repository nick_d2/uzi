; asin.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	asin
		rseg	CODE
code_base:
		public	asin
asin		equ	code_base+00000000h
		extern	atan
		extern	errno
		extern	sqrt
		extern	?CL64180B_4_06_L00
		extern	?SL_CMP_L03
		extern	?F_MUL_L04
		extern	?F_DIV_L04
		extern	?F_SUB_L04
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_LEAVE_32_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0FAh,0FFh,0CBh,078h,020h,004h,03Eh
		defb	001h,018h,001h,0AFh,0DDh,077h,0FEh
		defb	0DDh,06Eh,002h,0DDh,066h,003h,0CBh
		defb	0B8h,0DDh,070h,005h,0C5h,0E5h,001h
		defb	080h,03Fh,021h,000h,000h,0CDh
		defw	LWRD ?SL_CMP_L03
		defb	030h,00Eh,02Eh,021h,022h
		defw	LWRD errno
		defb	001h,07Fh,07Fh,021h,0FFh,0FFh,0C3h
		defw	LWRD code_base+000EDh
		defb	0DDh,06Eh,004h,0DDh,066h,005h,0E5h
		defb	0DDh,06Eh,002h,0DDh,066h,003h,0E5h
		defb	0DDh,04Eh,004h,0DDh,046h,005h,0CDh
		defw	LWRD ?F_MUL_L04
		defb	0C5h,0E5h,001h,080h,03Fh,021h,000h
		defb	000h,0CDh
		defw	LWRD ?F_SUB_L04
		defb	0EBh,03Eh
		defb	BYTE3 sqrt
		defb	021h
		defw	LWRD sqrt
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0DDh,075h,0FAh,0DDh,074h,0FBh,0DDh
		defb	071h,0FCh,0DDh,070h,0FDh,0DDh,06Eh
		defb	004h,0DDh,066h,005h,0E5h,0DDh,06Eh
		defb	002h,0DDh,066h,003h,0E5h,001h,033h
		defb	03Fh,069h,061h,0CDh
		defw	LWRD ?SL_CMP_L03
		defb	030h,033h,0DDh,06Eh,004h,0DDh,066h
		defb	005h,0E5h,0DDh,06Eh,002h,0DDh,066h
		defb	003h,0E5h,0DDh,04Eh,0FCh,0DDh,046h
		defb	0FDh,0DDh,06Eh,0FAh,0DDh,066h,0FBh
		defb	0CDh
		defw	LWRD ?F_DIV_L04
		defb	0EBh,03Eh
		defb	BYTE3 atan
		defb	021h
		defw	LWRD atan
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0C5h,0E5h,001h,0C9h,03Fh,021h,0DBh
		defb	00Fh,0CDh
		defw	LWRD ?F_SUB_L04
		defb	018h,026h,0DDh,06Eh,0FCh,0DDh,066h
		defb	0FDh,0E5h,0DDh,06Eh,0FAh,0DDh,066h
		defb	0FBh,0E5h,0DDh,04Eh,004h,0DDh,046h
		defb	005h,0DDh,06Eh,002h,0DDh,066h,003h
		defb	0CDh
		defw	LWRD ?F_DIV_L04
		defb	0EBh,03Eh
		defb	BYTE3 atan
		defb	021h
		defw	LWRD atan
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0AFh,0DDh,0B6h,0FEh,020h,008h,079h
		defb	0B0h,028h,004h,078h,0EEh,080h,047h
		defb	0C3h
		defw	LWRD ?BANK_LEAVE_32_L08
		endmod

; -----------------------------------------------------------------------------

	end
