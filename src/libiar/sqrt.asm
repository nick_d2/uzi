; sqrt.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	sqrt
		rseg	CODE
code_base:
		extern	errno
		extern	frexp
		public	sqrt
sqrt		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?SC_DIV_L01
		extern	?F_MUL_L04
		extern	?F_DIV_L04
		extern	?F_ADD_L04
		extern	?F_CMP_L04
		extern	?F_MULASG_L04
		extern	?F_ADDASG_L04
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_LEAVE_32_L08
		extern	?ENT_AUTO_DIRECT_L09
		extern	?__daddexp_L11
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0F6h,0FFh,0C5h,0DDh,06Eh,002h,0DDh
		defb	066h,003h,0E5h,001h,000h,000h,069h
		defb	060h,0CDh
		defw	LWRD ?F_CMP_L04
		defb	038h,017h,0DDh,07Eh,004h,0DDh,0B6h
		defb	005h,0CAh
		defw	LWRD code_base+000F9h
		defb	02Eh,021h,022h
		defw	LWRD errno
		defb	001h,07Fh,07Fh,021h,0FFh,0FFh,0C3h
		defw	LWRD code_base+000F9h
		defb	02Eh,008h,039h,0E5h,0DDh,04Eh,004h
		defb	0DDh,046h,005h,0DDh,05Eh,002h,0DDh
		defb	056h,003h,03Eh
		defb	BYTE3 frexp
		defb	021h
		defw	LWRD frexp
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0F1h,0DDh,075h,0F6h,0DDh,074h,0F7h
		defb	0DDh,071h,0F8h,0DDh,070h,0F9h,0C5h
		defb	0E5h,001h,042h,03Fh,021h,0C5h,003h
		defb	0CDh
		defw	LWRD ?F_ADD_L04
		defb	0C5h,0E5h,001h,012h,03Fh,021h,01Ah
		defb	051h,0CDh
		defw	LWRD ?F_MUL_L04
		defb	0DDh,075h,0FAh,0DDh,074h,0FBh,0DDh
		defb	071h,0FCh,0DDh,070h,0FDh,021h,004h
		defb	000h,039h,0E5h,0C5h,0DDh,04Eh,0FAh
		defb	0DDh,046h,0FBh,0C5h,0DDh,04Eh,0F8h
		defb	0DDh,046h,0F9h,0DDh,06Eh,0F6h,0DDh
		defb	066h,0F7h,0CDh
		defw	LWRD ?F_DIV_L04
		defb	0EBh,0E1h,0CDh
		defw	LWRD ?F_ADDASG_L04
		defb	0DDh,0CBh,0FEh,046h,028h,014h,021h
		defb	004h,000h,039h,001h,0B5h,03Fh,011h
		defb	0F3h,004h,0CDh
		defw	LWRD ?F_MULASG_L04
		defb	0C5h,0DDh,07Eh,0FEh,03Dh,018h,010h
		defb	0DDh,04Eh,0FCh,0DDh,046h,0FDh,0DDh
		defb	05Eh,0FAh,0DDh,056h,0FBh,0C5h,0DDh
		defb	07Eh,0FEh,006h,002h,0CDh
		defw	LWRD ?SC_DIV_L01
		defb	06Fh,0C1h,0CDh
		defw	LWRD ?__daddexp_L11
		defb	0DDh,071h,0F8h,0DDh,070h,0F9h,0C5h
		defb	0D5h,0DDh,04Eh,004h,0DDh,046h,005h
		defb	0DDh,06Eh,002h,0DDh,066h,003h,0CDh
		defw	LWRD ?F_DIV_L04
		defb	0C5h,0E5h,021h,080h,03Eh,0E5h,021h
		defb	000h,000h,0E5h,0DDh,04Eh,0F8h,0DDh
		defb	046h,0F9h,06Bh,062h,0CDh
		defw	LWRD ?F_MUL_L04
		defb	0CDh
		defw	LWRD ?F_ADD_L04
		defb	0C3h
		defw	LWRD ?BANK_LEAVE_32_L08
		endmod

; -----------------------------------------------------------------------------

	end
