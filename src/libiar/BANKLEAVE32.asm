; BANKLEAVE32.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?BANK_LEAVE_32_L08
		rseg	RCODE
rcode_base:
		public	?BANK_LEAVE_32_L08
?BANK_LEAVE_32_L08	equ	rcode_base+00000000h
		defb	0DDh,0F9h,0DDh,0E1h,0D1h,033h,033h
		defb	0F1h,0EDh,039h,039h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
