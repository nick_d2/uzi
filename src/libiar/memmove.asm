; memmove.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	memmove
		rseg	CODE
code_base:
		public	memmove
memmove		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	000h,000h,0FDh,0E5h,0DDh,05Eh,00Ah
		defb	0DDh,056h,00Bh,0DDh,06Eh,002h,0DDh
		defb	066h,003h,0E5h,0FDh,0E1h,0A7h,0EDh
		defb	042h,038h,050h,069h,060h,019h,0DDh
		defb	04Eh,002h,0DDh,046h,003h,0A7h,0EDh
		defb	042h,038h,042h,021h,004h,000h,039h
		defb	04Bh,042h,07Eh,081h,077h,023h,07Eh
		defb	088h,077h,023h,07Eh
		defb	081h,077h,023h,07Eh,088h,077h,07Bh
		defb	062h,01Bh,0B4h,028h,056h,021h,006h
		defb	000h,039h,07Eh,0C6h,0FFh,077h,023h
		defb	07Eh,0CEh,0FFh,077h,02Bh,02Bh,02Bh
		defb	07Eh,0C6h,0FFh,077h,023h,07Eh,0CEh
		defb	0FFh,077h,0DDh,06Eh,004h,0DDh,066h
		defb	005h,046h,0DDh,06Eh,002h,067h,070h
		defb	018h,0D3h,07Bh,062h
		defb	01Bh,0B4h,028h,029h,0DDh,06Eh,004h
		defb	0DDh,066h,005h,046h,0DDh,06Eh,002h
		defb	0DDh,066h,003h,070h,021h,006h,000h
		defb	039h,07Eh,0C6h,001h,077h,023h,07Eh
		defb	0CEh,000h,077h,02Bh,02Bh,02Bh,07Eh
		defb	0C6h,001h,077h,023h,07Eh,0CEh,000h
		defb	077h,018h,0D1h,0FDh,0E5h,0E1h,0FDh
		defb	0E1h,0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
