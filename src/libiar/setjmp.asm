; setjmp.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	setjmp
		rseg	CODE
code_base:
		public	setjmp
setjmp		equ	code_base+00000000h
		extern	?BANK_FAST_LEAVE_L08
		defb	021h,004h,000h,039h,0EBh,073h,023h
		defb	072h,023h,071h,023h,070h,023h,0DDh
		defb	0E5h,0D1h,073h,023h,072h,023h,0FDh
		defb	0E5h,0D1h,073h,023h,072h,023h,0F1h
		defb	0D1h,0D5h,0F5h,073h,023h,072h,023h
		defb	077h,021h,000h,000h,0C3h
		defw	LWRD ?BANK_FAST_LEAVE_L08
		endmod

; -----------------------------------------------------------------------------

	end
