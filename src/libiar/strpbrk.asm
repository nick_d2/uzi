; strpbrk.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	strpbrk
		rseg	CODE
code_base:
		public	strpbrk
strpbrk		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	000h,000h,0FDh,0E5h,0DDh,05Eh,002h
		defb	0DDh,056h,003h,06Bh,062h,0AFh,0B6h
		defb	028h,01Eh,0DDh,06Eh,004h,0DDh,066h
		defb	005h,0E5h,0FDh,0E1h,0AFh,0FDh,0B6h
		defb	000h,028h,00Ch,06Bh,062h,07Eh,0FDh
		defb	0BEh,000h,0FDh,023h,020h,0F0h,018h
		defb	005h,013h,018h,0DCh,06Fh,067h,0FDh
		defb	0E1h,0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
