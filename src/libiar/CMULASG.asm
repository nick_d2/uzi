; CMULASG.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?C_MULASG_L01
		rseg	RCODE
rcode_base:
		public	?C_MULASG_L01
?C_MULASG_L01	equ	rcode_base+00000000h
		extern	?C_MUL_L01
		defb	0C5h,047h,07Eh,0CDh
		defw	LWRD ?C_MUL_L01
		defb	077h,0C1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
