; MONITOR.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?MONITOR
		rseg	RCODE
rcode_base:
		rseg	DATA0
data0_base:
		public	?MONITOR
?MONITOR	equ	data0_base+00000000h
		public	?C_MON_BREAK
?C_MON_BREAK	equ	data0_base+0000002Fh
		public	?DBG_2
?DBG_2		equ	00000000h
    t_symbol_def         PUBLIC_REL (0001) 00000000 '?C_MON_CURR_ADDR'
		public	?C_MON_ADDR_SIZE
?C_MON_ADDR_SIZE	equ	00000003h
		defb	0E3h,022h
    t_push_rel           (0001) 00000003
		defb	02Bh,02Bh,02Bh,022h
    t_push_rel           (0001) 00000000
		defb	0C5h,0F5h,0EDh,038h,03Ah,047h,0E6h
		defb	0F0h,04Fh,07Ch,0B9h,00Eh,000h,030h
		defb	00Eh,0CBh,020h,0CBh,020h,0CBh,020h
		defb	0CBh,020h,0B8h,038h,003h,0EDh,008h
		defb	039h,079h,032h
    t_push_rel           (0001) 00000002
		defb	0F1h,0C1h,0E1h,000h,000h,0E5h,02Ah
    t_push_rel           (0001) 00000003
		defb	0E3h,0C9h
    t_org_rel            (01) 00000000
    t_org_rel            (01) 00000003
    t_org_rel            (01) 00000005
		endmod

; -----------------------------------------------------------------------------

	end
