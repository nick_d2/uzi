; LVSWITCH.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?L_V_SWITCH_L06
		rseg	RCODE
rcode_base:
		public	?L_V_SWITCH_L06
?L_V_SWITCH_L06	equ	rcode_base+00000000h
		extern	?V_SWITCH_END_L06
		defb	0E3h,0F5h,0C5h,0DDh,0E5h,0DDh,021h
		defb	000h,000h,0DDh,039h,04Eh,023h,046h
		defb	018h,002h,023h,023h,023h,078h,0B1h
		defb	028h,01Bh,00Bh,07Bh,096h,023h,020h
		defb	0F3h,07Ah,096h,020h,0EFh,023h,0DDh
		defb	07Eh,002h,096h,023h,020h,0E9h,0DDh
		defb	07Eh,003h,096h,020h,0E3h,023h,023h
		defb	023h,009h,009h,009h
		defb	009h,0DDh,0E1h,0C3h
		defw	LWRD ?V_SWITCH_END_L06
		endmod

; -----------------------------------------------------------------------------

	end
