; SCDIV.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?SC_DIV_L01
		rseg	RCODE
rcode_base:
		public	?SC_DIV_L01
?SC_DIV_L01	equ	rcode_base+00000000h
		extern	?C_FIND_SIGN_L01
		extern	?C_DIVMOD_L01
		defb	0C5h,0D5h,04Fh,0AFh,0CDh
		defw	LWRD ?C_FIND_SIGN_L01
		defb	0F5h,0CDh
		defw	LWRD ?C_DIVMOD_L01
		defb	0F1h,0B7h,079h,028h,003h,03Ch,018h
		defb	002h,0EEh,0FFh,0D1h,0C1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
