; longjmp.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	longjmp
		rseg	CODE
code_base:
		public	longjmp
longjmp		equ	code_base+00000000h
		extern	?BANK_FAST_LEAVE_L08
		defb	078h,0B1h,020h,001h,003h,0EBh,05Eh
		defb	023h,056h,023h,0EBh,0F9h,0EBh,0C5h
		defb	04Eh,023h,046h,023h,05Eh,023h,056h
		defb	023h,0D5h,0DDh,0E1h,05Eh,023h,056h
		defb	023h,0D5h,0FDh,0E1h,05Eh,023h,056h
		defb	023h,07Eh,0E1h,0D5h,0F5h,0C3h
		defw	LWRD ?BANK_FAST_LEAVE_L08
		endmod

; -----------------------------------------------------------------------------

	end
