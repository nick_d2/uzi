; mediumread.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	_medium_read
		rseg	CODE
code_base:
		extern	_Small_Ctype
		public	_medium_read
_medium_read	equ	code_base+00000168h
		extern	?CL64180B_4_06_L00
		extern	?SS_CMP_L02
		extern	?L_MUL_L03
		extern	?L_NEGASG_L03
		extern	?C_V_SWITCH_L06
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?ENT_PARM_DIRECT_L09
		extern	?ENT_AUTO_DIRECT_L09
		extern	?LEAVE_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	000h,000h,011h,000h,000h,0DDh,06Eh
		defb	002h,0DDh,066h,003h,07Eh,023h,066h
		defb	06Fh,04Eh,006h,000h,021h
		defw	LWRD _Small_Ctype 0001
		defb	009h,0CBh,056h,028h,048h,0DDh,06Eh
		defb	004h,0DDh,066h,005h,07Eh,023h,0B6h
		defb	028h,03Dh,0DDh,06Eh,002h,0DDh,066h
		defb	003h,04Eh,023h,046h,003h,070h,02Bh
		defb	071h,00Bh,00Ah,04Fh,006h,000h,0C5h
		defb	0EBh,029h,04Dh,044h,029h,029h,009h
		defb	0C1h,009h,001h,0D0h,0FFh,009h,0EBh
		defb	04Bh,042h,021h,0E8h
		defb	003h,0CDh
		defw	LWRD ?SS_CMP_L02
		defb	030h,003h,011h,0E8h,003h,0DDh,06Eh
		defb	004h,0DDh,066h,005h,04Eh,023h,046h
		defb	00Bh,070h,02Bh,071h,018h,0A3h,0EBh
		defb	0C3h
		defw	LWRD ?LEAVE_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	000h,000h,0DDh,06Eh,002h,0DDh,066h
		defb	003h,07Eh,023h,066h,06Fh,05Eh,021h
		defw	LWRD _Small_Ctype 0001
		defb	04Bh,006h,000h,009h,0CBh,05Eh,028h
		defb	011h,0DDh,06Eh,002h,0DDh,066h,003h
		defb	04Eh,023h,046h,003h,070h,02Bh,071h
		defb	00Ah,05Fh,018h,0E4h,07Bh,0C3h
		defw	LWRD ?LEAVE_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_PARM_DIRECT_L09
		defb	021h
		defw	LWRD _Small_Ctype 0001
		defb	016h,000h,019h,0CBh,04Eh,07Bh,028h
		defb	002h,0E6h,05Fh,0DDh,077h,002h,0FEh
		defb	041h,038h,00Eh,03Eh,05Ah,0DDh,0BEh
		defb	002h,038h,007h,0DDh,07Eh,002h,0C6h
		defb	0C9h,018h,00Fh,021h
		defw	LWRD _Small_Ctype 0001
		defb	0DDh,04Eh,002h,042h,009h,0CBh,056h
		defb	028h,008h,079h,0D6h,030h,0DDh,0BEh
		defb	004h,038h,002h,03Eh,064h,0C3h
		defw	LWRD ?LEAVE_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0FEh,0FFh,0DDh,05Eh,002h,0DDh,056h
		defb	003h,0CDh
		defw	LWRD code_base+00069h
		defb	0DDh,077h,0FEh,0FEh,02Bh,028h,004h
		defb	0FEh,02Dh,020h,038h,0DDh,06Eh,008h
		defb	0DDh,066h,009h,07Eh,023h,0B6h,028h
		defb	02Dh,0DDh,06Eh,008h,0DDh,066h,009h
		defb	04Eh,023h,046h,00Bh,070h,02Bh,071h
		defb	0DDh,07Eh,0FEh,0FEh,02Dh,020h,008h
		defb	0DDh,06Eh,004h,0DDh,066h,005h,036h
		defb	001h,0DDh,06Eh,002h
		defb	0DDh,066h,003h,04Eh,023h,046h,003h
		defb	070h,02Bh,071h,00Ah,0DDh,077h,0FEh
		defb	0DDh,07Eh,0FEh,0C3h
		defw	LWRD ?LEAVE_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_PARM_DIRECT_L09
		defb	0FDh,0E5h,0DDh,06Eh,008h,0DDh,066h
		defb	009h,0E5h,0FDh,0E1h,0FDh,07Eh,000h
		defb	0DDh,0BEh,002h,020h,005h,0DDh,07Eh
		defb	004h,018h,01Ah,0FDh,07Eh,001h,0B7h
		defb	0FDh,023h,028h,007h,0FDh,07Eh,000h
		defb	0FEh,05Dh,020h,0E4h,0AFh,0DDh,0B6h
		defb	004h,020h,004h,03Eh,001h,018h,001h
		defb	0AFh,0FDh,0E1h,0C3h
		defw	LWRD ?LEAVE_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0ECh,0FFh,0FDh,0E5h,0DDh,06Eh,002h
		defb	0DDh,066h,003h,0E5h,0FDh,0E1h,0FDh
		defb	06Eh,000h,0DDh,075h,0FCh,0FDh,066h
		defb	001h,0DDh,074h,0FDh,0AFh,0DDh,077h
		defb	0F2h,0DDh,077h,0F3h,0DDh,06Eh,004h
		defb	0DDh,066h,005h,07Eh,023h,066h,06Fh
		defb	046h,004h,005h,0CAh
		defw	LWRD code_base+00747h
		defb	021h
		defw	LWRD _Small_Ctype 0001
		defb	048h,006h,000h,009h,0CBh,05Eh,028h
		defb	026h,0FDh,06Eh,000h,0FDh,066h,001h
		defb	04Eh,021h
		defw	LWRD _Small_Ctype 0001
		defb	009h,0CBh,05Eh,028h,00Ah,0FDh,0E5h
		defb	0E1h,034h,023h,020h,0EAh,034h,018h
		defb	0E7h,0DDh,06Eh,004h,0DDh,066h,005h
		defb	034h,023h,020h,0C2h,034h,018h,0BFh
		defb	0DDh,06Eh,004h,0DDh,066h,005h,07Eh
		defb	023h,066h,06Fh,07Eh,0FEh,025h,020h
		defb	010h,0DDh,06Eh,004h,0DDh,066h,005h
		defb	046h,023h,066h,068h
		defb	023h,07Eh,0FEh,06Eh,028h,00Bh,0FDh
		defb	06Eh,000h,0FDh,066h,001h,0AFh,0B6h
		defb	0CAh
		defw	LWRD code_base+0071Bh
		defb	079h,0FEh,025h,028h,020h,0DDh,06Eh
		defb	004h,0DDh,066h,005h,034h,023h,020h
		defb	001h,034h,0FDh,0E5h,0D1h,0CDh
		defw	LWRD code_base+00069h
		defb	0B9h,0C2h
		defw	LWRD code_base+00747h
		defb	0FDh,0E5h,0E1h,034h,023h,020h,001h
		defb	034h,0C3h
		defw	LWRD code_base+0018Bh
		defb	0DDh,036h,0FEh,000h,0DDh,06Eh,004h
		defb	0DDh,066h,005h,04Eh,023h,046h,003h
		defb	070h,02Bh,071h,00Ah,0DDh,077h,0EDh
		defb	0FEh,02Ah,020h,017h,0DDh,06Eh,004h
		defb	0DDh,066h,005h,04Eh,023h,046h,003h
		defb	070h,02Bh,071h,00Ah,0DDh,077h,0EDh
		defb	0DDh,0CBh,0ECh,086h,018h,004h,0DDh
		defb	0CBh,0ECh,0C6h,021h
		defw	LWRD _Small_Ctype 0001
		defb	04Fh,006h,000h,009h,0CBh,056h,028h
		defb	02Ch,0DDh,070h,0FAh,0DDh,036h,0FBh
		defb	002h,021h,010h,000h,039h,04Dh,044h
		defb	0DDh,05Eh,004h,0DDh,056h,005h,0CDh
		defw	LWRD code_base+00000h
		defb	0DDh,075h,0EEh,0DDh,074h,0EFh,0DDh
		defb	06Eh,004h,0DDh,066h,005h,07Eh,023h
		defb	066h,06Fh,046h,0DDh,070h,0EDh,018h
		defb	007h,0DDh,070h,0EEh,0DDh,036h,0EFh
		defb	002h,0DDh,07Eh,0ECh,0E6h,0F3h,0DDh
		defb	077h,0ECh,0DDh,07Eh,0EDh,0FEh,06Ch
		defb	028h,004h,0FEh,04Ch,020h,006h,0DDh
		defb	0CBh,0ECh,0DEh,018h
		defb	00Bh,0FEh,068h,020h,015h,0AFh,028h
		defb	004h,0DDh,0CBh,0ECh,0D6h,0DDh,06Eh
		defb	004h,0DDh,066h,005h,04Eh,023h,046h
		defb	003h,070h,02Bh,071h,00Ah,0DDh,06Eh
		defb	004h,0DDh,066h,005h,034h,023h,020h
		defb	001h,034h,05Fh,0CDh
		defw	LWRD ?C_V_SWITCH_L06
		defb	00Dh,000h,025h,058h,05Bh,063h,064h
		defb	068h,069h,06Eh,06Fh,070h,073h,075h
		defb	078h
		defw	LWRD code_base+00747h
		defw	LWRD code_base+00300h
		defw	LWRD code_base+003ACh
		defw	LWRD code_base+0064Ah
		defw	LWRD code_base+0039Bh
		defw	LWRD code_base+002F9h
		defw	LWRD code_base+006DBh
		defw	LWRD code_base+00306h
		defw	LWRD code_base+003ACh
		defw	LWRD code_base+003ACh
		defw	LWRD code_base+005C2h
		defw	LWRD code_base+004B7h
		defw	LWRD code_base+00300h
		defw	LWRD code_base+0070Ah
		defb	0DDh,036h,0F0h,008h,0C3h
		defw	LWRD code_base+003B0h
		defb	0DDh,036h,0F0h,010h,018h,004h,0DDh
		defb	036h,0F0h,00Ah,0DDh,036h,0F1h,000h
		defb	021h,004h,000h,039h,0E5h,021h,016h
		defb	000h,039h,04Dh,044h,0FDh,0E5h,0D1h
		defb	0CDh
		defw	LWRD code_base+000D7h
		defb	0E1h,0DDh,077h,0EDh,0FEh,030h,020h
		defb	072h,0DDh,07Eh,0EEh,0DDh,0B6h,0EFh
		defb	028h,06Ah,0FDh,06Eh,000h,0FDh,066h
		defb	001h,023h,04Eh,006h,000h,021h
		defw	LWRD _Small_Ctype 0001
		defb	009h,0CBh,046h,0FDh,06Eh,000h,0FDh
		defb	066h,001h,023h,028h,006h,046h,0CBh
		defb	0E8h,078h,018h,001h,07Eh,0FEh,078h
		defb	020h,034h,0DDh,04Eh,0EEh,0DDh,046h
		defb	0EFh,021h,001h,000h,0CDh
		defw	LWRD ?SS_CMP_L02
		defb	030h,026h,0FDh,0E5h,0E1h,07Eh,0C6h
		defb	002h,077h,023h,07Eh,0CEh,000h,077h
		defb	02Bh,06Eh,067h,046h,0DDh,070h,0EDh
		defb	021h,004h,000h,039h,07Eh,0D6h,002h
		defb	077h,023h,07Eh,0DEh,000h,077h,0DDh
		defb	036h,0F0h,010h,018h,00Eh,03Eh,00Ah
		defb	0DDh,0AEh,0F0h,0DDh,0B6h,0F1h,020h
		defb	038h,0DDh,036h,0F0h
		defb	008h,0DDh,036h,0F1h,000h,018h,02Eh
		defb	0DDh,036h,0F0h,010h,0DDh,036h,0F1h
		defb	000h,0AFh,028h,00Eh,0DDh,0CBh,0ECh
		defb	0DEh,018h,008h,0DDh,036h,0F0h,00Ah
		defb	0DDh,036h,0F1h,000h,021h,004h,000h
		defb	039h,0E5h,021h,016h,000h,039h,04Dh
		defb	044h,0FDh,0E5h,0D1h,0CDh
		defw	LWRD code_base+000D7h
		defb	0E1h,0DDh,077h,0EDh,0DDh,07Eh,0EEh
		defb	0DDh,0B6h,0EFh,028h,00Dh,0DDh,04Eh
		defb	0F0h,0DDh,05Eh,0EDh,0CDh
		defw	LWRD code_base+00099h
		defb	0FEh,064h,020h,003h,0C3h
		defw	LWRD code_base+0071Bh
		defb	0AFh,0DDh,077h,0F6h,0DDh,077h,0F7h
		defb	0DDh,077h,0F8h,0DDh,077h,0F9h,0DDh
		defb	04Eh,0F0h,0FDh,06Eh,000h,0FDh,066h
		defb	001h,05Eh,0CDh
		defw	LWRD code_base+00099h
		defb	04Fh,0DDh,071h,0FAh,0DDh,036h,0FBh
		defb	000h,03Eh,064h,0A9h,028h,05Ah,0DDh
		defb	06Eh,0EEh,0DDh,066h,0EFh,02Bh,0DDh
		defb	075h,0EEh,0DDh,074h,0EFh,023h,07Dh
		defb	0B4h,028h,048h,0DDh,06Eh,0F0h,0DDh
		defb	066h,0F1h,07Ch,007h,09Fh,04Fh,041h
		defb	0C5h,0E5h,0DDh,04Eh,0F8h,0DDh,046h
		defb	0F9h,0DDh,06Eh,0F6h
		defb	0DDh,066h,0F7h,0CDh
		defw	LWRD ?L_MUL_L03
		defb	0C5h,0E5h,0DDh,06Eh,0FAh,0DDh,066h
		defb	0FBh,07Ch,007h,09Fh,05Fh,053h,0C1h
		defb	009h,0EBh,0C1h,0EDh,04Ah,04Dh,044h
		defb	0EBh,0DDh,075h,0F6h,0DDh,074h,0F7h
		defb	0DDh,071h,0F8h,0DDh,070h,0F9h,0FDh
		defb	0E5h,0E1h,034h,023h,020h,08Fh,034h
		defb	018h,08Ch,0DDh,0CBh,0ECh,046h,0CAh
		defw	LWRD code_base+0018Bh
		defb	0DDh,0B6h,0FEh,028h,007h,021h,00Ch
		defb	000h,039h,0CDh
		defw	LWRD ?L_NEGASG_L03
		defb	0DDh,0CBh,0ECh,05Eh,0DDh,06Eh,00Ah
		defb	0DDh,066h,00Bh,04Eh,059h,023h,046h
		defb	050h,013h,013h,072h,02Bh,073h,069h
		defb	060h,07Eh,023h,066h,06Fh,028h,012h
		defb	0DDh,04Eh,0F8h,0DDh,046h,0F9h,0DDh
		defb	05Eh,0F6h,073h,0DDh,056h,0F7h,023h
		defb	072h,023h,018h,006h,0DDh,04Eh,0F6h
		defb	0DDh,046h,0F7h,071h
		defb	023h,070h,0DDh,034h,0F2h,020h,003h
		defb	0DDh,034h,0F3h,0C3h
		defw	LWRD code_base+0018Bh
		defb	0DDh,0CBh,0ECh,046h,028h,019h,0DDh
		defb	06Eh,00Ah,0DDh,066h,00Bh,04Eh,059h
		defb	023h,046h,050h,013h,013h,072h,02Bh
		defb	073h,00Ah,0DDh,077h,0F4h,003h,00Ah
		defb	0DDh,077h,0F5h,0DDh,06Eh,004h,0DDh
		defb	066h,005h,07Eh,023h,066h,06Fh,046h
		defb	0DDh,070h,0EDh,078h,0FEh,05Eh,020h
		defb	017h,0DDh,036h,0FEh
		defb	000h,0DDh,06Eh,004h,0DDh,066h,005h
		defb	04Eh,023h,046h,003h,070h,02Bh,071h
		defb	00Ah,0DDh,077h,0EDh,018h,004h,0DDh
		defb	036h,0FEh,001h,0AFh,0DDh,0B6h,0EDh
		defb	0CAh
		defw	LWRD code_base+006C2h
		defb	0DDh,0CBh,0ECh,0CEh,0DDh,06Eh,0EEh
		defb	0DDh,066h,0EFh,02Bh,0DDh,075h,0EEh
		defb	0DDh,074h,0EFh,023h,07Dh,0B4h,028h
		defb	063h,0FDh,06Eh,000h,0FDh,066h,001h
		defb	0AFh,0B6h,028h,059h,0DDh,06Eh,004h
		defb	0DDh,066h,005h,04Eh,023h,046h,0C5h
		defb	0DDh,04Eh,0FEh,0FDh,06Eh,000h,0FDh
		defb	066h,001h,05Eh,0CDh
		defw	LWRD code_base+0012Eh
		defb	0E1h,0B7h,028h,03Eh,0DDh,0CBh,0ECh
		defb	04Eh,028h,012h,0DDh,0CBh,0ECh,046h
		defb	028h,008h,0DDh,034h,0F2h,020h,003h
		defb	0DDh,034h,0F3h,0DDh,0CBh,0ECh,08Eh
		defb	0DDh,0CBh,0ECh,046h,028h,016h,0FDh
		defb	06Eh,000h,0FDh,066h,001h,046h,0DDh
		defb	06Eh,0F4h,0DDh,066h,0F5h,023h,0DDh
		defb	075h,0F4h,0DDh,074h
		defb	0F5h,02Bh,070h,0FDh,0E5h,0E1h,034h
		defb	023h,020h,08Eh,034h,018h,08Bh,0DDh
		defb	0CBh,0ECh,04Eh,020h,011h,0DDh,0CBh
		defb	0ECh,046h,028h,00Bh,0DDh,06Eh,0F4h
		defb	0DDh,066h,0F5h,077h,0DDh,0CBh,0ECh
		defb	0CEh,0DDh,0CBh,0ECh,086h,0DDh,06Eh
		defb	004h,0DDh,066h,005h,04Eh,023h,046h
		defb	003h,070h,02Bh,071h
		defb	00Ah,0B7h,028h,00Fh,0FEh,05Dh,020h
		defb	0EBh,0DDh,06Eh,004h,0DDh,066h,005h
		defb	034h,023h,020h,001h,034h,0C3h
		defw	LWRD code_base+006C2h
		defb	0DDh,0CBh,0ECh,046h,028h,019h,0DDh
		defb	06Eh,00Ah,0DDh,066h,00Bh,04Eh,059h
		defb	023h,046h,050h,013h,013h,072h,02Bh
		defb	073h,00Ah,0DDh,077h,0F4h,003h,00Ah
		defb	0DDh,077h,0F5h,03Eh,002h,0DDh,0AEh
		defb	0EFh,0DDh,0B6h,0EEh,020h,007h,0DDh
		defb	036h,0EEh,001h,0DDh,077h,0EFh,0DDh
		defb	06Eh,0EEh,0DDh,066h
		defb	0EFh,02Bh,0DDh,075h,0EEh,0DDh,074h
		defb	0EFh,023h,07Dh,0B4h,028h,030h,0FDh
		defb	06Eh,000h,0FDh,066h,001h,0AFh,0B6h
		defb	028h,026h,0DDh,0CBh,0ECh,046h,028h
		defb	016h,0FDh,06Eh,000h,0FDh,066h,001h
		defb	046h,0DDh,06Eh,0F4h,0DDh,066h,0F5h
		defb	023h,0DDh,075h,0F4h,0DDh,074h,0F5h
		defb	02Bh,070h,0FDh,0E5h
		defb	0E1h,034h,023h,020h,0C1h,034h,018h
		defb	0BEh,0DDh,07Eh,0ECh,0E6h,001h,04Fh
		defb	006h,000h,021h,008h,000h,039h,07Eh
		defb	081h,077h,023h,07Eh,088h,077h,0C3h
		defw	LWRD code_base+0018Bh
		defb	0DDh,0CBh,0ECh,046h,028h,019h,0DDh
		defb	06Eh,00Ah,0DDh,066h,00Bh,04Eh,059h
		defb	023h,046h,050h,013h,013h,072h,02Bh
		defb	073h,00Ah,0DDh,077h,0F4h,003h,00Ah
		defb	0DDh,077h,0F5h,0FDh,0E5h,0D1h,0CDh
		defw	LWRD code_base+00069h
		defb	0DDh,06Eh,0EEh,0DDh,066h,0EFh,02Bh
		defb	0DDh,075h,0EEh,0DDh,074h,0EFh,023h
		defb	07Dh,0B4h,028h,041h,0FDh,06Eh,000h
		defb	0FDh,066h,001h,0AFh,0B6h,028h,037h
		defb	0FDh,06Eh,000h,0FDh,066h,001h,04Eh
		defb	006h,000h,021h
		defw	LWRD _Small_Ctype 0001
		defb	009h,0CBh,05Eh,020h,026h,0DDh,0CBh
		defb	0ECh,046h,028h,016h,0FDh,06Eh,000h
		defb	0FDh,066h,001h,046h,0DDh,06Eh,0F4h
		defb	0DDh,066h,0F5h,023h,0DDh,075h,0F4h
		defb	0DDh,074h,0F5h,02Bh,070h,0FDh,0E5h
		defb	0E1h,034h,023h,020h,0B0h,034h,018h
		defb	0ADh,0DDh,0CBh,0ECh,046h,028h,010h
		defb	0DDh,034h,0F2h,020h
		defb	003h,0DDh,034h,0F3h,0DDh,06Eh,0F4h
		defb	0DDh,066h,0F5h,036h,000h,0C3h
		defw	LWRD code_base+0018Bh
		defb	0DDh,06Eh,00Ah,0DDh,066h,00Bh,04Eh
		defb	059h,023h,046h,050h,013h,013h,072h
		defb	02Bh,073h,069h,060h,07Eh,023h,066h
		defb	06Fh,0E5h,0DDh,04Eh,0FCh,0DDh,046h
		defb	0FDh,0FDh,06Eh,000h,0FDh,066h,001h
		defb	0A7h,0EDh,042h,04Dh,044h,0E1h,071h
		defb	023h,070h,0C3h
		defw	LWRD code_base+0018Bh
		defb	0FDh,0E5h,0E1h,04Eh,023h,046h,003h
		defb	070h,02Bh,071h,00Bh,00Ah,0FEh,025h
		defb	0CAh
		defw	LWRD code_base+0018Bh
		defb	0FDh,06Eh,000h,0FDh,066h,001h,0AFh
		defb	0B6h,020h,022h,0DDh,06Eh,004h,0DDh
		defb	066h,005h,07Eh,023h,066h,06Fh,04Eh
		defb	006h,000h,021h
		defw	LWRD _Small_Ctype 0001
		defb	009h,0CBh,05Eh,028h,00Dh,0DDh,06Eh
		defb	004h,0DDh,066h,005h,034h,023h,020h
		defb	0E1h,034h,018h,0DEh,0DDh,06Eh,0F2h
		defb	0DDh,066h,0F3h,0FDh,0E1h,0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
