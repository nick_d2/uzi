; LFINDSIGN.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?L_FIND_SIGN_L03
		rseg	RCODE
rcode_base:
		public	?L_FIND_SIGN_L03
?L_FIND_SIGN_L03	equ	rcode_base+00000000h
		extern	?L_NOT_L03
		extern	?L_INC_L03
		defb	0CBh,07Ah,028h,018h,042h,04Bh,0DDh
		defb	066h,007h,0DDh,06Eh,006h,0CDh
		defw	LWRD ?L_NOT_L03
		defb	0CDh
		defw	LWRD ?L_INC_L03
		defb	050h,059h,0DDh,074h,007h,0DDh,075h
		defb	006h,03Eh,001h,0DDh,0CBh,0FFh,07Eh
		defb	0C8h,0F5h,0DDh,046h,0FFh,0DDh,04Eh
		defb	0FEh,0DDh,066h,0FDh,0DDh,06Eh,0FCh
		defb	0CDh
		defw	LWRD ?L_NOT_L03
		defb	0CDh
		defw	LWRD ?L_INC_L03
		defb	0DDh,070h,0FFh,0DDh,071h,0FEh,0DDh
		defb	074h,0FDh,0DDh,075h,0FCh,0F1h,0EEh
		defb	001h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
