; CLSHASG.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?C_LSHASG_L01
		rseg	RCODE
rcode_base:
		public	?C_LSHASG_L01
?C_LSHASG_L01	equ	rcode_base+00000000h
		extern	?C_LSH_L01
		defb	07Eh,0CDh
		defw	LWRD ?C_LSH_L01
		defb	077h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
