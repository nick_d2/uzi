; LEAVE.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?LEAVE_L09
		public	?LEAVE_L09
?LEAVE_L09	equ	00000020h
    t_org_abs            S86=0000 00000000
    t_org_abs            S86=0000 00000020
		defb	0DDh,0F9h,0DDh,0E1h,0D1h,0C1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
