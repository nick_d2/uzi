; tanh.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	tanh
		rseg	CODE
code_base:
		extern	exp
		public	tanh
tanh		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?SL_CMP_L03
		extern	?F_MUL_L04
		extern	?F_DIV_L04
		extern	?F_ADD_L04
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?BANK_LEAVE_32_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0FCh,0FFh,0CBh,078h,028h,004h,03Eh
		defb	001h,018h,001h,0AFh,05Fh,0DDh,06Eh
		defb	002h,0DDh,066h,003h,0CBh,0B8h,0DDh
		defb	075h,0FCh,0DDh,074h,0FDh,0DDh,070h
		defb	0FFh,021h,080h,000h,0E5h,06Ch,0E5h
		defb	0DDh,06Eh,0FCh,0DDh,066h,0FDh,0CDh
		defw	LWRD ?SL_CMP_L03
		defb	030h,010h,0AFh,0DDh,077h,002h,0DDh
		defb	077h,003h,0DDh,077h,004h,0DDh,077h
		defb	005h,0C3h
		defw	LWRD code_base+000C4h
		defb	021h,005h,041h,0E5h,021h,0B8h,01Eh
		defb	0E5h,0DDh,046h,005h,0DDh,06Eh,002h
		defb	0DDh,066h,003h,0CDh
		defw	LWRD ?SL_CMP_L03
		defb	030h,05Eh,0D5h,021h,000h,040h,0E5h
		defb	065h,0E5h,0DDh,046h,0FFh,0DDh,06Eh
		defb	0FCh,0DDh,066h,0FDh,0CDh
		defw	LWRD ?F_MUL_L04
		defb	0EBh,03Eh
		defb	BYTE3 exp
		defb	021h
		defw	LWRD exp
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0DDh,075h,002h,0DDh,074h,003h,0DDh
		defb	071h,004h,0DDh,070h,005h,0D1h,0C5h
		defb	0E5h,001h,080h,03Fh,021h,000h,000h
		defb	0CDh
		defw	LWRD ?F_ADD_L04
		defb	0C5h,0E5h,021h,080h,0BFh,0E5h,021h
		defb	000h,000h,0E5h,0DDh,04Eh,004h,0DDh
		defb	046h,005h,0DDh,06Eh,002h,0DDh,066h
		defb	003h,0CDh
		defw	LWRD ?F_ADD_L04
		defb	0CDh
		defw	LWRD ?F_DIV_L04
		defb	0DDh,075h,002h,0DDh,074h,003h,0DDh
		defb	071h,004h,0DDh,070h,005h,018h,00Fh
		defb	0AFh,0DDh,077h,002h,0DDh,077h,003h
		defb	0DDh,036h,004h,080h,0DDh,036h,005h
		defb	03Fh,01Ch,01Dh,0DDh,04Eh,004h,0DDh
		defb	046h,005h,0DDh,06Eh,002h,0DDh,066h
		defb	003h,028h,008h,079h,0B0h,028h,004h
		defb	078h,0EEh,080h,047h
		defb	0C3h
		defw	LWRD ?BANK_LEAVE_32_L08
		endmod

; -----------------------------------------------------------------------------

	end
