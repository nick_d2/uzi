; strerror.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	strerror
		rseg	CODE
code_base:
		rseg	CSTR
cstr_base:
		rseg	IDATA0
idata0_base:
		rseg	CDATA0
cdata0_base:
		public	strerror
strerror	equ	cdata0_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?SS_DIV_L02
		extern	?SS_MOD_L02
		extern	?SS_CMP_L02
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_PARM_DIRECT_L09
		defb	0C5h,0DDh,0E5h,0D5h,0DDh,0E1h,07Bh
		defb	0B2h,020h,005h,021h
    t_rel_p16            (01) 0000
		defb	018h,04Bh,03Eh,021h,0ABh,0B2h,020h
		defb	005h,021h
    t_rel_p16            (01) 0009
		defb	018h,040h,03Eh,022h,0ABh,0B2h,020h
		defb	005h,021h
    t_rel_p16            (01) 0016
		defb	018h,035h,0DDh,0E5h,0E1h,0CBh,07Ch
		defb	020h,00Bh,0DDh,0E5h,0C1h,021h,063h
		defb	000h,0CDh
		defw	LWRD ?SS_CMP_L02
		defb	030h,005h,021h
    t_rel_p16            (01) 0022
		defb	018h,01Eh,001h,00Ah,000h,0CDh
		defw	LWRD ?SS_MOD_L02
		defb	07Bh,0C6h,030h,032h
    t_rel_p16            (0B) 000A
		defb	0DDh,0E5h,0D1h,0CDh
		defw	LWRD ?SS_DIV_L02
		defb	0CDh
		defw	LWRD ?SS_MOD_L02
		defb	07Bh,0C6h,030h,032h
    t_rel_p16            (0B) 0009
		defb	021h
    t_rel_p16            (0B) 0000
		defb	0DDh,0E1h,0C1h,0C3h
		defw	LWRD ?BANK_FAST_LEAVE_L08
    t_org_rel            (01) 00000000
		defb	06Eh,06Fh,020h,065h,072h,072h,06Fh
		defb	072h,000h,064h,06Fh,06Dh,061h,069h
		defb	06Eh,020h,065h,072h,072h,06Fh,072h
		defb	000h,072h,061h,06Eh,067h,065h,020h
		defb	065h,072h,072h,06Fh,072h,000h,075h
		defb	06Eh,06Bh,06Eh,06Fh,077h,06Eh,020h
		defb	065h,072h,072h,06Fh,072h,000h
    t_org_rel            (0B) 00000000
    t_org_rel            (0B) 0000000C
    t_org_rel            (0C) 00000000
		defb	065h,072h,072h,06Fh,072h,020h,04Eh
		defb	06Fh,02Eh,078h,078h,000h
		endmod

; -----------------------------------------------------------------------------

	end
