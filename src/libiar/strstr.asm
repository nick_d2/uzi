; strstr.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	strstr
		rseg	CODE
code_base:
		public	strstr
strstr		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	000h,000h,0FDh,0E5h,0DDh,05Eh,002h
		defb	0DDh,056h,003h,069h,060h,0AFh,0B6h
		defb	028h,02Ch,06Bh,062h,0AFh,0B6h,028h
		defb	022h,0FDh,021h,000h,000h,0FDh,0E5h
		defb	0E1h,009h,0E5h,0FDh,0E5h,0E1h,019h
		defb	07Eh,0E1h,0BEh,020h,00Dh,0FDh,0E5h
		defb	0E1h,023h,009h,07Eh,0B7h,028h,00Bh
		defb	0FDh,023h,018h,0E5h
		defb	013h,018h,0D8h,06Fh,067h,018h,001h
		defb	0EBh,0FDh,0E1h,0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
