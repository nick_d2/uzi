; modf.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	modf
		rseg	CODE
code_base:
		public	modf
modf		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?SL_CMP_L03
		extern	?F_SUB_L04
		extern	?F_TO_L_L04
		extern	?SL_TO_F_L04
		extern	?BANK_FAST_LEAVE_L08
		extern	?BANK_LEAVE_32_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0FCh,0FFh,021h,018h,04Bh,0E5h,021h
		defb	080h,096h,0E5h,0DDh,06Eh,002h,0DDh
		defb	066h,003h,0CBh,0B8h,0CDh
		defw	LWRD ?SL_CMP_L03
		defb	0DDh,046h,005h,038h,016h,0E5h,0DDh
		defb	06Eh,00Ah,0DDh,066h,00Bh,0D1h,073h
		defb	023h,072h,023h,071h,023h,070h,001h
		defb	000h,000h,069h,060h,018h,032h,0CDh
		defw	LWRD ?F_TO_L_L04
		defb	0CDh
		defw	LWRD ?SL_TO_F_L04
		defb	0DDh,075h,0FCh,0DDh,074h,0FDh,0E5h
		defb	0DDh,06Eh,00Ah,0DDh,066h,00Bh,0D1h
		defb	073h,023h,072h,023h,071h,023h,070h
		defb	0C5h,0DDh,06Eh,0FCh,0DDh,066h,0FDh
		defb	0E5h,0DDh,04Eh,004h,0DDh,046h,005h
		defb	0DDh,06Eh,002h,0DDh,066h,003h,0CDh
		defw	LWRD ?F_SUB_L04
		defb	0C3h
		defw	LWRD ?BANK_LEAVE_32_L08
		endmod

; -----------------------------------------------------------------------------

	end
