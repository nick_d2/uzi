; BFSNEGASG.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?BF_S_NEGASG_L10
		rseg	RCODE
rcode_base:
		public	?BF_S_NEGASG_L10
?BF_S_NEGASG_L10	equ	rcode_base+00000000h
		extern	?BF_S_SHIFT_UP_L10
		extern	?BF_S_RET_VAL_L10
		defb	0EBh,0E3h,0DDh,0E5h,0D5h,0E5h,0DDh
		defb	0E1h,023h,023h,023h,0E3h,0E5h,0F5h
		defb	0C5h,011h,001h,000h,0CDh
		defw	LWRD ?BF_S_SHIFT_UP_L10
		defb	07Eh,02Fh,083h,05Fh,023h,07Eh,02Fh
		defb	08Ah,057h,0C3h
		defw	LWRD ?BF_S_RET_VAL_L10
		endmod

; -----------------------------------------------------------------------------

	end
