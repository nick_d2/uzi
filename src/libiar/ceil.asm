; ceil.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	ceil
		rseg	CODE
code_base:
		public	ceil
ceil		equ	code_base+00000000h
		extern	floor
		extern	?CL64180B_4_06_L00
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?BANK_LEAVE_32_L08
		extern	?ENT_PARM_DIRECT_L09EFINED
		defb	0CDh
		defw	LWRD ?ENT_PARM_DIRECT_L09
		defb	0EBh,079h,0B0h,028h,004h,078h,0EEh
		defb	080h,047h,0EBh,03Eh
		defb	BYTE3 floor
		defb	021h
		defw	LWRD floor
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	079h,0B0h,028h,004h,078h,0EEh,080h
		defb	047h,0C3h
		defw	LWRD ?BANK_LEAVE_32_L08
		endmod

; -----------------------------------------------------------------------------

	end
