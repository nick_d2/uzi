; BFSCLDSHIFTDOWN.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?BF_SC_LD_SHIFT_DOWN_L10
		rseg	RCODE
rcode_base:
		public	?BF_SC_LD_SHIFT_DOWN_L10
?BF_SC_LD_SHIFT_DOWN_L10	equ	rcode_base+00000000h
		extern	?BF_SC_EXT_L10
		extern	?SC_RSH_L01
		defb	047h,0C5h,07Eh,0A2h,04Fh,0CDh
		defw	LWRD ?BF_SC_EXT_L10
		defb	07Bh,0E6h,007h,047h,079h,0CDh
		defw	LWRD ?SC_RSH_L01
		defb	0C1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
