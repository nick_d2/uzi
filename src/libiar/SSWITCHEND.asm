; SSWITCHEND.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?S_SWITCH_END_L06
		rseg	RCODE
rcode_base:
		public	?S_SWITCH_END_L06
?S_SWITCH_END_L06	equ	rcode_base+00000000h
		defb	030h,004h,0EBh,023h,029h,019h,05Eh
		defb	023h,056h,0EBh,0D1h,0F1h,0E3h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
