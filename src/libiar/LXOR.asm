; LXOR.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?L_XOR_L03
		rseg	RCODE
rcode_base:
		public	?L_XOR_L03
?L_XOR_L03	equ	rcode_base+00000000h
		defb	0EBh,0E3h,0F5h,0E5h,021h,006h,000h
		defb	039h,07Eh,0ABh,077h,023h,07Eh,0AAh
		defb	077h,0D1h,023h,07Eh,073h,0A9h,04Fh
		defb	023h,07Eh,072h,0A8h,047h,0F1h,0D1h
		defb	0E1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
