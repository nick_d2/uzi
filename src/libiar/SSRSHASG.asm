; SSRSHASG.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?SS_RSHASG_L02
		rseg	RCODE
rcode_base:
		public	?SS_RSHASG_L02
?SS_RSHASG_L02	equ	rcode_base+00000000h
		extern	?SS_RSH_L02
		defb	05Eh,023h,056h,0CDh
		defw	LWRD ?SS_RSH_L02
		defb	072h,02Bh,073h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
