; SLDIVASG.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?SL_DIVASG_L03
		rseg	RCODE
rcode_base:
		public	?SL_DIVASG_L03
?SL_DIVASG_L03	equ	rcode_base+00000000h
		extern	?SL_DIV_L03
		extern	?L_END_MULDIVASG_L03
		defb	0C5h,0D5h,05Eh,023h,056h,023h,04Eh
		defb	023h,046h,0EBh,0CDh
		defw	LWRD ?SL_DIV_L03
		defb	0C3h
		defw	LWRD ?L_END_MULDIVASG_L03
		endmod

; -----------------------------------------------------------------------------

	end
