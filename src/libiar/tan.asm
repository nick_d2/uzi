; tan.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	tan
		rseg	CODE
code_base:
		extern	modf
		public	tan
tan		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?F_MUL_L04
		extern	?F_DIV_L04
		extern	?F_ADD_L04
		extern	?F_SUB_L04
		extern	?F_TO_L_L04
		extern	?F_MULASG_L04
		extern	?C_S_SWITCH_L06
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_LEAVE_32_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0FAh,0FFh,0CBh,078h,028h,004h,03Eh
		defb	001h,018h,001h,0AFh,0DDh,077h,0FAh
		defb	0CBh,0B8h,0DDh,070h,005h,021h,008h
		defb	000h,039h,001h,0A2h,03Fh,011h,083h
		defb	0F9h,0CDh
		defw	LWRD ?F_MULASG_L04
		defb	021h,002h,000h,039h,0E5h,03Eh
		defb	BYTE3 modf
		defb	021h
		defw	LWRD modf
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0F1h,0DDh,075h,002h,0DDh,074h,003h
		defb	0DDh,071h,004h,0DDh,070h,005h,0DDh
		defb	036h,0FBh,000h,0DDh,04Eh,0FEh,0DDh
		defb	046h,0FFh,0DDh,06Eh,0FCh,0DDh,066h
		defb	0FDh,0CDh
		defw	LWRD ?F_TO_L_L04
		defb	07Dh,0E6h,003h,05Fh,0CDh
		defw	LWRD ?C_S_SWITCH_L06
		defb	000h,004h,000h
		defw	LWRD code_base+000CEh
		defw	LWRD code_base+000CEh
		defw	LWRD code_base+00064h
		defw	LWRD code_base+00089h
		defw	LWRD code_base+0009Dh
		defb	0DDh,06Eh,004h,0DDh,066h,005h,0E5h
		defb	0DDh,06Eh,002h,0DDh,066h,003h,0E5h
		defb	001h,080h,03Fh,021h,000h,000h,0CDh
		defw	LWRD ?F_SUB_L04
		defb	0DDh,075h,002h,0DDh,074h,003h,0DDh
		defb	071h,004h,0DDh,070h,005h,018h,00Eh
		defb	0AFh,0DDh,0B6h,0FAh,020h,004h,03Eh
		defb	001h,018h,001h,0AFh,0DDh,077h,0FAh
		defb	0DDh,036h,0FBh,001h,018h,031h,0DDh
		defb	06Eh,004h,0DDh,066h,005h,0E5h,0DDh
		defb	06Eh,002h,0DDh,066h,003h,0E5h,001h
		defb	080h,03Fh,021h,000h
		defb	000h,0CDh
		defw	LWRD ?F_SUB_L04
		defb	0DDh,075h,002h,0DDh,074h,003h,0DDh
		defb	071h,004h,0DDh,070h,005h,0AFh,0DDh
		defb	0B6h,0FAh,020h,004h,03Eh,001h,018h
		defb	001h,0AFh,0DDh,077h,0FAh,0DDh,06Eh
		defb	004h,0DDh,066h,005h,0E5h,0DDh,06Eh
		defb	002h,0DDh,066h,003h,0E5h,0DDh,04Eh
		defb	004h,0DDh,046h,005h,0CDh
		defw	LWRD ?F_MUL_L04
		defb	0DDh,075h,0FCh,0DDh,074h,0FDh,0DDh
		defb	071h,0FEh,0DDh,070h,0FFh,021h,008h
		defb	000h,039h,0E5h,001h,08Eh,0C2h,0C5h
		defb	001h,03Dh,0D4h,0C5h,0DDh,04Eh,0FEh
		defb	0DDh,046h,0FFh,0DDh,06Eh,0FCh,0DDh
		defb	066h,0FDh,0CDh
		defw	LWRD ?F_ADD_L04
		defb	0C5h,0E5h,0CDh
		defw	LWRD code_base+00183h
		defb	0C5h,0E5h,001h,086h,043h,021h,015h
		defb	0DEh,0CDh
		defw	LWRD ?F_ADD_L04
		defb	0C5h,0E5h,021h,048h,0C1h,0E5h,021h
		defb	054h,076h,0E5h,0CDh
		defw	LWRD code_base+00183h
		defb	0C5h,0E5h,001h,053h,043h,021h,070h
		defb	0D9h,0CDh
		defw	LWRD ?F_ADD_L04
		defb	0CDh
		defw	LWRD ?F_DIV_L04
		defb	0EBh,0E1h,0CDh
		defw	LWRD ?F_MULASG_L04
		defb	0AFh,0DDh,0B6h,0FBh,028h,023h,0DDh
		defb	06Eh,004h,0DDh,066h,005h,0E5h,0DDh
		defb	06Eh,002h,0DDh,066h,003h,0E5h,001h
		defb	080h,03Fh,021h,000h,000h,0CDh
		defw	LWRD ?F_DIV_L04
		defb	0DDh,075h,002h,0DDh,074h,003h,0DDh
		defb	071h,004h,0DDh,070h,005h,0AFh,0DDh
		defb	0B6h,0FAh,0DDh,04Eh,004h,0DDh,046h
		defb	005h,0DDh,06Eh,002h,0DDh,066h,003h
		defb	028h,008h,079h,0B0h,028h,004h,078h
		defb	0EEh,080h,047h,0C3h
		defw	LWRD ?BANK_LEAVE_32_L08
		defb	0DDh,04Eh,0FEh,0DDh,046h,0FFh,0DDh
		defb	06Eh,0FCh,0DDh,066h,0FDh,0C3h
		defw	LWRD ?F_MUL_L04
		endmod

; -----------------------------------------------------------------------------

	end
