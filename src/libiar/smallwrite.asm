; smallwrite.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	_small_write
		rseg	CODE
code_base:
		public	_small_write
_small_write	equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?UL_DIV_L03
		extern	?UL_DIVASG_L03
		extern	?UL_MODASG_L03
		extern	?L_NEGASG_L03
		extern	?C_V_SWITCH_L06
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0F0h,0FFh,0FDh,0E5h,0FDh,021h,000h
		defb	000h,0DDh,06Eh,002h,0DDh,066h,003h
		defb	023h,0DDh,075h,002h,0DDh,074h,003h
		defb	02Bh,046h,0DDh,070h,0FCh,078h,0FEh
		defb	025h,028h,00Ch,0AFh,0B0h,020h,074h
		defb	0FDh,0E5h,0E1h,0FDh,0E1h,0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		defb	0DDh,036h,0FEh,000h,0DDh,036h,0FFh
		defb	000h,023h,07Eh,0FEh,06Ch,020h,00Eh
		defb	0DDh,034h,002h,020h,003h,0DDh,034h
		defb	003h,0DDh,036h,0FFh,001h,018h,010h
		defb	0FEh,068h,020h,00Ch,0DDh,034h,002h
		defb	020h,003h,0DDh,034h,003h,0DDh,036h
		defb	0FEh,001h,0DDh,06Eh,002h,0DDh,066h
		defb	003h,023h,0DDh,075h
		defb	002h,0DDh,074h,003h,02Bh,046h,0DDh
		defb	070h,0FCh,058h,0CDh
		defw	LWRD ?C_V_SWITCH_L06
		defb	006h,000h,058h,063h,064h,06Fh,073h
		defb	078h
		defw	LWRD code_base+0009Ah
		defw	LWRD code_base+00138h
		defw	LWRD code_base+000A2h
		defw	LWRD code_base+000DBh
		defw	LWRD code_base+00108h
		defw	LWRD code_base+00086h
		defw	LWRD code_base+00138h
		defb	0DDh,06Eh,010h,0DDh,066h,011h,04Eh
		defb	059h,023h,046h,050h,013h,013h,072h
		defb	02Bh,073h,00Ah,0DDh,077h,0FCh,0CDh
		defw	LWRD code_base+002A8h
		defb	0FDh,023h,0C3h
		defw	LWRD code_base+0000Bh
		defb	0DDh,06Eh,010h,0DDh,066h,011h,04Eh
		defb	059h,023h,046h,050h,013h,013h,072h
		defb	02Bh,073h,00Ah,0DDh,077h,0FAh,003h
		defb	00Ah,0DDh,077h,0FBh,0DDh,06Eh,0FAh
		defb	0DDh,066h,0FBh,023h,0DDh,075h,0FAh
		defb	0DDh,074h,0FBh,02Bh,046h,0DDh,070h
		defb	0FCh,004h,005h,028h,007h,0CDh
		defw	LWRD code_base+002A8h
		defb	0FDh,023h,018h,0E3h,0C3h
		defw	LWRD code_base+0000Bh
		defb	0DDh,036h,0F8h,008h,0DDh,036h,0F9h
		defb	000h,0AFh,0DDh,0B6h,0FEh,020h,005h
		defb	0DDh,0B6h,0FFh,020h,00Ah,0AFh,0DDh
		defb	077h,0F0h,0DDh,036h,0F1h,080h,018h
		defb	05Bh,0AFh,0DDh,077h,0F0h,0DDh,077h
		defb	0F1h,0DDh,077h,0F2h,0DDh,036h,0F3h
		defb	040h,018h,061h,0DDh,036h,0F8h,00Ah
		defb	0DDh,036h,0F9h,000h
		defb	0AFh,0DDh,0B6h,0FEh,020h,005h,0DDh
		defb	0B6h,0FFh,020h,00Bh,0AFh,0DDh,036h
		defb	0F0h,010h,0DDh,036h,0F1h,027h,018h
		defb	02Dh,0DDh,036h,0F0h,000h,0DDh,036h
		defb	0F1h,0CAh,0DDh,036h,0F2h,09Ah,0DDh
		defb	036h,0F3h,03Bh,018h,031h,0DDh,036h
		defb	0F8h,010h,0DDh,036h,0F9h,000h,0AFh
		defb	0DDh,0B6h,0FEh,020h
		defb	005h,0DDh,0B6h,0FFh,020h,010h,0AFh
		defb	0DDh,077h,0F0h,0DDh,036h,0F1h,010h
		defb	0DDh,077h,0F2h,0DDh,077h,0F3h,018h
		defb	00Eh,0AFh,0DDh,077h,0F0h,0DDh,077h
		defb	0F1h,0DDh,077h,0F2h,0DDh,036h,0F3h
		defb	010h,0AFh,0DDh,0B6h,0FEh,028h,01Ch
		defb	078h,0FEh,064h,0DDh,06Eh,010h,0DDh
		defb	066h,011h,04Eh,059h
		defb	023h,046h,050h,013h,013h,072h,02Bh
		defb	073h,00Ah,06Fh,003h,00Ah,067h,020h
		defb	045h,018h,03Dh,0DDh,0B6h,0FFh,028h
		defb	01Eh,0DDh,06Eh,010h,0DDh,066h,011h
		defb	04Eh,059h,023h,046h,050h,013h,013h
		defb	013h,013h,072h,02Bh,073h,069h,060h
		defb	05Eh,023h,056h,023h,04Eh,023h,046h
		defb	0EBh,018h,023h,078h
		defb	0FEh,064h,0DDh,06Eh,010h,0DDh,066h
		defb	011h,04Eh,059h,023h,046h,050h,013h
		defb	013h,072h,02Bh,073h,00Ah,06Fh,003h
		defb	00Ah,067h,020h,006h,007h,09Fh,04Fh
		defb	041h,018h,003h,001h,000h,000h,0DDh
		defb	075h,0F4h,0DDh,074h,0F5h,0DDh,071h
		defb	0F6h
		defb	0DDh,070h,0F7h,0DDh,07Eh,0FCh,0FEh
		defb	064h,020h,018h,0CBh,078h,028h,014h
		defb	021h,006h,000h,039h,0CDh
		defw	LWRD ?L_NEGASG_L03
		defb	0DDh,04Eh,00Eh,0DDh,046h,00Fh,01Eh
		defb	02Dh,0CDh
		defw	LWRD code_base+002B1h
		defb	0FDh,023h,0A7h,021h,001h,000h,0DDh
		defb	04Eh,0F0h,0DDh,046h,0F1h,0EDh,042h
		defb	021h,000h,000h,0DDh,04Eh,0F2h,0DDh
		defb	046h,0F3h,0EDh,042h,030h,028h,0A7h
		defb	0DDh,06Eh,0F4h,0DDh,066h,0F5h,0DDh
		defb	04Eh,0F0h,0DDh,046h,0F1h,0EDh,042h
		defb	0DDh,06Eh,0F6h,0DDh,066h,0F7h,0DDh
		defb	04Eh,0F2h,0DDh,046h
		defb	0F3h,0EDh,042h,030h,009h,021h,002h
		defb	000h,039h,0CDh
		defw	LWRD code_base+002BDh
		defb	018h,0BFh,0DDh,06Eh,0F2h,0DDh,066h
		defb	0F3h,0E5h,0DDh,06Eh,0F0h,0DDh,066h
		defb	0F1h,0E5h,0DDh,04Eh,0F6h,0DDh,046h
		defb	0F7h,0DDh,06Eh,0F4h,0DDh,066h,0F5h
		defb	0CDh
		defw	LWRD ?UL_DIV_L03
		defb	07Dh,0C6h,030h,0DDh,077h,0FDh,047h
		defb	03Eh,039h,0B8h,030h,011h,0DDh,07Eh
		defb	0FCh,0FEh,078h,078h,020h,004h,0C6h
		defb	027h,018h,002h,0C6h,007h,0DDh,077h
		defb	0FDh,0DDh,04Eh,00Eh,0DDh,046h,00Fh
		defb	0DDh,05Eh,0FDh,0CDh
		defw	LWRD code_base+002B1h
		defb	021h,006h,000h,039h,0DDh,04Eh,0F2h
		defb	0DDh,046h,0F3h,0DDh,05Eh,0F0h,0DDh
		defb	056h,0F1h,0CDh
		defw	LWRD ?UL_MODASG_L03
		defb	021h,002h,000h,039h,0CDh
		defw	LWRD code_base+002BDh
		defb	07Bh,0B2h,0B1h,0B0h,0FDh,023h,020h
		defb	098h,0C3h
		defw	LWRD code_base+0000Bh
		defb	0DDh,04Eh,00Eh,0DDh,046h,00Fh,0DDh
		defb	05Eh,0FCh,0DDh,07Eh,00Ch,0DDh,06Eh
		defb	00Ah,0DDh,066h,00Bh,0C3h
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0DDh,05Eh,0F8h,0DDh,056h,0F9h,001h
		defb	000h,000h,0C3h
		defw	LWRD ?UL_DIVASG_L03
		endmod

; -----------------------------------------------------------------------------

	end
