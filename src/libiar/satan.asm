; satan.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	satan
		rseg	CODE
code_base:
		public	__satan
__satan		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?SL_CMP_L03
		extern	?F_MUL_L04
		extern	?F_DIV_L04
		extern	?F_ADD_L04
		extern	?F_MULASG_L04
		extern	?BANK_LEAVE_32_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0F8h,0FFh,0DDh,06Eh,002h,0DDh,066h
		defb	003h,0CBh,0B8h,0DDh,070h,005h,0C5h
		defb	0E5h,001h,080h,03Fh,021h,000h,000h
		defb	0CDh
		defw	LWRD ?SL_CMP_L03
		defb	038h,032h,0DDh,06Eh,002h,0DDh,066h
		defb	003h,001h,000h,000h,0EDh,042h,020h
		defb	016h,0DDh,06Eh,004h,0DDh,066h,005h
		defb	001h,080h,03Fh,0EDh,042h,020h,009h
		defb	001h,049h,03Fh,021h,0DBh,00Fh,0C3h
		defw	LWRD code_base+00120h
		defb	0AFh,0DDh,077h,0FCh,0DDh,077h,0FDh
		defb	0DDh,077h,0FEh,0DDh,077h,0FFh,018h
		defb	031h,0C5h,0E5h,0CDh
		defw	LWRD code_base+00123h
		defb	0C5h,0E5h,021h,080h,0BFh,0E5h,021h
		defb	000h,000h,0E5h,0CDh
		defw	LWRD code_base+00123h
		defb	0CDh
		defw	LWRD ?F_DIV_L04
		defb	0DDh,075h,002h,0DDh,074h,003h,0DDh
		defb	071h,004h,0DDh,070h,005h,0DDh,036h
		defb	0FCh,0DBh,0DDh,036h,0FDh,00Fh,0DDh
		defb	036h,0FEh,049h,0DDh,036h,0FFh,03Fh
		defb	0DDh,06Eh,004h,0DDh,066h,005h,0E5h
		defb	0DDh,06Eh,002h,0DDh,066h,003h,0E5h
		defb	0DDh,04Eh,004h,0DDh,046h,005h,0CDh
		defw	LWRD ?F_MUL_L04
		defb	0DDh,075h,0F8h,0DDh,074h,0F9h,0DDh
		defb	071h,0FAh,0DDh,070h,0FBh,021h,00Ah
		defb	000h,039h,0E5h,001h,086h,040h,0C5h
		defb	001h,0EAh,0B4h,0C5h,0DDh,04Eh,0FAh
		defb	0DDh,046h,0FBh,0DDh,06Eh,0F8h,0DDh
		defb	066h,0F9h,0CDh
		defw	LWRD ?F_ADD_L04
		defb	0C5h,0E5h,0CDh
		defw	LWRD code_base+00132h
		defb	0C5h,0E5h,001h,068h,040h,021h,0B6h
		defb	0FDh,0CDh
		defw	LWRD ?F_ADD_L04
		defb	0C5h,0E5h,021h,02Bh,0BCh,0E5h,021h
		defb	024h,0FFh,0E5h,0CDh
		defw	LWRD code_base+00132h
		defb	0C5h,0E5h,001h,0A6h,03Eh,021h,088h
		defb	044h,0CDh
		defw	LWRD ?F_ADD_L04
		defb	0C5h,0E5h,0CDh
		defw	LWRD code_base+00132h
		defb	0C5h,0E5h,001h,03Fh,040h,021h,018h
		defb	0C0h,0CDh
		defw	LWRD ?F_ADD_L04
		defb	0C5h,0E5h,0CDh
		defw	LWRD code_base+00132h
		defb	0C5h,0E5h,001h,068h,040h,021h,0B5h
		defb	0FDh,0CDh
		defw	LWRD ?F_ADD_L04
		defb	0CDh
		defw	LWRD ?F_DIV_L04
		defb	0EBh,0E1h,0CDh
		defw	LWRD ?F_MULASG_L04
		defb	0C5h,0D5h,0DDh,04Eh,0FEh,0DDh,046h
		defb	0FFh,0DDh,06Eh,0FCh,0DDh,066h,0FDh
		defb	0CDh
		defw	LWRD ?F_ADD_L04
		defb	0C3h
		defw	LWRD ?BANK_LEAVE_32_L08
		defb	0DDh,04Eh,004h,0DDh,046h,005h,0DDh
		defb	06Eh,002h,0DDh,066h,003h,0C3h
		defw	LWRD ?F_ADD_L04
		defb	0DDh,04Eh,0FAh,0DDh,046h,0FBh,0DDh
		defb	06Eh,0F8h,0DDh,066h,0F9h,0C3h
		defw	LWRD ?F_MUL_L04
		endmod

; -----------------------------------------------------------------------------

	end
