; USMODASGDEprim.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?US_MODASG_DE_prim_L12
		rseg	RCODE
rcode_base:
		public	?US_MODASG_DE_prim_L12
?US_MODASG_DE_prim_L12	equ	rcode_base+00000000h
		extern	?US_MOD_L02
		defb	0C5h,0D9h,0E1h,0C5h,044h,04Dh,0CDh
		defw	LWRD ?US_MOD_L02
		defb	0C1h,0D5h,0D9h,0E1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
