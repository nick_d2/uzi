; STRCHR.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?STRCHR_L11
		rseg	RCODE
rcode_base:
		public	?STRCHR_L11
?STRCHR_L11	equ	rcode_base+00000000h
		defb	07Eh,0BBh,0C8h,0B7h,023h,020h,0F9h
		defb	021h,000h,000h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
