; CLSH.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?C_LSH_L01
		rseg	RCODE
rcode_base:
		public	?C_LSH_L01
?C_LSH_L01	equ	rcode_base+00000000h
		defb	004h,005h,0C8h,087h,010h,0FDh,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
