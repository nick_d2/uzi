; STRCPY.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?STRCPY_L11
		rseg	RCODE
rcode_base:
		public	?STRCPY_L11
?STRCPY_L11	equ	rcode_base+00000000h
		defb	0F5h,0E5h,0D5h,0C5h,0AFh,0EBh,0BEh
		defb	0EDh,0A0h,020h,0FBh,0C1h,0D1h,0E1h
		defb	0F1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
