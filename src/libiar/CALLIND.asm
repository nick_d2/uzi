; CALLIND.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?CALL_IND_L09
		rseg	RCODE
rcode_base:
		public	?CALL_IND_L09
?CALL_IND_L09	equ	rcode_base+00000000h
		defb	0E9h
		endmod

; -----------------------------------------------------------------------------

	end
