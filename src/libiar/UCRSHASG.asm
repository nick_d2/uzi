; UCRSHASG.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?UC_RSHASG_L01
		rseg	RCODE
rcode_base:
		public	?UC_RSHASG_L01
?UC_RSHASG_L01	equ	rcode_base+00000000h
		extern	?UC_RSH_L01
		defb	07Eh,0CDh
		defw	LWRD ?UC_RSH_L01
		defb	077h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
