; SLCMP.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?SL_CMP_L03
		rseg	RCODE
rcode_base:
		public	?SL_CMP_L03
?SL_CMP_L03	equ	rcode_base+00000000h
		defb	0EBh,0E3h,0D5h,0C5h,0F5h,0D5h,0C5h
		defb	0EBh,021h,00Fh,000h,039h,07Eh,0EEh
		defb	080h,047h,072h,02Bh,04Eh,073h,0EBh
		defb	0E1h,07Ch,0EEh,080h,067h,0EDh,042h
		defb	0E1h,020h,00Ah,0EBh,02Bh,07Eh,02Bh
		defb	06Eh,067h,0EBh,0A7h,0EDh,052h,0C1h
		defb	078h,0C1h,0E1h,0D1h,033h,033h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
