; STRLEN.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?STRLEN_L11
		rseg	RCODE
rcode_base:
		public	?STRLEN_L11
?STRLEN_L11	equ	rcode_base+00000000h
		defb	0C5h,0F5h,0AFh,047h,04Fh,0EDh,0B1h
		defb	021h,0FFh,0FFh,0EDh,042h,0F1h,0C1h
		defb	0C9h
		endmod

; -----------------------------------------------------------------------------

	end
