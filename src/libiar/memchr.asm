; memchr.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	memchr
		rseg	CODE
code_base:
		public	memchr
memchr		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_PARM_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_PARM_DIRECT_L09
		defb	0DDh,06Eh,00Ah,0DDh,066h,00Bh,02Bh
		defb	0DDh,075h,00Ah,0DDh,074h,00Bh,023h
		defb	07Dh,0B4h,028h,01Bh,0DDh,06Eh,002h
		defb	0DDh,066h,003h,0DDh,07Eh,004h,0BEh
		defb	028h,011h,021h,002h,000h,039h,07Eh
		defb	0C6h,001h,077h,023h,07Eh,0CEh,000h
		defb	077h,018h,0D3h,06Fh,067h,0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
