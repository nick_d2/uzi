; memcpy.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	memcpy
		rseg	CODE
code_base:
		public	memcpy
memcpy		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_PARM_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_PARM_DIRECT_L09
		defb	0DDh,04Eh,00Ah,079h,0DDh,046h,00Bh
		defb	0DDh,06Eh,004h,0DDh,066h,005h,0B0h
		defb	028h,004h,0D5h,0EDh,0B0h,0D1h,0EBh
		defb	0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
