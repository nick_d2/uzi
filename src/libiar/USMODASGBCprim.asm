; USMODASGBCprim.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?US_MODASG_BC_prim_L12
		rseg	RCODE
rcode_base:
		public	?US_MODASG_BC_prim_L12
?US_MODASG_BC_prim_L12	equ	rcode_base+00000000h
		extern	?US_MOD_L02
		defb	0C5h,0D9h,0EBh,0E3h,050h,059h,044h
		defb	04Dh,0CDh
		defw	LWRD ?US_MOD_L02
		defb	042h,04Bh,0D1h,0C5h,0D9h,0E1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
