; exp.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	exp
		rseg	CODE
code_base:
		extern	errno
		public	exp
exp		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?SL_CMP_L03
		extern	?F_MUL_L04
		extern	?F_ADD_L04
		extern	?F_SUB_L04
		extern	?F_TO_L_L04
		extern	?SL_TO_F_L04
		extern	?F_MULASG_L04
		extern	?F_ADDASG_L04
		extern	?BANK_LEAVE_32_L08
		extern	?ENT_AUTO_DIRECT_L09
		extern	?__daddexp_L11
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0FAh,0FFh,079h,0B0h,020h,008h,001h
		defb	080h,03Fh,06Fh,067h,0C3h
		defw	LWRD code_base+00149h
		defb	0DDh,06Eh,002h,0DDh,066h,003h,0CBh
		defb	0B8h,0C5h,0E5h,001h,0B0h,042h,021h
		defb	000h,000h,0CDh
		defw	LWRD ?SL_CMP_L03
		defb	030h,019h,02Eh,022h,022h
		defw	LWRD errno
		defb	0DDh,0CBh,005h,07Eh,028h,005h,04Ch
		defb	069h,044h,018h,006h,001h,07Fh,07Fh
		defb	021h,0FFh,0FFh,0C3h
		defw	LWRD code_base+00149h
		defb	02Eh,008h,039h,001h,0B8h,03Fh,011h
		defb	03Bh,0AAh,0CDh
		defw	LWRD ?F_MULASG_L04
		defb	0EBh,0CBh,0B8h,0CDh
		defw	LWRD ?F_TO_L_L04
		defb	0DDh,075h,0FEh,0DDh,0CBh,005h,07Eh
		defb	028h,01Dh,07Dh,007h,09Fh,067h,044h
		defb	04Ch,0CDh
		defw	LWRD ?SL_TO_F_L04
		defb	0C5h,0E5h,0CDh
		defw	LWRD code_base+0014Ch
		defb	079h,0B0h,028h,003h,0DDh,034h,0FEh
		defb	0DDh,07Eh,0FEh,0EDh,044h,0DDh,077h
		defb	0FEh,0DDh,06Eh,0FEh,07Dh,007h,09Fh
		defb	067h,044h,04Ch,0CDh
		defw	LWRD ?SL_TO_F_L04
		defb	0C5h,0E5h,0CDh
		defw	LWRD code_base+0014Ch
		defb	0DDh,075h,0FAh,0DDh,074h,0FBh,0DDh
		defb	071h,0FCh,0DDh,070h,0FDh,011h,000h
		defb	03Fh,0D5h,053h,0D5h,0CDh
		defw	LWRD ?SL_CMP_L03
		defb	030h,00Fh,06Bh,062h,039h,001h,000h
		defb	03Fh,0CDh
		defw	LWRD ?F_ADDASG_L04
		defb	0DDh,036h,0FFh,001h,018h,003h,0DDh
		defb	072h,0FFh,021h,063h,039h,0E5h,021h
		defb	08Ch,090h,0E5h,0CDh
		defw	LWRD code_base+0015Bh
		defb	0C5h,0E5h,001h,0A3h,03Ah,021h,0AAh
		defb	00Ch,0CDh
		defw	LWRD ?F_ADD_L04
		defb	0C5h,0E5h,0CDh
		defw	LWRD code_base+0015Bh
		defb	0C5h,0E5h,001h,01Eh,03Ch,021h,000h
		defb	094h,0CDh
		defw	LWRD ?F_ADD_L04
		defb	0C5h,0E5h,0CDh
		defw	LWRD code_base+0015Bh
		defb	0C5h,0E5h,001h,063h,03Dh,021h,080h
		defb	042h,0CDh
		defw	LWRD ?F_ADD_L04
		defb	0C5h,0E5h,0CDh
		defw	LWRD code_base+0015Bh
		defb	0C5h,0E5h,001h,075h,03Eh,021h,0CFh
		defb	0FEh,0CDh
		defw	LWRD ?F_ADD_L04
		defb	0C5h,0E5h,0CDh
		defw	LWRD code_base+0015Bh
		defb	0C5h,0E5h,001h,031h,03Fh,021h,015h
		defb	072h,0CDh
		defw	LWRD ?F_ADD_L04
		defb	0C5h,0E5h,0CDh
		defw	LWRD code_base+0015Bh
		defb	0C5h,0E5h,001h,080h,03Fh,021h,000h
		defb	000h,0CDh
		defw	LWRD ?F_ADD_L04
		defb	0DDh,075h,002h,0DDh,074h,003h,0DDh
		defb	071h,004h,0DDh,070h,005h,0AFh,0DDh
		defb	0B6h,0FFh,028h,00Dh,021h,008h,000h
		defb	039h,001h,035h,03Fh,011h,0F3h,004h
		defb	0CDh
		defw	LWRD ?F_MULASG_L04
		defb	0DDh,04Eh,004h,0DDh,046h,005h,0DDh
		defb	05Eh,002h,0DDh,056h,003h,0DDh,06Eh
		defb	0FEh,0CDh
		defw	LWRD ?__daddexp_L11
		defb	0EBh,0C3h
		defw	LWRD ?BANK_LEAVE_32_L08
		defb	0DDh,04Eh,004h,0DDh,046h,005h,0DDh
		defb	06Eh,002h,0DDh,066h,003h,0C3h
		defw	LWRD ?F_SUB_L04
		defb	0DDh,04Eh,0FCh,0DDh,046h,0FDh,0DDh
		defb	06Eh,0FAh,0DDh,066h,0FBh,0C3h
		defw	LWRD ?F_MUL_L04
		endmod

; -----------------------------------------------------------------------------

	end
