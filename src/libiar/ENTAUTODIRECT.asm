; ENTAUTODIRECT.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?ENT_AUTO_DIRECT_L09
		rseg	RCODE
rcode_base:
		public	?ENT_AUTO_DIRECT_L09
?ENT_AUTO_DIRECT_L09	equ	rcode_base+00000000h
		defb	0E1h,0C5h,0D5h,0DDh,0E5h,0DDh,021h
		defb	000h,000h,0DDh,039h,05Eh,023h,056h
		defb	023h,0EBh,039h,0F9h,0EBh,0E9h
		endmod

; -----------------------------------------------------------------------------

	end
