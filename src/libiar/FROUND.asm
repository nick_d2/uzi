; FROUND.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?F_ROUND_L04
		rseg	RCODE
rcode_base:
		public	?F_ROUND_L04
?F_ROUND_L04	equ	rcode_base+00000000h
		public	?F_UP_ROUND_L04
?F_UP_ROUND_L04	equ	rcode_base+00000010h
		extern	?F_PACK_L04
		extern	?F_OVERFLOW_L04
		extern	?F_UNDERFLOW_L04
		defb	07Ch,0B7h,0CAh
		defw	LWRD ?F_UNDERFLOW_L04
		defb	07Bh,017h,030h,01Bh,0B7h,020h,004h
		defb	0CBh,042h,028h,014h,07Ah,0C6h,001h
		defb	057h,079h,0CEh,000h,04Fh,078h,0CEh
		defb	000h,047h,030h,006h,0CBh,018h,024h
		defb	0CAh
		defw	LWRD ?F_OVERFLOW_L04
		defb	0C3h
		defw	LWRD ?F_PACK_L04
		endmod

; -----------------------------------------------------------------------------

	end
