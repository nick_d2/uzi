; FPACK.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?F_PACK_L04
		rseg	RCODE
rcode_base:
		public	?F_PACK_L04
?F_PACK_L04	equ	rcode_base+00000015h
		public	?F_OVERFLOW_L04
?F_OVERFLOW_L04	equ	rcode_base+00000000h
		public	?F_UNDERFLOW2_L04
?F_UNDERFLOW2_L04	equ	rcode_base+0000000Dh
		public	?F_UNDERFLOW_L04
?F_UNDERFLOW_L04	equ	rcode_base+0000000Eh
		public	?F_NO_PACK_L04
?F_NO_PACK_L04	equ	rcode_base+0000002Bh
		defb	006h,0FFh,048h,050h,058h,0DDh,0CBh
		defb	0F9h,01Eh,0CBh,018h,018h,01Dh,0C1h
		defb	006h,000h,048h,050h,058h,018h,015h
		defb	07Ch,0B7h,028h,0F5h,03Ch,028h,0E4h
		defb	05Ah,051h,048h,044h,0DDh,0CBh,0F9h
		defb	01Eh,0CBh,018h,038h,002h,0CBh,0B9h
		defb	0F1h,0F1h,0E1h,0DDh,075h,004h,0DDh
		defb	074h,005h,0DDh,0E1h
		defb	0E1h,0EBh,033h,033h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
