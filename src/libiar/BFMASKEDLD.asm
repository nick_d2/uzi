; BFMASKEDLD.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?BF_MASKED_LD_L10
		rseg	RCODE
rcode_base:
		public	?BF_MASKED_LD_L10
?BF_MASKED_LD_L10	equ	rcode_base+00000000h
		defb	07Eh,0DDh,0A6h,001h,05Fh,023h,07Eh
		defb	0DDh,0A6h,002h,057h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
