; sinh.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	sinh
		rseg	CODE
code_base:
		extern	errno
		extern	exp
		public	sinh
sinh		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?SL_CMP_L03
		extern	?F_MUL_L04
		extern	?F_DIV_L04
		extern	?F_SUBASG_L04
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?BANK_LEAVE_32_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0FEh,0FFh,0CBh,078h,028h,004h,03Eh
		defb	001h,018h,001h,0AFh,0DDh,077h,0FEh
		defb	0DDh,06Eh,002h,0DDh,066h,003h,0CBh
		defb	0B8h,0DDh,070h,005h,0C5h,0E5h,001h
		defb	031h,043h,021h,018h,072h,0CDh
		defw	LWRD ?SL_CMP_L03
		defb	030h,018h,021h,022h,000h,022h
		defw	LWRD errno
		defb	0DDh,036h,002h,0FFh,0DDh,036h,003h
		defb	0FFh,0DDh,036h,004h,07Fh,0DDh,036h
		defb	005h,07Fh,018h,066h,021h,080h,033h
		defb	0E5h,021h,06Ch,034h,0E5h,0DDh,04Eh
		defb	004h,0DDh,046h,005h,0DDh,06Eh,002h
		defb	0DDh,066h,003h,0CDh
		defw	LWRD ?SL_CMP_L03
		defb	038h,04Dh,021h,0FFh,040h,0E5h,021h
		defb	029h,05Ch,0E5h,0DDh,06Eh,002h,0DDh
		defb	066h,003h,0CDh
		defw	LWRD ?SL_CMP_L03
		defb	030h,03Ah,0EBh,03Eh
		defb	BYTE3 exp
		defb	021h
		defw	LWRD exp
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0C5h,0E5h,001h,000h,03Fh,069h,061h
		defb	0CDh
		defw	LWRD ?F_MUL_L04
		defb	0DDh,075h,002h,0DDh,074h,003h,0DDh
		defb	071h,004h,0DDh,070h,005h,021h,004h
		defb	000h,039h,0E5h,0C5h,0DDh,04Eh,002h
		defb	0DDh,046h,003h,0C5h,001h,080h,03Eh
		defb	021h,000h,000h,0CDh
		defw	LWRD ?F_DIV_L04
		defb	0EBh,0E1h,0CDh
		defw	LWRD ?F_SUBASG_L04
		defb	0AFh,0DDh,0B6h,0FEh,0DDh,04Eh,004h
		defb	0DDh,046h,005h,0DDh,06Eh,002h,0DDh
		defb	066h,003h,028h,008h,079h,0B0h,028h
		defb	004h,078h,0EEh,080h,047h,0C3h
		defw	LWRD ?BANK_LEAVE_32_L08
		endmod

; -----------------------------------------------------------------------------

	end
