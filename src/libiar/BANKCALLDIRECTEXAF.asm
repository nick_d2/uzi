; BANKCALLDIRECTEXAF.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?BANK_CALL_DIRECT_EXAF_L08
		rseg	RCODE
rcode_base:
		public	?BANK_CALL_DIRECT_EXAF_L08
?BANK_CALL_DIRECT_EXAF_L08	equ	rcode_base+00000000h
		defb	008h,0EDh,038h,039h,0F5h,008h,0EDh
		defb	039h,039h,0E9h
		endmod

; -----------------------------------------------------------------------------

	end
