; SMULASGIX.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?S_MULASG_IX_L12
		rseg	RCODE
rcode_base:
		public	?S_MULASG_IX_L12
?S_MULASG_IX_L12	equ	rcode_base+00000000h
		extern	?S_MUL_L02
		defb	0D5h,0DDh,0E5h,0D1h,0CDh
		defw	LWRD ?S_MUL_L02
		defb	0D5h,0EBh,0DDh,0E1h,0D1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
