; getchar.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	getchar
		rseg	CODE
code_base:
		rseg	CSTR
cstr_base:
		rseg	UDATA0
udata0_base:
		extern	_low_level_get
		public	getchar
getchar		equ	udata0_base+00000022h
		extern	putchar
		extern	?CL64180B_4_06_L00
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_PARM_DIRECT_L09
		extern	?ENT_AUTO_DIRECT_L09
		extern	?LEAVE_DIRECT_L09
		defb	0DDh,0E5h,0D5h,0DDh,0E1h,0AFh,0DDh
		defb	0B6h,000h,028h,00Ah,0DDh,05Eh,000h
		defb	0CDh
		defw	LWRD udata0_base+00018h
		defb	0DDh,023h,018h,0F0h,0DDh,0E1h,0C9h
		defb	016h,000h,03Eh
		defb	BYTE3 putchar
		defb	021h
		defw	LWRD putchar
		defb	0C3h
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0FEh,0FFh,021h
    t_rel_p16            (0A) 0000
		defb	0EDh,04Bh
    t_rel_p16            (0A) 0052
		defb	009h,07Eh,0B7h,028h,010h,069h,060h
		defb	023h,022h
    t_rel_p16            (0A) 0052
		defb	02Bh,001h
    t_rel_p16            (0A) 0000
		defb	009h,06Eh,026h,000h,018h,025h,06Fh
		defb	067h,022h
    t_rel_p16            (0A) 0052
		defb	03Eh
		defb	BYTE3 ?BANK_FAST_LEAVE_L08
		defb	021h
		defw	LWRD ?BANK_FAST_LEAVE_L08
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0DDh,075h,0FEh,07Dh,0FEh,01Ah,020h
		defb	013h,02Ah
    t_rel_p16            (0A) 0052
		defb	07Dh,0B4h,020h,00Ch,011h
    t_rel_p16            (01) 0000
		defb	0CDh
		defw	LWRD udata0_base+00000h
		defb	021h,0FFh,0FFh,0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		defb	0DDh,046h,0FEh,004h,0E2h
		defw	LWRD udata0_base+00085h
		defb	02Ah
    t_rel_p16            (0A) 0052
		defb	07Dh,0B4h,028h,0CFh,02Bh,022h
    t_rel_p16            (0A) 0052
		defb	011h
    t_rel_p16            (01) 0003
		defb	0CDh
		defw	LWRD udata0_base+00000h
		defb	018h,0C3h,0DDh,07Eh,0FEh,0FEh,003h
		defb	020h,00Bh,011h
    t_rel_p16            (01) 0007
		defb	0CDh
		defw	LWRD udata0_base+00000h
		defb	021h,051h,000h,018h,023h,0FEh,00Dh
		defb	020h,024h,02Ah
    t_rel_p16            (0A) 0052
		defb	023h,022h
    t_rel_p16            (0A) 0052
		defb	02Bh,001h
    t_rel_p16            (0A) 0000
		defb	009h,036h,00Ah,01Eh,00Ah,0CDh
		defw	LWRD udata0_base+00018h
		defb	069h,060h,0EDh,04Bh
    t_rel_p16            (0A) 0052
		defb	009h,036h,000h,021h,000h,000h,022h
    t_rel_p16            (0A) 0052
		defb	018h,030h,001h,050h,080h,02Ah
    t_rel_p16            (0A) 0052
		defb	078h,0ACh,067h,0EDh,042h,030h,01Ah
		defb	0DDh,07Eh,0FEh,0FEh,020h,038h,019h
		defb	02Ah
    t_rel_p16            (0A) 0052
		defb	023h,022h
    t_rel_p16            (0A) 0052
		defb	02Bh,001h
    t_rel_p16            (0A) 0000
		defb	009h,047h,070h,058h,016h,000h,018h
		defb	003h,011h,007h,000h,0CDh
		defw	LWRD udata0_base+0001Ah
		defb	0C3h
		defw	LWRD udata0_base+00048h
		defb	0C3h
		defw	LWRD udata0_base+00027h
    t_org_rel            (01) 00000000
		defb	05Eh,05Ah,000h,008h,020h,008h,000h
		defb	05Eh,043h,00Ah,000h
    t_org_rel            (0A) 00000000
    t_org_rel            (0A) 00000052
    t_org_rel            (0A) 00000054
		endmod

; -----------------------------------------------------------------------------

	end
