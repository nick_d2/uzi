; FCMP.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?F_CMP_L04
		rseg	RCODE
rcode_base:
		public	?F_CMP_L04
?F_CMP_L04	equ	rcode_base+00000000h
		defb	0EBh,0E3h,0DDh,0E5h,0DDh,021h,002h
		defb	000h,0DDh,039h,0E5h,067h,0DDh,07Eh
		defb	005h,0CBh,078h,028h,019h,0CBh,07Fh
		defb	028h,015h,090h,020h,02Bh,0DDh,07Eh
		defb	004h,091h,020h,025h,0DDh,07Eh,003h
		defb	092h,020h,01Fh,0DDh,07Eh,002h,093h
		defb	018h,019h,0EEh,080h,06Fh,078h,0EEh
		defb	080h,095h,020h,010h
		defb	079h,0DDh,096h,004h,020h,00Ah,07Ah
		defb	0DDh,096h,003h,020h,004h,07Bh,0DDh
		defb	096h,002h,07Ch,0E1h,0DDh,075h,004h
		defb	0DDh,074h,005h,0DDh,0E1h,0E1h,0EBh
		defb	033h,033h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
