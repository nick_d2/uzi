; exit0.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	exit0
		rseg	RCODE
rcode_base:
		public	exit
exit		equ	rcode_base+00000001h
		public	?C_EXIT
?C_EXIT		equ	rcode_base+00000001h
		public	?C_FUNCALL
?C_FUNCALL	equ	rcode_base+00000000h
		public	?DBG_0
?DBG_0		equ	00000000h
		defb	000h,018h,0FEh
		endmod

; -----------------------------------------------------------------------------

	end
