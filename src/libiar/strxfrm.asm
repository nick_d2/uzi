; strxfrm.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	strxfrm
		rseg	CODE
code_base:
		extern	strncpy
		public	strxfrm
strxfrm		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_PARM_DIRECT_L09
		extern	?STRLEN_L11
		defb	0CDh
		defw	LWRD ?ENT_PARM_DIRECT_L09
		defb	0DDh,06Eh,00Ah,0DDh,066h,00Bh,0E5h
		defb	03Eh
		defb	BYTE3 strncpy
		defb	021h
		defw	LWRD strncpy
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0F1h,0DDh,06Eh,004h,0DDh,066h,005h
		defb	0CDh
		defw	LWRD ?STRLEN_L11
		defb	0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
