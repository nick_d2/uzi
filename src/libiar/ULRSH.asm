; ULRSH.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?UL_RSH_L03
		rseg	RCODE
rcode_base:
		public	?UL_RSH_L03
?UL_RSH_L03	equ	rcode_base+00000000h
		defb	0B7h,0C8h,0FEh,008h,038h,009h,06Ch
		defb	061h,048h,006h,000h,0D6h,008h,018h
		defb	0F2h,0CBh,038h,0CBh,019h,0CBh,01Ch
		defb	0CBh,01Dh,03Dh,020h,0F5h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
