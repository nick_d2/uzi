; atan2.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	atan2
		rseg	CODE
code_base:
		extern	__satan
		public	atan2
atan2		equ	code_base+00000000h
		extern	errno
		extern	?CL64180B_4_06_L00
		extern	?F_DIV_L04
		extern	?F_ADD_L04
		extern	?F_SUB_L04
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_LEAVE_32_L08
		extern	?ENT_PARM_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_PARM_DIRECT_L09
		defb	079h,0B0h,020h,017h,0DDh,07Eh,00Ch
		defb	0DDh,0B6h,00Dh,020h,00Fh,021h,021h
		defb	000h,022h
		defw	LWRD errno
		defb	001h,07Fh,07Fh,021h,0FFh,0FFh,0C3h
		defw	LWRD code_base+000C8h
		defb	0DDh,06Eh,00Ch,0DDh,066h,00Dh,0E5h
		defb	0DDh,06Eh,00Ah,0DDh,066h,00Bh,0E5h
		defb	0EBh,0CDh
		defw	LWRD ?F_ADD_L04
		defb	059h,050h,0DDh,04Eh,002h,0DDh,046h
		defb	003h,0EDh,042h,020h,01Ch,0EBh,0DDh
		defb	04Eh,004h,0DDh,046h,005h,0EDh,042h
		defb	020h,011h,0CBh,078h,020h,005h,001h
		defb	0C9h,03Fh,018h,003h,001h,0C9h,0BFh
		defb	021h,0DBh,00Fh,018h,070h,0DDh,0CBh
		defb	00Dh,07Eh,028h,03Dh,0DDh,0CBh,005h
		defb	07Eh,0DDh,06Eh,00Ch
		defb	0DDh,066h,00Dh,0E5h,0DDh,06Eh,00Ah
		defb	0DDh,066h,00Bh,0E5h,0CDh
		defw	LWRD code_base+000CBh
		defb	0EBh,03Eh
		defb	BYTE3 __satan
		defb	021h
		defw	LWRD __satan
		defb	020h,010h,0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0C5h,0E5h,001h,049h,040h,021h,0DBh
		defb	00Fh,0CDh
		defw	LWRD ?F_SUB_L04
		defb	018h,03Dh,0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0C5h,0E5h,001h,049h,0C0h,021h,0DBh
		defb	00Fh,0CDh
		defw	LWRD ?F_ADD_L04
		defb	018h,02Dh,0DDh,0CBh,005h,07Eh,0DDh
		defb	06Eh,00Ch,0DDh,066h,00Dh,0E5h,0DDh
		defb	06Eh,00Ah,0DDh,066h,00Bh,0E5h,0CDh
		defw	LWRD code_base+000CBh
		defb	0EBh,03Eh
		defb	BYTE3 __satan
		defb	021h
		defw	LWRD __satan
		defb	020h,005h,0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	018h,00Bh,0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	079h,0B0h,028h,004h,078h,0EEh,080h
		defb	047h,0C3h
		defw	LWRD ?BANK_LEAVE_32_L08
		defb	0DDh,04Eh,004h,0DDh,046h,005h,0DDh
		defb	06Eh,002h,0DDh,066h,003h,0C3h
		defw	LWRD ?F_DIV_L04
		endmod

; -----------------------------------------------------------------------------

	end
