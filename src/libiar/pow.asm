; pow.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	pow
		rseg	CODE
code_base:
		extern	abs
		extern	errno
		extern	exp
		extern	floor
		extern	log
		public	pow
pow		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?SL_CMP_L03
		extern	?F_MUL_L04
		extern	?F_DIV_L04
		extern	?F_ADD_L04
		extern	?F_SUB_L04
		extern	?F_TO_L_L04
		extern	?SL_TO_F_L04
		extern	?F_MULASG_L04
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_LEAVE_32_L08
		extern	?ENT_AUTO_DIRECT_L09
		extern	?__daddexp_L11
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0E4h,0FFh,0FDh,0E5h,0AFh,0DDh,077h
		defb	0FAh,0DDh,077h,0FBh,0DDh,07Eh,00Ch
		defb	0DDh,0B6h,00Dh,020h,005h,001h,080h
		defb	03Fh,018h,06Eh,079h,0B0h,020h,002h
		defb	018h,066h,059h,050h,0DDh,06Eh,002h
		defb	0DDh,066h,003h,0CBh,0BAh,001h,0FFh
		defb	0FFh,0EDh,042h,020h,01Dh,0EBh,001h
		defb	07Fh,07Fh,0EDh,042h
		defb	020h,015h,021h,022h,000h,022h
		defw	LWRD errno
		defb	0DDh,04Eh,004h,0DDh,046h,005h,0DDh
		defb	06Eh,002h,0DDh,066h,003h,0C3h
		defw	LWRD code_base+00269h
		defb	0DDh,06Eh,00Ah,0DDh,066h,00Bh,001h
		defb	0FFh,0FFh,0A7h,0EDh,042h,020h,019h
		defb	0DDh,06Eh,00Ch,0DDh,066h,00Dh,001h
		defb	07Fh,07Fh,0EDh,042h,020h,00Ch,021h
		defb	022h,000h,022h
		defw	LWRD errno
		defb	021h,0FFh,0FFh,0C3h
		defw	LWRD code_base+00269h
		defb	03Eh,080h,0DDh,0AEh,00Ch,0DDh,0A6h
		defb	00Dh,0DDh,0A6h,00Bh,0DDh,0A6h,00Ah
		defb	03Ch,020h,004h,04Fh,047h,018h,025h
		defb	0DDh,0CBh,005h,07Eh,028h,03Ah,0CDh
		defw	LWRD code_base+0026Eh
		defb	0CDh
		defw	LWRD ?SL_TO_F_L04
		defb	0C5h,0E5h,0CDh
		defw	LWRD code_base+0027Dh
		defb	0C5h,0E5h,001h,080h,034h,021h,000h
		defb	000h,0CDh
		defw	LWRD ?SL_CMP_L03
		defb	030h,00Dh,02Eh,021h,022h
		defw	LWRD errno
		defb	04Ch,044h,021h,000h,000h,0C3h
		defw	LWRD code_base+00269h
		defb	0CDh
		defw	LWRD code_base+0026Eh
		defb	07Dh,0E6h,001h,0DDh,077h,0FAh,0DDh
		defb	036h,0FBh,000h,0DDh,046h,005h,0CBh
		defb	0B8h,0DDh,070h,005h,0DDh,04Eh,004h
		defb	0DDh,046h,005h,0DDh,06Eh,002h,0DDh
		defb	066h,003h,0CBh,0B0h,0CBh,0B9h,03Eh
		defb	03Fh,0B0h,047h,0DDh,075h,0E8h,0DDh
		defb	074h,0E9h,0DDh,071h,0EAh,0DDh,070h
		defb	0EBh,0DDh,07Eh,004h
		defb	0DDh,046h,005h,007h,078h,017h,0D6h
		defb	07Eh,06Fh,007h,09Fh,067h,044h,04Ch
		defb	0CDh
		defw	LWRD ?SL_TO_F_L04
		defb	0C5h,0E5h,0DDh,04Eh,00Ch,0DDh,046h
		defb	00Dh,0DDh,06Eh,00Ah,0DDh,066h,00Bh
		defb	0CDh
		defw	LWRD ?F_MUL_L04
		defb	0DDh,075h,0F2h,0DDh,074h,0F3h,0DDh
		defb	071h,0F4h,0DDh,070h,0F5h,0EBh,03Eh
		defb	BYTE3 floor
		defb	021h
		defw	LWRD floor
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0DDh,075h,0F6h,0DDh,074h,0F7h,0DDh
		defb	071h,0F8h,0DDh,070h,0F9h,0CDh
		defw	LWRD ?F_TO_L_L04
		defb	0E5h,0FDh,0E1h,0DDh,04Eh,0F8h,0DDh
		defb	046h,0F9h,0C5h,0DDh,04Eh,0F6h,0DDh
		defb	046h,0F7h,0C5h,0DDh,04Eh,0F4h,0DDh
		defb	046h,0F5h,0DDh,06Eh,0F2h,0DDh,066h
		defb	0F3h,0CDh
		defw	LWRD ?F_SUB_L04
		defb	0DDh,075h,0E4h,0DDh,074h,0E5h,0DDh
		defb	071h,0E6h,0DDh,070h,0E7h,0FDh,0E5h
		defb	0E1h,07Ch,007h,09Fh,04Fh,041h,0CDh
		defw	LWRD ?SL_TO_F_L04
		defb	0C5h,0E5h,0CDh
		defw	LWRD code_base+0027Dh
		defb	0C5h,0E5h,0DDh,04Eh,0EAh,0DDh,046h
		defb	0EBh,0DDh,05Eh,0E8h,0DDh,056h,0E9h
		defb	03Eh
		defb	BYTE3 log
		defb	021h
		defw	LWRD log
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0CDh
		defw	LWRD ?F_MUL_L04
		defb	0C5h,0E5h,021h,031h,03Fh,0E5h,021h
		defb	018h,072h,0E5h,0DDh,04Eh,0E6h,0DDh
		defb	046h,0E7h,0DDh,06Eh,0E4h,0DDh,066h
		defb	0E5h,0CDh
		defw	LWRD ?F_MUL_L04
		defb	0CDh
		defw	LWRD ?F_ADD_L04
		defb	0EBh,03Eh
		defb	BYTE3 exp
		defb	021h
		defw	LWRD exp
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0DDh,075h,0ECh,0DDh,074h,0EDh,0DDh
		defb	071h,0EEh,0DDh,070h,0EFh,0AFh,0DDh
		defb	077h,0E4h,0DDh,077h,0E5h,0DDh,036h
		defb	0E6h,080h,0DDh,036h,0E7h,03Fh,0FDh
		defb	0E5h,0D1h,03Eh
		defb	BYTE3 abs
		defb	021h
		defw	LWRD abs
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0DDh,075h,0F0h,0DDh,074h,0F1h,07Dh
		defb	0B4h,028h,053h,0DDh,0CBh,0F0h,046h
		defb	028h,007h,021h,002h,000h,039h,0CDh
		defw	LWRD code_base+0028Ch
		defb	03Eh,001h,0DDh,0AEh,0F0h,0DDh,0B6h
		defb	0F1h,028h,013h,021h,006h,000h,039h
		defb	0CDh
		defw	LWRD code_base+0028Ch
		defb	021h,00Eh,000h,039h,023h,0CBh,03Eh
		defb	02Bh,0CBh,01Eh,018h,0D6h,0FDh,0E5h
		defb	0E1h,0CBh,07Ch,028h,022h,0DDh,06Eh
		defb	0E6h,0DDh,066h,0E7h,0E5h,0DDh,06Eh
		defb	0E4h,0DDh,066h,0E5h,0E5h,001h,080h
		defb	03Fh,06Fh,067h,0CDh
		defw	LWRD ?F_DIV_L04
		defb	0DDh,075h,0E4h,0DDh,074h,0E5h,0DDh
		defb	071h,0E6h,0DDh,070h,0E7h,0DDh,04Eh
		defb	0E6h,0DDh,046h,0E7h,0DDh,05Eh,0E4h
		defb	0DDh,056h,0E5h,0FDh,0E5h,0E1h,0CDh
		defw	LWRD ?__daddexp_L11
		defb	021h,00Ah,000h,039h,0CDh
		defw	LWRD ?F_MULASG_L04
		defb	0DDh,07Eh,0FAh,0DDh,0B6h,0FBh,028h
		defb	00Bh,0EBh,079h,0B0h,028h,012h,078h
		defb	0EEh,080h,047h,018h,00Ch,0DDh,04Eh
		defb	0EEh,0DDh,046h,0EFh,0DDh,06Eh,0ECh
		defb	0DDh,066h,0EDh,0FDh,0E1h,0C3h
		defw	LWRD ?BANK_LEAVE_32_L08
		defb	0DDh,04Eh,00Ch,0DDh,046h,00Dh,0DDh
		defb	06Eh,00Ah,0DDh,066h,00Bh,0C3h
		defw	LWRD ?F_TO_L_L04
		defb	0DDh,04Eh,00Ch,0DDh,046h,00Dh,0DDh
		defb	06Eh,00Ah,0DDh,066h,00Bh,0C3h
		defw	LWRD ?F_SUB_L04
		defb	0DDh,04Eh,0EAh,0DDh,046h,0EBh,0DDh
		defb	05Eh,0E8h,0DDh,056h,0E9h,0C3h
		defw	LWRD ?F_MULASG_L04
		endmod

; -----------------------------------------------------------------------------

	end
