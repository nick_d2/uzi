; FENDASG2.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?F_END_ASG2_L04
		rseg	RCODE
rcode_base:
		public	?F_END_ASG2_L04
?F_END_ASG2_L04	equ	rcode_base+00000000h
		defb	0EBh,070h,02Bh,071h,02Bh,072h,02Bh
		defb	073h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
