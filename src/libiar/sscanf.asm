; sscanf.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	sscanf
		rseg	CODE
code_base:
		extern	_formatted_read
		public	sscanf
sscanf		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0FEh,0FFh,0FDh,0E5h,0DDh,06Eh,00Ah
		defb	0DDh,066h,00Bh,0AFh,0B6h,028h,026h
		defb	021h,010h,000h,039h,001h,002h,000h
		defb	009h,0DDh,075h,0FEh,0DDh,074h,0FFh
		defb	060h,069h,039h,0E5h,021h,012h,000h
		defb	039h,04Dh,044h,02Bh,02Bh,0EBh,03Eh
		defb	BYTE3 _formatted_read
		defb	021h
		defw	LWRD _formatted_read
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0F1h,018h,003h,021h,0FFh,0FFh,0FDh
		defb	0E1h,0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
