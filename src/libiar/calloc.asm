; calloc.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	calloc
		rseg	CODE
code_base:
		public	calloc
calloc		equ	code_base+00000000h
		extern	malloc
		extern	?CL64180B_4_06_L00
		extern	?S_MUL_L02
		extern	?L_INC_L03
		extern	?L_DEC_L03
		extern	?L_DECASG_L03
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0FDh,0E5h,0DDh,0E5h,0C5h,0D5h,0F5h
		defb	0F5h,0CDh
		defw	LWRD ?S_MUL_L02
		defb	0EBh,001h,000h,000h,0E5h,021h,002h
		defb	000h,039h,0D1h,073h,023h,072h,023h
		defb	071h,023h,070h,03Eh
		defb	BYTE3 malloc
		defb	021h
		defw	LWRD malloc
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0E5h,0FDh,0E1h,0E5h,0DDh,0E1h,07Dh
		defb	0B4h,028h,019h,021h,000h,000h,039h
		defb	0CDh
		defw	LWRD ?L_DECASG_L03
		defb	0EBh,0CDh
		defw	LWRD ?L_INC_L03
		defb	07Dh,0B4h,0B1h,0B0h,028h,008h,0DDh
		defb	036h,000h,000h,0DDh,023h,018h,0E7h
		defb	0FDh,0E5h,0E1h,0F1h,0F1h,0F1h,0F1h
		defb	0DDh,0E1h,0FDh,0E1h,0C3h
		defw	LWRD ?BANK_FAST_LEAVE_L08
		endmod

; -----------------------------------------------------------------------------

	end
