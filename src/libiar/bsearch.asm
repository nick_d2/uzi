; bsearch.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	bsearch
		rseg	CODE
code_base:
		public	bsearch
bsearch		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?S_MUL_L02
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0FAh,0FFh,0FDh,0E5h,0DDh,071h,0FCh
		defb	0DDh,070h,0FDh,0DDh,07Eh,00Ah,0DDh
		defb	0B6h,00Bh,0CAh
		defw	LWRD code_base+00096h
		defb	0DDh,06Eh,00Ah,0DDh,066h,00Bh,0CBh
		defb	03Ch,0CBh,01Dh,0E5h,0FDh,0E1h,04Dh
		defb	044h,0DDh,05Eh,00Ch,0DDh,056h,00Dh
		defb	0CDh
		defw	LWRD ?S_MUL_L02
		defb	0DDh,06Eh,0FCh,0DDh,066h,0FDh,019h
		defb	0DDh,075h,0FAh,0DDh,074h,0FBh,04Dh
		defb	044h,0DDh,05Eh,002h,0DDh,056h,003h
		defb	0DDh,07Eh,010h,0DDh,06Eh,00Eh,0DDh
		defb	066h,00Fh,0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	07Dh,0B4h,020h,008h,0DDh,06Eh,0FAh
		defb	0DDh,066h,0FBh,018h,03Dh,0CBh,07Ch
		defb	028h,00Bh,0FDh,0E5h,0E1h,0DDh,075h
		defb	00Ah,0DDh,074h,00Bh,018h,0A3h,0DDh
		defb	06Eh,00Ch,0DDh,066h,00Dh,0DDh,04Eh
		defb	0FAh,0DDh,046h,0FBh,009h,0DDh,075h
		defb	0FCh,0DDh,074h,0FDh,0FDh,0E5h,0C1h
		defb	021h,0FFh,0FFh,0A7h
		defb	0EDh,042h,04Dh,044h,021h,012h,000h
		defb	039h,07Eh,081h,077h,023h,07Eh,088h
		defb	077h,0C3h
		defw	LWRD code_base+0000Dh
		defb	06Fh,067h,0FDh,0E1h,0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
