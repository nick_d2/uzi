; realloc.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	realloc
                             00000000 "busy" [0072]    00000001 "next" [0066]
		rseg	CODE
code_base:
		extern	_heap_of_memory
		extern	_last_heap_object
		extern	_make_new_mem_hole
		extern	_top_of_heap
		extern	free
		extern	malloc
		public	realloc
realloc		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0F2h,0FFh,0FDh,0E5h,02Ah
		defw	LWRD _heap_of_memory
		defb	0DDh,075h,0F6h,0DDh,074h,0F7h,0DDh
		defb	06Eh,002h,0DDh,066h,003h,0DDh,075h
		defb	0F2h,0DDh,074h,0F3h,07Dh,0B4h,020h
		defb	00Dh,059h,050h,03Eh
		defb	BYTE3 malloc
		defb	021h
		defw	LWRD malloc
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0C3h
		defw	LWRD code_base+00245h
		defb	079h,0B0h,020h,00Dh,05Dh,054h,03Eh
		defb	BYTE3 free
		defb	021h
		defw	LWRD free
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0C3h
		defw	LWRD code_base+00242h
		defb	0EDh,04Bh
		defw	LWRD _last_heap_object
		defb	0DDh,06Eh,0F6h,0DDh,066h,0F7h,0A7h
		defb	0EDh,042h,0D2h
		defw	LWRD code_base+00242h
		defb	0DDh,06Eh,0F6h,0DDh,066h,0F7h,023h
		defb	023h,023h,0DDh,04Eh,0F2h,0DDh,046h
		defb	0F3h,0A7h,0EDh,042h,0DDh,06Eh,0F6h
		defb	0DDh,066h,0F7h,0C2h
		defw	LWRD code_base+00235h
		defb	0E5h,0FDh,0E1h,0AFh,0B6h,0CAh
		defw	LWRD code_base+00242h
		defb	0FDh,06Eh,001h,0FDh,066h,002h,0EDh
		defb	042h,04Dh,044h,0DDh,06Eh,004h,0DDh
		defb	066h,005h,0A7h,0EDh,042h,030h,06Eh
		defb	0FDh,06Eh,001h,0FDh,066h,002h,0EDh
		defb	04Bh
		defw	LWRD _last_heap_object
		defb	0A7h,0EDh,042h,020h,018h,0DDh,06Eh
		defb	004h,0DDh,066h,005h,0DDh,04Eh,0F2h
		defb	0DDh,046h,0F3h,009h,0FDh,075h,001h
		defb	0FDh,074h,002h,022h
		defw	LWRD _last_heap_object
		defb	018h,044h,0FDh,06Eh,001h,0DDh,075h
		defb	0F4h,0FDh,066h,002h,0DDh,074h,0F5h
		defb	0AFh,0B6h,0DDh,06Eh,004h,0DDh,066h
		defb	005h,0DDh,04Eh,0F2h,0DDh,046h,0F3h
		defb	009h,020h,024h,0FDh,075h,001h,0FDh
		defb	074h,002h,0FDh,06Eh,001h,0FDh,066h
		defb	002h,0E5h,0FDh,0E1h,0FDh,077h,000h
		defb	0DDh,06Eh,0F4h,0DDh
		defb	066h,0F5h,023h,046h,0FDh,070h,001h
		defb	023h,066h,0FDh,074h,002h,018h,003h
		defb	0CDh
		defw	LWRD code_base+0024Ah
		defb	0C3h
		defw	LWRD code_base+0022Dh
		defb	0DDh,04Eh,0F2h,0DDh,046h,0F3h,0FDh
		defb	06Eh,001h,0FDh,066h,002h,0EDh,042h
		defb	0DDh,04Eh,004h,0DDh,046h,005h,0A7h
		defb	0EDh,042h,0D2h
		defw	LWRD code_base+0022Dh
		defb	0FDh,06Eh,001h,0FDh,066h,002h,0EDh
		defb	04Bh
		defw	LWRD _last_heap_object
		defb	0A7h,0EDh,042h,020h,035h,0DDh,06Eh
		defb	004h,0DDh,066h,005h,0DDh,04Eh,0F2h
		defb	0DDh,046h,0F3h,009h,04Dh,044h,02Ah
		defw	LWRD _top_of_heap
		defb	0A7h,0EDh,042h,038h,018h,0DDh,06Eh
		defb	004h,0DDh,066h,005h,0DDh,04Eh,0F2h
		defb	0DDh,046h,0F3h,009h,0FDh,075h,001h
		defb	0FDh,074h,002h,022h
		defw	LWRD _last_heap_object
		defb	018h,003h,0C3h
		defw	LWRD code_base+00242h
		defb	0C3h
		defw	LWRD code_base+0022Dh
		defb	0FDh,06Eh,001h,0DDh,075h,0F4h,0FDh
		defb	066h,002h,0DDh,074h,0F5h,0AFh,0B6h
		defb	020h,03Dh,0DDh,04Eh,0F2h,0DDh,046h
		defb	0F3h,023h,056h,023h,066h,06Ah,0EDh
		defb	042h,04Dh,044h,0DDh,06Eh,004h,0DDh
		defb	066h,005h,0A7h,0EDh,042h,030h,023h
		defb	0DDh,06Eh,0F4h,0DDh,066h,0F5h,023h
		defb	046h,0FDh,070h,001h
		defb	023h,066h,0FDh,074h,002h,0DDh,06Eh
		defb	004h,0DDh,066h,005h,0DDh,04Eh,0F2h
		defb	0DDh,046h,0F3h,009h,0CDh
		defw	LWRD code_base+0024Ah
		defb	0C3h
		defw	LWRD code_base+0022Dh
		defb	0DDh,05Eh,004h,0DDh,056h,005h,03Eh
		defb	BYTE3 malloc
		defb	021h
		defw	LWRD malloc
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0DDh,075h,0FAh,0DDh,074h,0FBh,07Dh
		defb	0B4h,028h,074h,0DDh,04Eh,0F2h,0DDh
		defb	046h,0F3h,0FDh,06Eh,001h,0FDh,066h
		defb	002h,0EDh,042h,0DDh,075h,0F8h,0DDh
		defb	074h,0F9h,0DDh,06Eh,0FAh,0DDh,066h
		defb	0FBh,0DDh,075h,0FCh,0DDh,074h,0FDh
		defb	0DDh,071h,0FEh,0DDh,070h,0FFh,0DDh
		defb	06Eh,0F8h,0DDh,066h
		defb	0F9h,02Bh,0DDh,075h,0F8h,0DDh,074h
		defb	0F9h,023h,07Dh,0B4h,028h,020h,0DDh
		defb	06Eh,0FEh,0DDh,066h,0FFh,023h,0DDh
		defb	075h,0FEh,0DDh,074h,0FFh,02Bh,046h
		defb	0DDh,06Eh,0FCh,0DDh,066h,0FDh,023h
		defb	0DDh,075h,0FCh,0DDh,074h,0FDh,02Bh
		defb	070h,018h,0CEh,0DDh,05Eh,0F2h,0DDh
		defb	056h,0F3h,03Eh
		defb	BYTE3 free
		defb	021h
		defw	LWRD free
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0DDh,06Eh,0FAh,0DDh,066h,0FBh,0DDh
		defb	075h,0F2h,0DDh,074h,0F3h,018h,002h
		defb	018h,015h,0DDh,06Eh,0F2h,0DDh,066h
		defb	0F3h,018h,010h,023h,046h,0DDh,070h
		defb	0F6h,023h,066h,0DDh,074h,0F7h,0C3h
		defw	LWRD code_base+0003Eh
		defb	021h,000h,000h,0FDh,0E1h,0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		defb	04Dh,044h,0FDh,0E5h,0D1h,03Eh
		defb	BYTE3 _make_new_mem_hole
		defb	021h
		defw	LWRD _make_new_mem_hole
		defb	0C3h
		defw	LWRD ?BANK_CALL_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
