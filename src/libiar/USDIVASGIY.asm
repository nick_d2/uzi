; USDIVASGIY.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?US_DIVASG_IY_L12
		rseg	RCODE
rcode_base:
		public	?US_DIVASG_IY_L12
?US_DIVASG_IY_L12	equ	rcode_base+00000000h
		extern	?US_DIV_L02
		defb	0D5h,0FDh,0E5h,0D1h,0CDh
		defw	LWRD ?US_DIV_L02
		defb	0D5h,0EBh,0FDh,0E1h,0D1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
