; LSSWITCH.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?L_S_SWITCH_L06
		rseg	RCODE
rcode_base:
		public	?L_S_SWITCH_L06
?L_S_SWITCH_L06	equ	rcode_base+00000000h
		extern	?S_SWITCH_END_L06
		defb	0E3h,0F5h,0D5h,0C5h,07Bh,096h,023h
		defb	05Fh,07Ah,09Eh,023h,057h,079h,09Eh
		defb	023h,04Fh,078h,09Eh,023h,047h,07Bh
		defb	096h,023h,07Ah,09Eh,023h,079h,0DEh
		defb	000h,078h,0DEh,000h,0C1h,0C3h
		defw	LWRD ?S_SWITCH_END_L06
		endmod

; -----------------------------------------------------------------------------

	end
