; heap.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	heap
		rseg	CODE
code_base:
		rseg	CONST
const_base:
		rseg	UDATA0
udata0_base:
		rseg	IDATA0
idata0_base:
		rseg	CDATA0
cdata0_base:
    t_symbol_def         PUBLIC_REL (0004) 00000000 '_heap_of_memory'
    t_symbol_def         PUBLIC_REL (000B) 00000000 '_last_heap_object'
    t_symbol_def         PUBLIC_REL (0004) 00000002 '_top_of_heap'
		extern	?CL64180B_4_06_L00
    t_org_rel            (04) 00000000
    t_rel_p16            (0A) 0000
    t_rel_p16            (0A) 07CC
    t_org_rel            (0A) 00000000
    t_org_rel            (0A) 000007D0
    t_org_rel            (0B) 00000000
    t_org_rel            (0B) 00000002
    t_org_rel            (0C) 00000000
    t_rel_p16            (0A) 0000
		endmod

; -----------------------------------------------------------------------------

	end
