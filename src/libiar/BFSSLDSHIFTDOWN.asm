; BFSSLDSHIFTDOWN.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?BF_SS_LD_SHIFT_DOWN_L10
		rseg	RCODE
rcode_base:
		public	?BF_SS_LD_SHIFT_DOWN_L10
?BF_SS_LD_SHIFT_DOWN_L10	equ	rcode_base+00000000h
		extern	?BF_MASKED_LD_L10
		extern	?BF_SS_EXT_L10
		extern	?SS_RSH_L02
		defb	0C5h,0CDh
		defw	LWRD ?BF_MASKED_LD_L10
		defb	0CDh
		defw	LWRD ?BF_SS_EXT_L10
		defb	0DDh,07Eh,000h,0E6h,00Fh,047h,0CDh
		defw	LWRD ?SS_RSH_L02
		defb	0C1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
