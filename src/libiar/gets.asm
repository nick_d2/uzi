; gets.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	gets
		rseg	CODE
code_base:
		extern	getchar
		public	gets
gets		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0C5h,0FDh,0E5h,0DDh,0E5h,0F5h,0D5h
		defb	0FDh,0E1h,0D5h,0DDh,0E1h,03Eh
		defb	BYTE3 getchar
		defb	021h
		defw	LWRD getchar
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	04Dh,044h,03Eh,00Ah,0A9h,0B0h,028h
		defb	00Ch,079h,0A0h,03Ch,028h,007h,0DDh
		defb	071h,000h,0DDh,023h,018h,0E4h,079h
		defb	0A0h,03Ch,020h,00Ah,0FDh,0E5h,0E1h
		defb	0DDh,0E5h,0C1h,0EDh,042h,028h,007h
		defb	0DDh,036h,000h,000h,0FDh,0E5h,0E1h
		defb	0F1h,0DDh,0E1h,0FDh,0E1h,0C1h,0C3h
		defw	LWRD ?BANK_FAST_LEAVE_L08
		endmod

; -----------------------------------------------------------------------------

	end
