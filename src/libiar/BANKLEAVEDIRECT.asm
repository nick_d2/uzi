; BANKLEAVEDIRECT.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

	module	?BANK_LEAVE_DIRECT_L08

 .if 1
BBR	equ	39h
 .endif

	rseg	RCODE

	public	?BANK_LEAVE_DIRECT_L08

?BANK_LEAVE_DIRECT_L08:
 .if 1
	ld	sp,ix
	pop	ix
	pop	de
	pop	bc
 .else
	defb	0DDh,0F9h,0DDh,0E1h,0D1h,0C1h
 .endif

	public	?BANK_FAST_LEAVE_L08

?BANK_FAST_LEAVE_L08:
 .if 1
	pop	af
	out0	(BBR),a
	ret
 .else
	defb	0F1h,0EDh,039h,039h,0C9h
 .endif

	endmod

; -----------------------------------------------------------------------------

	end
