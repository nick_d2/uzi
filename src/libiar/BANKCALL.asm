; BANKCALL.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?BANK_CALL_L08
		public	?BANK_CALL_L08
?BANK_CALL_L08	equ	00000028h
		extern	?BANK_CALL_DIRECT_L08
    t_org_abs            S86=0000 00000000
    t_org_abs            S86=0000 00000028
		defb	0C3h
		defw	LWRD ?BANK_CALL_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
