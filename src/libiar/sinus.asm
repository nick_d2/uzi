; sinus.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	sinus
		rseg	CODE
code_base:
		public	__sinus
__sinus		equ	code_base+00000000h
		extern	modf
		extern	?CL64180B_4_06_L00
		extern	?SL_CMP_L03
		extern	?F_MUL_L04
		extern	?F_ADD_L04
		extern	?F_SUB_L04
		extern	?F_TO_L_L04
		extern	?UL_TO_F_L04
		extern	?F_NEGASG_L04
		extern	?F_MULASG_L04
		extern	?F_ADDASG_L04
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_LEAVE_32_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0F0h,0FFh,0FDh,0E5h,0DDh,05Eh,00Ah
		defb	0CBh,078h,028h,004h,03Eh,002h,018h
		defb	001h,0AFh,083h,05Fh,0CBh,0B8h,0DDh
		defb	070h,005h,0D5h,021h,016h,000h,039h
		defb	001h,022h,03Fh,011h,083h,0F9h,0CDh
		defw	LWRD ?F_MULASG_L04
		defb	0D1h,0DDh,06Eh,004h,0DDh,066h,005h
		defb	0E5h,0DDh,06Eh,002h,0DDh,066h,003h
		defb	0E5h,001h,07Fh,047h,021h,000h,0FCh
		defb	0CDh
		defw	LWRD ?SL_CMP_L03
		defb	0D2h
		defw	LWRD code_base+000D1h
		defb	0D5h,021h,00Ch,000h,039h,0E5h,0DDh
		defb	04Eh,004h,0DDh,046h,005h,0DDh,05Eh
		defb	002h,0DDh,056h,003h,03Eh
		defb	BYTE3 modf
		defb	021h
		defw	LWRD modf
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0F1h,0DDh,075h,0F0h,0DDh,074h,0F1h
		defb	0DDh,071h,0F2h,0DDh,070h,0F3h,0D1h
		defb	021h,00Ah,000h,039h,0E5h,06Bh,001h
		defb	000h,000h,060h,0CDh
		defw	LWRD ?UL_TO_F_L04
		defb	0EBh,0E1h,0CDh
		defw	LWRD ?F_ADDASG_L04
		defb	021h,00Eh,000h,039h,0E5h,021h,080h
		defb	03Eh,0E5h,021h,000h,000h,0E5h,0DDh
		defb	04Eh,0FAh,0DDh,046h,0FBh,0DDh,06Eh
		defb	0F8h,0DDh,066h,0F9h,0CDh
		defw	LWRD ?F_MUL_L04
		defb	0EBh,03Eh
		defb	BYTE3 modf
		defb	021h
		defw	LWRD modf
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0F1h,021h,080h,040h,0E5h,021h,000h
		defb	000h,0E5h,0DDh,04Eh,0FEh,0DDh,046h
		defb	0FFh,0DDh,06Eh,0FCh,0DDh,066h,0FDh
		defb	0CDh
		defw	LWRD ?F_MUL_L04
		defb	0C5h,0E5h,0DDh,04Eh,0FAh,0DDh,046h
		defb	0FBh,0DDh,06Eh,0F8h,0DDh,066h,0F9h
		defb	0CDh
		defw	LWRD ?F_SUB_L04
		defb	0CDh
		defw	LWRD ?F_TO_L_L04
		defb	05Dh,018h,03Dh,0DDh,04Eh,004h,0DDh
		defb	046h,005h,0DDh,06Eh,002h,0DDh,066h
		defb	003h,0CDh
		defw	LWRD ?F_TO_L_L04
		defb	0E5h,0FDh,0E1h,001h,000h,000h,0CDh
		defw	LWRD ?UL_TO_F_L04
		defb	0C5h,0E5h,0DDh,04Eh,004h,0DDh,046h
		defb	005h,0DDh,06Eh,002h,0DDh,066h,003h
		defb	0CDh
		defw	LWRD ?F_SUB_L04
		defb	0DDh,075h,0F0h,0DDh,074h,0F1h,0DDh
		defb	071h,0F2h,0DDh,070h,0F3h,0FDh,0E5h
		defb	0E1h,07Bh,085h,0E6h,003h,05Fh,0CBh
		defb	043h,028h,023h,0DDh,06Eh,0F2h,0DDh
		defb	066h,0F3h,0E5h,0DDh,06Eh,0F0h,0DDh
		defb	066h,0F1h,0E5h,001h,080h,03Fh,021h
		defb	000h,000h,0CDh
		defw	LWRD ?F_SUB_L04
		defb	0DDh,075h,0F0h,0DDh,074h,0F1h,0DDh
		defb	071h,0F2h,0DDh,070h,0F3h,03Eh,001h
		defb	0BBh,030h,007h,021h,002h,000h,039h
		defb	0CDh
		defw	LWRD ?F_NEGASG_L04
		defb	0DDh,06Eh,0F2h,0DDh,066h,0F3h,0E5h
		defb	0DDh,06Eh,0F0h,0DDh,066h,0F1h,0E5h
		defb	0DDh,04Eh,0F2h,0DDh,046h,0F3h,0CDh
		defw	LWRD ?F_MUL_L04
		defb	0DDh,075h,0F4h,0DDh,074h,0F5h,0DDh
		defb	071h,0F6h,0DDh,070h,0F7h,021h,002h
		defb	000h,039h,0E5h,001h,01Eh,039h,0C5h
		defb	001h,0FBh,0D7h,0C5h,0CDh
		defw	LWRD code_base+001C5h
		defb	0C5h,0E5h,001h,099h,0BBh,021h,065h
		defb	026h,0CDh
		defw	LWRD ?F_ADD_L04
		defb	0C5h,0E5h,0CDh
		defw	LWRD code_base+001C5h
		defb	0C5h,0E5h,001h,0A3h,03Dh,021h,058h
		defb	034h,0CDh
		defw	LWRD ?F_ADD_L04
		defb	0C5h,0E5h,0CDh
		defw	LWRD code_base+001C5h
		defb	0C5h,0E5h,001h,025h,0BFh,021h,0E1h
		defb	05Dh,0CDh
		defw	LWRD ?F_ADD_L04
		defb	0C5h,0E5h,0CDh
		defw	LWRD code_base+001C5h
		defb	0C5h,0E5h,001h,0C9h,03Fh,021h,0DBh
		defb	00Fh,0CDh
		defw	LWRD ?F_ADD_L04
		defb	0EBh,0E1h,0CDh
		defw	LWRD ?F_MULASG_L04
		defb	0DDh,04Eh,0F2h,0DDh,046h,0F3h,0DDh
		defb	06Eh,0F0h,0DDh,066h,0F1h,0FDh,0E1h
		defb	0C3h
		defw	LWRD ?BANK_LEAVE_32_L08
		defb	0DDh,04Eh,0F6h,0DDh,046h,0F7h,0DDh
		defb	06Eh,0F4h,0DDh,066h,0F5h,0C3h
		defw	LWRD ?F_MUL_L04
		endmod

; -----------------------------------------------------------------------------

	end
