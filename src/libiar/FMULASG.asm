; FMULASG.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?F_MULASG_L04
		rseg	RCODE
rcode_base:
		public	?F_MULASG_L04
?F_MULASG_L04	equ	rcode_base+00000000h
		extern	?F_END_ASG2_L04
		extern	?F_MUL_L04
		defb	0C5h,0D5h,05Eh,023h,056h,023h,04Eh
		defb	023h,046h,0EBh,0CDh
		defw	LWRD ?F_MUL_L04
		defb	0C3h
		defw	LWRD ?F_END_ASG2_L04
		endmod

; -----------------------------------------------------------------------------

	end
