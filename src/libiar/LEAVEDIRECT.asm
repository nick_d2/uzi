; LEAVEDIRECT.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?LEAVE_DIRECT_L09
		rseg	RCODE
rcode_base:
		public	?LEAVE_DIRECT_L09
?LEAVE_DIRECT_L09	equ	rcode_base+00000000h
		defb	0DDh,0F9h,0DDh,0E1h,0D1h,0C1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
