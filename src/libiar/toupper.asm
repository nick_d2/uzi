; toupper.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	toupper
		rseg	CODE
code_base:
		public	toupper
toupper		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?SS_CMP_L02
		extern	?BANK_LEAVE_DIRECT_L08NDEFINED
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_PARM_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_PARM_DIRECT_L09
		defb	001h,061h,080h,0EBh,078h,0ACh,067h
		defb	0EDh,042h,038h,015h,0DDh,04Eh,002h
		defb	0DDh,046h,003h,021h,07Ah,000h,0CDh
		defw	LWRD ?SS_CMP_L02
		defb	038h,007h,079h,0EEh,020h,06Fh,060h
		defb	018h,006h,0DDh,06Eh,002h,0DDh,066h
		defb	003h,0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
