; BFUCLDSHIFTDOWN.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?BF_UC_LD_SHIFT_DOWN_L10
		rseg	RCODE
rcode_base:
		public	?BF_UC_LD_SHIFT_DOWN_L10
?BF_UC_LD_SHIFT_DOWN_L10	equ	rcode_base+00000000h
		extern	?UC_RSH_L01
		defb	047h,0C5h,07Eh,0A2h,04Fh,07Bh,0E6h
		defb	007h,047h,079h,0CDh
		defw	LWRD ?UC_RSH_L01
		defb	0C1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
