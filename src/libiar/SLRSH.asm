; SLRSH.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?SL_RSH_L03
		rseg	RCODE
rcode_base:
		public	?SL_RSH_L03
?SL_RSH_L03	equ	rcode_base+00000000h
		defb	0B7h,0C8h,0CBh,028h,0CBh,019h,0CBh
		defb	01Ch,0CBh,01Dh,03Dh,020h,0F5h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
