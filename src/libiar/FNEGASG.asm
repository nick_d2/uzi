; FNEGASG.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?F_NEGASG_L04
		rseg	RCODE
rcode_base:
		public	?F_NEGASG_L04
?F_NEGASG_L04	equ	rcode_base+00000000h
		defb	0F5h,05Eh,023h,056h,023h,04Eh,023h
		defb	046h,079h,0B0h,028h,005h,078h,0EEh
		defb	080h,047h,070h,02Bh,02Bh,02Bh,0F1h
		defb	0C9h
		endmod

; -----------------------------------------------------------------------------

	end
