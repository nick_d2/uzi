; strncpy.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	strncpy
		rseg	CODE
code_base:
		public	strncpy
strncpy		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0FEh,0FFh,0FDh,0E5h,0DDh,05Eh,00Ah
		defb	0DDh,056h,00Bh,0DDh,06Eh,002h,0DDh
		defb	066h,003h,0E5h,0FDh,0E1h,0DDh,075h
		defb	0FEh,0DDh,074h,0FFh,07Bh,062h,01Bh
		defb	0B4h,028h,026h,0DDh,06Eh,004h,0DDh
		defb	066h,005h,023h,0DDh,075h,004h,0DDh
		defb	074h,005h,02Bh,046h,0FDh,070h,000h
		defb	004h,005h,0FDh,023h
		defb	020h,0E2h,07Bh,062h,01Bh,0B4h,028h
		defb	008h,0FDh,036h,000h,000h,0FDh,023h
		defb	018h,0F2h,0DDh,06Eh,0FEh,0DDh,066h
		defb	0FFh,0FDh,0E1h,0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
