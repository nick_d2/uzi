; FDIV.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?F_DIV_L04
		rseg	RCODE
rcode_base:
		public	?F_DIV_L04
?F_DIV_L04	equ	rcode_base+00000000h
		extern	?F_UNPACK_L04
		extern	?F_PACK_L04
		extern	?F_OVERFLOW_L04
		extern	?F_UNDERFLOW2_L04
		extern	?F_UNDERFLOW_L04
		extern	?F_UP_ROUND_L04
		defb	0EBh,0E3h,0DDh,0E5h,0DDh,021h,002h
		defb	000h,0DDh,039h,0E5h,0F5h,0CDh
		defw	LWRD ?F_UNPACK_L04
		defb	01Fh,01Fh,0F5h,0AFh,0BDh,0CAh
		defw	LWRD ?F_UNDERFLOW_L04
		defb	0B4h,0CAh
		defw	LWRD ?F_OVERFLOW_L04
		defb	0D6h,07Fh,0D2h
 defw rcode_base+28h ;    t_push_rel           (0000) 00000028
		defb	0EDh,044h,085h,0DAh
		defw	LWRD ?F_OVERFLOW_L04
		defb	018h,009h,067h,07Dh,094h,0DAh
		defw	LWRD ?F_UNDERFLOW_L04
		defb	0CAh
		defw	LWRD ?F_UNDERFLOW_L04
		defb	0F5h,06Ah,061h,048h,006h,000h,0DDh
		defb	056h,004h,0DDh,05Eh,003h,0A7h,0EDh
		defb	052h,079h,0DDh,09Eh,005h,030h,00Fh
		defb	0DDh,035h,0F7h,0CAh
		defw	LWRD ?F_UNDERFLOW2_L04
		defb	0CBh,025h,0CBh,014h,017h,019h,0DDh
		defb	08Eh,005h,04Fh,0DDh,036h,0F6h,017h
		defb	0CBh,025h,0CBh,014h,0CBh,011h,0CBh
		defb	010h,0EDh,052h,079h,0DDh,09Eh,005h
		defb	04Fh,078h,0DEh,000h,047h,030h,008h
		defb	019h,079h,0DDh,08Eh,005h,04Fh,006h
		defb	000h,0DDh,0CBh,002h,016h,0DDh,0CBh
		defb	003h,016h,0DDh,0CBh
		defb	004h,016h,0DDh,035h,0F6h,020h,0D2h
		defb	079h,0CBh,025h,0CBh,014h,017h,0CBh
		defb	010h,020h,007h,0DDh,096h,005h,020h
		defb	002h,0EDh,052h,0DDh,07Eh,004h,02Fh
		defb	047h,0DDh,07Eh,003h,02Fh,04Fh,0DDh
		defb	07Eh,002h,02Fh,057h,01Eh,000h,0E1h
		defb	038h,007h,020h,002h,0CBh,042h,0C2h
		defw	LWRD ?F_UP_ROUND_L04
		defb	0C3h
		defw	LWRD ?F_PACK_L04
		endmod

; -----------------------------------------------------------------------------

	end
