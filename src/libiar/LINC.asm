; LINC.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?L_INC_L03
		rseg	RCODE
rcode_base:
		public	?L_INC_L03
?L_INC_L03	equ	rcode_base+00000000h
		defb	02Ch,0C0h,024h,0C0h,00Ch,0C0h,004h
		defb	0C9h
		endmod

; -----------------------------------------------------------------------------

	end
