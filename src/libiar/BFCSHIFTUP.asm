; BFCSHIFTUP.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?BF_C_SHIFT_UP_L10
		rseg	RCODE
rcode_base:
		public	?BF_C_SHIFT_UP_L10
?BF_C_SHIFT_UP_L10	equ	rcode_base+00000000h
		extern	?C_LSH_L01
		defb	04Fh,07Bh,0E6h,007h,047h,079h,0C3h
		defw	LWRD ?C_LSH_L01
		endmod

; -----------------------------------------------------------------------------

	end
