; MEMCMP.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?MEMCMP_L11
		rseg	RCODE
rcode_base:
		public	?MEMCMP_L11
?MEMCMP_L11	equ	rcode_base+00000000h
		defb	0F5h,0C5h,0D5h,079h,0B0h,028h,009h
		defb	01Ah,0EDh,0A1h,020h,009h,013h,0EAh
 defw rcode_base+7 ;    t_push_rel           (0000) 00000007
		defb	021h,000h,000h,018h,005h,02Bh,096h
		defb	06Fh,09Fh,067h,0D1h,0C1h,0F1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
