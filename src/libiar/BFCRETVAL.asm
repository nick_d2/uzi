; BFCRETVAL.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?BF_C_RET_VAL_L10
		rseg	RCODE
rcode_base:
		public	?BF_C_RET_VAL_L10
?BF_C_RET_VAL_L10	equ	rcode_base+00000000h
		extern	?BF_SC_EXT_L10
		extern	?SC_RSH_L01
		extern	?UC_RSH_L01
		defb	0A2h,04Fh,07Ah,02Fh,0A6h,0B1h,077h
		defb	0CBh,07Bh,028h,01Ch,0CBh,073h,0C4h
		defw	LWRD ?BF_SC_EXT_L10
		defb	07Bh,0E6h,007h,047h,079h,0CBh,073h
		defb	028h,007h,0CDh
		defw	LWRD ?SC_RSH_L01
		defb	0D1h,0C3h
 defw rcode_base+28h ;    t_push_rel           (0000) 00000028
		defb	0CDh
		defw	LWRD ?UC_RSH_L01
		defb	0D1h,0C3h
 defw rcode_base+28h ;    t_push_rel           (0000) 00000028
		defb	0F1h,0D1h,0C1h,0EBh,0E3h,0EBh,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
