; acos.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	acos
		rseg	CODE
code_base:
		public	acos
acos		equ	code_base+00000000h
		extern	asin
		extern	errno
		extern	?CL64180B_4_06_L00
		extern	?SL_CMP_L03
		extern	?F_SUB_L04
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?BANK_LEAVE_32_L08
		extern	?ENT_PARM_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_PARM_DIRECT_L09
		defb	0CBh,0B8h,0C5h,0D5h,001h,080h,03Fh
		defb	021h,000h,000h,0CDh
		defw	LWRD ?SL_CMP_L03
		defb	030h,00Dh,02Eh,021h,022h
		defw	LWRD errno
		defb	001h,07Fh,07Fh,021h,0FFh,0FFh,018h
		defb	019h,0DDh,04Eh,004h,0DDh,046h,005h
		defb	03Eh
		defb	BYTE3 asin
		defb	021h
		defw	LWRD asin
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0C5h,0E5h,001h,0C9h,03Fh,021h,0DBh
		defb	00Fh,0CDh
		defw	LWRD ?F_SUB_L04
		defb	0C3h
		defw	LWRD ?BANK_LEAVE_32_L08
		endmod

; -----------------------------------------------------------------------------

	end
