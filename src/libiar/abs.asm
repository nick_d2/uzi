; abs.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	abs
		rseg	CODE
code_base:
		public	abs
abs		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_PARM_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_PARM_DIRECT_L09
		defb	0CBh,07Ah,020h,003h,0EBh,018h,008h
		defb	04Bh,042h,0A7h,021h,000h,000h,0EDh
		defb	042h,0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
