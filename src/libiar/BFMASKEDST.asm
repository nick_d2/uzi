; BFMASKEDST.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?BF_MASKED_ST_L10
		rseg	RCODE
rcode_base:
		public	?BF_MASKED_ST_L10
?BF_MASKED_ST_L10	equ	rcode_base+00000000h
		defb	0DDh,04Eh,001h,0DDh,046h,002h,07Bh
		defb	0A1h,05Fh,07Ah,0A0h,057h,078h,02Fh
		defb	0A6h,0B2h,077h,02Bh,079h,02Fh,0A6h
		defb	0B3h,077h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
