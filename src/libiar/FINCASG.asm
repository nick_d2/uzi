; FINCASG.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?F_INCASG_L04
		rseg	RCODE
rcode_base:
		public	?F_INCASG_L04
?F_INCASG_L04	equ	rcode_base+00000000h
		extern	?F_INC_L04
		defb	05Eh,023h,056h,023h,04Eh,023h,046h
		defb	0EBh,0CDh
		defw	LWRD ?F_INC_L04
		defb	0EBh,070h,02Bh,071h,02Bh,072h,02Bh
		defb	073h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
