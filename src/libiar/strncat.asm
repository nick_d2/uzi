; strncat.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	strncat
		rseg	CODE
code_base:
		public	strncat
strncat		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	000h,000h,0FDh,0E5h,0DDh,05Eh,002h
		defb	0DDh,056h,003h,0D5h,0FDh,0E1h,06Bh
		defb	062h,013h,07Eh,0B7h,020h,0F9h,01Bh
		defb	0DDh,06Eh,004h,0DDh,066h,005h,023h
		defb	0DDh,075h,004h,0DDh,074h,005h,02Bh
		defb	046h,06Bh,062h,013h,070h,004h,005h
		defb	028h,015h,0DDh,06Eh,00Ah,0DDh,066h
		defb	00Bh,02Bh,0DDh,075h
		defb	00Ah,0DDh,074h,00Bh,023h,07Dh,0B4h
		defb	020h,0D7h,01Bh,0EBh,077h,0FDh,0E5h
		defb	0E1h,0FDh,0E1h,0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
