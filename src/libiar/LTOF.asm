; LTOF.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?L_TO_F_L04
		rseg	RCODE
rcode_base:
		public	?SL_TO_F_L04
?SL_TO_F_L04	equ	rcode_base+00000000h
		public	?UL_TO_F_L04
?UL_TO_F_L04	equ	rcode_base+00000016h
		extern	?L_NOT_L03
		extern	?L_INC_L03
		defb	0F5h,0D5h,078h,0B1h,0B4h,0B5h,028h
		defb	038h,0AFh,0CBh,078h,028h,007h,0CDh
		defw	LWRD ?L_NOT_L03
		defb	0CDh
		defw	LWRD ?L_INC_L03
		defb	03Ch,018h,009h,0F5h,0D5h,078h,0B1h
		defb	0B4h,0B5h,028h,022h,0AFh,058h,006h
		defb	01Fh,0CBh,07Bh,020h,00Ah,0CBh,025h
		defb	0CBh,014h,0CBh,011h,0CBh,013h,010h
		defb	0F2h,057h,078h,0C6h,07Fh,047h,06Ch
		defb	061h,04Bh,0CBh,01Ah,0CBh,018h,038h
		defb	002h,0CBh,0B9h,0D1h,0F1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
