; BFSSHIFTUP.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?BF_S_SHIFT_UP_L10
		rseg	RCODE
rcode_base:
		public	?BF_S_SHIFT_UP_L10
?BF_S_SHIFT_UP_L10	equ	rcode_base+00000000h
		extern	?S_LSH_L02
		defb	0DDh,07Eh,000h,0E6h,00Fh,047h,0C3h
		defw	LWRD ?S_LSH_L02
		endmod

; -----------------------------------------------------------------------------

	end
