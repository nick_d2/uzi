; strtoul.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	strtoul
		rseg	CODE
code_base:
		extern	_Small_Ctype
		extern	errno
		public	strtoul
strtoul		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?S_MUL_L02
		extern	?SS_CMP_L02
		extern	?L_MUL_L03
		extern	?L_OR_L03
		extern	?L_NEG_L03
		extern	?BANK_LEAVE_32_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0EEh,0FFh,0FDh,0E5h,0DDh,06Eh,00Ah
		defb	0DDh,066h,00Bh,0E5h,0FDh,0E1h,0DDh
		defb	06Eh,002h,0DDh,066h,003h,0DDh,075h
		defb	0EEh,0DDh,074h,0EFh,0AFh,0DDh,077h
		defb	0F5h,0DDh,077h,0F6h,0DDh,077h,0F7h
		defb	0DDh,077h,0F8h,0DDh,06Eh,0EEh,0DDh
		defb	066h,0EFh,04Eh,006h,000h,021h
		defw	LWRD _Small_Ctype 0001
		defb	009h,0CBh,05Eh,028h,00Ah,0DDh,034h
		defb	0EEh,020h,0EAh,0DDh,034h,0EFh,018h
		defb	0E5h,0DDh,06Eh,0EEh,0DDh,066h,0EFh
		defb	07Eh,0FEh,02Bh,028h,005h,07Eh,0FEh
		defb	02Dh,020h,00Bh,023h,0DDh,075h,0EEh
		defb	0DDh,074h,0EFh,02Bh,07Eh,018h,002h
		defb	03Eh,02Bh,0DDh,077h,0F2h,0FDh,0E5h
		defb	0E1h,0CBh,07Ch,020h
		defb	011h,03Eh,001h,0ADh,0B4h,028h,00Bh
		defb	0FDh,0E5h,0C1h,021h,024h,000h,0CDh
		defw	LWRD ?SS_CMP_L02
		defb	030h,021h,0DDh,07Eh,004h,0DDh,0B6h
		defb	005h,028h,011h,0DDh,06Eh,002h,0DDh
		defb	066h,003h,0E5h,0DDh,06Eh,004h,0DDh
		defb	066h,005h,0C1h,071h,023h,070h,001h
		defb	000h,000h,069h,060h,0C3h
		defw	LWRD code_base+00269h
		defb	0FDh,0E5h,0E1h,07Dh,0B4h,0DDh,06Eh
		defb	0EEh,0DDh,066h,0EFh,07Eh,020h,02Eh
		defb	0FEh,030h,020h,024h,023h,07Eh,0FEh
		defb	078h,028h,005h,07Eh,0FEh,058h,020h
		defb	013h,021h,002h,000h,039h,07Eh,0C6h
		defb	002h,077h,023h,07Eh,0CEh,000h,077h
		defb	0FDh,021h,010h,000h,018h,037h,0FDh
		defb	021h,008h,000h,018h
		defb	031h,0FDh,021h,00Ah,000h,018h,02Bh
		defb	0FEh,030h,020h,027h,0FDh,0E5h,0E1h
		defb	03Eh,010h,0ADh,0B4h,020h,01Eh,0DDh
		defb	06Eh,0EEh,0DDh,066h,0EFh,023h,07Eh
		defb	0FEh,078h,028h,005h,07Eh,0FEh,058h
		defb	020h,00Dh,021h,002h,000h,039h,07Eh
		defb	0C6h,002h,077h,023h,07Eh,0CEh,000h
		defb	077h,0DDh,06Eh,0EEh
		defb	0DDh,066h,0EFh,0DDh,075h,0F0h,0DDh
		defb	074h,0F1h,0DDh,06Eh,0EEh,0DDh,066h
		defb	0EFh,07Eh,0FEh,030h,020h,00Ah,0DDh
		defb	034h,0EEh,020h,0F0h,0DDh,034h,0EFh
		defb	018h,0EBh,0DDh,06Eh,0EEh,0DDh,066h
		defb	0EFh,04Eh,006h,000h,021h
		defw	LWRD _Small_Ctype 0001
		defb	009h,07Eh,0E6h,007h,0CAh
		defw	LWRD code_base+00222h
		defb	0DDh,06Eh,0EEh,0DDh,066h,0EFh,04Eh
		defb	021h
		defw	LWRD _Small_Ctype 0001
		defb	009h,0CBh,056h,0DDh,06Eh,0EEh,0DDh
		defb	066h,0EFh,04Eh,028h,005h,021h,0D0h
		defb	0FFh,018h,01Ah,021h
		defw	LWRD _Small_Ctype 0001
		defb	009h,0CBh,046h,0DDh,06Eh,0EEh,0DDh
		defb	066h,0EFh,028h,006h,046h,0CBh,0E8h
		defb	068h,018h,001h,06Eh,026h,000h,001h
		defb	0A9h,0FFh,009h,0DDh,075h,0F3h,0DDh
		defb	074h,0F4h,0FDh,0E5h,0C1h,0CDh
		defw	LWRD ?SS_CMP_L02
		defb	0D2h
		defw	LWRD code_base+00222h
		defb	0DDh,04Eh,0F7h,0DDh,06Eh,0F5h,0DDh
		defb	066h,0F6h,006h,000h,0C5h,0E5h,0FDh
		defb	0E5h,0E1h,07Ch,007h,09Fh,04Fh,041h
		defb	0CDh
		defw	LWRD ?L_MUL_L03
		defb	0C5h,0E5h,0DDh,06Eh,0F3h,0DDh,066h
		defb	0F4h,07Ch,007h,09Fh,05Fh,053h,0C1h
		defb	009h,0EBh,0C1h,0EDh,04Ah,04Dh,044h
		defb	0EBh,0DDh,075h,0F9h,0DDh,074h,0FAh
		defb	0DDh,071h,0FBh,0DDh,070h,0FCh,0DDh
		defb	05Eh,0F8h,016h,000h,0FDh,0E5h,0C1h
		defb	0CDh
		defw	LWRD ?S_MUL_L02
		defb	0DDh,073h,0FDh,0DDh,072h,0FEh,0DDh
		defb	06Eh,0FCh,026h,000h,04Ch,044h,0EBh
		defb	021h,011h,000h,039h,07Eh,083h,077h
		defb	023h,07Eh,08Ah,0DDh,06Eh,0FDh,067h
		defb	078h,0B4h,028h,016h,021h,022h,000h
		defb	022h
		defw	LWRD errno
		defb	006h,0FFh,0DDh,070h,0F5h,0DDh,070h
		defb	0F6h,0DDh,070h,0F7h,0DDh,070h,0F8h
		defb	018h,02Ah,045h,069h,065h,04Dh,0C5h
		defb	0E5h,0DDh,04Eh,0FBh,0DDh,06Eh,0F9h
		defb	0DDh,066h,0FAh,042h,0CDh
		defw	LWRD ?L_OR_L03
		defb	0DDh,075h,0F5h,0DDh,074h,0F6h,0DDh
		defb	071h,0F7h,0DDh,070h,0F8h,0DDh,034h
		defb	0EEh,020h,003h,0DDh,034h,0EFh,0C3h
		defw	LWRD code_base+00125h
		defb	0DDh,07Eh,004h,0DDh,0B6h,005h,028h
		defb	029h,0DDh,06Eh,0EEh,0DDh,066h,0EFh
		defb	0DDh,04Eh,0F0h,0DDh,046h,0F1h,0EDh
		defb	042h,020h,008h,0DDh,06Eh,002h,0DDh
		defb	066h,003h,018h,006h,0DDh,06Eh,0EEh
		defb	0DDh,066h,0EFh,0E5h,0DDh,06Eh,004h
		defb	0DDh,066h,005h,0C1h,071h,023h,070h
		defb	0DDh,07Eh,0F2h,0FEh
		defb	02Bh,0DDh,04Eh,0F7h,0DDh,046h,0F8h
		defb	0DDh,06Eh,0F5h,0DDh,066h,0F6h,028h
		defb	003h,0CDh
		defw	LWRD ?L_NEG_L03
		defb	0FDh,0E1h,0C3h
		defw	LWRD ?BANK_LEAVE_32_L08
		endmod

; -----------------------------------------------------------------------------

	end
