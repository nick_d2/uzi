; BFSMULASG.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?BF_S_MULASG_L10
		rseg	RCODE
rcode_base:
		public	?BF_S_MULASG_L10
?BF_S_MULASG_L10	equ	rcode_base+00000000h
		extern	?S_MUL_L02
		extern	?BF_S_RET_VAL_L10
		defb	0EBh,0E3h,0DDh,0E5h,0D5h,0E5h,0DDh
		defb	0E1h,023h,023h,023h,0E3h,0E5h,0F5h
		defb	0C5h,07Eh,0DDh,0A6h,001h,05Fh,023h
		defb	07Eh,0DDh,0A6h,002h,057h,0CDh
		defw	LWRD ?S_MUL_L02
		defb	0C3h
		defw	LWRD ?BF_S_RET_VAL_L10
		endmod

; -----------------------------------------------------------------------------

	end
