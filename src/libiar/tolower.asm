; tolower.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	tolower
		rseg	CODE
code_base:
		public	tolower
tolower		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?SS_CMP_L02
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_PARM_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_PARM_DIRECT_L09
		defb	001h,041h,080h,0EBh,078h,0ACh,067h
		defb	0EDh,042h,038h,014h,0DDh,04Eh,002h
		defb	0DDh,046h,003h,021h,05Ah,000h,0CDh
		defw	LWRD ?SS_CMP_L02
		defb	038h,006h,0CBh,0E9h,069h,060h,018h
		defb	006h,0DDh,06Eh,002h,0DDh,066h,003h
		defb	0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
