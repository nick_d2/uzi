; SSCMP.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?SS_CMP_L02
		rseg	RCODE
rcode_base:
		public	?SS_CMP_L02
?SS_CMP_L02	equ	rcode_base+00000000h
		defb	0C5h,0E5h,0F5h,07Ch,0EEh,080h,067h
		defb	078h,0EEh,080h,047h,0F1h,0A7h,0EDh
		defb	042h,0E1h,0C1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
