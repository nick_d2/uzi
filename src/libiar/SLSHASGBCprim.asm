; SLSHASGBCprim.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?S_LSHASG_BC_prim_L12
		rseg	RCODE
rcode_base:
		public	?S_LSHASG_BC_prim_L12
?S_LSHASG_BC_prim_L12	equ	rcode_base+00000000h
		defb	0D9h,0B7h,028h,007h,0CBh,021h,0CBh
		defb	010h,03Dh,020h,0F9h,0C5h,0D9h,0E1h
		defb	0C9h
		endmod

; -----------------------------------------------------------------------------

	end
