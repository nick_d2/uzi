; atol.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	atol
		rseg	CODE
code_base:
		extern	_Small_Ctype
		public	atol
atol		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?L_MUL_L03
		extern	?L_NEG_L03
		extern	?BANK_FAST_LEAVE_L08
		extern	?BANK_LEAVE_32_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0DDh,0E5h,0F5h,0F5h,0F5h,0D5h,0DDh
		defb	0E1h,021h,004h,000h,039h,036h,000h
		defb	0DDh,04Eh,000h,006h,000h,021h
		defw	LWRD _Small_Ctype 0001
		defb	009h,0CBh,05Eh,028h,004h,0DDh,023h
		defb	018h,0EFh,0DDh,07Eh,000h,0FEh,02Dh
		defb	020h,008h,021h,004h,000h,039h,036h
		defb	001h,018h,007h,0DDh,07Eh,000h,0FEh
		defb	02Bh,020h,002h,0DDh,023h,068h,060h
		defb	039h,0AFh,077h,023h,077h,023h,077h
		defb	023h,077h,0DDh,04Eh,000h,006h,000h
		defb	021h
		defw	LWRD _Small_Ctype 0001
		defb	009h,0CBh,056h,028h,044h,0DDh,06Eh
		defb	000h,048h,061h,0C5h,0E5h,060h,0C5h
		defb	02Eh,00Ah,0E5h,02Bh,02Bh,039h,05Eh
		defb	023h,056h,023h,04Eh,023h,046h,0EBh
		defb	0CDh
		defw	LWRD ?L_MUL_L03
		defb	059h,050h,0C1h,009h,0EBh,0C1h,0EDh
		defb	04Ah,0E5h,0D5h,021h,0D0h,0FFh,0C1h
		defb	009h,0EBh,021h,0FFh,0FFh,0C1h,0EDh
		defb	04Ah,04Dh,044h,0EBh,0E5h,021h,002h
		defb	000h,039h,0D1h,073h,023h,072h,023h
		defb	071h,023h,070h,0DDh,023h,018h,0AFh
		defb	021h,004h,000h,039h,0AFh,0B6h,068h
		defb	060h,039h,05Eh,023h
		defb	056h,023h,04Eh,023h,046h,0EBh,028h
		defb	003h,0CDh
		defw	LWRD ?L_NEG_L03
		defb	0F1h,0F1h,0F1h,0DDh,0E1h,0C3h
		defw	LWRD ?BANK_FAST_LEAVE_L08
		endmod

; -----------------------------------------------------------------------------

	end
