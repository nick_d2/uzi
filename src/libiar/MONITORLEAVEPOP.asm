; MONITORLEAVEPOP.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?MONITOR_LEAVE_POP_L09
		rseg	RCODE
rcode_base:
		public	?MONITOR_LEAVE_POP_L09
?MONITOR_LEAVE_POP_L09	equ	rcode_base+00000000h
		defb	05Fh,0F1h,07Bh,0DDh,0F9h,0DDh,0E1h
		defb	0D1h,0C1h,0E0h,0FBh,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
