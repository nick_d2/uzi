; SSRSHASGDEprim.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?SS_RSHASG_DE_prim_L12
		rseg	RCODE
rcode_base:
		public	?SS_RSHASG_DE_prim_L12
?SS_RSHASG_DE_prim_L12	equ	rcode_base+00000000h
		defb	0D9h,0B7h,028h,007h,0CBh,02Ah,0CBh
		defb	01Bh,03Dh,020h,0F9h,0D5h,0D9h,0E1h
		defb	0C9h
		endmod

; -----------------------------------------------------------------------------

	end
