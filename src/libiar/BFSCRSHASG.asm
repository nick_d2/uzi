; BFSCRSHASG.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?BF_SC_RSHASG_L10
		rseg	RCODE
rcode_base:
		public	?BF_SC_RSHASG_L10
?BF_SC_RSHASG_L10	equ	rcode_base+00000000h
		extern	?BF_SC_EXT_L10
		extern	?SC_RSH_L01
		extern	?BF_C_RET_VAL_L10
		defb	0EBh,0E3h,0C5h,0D5h,05Eh,023h,056h
		defb	023h,0E3h,0F5h,0C5h,07Eh,0A2h,04Fh
		defb	0CDh
		defw	LWRD ?BF_SC_EXT_L10
		defb	0C1h,0CDh
		defw	LWRD ?SC_RSH_L01
		defb	0C3h
		defw	LWRD ?BF_C_RET_VAL_L10
		endmod

; -----------------------------------------------------------------------------

	end
