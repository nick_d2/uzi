; vsprintf.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	vsprintf
		rseg	CODE
code_base:
		extern	_formatted_write
		public	vsprintf
vsprintf	equ	code_base+00000012h
		extern	?CL64180B_4_06_L00
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_PARM_DIRECT_L09
		extern	?ENT_AUTO_DIRECT_L09
		defb	0C5h,069h,060h,04Eh,023h,046h,003h
		defb	070h,02Bh,071h,00Bh,069h,060h,073h
		defb	0E1h,0C3h
		defw	LWRD ?BANK_FAST_LEAVE_L08
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	000h,000h,0FDh,0E5h,0DDh,06Eh,00Ah
		defb	0DDh,066h,00Bh,0E5h,021h,006h,000h
		defb	039h,0E5h,01Eh
		defb	BYTE3 code_base+0000000h
		defb	0D5h,021h
		defw	LWRD code_base+00000h
		defb	0E5h,059h,050h,03Eh
		defb	BYTE3 _formatted_write
		defb	021h
		defw	LWRD _formatted_write
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0F1h,0F1h,0F1h,0F1h,0E5h,0FDh,0E1h
		defb	0DDh,06Eh,002h,0DDh,066h,003h,036h
		defb	000h,0FDh,0E5h,0E1h,0FDh,0E1h,0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
