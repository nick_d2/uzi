; strcspn.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	strcspn
		rseg	CODE
code_base:
		public	strcspn
strcspn		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	000h,000h,0FDh,0E5h,011h,000h,000h
		defb	0DDh,06Eh,002h,0DDh,066h,003h,0AFh
		defb	0B6h,028h,02Ah,0DDh,06Eh,004h,0DDh
		defb	066h,005h,0E5h,0FDh,0E1h,0AFh,0FDh
		defb	0B6h,000h,028h,010h,0DDh,06Eh,002h
		defb	0DDh,066h,003h,0FDh,07Eh,000h,0BEh
		defb	0FDh,023h,020h,0ECh,018h,00Bh,0DDh
		defb	034h,002h,020h,003h
		defb	0DDh,034h,003h,013h,018h,0CCh,0EBh
		defb	0FDh,0E1h,0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
