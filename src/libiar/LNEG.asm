; LNEG.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?L_NEG_L03
		rseg	RCODE
rcode_base:
		public	?L_NEG_L03
?L_NEG_L03	equ	rcode_base+00000000h
		defb	0F5h,0AFh,095h,06Fh,03Eh,000h,09Ch
		defb	067h,03Eh,000h,099h,04Fh,03Eh,000h
		defb	098h,047h,0F1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
