; USDIV.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?US_DIV_L02
		rseg	RCODE
rcode_base:
		public	?US_DIV_L02
?US_DIV_L02	equ	rcode_base+00000000h
		extern	?S_DIVMOD_L02
		defb	0F5h,0E5h,0CDh
		defw	LWRD ?S_DIVMOD_L02
		defb	07Bh,02Fh,05Fh,07Ah,02Fh,057h,0E1h
		defb	0F1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
