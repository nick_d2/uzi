; assert.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	assert
		rseg	CODE
code_base:
		rseg	CSTR
cstr_base:
		public	_Assert
_Assert		equ	cstr_base+00000000h
		extern	abort
		extern	printf
		extern	?CL64180B_4_06_L00
		extern	?BANK_CALL_DIRECT_L08
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_PARM_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_PARM_DIRECT_L09
		defb	0DDh,06Eh,00Ah,0DDh,066h,00Bh,0E5h
		defb	0C5h,0D5h,021h
    t_rel_p16            (01) 0000
		defb	0E5h,03Eh
		defb	BYTE3 printf
		defb	021h
		defw	LWRD printf
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0F1h,0F1h,0F1h,0F1h,03Eh
		defb	BYTE3 abort
		defb	021h
		defw	LWRD abort
		defb	0CDh
		defw	LWRD ?BANK_CALL_DIRECT_L08
		defb	0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
    t_org_rel            (01) 00000000
		defb	041h,073h,073h,065h,072h,074h,069h
		defb	06Fh,06Eh,020h,066h,061h,069h,06Ch
		defb	065h,064h,03Ah,020h,025h,073h,02Ch
		defb	020h,066h,069h,06Ch,065h,020h,025h
		defb	073h,02Ch,020h,06Ch,069h,06Eh,065h
		defb	020h,025h,064h,00Ah,000h
		endmod

; -----------------------------------------------------------------------------

	end
