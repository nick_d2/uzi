; BANKLEAVE.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?BANK_LEAVE_L08
		public	?BANK_LEAVE_L08
?BANK_LEAVE_L08	equ	00000018h
		extern	?BANK_LEAVE_DIRECT_L08
    t_org_abs            S86=0000 00000000
    t_org_abs            S86=0000 00000018
		defb	0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
