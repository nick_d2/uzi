; strlenlc.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	strlenlc
		rseg	CODE
code_base:
		public	strlen
strlen		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_PARM_DIRECT_L09
		extern	?STRLEN_L11
		defb	0CDh
		defw	LWRD ?ENT_PARM_DIRECT_L09
		defb	0EBh,0CDh
		defw	LWRD ?STRLEN_L11
		defb	0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
