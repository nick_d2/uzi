; ldiv.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	ldiv
                             00000000 "quot" [0066]    00000004 "rem" [0066]
		rseg	CODE
code_base:
		public	ldiv
ldiv		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?L_MUL_L03
		extern	?SL_DIV_L03
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	0F8h,0FFh,0DDh,06Eh,010h,0DDh,066h
		defb	011h,0E5h,0DDh,06Eh,00Eh,0DDh,066h
		defb	00Fh,0E5h,0DDh,04Eh,00Ch,0DDh,046h
		defb	00Dh,0DDh,06Eh,00Ah,0DDh,066h,00Bh
		defb	0CDh
		defw	LWRD ?SL_DIV_L03
		defb	0DDh,075h,0F8h,0DDh,074h,0F9h,0DDh
		defb	071h,0FAh,0DDh,070h,0FBh,0DDh,06Eh
		defb	010h,0DDh,066h,011h,0E5h,0DDh,06Eh
		defb	00Eh,0DDh,066h,00Fh,0E5h,0DDh,06Eh
		defb	0F8h,0DDh,066h,0F9h,0CDh
		defw	LWRD ?L_MUL_L03
		defb	059h,050h,04Dh,044h,0DDh,06Eh,00Ah
		defb	0DDh,066h,00Bh,0A7h,0EDh,042h,04Dh
		defb	044h,0DDh,06Eh,00Ch,0DDh,066h,00Dh
		defb	0EDh,052h,0E5h,0DDh,071h,0FCh,0DDh
		defb	070h,0FDh,0C1h,0DDh,071h,0FEh,0DDh
		defb	070h,0FFh,021h,000h,000h,039h,0DDh
		defb	05Eh,002h,0DDh,056h,003h,001h,008h
		defb	000h,0D5h,0EDh,0B0h
		defb	0E1h,0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
