; strspn.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	strspn
		rseg	CODE
code_base:
		public	strspn
strspn		equ	code_base+00000000h
		extern	?CL64180B_4_06_L00
		extern	?BANK_LEAVE_DIRECT_L08
		extern	?BANK_FAST_LEAVE_L08
		extern	?ENT_AUTO_DIRECT_L09
		defb	0CDh
		defw	LWRD ?ENT_AUTO_DIRECT_L09
		defb	000h,000h,0FDh,0E5h,0FDh,021h,000h
		defb	000h,0DDh,06Eh,002h,0DDh,066h,003h
		defb	0AFh,0B6h,028h,027h,0DDh,05Eh,004h
		defb	0DDh,056h,005h,06Bh,062h,0AFh,0B6h
		defb	028h,01Bh,0DDh,06Eh,002h,0DDh,066h
		defb	003h,0E5h,06Bh,062h,013h,07Eh,0E1h
		defb	0BEh,020h,0EBh,0DDh,034h,002h,0FDh
		defb	023h,020h,0D4h,0DDh
		defb	034h,003h,018h,0CFh,0FDh,0E5h,0E1h
		defb	0FDh,0E1h,0C3h
		defw	LWRD ?BANK_LEAVE_DIRECT_L08
		endmod

; -----------------------------------------------------------------------------

	end
