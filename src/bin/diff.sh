#!/bin/sh
diff --ignore-file-name-case --ignore-tab-expansion --ignore-space-change --ignore-blank-lines --unified --new-file --minimal --speed-large-files $1 $2
