@if .%1==. goto failure

@echo -k %2..\..\lib>%1.lnk
@echo -l libcb.lib>>%1.lnk
@echo -l libsysb.lib>>%1.lnk
@echo -l libiar.lib>>%1.lnk
@echo -m>>%1.lnk
@echo -u>>%1.lnk
@echo -i>>%1.lnk
@echo -o %1>>%1.lnk
@echo -bl RCODE=0x8100>>%1.lnk
@echo -bl CODE=0x4000,0x10000>>%1.lnk
@echo -bc CODE=0x4000>>%1.lnk
@echo %2..\..\lib\c0b.rel>>%1.lnk
@echo %1>>%1.lnk

@echo SUCCESS
@goto done

:failure
@echo usage: %0 filename
@echo.
@echo Writes link-z80 definition file "filename.lnk" for banked memory model,
@echo containing the needed commands to link "filename.rel" to "filename.i86".
@echo The generated file can then be manually edited to link further modules.
@echo Please note, any previously existing "filename.lnk" will be overwritten!
@echo.

:done

