/********** MSX-UZIX version of syscalls ************/
/* UNIX sysdebug(int onoff); */

#include "syscalls.h"

#define __str3(x)	__STRING(x)
#define __str2(x)	__str3(x)
#define __str1(x)	__str2(x)

#ifdef L_sysdebug
#ifndef HI_TECH_C
UNIX sysdebug(int onoff)
	{
#if 0
	return unix_set(SET_DEBUG, onoff);
#else
	return unix(7, SET_DEBUG, onoff);
#endif
	}
#else
	asm("	psect text,class=CODE");
        asm("   global __setset");
	asm("	global	_sysdebug");
	asm("	signat	_sysdebug,4154");
	asm("_sysdebug:");
	asm("	push de");
	asm("	ld de," __str1(SET_DEBUG));
	asm("	jp __setset");
#endif
#endif


