; sys1b.asm by Nick for UZI180, adapted from UZI280 syscalls.mac

; -----------------------------------------------------------------------------

	module	_sys1b

	extern	errno			; vendor _errno

	extern	?BANK_FAST_LEAVE_L08

; -----------------------------------------------------------------------------

	rseg	CODE

	public	unix_long		; vendor _unix_long

unix_long::
	ld	hl,4
	add	hl,sp			; assumes non-banked calling convention

	;push	bc
	push	de

	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	inc	hl			; de = callno (1st argument)

	ld	c,l
	ld	b,h			; bc -> stack frame (2nd argument)

	rst	30h			; call the kernel function dispatcher
	jr	nc,no_error		; no error, hl has return value already

	ld	(errno),hl		; vendor _errno
	ld	hl,-1			; dedicated return value meaning error
	ld	c,l
	ld	b,h			; sign extend for maximal politeness

no_error:
	pop	de
	;pop	bc			; bc:hl has syscall return value
	jp	?BANK_FAST_LEAVE_L08

; -----------------------------------------------------------------------------

	END
