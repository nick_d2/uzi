/********** MSX-UZIX version of syscalls ************/
/* UNIXV _exit(int val); */

#include "syscalls.h"

#ifdef L_exit
#ifndef HI_TECH_C
UNIXV _exit(int val)
	{
#if 0
 _AX = '~';
 geninterrupt(0x29);
 _AX = 'x';
 geninterrupt(0x29);
#endif
	unix(11, val);
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	__exit");
	asm("	signat	__exit,4152");
	asm("__exit:");
	asm("	ld hl,11");
	asm("	jp __sys__1");
#endif
#endif
