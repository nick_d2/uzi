; sys0b.asm by Nick for UZI180, adapted from UZI280 syscalls.mac

; -----------------------------------------------------------------------------

	module	_sys0b

	extern	errno			; vendor _errno

	extern	?BANK_FAST_LEAVE_L08

; -----------------------------------------------------------------------------

	rseg	CODE

	public	unix			; vendor _unix

unix::
	ld	hl,4
	add	hl,sp			; assumes banked calling convention

	push	bc
	push	de

	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	inc	hl			; de = callno (1st argument)

	ld	c,l
	ld	b,h			; bc -> stack frame (2nd argument)

	rst	30h			; call the kernel function dispatcher
	jr	nc,no_error		; no error, hl has return value already

	ld	(errno),hl		; vendor _errno
	ld	hl,-1			; dedicated return value meaning error

no_error:
	pop	de
	pop	bc
	jp	?BANK_FAST_LEAVE_L08

; -----------------------------------------------------------------------------

	END
