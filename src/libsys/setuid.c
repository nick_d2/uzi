/********** MSX-UZIX version of syscalls ************/
/* UNIX setuid(int uid); */

#include "syscalls.h"

#define __str3(x)	__STRING(x)
#define __str2(x)	__str3(x)
#define __str1(x)	__str2(x)

#ifdef L_setuid
#ifndef HI_TECH_C
UNIX setuid(int uid)
	{
#if 0
	return unix_set(SET_UID, uid);
#else
	return unix(7, SET_UID, uid);
#endif
	}
#else
	asm("	psect text,class=CODE");
        asm("   global __setset");
	asm("	global	_setuid");
	asm("	signat	_setuid,4154");
	asm("_setuid:");
	asm("	push de");
	asm("	ld de," __str1(SET_UID));
	asm("	jp __setset");
#endif
#endif


