/********** MSX-UZIX version of syscalls ************/
/* UNIX falign(int fd, int parm); */

#include "syscalls.h"

#ifdef L_falign
#ifndef HI_TECH_C
UNIX falign(int fd, int parm)
	{
	return unix(40, fd, parm);
	}
#else
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
	asm("	global	_falign");
	asm("	signat	_falign,8250");
	asm("_falign:");
	asm("	ld hl,13");
	asm("	jp __sys__2");
#endif
#endif
