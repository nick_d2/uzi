/********** MSX-UZIX version of syscalls ************/
/* UNIX creat(char *name, mode_t mode); */

#include "syscalls.h"
#include "fcntl.h"

/* this was not defined by syscalls.h, because it would break open.c */
/* here we need to define it as varargs so that IAR will not use regparms */
extern int open __P((char *name, uint flag, ...));

#ifdef L_creat
UNIX creat(char *name, mode_t mode) {
	return open(name,O_CREAT|O_WRONLY|O_TRUNC,mode);
}
#endif


