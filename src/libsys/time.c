/********** MSX-UZIX version of syscalls ************/
/* time_t time(time_t *tvec); */

#include "syscalls.h"

#ifdef L_time
#ifdef HI_TECH_C
	asm("	psect text,class=CODE");
	asm("	global __sys__, __sys__1, __sys__2, __ret__");
#endif

UNIX gtime(time_t *tvec);

#ifndef HI_TECH_C
UNIX gtime(time_t *tvec)
	{
	return unix(30, tvec);
	}
#else
	asm("_gtime:");
	asm("	ld hl,30");
	asm("	jp __sys__1");
#endif

time_t time(time_t *tvec) {
	time_t ttvec;
	
	if (tvec == NULL) {
		gtime(&ttvec);
		return ttvec;
	}
	gtime(tvec);
	return *tvec;
}
#endif
