/*
 *   cr.c  --  add cr's for cp/m editors 
 *
 *   syntax:
 *        cr file ...
 */

#include <stdio.h>
#ifndef VAX /* Nick */
#include <unistd.h>
#endif

int main(int argc, char **argv)
	{
	register char c;
#if 1 /* Nick */
	register char buf;
#endif
	FILE *in, *out;
	unsigned int j;
	char *s;
#if 1 /* Nick */
	int remove;
#endif

	if (argc < 2)
		{
		fprintf(stderr, "usage: %s [-r] file ...\n", argv[0]);
		exit(1);
		}

#if 1 /* Nick */
	remove = 0;
	for (j = 1; j < argc; j++)
		{
		s = argv[j];
		if (*s == '-')
			{
			while (*++s)
				{
				switch (*s)
					{
				case 'r':
					remove = 1;
					break;
				default:
					fprintf(stderr,
						"unknown switch: %s\n", s);
					exit(1);
					}
				}
			}
		else
			{
			break;
			}
		}

	for (; j < argc; j++)
#else
	for (j = 1; j < argc; j++)
#endif
  		{
		in = fopen(argv[j], "rb");
		if (in == NULL)
			{
			perror(argv[j]);
			exit(1);
			}
		s = tmpnam(NULL);
		out = fopen(s, "wb");
		if (out == NULL)
			{
			perror("Can't create temporary file");
			exit(1);
			}
#if 1 /* Nick */
		if (remove)
			{
			if ((buf = fgetc(in)) != EOF)
				{
				while ((c = fgetc(in)) != EOF)
					{
					if (buf != '\r' || c != '\n')
						{
						fputc(buf, out);
						}
					buf = c;
					}
				fputc(buf, out);
				}
			}
		else
			{
			while ((c = fgetc(in)) != EOF)
				{
				if (c == '\n')
					{
					fputc('\r', out);
					}
				fputc(c, out);
				}
			}
#else
		while ((c = fgetc(in)) != EOF)
			{
			if (c == '\n')
				{
				fputc('\r', out);
				}
			fputc(c, out);
			}
#endif
		fclose(in);
		fclose(out);

		if (unlink(argv[j]) < 0)
			{
			perror(argv[j]);
			exit(1);
			}
		if (rename(s, argv[j]) < 0)
			{
			perror(s);
			exit(1);
			}
   		}
	}
