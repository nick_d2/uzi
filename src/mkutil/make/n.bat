rem del WinRel /s /q
rem del WinDebug /s /q
rem del w32\subproc\WinRel /s /q
rem del w32\subproc\WinDebug /s /q

del config.h
nmake /f NMakefile
@if errorlevel 1 goto failure
copy WinDebug\make.exe ..\..\bin

@echo SUCCESS
@goto done
:failure
@echo FAILURE
:done

