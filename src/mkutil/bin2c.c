/* bin2c.c by Nick for UZI180 */

#include <stdio.h>
#include <fcntl.h>
#include <string.h>

#define PATHLEN 512

void main(int argc, char **argv)
	{
	int i, count, fdin;
	FILE *fileout;
	char *infile = NULL;
	char *outfile = NULL;
	char line[PATHLEN];
	char name[PATHLEN];
	char buffer[8];

	if (argc > 1)
		{
		infile = argv[1];
		}

	if (argc > 2)
		{
		outfile = argv[2];
		}

	if (infile == NULL)
		{
		printf("usage: bin2c infile.bin [outfile.c]\n");

		fflush(stdout);
		exit(1);
		}

	if (outfile == NULL)
		{
		outfile = name;
		strcpy(outfile, infile);

		i = strlen(outfile);
		while (i--)
			{
			if (outfile[i] == '\\')
				{
				break; /* no extension, so don't strip it */
				}
			if (outfile[i] == '.')
				{
				outfile[i] = 0; /* strip dot and extension */
				break; /* ready to concatenate our extension */
				}
			}

		strcat(outfile, ".c");
		}

	if (!strcmp(infile, outfile))
		{
		fprintf(stderr, "input and output filenames identical\n");

		fflush(stderr);
		exit(1);
		}

	fdin = open(infile, O_RDONLY | O_BINARY);
	if (fdin < 0)
		{
		fprintf(stderr, "can't open: ");
		perror(infile);

		fflush(stderr);
		exit(1);
		}

	fileout = fopen(outfile, "w");
	if (fileout == NULL)
		{
		fprintf(stderr, "can't create: ");
		perror(outfile);

		fflush(stderr);
		exit(1);
		}

	strcpy(line, "/* ");
	strcat(line, outfile);
	strcat(line, " generated from ");
	strcat(line, infile);
	strcat(line, ", do not edit! */\n");
	fputs(line, fileout);

	*line = 0; /* causes a blank line to be written after title */

	count = read(fdin, buffer, 8);
	while (count > 0)
		{
		strcat(line, "\n");
		fputs(line, fileout);

		*line = 0;
		for (i = 0; i < count; i++)
			{
			sprintf(&line[i*5], "0x%02x,", buffer[i] & 0xff);
			}

		count = read(fdin, buffer, 8);
		}

	if (*line)
		{
		line[strlen(line)-1] = 0; /* kill trailing comma */
		strcat(line, "\n");
		fputs(line, fileout);

		*line = 0; /* causes a blank line to be written at end */
		}

	strcat(line, "\n");
	fputs(line, fileout);

	fclose(fileout);
	close(fdin);

	printf("converted %s to %s\n", infile, outfile);

	fflush(stdout);
	exit(0);
	}

