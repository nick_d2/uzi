# /uzi/src/mkutil/make.d/host.mk by Nick for Hytech NOS/UZI project

# Enter with TOPSRCDIR = relative path to current directory, which will be
# blank if the project is at the same level as this host.mk file, for example
# /uzi/src/nos/ppp has an "include ../make.d/host.mk" with TOPSRCDIR = blank

# -----------------------------------------------------------------------------

TOPSRCDIR:=	$(TOPSRCDIR)../
TOPMKUDIR:=	$(TOPSRCDIR)

TOPSRCDIR:=	$(TOPSRCDIR)../
TOPMAKDIR:=	$(TOPSRCDIR)make.d/

BUILD=		build
ARCH=		w32

HOST:=		$(ARCH)

include $(TOPMAKDIR)/arch.mk

# -----------------------------------------------------------------------------

DEFINES=
INCDIRS=	.
LIBDIRS=
STDLIBS=

EXEOUT=		$(TOPSRCDIR)bin/

# without this setting make thinks tradcpp.h is out of date and clobbers it
HDROUT=		$(TOPMKUDIR)include/

# -----------------------------------------------------------------------------

