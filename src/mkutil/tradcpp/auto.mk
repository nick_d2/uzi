# auto.mk by Nick for Hytech NOS/UZI project

# -----------------------------------------------------------------------------

SUBDIRS=

OUTPUTS=		tradcpp$(EXEEXT)

tradcpp$(call CANON, $(EXEEXT))_SOURCES= \
			tradcpp.c tradcif.c cppdefault.c \
			mkdeps.c version.c \
			alloca.c hex.c lbasename.c safe-ctype.c \
			xexit.c xmalloc.c xstrdup.c

# -----------------------------------------------------------------------------

