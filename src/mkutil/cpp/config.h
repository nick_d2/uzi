/* config.h by Nick for GNU C preprocessor */

#define IN_GCC
/* #define __STDC__ 1 */
#define BITS_PER_UNIT 8

#define HAVE_LOCALE_H 1
#define HAVE_STDDEF_H 1
#define HAVE_STRING_H 1
#define HAVE_STRINGS_H 1
#define HAVE_STDLIB_H 1
#define HAVE_LIMITS_H 1
#define HAVE_TIME_H 1
#define HAVE_FCNTL_H 1
#define HAVE_MALLOC_H 1
#define HAVE_SYS_STAT_H 1

/* #define S_IFBLK -1 */
#define SIZEOF_LONG 4
#define BITS_PER_WORD 32

/* extra settings added by Nick */
#define HAVE_WINDOWS_H 1
#define HAVE_PARAM_H 1

#include <defaults.h>

