/* w32munge.c by Nick for Hytech */
/* note: under construction, it really needs a total rethink, see foster.c */

#include <stdio.h>
#ifdef _MSC_VER
#include <windows.h>
#else
#include <unistd.h>
#endif
#include <string.h>
#include <sys/stat.h>

#define COMMAND_MAX 0x800 /* cope with very long command lines */
#define PATHNAME_MAX 0x400 /* cope with very long path elements */
#define BASENAME_MAX 0x200 /* copy with very long filename stems */

int main(int argc, char **argv)
	{
	int i, j, k, quote_flag;
	int command_length, exitcode;
	int element_length, path_length;
	int basename_length, myself_length;
	int filename_length, target_length;
	char command[COMMAND_MAX];
	char element[PATHNAME_MAX], *path;
	char basename[BASENAME_MAX], myself[PATHNAME_MAX];
	char filename[PATHNAME_MAX], target[PATHNAME_MAX];
	struct stat mystatbuf, statbuf;
#ifdef _MSC_VER
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	DWORD dwExitCode;

	GetModuleFileName(NULL, myself, sizeof(myself));
	myself_length = strlen(myself);
#else
	myself_length = strlen(argv[0]);
	if (myself_length + 1> sizeof(myself))
		{
		fprintf(stderr, "invocation name too long\n");
		exit(1);
		}
	strcpy(myself, argv[0]);
#endif

	if (stat(myself, &mystatbuf) < 0)
		{
		fprintf(stderr, "%s: can't stat myself\n", myself);
		exit(1);
		}
/* printf("i'm %s inode %ld\n", myself, (long)mystatbuf.st_ino); */

	for (i = myself_length; i >= 0; i--)
		{
		if (myself[i] == ':' ||
#ifdef _MSC_VER
				myself[i] == '\\')
#else
				myself[i] == '/')
#endif
			{
			break;
			}
		}
	i++; /* skip separator, or restore i to 0 */

	basename_length = myself_length - i;
	if (basename_length < 1)
		{
		fprintf(stderr, "%s: null invocation name\n", myself);
		exit(1);
		}
	if (basename_length + 1 > sizeof(basename))
		{
		fprintf(stderr, "%s: invocation name too long\n", myself);
		exit(1);
		}
	strcpy(basename, &myself[i]);

	path = getenv("PATH");
	if (path == NULL)
		{
		fprintf(stderr, "%s: no PATH in environment\n", myself);
		exit(1);
		}
	path_length = strlen(path);

	target_length = 0;
	for (i = 0; i < path_length; i++)
		{
		for (j = i; j < path_length; j++)
			{
#ifdef _MSC_VER
			if (path[j] == ';')
#else
			if (path[j] == ':')
#endif
				{
				break;
				}
			}

		element_length = j - i;
		if (element_length < 1)
			{
			continue; /* i++ skips possible separator */
			}
		if (element_length + 1 > sizeof(element))
			{
			fprintf(stderr, "%s: path element too long\n", myself);
			exit(1);
			}

		memcpy(element, &path[i], element_length);
		i = j; /* don't need the starting position anymore */

		while (element_length &&
#ifdef _MSC_VER
				element[element_length] == '\\')
#else
				element[element_length] == '/')
#endif
			{
			element_length--;
			}
		element[element_length] = 0;

		if (element_length + 1 + basename_length + 1 >
				sizeof(filename))
			{
			fprintf(stderr, "%s: filename too long\n", myself);
			exit(1);
			}

		memcpy(filename, element, element_length);
		filename_length = element_length;
#ifdef _MSC_VER
		filename[filename_length++] = '\\';
#else
		filename[filename_length++] = '/';
#endif
		memcpy(&filename[filename_length], basename, basename_length);
		filename_length += basename_length;
		filename[filename_length] = 0;

		if (stat(filename, &statbuf) >= 0)
			{
/* printf("found %s inode %ld\n", filename, (long)statbuf.st_ino); */
#ifdef _MSC_VER
			if (statbuf.st_size == mystatbuf.st_size &&
				    statbuf.st_ctime == mystatbuf.st_ctime &&
				    statbuf.st_mtime == mystatbuf.st_mtime)
#else
			if (memcmp(&statbuf, &mystatbuf, sizeof(statbuf)) == 0)
#endif
				{
				/* found myself in path */
				target_length = 0; /* start searching here */
				}
			else if (target_length == 0)
				{
				/* found target in path */
				strcpy(target, filename);
				target_length = filename_length;
				}
			}

		/* i++ skips possible separator */
		}

	if (target_length < 1)
		{
		fprintf(stderr, "%s: can't find target in PATH\n", myself);
		exit(1);
		}

	quote_flag = 0;
	for (i = 0; i < target_length; i++)
		{
		if (target[i] == ' ')
			{
			quote_flag = 1;
			break;
			}
		}

	command_length = 0;
	if (quote_flag)
		{
		command[command_length++] = '"';
		}
	memcpy(&command[command_length], target, target_length);
	command_length += target_length;
	if (quote_flag)
		{
		command[command_length++] = '"';
		}
	command[command_length++] = ' ';

	for (i = 1; i < argc; i++)
		{
		j = strlen(argv[i]);

		quote_flag = 0;
		for (k = 0; k < j; k++)
			{
			if (argv[i][k] == ' ')
				{
				quote_flag = 1;
				break;
				}
			}

		if (command_length + quote_flag + j + quote_flag + 1 >
				sizeof(command))
			{
			fprintf(stderr, "%s: command line too long\n",
					argv[0]);
			exit(1);
			}

		if (quote_flag)
			{
			command[command_length++] = '"';
			}
#if 1
 k = 0;
 for (j = 0; argv[i][j]; j++)
  {
  if (argv[i][j] == '"')
   {
   command[command_length + k++] = '\\';
   }
  command[command_length + k++] = argv[i][j];
  }
 j = k;
#else
		memcpy(&command[command_length], argv[i], j);
#endif
#ifdef _MSC_VER /* finally! ready to munge something! */
		/* note k = 1, so we don't munge switchar */
		for (k = 1; k < j; k++)
			{
			if (command[command_length + k] == '/')
				{
				command[command_length + k] = '\\';
				}
			}
#endif
		command_length += j;
		if (quote_flag)
			{
			command[command_length++] = '"';
			}
		command[command_length++] = ' ';
		}
	command[command_length - 1] = 0;

	fprintf(stderr, "%s: executing: %s\n", myself, command);
	fflush(stderr);

#ifdef _MSC_VER
	memset(&si, 0, sizeof(STARTUPINFO));
	si.cb = sizeof(STARTUPINFO);
	si.dwFlags = STARTF_USESHOWWINDOW;
	si.wShowWindow = SW_SHOWMINNOACTIVE;

	memset(&pi, 0, sizeof(PROCESS_INFORMATION));

	if (CreateProcess(
			NULL, //argv[1], // name of executable module
			command, // command line string
			NULL, //LPSECURITY_ATTRIBUTES lpProcessAttributes,
			NULL, //LPSECURITY_ATTRIBUTES lpThreadAttributes,
			FALSE, // handle inheritance option
			HIGH_PRIORITY_CLASS, // creation flags
			NULL, //LPVOID lpEnvironment, // new environment block
			NULL, // current directory name
			&si, // startup information
			&pi // process information
			) == 0)
		{
		fprintf(stderr, "CreateProcess() failed\n");
		exit(1);
		}

	if (WaitForSingleObject(pi.hProcess, INFINITE) != WAIT_OBJECT_0)
		{
		fprintf(stderr, "WaitForSingleObject() failed\n");
		exit(1);
		}

	if (GetExitCodeProcess(pi.hProcess, &dwExitCode) == 0)
		{
		fprintf(stderr, "GetExitCodeProcess() failed\n");
		exit(1);
		}

	exitcode = dwExitCode;
#else
	exitcode = system(command);
#endif

	fprintf(stderr, "%s: exitcode: %d\n", myself, exitcode);
	exit(exitcode);

	return 0; /* can't happen, but keep the compiler happy */
	}

