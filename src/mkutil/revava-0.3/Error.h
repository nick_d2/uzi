/*
  Program: revava - Atmel Dis-Assembler
  File: Error.h
  Parts of this are Copyright (C) 1997-1999 Uros Platise
  The rest of it is Copyright (C) 2001 Daniel J. Winker

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef __ERROR
#define __ERROR

#include <stdio.h>

/* Global Error Class - Abstract Type */
class TGlobalError{
	public:
		TGlobalError(){}
		virtual ~TGlobalError(){}
		virtual void report(FILE* fd=stderr) = 0;
};

/* Generic Error Class */
class TGenericError : public TGlobalError{
	public:
		TGenericError(const char* msg){
			strcpy( errmsg, msg );
			op[0] = '\0';
		}

		TGenericError(const char* msg, const char* _op){
			strcpy( errmsg, msg );
			strcpy( op, _op );
		}

		~TGenericError(){}

		void report(FILE* fd=stderr){
			if( op[0] == '\0' ){
				fprintf (fd, "Fatal Error: %s\n", errmsg);
			} else {
				fprintf (fd, "Fatal Error: %s: %s\n", errmsg, op);
			}
		}

		void prepend_message( const char* msg ){
			char temp[256];
			sprintf( temp, "%s%s", msg, errmsg );
			strcpy( errmsg, temp );
		}

//		void append_message( const char* msg ){
//			sprintf( errmsg, "%s%s", errmsg, msg );
//		}

	private:
		char errmsg[256];
		char op[256];
};

/* File Error Class */
class TFileError: public TGlobalError{
	public:
		TFileError(const char *msg, const char* _op=NULL){
			errmsg = msg;
			op     = _op;
		}

		void report(FILE* fd=stderr){

			if ( op == NULL ){
				fprintf ( fd, "Fatal Error: %s, ", errmsg);
			} else {
				fprintf( fd, "Fatal Error:%s %s, ", errmsg, op);
			}

			perror( "" );
		}

		~TFileError (){}

	private:
		const char* errmsg;
		const char* op;
};

#endif
