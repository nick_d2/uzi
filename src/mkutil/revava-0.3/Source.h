/*
  Program: revava - Atmel Dis-Assembler
  File: Source.h, Copyright (C) 2001 Daniel J. Winker

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef __SOURCE
#define __SOURCE

#include "Flash.h"

class TSourceOut{

	public:

		TSourceOut( const char* OutFile, TFlash* pFlash );
		~TSourceOut ();

		void push_prolog( const char* s );
		void push_body  ( const char* s );
		// void push_epilog( const char* s );

	private:

		class TSourceString{
			public:
				TSourceString( const char* s, TSourceString* previous );
				~TSourceString(){}
				TSourceString* next;
				char string[ 80 ];	// FIXME - magic number
		};

		TSourceString* prolog_head;
		TSourceString* prolog_tail;
		TSourceString* body_head;
		TSourceString* body_tail;
		// TSourceString* epilog_head;
		// TSourceString* epilog_tail;
};

#endif
