/*
  Program: revava - Atmel Dis-Assembler
  File: Avr.h
  Parts of this are Copyright (C) 1997-1999 Uros Platise
  The rest of it is Copyright (C) 2001 Daniel J. Winker

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef __AVR
#define __AVR

class TAvr{

	public:

		enum eArgTypes {
			ARG_none,
			ARG_0_31,	/* registers and constants */
			ARG_0_7,
			ARG_0_63,
			ARG_16_31,	/* registers only */
			ARG_0_255,
			ARG_neg64_63,
			ARG_cm0_cm255,	/* complement bits */
			ARG_neg2KB_2KB,
			ARG_24_26_28_30,
			ARG_longCall,
			ARG_copyArg,
			ARG_specialX,
			ARG_specialIncX,
			ARG_specialDecX,
			ARG_specialY,
			ARG_specialIncY,
			ARG_specialDecY,
			ARG_specialZ,
			ARG_specialIncZ,
			ARG_specialDecZ
		};

		// It was actually kind of dumb to make this structure since by the
		// time I was done I basically just copied most of TInstSet to this.
		struct TInstruction {
			char           name[ 6 ];
			char           arg0_format;
			char           arg1_format;
			eArgTypes      arg0_type;
			eArgTypes      arg1_type;
			unsigned short arg0;
			unsigned short arg1;
		};

		TAvr (){}
		~TAvr (){}

		int TAvr::Word2Instruction(
			TInstruction*  pInstruction,
			unsigned short code_word,
			int            index );

	private:

		unsigned short AdjustArg( unsigned short arg, eArgTypes eArgType );

	// protected, not private, so make_test_source can use it.
	protected:

		struct TInstSet {		/* instruction entry */
			char      name[ 6 ];
			char      arg[ 4 ];
			char      opcode[ 17 ];
			eArgTypes arg0_type;
			eArgTypes arg1_type;
		};  

		static TInstSet InstSet [];
};

#endif
