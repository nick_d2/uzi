/*
  Program: revava - Atmel Dis-Assembler
  File: Flash.h, Copyright (C) 2001 Daniel J. Winker

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef __EEPROM
#define __EEPROM

// Code Size is 8K Flash
#define FLASH_SIZE ( 0x2000 )

/* Flash Class */
class TFlash{

	public:

		TFlash();
		~TFlash(){}

		void FlipEndian( void );

		void flashset(
			unsigned short       addr,
			unsigned char        nbytes,
			const unsigned char* data);

		unsigned short get_flash_word(
			unsigned short addr,
			int*           high_byte_written,
			int*           low_byte_written );

		void Dump( void );
		void DebugDump( void );

	private:

		unsigned char code_space[ FLASH_SIZE ];
		int code_byte_written[ FLASH_SIZE ];
};

#endif
