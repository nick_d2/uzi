README for revava-0.3, Copyright (C) 2001, Daniel J. Winker

Short description:
Disassembler for Atmel AVR microcontroller firmware.

Full description:
revava is a single pass disassembler that reads in a file containing a
program intended for an Atmel AVR microcontroller and outputs assembly
code that can be input to an avr assembler.  The output of revava
contains assembler mnemonics where possible and dc.W declarations where
no mnemonic matches the data.  The comment field for each assembly
instruction contains the address from the object code and the
destination address for branches, calls, jumps, etc.  In the case where
there are multiple assembly instructions that assemble to the same
opcode, all choices are presented in a group with all but the first
choice commented out.  make_labels.pl is a Perl script that's been
included.  It takes the output of revava and puts "jump to" and function
labels in the code.

This is an early release of revava.  Right now only Intel hex files are
supported for input.  It would not be hard to add support for other
formats.  See the file TODO for more ideas.

I took a lot of the code (probably about half) from AVA Version 0.3b
which was written by Uros Platise and is available from
http://medo.fov.uni-mb.si/mapp.  AVA is an Atmel AVR assembler.

Compiling:

The code is pretty vannilla C++.  It should build with just about any
C++ compiler.  I tried it with gcc egcs-2.91.66 on linux and gcc 2.95.1
on Solaris 7.  The only problem I noticed is that the Linux version
wanted

	#include <string.h>

and the Solaris version wanted

	#include <strings.h>

I left it at <string.h>, so you might have to adjust that to get it to
compile on your system.

After downloading the latest tarball (x.y is the version number)

	tar -xvzf revava-x.y.tar.gz
	cd revava-x.y

Here you might want to edit the Makefile for your own preferences, then:

	make

This should make two executables: "revava" and "make_test_source".  revava
is the disassembler.  make_test_source just spits out some AVR assembly code
that uses every instruction with different combinations of arguments.

After that you might want to

	strip revava

I thought not using -g as a compiler flag made the executable as small
as possible, but "strip revava" makes it even smaller.

Testing:

If you want to test it and you have AVA (which can be obtained at
http://medc.fov.uni-mb.si/mapp) you can execute the following:

	# This makes some crazy assembly code.  Don't try to run it!
	./make_test_source > test_source.s

	# The following two lines create an Intel hex file named test.flash
	ava test_source.s
	ava --intel -o test test_source.o

	# This creates another assembly file that should be equivalent
	# to test_source.s
	revava test.flash

	# Try to assemble revava's output
	ava out.s
	ava --intel out.o

	# See if we ended up back where we started
	diff test.flash a.out.flash

There should be no difference.

Installing:

There is no "make install" target in the makefile.  (See TODO file).
So, just copy or move the executable "revava" to /usr/local/bin or
wherever you want to keep it.  This README file is the extent of the
documentation (See TODO file), so keep it around when you use revava.

Using:

Just typing "revava" and hitting enter returns something like this:

	Atmel Dis-Assembler 0.1

	Syntax: revava [switches] file
	Switches:
	  -d         Dump Flash and exit.
	  -D         Debug Dump Flash (maybe should be Dubious Dump).
	  -e         Use little endian (Intel byte order).
	  -o file    Output file name (to redirect to stdout -o stdout).
	  --help     Print this help.
	  --version  Output version information.

"file" is of course the object file that we want to disassemble.  I'll
go through the switches one at a time.

-d	Dump Flash and exit.

This does the (probably) familiar hex dump that looks something like
this:

	04E0: 5000 570F 5F0F 5080  578F 5F8F 50F0 57FF  P.W._.P. W._.P.W.
	04F0: 5FFF 9402 94F2 95F2  2000 20FF 23FF 95A8  _.......  . .#...
	1000: FFFF 940E 940C ----  ---- ---- ---- ----  ......           

The leftmost column is the address in hex.  Then we have the hex data
and finally an ASCII representation of the data if it seems appropriate.
The '-' characters in the hex data area mean that that data did not show
up in the input object file.  (There was data at addresses 1000 to 1005,
but nothing from 1006 to 100F).  Similarily, there are no addresses
shown from 0500 to 09FF because they didn't show up in the input file.

This option in combination with the -e flag is actually very useful for
finding text strings embedded in the object file.

-D	Debug Dump Flash and exit.

This is mostly for debugging revava itself.  The output is pretty
similar to what you get with the -d flag, except the entire flash space
is dumped - whether it was present in the input file or not - and there
is an extra field that is the flags that keep track of what parts of
flash showed up in the input.  One problem with this output is that it
is more than 80 columns wide.

-e	Use little endian (Intel byte order)

This flips the bytes in each word right after reading in the input file.
This is useful because typically the way that works for disassembling is
oppisite of the way that makes strings show up right with the -d flag.
Try it once without -e and once with -e to see which one disassembles
right.  Then try the -d flag once without -e and once with -e to see
which one displays any strings correctly.

-o file	Output file name

Should be pretty straightforward.  I suppose redirecting to stdout with

	revava -o stdout input.hex

is a little strange.  Oh well.

--help     Print this help.
--version  Output version information.

These two should also be pretty straightforward.

make_labels.pl:

Run this on the output of revava to add function labels and simple
"jump to" labels.  This makes the output source a little easier to read.
For instance, it will change this:

	        clr     r16             ; 04E0
	        ;eor    r16, r16        ; 04E0

	        ldi     r16, 0x6        ; 04E2
	        sts     0x0074, r16     ; 04E4
	        rjmp    910             ; 04F8, Dest: 0888
	        rcall   1528            ; 04FA, Dest: 0AF4

into this:

	L042:
	        clr     r16             ; 04E0
	        ;eor    r16, r16        ; 04E0

	        ldi     r16, 0x6        ; 04EA
	        sts     0x0074, r16     ; 04EC
	        rjmp    L115            ; 04F0, Dest: 0888

	Func005:
	        rcall   Func008         ; 04F2, Dest: 0AF4

make_labels.pl reads in a file and outputs to stdout, so typically you
would do something like this:

	revava -o prog.s program.hex
	make_labels.pl prog.s > program.s

If make_labels.pl can't find the "jump to" or function address it just
adds "##### Dest BOGUS #####" to the end of the source line.  This means
that the destination address is either in the middle of a double word
instruction or the destination address was never included in the
original object file.  A section of the code with lots of
"##### Dest BOGUS #####" comments is probably really a data section.

Note, make_labels.pl is much less tested than revava.  You should use an
assembler to make sure you can recreate the object code from your brand
new source.

Contact:

The website for revava is currently http://www.visi.com/~dwinker/revava.
I can be contacted at dwinker@tiny.net.
