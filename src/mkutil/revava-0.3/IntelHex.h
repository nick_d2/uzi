/*
  Program: revava - Atmel Dis-Assembler
  File: IntelHex.h, Copyright (C) 2001 Daniel J. Winker

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef __INTELHEX
#define __INTELHEX

/* Intel Hex Record Class */
class TIntelHexRecord{
	public:
		TIntelHexRecord(){}
		~TIntelHexRecord(){}

		void StringToRecord(const char* ps);
		inline       unsigned char  get_nbytes(){ return nbytes; }
		inline       unsigned short get_addr()  { return addr;   }
		inline       unsigned char  get_type()  { return type;   }
		inline const unsigned char* get_data()  { return data;   }

	private:
		unsigned char  nbytes;
		unsigned short addr;
		unsigned char  type;
		unsigned char  data[ 256 ];	// FIXME - Magic Number
};

#endif
