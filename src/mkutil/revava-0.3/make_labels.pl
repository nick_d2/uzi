#!/bin/perl -w
##############################################################################
# Program: revava - Atmel Dis-Assembler
# File: make_labels.pl
# Copyright (C) 2001 Daniel J. Winker
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##############################################################################

use strict ;

die "usage: $0 infile > outfile\n" if ( 0 != $#ARGV );
open( INFILE, $ARGV[ 0 ] ) or die "$0: Can't open $ARGV[ 0 ]. $!\n";

my %AddrList;
my %Addr2Func;
my $FuncNum = 0;
my %Addr2Label;
my $LabelNum = 0;

# Read the file into a buffer because we're going to make several
# passes through it.  This will be more efficient.
my @Buf = <INFILE>;

# First spin through collecting addresses and looking for "Dest"s.  We
# will put in function labels or simple labels anywere there is a
# "Dest".  This first pass just gets a list of addresses.
LOOP0: foreach( @Buf ){

	next LOOP0 unless m/; (\w+)/o;
	++$AddrList{ $1 };

	if( m/, Dest: (\w+)$/o ){
		my $Address = $1;

		# Create a cross reference if one does not exist.
		if( m/rcall/o ){

			# We will call these things that are rcalled "functions".

			$Addr2Func{  $Address } = "";

		} else {

			$Addr2Label{ $Address } = "";
		}
	}
}

# Now sort the addresses and assign labels in increasing order.
foreach my $Address ( sort keys %Addr2Func ){
	$Addr2Func{  $Address } = sprintf( "Func%03d",  $FuncNum++ );
}

foreach my $Address ( sort keys %Addr2Label ){
	$Addr2Label{ $Address } = sprintf( "L%03d", $LabelNum++ );
}

# I suppose we wouldn't really have to revisit every single line in the
# source file here, if we would have kept track of the Buf Indexes that
# need updating.  Oh well.  This is pass 2.
LOOP1: foreach( @Buf ){

	if( m/^(\s;*\w+\s+)-*[\d]+(\s+; \w+, Dest: (\w+))$/o ||
	    m/^(\s;*\w+\s+\d+, )-*[\d]+(\s+; \w+, Dest: (\w+))$/o ){
		my $LineBeg = $1;
		my $LineEnd = $2;
		my $Address = $3;

		# Check for the existance of a Labelable target
		if( !exists( $AddrList{ $Address } )){

			# No Labelable target.  Mark this line.
			chomp;
			$_ = $_." ##### Dest BOGUS #####\n";

			# Delete this key so we don't look for the place to put the
			# label later.

			if( $LineBeg =~ m/rcall/o ){
				delete $Addr2Func{  $Address };
			} else {
				delete $Addr2Label{ $Address };
			}

			next LOOP1;

		}

		# Change the source line to include our new label
		if( $LineBeg =~ m/rcall/o ){

			$_ = "$LineBeg$Addr2Func{ $Address }$LineEnd\n";

		} else {

			$_ = "$LineBeg$Addr2Label{ $Address }$LineEnd\n";
		}
	}
}

# Now go through and put in the labels.  First get a list of function
# addresses and label addresses in order.
my @FuncAddrList = sort keys %Addr2Func;
my $FuncAddress = shift @FuncAddrList;
my @LabelAddrList = sort keys %Addr2Label;
my $LabelAddress = shift @LabelAddrList;
#print "FuncAddrList=@FuncAddrList\nLabelAddrList=@LabelAddrList\n";

# I guess this is pass 3.  Actually, it might be easier to read if this
# pass were split into two seperate passes - one for Func's and one for
# Label's.
LOOP2: for( my $Index = 0; $Index < $#Buf; $Index++ ){

	next LOOP2 unless $Buf[ $Index ] =~ m/; (\w+)/o;
	my $ThisAddr = $1;

	$FuncAddress  = shift @FuncAddrList  if( $FuncAddress  and $ThisAddr gt $FuncAddress );
	$LabelAddress = shift @LabelAddrList if( $LabelAddress and $ThisAddr gt $LabelAddress );

	next LOOP2 if( $FuncAddress  and $ThisAddr lt $FuncAddress  ) and
	             ( $LabelAddress and $ThisAddr lt $LabelAddress );

	#printf "ThisAddr=%s FuncAddress=%s LabelAddress=%s\n",
	#	defined $ThisAddr     ? $ThisAddr     : "undef",
	#	defined $FuncAddress  ? $FuncAddress  : "undef",
	#	defined $LabelAddress ? $LabelAddress : "undef";

	if( $FuncAddress and $ThisAddr eq $FuncAddress ){

		#print "Before\n";
		#print "Index - 2 =".( $Index - 2 )." $Buf[ $Index - 2 ]";
		#print "Index - 1 =".( $Index - 1 )." $Buf[ $Index - 1 ]";
		#print "Index     =".( $Index + 0 )." $Buf[ $Index + 0 ]";
		#print "Index + 1 =".( $Index + 1 )." $Buf[ $Index + 1 ]";
		#print "Index + 2 =".( $Index + 2 )." $Buf[ $Index + 2 ]";
		#print "Index + 3 =".( $Index + 3 )." $Buf[ $Index + 3 ]";

		# The outer if is just because I'm paranoid.  It would only be false
		# on the first line of the input file which should never even have
		# an address.
		if( $Index ){

			# If there is a blank line above this, just put in the function
			# label.  Otherwise, add a blank line and the function label.
			if( $Buf[ $Index - 1 ] eq "\n" ){
				splice( @Buf, $Index, 0, "$Addr2Func{ $FuncAddress }:\n" );
				$Index += 1;
			} else {
				splice( @Buf, $Index, 0, "\n", "$Addr2Func{ $FuncAddress }:\n" );
				$Index += 2;
			}

		} else {
			splice( @Buf, $Index, 0, "$Addr2Func{ $FuncAddress }:\n" );
			$Index += 1;
		}

		#print "After\n";
		#print "Index - 4 =".( $Index - 4 )." $Buf[ $Index - 4 ]";
		#print "Index - 3 =".( $Index - 3 )." $Buf[ $Index - 3 ]";
		#print "Index - 2 =".( $Index - 2 )." $Buf[ $Index - 2 ]";
		#print "Index - 1 =".( $Index - 1 )." $Buf[ $Index - 1 ]";
		#print "Index     =".( $Index + 0 )." $Buf[ $Index + 0 ]";
		#print "Index + 1 =".( $Index + 1 )." $Buf[ $Index + 1 ]";

		$FuncAddress = shift @FuncAddrList;
	}

	if( $LabelAddress and $ThisAddr eq $LabelAddress ){

		#print "Before\n";
		#print "Index - 2 =".( $Index - 2 )." $Buf[ $Index - 2 ]";
		#print "Index - 1 =".( $Index - 1 )." $Buf[ $Index - 1 ]";
		#print "Index     =".( $Index + 0 )." $Buf[ $Index + 0 ]";
		#print "Index + 1 =".( $Index + 1 )." $Buf[ $Index + 1 ]";
		#print "Index + 2 =".( $Index + 2 )." $Buf[ $Index + 2 ]";
		#print "Index + 3 =".( $Index + 3 )." $Buf[ $Index + 3 ]";

		# The outer if is just because I'm paranoid.  It would only be true
		# on the first line of the input file which should never even have
		# an address.
		if( $Index ){

			# If there is a blank line above this, just put in the function
			# label.  Otherwise, add a blank line and the function label.
			if( $Buf[ $Index - 1 ] eq "\n" ){
				splice( @Buf, $Index, 0, "$Addr2Label{ $LabelAddress }:\n" );
				$Index += 1;
			} else {
				splice( @Buf, $Index, 0, "\n", "$Addr2Label{ $LabelAddress }:\n" );
				$Index += 2;
			}

		} else {
			splice( @Buf, $Index, 0, "$Addr2Label{ $LabelAddress }:\n" );
			$Index += 1;
		}

		#print "After\n";
		#print "Index - 4 =".( $Index - 4 )." $Buf[ $Index - 4 ]";
		#print "Index - 3 =".( $Index - 3 )." $Buf[ $Index - 3 ]";
		#print "Index - 2 =".( $Index - 2 )." $Buf[ $Index - 2 ]";
		#print "Index - 1 =".( $Index - 1 )." $Buf[ $Index - 1 ]";
		#print "Index     =".( $Index + 0 )." $Buf[ $Index + 0 ]";
		#print "Index + 1 =".( $Index + 1 )." $Buf[ $Index + 1 ]";

		$LabelAddress = shift @LabelAddrList;
	}
}

# Pass 4 is the shortest code, but probably the longest time.
# Isn't that funny?
foreach( @Buf ){
	print;
}
