cl -Zi -I. -DWIN32 -DINDEXLIB -c lbdata.c
@if errorlevel 1 goto failure
cl -Zi -I. -DWIN32 -DINDEXLIB -c lblex.c
@if errorlevel 1 goto failure
cl -Zi -I. -DWIN32 -DINDEXLIB -c lbmain.c
@if errorlevel 1 goto failure
cl -Zi -I. -DWIN32 -DINDEXLIB -c lbsym.c
@if errorlevel 1 goto failure
link @lib-z80.lnk
@if errorlevel 1 goto failure
copy lib-z80.exe ..\..\bin

@echo SUCCESS
@goto done
:failure
@echo FAILURE
:done

