/* lbmain.c */

/*
 * (C) Copyright 1989-2003
 * All Rights Reserved
 *
 * Nick Downing
 * nick.downing@hytech-international.com
 *
 * Alan R. Baldwin
 * 721 Berkeley St.
 * Kent, Ohio  44240
 */

#include <stdio.h>
#include <string.h>

#ifdef WIN32
#include <stdlib.h>
#else
#include <alloc.h>
#endif
#include <sys/stat.h> /* Nick */

#include "aslib.h"

/*)Module	lbmain.c
 *
 *	The module lbmain.c contains the functions which
 *	(1) input the linker options, parameters, and specifications
 *	(2) perform a two pass link
 *	(3) produce the appropriate linked data output and/or
 *	    link map file and/or relocated listing files.
 *
 *	lbmain.c contains the following functions:
 *		FILE *	afile()
 *		VOID	bassav()
 *		VOID	gblsav()
 *		VOID	link()
 *		VOID	lbexit()
 *		int	fndidx()
 *		int	main()
 *		VOID	map()
 *		int	parse()
 *		VOID	doparse()
 *		VOID	setbas()
 *		VOID	setgbl()
 *		VOID	usage()
 *
 *	lbmain.c contains the following local variables:
 *		char *	usetext[]	array of pointers to the
 *					command option tect lines
 *
 */

/*)Function	int	main(argc,argv)
 *
 *		int	argc		number of command line arguments + 1
 *		char *	argv[]		array of pointers to the command line
 *					arguments
 *
 *	The function main() evaluates the command line arguments to
 *	determine if the linker parameters are to input through 'stdin'
 *	or read from a command file.  The functiond getline() and parse()
 *	are to input and evaluate the linker parameters.  The linking process
 *	proceeds by making the first pass through each .rel file in the order
 *	presented to the linker.  At the end of the first pass the setbase(),
 *	lnkarea(), setgbl(), and symdef() functions are called to evaluate
 *	the base address terms, link all areas, define global variables,
 *	and look for undefined symbols.  Following these routines a linker
 *	map file may be produced and the linker output files may be opened.
 *	The second pass through the .rel files will output the linked data
 *	in one of the four supported formats.
 *
 *	local variables:
 *		int	c		character from argument string
 *		int	i		loop counter
 *		int	j		loop counter
 *		int	k		loop counter
 *
 *	global variables:
 *				 	text line in ib[]
 *		lfile	*cfp		The pointer *cfp points to the
 *				 	current lfile structure
 *		char	ctype[]		array of character types, one per
 *				 	ASCII character
 *		lfile	*filep	 	The pointer *filep points to the
 *				 	beginning of a linked list of
 *				 	lfile structures.
 *		head	*hp		Pointer to the current
 *				 	head structure
 *		char	ib[NINPUT]	.rel file text line
 *		char	*ip		pointer into the .rel file
 *		lfile	*linkp		pointer to first lfile structure
 *				 	containing an input .rel file
 *				 	specification
 *		int	lberr		error flag
 *		int	mflag		Map output flag
 *		int	oflag		Output file type flag
 *		FILE	*ofp		Output file handle
 *				 	for word formats
 *		FILE	*ofph		Output file handle
 *				 	for high byte format
 *		FILE	*ofpl		Output file handle
 *				 	for low byte format
 *		int	pass		linker pass number
 *		int	pflag		print linker command file flag
 *		int	radix		current number conversion radix
 *		FILE	*sfp		The file handle sfp points to the
 *				 	currently open file
 *		lfile	*startp		asmlnk startup file structure
 *		FILE *	stdout		c_library
 *
 *	functions called:
 *		FILE *	afile()		lbmain.c
 *		int	fclose()	c_library
 *		int	fprintf()	c_library
 *		int	getline()	lblex.c
 *		VOID	library()	lblibr.c
 *		VOID	link()		lbmain.c
 *		VOID	lbexit()	lbmain.c
 *		VOID	lnkarea()	lkarea.c
 *		VOID	map()		lbmain.c
 *		VOID	new()		lksym.c
 *		int	parse()		lbmain.c
 *		VOID	reloc()		lkreloc.c
 *		VOID	search()	lblibr.c
 *		VOID	setbas()	lbmain.c
 *		VOID	setgbl()	lbmain.c
 *		char *	sprintf()	c_library
 *		VOID	symdef()	lksym.c
 *		VOID	usage()		lbmain.c
 *
 *	side effects:
 *		Completion of main() completes the linking process
 *		and may produce a map file (.map) and/or a linked
 *		data files (.ihx or .s19) and/or one or more
 *		relocated listing files (.rst).
 */

int
main(argc, argv)
int argc;
char *argv[];
{
	register int c, i, j, k;
	struct stat statbuf;

#if 0 /* Nick */
	fprintf(stdout, "\n");
#endif

	startp = (struct lfile *) new(sizeof (struct lfile));
	startp->f_idp = "";

	pflag = 0; /* Nick 1; */

	for(i=1; i<argc; i++) {
		ip = ib;
		if(argv[i][0] == '-') {
			j = i;
			k = 1;
			while((c = argv[j][k]) != '\0') {
				ip = ib;
				sprintf(ip, "-%c", c);
				switch(c) {

				/*
				 * Options with arguments
				 */
				case 'f':
				case 'F':
					strcat(ip, " ");
					strcat(ip, argv[++i]);
					break;

				/*
				 * Preprocess these commands
				 */
				case 'n':
				case 'N':
					pflag = 0;
					break;

				case 'p':
				case 'P':
					pflag = 1;
					break;

				/*
				 * Options without arguments
				 */
				default:
					break;
				}
#if 0 /* Nick */
				if(pflag)
					fprintf(stdout, "ASlink >> %s\n", ip);
#endif
				parse();
				k++;
			}
		} else {
			strcpy(ip, argv[i]);
#if 0 /* Nick */
			if(pflag)
				fprintf(stdout, "ASlink >> %s\n", ip);
#endif
			parse();
		}
	}

	if (linkp == NULL)
		usage(ER_FATAL);

	cfp = NULL;
	sfp = NULL;
	filep = linkp;
	hp = NULL;
	radix = 10;

	/*
	 * Open output file
	 */
#if 1 /* Nick */
	ofp = afile(outfp->f_idp, "lib", 1);
#else
	ofp = afile(linkp->f_idp, "lib", 1);
#endif
	if (ofp == NULL) {
		lbexit(ER_FATAL);
	}

	/* record output filename, and how much is path */
	strcpy(afn, afntmp);
	afp = afptmp;

	/* canonicalize output filename by changing FSEPX to _ */
	for (i = afp; afn[i]; i++)
		{
		if (afn[i] == FSEPX)
			{
			afn[i] = '_';
			}
		}

	/* create a directory named from the canonicalized filename */
	if (stat(afn, &statbuf) < 0)
		{
		if (mkdir(afn) < 0)
			{
	 		fprintf(stderr, "%s: cannot mkdir.\n", afn);
			lbexit(ER_FATAL);
			}
		}
	else
		{
		if ((statbuf.st_mode & S_IFMT) != S_IFDIR)
			{
	 		fprintf(stderr, "%s: not a directory.\n", afn);
			lbexit(ER_FATAL);
			}
		}

	while (getline()) {
		ip = ib;
		link();
	}

	fclose(ofp);

	lbexit(lberr ? ER_ERROR : ER_NONE);
	return(0);
}

/*)Function	VOID	lbexit(i)
 *
 *			int	i	exit code
 *
 *	The function lbexit() explicitly closes all open
 *	files and then terminates the program.
 *
 *	local variables:
 *		none
 *
 *	global variables:
 *		FILE *	mfp		file handle for .map
 *		FILE *	ofp		file handle for .ihx/.s19
 *		FILE *	rfp		file hanlde for .rst
 *		FILE *	sfp		file handle for .rel
 *		FILE *	tfp		file handle for .lst
 *
 *	functions called:
 *		int	fclose()	c_library
 *		VOID	exit()		c_library
 *
 *	side effects:
 *		All files closed. Program terminates.
 */

VOID
lbexit(i)
int i;
{
	if (mfp != NULL) fclose(mfp);
	if (ofp != NULL) fclose(ofp);
	if (rfp != NULL) fclose(rfp);
	if (sfp != NULL) { if (sfp != stdin) fclose(sfp); }
	if (tfp != NULL) fclose(tfp);
	exit(i);
}

/*)Function	link()
 *
 *	The function link() evaluates the directives for each line of
 *	text read from the .rel file(s).  The valid directives processed
 *	are:
 *		X, D, Q, H, M, A, S, T, R, and P.
 *
 *	local variables:
 *		int	c		first non blank character of a line
 *
 *	global variables:
 *		head	*headp		The pointer to the first
 *				 	head structure of a linked list
 *		head	*hp		Pointer to the current
 *				 	head structure
 *		sdp	sdp		Base Paged structure
 *		int	a_bytes		T Line address bytes
 *		int	hilo		Byte ordering
 *		int	pass		linker pass number
 *		int	radix		current number conversion radix
 *
 *	functions called:
 *		char	endline()	lklex.c
 *		int	get()		lklex.c
 *		VOID	module()	lkhead.c
 *		VOID	newarea()	lkarea.c
 *		VOID	newhead()	lkhead.c
 *		sym *	newsym()	lksym.c
 *		VOID	reloc()		lkreloc.c
 *
 *	side effects:
 *		Head, area, and symbol structures are created and
 *		the radix is set as the .rel file(s) are read.
 */

VOID
link()
{
	register int c;
	char symname[NINPUT+2];
	char d;

	/* copy line to new *.rel file in the library directory */
	fprintf(rfp, "%s\n", ip);

	if ((c=endline()) == 0) { return; }
	switch (c) {

	case 'X':
		radix = 16;
		break;

	case 'D':
		radix = 10;
		break;

	case 'Q':
		radix = 8;
		break;

#if 0
	case 'H':
		break;

	case 'M':
		break;

	case 'A':
		break;
#endif

	case 'S':
		sscanf(ip, " %s %c", symname, &d);

		/* If it's an actual symbol, record it */
		if (d == 'D' && strcmp(symname, ".__.ABS.") != 0)
			{
			oflag = 0;
			fprintf(ofp, " %s\n", symname);
			}

		break;

#if 0
	case 'T':
	case 'R':
	case 'P':
		break;
#endif

	default:
		break;
	}
#if 1
	if (c == 'X' || c == 'D' || c == 'Q') {
		while ((c = get()) != 0) {
			switch(c) {
			case 'H':
				hilo = 1;
				break;

			case 'L':
				hilo = 0;
				break;

			case '2':
				a_bytes = 2;
				break;

			case '3':
				a_bytes = 3;
				break;

			case '4':
				a_bytes = 4;
				break;

			default:
				break;
			}
		}
		switch(a_bytes) {
		default:
			a_bytes = 2;
		case 2:
			a_mask = 0x0000FFFF;
			s_mask = 0x00008000;
			v_mask = 0x00007FFF;
			break;

		case 3:
			a_mask = 0x00FFFFFF;
			s_mask = 0x00800000;
			v_mask = 0x007FFFFF;
			break;

		case 4:
			a_mask = 0xFFFFFFFF;
			s_mask = 0x80000000;
			v_mask = 0x7FFFFFFF;
			break;
		}
	}
#endif
}

/*)Function	int	parse()
 *
 *	The function parse() evaluates all command line or file input
 *	linker directives and updates the appropriate variables.
 *
 *	local variables:
 *		int	c		character value
 *		int	idx		string index
 *		int	sv_type		save type of processing
 *		char	*p;		string pointer
 *		char	fid[]		file id string
 *
 *	global variables:
 *		char	ctype[]		array of character types, one per
 *				 	ASCII character
 *		lfile	*lfp		pointer to current lfile structure
 *				 	being processed by parse()
 *		lfile	*linkp		pointer to first lfile structure
 *				 	containing an input REL file
 *				 	specification
 *		int	mflag		Map output flag
 *		int	oflag		Output file type flag
 *		int	objflg		Linked file/library output object flag
 *		int	pflag		print linker command file flag
 *		FILE *	stderr		c_library
 *		int	uflag		Relocated listing flag
 *		int	xflag		Map file radix type flag
 *		int	wflag		Wide listing format
 *		int	zflag		Enable symbol case sensitivity
 *
 *	Functions called:
 *		VOID	addlib()	lblibr.c
 *		VOID	addpath()	lblibr.c
 *		VOID	bassav()	lbmain.c
 *		VOID	doparse()	lbmain.c
 *		int	fprintf()	c_library
 *		VOID	gblsav()	lbmain.c
 *		VOID	getfid()	lblex.c
 *		int	get()		lblex.c
 *		int	getnb()		lblex.c
 *		VOID	lbexit()	lbmain.c
 *		char *	strsto()	lksym.c
 *		int	strlen()	c_library
 *
 *	side effects:
 *		Various linker flags are updated and the linked
 *		structure lfile is created.
 */

int
parse()
{
	register int c, idx;
	register char *p;
	int sv_type;
	char fid[FILSPC+FILSPC];

	while ((c = getnb()) != 0) {
		if ( c == '-') {
			while (ctype[c=get()] & LETTER) {
				switch(c) {

				case 'c':
				case 'C':
					if (startp->f_type != 0)
						break;
					startp->f_type = F_STD;
					doparse();
					return(0);

				case 'f':
				case 'F':
					if (startp->f_type == F_LBR)
						return(0);
					unget(getnb());
					if (*ip == 0)
						usage(ER_FATAL);
					sv_type = startp->f_type;
#if 1 /* Nick */
					startp->f_idp = strsto(ip);
					outfp = startp; /* Nick */
#else
					startp->f_idp = ip;
#endif
					startp->f_idx = fndidx(ip);
					startp->f_type = F_LBR;
					doparse();
					if (sv_type == F_STD) {
						cfp = NULL;
						sfp = NULL;
						startp->f_type = F_STD;
						filep = startp;
					}
					return(0);

				case 'o':
				case 'O':
					/* note, this will never be freed */
					outfp = (struct lfile *)
						new(sizeof (struct lfile));

#if 1
					c = getnb();
					if (ctype[c] == ILL)
						{
						return 0; /* ignore it */
						}

					/*
					 * Copy Path from .LBR file
					 */
					idx = startp->f_idx;
					strncpy(fid, startp->f_idp, idx);
					/*
					 * Concatenate the output file spec
					 */
					getfid(fid + idx, c);
					/*
					 * If output file spec has a path
					 * 	use it
					 * else
					 *	use path of .LBR file
					 */
					if (fndidx(fid + idx) != 0) {
						p = fid + idx;
					} else {
						p = fid;
					}
					/*
					 * Save output file specification
					 */
					outfp->f_idp = strsto(p);
					outfp->f_idx = fndidx(p);
#else
					unget(getnb());
					outfp->f_idp = strsto(ip);
					outfp->f_idx = fndidx(ip);
#endif

					/* don't process any more switches */
					return 0;

				case 'e':
				case 'E':
					return(1);

				case 'n':
				case 'N':
					pflag = 0;
					break;

				case 'p':
				case 'P':
					pflag = 1;
					break;

				default:
					fprintf(stderr,
					    "Unkown option -%c ignored\n", c);
					break;
				}
			}
		} else
		if (ctype[c] != ILL) {
			if (linkp == NULL) {
				linkp = (struct lfile *)
						new(sizeof (struct lfile));
#if 1 /* Nick */
				if (outfp == NULL)
					{
					outfp = linkp;
					}
#endif
				lfp = linkp;
			} else {
				lfp->f_flp = (struct lfile *)
						new(sizeof (struct lfile));
				lfp = lfp->f_flp;
			}
			/*
			 * Copy Path from .LBR file
			 */
			idx = startp->f_idx;
			strncpy(fid, startp->f_idp, idx);
			/*
			 * Concatenate the .REL file spec
			 */
			getfid(fid + idx, c);
			/*
			 * If .REL file spec has a path
			 * 	use it
			 * else
			 *	use path of .LBR file
			 */
			if (fndidx(fid + idx) != 0) {
				p = fid + idx;
			} else {
				p = fid;
			}
			/*
			 * Save .REL file specification
			 */
			lfp->f_idp = strsto(p);
			lfp->f_idx = fndidx(p);
			lfp->f_type = F_REL;
			lfp->f_obj = objflg;
		} else {
			fprintf(stderr, "Invalid input");
			lbexit(ER_FATAL);
		}
	}
	return(0);
}

/*)Function	VOID	doparse()
 *
 *	The function doparse() evaluates all interactive
 *	command line or file input linker directives and
 *	updates the appropriate variables.
 *
 *	local variables:
 *		none
 *
 *	global variables:
 *		FILE *	stdin		standard input
 *		FILE *	stdout		standard output
 *		lfile	*cfp		The pointer *cfp points to the
 *				 	current lfile structure
 *		FILE	*sfp		The file handle sfp points to the
 *				 	currently open file
 *		char	ib[NINPUT]	.rel file text line
 *		char	*ip		pointer into the .rel file
 *		lfile	*filep	 	The pointer *filep points to the
 *				 	beginning of a linked list of
 *				 	lfile structures.
 *		lfile	*startp		asmlnk startup file structure
 *		int	pflag		print linker command file flag
 *
 *	Functions called:
 *		int	fclose()	c_library
 *		int	fprintf()	c_library
 *		VOID	getfid()	lblex.c
 *		int	getline()	lblex.c
 *		int	parse()		lbmain.c
 *
 *	side effects:
 *		Various linker flags are updated and the linked
 *		structure lfile may be updated.
 */

VOID
doparse()
{
	cfp = NULL;
	sfp = NULL;
	filep = startp;
	while (1) {
		ip = ib;
		if (getline() == 0)
			break;
#if 0 /* Nick */
		if (pflag && cfp->f_type != F_STD)
			fprintf(stdout, "ASlink >> %s\n", ip);
#endif
		if (*ip == 0 || parse())
			break;
	}
#if 1 /* Nick */
	if (sfp != NULL && sfp != stdin) {
#else
	if(sfp != stdin) {
#endif
		fclose(sfp);
	}
	sfp = NULL;
#if 0 /* Nick */
	startp->f_idp = "";
#endif
	startp->f_idx = 0;
	startp->f_type = 0;
}

/*)Function	FILE *	afile(fn, ft, wf)
 *
 *		char *	fn		file specification string
 *		char *	ft		file type string
 *		int	wf		read(0)/write(1) flag
 *
 *	The function afile() opens a file for reading or writing.
 *
 *	afile() returns a file handle for the opened file or aborts
 *	the assembler on an open error.
 *
 *	local variables:
 *		FILE *	fp		file handle for opened file
 *
 *	global variables:
 *		char	afn[]		afile() constructed filespec
 *		int	afp		afile() constructed path length
 *		char	afntmp[]	afilex() constructed filespec
 *		int	afptmp		afilex() constructed path length
 *
 *	functions called:
 *		VOID	afilex()	asmain.c
 *		int	fndidx()	asmain.c
 *		FILE *	fopen()		c_library
 *		int	fprintf()	c_library
 *		char *	strcpy()	c_library
 *
 *	side effects:
 *		File is opened for read or write.
 */

FILE *
afile(fn, ft, wf)
char *fn;
char *ft;
int wf;
{
	FILE *fp;

	afilex(fn, ft);

	if ((fp = fopen(afntmp, wf ? "w" : "r")) == NULL) {
#if 1 /* Nick */
		fflush(stdout);
		fprintf(stderr, "file \"%s\": can't %s: ",
				afntmp, wf ? "create" : "open");
		perror("");
#else
		fprintf(stderr, "%s: cannot %s.\n",
				afntmp, wf ? "create" : "open");
#endif
		lbexit(ER_FATAL);
	}
#ifdef VERBOSE /* Nick */
 fflush(stderr);
 printf("%s \"%s\"\n", wf ? "writing" : "reading", afntmp);
#endif

#if 0
	strcpy(afn, afntmp);
	afp = afptmp;
#endif

	return (fp);
}

/*)Function	VOID	afilex(fn, ft)
 *
 *		char *	fn		file specification string
 *		char *	ft		file type string
 *
 *	The function afilex() processes the file specification string:
 *		(1)	If the file type specification string ft
 *			is not NULL then a file specification is
 *			constructed with the file path\name in fn
 *			and the extension in ft.
 *		(2)	If the file type specification string ft
 *			is NULL then the file specification is
 *			constructed from fn.  If fn does not have
 *			a file type then the default source file
 *			type dsft is appended to the file specification.
 *
 *	afilex() aborts the assembler on a file specification length error.
 *
 *	local variables:
 *		int	c		character value
 *		char *	p1		pointer into filespec string afntmp
 *		char *	p2		pointer into filespec string fn
 *		char *	p3		pointer to filetype string ft
 *
 *	global variables:
 *		char	afntmp[]	afilex() constructed filespec
 *		int	afptmp		afilex() constructed path length
 *		char	dsft[]		default assembler file type string
 *
 *	functions called:
 *		VOID	lbexit()	asmain.c
 *		int	fndidx()	asmain.c
 *		int	fprintf()	c_library
 *		char *	strcpy()	c_library
 *
 *	side effects:
 *		File specification string may be modified.
 */

VOID
afilex(fn, ft)
char *fn;
char *ft;
{
	register char *p1, *p2, *p3;
	register int c;
#if 0 /* Nick */
	int l;
#endif

	if (strlen(fn) > (FILSPC-5)) {
#if 1 /* Nick */
		fflush(stdout);
		fprintf(stderr, "file \"%s\": name too long\n", fn);
#else
		fprintf(stderr, "File Specification %s is too long.", fn);
#endif
		lbexit(ER_FATAL);
	}

	/*
	 * Save the File Name Index
	 */
	strcpy(afntmp, fn);
#ifdef WIN32 /* Nick */
	for (p1 = afntmp; *p1; p1++)
		{
		if (*p1 == '/')
			{
			*p1 = SLASH;
			}
		}
#endif
	afptmp = fndidx(afntmp);
	p1 = &afntmp[afptmp];
	p2 = &fn[afptmp];

	/*
	 * Skip to File Extension Seperator
	 */
#if 0 /* Nick */
	l = strlen(p2);
	for (c = l - 1; c >= 0; c--)
		{
		if (p2[c] == FSEPX)
			{
			l = c;
			break;
			}
		if (p2[c] == SLASH)
			{
			break;
			}
		}
	p1 += l;
	p2 += l;

	c = *p2++;
#else
	while (((c = *p2++) != 0) && (c != FSEPX)) {
		p1++;
	}
#endif
	*p1++ = FSEPX;

	/*
	 * Copy File Extension
	 */
	 p3 = ft;
	 if (*p3 == 0) {
		if (c == FSEPX) {
			p3 = p2;
		} else {
			p3 = "rel"; /* dsft; */
		}
	}
	while ((c = *p3++) != 0) {
		if (p1 < &afntmp[FILSPC-1])
			*p1++ = c;
	}
	*p1++ = 0;
}

/*)Function	int	fndidx(str)
 *
 *		char *	str		file specification string
 *
 *	The function fndidx() scans the file specification string
 *	to find the index to the file name.  If the file
 *	specification contains a 'path' then the index will
 *	be non zero.
 *
 *	fndidx() returns the index value.
 *
 *	local variables:
 *		char *	p1		temporary pointer
 *		char *	p2		temporary pointer
 *
 *	global variables:
 *		none
 *
 *	functions called:
 *		char *	strrchr()	c_library
 *
 *	side effects:
 *		none
 */

int
fndidx(str)
char *str;
{
	register char *p1, *p2;

	/*
	 * Skip Path Delimiters
	 */
	p1 = str;
	if ((p2 = strrchr(p1, ':')) != NULL) { p1 = p2 + 1; }
#ifdef WIN32 /* Nick */
	if ((p2 = strrchr(p1, '/')) != NULL) { p1 = p2 + 1; }
#endif
	if ((p2 = strrchr(p1, SLASH)) != NULL) { p1 = p2 + 1; }

	return(p1 - str);
}

char *usetxt[] = {
	"Usage: [-Options] [-Option with arg] file [file ...]",
#if 0
	"  -p   Echo commands to stdout (default)",
	"  -n   No echo of commands to stdout",
#endif
	"Alternates to Command Line Input:",
	"  -c                   ASlib >> prompt input",
	"  -f   file[.lbr]      Command File input",
	"Output:",
	"  -o   Output filename (default is first source filename)",
	"End:",
	"  -e   or null line terminates input",
	"",
	0
};

/*)Function	VOID	usage(n)
 *
 *		int	n		exit code
 *
 *	The function usage() outputs to the stderr device the
 *	linker name and version and a list of valid linker options.
 *
 *	local variables:
 *		char **	dp		pointer to an array of
 *					text string pointers.
 *
 *	global variables:
 *		FILE *	stderr		c_library
 *
 *	functions called:
 *		int	fprintf()	c_library
 *
 *	side effects:
 *		none
 */

VOID
usage(n)
int n;
{
	register char	**dp;

#if 1 /* Nick */
	fflush(stderr);
	printf("\nASxxxx Librarian %s\n\n", VERSION);
	for (dp = usetxt; *dp; dp++)
		printf("%s\n", *dp);
#else
	fprintf(stderr, "\nASxxxx Librarian %s\n\n", VERSION);
	for (dp = usetxt; *dp; dp++)
		fprintf(stderr, "%s\n", *dp);
#endif
	lbexit(n);
}
