/* setfsize.c by Nick for UZI180 utils */

#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <ctype.h>

#define BUFFER 0x200

void main(int argc, char **argv)
	{
	int fd, count;
	off_t wanted, current, remain;
	char *buf;

	if (argc < 3 || !isdigit(argv[2][0]))
		{
		printf("usage: %s filename size\n", argv[0]);
		exit(1);
		}

	fd = open(argv[1], O_RDWR);
	if (fd < 0)
		{
		fd = creat(argv[1], 0666);
		if (fd < 0)
			{
			fprintf(stderr, "%s: can't create: ", argv[0]);
			fflush(stderr);
			perror(argv[1]);
			exit(1);
			}
		}

	current = lseek(fd, 0, SEEK_END);
	if (current < 0)
		{
		fprintf(stderr, "%s: can't seek: ", argv[0]);
		fflush(stderr);
		perror(argv[1]);
		exit(1);
		}

	printf("Existing size was %ld\n", current);

	wanted = atol(argv[2]);
	if (wanted < current)
		{
		if (lseek(fd, wanted, SEEK_SET) != wanted)
			{
			fprintf(stderr, "%s: can't seek: ", argv[0]);
			fflush(stderr);
			perror(argv[1]);
			exit(1);
			}

		write(fd, NULL, 0);
		printf("Truncated to new size of %ld\n", wanted);
		}
	else
		{
		buf = calloc(BUFFER, 1);
		if (buf == NULL)
			{
			fprintf(stderr, "%s: can't allocate %d bytes\n",
					argv[0], BUFFER);
			fflush(stderr);
			exit(1);
			}

		remain = wanted - current;
		while (remain)
			{
			count = min(remain, BUFFER);
			if (write(fd, buf, count) != count)
				{
				fprintf(stderr, "%s: can't write: ", argv[0]);
				fflush(stderr);
				perror(argv[1]);
				exit(1);
				}

			remain -= count;
			}

		printf("Extended to new size of %ld\n", wanted);
		}

	close(fd);
	exit(0);
	}

