cl -Zi -I. -DWIN32 -DINDEXLIB -c lkarea.c
@if errorlevel 1 goto failure
cl -Zi -I. -DWIN32 -DINDEXLIB -c lkdata.c
@if errorlevel 1 goto failure
cl -Zi -I. -DWIN32 -DINDEXLIB -c lkeval.c
@if errorlevel 1 goto failure
cl -Zi -I. -DWIN32 -DINDEXLIB -c lkhead.c
@if errorlevel 1 goto failure
cl -Zi -I. -DWIN32 -DINDEXLIB -c lklex.c
@if errorlevel 1 goto failure
cl -Zi -I. -DWIN32 -DINDEXLIB -c lklibr.c
@if errorlevel 1 goto failure
cl -Zi -I. -DWIN32 -DINDEXLIB -c lklist.c
@if errorlevel 1 goto failure
cl -Zi -I. -DWIN32 -DINDEXLIB -c lkmain.c
@if errorlevel 1 goto failure
cl -Zi -I. -DWIN32 -DINDEXLIB -c lkout.c
@if errorlevel 1 goto failure
cl -Zi -I. -DWIN32 -DINDEXLIB -c lkrloc.c
@if errorlevel 1 goto failure
cl -Zi -I. -DWIN32 -DINDEXLIB -c lksym.c
@if errorlevel 1 goto failure
link @link-z80.lnk
@if errorlevel 1 goto failure
copy link-z80.exe ..\..\bin

@echo SUCCESS
@goto done
:failure
@echo FAILURE
:done

