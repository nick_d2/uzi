cl -Zi -I. -DWIN32 -c asdata.c
@if errorlevel 1 goto failure
cl -Zi -I. -DWIN32 -c asexpr.c
@if errorlevel 1 goto failure
cl -Zi -I. -DWIN32 -c aslex.c
@if errorlevel 1 goto failure
cl -Zi -I. -DWIN32 -c aslist.c
@if errorlevel 1 goto failure
cl -Zi -I. -DWIN32 -c asmain.c
@if errorlevel 1 goto failure
cl -Zi -I. -DWIN32 -c asout.c
@if errorlevel 1 goto failure
cl -Zi -I. -DWIN32 -c assubr.c
@if errorlevel 1 goto failure
cl -Zi -I. -DWIN32 -c assym.c
@if errorlevel 1 goto failure
cl -Zi -I. -DWIN32 -c z80adr.c
@if errorlevel 1 goto failure
cl -Zi -I. -DWIN32 -c z80ext.c
@if errorlevel 1 goto failure
cl -Zi -I. -DWIN32 -c z80mch.c
@if errorlevel 1 goto failure
cl -Zi -I. -DWIN32 -c z80pst.c
@if errorlevel 1 goto failure
link @as-z80.lnk
@if errorlevel 1 goto failure
copy as-z80.exe ..\..\bin

@echo SUCCESS
@goto done
:failure
@echo FAILURE
:done

