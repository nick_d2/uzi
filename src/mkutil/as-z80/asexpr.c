/* asexpr.c */

/*
 * (C) Copyright 1989-2002
 * All Rights Reserved
 *
 * Alan R. Baldwin
 * 721 Berkeley St.
 * Kent, Ohio  44240
 *
 *   With enhancements from
 *
 *	Bill McKinnon (BM)
 *	w_mckinnon@conknet.com
 */

#include <stdio.h>
#include <setjmp.h>
#include <string.h>

#ifdef WIN32
#include <stdlib.h>
#else
#include <alloc.h>
#endif

#include "asxxxx.h"

/*)Module	asexpr.c
 *
 *	The module asexpr.c contains the routines to evaluate
 *	arithmetic/numerical expressions.  The functions in
 *	asexpr.c perform a recursive evaluation of the arithmetic
 *	expression read from the assembler-source text line.
 *	The expression may include binary/unary operators, brackets,
 *	symbols, labels, and constants in hexadecimal, decimal, octal
 *	and binary.  Arithmetic operations are prioritized and
 *	evaluated by normal arithmetic conventions.
 *
 *	asexpr.c contains the following functions:
 *		VOID	abscheck()
 *		a_uint	absexpr()
 *		VOID	clrexpr()
 *		int	digit()
 *		VOID	expr()
 *		int	oprio()
 *		VOID	term()
 *
 *	asexpr.c contains no local/static variables
 */

/*)Function	VOID	expr(esp, n)
 *
 *		expr *	esp		pointer to an expr structure
 *		int	n		a firewall priority; all top
 *					level calls (from the user)
 *					should be made with n set to 0.
 *
 *	The function expr() evaluates an expression and
 *	stores its value and relocation information into
 *	the expr structure supplied by the user.
 *
 *	Notes about the arithmetic:
 *		The coding emulates N-Bit unsigned
 *		arithmetic operations.  This allows
 *		program compilation without regard to the
 *		intrinsic integer length of the host
 *		machine.
 *
 *	local variables:
 *		a_uint	ae		value from expr esp
 *		a_uint	ar		value from expr re
 *		int	c		current assembler-source
 *					text character
 *		int	p		current operator priority
 *		area *	ap		pointer to an area structure
 *		exp	re		internal expr structure
 *
 *	global variables:
 *		char	ctype[]		array of character types, one per
 *					ASCII character
 *
 *	functions called:
 *		VOID	abscheck()	asexpr.c
 *		VOID	clrexpr()	asexpr.c
 *		VOID	expr()		asexpr.c
 *		int	get()		aslex.c
 *		int	getnb()		aslex.c
 *		int	oprio()		asexpr.c
 *		VOID	qerr()		assubr.c
 *		VOID	rerr()		assubr.c
 *		VOID	term()		asexpr.c
 *		VOID	unget()		aslex.c
 *
 *
 *	side effects:
 *		An expression is evaluated modifying the user supplied
 *		expr structure, a sym structure maybe created for an
 *		undefined symbol, and the parse of the expression may
 *		terminate if a 'q' error occurs.
 */

VOID
expr(esp, n)
register struct expr *esp;
int n;
{
#if 1 /* Nick has added this to allow operators such as .low. etc */
	char *ip_save;
#endif
	register a_uint ae, ar;	
	int c, p;
	struct area *ap;
	struct expr re;

	term(esp);
#if 1 /* Nick has modified this to allow operators such as .low. etc */
	ip_save = ip;
	while ((c = getop())) {
#else
	while (ctype[c = getnb()] & BINOP) {
#endif
		/*
		 * Handle binary operators + - * / & | % ^ << >>
		 */
#if 1 /* Nick has modified this to allow operators such as .low. etc */
                if ((p = oprio(c)) <= n) {
			ip = ip_save;
                        break;
		}
#else
		if ((p = oprio(c)) <= n)
			break;
#endif
#if 1 /* Nick has modified this to allow less-than / greater-than operators */
		if (c == '<' && c != get()) {
			unget(c);
			if (get() != '=') {
				unget(c);
				c = 'l'; /* less-than operator */
			} else {
				c = 'L'; /* less-than-or-equal operator */
			}
		} else
		if (c == '>' && c != get()) {
			unget(c);
			if (get() != '=') {
				unget(c);
				c = 'g'; /* greather-than operator */
			} else {
				c = 'G'; /* greater-than-or-equal operator */
			}
		}
#else
		if ((c == '>' || c == '<') && c != get())
			qerr();
#endif
		clrexpr(&re);
		expr(&re, p);
		esp->e_rlcf |= re.e_rlcf;
		
		/*
		 * 16-Bit Unsigned Arithmetic
		 */
		ae = esp->e_addr & a_mask;
		ar = re.e_addr & a_mask;

		if (c == '+') {
			/*
			 * esp + re, at least one must be absolute
			 */
			if (esp->e_base.e_ap == NULL) {
				/*
				 * esp is absolute (constant),
				 * use area from re
				 */
				esp->e_base.e_ap = re.e_base.e_ap;
			} else
			if (re.e_base.e_ap) {
				/*
				 * re should be absolute (constant)
				 */
				rerr();
			}
			if (esp->e_flag && re.e_flag)
				rerr();
			if (re.e_flag)
				esp->e_flag = 1;
			ae += ar;
		} else
#if 1 /* Nick has modified this to allow less-than / greater-than operators */
                if (c == '-' || c == 'l' || c == 'L' || c == 'g' || c == 'G') {
#else
		if (c == '-') {
#endif
			/*
			 * esp - re
			 */
			if ((ap = re.e_base.e_ap) != NULL) {
				if (esp->e_base.e_ap == ap) {
					esp->e_base.e_ap = NULL;
				} else {
					rerr();
				}
			}
			if (re.e_flag)
				rerr();
#if 1 /* Nick has modified this to allow less-than / greater-than operators */
			switch (c) {
			case 'l':
				ae = (ae < ar);
				break;
			case 'L':
				ae = (ae <= ar);
				break;
			case 'g':
				ae = (ae > ar);
				break;
			case 'G':
				ae = (ae >= ar);
				break;
			case '-':
				ae -= ar;
				break;
			}
#else
                        esp->e_addr -= re.e_addr;
			ae -= ar;
#endif
		} else {
			/*
			 * Both operands (esp and re) must be constants
			 */
			abscheck(esp);
			abscheck(&re);
			switch (c) {
			/*
			 * The (int) /, %, and >> operations
			 * are truncated to 2-bytes.  (Really? --Nick)
			 */

#if 1 /* Nick has modified this to allow operators such as .or. etc */
			case 'a':
				ae = ae && ar;
				break;

			case 'o':
				ae = ae || ar;
				break;
#endif

			case '*':
				ae *= ar;
				break;

			case '/':
				if (ar == 0) {
					ae = 0;
					err('z');
				} else {
					ae /= ar;
				}
				break;

			case '&':
				ae &= ar;
				break;

			case '|':
				ae |= ar;
				break;

			case '%':
				if (ar == 0) {
					ae = 0;
					err('z');
				} else {
					ae %= ar;
				}
				break;

			case '^':
				ae ^= ar;
				break;

			case '<':
				ae <<= ar;
				break;

			case '>':
				ae >>= ar;
				break;

			default:
				qerr();
				break;
			}
		}
		esp->e_addr = (ae & s_mask) ? ae | ~v_mask : ae & v_mask;
#if 1 /* Nick has modified this to allow operators such as .low. etc */
		ip_save = ip;
#endif
	}
#if 0 /* Nick has modified this to allow operators such as .low. etc */
	unget(c);
#endif
}

#if 1 /* Nick has added this function to allow operators such as .low. etc */
int getop(void)
{
	register int c;

	if (ctype[c = getnb()] & BINOP) {
		return c; /* it's a single-character operator */
	}
	unget(c); /* it's not a single-character operator */
	return getop_binary(); /* it may be a textual operator string */
}

int getnop(void)
{
	char *ip_save;
	register int c;

	ip_save = ip;
	if (getop_binary()) {
		ip = ip_save;
		return 0; /* it's a valid character, but stop processing! */
		}
	if (ctype[c = get()] & (LETTER|DIGIT)) {
		return c; /* it's a valid symbol character */
	}
	unget(c); /* it's not a valid symbol character */
	return 0;
}

int getop_binary(void)
{
	if (getop_srch(".AND.")) {
		return 'a'; /* encode the operator as a single character */
	}

	if (getop_srch(".BINAND.")) {
		return '&'; /* encode the operator as a single character */
	}

	if (getop_srch(".OR.")) {
		return 'o'; /* encode the operator as a single character */
	}

	if (getop_srch(".BINOR.")) {
		return '|'; /* encode the operator as a single character */
	}

	if (getop_srch(".XOR.")) {
		return '^'; /* encode the operator as a single character */
	}

	return 0; /* it's not a binary operator at all */
}

int getop_unary(void)
{
#if 0
	if (getop_srch("LWRD")) {
		unget(getnb()); /* skip this IAR keyword (what is it??) */
	}
#endif

	if (getop_srch(".NOT.")) {
		return '!'; /* encode the operator as a single character */
	}

	if (getop_srch(".BINNOT.")) {
		return '~'; /* encode the operator as a single character */
	}

	if (getop_srch(".LOW.")) {
		return '<'; /* encode the operator as a single character */
	}

	if (getop_srch(".HIGH.")) {
		return '>'; /* encode the operator as a single character */
	}

	if (getop_srch(".SFB.")) {
		return '{'; /* encode the operator as a single character */
	}

	if (getop_srch(".SFE.")) {
		return '}'; /* encode the operator as a single character */
	}

	return getnb(); /* it may be a single-character operator */
}

/*
 *      srch --- does string match ?
 */
int
getop_srch(str)
register char *str;
{
	register char *ptr;
	ptr = ip;

#if 0 /* CASE_SENSITIVE */
	while (*ptr && *str) {
		if (*ptr != *str)
			break;
		ptr++;
		str++;
	}
	if (*ptr == *str) {
		ip = ptr;
		return(1);
	}
#else
	while (*ptr && *str) {
		if (ccase[(unsigned char)(*ptr)] != ccase[(unsigned char)(*str)])
			break;
		ptr++;
		str++;
	}
	if (ccase[(unsigned char)(*ptr)] == ccase[(unsigned char)(*str)]) {
		ip = ptr;
		return(1);
	}
#endif

	if (!*str)
		/*if (any(*ptr," \t\n,);+-"))*/ { /* if str is exhausted */
			ip = ptr;
			return(1);
		}
	return(0);
}
#endif

/*)Function	a_uint	absexpr()
 *
 *	The function absexpr() evaluates an expression, verifies it
 *	is absolute (i.e. not position dependent or relocatable), and
 *	returns its value.
 *
 *	local variables:
 *		expr	e		expr structure
 *
 *	global variables:
 *		none
 *
 *	functions called:
 *		VOID	abscheck()	asexpr.c
 *		VOID	clrexpr()	asexpr.c
 *		VOID	expr()		asexpr.c
 *
 *	side effects:
 *		If the expression is not absolute then
 *		a 'r' error is reported.
 */

a_uint
absexpr()
{
	struct expr e;

	clrexpr(&e);
	expr(&e, 0);
	abscheck(&e);
	return (e.e_addr);
}

/*)Function	VOID	term(esp)
 *
 *		expr *	esp		pointer to an expr structure
 *
 *	The function term() evaluates a single constant
 *	or symbol value prefaced by any unary operator
 *	( +, -, ~, ', ", >, or < ).  This routine is also
 *	responsible for setting the relocation type to symbol
 *	based (e.flag != 0) on global references.
 *
 *	Notes about the arithmetic:
 *		The coding emulates N-Bit unsigned
 *		arithmetic operations.  This allows
 *		program compilation without regard to the
 *		intrinsic integer length of the host
 *		machine.
 *
 *	local variables:
 *		int	c		current character
 *		char	id[]		symbol name
 *		char *	jp		pointer to assembler-source text
 *		int	n		constant evaluation running sum
 *		int	r		current evaluation radix
 *		sym *	sp		pointer to a sym structure
 *		tsym *	tp		pointer to a tsym structure
 *		int	v		current digit evaluation
 *
 *	global variables:
 *		char	ctype[]		array of character types, one per
 *					ASCII character
 *		sym *	symp		pointer to a symbol structure
 *
 *	functions called:
 *		VOID	abscheck()	asexpr.c
 *		int	digit()		asexpr.c
 *		VOID	err()		assubr.c
 *		VOID	expr()		asexpr.c
 *		int	is_abs()	asexpr.c
 *		int	get()		aslex.c
 *		VOID	getid()		aslex.c
 *		int	getmap()	aslex.c
 *		int	getnb()		aslex.c
 *		sym *	lookup()	assym.c
 *		VOID	qerr()		assubr.c
 *		VOID	unget()		aslex.c
 *
 *	side effects:
 *		An arithmetic term is evaluated, a symbol structure
 *		may be created, term evaluation may be terminated
 *		by a 'q' error.
 */

VOID
term(esp)
register struct expr *esp;
{
	register int c, n;
	register char *jp;
	char id[NCPS];
	struct sym  *sp;
	struct tsym *tp;
	int r, v;

	r = radix;
	c = getnb();
	/*
 	 * Discard the unary '+' at this point and
	 * also any reference to numerical arguments
	 * associated with the '#' prefix.
	 */
	while (c == '+' || c == '#') { c = getnb(); }
#if 1 /* Nick has modified this to allow operators such as .low. etc */
	unget(c); /* so that c is included in the search for .low. etc */
	c = getop_unary(); /* encodes the operator as a single character */
#endif
	/*
 	 * Evaluate all binary operators
	 * by recursively calling expr().
	 */
	if (c == LFTERM) {
		expr(esp, 0);
		if (getnb() != RTTERM)
			qerr();
		return;
	}
	if (c == '-') {
		expr(esp, 100);
		abscheck(esp);
		esp->e_addr = ~esp->e_addr + 1;
		return;
	}
	if (c == '~') {
		expr(esp, 100);
		abscheck(esp);
		esp->e_addr = ~esp->e_addr;
		return;
	}
#if 1 /* Nick has added a new single-character operator, also used for .not. */
        if (c == '!') {
                expr(esp, 100);
                abscheck(esp);
                esp->e_addr = !esp->e_addr;
                return;
        }
#endif
	if (c == '\'') {
		esp->e_mode = S_USER;
		esp->e_addr = getmap(-1)&0377;
#if 1 /* Nick has added this to allow an optional closing quote for literals */
		c = get();
		if (c != '\'') {
			unget(c);
		}
#endif
		return;
	}
	if (c == '\"') {
		esp->e_mode = S_USER;
		if (hilo) {
		    esp->e_addr  = (getmap(-1)&0377)<<8;
		    esp->e_addr |= (getmap(-1)&0377);
		} else {
		    esp->e_addr  = (getmap(-1)&0377);
		    esp->e_addr |= (getmap(-1)&0377)<<8;
		}
		if (esp->e_addr & s_mask) {
			esp->e_addr |= ~v_mask;
		} else {
			esp->e_addr &=  v_mask;
		}
		return;
	}
	if (c == '>' || c == '<') {
		expr(esp, 100);
		if (is_abs (esp)) {
			/*
			 * evaluate msb/lsb directly
			 */
			if (c == '>')
				esp->e_addr >>= 8;
			esp->e_addr &= 0377;
			return;
		} else {
			/*
			 * let linker perform msb/lsb, lsb is default
			 */
			esp->e_rlcf |= R_BYTX;
			if (c == '>')
				esp->e_rlcf |= R_MSB;
			return;
		}
	}
#if 1 /* Nick, for .SFB. and .SFE. operators */
	if (c == '{' || c == '}')
		{
		/*
		 * Evaluate symbols and labels
		 */
		n = getnb();
	        if (ctype[n] & LETTER)
			{
			unget(n); /* skip white space */
	                esp->e_mode = S_USER;
	                getid(id, '_');
			n = NCPS;
			while (--n)
				{
				id[n] = id[n-1];
				}
			id[n] = (c == '{') ? 's' : 'e';
	                sp = lookup(id);
	                if (sp->s_type == S_NEW)
				{
#if 1
				sp->s_flag |= S_GBL;
#else
				if (sp->s_flag&S_GBL)
					{
#endif
					esp->e_flag = 1;
					esp->e_base.e_sp = sp;
					return;
#if 0
					}
				err('u');
#endif
	                	}
			else
				{
	                        esp->e_mode = sp->s_type;
	                        esp->e_addr = sp->s_addr;
	                        esp->e_base.e_ap = sp->s_area;
	                	}
	                return;
	        	}
		/*
		 * Else not a term.
		 */
	        qerr();
		}
#endif
	/*
	 * Evaluate digit sequences as local symbols
	 * if followed by a '$' or as constants.
	 */
	if (ctype[c] & DIGIT) {
		esp->e_mode = S_USER;
		jp = ip;
		while (ctype[*jp & 0x007F] & RAD10) {
			jp++;
		}
		if (*jp == '$') {
			n = 0;
			while ((v = digit(c, 10)) >= 0) {
				n = 10*n + v;
				c = get();
			}
			tp = symp->s_tsym;
			while (tp) {
				if (n == tp->t_num) {
					esp->e_base.e_ap = tp->t_area;
					esp->e_addr = tp->t_addr;
					return;
				}
				tp = tp->t_lnk;
			}
			err('u');
			return;
		}
#if 1 /* Nick has copied the code below, to process suffixes eg. 1234h */
                jp = ip;
                while (ctype[(unsigned char)(*jp)] & RAD16) {
                        jp++;
                }
		if (ctype[(unsigned char)(*jp)] & LETTER) {
			jp++;
		}
		if (ctype[(unsigned char)(*jp)] & (LETTER|DIGIT)) {
			goto radix_prefix;
		}
                switch (*--jp) {
                        case 'b':
                        case 'B':
                                r = 2;
                                goto radix_suffix;
                        case 'o':
                        case 'O':
                        case 'q':
                        case 'Q':
                                r = 8;
                                goto radix_suffix;
                        case 'd':
                        case 'D':
                                r = 10;
                                goto radix_suffix;
                        case 'h':
                        case 'H':
                        case 'x':
                        case 'X':
                                r = 16;
                                goto radix_suffix;
                        default:
                                break;
                }
	radix_prefix:
#endif
		if (c == '0') {
			c = get();
			switch (c) {
				case 'b':
				case 'B':
					r = 2;
					c = get();
					break;
				case 'o':
				case 'O':
				case 'q':
				case 'Q':
					r = 8;
					c = get();
					break;
				case 'd':
				case 'D':
					r = 10;
					c = get();
					break;
				case 'h':
				case 'H':
				case 'x':
				case 'X':
					r = 16;
					c = get();
					break;
				default:
					break;
			}
		}
		n = 0;
		while ((v = digit(c, r)) >= 0) {
			n = r*n + v;
			c = get();
		}
		unget(c);
		esp->e_addr = (n & s_mask) ? n | ~v_mask : n & v_mask;
		return;
#if 1 /* Nick has inserted the label below, to process suffixes eg. 1234h */
	radix_suffix:
                n = 0;
                while ((v = digit(c, r)) >= 0) {
                        n = r*n + v;
                        c = get();
                }
                /*unget(c);*/
		esp->e_addr = (n & s_mask) ? n | ~v_mask : n & v_mask;
                return;
#endif
	}
	/*
	 * Evaluate '$' sequences as a temporary radix
	 * if followed by a '%', '&', '#', or '$'.
	 */
	if (c == '$') {
		c = get();
		if (c == '%' || c == '&' || c == '#' || c == '$') {
			switch (c) {
				case '%':
					r = 2;
					break;
				case '&':
					r = 8;
					break;
				case '#':
					r = 10;
					break;
				case '$':
					r = 16;				
					break;
				default:
					break;
			}
			c = get();
			n = 0;
			while ((v = digit(c, r)) >= 0) {
				n = r*n + v;
				c = get();
			}
			unget(c);
			esp->e_mode = S_USER;
			esp->e_addr = (n & s_mask) ? n | ~v_mask : n & v_mask;
			return;
		}
		unget(c);
		c = '$';
	}
	/*
	 * Evaluate symbols and labels
	 */
	if (ctype[c] & LETTER) {
		esp->e_mode = S_USER;
		getid(id, c);
#if 1 /* Nick */
/* printf("\"%s\"\n", id); */
		if (symeq(id, "LOW", 0))
			{
			expr(esp, 100);
			if (is_abs(esp))
				{
				/*
				 * evaluate msb/lsb directly
				 */
				esp->e_addr &= 0xff;
				return;
				}
			else
				{
				/*
				 * let linker perform msb/lsb, lsb is default
				 */
				esp->e_rlcf |= R_BYTX;
				return;
				}
			}
		else if (symeq(id, "HIGH", 0))
			{
			expr(esp, 100);
			if (is_abs(esp))
				{
				/*
				 * evaluate msb/lsb directly
				 */
				esp->e_addr >>= 8;
				esp->e_addr &= 0xff;
				return;
				}
			else
				{
				/*
				 * let linker perform msb/lsb, lsb is default
				 */
				esp->e_rlcf |= R_BYTX;
				esp->e_rlcf |= R_MSB;
				return;
				}
			}
		else if (symeq(id, "LWRD", 0))
			{
			expr(esp, 100);
			if (is_abs(esp))
				{
				/*
				 * evaluate msb/lsb directly
				 */
				esp->e_addr &= 0xffff;
				return;
				}
			else
				{
				/*
				 * let linker perform msb/lsb, lsb is default
				 */
#if 0
				esp->e_rlcf |= R_BYTX;
				esp->e_rlcf |= R_MSB; /* ???? */
#endif
				return;
				}
			}
#if 1 /*def R_BYTE3 /* Nick has reassigned the R_PAG0 and R_PAG bits! */
		else if (symeq(id, "BYTE3", 0))
			{
			expr(esp, 100);
			if (is_abs(esp))
				{
				/*
				 * evaluate msb/lsb directly
				 */
				esp->e_addr >>= 16;
				esp->e_addr &= 0xff;
				return;
				}
			else
				{
				/*
				 * let linker perform msb/lsb, lsb is default
				 */
				esp->e_rlcf |= R_BYTX;
#ifdef R_BYTE3
				esp->e_rlcf |= R_BYTE3;
#endif
				return;
				}
			}
#endif
#ifdef R_BYTE4 /* Nick has reassigned the R_PAG0 and R_PAG bits! */
		else if (symeq(id, "BYTE4", 0))
			{
			expr(esp, 100);
			if (is_abs(esp))
				{
				/*
				 * evaluate msb/lsb directly
				 */
				esp->e_addr >>= 24;
				esp->e_addr &= 0xff;
				return;
				}
			else
				{
				/*
				 * let linker perform msb/lsb, lsb is default
				 */
				esp->e_rlcf |= R_BYTX;
				esp->e_rlcf |= R_BYTE4;
				return;
				}
			}
#endif
#endif
		sp = lookup(id);
		if (sp->s_type == S_NEW) {
			if (sp->s_flag&S_GBL) {
				esp->e_flag = 1;
				esp->e_base.e_sp = sp;
				return;
			}
			err('u');
		} else {
			esp->e_mode = sp->s_type;
			esp->e_addr = sp->s_addr;
			esp->e_base.e_ap = sp->s_area;
		}
		return;
	}
	/*
	 * Else not a term.
	 */
	qerr();
}

/*)Function	int	digit(c, r)
 *
 *		int	c		digit character
 *		int	r		current radix
 *
 *	The function digit() returns the value of c
 *	in the current radix r.  If the c value is not
 *	a number of the current radix then a -1 is returned.
 *
 *	local variables:
 *		none
 *
 *	global variables:
 *		char	ctype[]		array of character types, one per
 *					ASCII character
 *
 *	functions called:
 *		none
 *
 *	side effects:
 *		none
 */

int
digit(c, r)
register int c, r;
{
	if (r == 16) {
		if (ctype[c] & RAD16) {
			if (c >= 'A' && c <= 'F')
				return (c - 'A' + 10);
			if (c >= 'a' && c <= 'f')
				return (c - 'a' + 10);
			return (c - '0');
		}
	} else
	if (r == 10) {
		if (ctype[c] & RAD10)
			return (c - '0');
	} else
	if (r == 8) {
		if (ctype[c] & RAD8)
			return (c - '0');
	} else
	if (r == 2) {
		if (ctype[c] & RAD2)
			return (c - '0');
	}
	return (-1);
}

/*)Function	VOID	abscheck(esp)
 *
 *		expr *	esp		pointer to an expr structure
 *
 *	The function abscheck() tests the evaluation of an
 *	expression to verify it is absolute.  If the evaluation
 *	is relocatable then an 'r' error is noted and the expression
 *	made absolute.
 *
 *	Note:	The area type (i.e. ABS) is not checked because
 *		the linker can be told to explicitly relocate an
 *		absolute area.
 *
 *	local variables:
 *		none
 *
 *	global variables:
 *		none
 *
 *	functions called:
 *		VOID	rerr()		assubr.c
 *
 *	side effects:
 *		The expression may be changed to absolute and the
 *		'r' error invoked.
 */

VOID
abscheck(esp)
register struct expr *esp;
{
	if (esp->e_flag || esp->e_base.e_ap) {
		esp->e_flag = 0;
		esp->e_base.e_ap = NULL;
		rerr();
	}
}

/*)Function	int	is_abs(esp)
 *
 *		expr *	esp		pointer to an expr structure
 *
 *	The function is_abs() tests the evaluation of an
 *	expression to verify it is absolute.  If the evaluation
 *	is absolute then 1 is returned, else 0 is returned.
 *
 *	Note:	The area type (i.e. ABS) is not checked because
 *		the linker can be told to explicitly relocate an
 *		absolute area.
 *
 *	local variables:
 *		none
 *
 *	global variables:
 *		none
 *
 *	functions called:
 *		none
 *
 *	side effects:
 *		none
 */

int
is_abs (esp)
register struct expr *esp;
{
	if (esp->e_flag || esp->e_base.e_ap) {
		return(0);
	}
	return(1);
}

/*)Function	int	oprio(c)
 *
 *		int	c		operator character
 *
 *	The function oprio() returns a relative priority
 *	for all valid unary and binary operators.
 *
 *	local variables:
 *		none
 *
 *	global variables:
 *		none
 *
 *	functions called:
 *		none
 *
 *	side effects:
 *		none
 */
 
int
oprio(c)
register int c;
{
	if (c == '*' || c == '/' || c == '%')
		return (10);
	if (c == '+' || c == '-')
		return (7);
	if (c == '<' || c == '>')
		return (5);
	if (c == '^')
		return (4);
	if (c == '&')
		return (3);
	if (c == '|')
		return (1);
	return (0);
}

/*)Function	VOID	clrexpr(esp)
 *
 *		expr *	esp		pointer to expression structure
 *
 *	The function clrexpr() clears the expression structure.
 *
 *	local variables:
 *		none
 *
 *	global variables:
 *		none
 *
 *	functions called:
 *		none
 *
 *	side effects:
 *		expression structure cleared.
 */
 
VOID
clrexpr(esp)
register struct expr *esp;
{
	esp->e_mode = 0;
	esp->e_flag = 0;
	esp->e_addr = 0;
	esp->e_base.e_ap = NULL;
	esp->e_rlcf = 0;
}

/*)Function	VOID	exprmasks(esp)
 *
 *		int	n		T Line Bytes in Address
 *
 *	The function exprmasks() configures the assembler
 *	for 16, 24, or 32-Bit Data/Addresses.
 *
 *	local variables:
 *		none
 *
 *	global variables:
 *		int	a_bytes		T Line Bytes in Address
 *		a_uint	a_mask		Address mask
 *		a_uint	s_mask		Sign mask
 *		a_uint	v_mask		Value mask
 *
 *	functions called:
 *		none
 *
 *	side effects:
 *		The arithmetic precision parameters are set.
 */
 
VOID
exprmasks(n)
int n;
{
	a_bytes = n;

	switch(a_bytes) {
	default:
		a_bytes = 2;
	case 2:
		a_mask = 0x0000FFFF;
		s_mask = 0x00008000;
		v_mask = 0x00007FFF;
		break;

	case 3:
		a_mask = 0x00FFFFFF;
		s_mask = 0x00800000;
		v_mask = 0x007FFFFF;
		break;

	case 4:
		a_mask = 0xFFFFFFFF;
		s_mask = 0x80000000;
		v_mask = 0x7FFFFFFF;
		break;
	}
}


