/***************************************************************************
* ihex2bin
* By Jason Nunn <jsno@downunder.net.au>, Oct 2000
***************************************************************************/
#include <stdio.h>
// really for Turbo C: #ifdef _MSC_VER
// really for Turbo C: #include <alloc.h>
// really for Turbo C: #else
#include <malloc.h>
// really for Turbo C: #endif
#include <ctype.h>
#if 1 /* Nick */
#include <fcntl.h>
#endif

#define STR_LEN 255

unsigned char *text;
unsigned int text_base = -1; /* Nick */
unsigned int text_top = 0; /* Nick unsigned */
unsigned int text_size = 0; /* Nick */
unsigned int mem_offset = 0; /* Nick */
int embed_length = 0; /* Nick */
int iar_mapping = 0; /* Nick */
char strln[STR_LEN + 1];

int readtoken(int sl,int el)
{
  int x,v = 0,ord;

  ord = (el - sl) * 4;
  for(x = sl;x <= el;x++)
  {
    char c;

    c = tolower(strln[x]);

    if(isdigit(c))
      v |= (c - '0') << ord;
    else
      if(isxdigit(c))
        v |= ((c - 'a') + 10) << ord;
      else
      {
        printf("garbage found in input file. Aborting.\n");
        exit(-1);
      }

    ord -= 4;
  }

  return v;
}

int main(int argc,char *argv[])
{
  FILE *fp;
#if 1 /* Nick */
  int j, ac, ap;
#endif
  char nstr[STR_LEN];

  printf("ihex2bin, by Jason Nunn <jsno@downunder.net.au>, Oct 2000\n");

#if 1 /* Nick */
  ac = argc;
  for (ap = 1; ap < ac; ap++)
    {
    if (strcmp(argv[ap], "-l") == 0 || strcmp(argv[ap], "-L") == 0)
      {
      embed_length = 1;
      }
    else if (strcmp(argv[ap], "-i") == 0 || strcmp(argv[ap], "-I") == 0)
      {
      iar_mapping = 1;
      }
    else
      {
      break;
      }
    }

  if (ap >= ac || argv[ap][0] == '-')
    {
    printf("usage: ihex2bin [-l] [-i] infile [outfile]\n");
    printf("  -l  embed length at location 4 in output\n");
    printf("  -i  logical/physical mapping for IAR compiler\n");
    printf("outfile name is constructed from infile if not specified,\n");
    printf("and in this case the filename extension is changed to .bin\n");
    exit(1);
    }
#else
  if(argc < 2)
  {
    printf("No filename specified, Aborting.\n");
    exit(-1);
  }
#endif

#if 1
  fp = fopen(argv[ap++],"r");
#else
  fp = fopen(argv[1],"r");
#endif
  if(fp == NULL)
#if 1
    {
    printf("Error: can't open input file '%s'\n", argv[1]);
    exit(1);
    }
#else
    perror("main()::fopen()");
#endif

  text = malloc(0x100000); // malloc(0xffff);
  if(text == NULL)
  {
#if 1
    printf("Error: can't allocate 0x100000 bytes\n");
    exit(1);
#else
    perror("main()::malloc()");
    exit(-1);
#endif
  }

  memset(text, 0xff, 0x100000); // memset(text,0xff,0xffff);

  for(;;)
  {
    unsigned int data_no,mem_start,i,pos; /* Nick unsigned */

    if(feof(fp))
      break;

    if(fgets(strln,STR_LEN,fp) == NULL)
      break;

    if(strln[0] != ':')
      continue;

#if 1 /* Nick */
    if(readtoken(7,8) == 0x04)
      {
      mem_offset = readtoken(9,12);
      if (iar_mapping)
        {
        mem_offset = (mem_offset & 0xf) << 14;
        }
      else
        {
        mem_offset <<= 16;
        }
      continue;
      }

    if(readtoken(7,8) == 0x05)
      {
      /* not sure what this means, some kind of configuration area? */
      continue;
      }
#endif

    if(readtoken(7,8)) /* Nick  == 0x01) */
      break;

    data_no = readtoken(1,2);
    mem_start = readtoken(3,6) + mem_offset; /* Nick mem_offset */

#if 1 /* Nick */
    if (mem_start < text_base)
      text_base = mem_start;
#endif

    i = (mem_start + data_no);
    if(i > text_top)
      text_top = i;

    for(i = 0,pos = 9;i < data_no;i++)
    {
      text[mem_start + i] = readtoken(pos,pos + 1);
      pos += 2;
    }
  }
  fclose(fp);

#if 1 /* Nick */
  if (text_top < text_base)
    text_base = text_top;
#endif

#if 1 /* Nick */
  if (ap >= ac)
    {
    strcpy(nstr, argv[ap - 1]);
    j = strlen(nstr);
    while (j--)
      {
      if (nstr[j] == '\\')
        {
        break; /* the filename has no extension, so don't strip it */
        }
      if (nstr[j] == '.')
        {
        nstr[j] = 0; /* strip the period and the extension */
        break; /* now ready to concatenate our extension */
        }
      }
    strcat(nstr, ".bin");
    }
  else
    {
    strcpy(nstr, argv[ap]);
#if 0
    j = strlen(nstr);
    while (j--)
      {
      if (nstr[j] == '\\')
        {
        strcat(nstr, ".bin");
        break; /* the filename has no extension, so don't strip it */
        }
      if (nstr[j] == '.')
        {
        break; /* an extension was provided, so use it */
        }
      }
#endif
    }
  if (!strcmp(nstr, argv[ap - 1]))
    {
    printf("Error: input and output filenames identical\n");
    exit(1);
    }
  j = open(nstr, O_WRONLY | O_CREAT | O_TRUNC | O_BINARY, 0644);
  if (j < 0)
    {
    printf("Error: could not create file '%s'\n", nstr);
    exit(1);
    }
  text_size = text_top - text_base;
  if (embed_length)
    {
    if (text_size < 8 ||
        text[text_base + 4] != 0x78 ||
        text[text_base + 5] != 0x56 ||
        text[text_base + 6] != 0x34 ||
        text[text_base + 7] != 0x12)
      {
      printf("Error: length placeholder value of 0x12345678 not found\n");
      exit(1);
      }
    text[text_base + 4] = text_size & 0xff;
    text[text_base + 5] = (text_size >> 8) & 0xff;
    text[text_base + 6] = (text_size >> 16) & 0xff;
    text[text_base + 7] = (text_size >> 24) & 0xff;
    }
  write(j, text + text_base, text_size);
  close(j);

  printf("Created '%s' origin 0x%x size 0x%x\n", nstr, text_base, text_size);
#else
  sprintf(nstr,"%s.bin",argv[1]);
  fp = fopen(nstr,"w");
  fwrite(text,text_top,1,fp);
  fclose(fp);

  printf("Wrote %u bytes.\n",text_top);
#endif

  return 0;
}
