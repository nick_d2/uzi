copy 4dos.com ..\bin
copy crcd.com ..\bin
copy mklink-l.bat ..\bin
copy mklink-b.bat ..\bin
copy mknbat-l.bat ..\bin
copy mknbat-b.bat ..\bin

tasm crc /m3 /l
if errorlevel 1 goto failure
tlink crc /t
if errorlevel 1 goto failure
copy crc.com ..\bin

rem cl -Zi -I. false.c
rem @if errorlevel 1 goto failure
rem copy false.exe ..\bin

rem cl -Zi -I. true.c
rem @if errorlevel 1 goto failure
rem copy true.exe ..\bin

cl -Zi -I. foster.c
@if errorlevel 1 goto failure
copy foster.exe ..\bin

rem cl -Zi -I. loginsh.c
rem @if errorlevel 1 goto failure
rem copy loginsh.exe ..\bin

cl -Zi -I. bin2c.c
@if errorlevel 1 goto failure
copy bin2c.exe ..\bin

cl -Zi -I. bin2avr.c
@if errorlevel 1 goto failure
copy bin2avr.exe ..\bin

rem cl -Zi -I. -DVAX cr.c
rem @if errorlevel 1 goto failure
rem copy cr.exe ..\bin

rem cl -Zi -I. -DVAX crc.c
rem @if errorlevel 1 goto failure
rem copy crc.exe ..\bin

cl -Zi -I. ihex2bin.c
@if errorlevel 1 goto failure
copy ihex2bin.exe ..\bin

rem cl -Zi -I. -DVAX touch.c
rem @if errorlevel 1 goto failure
rem copy touch.exe ..\bin

rem cl -Zi -I. -DVAX setfsize.c
rem @if errorlevel 1 goto failure
rem copy setfsize.exe ..\bin

@echo SUCCESS
@goto done
:failure
@echo FAILURE
:done

