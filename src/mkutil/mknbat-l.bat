@if .%1==. goto failure

@echo iccz80 -S -w -ml -v1 -z -A -I..\..\include\ %1>n.bat
@echo @if errorlevel 1 goto failure>>n.bat
@echo del %1.r01>>n.bat
@echo as-z80 -l -o %1.s01>>n.bat
@echo @if errorlevel 1 goto failure>>n.bat
@echo.>>n.bat
@echo link-z80 -f %1>>n.bat
@echo @if errorlevel 1 goto failure>>n.bat
@echo ihex2bin -l %1.i86 %1>>n.bat
@echo @if errorlevel 1 goto failure>>n.bat
@echo.>>n.bat
@echo @echo SUCCESS>>n.bat
@echo @goto done>>n.bat
@echo :failure>>n.bat
@echo @echo FAILURE>>n.bat
@echo :done>>n.bat
@echo.>>n.bat

@echo SUCCESS
@goto done

:failure
@echo usage: %0 filename
@echo.
@echo Writes Windows NT/2000/XP batch file "n.bat" for large memory model,
@echo containing the needed commands to compile "filename.c" to "filename".
@echo The generated file can then be manually edited to add further commands.
@echo Please note, any previously existing "n.bat" will be overwritten!
@echo.

:done

