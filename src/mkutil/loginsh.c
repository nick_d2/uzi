/* loginsh.c by Nick for Hytech */

#include <stdio.h>
#ifdef _MSC_VER
#include <windows.h>
#else
#include <unistd.h>
#endif
#include <string.h>

#define COMMAND_MAX 0x800 /* cope with very long command lines */

int main(int argc, char **argv)
	{
	int i, j, k, quote_flag;
	int command_length, exitcode;
	char command[COMMAND_MAX];
#if 1 /* customisation for loginsh.c */
	int exefile_length;
	char exefile[COMMAND_MAX];
#endif
	char *shell;
#ifdef _MSC_VER
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	DWORD dwExitCode;
#endif

	command_length = 0;
#if 1 /* customisation for loginsh.c */
	exefile_length = 0;

	shell = getenv("SHELL");
	if (shell == NULL)
		{
		shell = "/bin/sh";
		}
	j = strlen(shell);

	quote_flag = 0;
	for (k = 0; k < j; k++)
		{
		if (shell[k] == ' ')
			{
			quote_flag = 1;
			break;
			}
		}

	if (exefile_length + quote_flag + j + quote_flag + 1 >
			sizeof(exefile))
		{
		fprintf(stderr, "%s: exefile line too long\n",
				argv[0]);
		exit(1);
		}

	if (quote_flag)
		{
		exefile[exefile_length++] = '"';
		}
#ifdef _MSC_VER /* temporary escaping of quotes, but no length check!! fix!! */
 k = 0;
 if (shell[0] == '/') /* UNIX style path specifier?  if so, put under cygwin */
  {
  k = 7; /*9;*/
  memcpy(&exefile[exefile_length], "\\cygwin", k); /*"c:\\cygwin", k);*/
  }
 for (j = 0; shell[j]; j++)
  {
  if (shell[j] == '/') /* munge the pathname from the SHELL environment var */
   {
   exefile[exefile_length + k++] = '\\';
   }
  else
   {
   if (shell[j] == '"')
    {
    exefile[exefile_length + k++] = '\\';
    }
   exefile[exefile_length + k++] = shell[j];
   }
  }
 j = k;
#else
	memcpy(&exefile[exefile_length], shell, j);
#endif
	exefile_length += j;
#ifdef _MSC_VER
	exefile[exefile_length++] = '.';
	exefile[exefile_length++] = 'e';
	exefile[exefile_length++] = 'x';
	exefile[exefile_length++] = 'e';
#endif
	if (quote_flag)
		{
		exefile[exefile_length++] = '"';
		}
	exefile[exefile_length] = 0;

	command[command_length++] = '-'; /* pass as first argument to sh.exe */
	command[command_length++] = ' ';
#endif
	for (i = 1; i < argc; i++)
		{
		j = strlen(argv[i]);

		quote_flag = 0;
		for (k = 0; k < j; k++)
			{
			if (argv[i][k] == ' ')
				{
				quote_flag = 1;
				break;
				}
			}

		if (command_length + quote_flag + j + quote_flag + 1 >
				sizeof(command))
			{
			fprintf(stderr, "%s: command line too long\n",
					argv[0]);
			exit(1);
			}

		if (quote_flag)
			{
			command[command_length++] = '"';
			}
#ifdef _MSC_VER /* temporary escaping of quotes, but no length check!! fix!! */
 k = 0;
 for (j = 0; argv[i][j]; j++)
  {
  if (argv[i][j] == '"')
   {
   command[command_length + k++] = '\\';
   }
  command[command_length + k++] = argv[i][j];
  }
 j = k;
#else
		memcpy(&command[command_length], argv[i], j);
#endif
		command_length += j;
		if (quote_flag)
			{
			command[command_length++] = '"';
			}
		command[command_length++] = ' ';
		}

#if 0 /* not used after all */
	if (command_length < 1)
		{
#if 1 /* customisation for loginsh.c */
		fprintf(stderr, "usage: %s [arguments]\n", argv[0]);
		fprintf(stderr, "references the SHELL environment variable\n");
		fprintf(stderr, "if SHELL is not set, uses value "
				"\"/bin/sh\"\n");
#ifdef _MSC_VER
		fprintf(stderr, "prefix \"c:\\cygwin\" and suffix \".exe\" "
				"are added\n");
#endif
#else
		fprintf(stderr, "usage: %s program [arguments]\n", argv[0]);
#endif
		exit(1);
		}
#endif
	command[command_length - 1] = 0;

#if 1 /* customisation for loginsh.c */
	fprintf(stderr, "%s: executing: %s\n", argv[0], exefile);
	fprintf(stderr, "%s: arguments: %s\n", argv[0], command);
#else
	fprintf(stderr, "%s: executing: %s\n", argv[0], command);
#endif
	fflush(stderr);

#ifdef _MSC_VER
	memset(&si, 0, sizeof(STARTUPINFO));
	si.cb = sizeof(STARTUPINFO);
	si.dwFlags = STARTF_USESHOWWINDOW;
	si.wShowWindow = SW_SHOWMINNOACTIVE;

	memset(&pi, 0, sizeof(PROCESS_INFORMATION));

	if (CreateProcess(
#if 1 /* customisation for loginsh.c */
			exefile, // name of executable module
#else
			NULL, // name of executable module
#endif
			command, // command line string
			NULL, //LPSECURITY_ATTRIBUTES lpProcessAttributes,
			NULL, //LPSECURITY_ATTRIBUTES lpThreadAttributes,
			FALSE, // handle inheritance option
			HIGH_PRIORITY_CLASS, // creation flags
			NULL, //LPVOID lpEnvironment, // new environment block
			NULL, // current directory name
			&si, // startup information
			&pi // process information
			) == 0)
		{
		fprintf(stderr, "CreateProcess() failed\n");
		exit(1);
		}

	if (WaitForSingleObject(pi.hProcess, INFINITE) != WAIT_OBJECT_0)
		{
		fprintf(stderr, "WaitForSingleObject() failed\n");
		exit(1);
		}

	if (GetExitCodeProcess(pi.hProcess, &dwExitCode) == 0)
		{
		fprintf(stderr, "GetExitCodeProcess() failed\n");
		exit(1);
		}

	exitcode = dwExitCode;
#else
	exitcode = system(command);
#endif

	fprintf(stderr, "%s: exitcode: %d\n", argv[0], exitcode);
	exit(exitcode);

	return 0; /* can't happen, but keep the compiler happy */
	}

