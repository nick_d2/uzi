c:\tc\bin\tcc -ml -P -I. -DAVRLANG=LANGUAGE_US -c avrasm.cc
c:\tc\bin\tcc -ml -P -I. -DAVRLANG=LANGUAGE_US -c semantic.cc
c:\tc\bin\tcc -ml -P -I. -DAVRLANG=LANGUAGE_US -c symbol.cc
c:\tc\bin\tcc -ml -P -I. -DAVRLANG=LANGUAGE_US -c utils.cc
c:\tc\bin\tcc -ml -P -I. -DAVRLANG=LANGUAGE_US -c avrparse.cc
c:\tc\bin\tcc -ml -P -I. -DAVRLANG=LANGUAGE_US -c avrlex.cc
del tavrasm.exe
c:\tc\bin\tlink @t.lnk
del *.obj
del *.map
copy tavrasm.exe \hytech\src\avr
