#ifndef BISON_AVRPARSE_H
# define BISON_AVRPARSE_H

#ifndef YYSTYPE
typedef union
{
  regSA                 regid;
  instSA                inst;
  opcodeSA              opcode;
  valueSA               val;
  nameSA                name;
  symbolSA              symb;
  indirectSA            indi;
  functionSA            func;
  stringSA              string;
} yystype;
# define YYSTYPE yystype
# define YYSTYPE_IS_TRIVIAL 1
#endif
# define	STAR	257
# define	DIV	258
# define	MOD	259
# define	LS	260
# define	RS	261
# define	LE	262
# define	GE	263
# define	LESS	264
# define	GREAT	265
# define	EQ	266
# define	NE	267
# define	OR	268
# define	XOR	269
# define	AND	270
# define	OROR	271
# define	ANDAND	272
# define	LPAR	273
# define	RPAR	274
# define	COLON	275
# define	COMMA	276
# define	DOT	277
# define	EQUAL	278
# define	PLUS	279
# define	MINUS	280
# define	WAVE	281
# define	NOT	282
# define	EOL	283
# define	RESTART	284
# define	ENDOFFILE	285
# define	DEF	286
# define	EQU	287
# define	DB	288
# define	DW	289
# define	ORG	290
# define	CSEG	291
# define	DSEG	292
# define	ESEG	293
# define	BYTE	294
# define	SET	295
# define	DEVICE	296
# define	STRING	297
# define	MACRODEF	298
# define	REGISTER	299
# define	REGXYZ	300
# define	SYMBOL	301
# define	INTEGER	302
# define	COUNTER	303
# define	FUNCTION	304
# define	IREGREG	305
# define	IIMMIMM	306
# define	IREGIMM	307
# define	IIMMREG	308
# define	IREG	309
# define	IIMM	310
# define	INOARGS	311
# define	IINDIRC	312
# define	ILPM	313


extern YYSTYPE yylval;

#endif /* not BISON_AVRPARSE_H */
