case 100:
YY_RULE_SETUP
/* #line 218 "avrlex.l" */
{ yylval.opcode = O_LSR;    return IREG;    }
	YY_BREAK
case 101:
YY_RULE_SETUP
/* #line 219 "avrlex.l" */
{ yylval.opcode = O_DEC;    return IREG;    }
	YY_BREAK
case 102:
YY_RULE_SETUP
/* #line 220 "avrlex.l" */
{ yylval.opcode = O_POP;    return IREG;    }
	YY_BREAK
case 103:
YY_RULE_SETUP
/* #line 221 "avrlex.l" */
{ yylval.opcode = O_PUSH;   return IREG;    }
	YY_BREAK
case 104:
YY_RULE_SETUP
/* #line 222 "avrlex.l" */
{ yylval.opcode = O_CLR;    return IREG;    }
	YY_BREAK
case 105:
YY_RULE_SETUP
/* #line 223 "avrlex.l" */
{ yylval.opcode = O_TST;    return IREG;    }
	YY_BREAK
case 106:
YY_RULE_SETUP
/* #line 224 "avrlex.l" */
{ yylval.opcode = O_COM;    return IREG;    }
	YY_BREAK
case 107:
YY_RULE_SETUP
/* #line 225 "avrlex.l" */
{ yylval.opcode = O_ROR;    return IREG;    }
	YY_BREAK
case 108:
YY_RULE_SETUP
/* #line 226 "avrlex.l" */
{ yylval.opcode = O_SEC;    return INOARGS; } /* No arguments */
	YY_BREAK
case 109:
YY_RULE_SETUP
/* #line 227 "avrlex.l" */
{ yylval.opcode = O_SEZ;    return INOARGS; }
	YY_BREAK
case 110:
YY_RULE_SETUP
/* #line 228 "avrlex.l" */
{ yylval.opcode = O_SEN;    return INOARGS; }
	YY_BREAK
case 111:
YY_RULE_SETUP
/* #line 229 "avrlex.l" */
{ yylval.opcode = O_SEV;    return INOARGS; }
	YY_BREAK
case 112:
YY_RULE_SETUP
/* #line 230 "avrlex.l" */
{ yylval.opcode = O_SES;    return INOARGS; }
	YY_BREAK
case 113:
YY_RULE_SETUP
/* #line 231 "avrlex.l" */
{ yylval.opcode = O_SEH;    return INOARGS; }
	YY_BREAK
case 114:
YY_RULE_SETUP
/* #line 232 "avrlex.l" */
{ yylval.opcode = O_SET;    return INOARGS; }
	YY_BREAK
case 115:
YY_RULE_SETUP
/* #line 233 "avrlex.l" */
{ yylval.opcode = O_SEI;    return INOARGS; }
	YY_BREAK
case 116:
YY_RULE_SETUP
/* #line 234 "avrlex.l" */
{ yylval.opcode = O_CLC;    return INOARGS; }
	YY_BREAK
case 117:
YY_RULE_SETUP
/* #line 235 "avrlex.l" */
{ yylval.opcode = O_CLZ;    return INOARGS; }
	YY_BREAK
case 118:
YY_RULE_SETUP
/* #line 236 "avrlex.l" */
{ yylval.opcode = O_CLN;    return INOARGS; }
	YY_BREAK
case 119:
YY_RULE_SETUP
/* #line 237 "avrlex.l" */
{ yylval.opcode = O_CLV;    return INOARGS; }
	YY_BREAK
case 120:
YY_RULE_SETUP
/* #line 238 "avrlex.l" */
{ yylval.opcode = O_CLS;    return INOARGS; }
	YY_BREAK
case 121:
YY_RULE_SETUP
/* #line 239 "avrlex.l" */
{ yylval.opcode = O_CLH;    return INOARGS; }
	YY_BREAK
case 122:
YY_RULE_SETUP
/* #line 240 "avrlex.l" */
{ yylval.opcode = O_CLT;    return INOARGS; }
	YY_BREAK
case 123:
YY_RULE_SETUP
/* #line 241 "avrlex.l" */
{ yylval.opcode = O_CLI;    return INOARGS; }
	YY_BREAK
case 124:
YY_RULE_SETUP
/* #line 242 "avrlex.l" */
{ yylval.opcode = O_NOP;    return INOARGS; }
	YY_BREAK
case 125:
YY_RULE_SETUP
/* #line 243 "avrlex.l" */
{ yylval.opcode = O_ICALL;  return INOARGS; } /* Misc. */
	YY_BREAK
case 126:
YY_RULE_SETUP
/* #line 244 "avrlex.l" */
{ yylval.opcode = O_IJMP;   return INOARGS; }
	YY_BREAK
case 127:
YY_RULE_SETUP
/* #line 245 "avrlex.l" */
{ yylval.opcode = O_RETI;   return INOARGS; }
	YY_BREAK
case 128:
YY_RULE_SETUP
/* #line 246 "avrlex.l" */
{ yylval.opcode = O_RET;    return INOARGS; }
	YY_BREAK
case 129:
YY_RULE_SETUP
/* #line 247 "avrlex.l" */
{ yylval.opcode = O_SLEEP;  return INOARGS; }
	YY_BREAK
case 130:
YY_RULE_SETUP
/* #line 248 "avrlex.l" */
{ yylval.opcode = O_WDR;    return INOARGS; }
	YY_BREAK
case 131:
YY_RULE_SETUP
/* #line 249 "avrlex.l" */
{ yylval.opcode = O_EIJMP;  return INOARGS; }
	YY_BREAK
case 132:
YY_RULE_SETUP
/* #line 250 "avrlex.l" */
{ yylval.opcode = O_EICALL; return INOARGS; }
	YY_BREAK
case 133:
YY_RULE_SETUP
/* #line 251 "avrlex.l" */
{ yylval.opcode = O_SPM;    return INOARGS; }
	YY_BREAK
case 134:
YY_RULE_SETUP
/* #line 252 "avrlex.l" */
{ yylval.opcode = O_ESPM;   return INOARGS; }
	YY_BREAK
case 135:
YY_RULE_SETUP
/* #line 253 "avrlex.l" */
{ yylval.opcode = O_BCLR;   return IIMM;    }
	YY_BREAK
case 136:
YY_RULE_SETUP
/* #line 254 "avrlex.l" */
{ yylval.opcode = O_BSET;   return IIMM;    }
	YY_BREAK
case 137:
YY_RULE_SETUP
/* #line 255 "avrlex.l" */
{ yylval.opcode = O_RCALL;  return IIMM;    }
	YY_BREAK
case 138:
YY_RULE_SETUP
/* #line 256 "avrlex.l" */
{ yylval.opcode = O_RJMP;   return IIMM;    }
	YY_BREAK
case 139:
YY_RULE_SETUP
/* #line 257 "avrlex.l" */
{ yylval.opcode = O_JMP;    return IIMM;    }
	YY_BREAK
case 140:
YY_RULE_SETUP
/* #line 258 "avrlex.l" */
{ yylval.opcode = O_CALL;   return IIMM;    }
	YY_BREAK
case 141:
YY_RULE_SETUP
/* #line 259 "avrlex.l" */
{ yylval.opcode = O_BRBC;   return IIMMIMM; }
	YY_BREAK
case 142:
YY_RULE_SETUP
/* #line 260 "avrlex.l" */
{ yylval.opcode = O_BRBS;   return IIMMIMM; }
	YY_BREAK
case 143:
YY_RULE_SETUP
/* #line 261 "avrlex.l" */
{ yylval.opcode = O_CBI;    return IIMMIMM; }
	YY_BREAK
case 144:
YY_RULE_SETUP
/* #line 262 "avrlex.l" */
{ yylval.opcode = O_SBI;    return IIMMIMM; }
	YY_BREAK
case 145:
YY_RULE_SETUP
/* #line 263 "avrlex.l" */
{ yylval.opcode = O_SBIC;   return IIMMIMM; }
	YY_BREAK
case 146:
YY_RULE_SETUP
/* #line 264 "avrlex.l" */
{ yylval.opcode = O_SBIS;   return IIMMIMM; }
	YY_BREAK
case 147:
YY_RULE_SETUP
/* #line 265 "avrlex.l" */
{ yylval.opcode = O_OUT;    return IIMMREG; }
	YY_BREAK
case 148:
YY_RULE_SETUP
/* #line 266 "avrlex.l" */
{ yylval.opcode = O_STS;    return IIMMREG; }
	YY_BREAK
case 149:
YY_RULE_SETUP
/* #line 267 "avrlex.l" */
{ yylval.opcode = O_ROL;    return IREG;    }
	YY_BREAK
case 150:
YY_RULE_SETUP
/* #line 268 "avrlex.l" */
{ yylval.opcode = O_SER;    return IREG;    }
	YY_BREAK
case 151:
YY_RULE_SETUP
/* #line 269 "avrlex.l" */
{ yylval.opcode = O_LSL;    return IREG;    }
	YY_BREAK
case 152:
YY_RULE_SETUP
/* #line 270 "avrlex.l" */
{ yylval.opcode = O_BLD;    return IREGIMM; }
	YY_BREAK
case 153:
YY_RULE_SETUP
/* #line 271 "avrlex.l" */
{ yylval.opcode = O_BST;    return IREGIMM; }
	YY_BREAK
case 154:
YY_RULE_SETUP
/* #line 272 "avrlex.l" */
{ yylval.opcode = O_CBR;    return IREGIMM; }
	YY_BREAK
case 155:
YY_RULE_SETUP
/* #line 273 "avrlex.l" */
{ yylval.opcode = O_IN;     return IREGIMM; }
	YY_BREAK
case 156:
YY_RULE_SETUP
/* #line 274 "avrlex.l" */
{ yylval.opcode = O_SBRC;   return IREGIMM; }
	YY_BREAK
case 157:
YY_RULE_SETUP
/* #line 275 "avrlex.l" */
{ yylval.opcode = O_SBRS;   return IREGIMM; }
	YY_BREAK
case 158:
YY_RULE_SETUP
/* #line 276 "avrlex.l" */
{ yylval.opcode = O_LD;     return IINDIRC; }
	YY_BREAK
case 159:
YY_RULE_SETUP
/* #line 277 "avrlex.l" */
{ yylval.opcode = O_ST;     return IINDIRC; }
	YY_BREAK
case 160:
YY_RULE_SETUP
/* #line 278 "avrlex.l" */
{ yylval.opcode = O_LDD;    return IINDIRC; }
	YY_BREAK
case 161:
YY_RULE_SETUP
/* #line 279 "avrlex.l" */
{ yylval.opcode = O_STD;    return IINDIRC; }
	YY_BREAK
case 162:
YY_RULE_SETUP
/* #line 280 "avrlex.l" */
{ yylval.regid = 26;        return REGXYZ;  }
	YY_BREAK
case 163:
YY_RULE_SETUP
/* #line 281 "avrlex.l" */
{ yylval.regid = 28;        return REGXYZ;  }
	YY_BREAK
case 164:
YY_RULE_SETUP
/* #line 282 "avrlex.l" */
{ yylval.regid = 30;        return REGXYZ;  }
	YY_BREAK
case 165:
YY_RULE_SETUP
/* #line 283 "avrlex.l" */
{ yylval.opcode = O_LPM;    return ILPM;    }
	YY_BREAK
case 166:
YY_RULE_SETUP
/* #line 284 "avrlex.l" */
{ yylval.opcode = O_ELPM;   return ILPM;    }
	YY_BREAK
case 167:
YY_RULE_SETUP
/* #line 286 "avrlex.l" */
{ yylval.val.value=yycodepos/2;yylval.val.valid=TRUE; return COUNTER;}
	YY_BREAK
case 168:
YY_RULE_SETUP
/* #line 287 "avrlex.l" */
{ yylval.val.value=yydatapos;  yylval.val.valid=TRUE; return COUNTER;}
	YY_BREAK
case 169:
YY_RULE_SETUP
/* #line 288 "avrlex.l" */
{ yylval.val.value=yyerompos;  yylval.val.valid=TRUE; return COUNTER;}
	YY_BREAK
case 170:
YY_RULE_SETUP
/* #line 290 "avrlex.l" */
list(LIST_NO_MACRO);
	YY_BREAK
case 171:
YY_RULE_SETUP
/* #line 291 "avrlex.l" */
list(LIST_NO);
	YY_BREAK
case 172:
YY_RULE_SETUP
/* #line 292 "avrlex.l" */
list(LIST_YES_MACRO);
	YY_BREAK
case 173:
YY_RULE_SETUP
/* #line 293 "avrlex.l" */
list(LIST_YES);
	YY_BREAK
case 174:
YY_RULE_SETUP
/* #line 294 "avrlex.l" */
yyeol=TRUE; if((ret=doexit()   )!=SKIPRESTART) return ret;
	YY_BREAK
case 175:
YY_RULE_SETUP
/* #line 295 "avrlex.l" */
if(includefile()==EOL) return EOL;
	YY_BREAK
case 176:
YY_RULE_SETUP
/* #line 296 "avrlex.l" */
return macrodef();
	YY_BREAK
case 177:
YY_RULE_SETUP
/* #line 297 "avrlex.l" */
errorin(E_ENDM);
	YY_BREAK
case 178:
YY_RULE_SETUP
/* #line 298 "avrlex.l" */
return string();
	YY_BREAK
case 179:
YY_RULE_SETUP
/* #line 299 "avrlex.l" */
return registers();
	YY_BREAK
case 180:
YY_RULE_SETUP
/* #line 300 "avrlex.l" */
return integers();
	YY_BREAK
case 181:
YY_RULE_SETUP
/* #line 301 "avrlex.l" */
return integers();
	YY_BREAK
case 182:
YY_RULE_SETUP
/* #line 302 "avrlex.l" */
return integers();
	YY_BREAK
case 183:
YY_RULE_SETUP
/* #line 303 "avrlex.l" */
return integers();
	YY_BREAK
case 184:
YY_RULE_SETUP
/* #line 304 "avrlex.l" */
return integers();
	YY_BREAK
case 185:
YY_RULE_SETUP
/* #line 305 "avrlex.l" */
return character();
	YY_BREAK
case 186:
YY_RULE_SETUP
/* #line 306 "avrlex.l" */
warningin(W_HEX_ESCAPE_INVALID);yylval.val.valid = TRUE;yylval.val.value = 0; return INTEGER;
	YY_BREAK
case 187:
YY_RULE_SETUP
/* #line 307 "avrlex.l" */
return character();
	YY_BREAK
case 188:
YY_RULE_SETUP
/* #line 308 "avrlex.l" */
warningin(W_OCT_ESCAPE_INVALID);yylval.val.valid = TRUE;yylval.val.value = 0; return INTEGER;
	YY_BREAK
case 189:
YY_RULE_SETUP
/* #line 309 "avrlex.l" */
return character();
	YY_BREAK
case 190:
YY_RULE_SETUP
/* #line 310 "avrlex.l" */
return character();
	YY_BREAK
case 191:
YY_RULE_SETUP
/* #line 311 "avrlex.l" */
if((ret=identifier())!=SKIPRESTART)return ret;
	YY_BREAK
case 192:
YY_RULE_SETUP
/* #line 313 "avrlex.l" */
yyeol=TRUE; return endofline();
	YY_BREAK
case 193:
YY_RULE_SETUP
/* #line 314 "avrlex.l" */
;
	YY_BREAK
case 194:
YY_RULE_SETUP
/* #line 315 "avrlex.l" */
;
	YY_BREAK
case 195:
YY_RULE_SETUP
/* #line 316 "avrlex.l" */
;
	YY_BREAK
case 196:
YY_RULE_SETUP
/* #line 317 "avrlex.l" */
;
	YY_BREAK
case 197:
YY_RULE_SETUP
/* #line 318 "avrlex.l" */
illegal();
	YY_BREAK
case 198:
YY_RULE_SETUP
/* #line 320 "avrlex.l" */
ECHO;
	YY_BREAK
