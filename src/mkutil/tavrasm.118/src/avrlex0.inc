case 1:
YY_RULE_SETUP
/* #line 116 "avrlex.l" */
return STAR;
	YY_BREAK
case 2:
YY_RULE_SETUP
/* #line 117 "avrlex.l" */
return DIV;
	YY_BREAK
case 3:
YY_RULE_SETUP
/* #line 118 "avrlex.l" */
return MOD;
	YY_BREAK
case 4:
YY_RULE_SETUP
/* #line 119 "avrlex.l" */
return LS;
	YY_BREAK
case 5:
YY_RULE_SETUP
/* #line 120 "avrlex.l" */
return RS;
	YY_BREAK
case 6:
YY_RULE_SETUP
/* #line 121 "avrlex.l" */
return LE;
	YY_BREAK
case 7:
YY_RULE_SETUP
/* #line 122 "avrlex.l" */
return GE;
	YY_BREAK
case 8:
YY_RULE_SETUP
/* #line 123 "avrlex.l" */
return LESS;
	YY_BREAK
case 9:
YY_RULE_SETUP
/* #line 124 "avrlex.l" */
return GREAT;
	YY_BREAK
case 10:
YY_RULE_SETUP
/* #line 125 "avrlex.l" */
return EQ;
	YY_BREAK
case 11:
YY_RULE_SETUP
/* #line 126 "avrlex.l" */
return NE;
	YY_BREAK
case 12:
YY_RULE_SETUP
/* #line 127 "avrlex.l" */
return OROR;
	YY_BREAK
case 13:
YY_RULE_SETUP
/* #line 128 "avrlex.l" */
return ANDAND;
	YY_BREAK
case 14:
YY_RULE_SETUP
/* #line 129 "avrlex.l" */
return OR;
	YY_BREAK
case 15:
YY_RULE_SETUP
/* #line 130 "avrlex.l" */
return AND;
	YY_BREAK
case 16:
YY_RULE_SETUP
/* #line 131 "avrlex.l" */
return XOR;
	YY_BREAK
case 17:
YY_RULE_SETUP
/* #line 132 "avrlex.l" */
return LPAR;
	YY_BREAK
case 18:
YY_RULE_SETUP
/* #line 133 "avrlex.l" */
return RPAR;
	YY_BREAK
case 19:
YY_RULE_SETUP
/* #line 134 "avrlex.l" */
return COLON;
	YY_BREAK
case 20:
YY_RULE_SETUP
/* #line 135 "avrlex.l" */
return COMMA;
	YY_BREAK
case 21:
YY_RULE_SETUP
/* #line 136 "avrlex.l" */
return DOT;
	YY_BREAK
case 22:
YY_RULE_SETUP
/* #line 137 "avrlex.l" */
return PLUS;
	YY_BREAK
case 23:
YY_RULE_SETUP
/* #line 138 "avrlex.l" */
return MINUS;
	YY_BREAK
case 24:
YY_RULE_SETUP
/* #line 139 "avrlex.l" */
return EQUAL;
	YY_BREAK
case 25:
YY_RULE_SETUP
/* #line 140 "avrlex.l" */
return WAVE;
	YY_BREAK
case 26:
YY_RULE_SETUP
/* #line 141 "avrlex.l" */
return NOT;
	YY_BREAK
case 27:
YY_RULE_SETUP
/* #line 143 "avrlex.l" */
return DEF;
	YY_BREAK
case 28:
YY_RULE_SETUP
/* #line 144 "avrlex.l" */
return EQU;
	YY_BREAK
case 29:
YY_RULE_SETUP
/* #line 145 "avrlex.l" */
return EQU;
	YY_BREAK
case 30:
YY_RULE_SETUP
/* #line 146 "avrlex.l" */
return DB;
	YY_BREAK
case 31:
YY_RULE_SETUP
/* #line 147 "avrlex.l" */
return DW;
	YY_BREAK
case 32:
YY_RULE_SETUP
/* #line 148 "avrlex.l" */
return ORG;
	YY_BREAK
case 33:
YY_RULE_SETUP
/* #line 149 "avrlex.l" */
return BYTE;
	YY_BREAK
case 34:
YY_RULE_SETUP
/* #line 150 "avrlex.l" */
return SET;
	YY_BREAK
case 35:
YY_RULE_SETUP
/* #line 151 "avrlex.l" */
return DEVICE;
	YY_BREAK
case 36:
YY_RULE_SETUP
/* #line 153 "avrlex.l" */
{ yysegment = SEGMENT_CODE;   return CSEG;  }
	YY_BREAK
case 37:
YY_RULE_SETUP
/* #line 154 "avrlex.l" */
{ yysegment = SEGMENT_DATA;   return ESEG;  }
	YY_BREAK
case 38:
YY_RULE_SETUP
/* #line 155 "avrlex.l" */
{ yysegment = SEGMENT_EEPROM; return DSEG;  }
	YY_BREAK
case 39:
YY_RULE_SETUP
/* #line 157 "avrlex.l" */
{ yylval.func   = OP_LOW;   return FUNCTION;}
	YY_BREAK
case 40:
YY_RULE_SETUP
/* #line 158 "avrlex.l" */
{ yylval.func   = OP_HIGH;  return FUNCTION;}
	YY_BREAK
case 41:
YY_RULE_SETUP
/* #line 159 "avrlex.l" */
{ yylval.func   = OP_LOW;   return FUNCTION;}
	YY_BREAK
case 42:
YY_RULE_SETUP
/* #line 160 "avrlex.l" */
{ yylval.func   = OP_BYTE2; return FUNCTION;}
	YY_BREAK
case 43:
YY_RULE_SETUP
/* #line 161 "avrlex.l" */
{ yylval.func   = OP_BYTE3; return FUNCTION;}
	YY_BREAK
case 44:
YY_RULE_SETUP
/* #line 162 "avrlex.l" */
{ yylval.func   = OP_BYTE4; return FUNCTION;}
	YY_BREAK
case 45:
YY_RULE_SETUP
/* #line 163 "avrlex.l" */
{ yylval.func   = OP_LWRD;  return FUNCTION;}
	YY_BREAK
case 46:
YY_RULE_SETUP
/* #line 164 "avrlex.l" */
{ yylval.func   = OP_HWRD;  return FUNCTION;}
	YY_BREAK
case 47:
YY_RULE_SETUP
/* #line 165 "avrlex.l" */
{ yylval.func   = OP_PAGE;  return FUNCTION;}
	YY_BREAK
case 48:
YY_RULE_SETUP
/* #line 166 "avrlex.l" */
{ yylval.func   = OP_EXP2;  return FUNCTION;}
	YY_BREAK
case 49:
YY_RULE_SETUP
/* #line 167 "avrlex.l" */
{ yylval.func   = OP_LOG2;  return FUNCTION;}
	YY_BREAK
case 50:
YY_RULE_SETUP
/* #line 168 "avrlex.l" */
{ yylval.opcode = O_CPC;    return IREGREG; } /* Register */
	YY_BREAK
case 51:
YY_RULE_SETUP
/* #line 169 "avrlex.l" */
{ yylval.opcode = O_CP;     return IREGREG; }
	YY_BREAK
case 52:
YY_RULE_SETUP
/* #line 170 "avrlex.l" */
{ yylval.opcode = O_SBC;    return IREGREG; }
	YY_BREAK
case 53:
YY_RULE_SETUP
/* #line 171 "avrlex.l" */
{ yylval.opcode = O_SUB;    return IREGREG; }
	YY_BREAK
case 54:
YY_RULE_SETUP
/* #line 172 "avrlex.l" */
{ yylval.opcode = O_ADD;    return IREGREG; }
	YY_BREAK
case 55:
YY_RULE_SETUP
/* #line 173 "avrlex.l" */
{ yylval.opcode = O_ADC;    return IREGREG; }
	YY_BREAK
case 56:
YY_RULE_SETUP
/* #line 174 "avrlex.l" */
{ yylval.opcode = O_CPSE;   return IREGREG; }
	YY_BREAK
case 57:
YY_RULE_SETUP
/* #line 175 "avrlex.l" */
{ yylval.opcode = O_AND;    return IREGREG; }
	YY_BREAK
case 58:
YY_RULE_SETUP
/* #line 176 "avrlex.l" */
{ yylval.opcode = O_EOR;    return IREGREG; }
	YY_BREAK
case 59:
YY_RULE_SETUP
/* #line 177 "avrlex.l" */
{ yylval.opcode = O_OR;     return IREGREG; }
	YY_BREAK
case 60:
YY_RULE_SETUP
/* #line 178 "avrlex.l" */
{ yylval.opcode = O_MOV;    return IREGREG; }
	YY_BREAK
case 61:
YY_RULE_SETUP
/* #line 179 "avrlex.l" */
{ yylval.opcode = O_MOVW;   return IREGREG; }
	YY_BREAK
case 62:
YY_RULE_SETUP
/* #line 180 "avrlex.l" */
{ yylval.opcode = O_MUL;    return IREGREG; }
	YY_BREAK
case 63:
YY_RULE_SETUP
/* #line 181 "avrlex.l" */
{ yylval.opcode = O_MULS;   return IREGREG; }
	YY_BREAK
case 64:
YY_RULE_SETUP
/* #line 182 "avrlex.l" */
{ yylval.opcode = O_MULSU;  return IREGREG; }
	YY_BREAK
case 65:
YY_RULE_SETUP
/* #line 183 "avrlex.l" */
{ yylval.opcode = O_FMUL;   return IREGREG; }
	YY_BREAK
case 66:
YY_RULE_SETUP
/* #line 184 "avrlex.l" */
{ yylval.opcode = O_FMULS;  return IREGREG; }
	YY_BREAK
case 67:
YY_RULE_SETUP
/* #line 185 "avrlex.l" */
{ yylval.opcode = O_FMULSU; return IREGREG; }
	YY_BREAK
case 68:
YY_RULE_SETUP
/* #line 186 "avrlex.l" */
{ yylval.opcode = O_ADIW;   return IREGIMM; } /* Reg/Imm */
	YY_BREAK
case 69:
YY_RULE_SETUP
/* #line 187 "avrlex.l" */
{ yylval.opcode = O_SBIW;   return IREGIMM; }
	YY_BREAK
case 70:
YY_RULE_SETUP
/* #line 188 "avrlex.l" */
{ yylval.opcode = O_CPI;    return IREGIMM; }
	YY_BREAK
case 71:
YY_RULE_SETUP
/* #line 189 "avrlex.l" */
{ yylval.opcode = O_SBCI;   return IREGIMM; }
	YY_BREAK
case 72:
YY_RULE_SETUP
/* #line 190 "avrlex.l" */
{ yylval.opcode = O_SUBI;   return IREGIMM; }
	YY_BREAK
case 73:
YY_RULE_SETUP
/* #line 191 "avrlex.l" */
{ yylval.opcode = O_ORI;    return IREGIMM; }
	YY_BREAK
case 74:
YY_RULE_SETUP
/* #line 192 "avrlex.l" */
{ yylval.opcode = O_ANDI;   return IREGIMM; }
	YY_BREAK
case 75:
YY_RULE_SETUP
/* #line 193 "avrlex.l" */
{ yylval.opcode = O_LDI;    return IREGIMM; }
	YY_BREAK
case 76:
YY_RULE_SETUP
/* #line 194 "avrlex.l" */
{ yylval.opcode = O_LDS;    return IREGIMM; }
	YY_BREAK
case 77:
YY_RULE_SETUP
/* #line 195 "avrlex.l" */
{ yylval.opcode = O_SBR;    return IREGIMM; }
	YY_BREAK
case 78:
YY_RULE_SETUP
/* #line 196 "avrlex.l" */
{ yylval.opcode = O_BRCC;   return IIMM;    } /* Immediate */
	YY_BREAK
case 79:
YY_RULE_SETUP
/* #line 197 "avrlex.l" */
{ yylval.opcode = O_BRCS;   return IIMM;    }
	YY_BREAK
case 80:
YY_RULE_SETUP
/* #line 198 "avrlex.l" */
{ yylval.opcode = O_BRNE;   return IIMM;    }
	YY_BREAK
case 81:
YY_RULE_SETUP
/* #line 199 "avrlex.l" */
{ yylval.opcode = O_BREQ;   return IIMM;    }
	YY_BREAK
case 82:
YY_RULE_SETUP
/* #line 200 "avrlex.l" */
{ yylval.opcode = O_BRPL;   return IIMM;    }
	YY_BREAK
case 83:
YY_RULE_SETUP
/* #line 201 "avrlex.l" */
{ yylval.opcode = O_BRMI;   return IIMM;    }
	YY_BREAK
case 84:
YY_RULE_SETUP
/* #line 202 "avrlex.l" */
{ yylval.opcode = O_BRVC;   return IIMM;    }
	YY_BREAK
case 85:
YY_RULE_SETUP
/* #line 203 "avrlex.l" */
{ yylval.opcode = O_BRVS;   return IIMM;    }
	YY_BREAK
case 86:
YY_RULE_SETUP
/* #line 204 "avrlex.l" */
{ yylval.opcode = O_BRGE;   return IIMM;    }
	YY_BREAK
case 87:
YY_RULE_SETUP
/* #line 205 "avrlex.l" */
{ yylval.opcode = O_BRLT;   return IIMM;    }
	YY_BREAK
case 88:
YY_RULE_SETUP
/* #line 206 "avrlex.l" */
{ yylval.opcode = O_BRHC;   return IIMM;    }
	YY_BREAK
case 89:
YY_RULE_SETUP
/* #line 207 "avrlex.l" */
{ yylval.opcode = O_BRHS;   return IIMM;    }
	YY_BREAK
case 90:
YY_RULE_SETUP
/* #line 208 "avrlex.l" */
{ yylval.opcode = O_BRTC;   return IIMM;    }
	YY_BREAK
case 91:
YY_RULE_SETUP
/* #line 209 "avrlex.l" */
{ yylval.opcode = O_BRTS;   return IIMM;    }
	YY_BREAK
case 92:
YY_RULE_SETUP
/* #line 210 "avrlex.l" */
{ yylval.opcode = O_BRID;   return IIMM;    }
	YY_BREAK
case 93:
YY_RULE_SETUP
/* #line 211 "avrlex.l" */
{ yylval.opcode = O_BRIE;   return IIMM;    }
	YY_BREAK
case 94:
YY_RULE_SETUP
/* #line 212 "avrlex.l" */
{ yylval.opcode = O_BRSH;   return IIMM;    }
	YY_BREAK
case 95:
YY_RULE_SETUP
/* #line 213 "avrlex.l" */
{ yylval.opcode = O_BRLO;   return IIMM;    }
	YY_BREAK
case 96:
YY_RULE_SETUP
/* #line 214 "avrlex.l" */
{ yylval.opcode = O_NEG;    return IREG;    } /* Register */
	YY_BREAK
case 97:
YY_RULE_SETUP
/* #line 215 "avrlex.l" */
{ yylval.opcode = O_SWAP;   return IREG;    }
	YY_BREAK
case 98:
YY_RULE_SETUP
/* #line 216 "avrlex.l" */
{ yylval.opcode = O_INC;    return IREG;    }
	YY_BREAK
case 99:
YY_RULE_SETUP
/* #line 217 "avrlex.l" */
{ yylval.opcode = O_ASR;    return IREG;    }
	YY_BREAK
