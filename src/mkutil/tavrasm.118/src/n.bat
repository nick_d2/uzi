del tavrasm.exe
cl -Zi -I. -DAVRLANG=LANGUAGE_US -otavrasm.exe -Tpavrasm.cc -Tpsemantic.cc -Tpsymbol.cc -Tputils.cc -Tpavrparse.cc -Tpavrlex.cc
del *.obj
del *.pdb
del *.ilk
copy tavrasm.exe ..\..\..\bin
