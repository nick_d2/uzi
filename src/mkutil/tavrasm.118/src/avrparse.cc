/* A Bison parser, made from avrparse.y
   by GNU bison 1.35.  */

#define YYBISON 1  /* Identify Bison output.  */

# define	STAR	257
# define	DIV	258
# define	MOD	259
# define	LS	260
# define	RS	261
# define	LE	262
# define	GE	263
# define	LESS	264
# define	GREAT	265
# define	EQ	266
# define	NE	267
# define	OR	268
# define	XOR	269
# define	AND	270
# define	OROR	271
# define	ANDAND	272
# define	LPAR	273
# define	RPAR	274
# define	COLON	275
# define	COMMA	276
# define	DOT	277
# define	EQUAL	278
# define	PLUS	279
# define	MINUS	280
# define	WAVE	281
# define	NOT	282
# define	EOL	283
# define	RESTART	284
# define	ENDOFFILE	285
# define	DEF	286
# define	EQU	287
# define	DB	288
# define	DW	289
# define	ORG	290
# define	CSEG	291
# define	DSEG	292
# define	ESEG	293
# define	BYTE	294
# define	SET	295
# define	DEVICE	296
# define	STRING	297
# define	MACRODEF	298
# define	REGISTER	299
# define	REGXYZ	300
# define	SYMBOL	301
# define	INTEGER	302
# define	COUNTER	303
# define	FUNCTION	304
# define	IREGREG	305
# define	IIMMIMM	306
# define	IREGIMM	307
# define	IIMMREG	308
# define	IREG	309
# define	IIMM	310
# define	INOARGS	311
# define	IINDIRC	312
# define	ILPM	313

/* #line 43 "avrparse.y" */


/// Include //////////////////////////////////////////////////////////////////

#include <string.h>
#include <stdio.h>
#ifdef _MSC_VER /* Nick */
#include <malloc.h>
#else
#include <alloc.h>
#endif
#include "avrasm.hh"
#include "symbol.hh"
#include "semantic.hh"
#include "utils.hh"

/// Extern ///////////////////////////////////////////////////////////////////

GLOBALS(extern);
extern char *yytext;

/// Prototypes ///////////////////////////////////////////////////////////////

int yylex();

/// yyerror //////////////////////////////////////////////////////////////////

void yyerror(char *s)
{
  s = s; // Used for debugging purposes
}

/// Start of grammar /////////////////////////////////////////////////////////


/* #line 77 "avrparse.y" */
#ifndef YYSTYPE
typedef union
{
  regSA                 regid;
  instSA                inst;
  opcodeSA              opcode;
  valueSA               val;
  nameSA                name;
  symbolSA              symb;
  indirectSA            indi;
  functionSA            func;
  stringSA              string;
} yystype;
# define YYSTYPE yystype
# define YYSTYPE_IS_TRIVIAL 1
#endif
#ifndef YYDEBUG
# define YYDEBUG 0
#endif



#define	YYFINAL		223
#define	YYFLAG		-32768
#define	YYNTBASE	60

/* YYTRANSLATE(YYLEX) -- Bison token number corresponding to YYLEX. */
#define YYTRANSLATE(x) ((unsigned)(x) <= 313 ? yytranslate[x] : 89)

/* YYTRANSLATE[YYLEX] -- Bison token number corresponding to YYLEX. */
static const char yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     3,     4,     5,
       6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59
};

#if YYDEBUG
static const short yyprhs[] =
{
       0,     0,     4,     5,     8,    10,    13,    16,    19,    21,
      24,    30,    36,    42,    48,    52,    56,    59,    65,    71,
      73,    77,    81,    85,    89,    93,    97,   101,   105,   108,
     114,   118,   120,   122,   125,   127,   133,   139,   145,   146,
     151,   152,   157,   161,   164,   167,   170,   174,   180,   182,
     186,   190,   194,   198,   202,   206,   210,   214,   218,   222,
     226,   230,   233,   235,   238,   242,   246,   248,   250,   252,
     256,   258,   260,   262,   264,   266,   270,   275,   278,   281,
     284,   286,   288,   292,   296,   300,   302,   306,   310,   312,
     316,   320,   322,   326,   330,   334,   338,   340,   344,   348,
     350,   354,   356,   360,   362,   366,   368,   372,   374,   378
};
static const short yyrhs[] =
{
      62,    30,    62,     0,     0,    62,    63,     0,    63,     0,
      67,    64,     0,    67,    29,     0,    67,    68,     0,    29,
       0,     1,    29,     0,    51,    66,    22,    66,    29,     0,
      53,    66,    22,    75,    29,     0,    54,    75,    22,    66,
      29,     0,    52,    75,    22,    75,    29,     0,    56,    75,
      29,     0,    55,    66,    29,     0,    57,    29,     0,    58,
      66,    22,    71,    29,     0,    58,    71,    22,    66,    29,
       0,    65,     0,    51,     1,    29,     0,    53,     1,    29,
       0,    54,     1,    29,     0,    52,     1,    29,     0,    56,
       1,    29,     0,    55,     1,    29,     0,    57,     1,    29,
       0,    58,     1,    29,     0,    59,    29,     0,    59,    66,
      22,    71,    29,     0,    59,     1,    29,     0,    45,     0,
      47,     0,    47,    21,     0,    61,     0,    32,    47,    24,
      45,    29,     0,    32,    47,    24,    47,    29,     0,    33,
      47,    24,    75,    29,     0,     0,    34,    69,    72,    29,
       0,     0,    35,    70,    74,    29,     0,    36,    75,    29,
       0,    37,    29,     0,    38,    29,     0,    39,    29,     0,
      40,    75,    29,     0,    41,    47,    24,    75,    29,     0,
      44,     0,    42,    47,    29,     0,    32,     1,    29,     0,
      33,     1,    29,     0,    34,     1,    29,     0,    35,     1,
      29,     0,    36,     1,    29,     0,    37,     1,    29,     0,
      38,     1,    29,     0,    39,     1,    29,     0,    40,     1,
      29,     0,    41,     1,    29,     0,    42,     1,    29,     0,
      26,    46,     0,    46,     0,    46,    25,     0,    46,    25,
      75,     0,    72,    22,    73,     0,    73,     0,    43,     0,
      75,     0,    74,    22,    75,     0,    75,     0,    88,     0,
      48,     0,    49,     0,    47,     0,    19,    75,    20,     0,
      50,    19,    75,    20,     0,    26,    77,     0,    27,    77,
       0,    28,    77,     0,    76,     0,    77,     0,    78,     3,
      77,     0,    78,     4,    77,     0,    78,     5,    77,     0,
      78,     0,    79,    25,    78,     0,    79,    26,    78,     0,
      79,     0,    80,     6,    79,     0,    80,     7,    79,     0,
      80,     0,    81,    10,    80,     0,    81,    11,    80,     0,
      81,     8,    80,     0,    81,     9,    80,     0,    81,     0,
      82,    12,    81,     0,    82,    13,    81,     0,    82,     0,
      83,    16,    82,     0,    83,     0,    84,    15,    83,     0,
      84,     0,    85,    14,    84,     0,    85,     0,    86,    18,
      85,     0,    86,     0,    87,    17,    86,     0,    87,     0
};

#endif

#if YYDEBUG
/* YYRLINE[YYN] -- source line where rule number YYN was defined. */
static const short yyrline[] =
{
       0,   173,   176,   190,   191,   204,   205,   206,   207,   211,
     245,   247,   249,   251,   253,   255,   257,   259,   261,   264,
     268,   269,   270,   271,   272,   273,   274,   275,   288,   290,
     295,   308,   309,   331,   332,   357,   358,   359,   360,   360,
     361,   361,   362,   363,   364,   365,   366,   367,   368,   369,
     373,   374,   375,   376,   377,   378,   379,   380,   381,   382,
     383,   401,   403,   405,   407,   426,   427,   430,   431,   434,
     435,   448,   451,   452,   453,   462,   464,   468,   469,   470,
     471,   474,   475,   477,   479,   483,   484,   486,   490,   491,
     493,   497,   498,   500,   502,   504,   508,   509,   511,   515,
     516,   520,   521,   525,   526,   530,   531,   535,   536,   540
};
#endif


#if (YYDEBUG) || defined YYERROR_VERBOSE

/* YYTNAME[TOKEN_NUM] -- String name of the token TOKEN_NUM. */
static const char *const yytname[] =
{
  "$", "error", "$undefined.", "STAR", "DIV", "MOD", "LS", "RS", "LE", "GE", 
  "LESS", "GREAT", "EQ", "NE", "OR", "XOR", "AND", "OROR", "ANDAND", 
  "LPAR", "RPAR", "COLON", "COMMA", "DOT", "EQUAL", "PLUS", "MINUS", 
  "WAVE", "NOT", "EOL", "RESTART", "ENDOFFILE", "DEF", "EQU", "DB", "DW", 
  "ORG", "CSEG", "DSEG", "ESEG", "BYTE", "SET", "DEVICE", "STRING", 
  "MACRODEF", "REGISTER", "REGXYZ", "SYMBOL", "INTEGER", "COUNTER", 
  "FUNCTION", "IREGREG", "IIMMIMM", "IREGIMM", "IIMMREG", "IREG", "IIMM", 
  "INOARGS", "IINDIRC", "ILPM", "program", "e", "programlist", 
  "programelement", "instruction", "lpminst", "registername", "label", 
  "directive", "@1", "@2", "indirectaddr", "byteexprlist", "byteelement", 
  "wordexprlist", "expr", "primary_expr", "unary_expr", "mult_expr", 
  "additive_expr", "shift_expr", "relational_expr", "equality_expr", 
  "AND_expression", "exclusive_OR", "inclusive_OR", "logical_AND", 
  "logical_OR", "composite_expr", 0
};
#endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives. */
static const short yyr1[] =
{
       0,    60,    61,    62,    62,    63,    63,    63,    63,    63,
      64,    64,    64,    64,    64,    64,    64,    64,    64,    64,
      64,    64,    64,    64,    64,    64,    64,    64,    65,    65,
      65,    66,    66,    67,    67,    68,    68,    68,    69,    68,
      70,    68,    68,    68,    68,    68,    68,    68,    68,    68,
      68,    68,    68,    68,    68,    68,    68,    68,    68,    68,
      68,    71,    71,    71,    71,    72,    72,    73,    73,    74,
      74,    75,    76,    76,    76,    76,    76,    77,    77,    77,
      77,    78,    78,    78,    78,    79,    79,    79,    80,    80,
      80,    81,    81,    81,    81,    81,    82,    82,    82,    83,
      83,    84,    84,    85,    85,    86,    86,    87,    87,    88
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN. */
static const short yyr2[] =
{
       0,     3,     0,     2,     1,     2,     2,     2,     1,     2,
       5,     5,     5,     5,     3,     3,     2,     5,     5,     1,
       3,     3,     3,     3,     3,     3,     3,     3,     2,     5,
       3,     1,     1,     2,     1,     5,     5,     5,     0,     4,
       0,     4,     3,     2,     2,     2,     3,     5,     1,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     2,     1,     2,     3,     3,     1,     1,     1,     3,
       1,     1,     1,     1,     1,     3,     4,     2,     2,     2,
       1,     1,     3,     3,     3,     1,     3,     3,     1,     3,
       3,     1,     3,     3,     3,     3,     1,     3,     3,     1,
       3,     1,     3,     1,     3,     1,     3,     1,     3,     1
};

/* YYDEFACT[S] -- default rule to reduce with in state S when YYTABLE
   doesn't specify something else to do.  Zero means the default is an
   error. */
static const short yydefact[] =
{
       0,     0,     8,     0,    34,     0,     4,     0,     9,    33,
       0,     3,     6,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    48,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     5,    19,     7,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    74,    72,    73,     0,     0,    80,    81,    85,    88,
      91,    96,    99,   101,   103,   105,   107,   109,    71,     0,
      43,     0,    44,     0,    45,     0,     0,     0,     0,     0,
       0,     0,    31,    32,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    16,     0,     0,    62,
       0,     0,     0,    28,     0,    50,     0,    51,     0,    52,
      67,     0,    66,    68,    53,     0,    70,    54,     0,    77,
      78,    79,     0,    42,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    55,    56,    57,    58,    46,    59,     0,    60,
      49,    20,     0,    23,     0,    21,     0,    22,     0,    25,
      15,    24,    14,    26,    27,    61,    63,     0,     0,    30,
       0,     0,     0,     0,     0,    39,     0,    41,    75,     0,
      82,    83,    84,    86,    87,    89,    90,    94,    95,    92,
      93,    97,    98,   100,   102,   104,   106,   108,     0,     0,
       0,     0,     0,    64,     0,     0,     0,    35,    36,    37,
      65,    69,    76,    47,    10,    13,    11,    12,    17,    18,
      29,     0,     0,     0
};

static const short yydefgoto[] =
{
     221,     4,     5,     6,    34,    35,    84,     7,    36,    43,
      45,   101,   111,   112,   115,   113,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68
};

static const short yypact[] =
{
     170,   -19,-32768,   -16,-32768,   128,-32768,   289,-32768,-32768,
     170,-32768,-32768,    32,    33,    59,    96,   212,    34,    75,
      88,   217,    37,    63,-32768,    43,   229,    56,   262,    73,
     267,    98,    36,    30,-32768,-32768,-32768,    14,    -8,    12,
      13,    21,    66,   256,    71,   223,    87,   223,   223,   223,
     223,-32768,-32768,-32768,   100,    92,-32768,-32768,   130,   115,
     141,    15,   138,   109,   113,   116,   118,   121,-32768,   125,
  -32768,   147,-32768,   148,-32768,   149,   159,   160,   166,   162,
     163,   164,-32768,-32768,   172,   167,   173,   168,   176,   171,
     179,   186,   187,   190,   191,   203,-32768,   204,   188,   210,
     215,   219,   218,-32768,   224,-32768,    -6,-32768,   223,-32768,
  -32768,   -10,-32768,-32768,-32768,    -2,-32768,-32768,   232,-32768,
  -32768,-32768,   223,-32768,   223,   223,   223,   223,   223,   223,
     223,   223,   223,   223,   223,   223,   223,   223,   223,   223,
     223,   223,-32768,-32768,-32768,-32768,-32768,-32768,   223,-32768,
  -32768,-32768,    51,-32768,   223,-32768,   223,-32768,    51,-32768,
  -32768,-32768,-32768,-32768,-32768,-32768,   223,   -24,    51,-32768,
     -24,   225,   240,   245,   256,-32768,   223,-32768,-32768,   233,
  -32768,-32768,-32768,   130,   130,   115,   115,   141,   141,   141,
     141,    15,    15,   138,   109,   113,   116,   118,   251,   258,
     263,   268,   269,-32768,   271,   272,   273,-32768,-32768,-32768,
  -32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
  -32768,   285,   291,-32768
};

static const short yypgoto[] =
{
  -32768,-32768,   248,     3,-32768,-32768,   -26,-32768,-32768,-32768,
  -32768,  -138,-32768,   122,-32768,   -17,-32768,   -32,    25,    26,
     -20,    38,   182,   169,   174,   180,   193,-32768,-32768
};


#define	YYLAST		348


static const short yytable[] =
{
      55,    88,    98,    92,    76,     9,   100,   104,    11,    86,
       8,    90,   174,    94,    -1,     1,   119,   120,   121,   175,
     176,   105,    99,   131,   132,   133,   134,   177,   116,   204,
     118,   102,   206,    38,    40,    69,   106,    97,    77,   171,
      11,   172,   107,     2,    81,   108,    -2,    -2,    -2,    -2,
      -2,    -2,    -2,    -2,    -2,    -2,    -2,    87,    -2,   103,
      42,     3,    98,    70,    79,    -2,    -2,    -2,    -2,    -2,
      -2,    -2,    -2,    -2,    91,    82,    71,    83,   -38,    39,
      41,    82,    99,    83,    78,   -38,   -38,   -38,    82,    73,
      83,   173,   180,   181,   182,   109,    82,    44,    83,    95,
     114,    82,   -38,    83,    72,   179,   -38,   -38,   -38,   -38,
      80,   187,   188,   189,   190,   -40,   117,    74,    82,   122,
      83,   123,   -40,   -40,   -40,   137,   199,    96,   138,     1,
     139,   198,   202,   124,   125,   126,   140,   200,   141,   201,
     127,   128,   205,   -40,   -40,   -40,   -40,   129,   130,   203,
     135,   136,   183,   184,   142,   185,   186,     2,    10,   211,
      -2,    -2,    -2,    -2,    -2,    -2,    -2,    -2,    -2,    -2,
      -2,     1,    -2,   191,   192,     3,   143,   144,   145,    -2,
      -2,    -2,    -2,    -2,    -2,    -2,    -2,    -2,   146,   147,
     148,   149,   150,   151,   152,   154,   153,   155,   156,     2,
     157,   158,    -2,    -2,    -2,    -2,    -2,    -2,    -2,    -2,
      -2,    -2,    -2,    46,    -2,   159,   160,     3,    75,   161,
     162,    -2,    -2,    -2,    -2,    -2,    -2,    -2,    -2,    -2,
      85,    47,   163,   164,   165,   166,    47,   167,    48,    49,
      50,   168,    47,    48,    49,    50,   170,   169,    47,    48,
      49,    50,   178,   212,   207,    48,    49,    50,    37,    51,
      52,    53,    54,    89,    51,    52,    53,    54,    93,   208,
      51,    52,    53,    54,   209,    47,    51,    52,    53,    54,
     213,    47,    48,    49,    50,   222,    47,   214,    48,    49,
      50,   223,   215,    48,    49,    50,   210,   216,   217,   110,
     218,   219,   220,    51,    52,    53,    54,   194,     0,    51,
      52,    53,    54,   195,    51,    52,    53,    54,    12,   193,
     196,    13,    14,    15,    16,    17,    18,    19,    20,    21,
      22,    23,     0,    24,   197,     0,     0,     0,     0,     0,
      25,    26,    27,    28,    29,    30,    31,    32,    33
};

static const short yycheck[] =
{
      17,    27,    26,    29,    21,    21,    32,    33,     5,    26,
      29,    28,    22,    30,     0,     1,    48,    49,    50,    29,
      22,    29,    46,     8,     9,    10,    11,    29,    45,   167,
      47,     1,   170,     1,     1,     1,    24,     1,     1,    45,
      37,    47,    29,    29,     1,    24,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,     1,    44,    29,
       1,    47,    26,    29,     1,    51,    52,    53,    54,    55,
      56,    57,    58,    59,     1,    45,     1,    47,    19,    47,
      47,    45,    46,    47,    47,    26,    27,    28,    45,     1,
      47,   108,   124,   125,   126,    29,    45,     1,    47,     1,
      29,    45,    43,    47,    29,   122,    47,    48,    49,    50,
      47,   131,   132,   133,   134,    19,    29,    29,    45,    19,
      47,    29,    26,    27,    28,    16,   152,    29,    15,     1,
      14,   148,   158,     3,     4,     5,    18,   154,    17,   156,
      25,    26,   168,    47,    48,    49,    50,     6,     7,   166,
      12,    13,   127,   128,    29,   129,   130,    29,    30,   176,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      42,     1,    44,   135,   136,    47,    29,    29,    29,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    29,    29,
      24,    29,    29,    29,    22,    22,    29,    29,    22,    29,
      29,    22,    32,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,     1,    44,    29,    29,    47,     1,    29,
      29,    51,    52,    53,    54,    55,    56,    57,    58,    59,
       1,    19,    29,    29,    46,    25,    19,    22,    26,    27,
      28,    22,    19,    26,    27,    28,    22,    29,    19,    26,
      27,    28,    20,    20,    29,    26,    27,    28,    10,    47,
      48,    49,    50,     1,    47,    48,    49,    50,     1,    29,
      47,    48,    49,    50,    29,    19,    47,    48,    49,    50,
      29,    19,    26,    27,    28,     0,    19,    29,    26,    27,
      28,     0,    29,    26,    27,    28,   174,    29,    29,    43,
      29,    29,    29,    47,    48,    49,    50,   138,    -1,    47,
      48,    49,    50,   139,    47,    48,    49,    50,    29,   137,
     140,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    -1,    44,   141,    -1,    -1,    -1,    -1,    -1,
      51,    52,    53,    54,    55,    56,    57,    58,    59
};
/* -*-C-*-  Note some compilers choke on comments on `#line' lines.  */
/* #line 3 "/usr/share/bison/bison.simple" */

/* Skeleton output parser for bison,

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002 Free Software
   Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* This is the parser code that is written into each bison parser when
   the %semantic_parser declaration is not specified in the grammar.
   It was written by Richard Stallman by simplifying the hairy parser
   used when %semantic_parser is specified.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

#if ! defined (yyoverflow) || defined (YYERROR_VERBOSE)

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# if YYSTACK_USE_ALLOCA
#  define YYSTACK_ALLOC alloca
# else
#  ifndef YYSTACK_USE_ALLOCA
#   if defined (alloca) || defined (_ALLOCA_H)
#    define YYSTACK_ALLOC alloca
#   else
#    ifdef __GNUC__
#     define YYSTACK_ALLOC __builtin_alloca
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning. */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
# else
#  if defined (__STDC__) || defined (__cplusplus)
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   define YYSIZE_T size_t
#  endif
#if 1 /* Nick */
void *yy_flex_alloc( unsigned long size );
void yy_flex_free( void *ptr );
#define YYSTACK_ALLOC yy_flex_alloc
#define YYSTACK_FREE yy_flex_free
#else
#  define YYSTACK_ALLOC malloc
#  define YYSTACK_FREE free
#endif
# endif
#endif /* ! defined (yyoverflow) || defined (YYERROR_VERBOSE) */


#if (! defined (yyoverflow) \
     && (! defined (__cplusplus) \
	 || (YYLTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  short yyss;
  YYSTYPE yyvs;
# if YYLSP_NEEDED
  YYLTYPE yyls;
# endif
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAX (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# if YYLSP_NEEDED
#  define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short) + sizeof (YYSTYPE) + sizeof (YYLTYPE))	\
      + 2 * YYSTACK_GAP_MAX)
# else
#  define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short) + sizeof (YYSTYPE))				\
      + YYSTACK_GAP_MAX)
# endif

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  register YYSIZE_T yyi;		\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (0)
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAX;	\
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (0)

#endif


#if ! defined (YYSIZE_T) && defined (__SIZE_TYPE__)
# define YYSIZE_T __SIZE_TYPE__
#endif
#if ! defined (YYSIZE_T) && defined (size_t)
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T)
# if defined (__STDC__) || defined (__cplusplus)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# endif
#endif
#if ! defined (YYSIZE_T)
# define YYSIZE_T unsigned int
#endif

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0
#define YYACCEPT	goto yyacceptlab
#define YYABORT 	goto yyabortlab
#define YYERROR		goto yyerrlab1
/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */
#define YYFAIL		goto yyerrlab
#define YYRECOVERING()  (!!yyerrstatus)
#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yychar1 = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { 								\
      yyerror ("syntax error: cannot back up");			\
      YYERROR;							\
    }								\
while (0)

#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Compute the default location (before the actions
   are run).

   When YYLLOC_DEFAULT is run, CURRENT is set the location of the
   first token.  By default, to implement support for ranges, extend
   its range to the last symbol.  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)       	\
   Current.last_line   = Rhs[N].last_line;	\
   Current.last_column = Rhs[N].last_column;
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#if YYPURE
# if YYLSP_NEEDED
#  ifdef YYLEX_PARAM
#   define YYLEX		yylex (&yylval, &yylloc, YYLEX_PARAM)
#  else
#   define YYLEX		yylex (&yylval, &yylloc)
#  endif
# else /* !YYLSP_NEEDED */
#  ifdef YYLEX_PARAM
#   define YYLEX		yylex (&yylval, YYLEX_PARAM)
#  else
#   define YYLEX		yylex (&yylval)
#  endif
# endif /* !YYLSP_NEEDED */
#else /* !YYPURE */
# define YYLEX			yylex ()
#endif /* !YYPURE */


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (0)
/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
#endif /* !YYDEBUG */

/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   SIZE_MAX < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#if YYMAXDEPTH == 0
# undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif

#ifdef YYERROR_VERBOSE

# ifndef yystrlen
#  if defined (__GLIBC__) && defined (_STRING_H)
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
#   if defined (__STDC__) || defined (__cplusplus)
yystrlen (const char *yystr)
#   else
yystrlen (yystr)
     const char *yystr;
#   endif
{
  register const char *yys = yystr;

  while (*yys++ != '\0')
    continue;

  return yys - yystr - 1;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined (__GLIBC__) && defined (_STRING_H) && defined (_GNU_SOURCE)
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
#   if defined (__STDC__) || defined (__cplusplus)
yystpcpy (char *yydest, const char *yysrc)
#   else
yystpcpy (yydest, yysrc)
     char *yydest;
     const char *yysrc;
#   endif
{
  register char *yyd = yydest;
  register const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif
#endif

/* #line 315 "/usr/share/bison/bison.simple" */


/* The user can define YYPARSE_PARAM as the name of an argument to be passed
   into yyparse.  The argument should have type void *.
   It should actually point to an object.
   Grammar actions can access the variable by casting it
   to the proper pointer type.  */

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
#  define YYPARSE_PARAM_ARG void *YYPARSE_PARAM
#  define YYPARSE_PARAM_DECL
# else
#  define YYPARSE_PARAM_ARG YYPARSE_PARAM
#  define YYPARSE_PARAM_DECL void *YYPARSE_PARAM;
# endif
#else /* !YYPARSE_PARAM */
# define YYPARSE_PARAM_ARG
# define YYPARSE_PARAM_DECL
#endif /* !YYPARSE_PARAM */

/* Prevent warning if -Wstrict-prototypes.  */
#ifdef __GNUC__
# ifdef YYPARSE_PARAM
int yyparse (void *);
# else
int yyparse (void);
# endif
#endif

/* YY_DECL_VARIABLES -- depending whether we use a pure parser,
   variables are global, or local to YYPARSE.  */

#define YY_DECL_NON_LSP_VARIABLES			\
/* The lookahead symbol.  */				\
int yychar;						\
							\
/* The semantic value of the lookahead symbol. */	\
YYSTYPE yylval;						\
							\
/* Number of parse errors so far.  */			\
int yynerrs;

#if YYLSP_NEEDED
# define YY_DECL_VARIABLES			\
YY_DECL_NON_LSP_VARIABLES			\
						\
/* Location data for the lookahead symbol.  */	\
YYLTYPE yylloc;
#else
# define YY_DECL_VARIABLES			\
YY_DECL_NON_LSP_VARIABLES
#endif


/* If nonreentrant, generate the variables here. */

#if !YYPURE
YY_DECL_VARIABLES
#endif  /* !YYPURE */

int
yyparse (YYPARSE_PARAM_ARG)
     YYPARSE_PARAM_DECL
{
  /* If reentrant, generate the variables here. */
#if YYPURE
  YY_DECL_VARIABLES
#endif  /* !YYPURE */

  register int yystate;
  register int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Lookahead token as an internal (translated) token number.  */
  int yychar1 = 0;

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack. */
  short	yyssa[YYINITDEPTH];
  short *yyss = yyssa;
  register short *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  register YYSTYPE *yyvsp;

#if YYLSP_NEEDED
  /* The location stack.  */
  YYLTYPE yylsa[YYINITDEPTH];
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;
#endif

#if YYLSP_NEEDED
# define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)
#else
# define YYPOPSTACK   (yyvsp--, yyssp--)
#endif

  YYSIZE_T yystacksize = YYINITDEPTH;


  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
#if YYLSP_NEEDED
  YYLTYPE yyloc;
#endif

  /* When reducing, the number of symbols on the RHS of the reduced
     rule. */
  int yylen;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;
#if YYLSP_NEEDED
  yylsp = yyls;
#endif
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed. so pushing a state here evens the stacks.
     */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyssp >= yyss + yystacksize - 1)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack. Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	short *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  */
# if YYLSP_NEEDED
	YYLTYPE *yyls1 = yyls;
	/* This used to be a conditional around just the two extra args,
	   but that might be undefined if yyoverflow is a macro.  */
	yyoverflow ("parser stack overflow",
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yyls1, yysize * sizeof (*yylsp),
		    &yystacksize);
	yyls = yyls1;
# else
	yyoverflow ("parser stack overflow",
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);
# endif
	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyoverflowlab;
# else
      /* Extend the stack our own way.  */
      if (yystacksize >= YYMAXDEPTH)
	goto yyoverflowlab;
      yystacksize *= 2;
      if (yystacksize > YYMAXDEPTH)
	yystacksize = YYMAXDEPTH;

      {
	short *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyoverflowlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);
# if YYLSP_NEEDED
	YYSTACK_RELOCATE (yyls);
# endif
# undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
#if YYLSP_NEEDED
      yylsp = yyls + yysize - 1;
#endif

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyssp >= yyss + yystacksize - 1)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* yychar is either YYEMPTY or YYEOF
     or a valid token in external form.  */

  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  /* Convert token to internal form (in yychar1) for indexing tables with */

  if (yychar <= 0)		/* This means end of input. */
    {
      yychar1 = 0;
      yychar = YYEOF;		/* Don't call YYLEX any more */

      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yychar1 = YYTRANSLATE (yychar);

#if YYDEBUG
     /* We have to keep this `#if YYDEBUG', since we use variables
	which are defined only if `YYDEBUG' is set.  */
      if (yydebug)
	{
	  YYFPRINTF (stderr, "Next token is %d (%s",
		     yychar, yytname[yychar1]);
	  /* Give the individual parser a way to print the precise
	     meaning of a token, for further debugging info.  */
# ifdef YYPRINT
	  YYPRINT (stderr, yychar, yylval);
# endif
	  YYFPRINTF (stderr, ")\n");
	}
#endif
    }

  yyn += yychar1;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != yychar1)
    goto yydefault;

  yyn = yytable[yyn];

  /* yyn is what to do for this token type in this state.
     Negative => reduce, -yyn is rule number.
     Positive => shift, yyn is new state.
       New state is final state => don't bother to shift,
       just return success.
     0, or most negative number => error.  */

  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrlab;

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */
  YYDPRINTF ((stderr, "Shifting token %d (%s), ",
	      yychar, yytname[yychar1]));

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;
#if YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  yystate = yyn;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to the semantic value of
     the lookahead token.  This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

#if YYLSP_NEEDED
  /* Similarly for the default location.  Let the user run additional
     commands if for instance locations are ranges.  */
  yyloc = yylsp[1-yylen];
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
#endif

#if YYDEBUG
  /* We have to keep this `#if YYDEBUG', since we use variables which
     are defined only if `YYDEBUG' is set.  */
  if (yydebug)
    {
      int yyi;

      YYFPRINTF (stderr, "Reducing via rule %d (line %d), ",
		 yyn, yyrline[yyn]);

      /* Print the symbols being reduced, and their result.  */
      for (yyi = yyprhs[yyn]; yyrhs[yyi] > 0; yyi++)
	YYFPRINTF (stderr, "%s ", yytname[yyrhs[yyi]]);
      YYFPRINTF (stderr, " -> %s\n", yytname[yyr1[yyn]]);
    }
#endif

  switch (yyn) {

case 5:
/* #line 204 "avrparse.y" */
{ tolog(); ;
    break;}
case 6:
/* #line 205 "avrparse.y" */
{ tolog(); ;
    break;}
case 7:
/* #line 206 "avrparse.y" */
{ tolog(); ;
    break;}
case 8:
/* #line 207 "avrparse.y" */
{ tolog(); ;
    break;}
case 9:
/* #line 211 "avrparse.y" */
{ error(E_UNKNOWN_OPCODE); tolog();  ;
    break;}
case 10:
/* #line 246 "avrparse.y" */
{genRegReg(yyvsp[-4].opcode, yyvsp[-3].regid, yyvsp[-1].regid);;
    break;}
case 11:
/* #line 248 "avrparse.y" */
{genRegImm(yyvsp[-4].opcode, yyvsp[-3].regid, &yyvsp[-1].val);;
    break;}
case 12:
/* #line 250 "avrparse.y" */
{genImmReg(yyvsp[-4].opcode, &yyvsp[-3].val, yyvsp[-1].regid);;
    break;}
case 13:
/* #line 252 "avrparse.y" */
{genImmImm(yyvsp[-4].opcode, &yyvsp[-3].val, &yyvsp[-1].val);;
    break;}
case 14:
/* #line 254 "avrparse.y" */
{genImmedi(yyvsp[-2].opcode, &yyvsp[-1].val);;
    break;}
case 15:
/* #line 256 "avrparse.y" */
{genRegist(yyvsp[-2].opcode, yyvsp[-1].regid);;
    break;}
case 16:
/* #line 258 "avrparse.y" */
{genNoargs(yyvsp[-1].opcode);;
    break;}
case 17:
/* #line 260 "avrparse.y" */
{genIndirc(yyvsp[-4].opcode, &yyvsp[-1].indi, yyvsp[-3].regid,TRUE);;
    break;}
case 18:
/* #line 262 "avrparse.y" */
{genIndirc(yyvsp[-4].opcode, &yyvsp[-3].indi, yyvsp[-1].regid);;
    break;}
case 20:
/* #line 268 "avrparse.y" */
{ error(E_REGISTER_EXPECTED);  ;
    break;}
case 21:
/* #line 269 "avrparse.y" */
{ error(E_INVALID_REGIMM_SPEC);    ;
    break;}
case 22:
/* #line 270 "avrparse.y" */
{ error(E_INVALID_REGIMM_SPEC);    ;
    break;}
case 23:
/* #line 271 "avrparse.y" */
{ error(E_INVALID_IMMEDIATE_SPEC); ;
    break;}
case 24:
/* #line 272 "avrparse.y" */
{ error(E_INVALID_IMMEDIATE_SPEC); ;
    break;}
case 25:
/* #line 273 "avrparse.y" */
{ error(E_INVALID_REGISTER_SPEC);  ;
    break;}
case 26:
/* #line 274 "avrparse.y" */
{ error(E_NOARGS_EXPECTED_SPEC);   ;
    break;}
case 27:
/* #line 275 "avrparse.y" */
{ error(E_INVALID_REGISTER_SPEC);  ;
    break;}
case 28:
/* #line 289 "avrparse.y" */
{ genLpm(yyvsp[-1].opcode, 0, NULL, FALSE); ;
    break;}
case 29:
/* #line 291 "avrparse.y" */
{ genLpm(yyvsp[-4].opcode, yyvsp[-3].regid, &yyvsp[-1].indi, TRUE); ;
    break;}
case 30:
/* #line 295 "avrparse.y" */
{ error(E_INVALID_REGISTER_SPEC); ;
    break;}
case 32:
/* #line 310 "avrparse.y" */
{ if(yyvsp[0].symb->isdefine) 
                      yyval.regid=yyvsp[0].symb->reg;
                    else
		    {
                      yyval.regid=-1;
                      errorin(E_INVALID_REGISTER_SPEC);
                    }
                  ;
    break;}
case 33:
/* #line 331 "avrparse.y" */
{ doLab(yyvsp[-1].symb); ;
    break;}
case 35:
/* #line 357 "avrparse.y" */
{doDef(yyvsp[-3].symb,yyvsp[-1].regid);;
    break;}
case 36:
/* #line 358 "avrparse.y" */
{doDef(yyvsp[-3].symb,yyvsp[-1].symb); ;
    break;}
case 37:
/* #line 359 "avrparse.y" */
{doEqu(yyvsp[-3].symb,&yyvsp[-1].val); ;
    break;}
case 38:
/* #line 360 "avrparse.y" */
{doAdb();;
    break;}
case 39:
/* #line 360 "avrparse.y" */
{/*XXX*/                   ;
    break;}
case 40:
/* #line 361 "avrparse.y" */
{doAdw();;
    break;}
case 41:
/* #line 361 "avrparse.y" */
{                          ;
    break;}
case 42:
/* #line 362 "avrparse.y" */
{doOrg(&yyvsp[-1].val);          ;
    break;}
case 43:
/* #line 363 "avrparse.y" */
{                          ;
    break;}
case 44:
/* #line 364 "avrparse.y" */
{                          ;
    break;}
case 45:
/* #line 365 "avrparse.y" */
{                          ;
    break;}
case 46:
/* #line 366 "avrparse.y" */
{doByt(&yyvsp[-1].val);          ;
    break;}
case 47:
/* #line 367 "avrparse.y" */
{doSet(yyvsp[-3].symb,&yyvsp[-1].val); ;
    break;}
case 48:
/* #line 368 "avrparse.y" */
{                          ;
    break;}
case 49:
/* #line 369 "avrparse.y" */
{doDev(yyvsp[-1].symb);          ;
    break;}
case 50:
/* #line 373 "avrparse.y" */
{ error(E_EXPECTED_ID_REG);    ;
    break;}
case 51:
/* #line 374 "avrparse.y" */
{ error(E_EXPECTED_ID_EXPR);   ;
    break;}
case 52:
/* #line 375 "avrparse.y" */
{ error(E_EXPECTED_VALLIST);   ;
    break;}
case 53:
/* #line 376 "avrparse.y" */
{ error(E_EXPECTED_VALLIST);   ;
    break;}
case 54:
/* #line 377 "avrparse.y" */
{ error(E_EXPECTED_VAL_LABEL); ;
    break;}
case 55:
/* #line 378 "avrparse.y" */
{ error(E_EXPECTED_NOARGS);    ;
    break;}
case 56:
/* #line 379 "avrparse.y" */
{ error(E_EXPECTED_NOARGS);    ;
    break;}
case 57:
/* #line 380 "avrparse.y" */
{ error(E_EXPECTED_NOARGS);    ;
    break;}
case 58:
/* #line 381 "avrparse.y" */
{ error(E_EXPECTED_VAL_LABEL); ;
    break;}
case 59:
/* #line 382 "avrparse.y" */
{ error(E_EXPECTED_ID_EXPR);   ;
    break;}
case 60:
/* #line 383 "avrparse.y" */
{ error(E_EXPECTED_DEVICE);    ;
    break;}
case 61:
/* #line 402 "avrparse.y" */
{yyval.indi.regno=yyvsp[0].regid;yyval.indi.plus=2;yyval.indi.disp=0;;
    break;}
case 62:
/* #line 404 "avrparse.y" */
{yyval.indi.regno=yyvsp[0].regid;yyval.indi.plus=0;yyval.indi.disp=0;;
    break;}
case 63:
/* #line 406 "avrparse.y" */
{yyval.indi.regno=yyvsp[-1].regid;yyval.indi.plus=1;yyval.indi.disp=0;;
    break;}
case 64:
/* #line 408 "avrparse.y" */
{ yyval.indi.regno  = yyvsp[-2].regid;
                    yyval.indi.plus=1;yyval.indi.disp=1;yyval.indi.offset=yyvsp[0].val;;
    break;}
case 67:
/* #line 430 "avrparse.y" */
{doAdb(yyvsp[0].string); ;
    break;}
case 68:
/* #line 431 "avrparse.y" */
{doAdb(&yyvsp[0].val);;
    break;}
case 69:
/* #line 434 "avrparse.y" */
{doAdw(&yyvsp[0].val);;
    break;}
case 70:
/* #line 435 "avrparse.y" */
{doAdw(&yyvsp[0].val);;
    break;}
case 74:
/* #line 454 "avrparse.y" */
{yyval.val.valid=yyvsp[0].symb->valid;
                   yyval.val.value=yyvsp[0].symb->value
                   +yyvsp[0].symb->islabel*yyvsp[0].symb->macrolabel*
                    (yyoffset    *(yyvsp[0].symb->segment==SEGMENT_CODE))
                   +yyvsp[0].symb->islabel*yyvsp[0].symb->macrolabel*
                    (yydataoffset*(yyvsp[0].symb->segment==SEGMENT_DATA))
                   +yyvsp[0].symb->islabel*yyvsp[0].symb->macrolabel*
                    (yyeromoffset*(yyvsp[0].symb->segment==SEGMENT_EEPROM));;
    break;}
case 75:
/* #line 463 "avrparse.y" */
{ yyval.val=yyvsp[-1].val; ;
    break;}
case 76:
/* #line 465 "avrparse.y" */
{ genFun(yyvsp[-3].func, &yyvsp[-1].val, &yyval.val); ;
    break;}
case 77:
/* #line 468 "avrparse.y" */
{ oprUna(OP_MINUS, &yyvsp[0].val, &yyval.val); ;
    break;}
case 78:
/* #line 469 "avrparse.y" */
{ oprUna(OP_WAVE , &yyvsp[0].val, &yyval.val); ;
    break;}
case 79:
/* #line 470 "avrparse.y" */
{ oprUna(OP_NOT  , &yyvsp[0].val, &yyval.val); ;
    break;}
case 82:
/* #line 476 "avrparse.y" */
{ oprBin(&yyvsp[-2].val,OP_STAR  , &yyvsp[0].val, &yyval.val); ;
    break;}
case 83:
/* #line 478 "avrparse.y" */
{ oprBin(&yyvsp[-2].val,OP_DIV   , &yyvsp[0].val, &yyval.val); ;
    break;}
case 84:
/* #line 480 "avrparse.y" */
{ oprBin(&yyvsp[-2].val,OP_MOD   , &yyvsp[0].val, &yyval.val); ;
    break;}
case 86:
/* #line 485 "avrparse.y" */
{ oprBin(&yyvsp[-2].val,OP_PLUS  , &yyvsp[0].val, &yyval.val); ;
    break;}
case 87:
/* #line 487 "avrparse.y" */
{ oprBin(&yyvsp[-2].val,OP_MINUS , &yyvsp[0].val, &yyval.val); ;
    break;}
case 89:
/* #line 492 "avrparse.y" */
{ oprBin(&yyvsp[-2].val,OP_LS    , &yyvsp[0].val, &yyval.val); ;
    break;}
case 90:
/* #line 494 "avrparse.y" */
{ oprBin(&yyvsp[-2].val,OP_RS    , &yyvsp[0].val, &yyval.val); ;
    break;}
case 92:
/* #line 499 "avrparse.y" */
{ oprBin(&yyvsp[-2].val,OP_LESS  , &yyvsp[0].val, &yyval.val); ;
    break;}
case 93:
/* #line 501 "avrparse.y" */
{ oprBin(&yyvsp[-2].val,OP_GREAT , &yyvsp[0].val, &yyval.val); ;
    break;}
case 94:
/* #line 503 "avrparse.y" */
{ oprBin(&yyvsp[-2].val,OP_LE    , &yyvsp[0].val, &yyval.val); ;
    break;}
case 95:
/* #line 505 "avrparse.y" */
{ oprBin(&yyvsp[-2].val,OP_GE    , &yyvsp[0].val, &yyval.val); ;
    break;}
case 97:
/* #line 510 "avrparse.y" */
{ oprBin(&yyvsp[-2].val,OP_EQ    , &yyvsp[0].val, &yyval.val); ;
    break;}
case 98:
/* #line 512 "avrparse.y" */
{ oprBin(&yyvsp[-2].val,OP_NE    , &yyvsp[0].val, &yyval.val); ;
    break;}
case 100:
/* #line 517 "avrparse.y" */
{ oprBin(&yyvsp[-2].val,OP_AND   , &yyvsp[0].val, &yyval.val); ;
    break;}
case 102:
/* #line 522 "avrparse.y" */
{ oprBin(&yyvsp[-2].val,OP_XOR   , &yyvsp[0].val, &yyval.val); ;
    break;}
case 104:
/* #line 527 "avrparse.y" */
{ oprBin(&yyvsp[-2].val,OP_OR    , &yyvsp[0].val, &yyval.val); ;
    break;}
case 106:
/* #line 532 "avrparse.y" */
{ oprBin(&yyvsp[-2].val,OP_ANDAND, &yyvsp[0].val, &yyval.val); ;
    break;}
case 108:
/* #line 537 "avrparse.y" */
{ oprBin(&yyvsp[-2].val,OP_OROR  , &yyvsp[0].val, &yyval.val); ;
    break;}
}

/* #line 705 "/usr/share/bison/bison.simple" */


  yyvsp -= yylen;
  yyssp -= yylen;
#if YYLSP_NEEDED
  yylsp -= yylen;
#endif

#if YYDEBUG
  if (yydebug)
    {
      short *yyssp1 = yyss - 1;
      YYFPRINTF (stderr, "state stack now");
      while (yyssp1 != yyssp)
	YYFPRINTF (stderr, " %d", *++yyssp1);
      YYFPRINTF (stderr, "\n");
    }
#endif

  *++yyvsp = yyval;
#if YYLSP_NEEDED
  *++yylsp = yyloc;
#endif

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTBASE] + *yyssp;
  if (yystate >= 0 && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTBASE];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;

#ifdef YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (yyn > YYFLAG && yyn < YYLAST)
	{
	  YYSIZE_T yysize = 0;
	  char *yymsg;
	  int yyx, yycount;

	  yycount = 0;
	  /* Start YYX at -YYN if negative to avoid negative indexes in
	     YYCHECK.  */
	  for (yyx = yyn < 0 ? -yyn : 0;
	       yyx < (int) (sizeof (yytname) / sizeof (char *)); yyx++)
	    if (yycheck[yyx + yyn] == yyx)
	      yysize += yystrlen (yytname[yyx]) + 15, yycount++;
	  yysize += yystrlen ("parse error, unexpected ") + 1;
	  yysize += yystrlen (yytname[YYTRANSLATE (yychar)]);
	  yymsg = (char *) YYSTACK_ALLOC (yysize);
	  if (yymsg != 0)
	    {
	      char *yyp = yystpcpy (yymsg, "parse error, unexpected ");
	      yyp = yystpcpy (yyp, yytname[YYTRANSLATE (yychar)]);

	      if (yycount < 5)
		{
		  yycount = 0;
		  for (yyx = yyn < 0 ? -yyn : 0;
		       yyx < (int) (sizeof (yytname) / sizeof (char *));
		       yyx++)
		    if (yycheck[yyx + yyn] == yyx)
		      {
			const char *yyq = ! yycount ? ", expecting " : " or ";
			yyp = yystpcpy (yyp, yyq);
			yyp = yystpcpy (yyp, yytname[yyx]);
			yycount++;
		      }
		}
	      yyerror (yymsg);
	      YYSTACK_FREE (yymsg);
	    }
	  else
	    yyerror ("parse error; also virtual memory exhausted");
	}
      else
#endif /* defined (YYERROR_VERBOSE) */
	yyerror ("parse error");
    }
  goto yyerrlab1;


/*--------------------------------------------------.
| yyerrlab1 -- error raised explicitly by an action |
`--------------------------------------------------*/
yyerrlab1:
  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      /* return failure if at end of input */
      if (yychar == YYEOF)
	YYABORT;
      YYDPRINTF ((stderr, "Discarding token %d (%s).\n",
		  yychar, yytname[yychar1]));
      yychar = YYEMPTY;
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */

  yyerrstatus = 3;		/* Each real token shifted decrements this */

  goto yyerrhandle;


/*-------------------------------------------------------------------.
| yyerrdefault -- current state does not do anything special for the |
| error token.                                                       |
`-------------------------------------------------------------------*/
yyerrdefault:
#if 0
  /* This is wrong; only states that explicitly want error tokens
     should shift them.  */

  /* If its default is to accept any token, ok.  Otherwise pop it.  */
  yyn = yydefact[yystate];
  if (yyn)
    goto yydefault;
#endif


/*---------------------------------------------------------------.
| yyerrpop -- pop the current state because it cannot handle the |
| error token                                                    |
`---------------------------------------------------------------*/
yyerrpop:
  if (yyssp == yyss)
    YYABORT;
  yyvsp--;
  yystate = *--yyssp;
#if YYLSP_NEEDED
  yylsp--;
#endif

#if YYDEBUG
  if (yydebug)
    {
      short *yyssp1 = yyss - 1;
      YYFPRINTF (stderr, "Error: state stack now");
      while (yyssp1 != yyssp)
	YYFPRINTF (stderr, " %d", *++yyssp1);
      YYFPRINTF (stderr, "\n");
    }
#endif

/*--------------.
| yyerrhandle.  |
`--------------*/
yyerrhandle:
  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yyerrdefault;

  yyn += YYTERROR;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != YYTERROR)
    goto yyerrdefault;

  yyn = yytable[yyn];
  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrpop;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrpop;

  if (yyn == YYFINAL)
    YYACCEPT;

  YYDPRINTF ((stderr, "Shifting error token, "));

  *++yyvsp = yylval;
#if YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

/*---------------------------------------------.
| yyoverflowab -- parser overflow comes here.  |
`---------------------------------------------*/
yyoverflowlab:
  yyerror ("parser stack overflow");
  yyresult = 2;
  /* Fall through.  */

yyreturn:
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  return yyresult;
}
/* #line 544 "avrparse.y" */
