/************************************************
 * Graphical User Interface driver (GUI)        *
 * By J.W. Morsink for Retail Vision B.V.       *
 ************************************************/

#include <wnd.h>
#include "..\libc\malloc-l.h"
#undef NULL
#define NULL	0
#define TEXT_BOTTOM_ALIGNED

/*void *malloc(size_t size)
  {
  void *ptr;

  ptr = (void *)sbrk(size);
  return (ptr == (void *)-1) ? NULL : ptr;
  }

void free(void *ptr)
  {
  }

*/
/*#define JOOST_DEBUG
/*#define STR_DEBUG*/
/*#define MALLOC_DEBUG

/************************************************
 * Global variables for GUI                     *
 ************************************************/

struct Global_GUI* global;

/************************************************
 * Graphics                                     *
 ************************************************/

void SetCurrentGraphicsDevice(int n){
	global->lcd=global->lcda[n];
}

void GraphicsOpen(){
	struct sgttyb temp;
	char init[]={0x1B,'K'};

	global->lcda[0]=open("/dev/lcd0",O_RDWR);
	gtty(global->lcda[0],&temp);
	temp.sg_flags=RAW | UNBUFF;
	stty(global->lcda[0],&temp);

	write(global->lcda[0],init,2);

	global->lcda[1]=open("/dev/lcd1",O_RDWR);
	gtty(global->lcda[1],&temp);
	temp.sg_flags=RAW | UNBUFF;
	stty(global->lcda[1],&temp);

	write(global->lcda[1],init,2);
	SetCurrentGraphicsDevice(1);
	Text("HYTECH 1000");
	SetCurrentGraphicsDevice(0);
}

void GraphicsClose(){
	close(global->lcda[0]);
	close(global->lcda[1]);
}

void Move(int x, int y){
	unsigned char cmd[]={0x1B,'S',0,0};
	cmd[2]=(unsigned char) (x);
	cmd[3]=(unsigned char) (y);
	write(global->lcd,cmd,4);
}

void ClearRect(int sx, int sy){
	unsigned char cmd[]={0x1B,'C',0,0};
	cmd[2]=(unsigned char) (sx);
	cmd[3]=(unsigned char) (sy);
	write(global->lcd,cmd,4);
}

void DrawRectNC(int sx, int sy){
	unsigned char cmd[]={0x1B,'N',0,0}; 
	cmd[2]=(unsigned char) (sx);
	cmd[3]=(unsigned char) (sy);
	write(global->lcd,cmd,4);
}

void FillRect(int sx, int sy){
	unsigned char cmd[]={0x1B,'R',0,0};
	cmd[2]=(unsigned char) (sx);
	cmd[3]=(unsigned char) (sy);
	write(global->lcd,cmd,4);
}

void FillRectNC(int sx, int sy){
	unsigned char cmd[]={0x1B,'F',0,0};
	cmd[2]=(unsigned char) (sx);
	cmd[3]=(unsigned char) (sy);
	write(global->lcd,cmd,4);
}

void GrayRect(int sx,int sy){
	unsigned char cmd[]={0x1B,'h',0,0};
	cmd[2]=(unsigned char) (sx);
	cmd[3]=(unsigned char) (sy);
	write(global->lcd,cmd,4);
}

void Text(char *s){
	int c=strlen(s);
	write(global->lcd,s,c);
}

void TouchRegion(int msg, int x, int y, int sx, int sy){
	char touch[]={0x1B,'Z',0,0,0,0,0};
	touch[2]=(unsigned char) (msg);
	touch[3]=(unsigned char) (x);
	touch[4]=(unsigned char) (y);
	touch[5]=(unsigned char) (sx);
	touch[6]=(unsigned char) (sy);
	write(global->lcd,touch,7);
#ifdef JOOST_DEBUG
	printf("Touch: %x,%x,%x,%x,%x\n",msg,x,y,sx,sy); fflush(stdout);
#endif
}

void ClearScreen(){
	Move(0,0);
	ClearRect(0,0x80);
	TouchRegion(0,0,0,0,0x80);
}

void EnableScreen(unsigned char c){
	char cmd[]={0x1B,'E',0};
	cmd[2]=c;
	write(global->lcd,cmd,3);
}

void grSetFont(int f){
	char cmd[]={0x1B,0};
	cmd[1]=f & 0xff;
	write(global->lcd,cmd,2);
}

/************************************************
 * Fonts                                        *
 ************************************************/

struct Font* fontSearch(int f){
	struct Font* current=global->firstfont;
	while(current!=NULL && current->id!=f)
		current=current->nextfont;
	return current;
}

void setCurrentFont(int f){
	if(fontSearch(f)!=NULL){
		grSetFont(f);
		global->currentfont=fontSearch(f);
	}
}

void loadFont(int id,int (*sw)(char*),int (*sh)(char*),int sizedata, void* data){
	struct Font* font=(struct Font*) malloc(sizeof(struct Font));
#ifdef MALLOC_DEBUG
	printf("malloc: %x\n",font); fflush(stdout);
#endif
	font->id=id;
	font->stringwidth=sw;
	font->stringheight=sh;
	font->nextfont=NULL;
	font->sizefontdata=sizedata;
	font->fontdata=data;
	if(global->firstfont==NULL){
		global->firstfont=font;
		global->currentfont=font;
	}
	else
	{
		struct Font* current=global->firstfont;
		while(current->nextfont!=NULL)
			current=current->nextfont;
		current->nextfont=font;
	}
}

int stringwidth(char *str,int extra){
	int w=0,i=0;
	while(str[i]!=0x00){
		switch(str[i]){
		case 'i':
		case 'I':
		case '!':
			w+=2;
			break;
		case 'l':
			w+=3;
			break;
		case ',':
		case '1':
		case '.':
			w+=4;
			break;
		case 'r':
		case 'k':
		case 'j':
			w+=5;
			break;
		default:
			w+=6;
		}
		i++;
		w+=extra;
	}
	return w;
}

int stringheight(char ch, int extra){
	if(ch<0x80) return 7+extra;
	if(ch>=0xcc && ch<=0xdf) return 1+extra;
}

int stringheight01(char* ch){
	return stringheight(ch[0],0);
}

int stringheight2(char* ch){
	return stringheight(ch[0],5);
}

int stringwidth0(char *str){
	return stringwidth(str,0);
}

int stringwidth1(char *str){
	return stringwidth(str,2);
}

int stringwidth2(char *str){
	return stringwidth(str,3);
}
/************************************************
 * Components                                   *
 ************************************************/

struct Component* Component_new(int x,int y,int sx,int sy){
	struct Component* this=(struct Component*) (malloc(sizeof(struct Component)));
#ifdef MALLOC_DEBUG
	printf("malloc: %x\n",this); fflush(stdout);
#endif

	this->id=(++global->id);
	this->x=x;
	this->y=y;
	this->sizex=sx;
	this->sizey=sy;
	this->state=0;
	this->order=0;
	this->font=0;
	this->msg=0;
	this->parent=NULL;
	this->next_sibling=NULL;
	this->first_child=NULL;
	this->handler=NULL;
	this->paint=NULL;
	this->add=NULL;
	this->sizexds=0;
	this->xds=NULL;
	this->label=NULL;
	this->label2=NULL;
	return this;
}

void Component_setlabel(struct Component* this, char* s){
	int i=0;
	while(s[i]!=0 && i<LABEL_LENGTH){
		this->label[i]=s[i];
		i++;
	}
	this->label[i]=0;
}

void Component_add(struct Component* this, struct Component* comp){
	struct Component* current=this->first_child;
	if(this->first_child==NULL){
		this->first_child=comp;
		comp->next_sibling=NULL;
	}
	else
	{
		while(current->next_sibling!=NULL){
			current=current->next_sibling;
		}
		current->next_sibling=comp;
		comp->next_sibling=NULL;
	}
	comp->parent=this;
}

void Component_paint(struct Component* this){
	struct Component* current=this->first_child;
	EnableScreen(0);
	while(current!=NULL){
		current->paint(current);
		current=current->next_sibling;
	}
	EnableScreen(1);
}

void Component_findXY(struct Component* this, int* x, int* y){
    struct Component* current=this;
    do{
        *x+=current->x;
        *y+=current->y;
        current=current->parent;
    } while(current!=NULL);
}

struct Component* Component_find(struct Component* this, int msg){
	struct Component* current=this->first_child;
#ifdef JOOST_DEBUG
	printf("Find %x in %x:\n",msg, this->id); fflush(stdout);
#endif
	while(current!=NULL){
#ifdef JOOST_DEBUG
		printf("Comp_find: %x, %x\n",msg, current->msg); fflush(stdout);
#endif
		if(current->msg==msg)
			return current;
		current=current->next_sibling;
	}
	return NULL;
}

struct Component* Component_delete(struct Component* this){
	struct Component* current=this->first_child;
	int id=this->id;
	if(this!=NULL){
#ifdef JOOST_DEBUG
		printf("Deleting component: %d\n",id); fflush(stdout);
#endif
		while(current!=NULL){
			current=Component_delete(current);
		}
		if(this->parent!=NULL){
			if(this==this->parent->first_child)
				this->parent->first_child=this->parent->first_child->next_sibling;
			else{
				current=this->parent->first_child;
				while(this!=current->next_sibling && current->next_sibling!=NULL)
					current=current->next_sibling;
				if(current->next_sibling!=NULL)
					current->next_sibling=current->next_sibling->next_sibling;
			}
		}	
		current=this->next_sibling;
		if(this->sizexds==sizeof(struct StringTableXDS))
		{
			struct Component* par=this->parent;
			struct StringTableXDS* stx=(struct StringTableXDS*) this->xds;
			
#ifdef STR_DEBUG
			printf("Stringtable @ %x freed!\n",stx->stringtabledata); fflush(stdout);
#endif

#ifdef MALLOC_DEBUG
	printf("free: %x\n",stx->stringtabledata); fflush(stdout);
#endif
			free(stx->stringtabledata);
#ifdef MALLOC_DEBUG
	printf("free: %x\n",stx->stringtable); fflush(stdout);
#endif
			free(stx->stringtable);

			while((par!=NULL) && (par->sizexds!=sizeof(struct StringTableXDS)))
				par=par->parent;
			if(par!=NULL)
			{
				stx=(struct StringTableXDS*) par->xds;
				global->stringtabledata=stx->stringtabledata;
				global->stringtable=stx->stringtable;
			}		
		}
		if(this->sizexds>0){
#ifdef MALLOC_DEBUG
	printf("free: %x\n",this->xds); fflush(stdout);
#endif
			free(this->xds);
		}
		if((this->state & STRING_FROMTABLE)==0  && this->label!=NULL){
#ifdef MALLOC_DEBUG
	printf("free: %x\n",this->label); fflush(stdout);
#endif
			free(this->label);
		}
#ifdef MALLOC_DEBUG
	printf("free: %x\n",this); fflush(stdout);
#endif

		free(this);
#ifdef JOOST_DEBUG
		printf("Deleted component: %d\n",id); fflush(stdout);
#endif
	}
	return current;
}

void Component_delete_children(struct Component* this){
	struct Component* current=this->first_child;
	while(current!=NULL)
		current=Component_delete(current);
}

void Component_changestate_children(struct Component* this, int andmask, int ormask, int xormask){
	struct Component* current=this->first_child;
	while(current!=NULL){
		current->state&=(0xffff-andmask);
		current->state|=ormask;
		current->state^=xormask;
		current=current->next_sibling;
	}
	fflush(stdout);
}

void Component_linkstringtable(struct Component* this){
	struct Component* child=this->first_child;
#ifdef STR_DEBUG
	printf("%d: %x -> %x\n",this->id,this->label-1,global->stringtable[(int) (this->label)-1]); fflush(stdout);
#endif
	if(((int)(this->label))<0x1000 && ((int)(this->label))>0)
		this->label=global->stringtable[(int) (this->label)-1];
	if(((int)(this->label2))<0x1000 && ((int)(this->label2))>0)
		this->label2=global->stringtable[(int) (this->label2)-1];
	while(child!=NULL){
		Component_linkstringtable(child);
		child=child->next_sibling;
	}

}

/************************************************
 * Windows                                      *
 ************************************************/

void Window_handler (struct Component* this, int msg, int param){
	struct Component* current,current2;
	int x=0,y=0;
#ifdef JOOST_DEBUG
	printf("Message in window %d, Msg: %x, Param: %x\n",this->id,msg,param); fflush(stdout);
#endif
	switch(msg){
	case KEY_PRESSED:
		current=this->first_child;
		while(current!=NULL) {
			if((current->state & STATE_VISIBLE)==(STATE_VISIBLE))
				current->handler(current,msg,param);
			current=current->next_sibling;
		}
		break;
	case KEY_RELEASED:
		current=this->first_child;
		while(current!=NULL) {
			if((current->state & STATE_VISIBLE)!=0){
				current->handler(current,msg,param);
				current->handler(current,KEY_TYPED,param);
				if((current->state & (STATE_ACTION | STATE_ENABLED))==(STATE_ACTION | STATE_ENABLED) && current->msg==param)
					current=NULL;
			}			
			if(current!=NULL)
				current=current->next_sibling;
		}
		break;
	case KEY_TYPED:
		current=this;
		if((current->state & STATE_VISIBLE)==(STATE_VISIBLE)){
				
				if((current->state & STATE_ACTION)!=0 && current->msg==param){
					struct ActionXDS* act=(struct ActionXDS*) (current->xds);
#ifdef JOOST_DEBUG
					printf("Before actionhandler...\n");
#endif
					act->actionhandler(current);
					if((act->action & 0xff00)==0){						
						current=NULL;
					}
#ifdef JOOST_DEBUG
					printf("After actionhandler...\n");
#endif
				}
		}
		if(current!=NULL)
			current=current->next_sibling;
		
		break;
	case WND_REDRAW:
		this->paint(this);
		break;
	case WND_CLOSE:
		this->state=this->state & (0xffff-(STATE_VISIBLE | STATE_ENABLED));
		Component_findXY(this,&x,&y);
		TouchRegion(0,x,y,this->sizex,this->sizey);
		this->parent->handler(this->parent,WND_REDRAW,0);
		break;
	case WND_DESTROY:
		current=this->parent;
		Component_delete(this);
		current->handler(current,WND_REDRAW,0);
		break;
	case WND_BROADCAST:
		current=this->first_child;
		while(current!=NULL){
			if(current!=(struct Component*) (param) && (current->state & STATE_FOCUS)!=0)
				current->handler(current,CMP_BROADCAST,param);
			current=current->next_sibling;
		}
		break;
	case APP_QUIT:
		if(this->parent==NULL)
			global->willquit=1;
		else if((this->state & STATE_APPLICATION)==0)
			this->parent->handler(this->parent,APP_QUIT,param);
		else{
			current=this->parent;
			current->state|=STATE_VISIBLE | STATE_ENABLED;
			Component_changestate_children(current,0,STATE_VISIBLE,0);
			this->handler(this,WND_DESTROY,0);
		}
		break;			
	}
}

void Window_paint(struct Component* this){
	int x=0,y=0;
	struct Component* current;
	if(global->currentfont->id!=this->font)
		setCurrentFont(this->font);
	if((this->state & STATE_VISIBLE)!=0){
		Component_findXY(this,&x,&y);
		
		EnableScreen(0);
		Move(x,y);
		ClearRect(this->sizex,this->sizey);
		TouchRegion(0,x,y,this->sizex,this->sizey);
		Move(x,y+global->currentfont->stringheight(this->label)+4);
		DrawRectNC(this->sizex,this->sizey-global->currentfont->stringheight(this->label)-4);
		Move(x,y);
		FillRect(this->sizex,global->currentfont->stringheight(this->label)+5);
		Move(x+2,y+2);
		Text(this->label);
		Component_paint(this);
	}
}

void Root_paint(struct Component* this){
	EnableScreen(0);
	Move(0,0);
	ClearRect(0,0x80);
	GrayRect(0,0x80);
	TouchRegion(0,0,0,0,0x80);

	Component_paint(this);	
/*	EnableScreen(0);
	Window_paint(this);
	EnableScreen(1);*/
}

void Window_add(struct Component* this, struct Component* comp){
	Component_add(this,comp);
}

struct Component* Window_new(int x,int y,int sx,int sy){
	struct Component* this=Component_new(x,y,sx,sy);
	this->handler=Window_handler;
	this->paint=Window_paint;
	this->add=Window_add;
	return this;
}

/************************************************
 * Buttons                                      *
 ************************************************/

void Button_handler(struct Component* this, int msg, int param){
	Window_handler(this,msg,param);
	if(msg==KEY_TYPED && this->msg==param && (this->state & (STATE_VISIBLE | STATE_ENABLED))!=0){
		this->parent->handler(this->parent, CMP_ACTION, param);
		if((this->state & STATE_CASTING)!=0)
			this->parent->handler(this->parent, WND_BROADCAST, this->msg);
	}
	if(msg==CMP_ACTION)
		this->parent->handler(this->parent, CMP_ACTION, param);
}

void Button_drawtext(struct Component* this, int x, int y){
#ifdef TEXT_TOP_ALIGNED
	if((this->state & STYLE_MULTILINE)!=0){		
		Move(x+(this->sizex-global->currentfont->stringwidth(this->label))/2,y+(this->sizey-2*global->currentfont->stringheight(this->label))/2);
		Text(this->label);
		Move(x+(this->sizex-global->currentfont->stringwidth(this->label2))/2,y+(this->sizey)/2+1);
		Text(this->label2);
	} else {
		Move(x+(this->sizex-global->currentfont->stringwidth(this->label))/2,y+(this->sizey-global->currentfont->stringheight(this->label))/2+1);
		Text(this->label);
	}		
#endif
#ifdef TEXT_BOTTOM_ALIGNED
	if((this->state & STYLE_MULTILINE)!=0){		
		Move(x+(this->sizex-global->currentfont->stringwidth(this->label))/2,y+(this->sizey)/2);
		Text(this->label);
		Move(x+(this->sizex-global->currentfont->stringwidth(this->label2))/2,y+(this->sizey+2*global->currentfont->stringheight(this->label))/2+1);
		Text(this->label2);
	} else {
		Move(x+(this->sizex-global->currentfont->stringwidth(this->label))/2,y+(this->sizey+global->currentfont->stringheight(this->label))/2+1);
		Text(this->label);
	}		

#endif

#ifdef STR_DEBUG
	printf("Drawtext (%d): ""%s"" @ %x\n",this->id,this->label,this->label); fflush(stdout);
#endif
}

void Button_paint(struct Component* this){
	int x=0, y=0;
	struct Component* current;
	if(global->currentfont->id!=this->font)
		setCurrentFont(this->font);
	if((this->state & STATE_VISIBLE)!=0){
		Component_findXY(this,&x,&y);
		Move(x,y);
		ClearRect(this->sizex,this->sizey);
		TouchRegion((this->state & STATE_ENABLED)?this->msg:0,x,y,this->sizex,this->sizey);
		
		if( (this->state & (STATE_ENABLED | STYLE_INVERTED))!=0 && (this->state & (STATE_ENABLED | STYLE_INVERTED))!=(STATE_ENABLED | STYLE_INVERTED) )
			FillRectNC(this->sizex,this->sizey);
		else
			DrawRectNC(this->sizex,this->sizey);
		Button_drawtext(this,x,y);
	}
}

void Button_add(struct Component* this, struct Component* comp){
	return;
}

struct Component* Button_new(int x, int y, int sx, int sy, int msg){
	struct Component* this=Component_new(x,y,sx,sy);
	this->state|=STATE_ENABLED | STATE_VISIBLE;
	this->msg=msg;
	this->handler=Button_handler;
	this->paint=Button_paint;
	this->add=Button_add;
	
	return this;
}

/*void Numpad(struct Component* this, int x, int y){
	int i;
	char s[2]={0,0};
	struct Component* button;
	for(i=0; i<9; i++){
		s[0]=0x31+(char) i;
		button=Button_new(x+(i%3)*0x18,y+0x30-(i/3)*0x18,0x16,0x16,i+0x31);
		Component_setlabel(button,s);
		button->state|=STATE_CASTING;
		this->add(this,button);
	}
	button=Button_new(x+0x18,y+0x48,0x16,0x16,0x30);
	Component_setlabel(button,"0");
	button->state|=STATE_CASTING;
	this->add(this,button);

	button=Button_new(x+0x30,y+0x48,0x16,0x16,0x08);
	Component_setlabel(button,"CE");
	button->state|=STATE_CASTING;
	this->add(this,button);
}*/

/************************************************
 * Toggle-buttons                               *
 ************************************************/

void ToggleButton_handler(struct Component* this, int msg, int param){
	Window_handler(this,msg,param);
	if(msg==KEY_TYPED && this->msg==param && (this->state & (STATE_VISIBLE | STATE_ENABLED))!=0){
		this->state^=STATE_CHECKED;
		this->parent->handler(this->parent, CMP_STATECHANGE, param);
		this->handler(this, WND_REDRAW, 0);
		if((this->state & STATE_CASTING)!=0)
			this->parent->handler(this->parent, WND_BROADCAST, this->msg);
	}
	if(msg==CMP_ACTION)
		this->parent->handler(this->parent, CMP_ACTION, param);
}

void ToggleButton_paint(struct Component* this){
	int x=0, y=0;
	struct Component* current;
	if(global->currentfont->id!=this->font)
		setCurrentFont(this->font);
	if((this->state & STATE_VISIBLE)!=0){
		Component_findXY(this,&x,&y);
		Move(x,y);
		ClearRect(this->sizex,this->sizey);
		if((this->state & STATE_ENABLED)!=0 && (this->state & STATE_CHECKED)!=0){
			FillRectNC(this->sizex,this->sizey);
			TouchRegion(this->msg,x,y,this->sizex,this->sizey);
		}else{
			DrawRectNC(this->sizex,this->sizey);
			TouchRegion((this->state & STATE_ENABLED)?this->msg:0,x,y,this->sizex,this->sizey);
		}
		Button_drawtext(this,x,y);
	}
}

struct Component* ToggleButton_new(int x, int y, int sx, int sy, int msg){
	struct Component* this=Component_new(x,y,sx,sy);
	this->state|=STATE_ENABLED | STATE_VISIBLE | STATE_CHECKED;
	this->msg=msg;
	this->handler=ToggleButton_handler;
	this->paint=ToggleButton_paint;
	this->add=Button_add;
	
	return this;
}

/************************************************
 * Dialogs                                      *
 ************************************************/

void Dialog_handler(struct Component* this, int msg, int param){
	Window_handler(this,msg,param);
	if(msg==CMP_ACTION && param==0xff){
		if((this->state & STYLE_SELFDESTRUCT)!=0)
			this->handler(this,WND_DESTROY,0);
		else
			this->handler(this,WND_CLOSE,0);
		this->parent->handler(this->parent, DLG_OK, this->msg);
	}
	if(msg==CMP_ACTION && param==0xfe){
		if((this->state & STYLE_SELFDESTRUCT)!=0)
			this->handler(this,WND_DESTROY,0);
		else
			this->handler(this,WND_CLOSE,0);
		this->parent->handler(this->parent, DLG_CANCEL, this->msg);

	}
}

struct Component* Dialog_new(int x, int y, int sx, int sy, int msg){
	struct Component* ok;
	struct Component* this=Component_new(x,y,sx,sy);
	this->state|=STATE_VISIBLE;
	this->msg=msg;
	this->handler=Dialog_handler;
	this->paint=Window_paint;
	this->add=Window_add;

	ok=Button_new(sx-0x30,sy-0x1c,0x28,0x14,0xff);
	ok->label=(char*) malloc(3);
	Component_setlabel(ok,"OK");
	this->add(this,ok);

	ok=Button_new(sx-0x30,sy-0x38,0x28,0x14,0xfe);  /*Cancel*/
	ok->label=(char*) malloc(7);
	Component_setlabel(ok,"Cancel");
	this->add(this,ok);

	return this;
}

/************************************************
 * Displays                                     *
 ************************************************/
void Display_handler(struct Component* this, int msg, int param){
	Button_handler(this,msg,param);
	if(msg==CMP_BROADCAST && (this->state & STATE_FOCUS)!=0){
#ifdef JOOST_DEBUG
		printf("strlen = %d->",strlen(this->label)); fflush(stdout);
#endif
		if(param==0x08){
			if(strlen(this->label)!=0)
				this->label[strlen(this->label)-1]=0;
		}else{
			this->label[strlen(this->label)+1]=0;
			this->label[strlen(this->label)]=(char) param;
		}	
#ifdef JOOST_DEBUG
		printf("%d\n",strlen(this->label)); fflush(stdout);
#endif
		this->handler(this,WND_REDRAW,0);
	}
}

void Display_paint(struct Component* this){
	int x=0, y=0;
	struct Component* current;
	char pwd[64];
	if(global->currentfont->id!=this->font)
		setCurrentFont(this->font);
	if((this->state & STATE_VISIBLE)!=0){
		Component_findXY(this,&x,&y);
		Move(x,y);
		ClearRect(this->sizex,this->sizey);
		TouchRegion((this->state & STATE_ENABLED)?this->msg:0,x,y,this->sizex,this->sizey);
		
		if( (this->state & STYLE_INVERTED)==0)
			FillRectNC(this->sizex,this->sizey);
		else
			DrawRectNC(this->sizex,this->sizey);
#ifdef TEXT_TOP_ALIGNED
		Move(x+2,y+(this->sizey-global->currentfont->stringheight(this->label))/2);
#endif
#ifdef TEXT_BOTTOM_ALIGNED
		Move(x+2,y+(this->sizey+global->currentfont->stringheight(this->label))/2);
#endif
		if(this->state & STYLE_PASSWORD){
			int i;
			for(i=0; i<strlen(this->label); i++)
				pwd[i]='*';
			pwd[i]=0;
			Text(pwd);
		}else
			Text(this->label);

/*		Component_findXY(this,&x,&y);

		Move(x,y);
		ClearRect(this->sizex,this->sizey);
	
		FillRectNC(this->sizex,this->sizey);
		Move(x+2,y+(this->sizey-global->currentfont->stringheight(this->label))/2);
		Text(this->label);
		TouchRegion((this->state&STATE_ENABLED)?this->msg:0,x,y,this->sizex,this->sizey);
*/
	}
}

struct Component* Display_new(int x, int y, int sx, int sy, int msg){
	struct Component* this=Button_new(x,y,sx,sy,msg);
	this->state|=STATE_FOCUS;
	Component_setlabel(this,"");
	this->paint=Display_paint;
	this->handler=Display_handler;
	return this;
}

/************************************************
 * Labels                                       *
 ************************************************/
void Label_handler(struct Component* this, int msg, int param){
	return;
}

void Label_paint(struct Component* this){
	int x=0, y=0;
	if(global->currentfont->id!=this->font)
		setCurrentFont(this->font);
	if((this->state & STATE_VISIBLE)!=0){
		Component_findXY(this,&x,&y);
		Button_drawtext(this,x,y);
	}
}

struct Component* Label_new(int x, int y, int sx, int sy,char* l){
	struct Component* comp=Component_new(x,y,sx,sy);
	comp->label=l;
	comp->state=STATE_VISIBLE;
	comp->handler=Label_handler;
	comp->paint=Label_paint;
	comp->add=Button_add;
	return comp;
}
 /************************************************
 * Implementation                               *
 ************************************************/
/*
void ScreenDialog_handler(struct Component* this, int msg, int param){
	char cmd[]={0x1B,'V',0};
	Dialog_handler(this,msg,param);
	if(msg==CMP_ACTION){
		if(param=='U')
			cmd[2]=0;
		else if(param=='D')
			cmd[2]=1;
		else 
			return;
		write(global->lcd,cmd,3);
	}
}

struct Component* ScreenDialog_new(int x, int y, int sx, int sy, int msg){
	struct Component* button;
	struct Component* this=Dialog_new(x,y,sx,sy,msg);
	
	button=Button_new(0x04,0x10,0x20,0x14,'U');
	Component_setlabel(button,"Up");
	this->add(this,button);

	button=Button_new(0x04,0x28,0x20,0x14,'D');
	Component_setlabel(button,"Down");
	this->add(this,button);

	this->handler=ScreenDialog_handler;

	return this;
}
*/
void rootWindow_handler(struct Component* this, int msg, int param){
	struct Component* dummy;
	Window_handler(this,msg,param);
}


void screenbuilder(struct Component* root, char* filename){
	struct Component* subw;
	struct ActionXDS* action;
	int mode;		
	int fh,rd;
	char buf[SIZE_SCRBLD_RECORD];
	long l;
	int xs,ys;
	xs=6;
	ys=5;
	buf[0]=0;
	fh=open(filename,O_RDONLY);
	do{	
		char comp=0;
		if(buf[0]=='s'){		/* Change size */
			xs=*((int*)(buf+0x1));
			ys=*((int*)(buf+0x3));
		}

		if(buf[0]=='B' || buf[0]=='M'){		/* Button, Multi-line button */
			subw=Button_new(0x103*buf[1]/xs,0x083*buf[2]/ys,0x103*(buf[1]+buf[3])/xs-0x103*buf[1]/xs-3,0x083*buf[4]/ys-3,buf[5]);
			if(buf[0]=='M') subw->state|=STYLE_MULTILINE;
			comp=1;
		}
		if(buf[0]=='T'){					/* Toggle-button */
			subw=ToggleButton_new(0x103*buf[1]/xs,0x083*buf[2]/ys,0x103*(buf[1]+buf[3])/xs-0x103*buf[1]/xs-3,0x083*buf[4]/ys-3,buf[5]);
			comp=1;
		}
		if(buf[0]=='D'){					/* Display button */
			subw=Display_new(0x103*buf[1]/xs,0x083*buf[2]/ys,0x103*(buf[1]+buf[3])/xs-0x103*buf[1]/xs-3,0x083*buf[4]/ys-3,buf[5]);
			comp=1;
		}
		if(buf[0]=='L'){					/* Label */
			subw=Label_new(0x103*buf[1]/xs,0x083*buf[2]/ys,0x103*(buf[1]+buf[3])/xs-0x103*buf[1]/xs-3,0x083*buf[4]/ys-3,buf+0x12);
			comp=1;
		}
		if(buf[0]=='A'){			
			struct ActionXDS a;
			root->xds=&a;
			a.action=*((int*)(buf+0x6));
			a.wParam=*((int*)(buf+0x8));
			a.lParam=*((long*)(buf+0xa));
			root_actionhandler(root);
		}
				
		if(buf[0]=='S'){
			struct StringTableXDS* stx;
			read_stringtable(fh,buf);
			root->xds=(void*) malloc(sizeof(struct StringTableXDS));
#ifdef MALLOC_DEBUG
	printf("malloc: %x\n",root->xds); fflush(stdout);
#endif

			stx=(struct StringTableXDS*) root->xds;
			root->sizexds=sizeof(struct StringTableXDS);
			stx->stringtable=global->stringtable;
			stx->stringtabledata=global->stringtabledata;
#ifdef STR_DEBUG
			printf("Stringtable created @ %x\n",stx->stringtabledata); fflush(stdout);
#endif
			comp=0;
		}
		if(comp!=0){
			if((buf[6] | buf[7])!=0){
				subw->state|=STATE_ACTION;
				subw->sizexds=sizeof(struct ActionXDS);
				subw->xds=(void*) malloc(sizeof(struct ActionXDS));
#ifdef MALLOC_DEBUG
	printf("malloc Action: %x\n",subw->xds); fflush(stdout);
#endif

				action=(struct ActionXDS*) (subw->xds);
				action->actionhandler=root_actionhandler;
				action->action=*((int*)(buf+0x6));
				action->wParam=*((int*)(buf+0x8));
				action->lParam=*((long*)(buf+0xa));
			}
			if((buf[0x0e] | buf[0x0f])!=0) 
				subw->state=*((int*)(buf+0x0e));
			subw->font=*((int*)(buf+0x10));

			subw->label=(char*) (*((int*) (buf+0x12)) & 0x0fff);
			if(subw->label==NULL) {
				subw->label=(char*) malloc(LABEL_LENGTH); /* Stringtable ref<0x8000 alloced data >0x8000*/
#ifdef MALLOC_DEBUG
	printf("malloc: %x\n",subw->label); fflush(stdout);
#endif
				subw->label[0]=0;
			}
			else
				subw->state|=STRING_FROMTABLE;

			if((*((int*) (buf+0x12)) &0xf000)>0x1000)
				subw->label2=(char*)((*((int*) (buf+0x12)) & 0x0fff)+1);

			root->add(root,subw);
		}
		
		rd=read(fh,buf,SIZE_SCRBLD_RECORD);
	} while(rd==SIZE_SCRBLD_RECORD);
	close(fh);
	Component_linkstringtable(root);
	root->paint(root);
}

void root_actionhandler(struct Component* this){
	struct ActionXDS *act;
	struct Component* subw;

	act=(struct ActionXDS*) (this->xds);
#ifdef JOOST_DEBUG
	printf("0x%x: (0x%x, 0x%x, 0x%lx)",act, act->action, act->wParam, act->lParam);
#endif
	if(act->action==0x0001){			/*QUIT*/
		this->handler(this,APP_QUIT,0);
	}
	if((act->action & 0xfffe)==0x0002){		/*Open Screen*/
		char* filename=global->stringtable[(act->wParam & 0xfff)-1];
		Component_changestate_children(this->parent,STATE_VISIBLE,0,0);		
		subw=Window_new(0,0,this->parent->sizex,this->parent->sizey);
		subw->state=STATE_VISIBLE | STATE_ENABLED;
		if((act->action & 1)!=0)
			subw->state|=STATE_APPLICATION;
		subw->handler=rootWindow_handler;
		subw->paint=Root_paint;
		screenbuilder(subw,filename);
		this->parent->add(this->parent,subw);
	}
	if(act->action==0x0004) {				/* Open MessageBox */
		/*Component_changestate_children(this->parent,0,0,0);*/
		struct Component* label;
		int i;
		subw=Dialog_new(0x05,0x05,this->parent->sizex-0x0a,this->parent->sizey-0x0a,1);
		subw->state=STATE_VISIBLE | STATE_ENABLED | STYLE_SELFDESTRUCT;
		subw->font=0x0030;
		for(i=0; i<(act->wParam>>12)&0x0f; i++){
			label=Label_new(0x08,0x14+i*0x0c,(subw->sizex & 0xff)-0x10,0x10,global->stringtable[(act->wParam&0xfff)+i-1]);
			label->font=0x0030;
			subw->add(subw,label);
		}
		Component_setlabel(subw,"Message Box");
		Component_find(subw,0xff)->font=0x0030;
		Component_delete(Component_find(subw,0xfe));
		this->parent->add(this->parent,subw);
		this->parent->handler(this->parent,WND_REDRAW,0);
	}

	if(act->action==0x0005){ /* Send info to lcd0 */
		write(global->lcda[0],&(act->lParam),act->wParam);
	}

	if(act->action==0x0006) { /* Send info to lcd1 */
		write(global->lcda[1],&(act->lParam),act->wParam);
	}

#ifdef JOOST_DEBUG
	printf("Before external handler...\n"); fflush(stdout);
#endif
	if(global->actionhandler!=NULL)
		global->actionhandler(this);
#ifdef JOOST_DEBUG
	printf("After external handler...\n"); fflush(stdout);
#endif
}

void read_stringtable(int fh,char* buf){
	int siz,bsiz;
	int i=0,j=0;
	char **ptrs;
	char *rawdata;

	siz=*((int*) (buf+0x01));
	bsiz=*((int*) (buf+0x03));
#ifdef STR_DEBUG
	printf("%d %d\n",siz,bsiz);
#endif

	ptrs=(char**) malloc(sizeof(char*)*siz);
#ifdef MALLOC_DEBUG
	printf("malloc: %x\n",ptrs); fflush(stdout);
#endif

	rawdata=(char*) malloc(bsiz);
#ifdef MALLOC_DEBUG
	printf("malloc: %x\n",rawdata); fflush(stdout);
#endif

	read(fh,rawdata,bsiz);
	while(i<bsiz && j<siz){
		ptrs[j++]=&(rawdata[i]);
		while(rawdata[i]!=0x00)
			i++;
		rawdata[i++]=0x00;
	}
	if(j<siz)
		ptrs[j]=&(rawdata[i]);
	global->stringtabledata=rawdata;
	global->stringtable=ptrs;
#ifdef STR_DEBUG
	for(i=0; i<siz; i++)
		printf("%d: %x->""%s""\n",i,global->stringtable[i],global->stringtable[i]);
	fflush(stdout);
#endif

}

struct Global_GUI* gui_globals(){
	return global;
}


void GUIinit(void){
	global=(struct Global_GUI*) malloc(sizeof(struct Global_GUI));
#ifdef MALLOC_DEBUG
	printf("malloc: %x\n",global); fflush(stdout);
#endif

	global->id=0;
	global->willquit=0;
	global->currentfont=NULL;
	global->firstfont=NULL;
	global->stringtable=NULL;
	global->stringtabledata=NULL;
	global->actionhandler=NULL;
	GraphicsOpen();
	loadFont(0x0030,stringwidth0,stringheight01,0,NULL);
	loadFont(0x0031,stringwidth1,stringheight01,0,NULL);
	loadFont(0x0032,stringwidth2,stringheight2,0,NULL);
	global->root=Window_new(0x0,0x0,0x00,0x80);
	global->root->state=STATE_VISIBLE;
	global->root->handler=rootWindow_handler;
	global->root->paint=Root_paint;
}

void GUIunit(void){
	Component_delete(global->root);
	ClearScreen();
	GraphicsClose();
}

void GUIgo(char* scr){
	char buf,obuf;
	int ptr=0;
	struct sgttyb temp;

    screenbuilder(global->root,scr);
    global->barcode=malloc(16);
	global->barcode[0]=0;
	global->barcodetty=open("/dev/barcode",O_RDONLY);

	gtty(global->barcodetty,&temp);
	temp.sg_flags=RAW | UNBUFF;
	stty(global->barcodetty,&temp);

#if 0 /* Nick, don't flush because continuity of testscript is ruined */
	while(read(global->lcd,&buf,1)==1);
#endif

	while(!global->willquit){
		if(read(global->lcd,&buf,1)==1){
#if 0 /* Nick, optional code useful for debugging testscript execution */
			printf("0x%02x\n", (int)buf);
#endif
			if(buf!=0)
				global->root->handler(global->root,KEY_PRESSED,buf);		
			else
				global->root->handler(global->root,KEY_RELEASED,obuf);
			obuf=buf;
		}
		if(read(global->barcodetty,&buf,1)==1){
			if(buf>0x10)
				global->barcode[ptr++]=buf;
			else {
				global->barcode[ptr++]=0;
				global->root->handler(global->root,BARCODE_SCAN,(int) (&(global->barcode)));
				ptr=0;
			}
		}
	}
}

/*void main(void){
	GUIinit();
	GUIgo();
	GUIunit();
}*/

