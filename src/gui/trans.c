#include <wnd.h>
#include <trans.h>
#include "..\libc\malloc-l.h"
#undef NULL
#define NULL	0

/************************************************
 * Transactions                                 *
 ************************************************/

struct Global_Trans* trans;

void Transaction_init(){
	trans=(struct Global_Trans*) malloc(sizeof(struct Global_Trans));
	trans->first=(struct Transaction*) malloc(sizeof(struct Transaction));
	trans->first->next=NULL;
	trans->first->barcode[0]=0;
	trans->first->stuks=0;
	trans->first->prijs=0;
}

void Transaction_handler(struct Component* this){
	struct ActionXDS *act;

	act=(struct ActionXDS*) (this->xds);

	if(trans==NULL){
		Transaction_init();
	}

	/*if(act->action==0x1000)		 Transaction module init
	{
		trans->barcode=act->lParam & 0xff;
		trans->stuks=(act->lParam>>8) & 0xff;
		trans->prijs=(act->lParam>>16) & 0xff;
		Component_setlabel(Component_find(this,trans->barcode),"Barcode");
		Component_setlabel(Component_find(this,trans->stuks),"Stuks");
		Component_setlabel(Component_find(this,trans->prijs),"Prijs");
	}*/

	if(act->action>=0x1010 && act->action<=0x1012){
		struct Component* display;
		struct Transaction* tr=trans->first;
		while(tr->next!=NULL)
			tr=tr->next;
		display=Component_find(this->parent,act->wParam);
		Component_setlabel(this, display->label);
		Component_setlabel(display,"");
		display->paint(display);
		switch(act->action){
		case 0x1010:
			strcpy(tr->barcode,this->label);
			break;
		case 0x1011:			
			tr->stuks=atoi(this->label);
			if(tr->stuks==0) 
				tr->stuks=1;
			break;
		case 0x1012:
			tr->prijs=atol(this->label);
			break;
		}
		this->paint(this);
	}

	if(act->action==0x101e){		/* Print receipt */
		int prn,i;
		struct sgttyb temp;
		struct Transaction* tr;
		long totaal=0;
		char s[80];

		tr=trans->first;

		prn=open("/dev/printer",O_WRONLY);
		gtty(prn,&temp);
		temp.sg_flags=RAW | UNBUFF;
		stty(prn,&temp);
		while(tr!=NULL){
			if(tr->stuks!=0){
				if(tr->stuks==1)
					sprintf(s,"%13s Verkoopartikel  \x7c%7ld,%02ld\r\n",tr->barcode,tr->prijs/100,tr->prijs%100);
				else
					sprintf(s,"%13s Verkoopartikel\r\n%4dx @ \x7c %7ld,%02ld          \x7c%7ld,%02ld\r\n",tr->barcode,tr->stuks,tr->prijs/100,tr->prijs%100,(tr->stuks*tr->prijs)/100,(tr->stuks*tr->prijs)%100);
				totaal+=tr->stuks*tr->prijs;
				write(prn,s,strlen(s));
			}
			tr=tr->next;
		}
		sprintf(s,"\r\n%13sTotaal:%10s\x7c%7ld,%02ld\r\n\r\n\r\n","","",totaal/100,totaal%100);
		write(prn,s,strlen(s));
		close(prn);
		tr=trans->first;
		while(tr!=NULL){
			free(tr);
			tr=tr->next;
		}
		trans->first=(struct Transaction*) malloc(sizeof(struct Transaction));
		trans->first->next=NULL;
		trans->first->barcode[0]=0;
		trans->first->stuks=0;
		trans->first->prijs=0;

	}

	if(act->action==0x101f){	/* Register transaction line*/
		struct Transaction* tr=trans->first;
		while(tr->next!=NULL)
			tr=tr->next;
		trans->barcode=act->lParam & 0xff;
		trans->stuks=(act->lParam>>8) & 0xff;
		trans->prijs=(act->lParam>>16) & 0xff;
		Component_setlabel(Component_find(this->parent,trans->barcode),"Barcode");
		Component_setlabel(Component_find(this->parent,trans->stuks),"Stuks");
		Component_setlabel(Component_find(this->parent,trans->prijs),"Prijs");
		this->parent->paint(this->parent);
		tr->next=(struct Transaction*) malloc(sizeof(struct Transaction));
		tr=tr->next;
		tr->next=NULL;
		tr->barcode[0]=0;
		tr->stuks=0;
		tr->prijs=0;
	}

}
 
