#ifndef JOOST_WND_H
#define JOOST_WND_H
#include <stdio.h>
#include <syscalls.h>
#include <fcntl.h>
#include <sys\ioctl.h>
#include <sgtty.h>
#include <string.h>

#define FONT_HEIGHT			global->currentfont->stringheight
#define FONT_WIDTH(x)		global->currentfont->stringwidth(x)
#define LABEL_LENGTH		0x14
#define SIZE_SCRBLD_RECORD	0x14


#define STATE_VISIBLE		0x0001
#define STATE_ENABLED		0x0002
#define STATE_CHECKED		0x0004
#define STATE_FOCUS			0x0008
#define STATE_CASTING		0x0010
#define STATE_ACTION		0x0020
#define STYLE_MULTILINE		0x0040
#define STYLE_INVERTED		0x0080
#define STYLE_PASSWORD		0x0100
#define STYLE_SELFDESTRUCT	0x1000
#define STATE_APPLICATION	0x2000
#define STRING_FROMTABLE	0x4000
#define STATE_DUMMY			0x8000

#define KEY_PRESSED			0x0001
#define KEY_RELEASED		0x0002
#define KEY_TYPED			0x0003
#define WND_OPEN			0x0101
#define WND_CLOSE			0x0102
#define WND_REDRAW			0x0103
#define WND_BROADCAST		0x0104
#define WND_DESTROY			0x0105
#define CMP_ACTION			0x0201
#define CMP_STATECHANGE		0x0202
#define CMP_BROADCAST		0x0203
#define DLG_OK				0x0301
#define DLG_CANCEL			0x0302
#define BARCODE_SCAN		0x0401
#define APP_QUIT			0x0501

struct Global_GUI {
	int id;
	int lcda[2];
	int lcd;
	int willquit;
	char *stringtabledata;
	char **stringtable;
	char *barcode;
	int barcodetty;
	struct Font* currentfont;
	struct Font* firstfont;
	struct Component* root;
	void (*actionhandler) (struct Component*);
};

/************************************************
 * Graphics                                     *
 ************************************************/

void GraphicsOpen();
void GraphicsClose();
void Move(int x, int y);
void ClearRect(int sx, int sy);
void DrawRect(int sx, int sy);
void FillRect(int sx, int sy);
void FillRectNC(int sx, int sy);
void GrayRect(int sx, int sy);
void Text(char *s);
void TouchRegion(int msg, int x, int y, int sx, int sy);
void ClearScreen();
void EnableScreen(unsigned char c);
void grSetFont(int f);

/************************************************
 * Fonts                                        *
 ************************************************/

struct Font {
	int id;
	int (*stringwidth)(char*);
	int (*stringheight)(char*);
	struct Font* nextfont;
	int sizefontdata;
	void* fontdata;
};

struct Font* fontSearch(int f);
void setCurrentFont(int f);
void loadFont(int id,int (*sw)(char*),int (*sh)(char*),int sizedata, void* data);


/************************************************
 * Components                                   *
 ************************************************/

struct Component {
	int id;
	int x,y;
	int sizex,sizey;
	int state;
	int order;
	int font;
	char msg;
	struct Component* parent;
	struct Component *next_sibling;
	struct Component *first_child;
	void (*handler) (struct Component*, int, int);
	void (*paint) (struct Component*);
	void (*add) (struct Component*, struct Component*);
	char* label;
	char* label2;
	int sizexds;
	void *xds;
};

struct ActionXDS {
	void (*actionhandler)(struct Component*);
	int action;
	int wParam;
	long lParam;
};

struct StringTableXDS {
	char *stringtabledata;
	char **stringtable;
};

struct Component* Component_new(int x,int y,int sx,int sy);
void Component_setlabel(struct Component* this, char* s);
void Component_add(struct Component* this, struct Component* comp);
void Component_paint(struct Component* this);
void Component_findXY(struct Component* this, int* x, int* y);
struct Component* Component_find(struct Component* this, int msg);
struct Component* Component_delete(struct Component* this);
void Component_delete_children(struct Component* this);
void Component_changestate_children(struct Component* this, int andmask, int ormask, int xormask);
void Component_linkstringtable(struct Component* this);

/************************************************
 * Windows                                      *
 ************************************************/

void Window_handler (struct Component* this, int msg, int param);
void Window_paint(struct Component* this);
void Root_paint(struct Component* this);
void Window_add(struct Component* this, struct Component* comp);
struct Component* Window_new(int x,int y,int sx,int sy);
	
/************************************************
 * Buttons                                      *
 ************************************************/

void Button_handler(struct Component* this, int msg, int param);
void Button_drawtext(struct Component* this, int x, int y);
void Button_paint(struct Component* this);
void Button_add(struct Component* this, struct Component* comp);
struct Component* Button_new(int x, int y, int sx, int sy, int msg);

/************************************************
 * Toggle-buttons                               *
 ************************************************/

void ToggleButton_handler(struct Component* this, int msg, int param);
void ToggleButton_paint(struct Component* this);
struct Component* ToggleButton_new(int x, int y, int sx, int sy, int msg);

/************************************************
 * Dialogs                                      *
 ************************************************/

void Dialog_handler(struct Component* this, int msg, int param);
struct Component* Dialog_new(int x, int y, int sx, int sy, int msg);

/************************************************
 * Displays                                     *
 ************************************************/
void Display_handler(struct Component* this, int msg, int param);
void Display_paint(struct Component* this);
struct Component* Display_new(int x, int y, int sx, int sy, int msg);

/************************************************
 * Labels                                       *
 ************************************************/
void Label_handler(struct Component* this, int msg, int param);
void Label_paint(struct Component* this);
struct Component* Label_new(int x, int y, int sx, int sy,char* l);

/************************************************
 * Implementation                               *
 ************************************************/
void rootWindow_handler(struct Component* this, int msg, int param);
void root_actionhandler(struct Component*);
void screenbuilder(struct Component* root, char* filename);
void read_stringtable(int fh,char* buf);
struct Global_GUI* gui_globals();
void GUIinit();
void GUIgo(char* scr);
void GUIunit();

#endif