/* login.c
 * descripton:	 login into a user account
 * author:	 Alistair Riddoch
 * modification: David Murn <scuffer@hups.apana.org.au>
 *		 Added password entry
 * modification: Shane Kerr <kerr@wizard.net>
 *		 More work on password entry
 */

/* todo:  add a timeout for serial and network logins */
/* ???	  need a signal mechanism (i.e. alarm() and SIGALRM) */
/* 	  real getpass() ! */

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <pwd.h>
#include <wnd.h>

void login(pwd, argv)
	struct passwd *pwd;
	char **argv;
{
	char pname[64];
	char *bname;

#if 1	/* addition by Nick, please see linux manpage passwd.5 */
	if (!*pwd->pw_shell)
		{
		pwd->pw_shell = "/bin/sh";
		}
#endif

/*	chown("/dev/tty",pwd->pw_uid,pwd->pw_gid); */
	pname[0] = '-';
	if ((bname = strrchr(pwd->pw_shell, '/')) != NULL)
		bname++;
	else	bname = pwd->pw_shell;
	strcpy(pname+1, bname);
	argv[0] = pname;
	argv[1] = NULL;
	/* we must set first GID, because we're still root
	   if UID is set first and we aren't logging as root,
	   we won't have permission to change our GID */
	setgid(pwd->pw_gid);
	setuid(pwd->pw_uid);
	chdir(pwd->pw_dir);
	setenv("HOME", pwd->pw_dir, 1);
	setenv("USER", pwd->pw_name, 1);
	execv(pwd->pw_shell, argv);
	perror(pwd->pw_shell);
	exit(1);
}

void login_actionhandler(struct Component* this){
	char lbuf[20], *pbuf, salt[3], *s;
	struct passwd *pwd;
	int n;
	struct ActionXDS *act;
	struct Component* disp=this;

	act=(struct ActionXDS*) (this->xds);
	if(act->action==0x0101){
		lbuf[0]=disp->label[0];
		lbuf[1]=disp->label[1];
		lbuf[2]=0;
		disp->label[0]=0;
		pwd = getpwuid(atoi(lbuf));
		pbuf=disp->label+2;
		if (pwd != NULL) {
			salt[0] = pwd->pw_passwd[0];
			salt[1] = pwd->pw_passwd[1];
			salt[2] = 0;
			s = crypt(pbuf, salt);
			if (!strcmp(s, pwd->pw_passwd)){
				ClearScreen();
				login(pwd, NULL);
			}
		}
		disp->paint(disp);
	}
}

void main(int argc, char** argv)
{
	GUIinit();
	gui_globals()->actionhandler=login_actionhandler;
	GUIgo("/wnd/logn.scr");
	GUIunit();
}


