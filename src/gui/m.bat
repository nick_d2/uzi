iccz80 -S -w -mb -v1 -z9 -A -I..\..\include\ wnd
@if errorlevel 1 goto failure
del wnd.r01
as-z80 -l -o wnd.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z9 -A -I..\..\include\ login
@if errorlevel 1 goto failure
del login.r01
as-z80 -l -o login.s01
@if errorlevel 1 goto failure

link-z80 -f wnd
@if errorlevel 1 goto failure
ihex2bin -l grlogin.i86 ..\..\bin\banked\grlogin
@if errorlevel 1 goto failure

cd ..\..\bin
call appinst.bat
cd ..\src\gui

@echo SUCCESS
@goto done
:failure
@echo FAILURE
:done

