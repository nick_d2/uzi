iccz80 -S -w -mb -v1 -z9 -A -I..\..\include\ wnd
@if errorlevel 1 goto failure
del wnd.r01
as-z80 -l -o wnd.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z9 -A -I..\..\include\ trans
@if errorlevel 1 goto failure
del trans.r01
as-z80 -l -o trans.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z9 -A -I..\..\include\ test
@if errorlevel 1 goto failure
del test.r01
as-z80 -l -o test.s01
@if errorlevel 1 goto failure

link-z80 -f test
@if errorlevel 1 goto failure
ihex2bin -l test.i86 ..\..\bin\banked\test
@if errorlevel 1 goto failure

cd ..\..\bin
call appinst.bat
cd ..\src\gui

@echo SUCCESS
@goto done
:failure
@echo FAILURE
:done

