#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#if 1 /* Nick */
#include <paths.h>
#endif

main(argc,argv)
int argc;
char *argv[];
{
    int lpf;
    int file;
    int cnt;
    char *buf;

    buf = (char *)sbrk(BUFSIZ); /* Nick added cast */

    if (argc != 2)
    {
	fprintf(stderr,"usage: lpr file\n");
	exit(1);
    }
    file = open(argv[1],0);
    if (file < 0)
    {
	perror(argv[1]);
	exit(1);
    }
#if 1 /* Nick */
    lpf = open(_PATH_PRINTER,1);
#else
    lpf = open("/dev/lpr",1);
#endif
    if (lpf < 0)
    {
#if 1 /* Nick */
	perror("lpr: " _PATH_PRINTER);
#else
	perror("lpr: /dev/lpr");
#endif
      	exit(1);
    }

    while ((cnt = read(file,buf,BUFSIZ)) > 0)
	write(lpf,buf,cnt);

    if (cnt == -1)
    {
	perror("lpr: read failed");
        exit(1);
    }

    exit(0);
}
