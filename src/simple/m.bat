cd build-l
@if .%1==. goto failure
call mklink-l %1 ..\

iccz80 -S -w -ml -v1 -z -A -I..\..\..\include\ -I..\..\kernel\uzi\ -DPC_UZIX_TARGET -DDEBUG=0 ..\%1
@if errorlevel 1 goto failure
del %1.r01
as-z80 -l -o %1.s01
@if errorlevel 1 goto failure

link-z80 -f %1
@if errorlevel 1 goto failure
ihex2bin -l %1.i86 ..\..\..\bin\large\%1
@if errorlevel 1 goto failure

cd ..\build-b
call mklink-b %1 ..\

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ -I..\..\kernel\uzi\ -DPC_UZIX_TARGET -DDEBUG=0 ..\%1
@if errorlevel 1 goto failure
del %1.r01
as-z80 -l -o %1.s01
@if errorlevel 1 goto failure

link-z80 -f %1
@if errorlevel 1 goto failure
ihex2bin -l %1.i86 ..\..\..\bin\banked\%1
@if errorlevel 1 goto failure

@echo SUCCESS
@goto done
:failure
@echo FAILURE
:done

cd ..

