# auto.mk by Nick for Hytech NOS/UZI project

# -----------------------------------------------------------------------------

SUBDIRS=

OUTPUTS=	adduser$(EXEEXT) banner$(EXEEXT) basename$(EXEEXT) \
		cal$(EXEEXT) cdiff$(EXEEXT) \
		cksum$(EXEEXT) cmp$(EXEEXT) cr$(EXEEXT) crc$(EXEEXT) \
		cron$(EXEEXT) date$(EXEEXT) dd$(EXEEXT) df$(EXEEXT) \
		dhry$(EXEEXT) dirname$(EXEEXT) \
		diskusag$(EXEEXT) du$(EXEEXT) \
		false$(EXEEXT) fgrep$(EXEEXT) \
		file$(EXEEXT) fld$(EXEEXT) fortune$(EXEEXT) \
		head$(EXEEXT) id$(EXEEXT) \
		inodes$(EXEEXT) kill$(EXEEXT) lpd$(EXEEXT) lpr$(EXEEXT) \
		ncheck$(EXEEXT) od$(EXEEXT) passwd$(EXEEXT) pathchk$(EXEEXT) \
		pr$(EXEEXT) printenv$(EXEEXT) ps$(EXEEXT) pwd$(EXEEXT) \
		readall$(EXEEXT) reboot$(EXEEXT) renice$(EXEEXT) \
		setclock$(EXEEXT) split$(EXEEXT) \
		su$(EXEEXT) sum$(EXEEXT) sync$(EXEEXT) tail$(EXEEXT) \
		tee$(EXEEXT) ter$(EXEEXT) termcap$(EXEEXT) \
		test$(EXEEXT) tget$(EXEEXT) time$(EXEEXT) top$(EXEEXT) \
		tr$(EXEEXT) true$(EXEEXT) uname$(EXEEXT) uniq$(EXEEXT) \
		uuencode$(EXEEXT) wc$(EXEEXT) \
		which$(EXEEXT) whoami$(EXEEXT) yes$(EXEEXT)
#bogomips$(EXEEXT)
# the following would need to be modulized to compile with banked model:
#cgrep$(EXEEXT)
#diff$(EXEEXT)
#dosread$(EXEEXT)
#dtree$(EXEEXT)
#ed$(EXEEXT)
#expr$(EXEEXT)
#find$(EXEEXT)
#grep$(EXEEXT)
#gres$(EXEEXT)
#roff$(EXEEXT)
#sort$(EXEEXT)
#tar$(EXEEXT)
#uudecode$(EXEEXT)

adduser$(call CANON, $(EXEEXT))_SOURCES= \
		adduser.c

banner$(call CANON, $(EXEEXT))_SOURCES= \
		banner.c

basename$(call CANON, $(EXEEXT))_SOURCES= \
		basename.c

#bogomips$(call CANON, $(EXEEXT))_SOURCES= \
#		bogomips.c

cal$(call CANON, $(EXEEXT))_SOURCES= \
		cal.c

cdiff$(call CANON, $(EXEEXT))_SOURCES= \
		cdiff.c

cgrep$(call CANON, $(EXEEXT))_SOURCES= \
		cgrep.c

cksum$(call CANON, $(EXEEXT))_SOURCES= \
		cksum.c

cmp$(call CANON, $(EXEEXT))_SOURCES= \
		cmp.c

cr$(call CANON, $(EXEEXT))_SOURCES= \
		cr.c

crc$(call CANON, $(EXEEXT))_SOURCES= \
		crc.c

cron$(call CANON, $(EXEEXT))_SOURCES= \
		cron.c

date$(call CANON, $(EXEEXT))_SOURCES= \
		date.c

dd$(call CANON, $(EXEEXT))_SOURCES= \
		dd.c

df$(call CANON, $(EXEEXT))_SOURCES= \
		df.c

dhry$(call CANON, $(EXEEXT))_SOURCES= \
		dhry.c

diff$(call CANON, $(EXEEXT))_SOURCES= \
		diff.c

dirname$(call CANON, $(EXEEXT))_SOURCES= \
		dirname.c

diskusag$(call CANON, $(EXEEXT))_SOURCES= \
		diskusag.c

dosread$(call CANON, $(EXEEXT))_SOURCES= \
		dosread.c

dtree$(call CANON, $(EXEEXT))_SOURCES= \
		dtree.c

du$(call CANON, $(EXEEXT))_SOURCES= \
		du.c

ed$(call CANON, $(EXEEXT))_SOURCES= \
		ed.c

expr$(call CANON, $(EXEEXT))_SOURCES= \
		expr.c

false$(call CANON, $(EXEEXT))_SOURCES= \
		false.c

fgrep$(call CANON, $(EXEEXT))_SOURCES= \
		fgrep.c

file$(call CANON, $(EXEEXT))_SOURCES= \
		file.c

find$(call CANON, $(EXEEXT))_SOURCES= \
		find.c

fld$(call CANON, $(EXEEXT))_SOURCES= \
		fld.c

fortune$(call CANON, $(EXEEXT))_SOURCES= \
		fortune.c

grep$(call CANON, $(EXEEXT))_SOURCES= \
		grep.c

gres$(call CANON, $(EXEEXT))_SOURCES= \
		gres.c

head$(call CANON, $(EXEEXT))_SOURCES= \
		head.c

id$(call CANON, $(EXEEXT))_SOURCES= \
		id.c

inodes$(call CANON, $(EXEEXT))_SOURCES= \
		inodes.c

kill$(call CANON, $(EXEEXT))_SOURCES= \
		kill.c

lpd$(call CANON, $(EXEEXT))_SOURCES= \
		lpd.c

lpr$(call CANON, $(EXEEXT))_SOURCES= \
		lpr.c

ncheck$(call CANON, $(EXEEXT))_SOURCES= \
		ncheck.c

od$(call CANON, $(EXEEXT))_SOURCES= \
		od.c

passwd$(call CANON, $(EXEEXT))_SOURCES= \
		passwd.c

pathchk$(call CANON, $(EXEEXT))_SOURCES= \
		pathchk.c

pr$(call CANON, $(EXEEXT))_SOURCES= \
		pr.c

printenv$(call CANON, $(EXEEXT))_SOURCES= \
		printenv.c

ps$(call CANON, $(EXEEXT))_SOURCES= \
		ps.c

pwd$(call CANON, $(EXEEXT))_SOURCES= \
		pwd.c

readall$(call CANON, $(EXEEXT))_SOURCES= \
		readall.c

reboot$(call CANON, $(EXEEXT))_SOURCES= \
		reboot.c

renice$(call CANON, $(EXEEXT))_SOURCES= \
		renice.c

roff$(call CANON, $(EXEEXT))_SOURCES= \
		roff.c

setclock$(call CANON, $(EXEEXT))_SOURCES= \
		setclock.c

sort$(call CANON, $(EXEEXT))_SOURCES= \
		sort.c

split$(call CANON, $(EXEEXT))_SOURCES= \
		split.c

su$(call CANON, $(EXEEXT))_SOURCES= \
		su.c

sum$(call CANON, $(EXEEXT))_SOURCES= \
		sum.c

sync$(call CANON, $(EXEEXT))_SOURCES= \
		sync.c

tail$(call CANON, $(EXEEXT))_SOURCES= \
		tail.c

tar$(call CANON, $(EXEEXT))_SOURCES= \
		tar.c

tee$(call CANON, $(EXEEXT))_SOURCES= \
		tee.c

ter$(call CANON, $(EXEEXT))_SOURCES= \
		ter.c

termcap$(call CANON, $(EXEEXT))_SOURCES= \
		termcap.c

test$(call CANON, $(EXEEXT))_SOURCES= \
		test.c

tget$(call CANON, $(EXEEXT))_SOURCES= \
		tget.c

time$(call CANON, $(EXEEXT))_SOURCES= \
		time.c

top$(call CANON, $(EXEEXT))_SOURCES= \
		top.c

tr$(call CANON, $(EXEEXT))_SOURCES= \
		tr.c

true$(call CANON, $(EXEEXT))_SOURCES= \
		true.c

uname$(call CANON, $(EXEEXT))_SOURCES= \
		uname.c

uniq$(call CANON, $(EXEEXT))_SOURCES= \
		uniq.c

uudecode$(call CANON, $(EXEEXT))_SOURCES= \
		uudecode.c

uuencode$(call CANON, $(EXEEXT))_SOURCES= \
		uuencode.c

wc$(call CANON, $(EXEEXT))_SOURCES= \
		wc.c

which$(call CANON, $(EXEEXT))_SOURCES= \
		which.c

whoami$(call CANON, $(EXEEXT))_SOURCES= \
		whoami.c

yes$(call CANON, $(EXEEXT))_SOURCES= \
		yes.c

# -----------------------------------------------------------------------------

