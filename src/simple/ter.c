/* ter.c for uzi180 by Nick */

#include <stdio.h>
#include <fcntl.h>
#include <sgtty.h>
#include <unistd.h>

/* ------------------------------------------------------------------------- */
/* definitions */

#define BUFFER 0x80

/* ------------------------------------------------------------------------- */
/* prototypes */

void process_esc(char *buf, int *count);
void exit_error(char *mess1, char *mess2);
void exit_restore(void);
void entry_setup(void);
int main(int argc, char **argv);

/* ------------------------------------------------------------------------- */
/* global variables */

int fd_in, fd_out, fd_ter;
int raw_in, raw_ter;

/* ------------------------------------------------------------------------- */

void exit_error(char *mess1, char *mess2)
	{
	exit_restore();
	write(fileno(stderr), mess1, strlen(mess1));
	perror(mess2);
	exit(1);
	}

void exit_restore(void)
	{
	struct sgttyb state;

	if (fd_in >= 0 && isatty(fd_in))
		{
		gtty(fd_in, &state);
		state.sg_flags = raw_in;
		stty(fd_in, &state);
		}

	if (fd_ter >= 0 && isatty(fd_ter))
		{
		gtty(fd_ter, &state);
		state.sg_flags = raw_ter;
		stty(fd_ter, &state);
		close(fd_ter);
		}
	}

void entry_setup(void)
	{
	struct sgttyb state;

	fd_in = fileno(stdin);
	fd_out = fileno(stdout);

	if (isatty(fd_in))
		{
		gtty(fd_in, &state);
		raw_in = state.sg_flags;
		state.sg_flags = RAW | UNBUFF;
		stty(fd_in, &state);
		}

	if (isatty(fd_ter))
		{
		gtty(fd_ter, &state);
		raw_ter = state.sg_flags;
		state.sg_flags = RAW | UNBUFF;
		stty(fd_ter, &state);
		}
	}

void process_esc(char *buf, int *count)
	{
	switch (buf[0])
		{
	case 'x':
	case 'X':
		exit_restore();
		printf("\nter: exiting\n");
		exit(0);

	case 0x1b:
		return; /* with count = 1, so as to write it */

	default:
		*count = 0;
		return;
		}
	}

int main(int argc, char **argv)
	{
	int escflag, count;
	char buf[BUFFER];

	if (argc < 2)
		{
		printf("usage: ter devicename\n");
		exit(1);
		}

	fd_in = -1;
	fd_out = -1;

	fd_ter = open(argv[1], O_RDWR);
	if (fd_ter < 0)
		{
		exit_error("ter: can't open ", argv[1]);
		}

	printf("ter: ready\n");
	fflush(stdout);

	entry_setup();

	escflag = 0;
	while (1)
		{
		count = read(fd_ter, buf, BUFFER);
		if (count < 0)
			{
			exit_error("ter: can't read ", argv[1]);
			}

		if (count)
			{
			if (write(fd_out, buf, count) != count)
				{
				exit_error("ter: can't write ", "stdout");
				}
			}

		count = read(fd_in, buf, 1);
		if (count < 0)
			{
			exit_error("ter: can't read ", "stdin");
			}

		if (count)
			{
			if (escflag)
				{
				escflag = 0;
				process_esc(buf, &count);
				}
			else if (buf[0] == 0x1b)
				{
				escflag = 1;
				count = 0;
				}
			}

		if (count)
			{
#if 0
 printf("[%x]", buf[0]);
 fflush(stdout);
#endif
			if (write(fd_ter, buf, count) != count)
				{
				exit_error("ter: can't write ", argv[1]);
				}
			}
		}
	}

/* ------------------------------------------------------------------------- */

