/* reboot.c for UZI180 */

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int main(void)
	{
	char *s = "\n\aSystem is going to reboot NOW!\a\n";

	write(fileno(stdin), s, strlen(s));
	sync();
#if 1 /* Nick, it was rather silly anyway, but useful for a test - reinstate */
	reboot('m','e');
	perror("reboot");
	return errno;
#else
	sleep(3);
	sync();
	sync();
	if (reboot('m','e'))
		{
		perror("reboot");
		return (errno);
		}
#endif
	}

