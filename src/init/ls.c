/* ls.c for uzi180, ripped from ucpsub.c and modified by Nick */

#include <stdio.h>
#include <sys/types.h> /* Nick types.h */
#include <pwd.h> /* Nick */
#include <grp.h> /* Nick */
#include <time.h> /* Nick */
#include <fcntl.h> /* Nick */
#include <sys/stat.h> /* Nick stat.h */
#include <unistd.h> /* Nick dir.h */

int devdir(int stat);
char *prot(int stat);
char *sizefield(struct stat *statptr);
char *timefield(struct stat *statptr, char *year);
char *inofield(ino_t ino, int inoflag);
void dols(int d, char *path, int wide, char *year, int inoflag);
int xls(char *path, int wide, char *year, int inoflag);
int main(int argc, char **argv);

int devdir(int stat)
	{
	stat &= S_IFMT;

	switch (stat)
		{

	case S_IFDIR:
		return 'd';

	case S_IFPIPE:
		return 'p';

	case S_IFBLK:
		return 'b';

	case S_IFCHR:
		return 'c';

	case S_IFLNK:
		return 'l';

		}

	return '-';
	}

char *prot(int stat)
	{
	static char _prots[8][4] =
		{
		"---", "--x", "-w-", "-wx",
		"r--", "r-x", "rw-", "rwx"
		};

	return _prots[stat & 7];
	}

#if 1 /* Nick */
char *sizefield(struct stat *statptr)
	{
	static char sizebuf[10];

	if (S_ISDEV(statptr->st_mode))
		{
		sprintf(sizebuf, "%4d,%-4d",
			MAJOR(statptr->st_rdev),
			MINOR(statptr->st_rdev));
		}
	else
		{
		sprintf(sizebuf, "%9ld", statptr->st_size);
		}

	return sizebuf;
	}

char *timefield(struct stat *statptr, char *year)
	{
	char *timeptr;

	timeptr = ctime(&statptr->st_mtime);

	if (strcmp(year, timeptr + 19))
		{
		strcpy(timeptr + 11, timeptr + 19); /* replace seconds */
		}
	timeptr[16] = 0; /* truncate after seconds or year */

	return timeptr + 4;
	}

char *inofield(ino_t ino, int inoflag)
	{
	static char inobuf[7];

	if (inoflag)
		{
		sprintf(inobuf, "@%-5d", ino);
		return inobuf;
		}

	return " ";
	}
#endif

void dols(int d, char *path, int wide, char *year, int inoflag)
	{
	int i, j, fd;
	direct_t buf;
	char dname[512], *dpref, *dsuff;
	struct stat statbuf;

	j = 0; /* current column */
	while (read(d, (char *) &buf, sizeof(direct_t)) == sizeof(direct_t))
		{
		if (buf.d_name[0] == '\0')
			continue;
		dname[0] = '\0';
		if (strcmp(path, "."))
			{
			strcpy(dname, path);
			strcat(dname, "/");
			}
		strncat(dname, buf.d_name, DIRNAMELEN);
		fd = open(dname, O_SYMLINK);
		i = (fd >= 0) ? fstat(fd, &statbuf) :
				stat(dname, &statbuf);
		if (i)
			{
			printf("ls: can't stat %s\n", dname);
			if (fd >= 0)
				close(fd);
			continue;
			}
#if 1 /* Nick */
		strncpy(dname, buf.d_name, DIRNAMELEN);
		dname[DIRNAMELEN] = 0; /* in case full length */
#endif
		if ((statbuf.st_mode & S_IFMT) == S_IFDIR)
			{
			dpref = "\x1b[01;34m";
			dsuff = "\x1b[00m/";
			}
		else if ((statbuf.st_mode & S_IFMT) == S_IFLNK)
			{
			dpref = "\x1b[01;36m";
			dsuff = "\x1b[00m@";
			}
		else if ((statbuf.st_mode & S_IEXEC) ||
			    (statbuf.st_mode & S_IGEXEC) ||
			    (statbuf.st_mode & S_IOEXEC))
			{
			dpref = "\x1b[01;32m";
			dsuff = "\x1b[00m*";
			}
		else
			{
			dpref = "";
			dsuff = "";
			}
		if (!wide)
			{
#if 1 /* Nick */
			printf("%c%s%s%s%4d%s%-8s %-8s%s %s %s%s%s",
			       devdir(statbuf.st_mode),
			       prot(statbuf.st_mode >> 6),
			       prot(statbuf.st_mode >> 3),
			       prot(statbuf.st_mode >> 0),
			       statbuf.st_nlink,
			       inofield(statbuf.st_ino, inoflag),
			       getpwuid(statbuf.st_uid)->pw_name,
			       getgrgid(statbuf.st_gid)->gr_name,
			       sizefield(&statbuf),
			       timefield(&statbuf, year),
			       dpref, dname, dsuff);
#else
			if (S_ISDEV(statbuf.st_mode))
				printf("%3d,%-5d",
					MAJOR(statbuf.st_rdev),
					MINOR(statbuf.st_rdev));
			else	printf("%-9ld", statbuf.st_size);
			printf(" %c%s%s%s %2d @%-5u %s",
			       devdir(statbuf.st_mode),
			       prot(statbuf.st_mode >> 6),
			       prot(statbuf.st_mode >> 3),
			       prot(statbuf.st_mode >> 0),
			       statbuf.st_nlink,
			       statbuf.st_ino,
			       dname);
#endif
			if ((statbuf.st_mode & S_IFMT) == S_IFLNK)
				{
				dname[0] = 0;
				i = read(fd, dname, sizeof(dname)-1);
				dname[i] = 0;
				printf(" -> %s", dname);
				close(fd);
				}
			}
		else
			{
			printf("%s%s%s", dpref, dname, dsuff);
#if 1
			i = strlen(dname) + (*dsuff != 0);
#else
			i = strlen(dname) + 1;
			if ((statbuf.st_mode & S_IFMT) == S_IFLNK) putchar('@');
			else if (((statbuf.st_mode & S_IFMT) != S_IFDIR) &&
			    ((statbuf.st_mode & S_IEXEC) +
			    (statbuf.st_mode & S_IGEXEC) +
			    (statbuf.st_mode & S_IOEXEC) != 0)) putchar('*');
			else i--;
#endif
			if (wide >= 2)
				{
				if (j >= 4)
					{
					j = 0;
					putchar('\n');
					}
				else
					{
					j++;
					if (i < 16)
						{
						putchar('\t');
						}
					if (i < 8)
						{
						putchar('\t');
						}
					}
				}
			}
		if (wide < 2)
			{
			putchar('\n');
			}
		}
	if (j)
		{
		putchar('\n');
		}
#if 1 /* Nick */
	fflush(stdout);
#endif
	}

int xls(char *path, int wide, char *year, int inoflag)
	{
	int d;
	struct stat statbuf;

	if (stat(path, &statbuf) != 0)
		{
		printf("ls: can't stat %s\n", path);
		/*pse();*/
		}
	else if ((statbuf.st_mode & S_IFMT) != S_IFDIR)
		{
		printf("ls: %s is not a directory\n", path);
		/*pse();*/
		}
	else if ((d = open(path, 0)) < 0)
		{
		printf("ls: can't open %s\n", path);
		/*pse();*/
		}
	else
		{
		dols(d, path, wide, year, inoflag);
		close(d);
		}
	return 0;
}

int main(int argc, char **argv)
{
	int j, wide;
#if 1 /* Nick */
	char *p;
	time_t now;
	char year[7];
	int inoflag;
#endif

#if 0 /* Nick */
	printf("argc = %d\n", argc);
	for (j = 0; j < argc; j++)
		{
		printf("argv[%d] = \"%s\"\n", j, argv[j]);
		}
	fflush(stdout);
#endif

	j = 1;
	wide = 2;
#if 1 /* Nick */
	inoflag = 0;

	while (j < argc && argv[j][0] == '-')
		{
		p = argv[j++];
		while (*++p)
			{
			switch (*p)
				{
			case 'i':
				inoflag = 1;
				/* fallthru */
			case 'l':
				wide = 0;
				break;
			default:
				printf("usage: ls [-i] [-l] [files...]\n");
				fflush(stdout);
				return 1;
				}
			}
		}
#else
	if (j < argc)
		{
		if (strcmp(argv[j], "-l") == 0)
			{
			wide = 0;
			j++;
			}
		else if (argv[j][0] == '-')
			{
			printf("usage: ls [-l] [-i] [files...]\n");
			exit(1);
			}
		}
#endif

	if (wide && isatty(fileno(stdout)) == 0)
		{
		wide = 1; /* says we should only output a single column */
		}

#if 1 /* Nick */
	if (wide == 0)
		{
		time(&now);
		strcpy(year, ctime(&now) + 19);
		}
#endif

	if (j >= argc)
		{
		xls(".", wide, year, inoflag);
		}
	else
		{
		while (j < argc)
			{
			xls(argv[j++], wide, year, inoflag);
			}
		}

#if 1 /* Nick */
/* printf("exiting\n"); */
/* fflush(stdout); */
	return 0;
#else
	exit(0);
#endif
	}

