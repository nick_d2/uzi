/* init.c
 */

/* the use of 'firsttime' var for erasing /etc/mtab isn't the best solution.
 * if init can't spawn the first tty, but can respawn it or spawn the second,
 * /etc/mtab will not be erased and initialized.
 */

/* #define NO_RTC
 * causes init to ask and set system clock when executed at first time
 */
#define NO_RTC /* modified by Nick to skip prompt when year >= 2003 */

#include <errno.h> /* Nick */
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <paths.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include <sgtty.h> /* Nick */
#include <uzi/filesys.h>
#include <ctype.h> /* Nick */
#include <sys/wait.h>

static firsttime = 1;

struct exec_struct
{
	char *tty_name;
	char *sh_name;
	int pid;
};

#define NUM_TTYS 1

struct exec_struct tty_list[NUM_TTYS] =
{
	{ _PATH_CONSOLE, _PATH_LOGIN }
};

void process_inittab(void)
	{
	FILE *fdm;
	char cmd[128], *p;
	char silent;
	int i, argc;
	char *argv[18];
	int pid;

	printf("Processing /etc/inittab\n");
	fflush(stdout);

	fdm = fopen("/etc/inittab", "r");
        if (fdm == NULL)
		{
		perror("can't open /etc/inittab");
		return;
		}

	while (feof(fdm) == 0)
		{
		if (fgets(cmd, sizeof(cmd)-1, fdm) == NULL)
			{
			break;
			}
/* printf("@@@ %s", cmd); */
		/* check for comment lines */
		p = cmd;
		while (isspace(*p))
			{
			p++;
			}

		if (*p == '#')
			{
			continue;
			}

		/* check for silent lines */
		silent = 0;
		if (*p == '@')
			{
			silent = 1;
			p++;
			}

		/* convert line into arguments */
		argc = 0;
		while (argc < 16)
			{
			while (isspace(*p))
				{
				p++;
				}
			if (*p == 0)
				{
				break;
				}
			argv[1 + argc++] = p;
			while (*p && isspace(*p) == 0)
				{
				p++;
				}
			if (*p == 0)
				{
				break;
				}
			*p = 0;
			}

		/* got a command to execute? */
		if (argc < 1)
			{
			continue;
			}

		/* pacify the user, unless silent */
		if (silent == 0)
			{
			printf("%s", argv[1]);
			for (i = 1; i < argc; i++)
				{
				printf(" %s", argv[1 + i]);
				}
			printf("\n");
			fflush(stdout);
			}

		/* fork and execute the program */
		pid = fork();
		if (pid == -1)
			{
			if (silent == 0)
				{
				perror("can't fork");
				}
			continue;
			}

		if (pid == 0)
			{
			argv[1 + argc] = NULL;
			execve(argv[1], argv + 1, environ);
			if (errno == ESHELL)
				{
				argv[0] = _PATH_BSHELL;
				execve(argv[0], argv, environ);
				}
			if (silent == 0)
				{
				perror("can't execute");
				}
			_exit(1); /* don't close handles in this case!!!!! */
			}

		/* wait for child to complete */
		wait(&pid);
		}

	fclose(fdm);
	}

int determine_tty(int pid)
{
	int i;

	for (i = 0; i < NUM_TTYS; i++) {
		if (pid == tty_list[i].pid)
			return i;
	}
	return -1;
}

void generate_mtab(void)
	{
	filesys_t fsys;
	int j;
	char mtabline[] = "/dev/ramX\t/\trw\n";
	FILE *fdm;

	printf("Generating /etc/mtab\n");
	fflush(stdout);
	for (j = 0; j < 8; ++j)
		{
		if (getfsys(j, &fsys) == 0 &&
				fsys.s_mounted == SMOUNTED &&
				fsys.s_mntpt == NULL)
			{
			j = MINOR(fsys.s_dev);
			if (unlink("/etc/mtab") < 0 && errno != ENOENT)
				{
				perror("can't unlink /etc/mtab");
				break;
				}
			mtabline[8] = j + '0';
			if ((fdm = fopen("/etc/mtab", "w")) == NULL)
				{
				perror("can't create /etc/mtab");
				break;
				}
			if (fputs(mtabline, fdm) == EOF)
				{
				perror("can't write /etc/mtab");
				break;
				}
			fclose(fdm);
			break;
			}
		}
	}

#define PLENGTH 40

void process_fstab(void)
	{
	int r;
	FILE *fdf, *fdm;
	char line[PLENGTH];
	char mf1[PLENGTH], mf2[PLENGTH], mf3[PLENGTH];

	printf("Processing /etc/fstab\n");
	fflush(stdout);

	if ((fdf = fopen("/etc/fstab", "r")) == NULL)
		{
		perror("can't open /etc/fstab");
		return;
		}

	while (feof(fdf) == 0)
		{
		if (fgets(line, PLENGTH, fdf) == NULL)
			{
			break;
			}

		mf1[0] = mf2[0] = mf3[0] = '\0';
		r = sscanf(line, "%s%s%s", mf1, mf2, mf3);
		if (r < 2 || strcmp(mf2, "/") == 0)
			{
			continue;
			}

		if (r > 2)
			{
			r = !strcmp(mf3, "ro");
			}
		else
			{
			r = 0; /* default to read/write */
			}
		r = mount(mf1, mf2, r);
		if (r < 0)
			{
			perror("mount failed");
			continue;
			}

		fdm = fopen("/etc/mtab", "a");
		if (fdm == NULL)
			{
			perror("can't open /etc/mtab");
			continue;
			}

		sprintf(line,"%s\t%s\tr%c\n", mf1, mf2, r ? 'o' : 'w');
		if (fputs(line, fdm) == EOF)
			{
			perror("can't write /etc/mtab");
			continue;
			}

		fclose(fdm);
		}

	fclose(fdf);
	}

int spawn_tty(unsigned num, char **argv)
{
	char *s;
	int pid, fd;
	struct sgttyb state;

	struct exec_struct *p = &tty_list[num];

	if (num >= NUM_TTYS)
		exit(-1);
	if ((pid = fork()) != 0) {	/* error/parent */
		firsttime = 0;
		p->pid = pid;
		return pid;
	}
	/* child - must open own tty */
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);
	if ((fd = open(p->tty_name, O_RDWR)) < 0)
		exit(-2);
	if (isatty(fd))
		{
		gtty(fd, &state);
		state.sg_flags = XTABS | CRMOD | ECHO | COOKED;
		stty(fd, &state);
		}
	dup2(fd, STDOUT_FILENO);
	dup2(fd, STDERR_FILENO);
	if (firsttime) {
		generate_mtab();
		process_fstab();
		process_inittab();
		printf("Spawning tty #%d (%s): ok\n", num, p->tty_name);
	}
	setenv("TTY", p->tty_name, 1);
	setenv("TERM",
#ifdef MSX_UZIX_TARGET
			"vt52"
#endif
#ifdef PC_UZIX_TARGET
			"ansi"
#endif
				, 1);
	for (s = p->sh_name; s[1] != 0; ++s)
		;
	while (s > p->sh_name && s[-1] != '/')
		--s;
	argv[0] = s;
	printf("Starting login...\n\n");
	execve(p->sh_name, argv, environ);
	printf("init: can't execute %s\n", p->sh_name);
	exit(-3);
}

#ifdef NO_RTC
/* get an integer value from *p and return the first char after in p1 */
int getint(char *p, char **p1)
{
	int v = 0;

	while (*p >= '0' && *p <= '9') {
		v *= 10;
		v += *p - '0';
		++p;
	}
	*p1 = p;
	return v;
}

/* convert a given time in t1 and date in t2 into struct tm */
int conv_time(char *t1, char *t2, struct tm *tt)
{
	int h, m, s, d, mm, y;
	char *t;

	/* hh:mm[:ss] */
	t = t1;
	h = getint(t, &t);
	if ((h < 0) || (h > 23) || (*t++ != ':'))	return 0;
	m = getint(t, &t);
	if ((m < 0) || (m > 59))			return 0;
	if (*t) {
		if (*t++ != ':')			return 0;
		s = getint(t, &t);
		if ((s < 0) || (s > 59) || (*t != 0))	return 0;
	}
	else	s = 0;

	/* dd/mm/yy[yy] */
	t = t2;
	d = getint(t, &t);
	if ((d < 1) || (d > 31) || (*t++ != '/'))	return 0;
	mm = getint(t, &t);
	if ((mm < 1) || (mm > 12) || (*t++ != '/'))	return 0;
	y = getint(t, &t);
	if (y < 100) {
		if (y < 80)
			y += 100;
	}
	else	y -= 1900;
	if ((y < 0) || (*t != 0))			return 0;
	tt->tm_hour = h;
	tt->tm_min = m;
	tt->tm_sec = s;
	tt->tm_year = y;
	tt->tm_mon = mm - 1;
	tt->tm_mday = d;
	return 1;
}
#endif

void reopen_console(void)
	{
	int fd;
	struct sgttyb state;

	/* reopen init's tty */
	close(STDIN_FILENO);

	fd = open(_PATH_CONSOLE, O_RDWR);
	if (isatty(fd))
		{
		gtty(fd, &state);
		state.sg_flags = XTABS | CRMOD | ECHO | COOKED;
		stty(fd, &state);
		}

	dup2(fd, STDOUT_FILENO);
	dup2(fd, STDERR_FILENO);
	}

VOID main(int argc, char *argv[])
{
	int i, pid, rv;
#ifdef NO_RTC
	char buf[40], *p;
	struct tm tt;
	time_t mytimet;
	struct tm *tmbuf;
	char *tmptr;
#endif

	signal(SIGINT, SIG_IGN);
/*	signal(SIGKILL, SIG_IGN);	/* can't caught KILL!!! */
	signal(SIGABRT, SIG_IGN);

	reopen_console();

	setenv("PATH", _PATH_DEFPATH, 0);
	setenv("HOME", "/", 0);

#ifdef NO_RTC
	printf("\nTime/date: ");
	fflush(stdout);

	time(&mytimet);
	while (mytimet == 0) /*(mytimet.t_time == 0 && mytimet.t_date == 0)*/
		{
		time(&mytimet);
		}
	tmbuf = localtime(&mytimet);

	tmptr = asctime(tmbuf);
	tmptr[3] = 0; /* terminate day of week */
	tmptr[7] = 0; /* terminate month */
	tmptr[10] = 0; /* terminate day of month */
	tmptr[19] = 0; /* terminate time of day */
	printf("%s %s %s %s %s",
	       tmptr + 11, tmptr, tmptr + 8, tmptr + 4, tmptr + 20);
	fflush(stdout);

	if (tmbuf->tm_year < 104) /* 2004 */
		{
		printf("Enter time/date (hh:mm[:ss] dd/mm/yyyy): ");
		fflush(stdout);

		memset(buf, 0, sizeof(buf));
		read(0, buf, sizeof(buf)-1);

		p = strchr(buf, '\n');
		if (p)
			{
			*p = 0;
			}

		p = strchr(buf, ' ');
		if (p)
			{
			*p = 0;
			}

		if (p == NULL || conv_time(buf, p + 1, &tt) == 0)
			{
			printf("Bad time/date format\n");
			}
		else
			{
			mytimet = mktime(&tt);
			if (stime(&mytimet) < 0)
				{
				printf("Error setting clock\n");
				}
			else
				{
				tmptr = ctime(&mytimet);
				tmptr[3] = 0; /* terminate day of week */
				tmptr[7] = 0; /* terminate month */
				tmptr[10] = 0; /* terminate day of month */
				tmptr[19] = 0; /* terminate time of day */
				printf("New time/date: %s %s %s %s %s",
				       tmptr + 11, tmptr, tmptr + 8, tmptr + 4,
				       tmptr + 20);
				}
			}
		}

	printf("\n");
	fflush(stdout);
#endif

	for (i = 0; i < NUM_TTYS; i++)
		spawn_tty(i, argv);

	reopen_console();
#if DEBUG > 0
	printf("INIT's PID is %d\n",getpid()); fflush(stdout);
#endif
	while (1) {
		pid = wait(&rv);
#if DEBUG > 0
		printf("process #%d returns %d,%d\n",
			pid, (unsigned char)(rv >> 8), (unsigned char)rv);
#endif
		fflush(stdout);
		if ((i = determine_tty(pid)) >= 0) {
			printf("\nRespawning tty #%d (%s): ok\n",
				i, tty_list[i].tty_name);
			fflush(stdout);
			tty_list[i].pid = 0;
			spawn_tty(i, argv);
		}
	}
}

