/* mkdir.c modified by Nick for mkdir/rmdir functionality */

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>

char *prog_name;		/* Call name of this program. */

typedef enum identity { MKDIR, RMDIR } identity_t;

identity_t identity;		/* How did the user call me? */

static unsigned short newmode;

static int make_dir __P((char *name, int f));
static int remove_dir __P((char *name, int f));
static void wr2 __P((char *s));

static int make_dir(name,f)
	char *name;
	int f;
{
	char *line, iname[256];

	strcpy(iname, name);
	line = rindex(iname,'/');
	if ((line != NULL) && (line != iname) && f) {
		while ((line > iname) && (*line == '/'))
			--line;
		line[1] = 0;
		make_dir(iname,1);
	}
	return mkdir(name, newmode) && !f;
}

static int remove_dir(name, f)
	char *name;
	int f;
{
	int er, era = 2;
	char *line;

	while ((er = rmdir(name)) == 0 &&
	       (line = rindex(name, '/')) != NULL &&
	       f) {
		while (line > name && *line == '/')
			*line-- = 0;
		era = 0;
	}
	return (er && era);
}

static void wr2(s)
	char *s;
{
	write(STDERR_FILENO, s, strlen(s));
}

char *basename(const char *path)
/* Return the last component of a pathname.  (Note: declassifies a const
 * char * just like strchr.
 */
{
	const char *p= path;

	for (;;) {
		while (*p == '/') p++;		/* Trailing slashes? */

		if (*p == 0) break;

		path= p;
		while (*p != 0 && *p != '/') p++;	/* Skip component. */
	}
	return (char *) path;
}

int main (argc,argv)
	int argc;
	char **argv;
{
	char *p;
	int i, n, parent = 0, er = 0;

	/* Call name of this program. */
	prog_name= basename(argv[0]);

	/* Required action. */
	if (strcmp(prog_name, "mkdir") == 0) {
		identity= MKDIR;
	} else
	if (strcmp(prog_name, "rmdir") == 0) {
		identity= RMDIR;
	} else {
		fprintf(stderr,
	"%s: Identity crisis, not called mkdir or rmdir\n", prog_name);
		exit(1);
	}

#if 1 /* Nick */
	newmode = 0777 & ~umask(0);
#else
	newmode = 0666 & ~umask(0);
#endif

	argc--;
	argv++;
	while ((argc > 0) && (**argv == '-')) {
		argc--;
		p = *argv++ ;
		while (*++p) switch (*p) {
			case 'm':
				argc--;
				p = *argv++ ;
				if (*p >= '0' && *p <= '7') {
					for (newmode = 0; *p >= '0' && *p <= '7'; ++p) {
						newmode <<= 3;
						newmode += *p - '0';
					}
					if (*p) {
						wr2("Bad octal mode\n");
						return 1;
					}
				}
				p--;	/* *p=0, exit while/switch loop */
				break;
			case 'p':
				++parent;
				break;
			default:
				wr2("Unknown option\n");
				return 0;
		}
	}

	if (argc < 1) {
		switch (identity) {
		case MKDIR:
			wr2("usage: mkdir [-p] [-m mode] dirname ...\n");
			break;
		case RMDIR:
			wr2("usage: rmdir [-p] dirname ...\n");
			break;
		}
		return 1;
	}

	for (i = 0; i < argc; i++) {
		p = argv[i];
		n = strlen(p)-1;
		while (p[n] == '/')
			p[n--] = '\0';
		switch (identity) {
		case MKDIR:
			if (make_dir(p, parent)) {
#if 1
				perror(p);
#else
				wr2("mkdir: cannot create directory ");
				wr2(p);
				wr2("\n");
#endif
				er = 1;
			}
			break;
		case RMDIR:
			if (remove_dir(p, parent)) {
				perror(p);
				er = 1;
			}
			break;
		}
	}
	return er;
}

