# auto.mk by Nick for Hytech NOS/UZI project

# -----------------------------------------------------------------------------

SUBDIRS=

OUTPUTS=	align$(EXEEXT) bd$(EXEEXT) cat$(EXEEXT) chgrp$(EXEEXT) \
		chmod$(EXEEXT) chown$(EXEEXT) cp$(EXEEXT) echo$(EXEEXT) \
		fsck$(EXEEXT) init$(EXEEXT) login$(EXEEXT) ls$(EXEEXT) \
		mkdir$(EXEEXT) mkfs$(EXEEXT) mknod$(EXEEXT) more$(EXEEXT) \
		mount$(EXEEXT) touch$(EXEEXT)

align$(call CANON, $(EXEEXT))_SOURCES= \
		align.c

bd$(call CANON, $(EXEEXT))_SOURCES= \
		bd.c
bd$(call CANON, $(EXEEXT))_DEFINES= \
		NATIVE $(DEFINES)

cat$(call CANON, $(EXEEXT))_SOURCES= \
		cat.c

chgrp$(call CANON, $(EXEEXT))_SOURCES= \
		chgrp.c

chmod$(call CANON, $(EXEEXT))_SOURCES= \
		chmod.c

chown$(call CANON, $(EXEEXT))_SOURCES= \
		chown.c

cp$(call CANON, $(EXEEXT))_SOURCES= \
		cp.c
cp_c_MODULES= \
		cp___global cp_report cp_fatal cp_report2 cp_allocate \
		cp_deallocate cp_path_init cp_path_add cp_path_trunc \
		cp_basename cp_affirmative cp_writable cp_trylink cp_copy \
		cp_copy1 cp_remove1 cp_link1 cp_eat_dir cp_chop_dlist \
		cp_drop_dlist cp_do1 cp_usage cp_main

echo$(call CANON, $(EXEEXT))_SOURCES= \
		echo.c

fsck$(call CANON, $(EXEEXT))_SOURCES= \
		fsck.c
fsck$(call CANON, $(EXEEXT))_DEFINES= \
		NATIVE $(DEFINES)
fsck_c_MODULES= \
		fsck___global fsck_da_read fsck_da_write fsck_pass1 \
		fsck_pass2 fsck_pass3 fsck_ckdir fsck_pass4 fsck_mkentry \
		fsck_pass5 fsck_getblkno fsck_setblkno fsck__blk_alloc \
		fsck_daread fsck_dwrite fsck_iread fsck_iwrite fsck_dirread \
		fsck_dirwrite fsck_yes fsck_main fsck_bitmap_dump \
		fsck_bitmap_find fsck_mypanic

init$(call CANON, $(EXEEXT))_SOURCES= \
		init.c

login$(call CANON, $(EXEEXT))_SOURCES= \
		login.c

ls$(call CANON, $(EXEEXT))_SOURCES= \
		ls.c

mkdir$(call CANON, $(EXEEXT))_SOURCES= \
		mkdir.c

mkfs$(call CANON, $(EXEEXT))_SOURCES= \
		mkfs.c
mkfs$(call CANON, $(EXEEXT))_DEFINES= \
		NATIVE $(DEFINES)

mknod$(call CANON, $(EXEEXT))_SOURCES= \
		mknod.c

more$(call CANON, $(EXEEXT))_SOURCES= \
		more.c

mount$(call CANON, $(EXEEXT))_SOURCES= \
		mount.c

touch$(call CANON, $(EXEEXT))_SOURCES= \
		touch.c

# -----------------------------------------------------------------------------

