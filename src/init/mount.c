/* Copyright (c) 1993 by David I. Bell
 * Permission is granted to use, distribute, or modify this source,
 * provided that this copyright notice remains intact.
 *
 * jun/99: Adriano Cunha <adrcunha@dcc.unicamp.br>
 *	   added /etc/mtab update
 */
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

char *prog_name;		/* Call name of this program. */

typedef enum identity { MOUNT, UMOUNT } identity_t;

identity_t identity;		/* How did the user call me? */

/* Return the last component of a pathname.  (Note: declassifies a const
 * char * just like strchr.
 */
char *basename(const char *path)
	{
	const char *p;

	p = path;
	while (1)
		{
		while (*p == '/')
			{
			p++;		/* Trailing slashes? */
			}

		if (*p == 0)
			{
			break;
			}

		path = p;
		while (*p != 0 && *p != '/')
			{
			p++;	/* Skip component. */
			}
		}

	return (char *) path;
	}

int main(int argc, char **argv)
	{
	int i, fnd;
	FILE *fdi, *fdo;
	char buf[256], *p, c;

	/* Call name of this program. */
	prog_name = basename(argv[0]);

	/* Required action. */
	if (strcmp(prog_name, "mount") == 0)
		{
		identity = MOUNT;
		}
	else if (strcmp(prog_name, "umount") == 0)
		{
		identity = UMOUNT;
		}
	else
		{
		fprintf(stderr, "%s: Identity crisis, not called "
				"mount or umount\n", prog_name);
		return 1;
		}

	if (identity == MOUNT)
		{
		i = 0; /* readonly flag */

		argc--;
		argv++;
		while ((argc > 0) && (**argv == '-'))
			{
			argc--;
			p = *argv++;
			while (*++p)
				{
				switch (*p)
					{
				case 'r':
					++i;
					break;
				default:
					fprintf(stderr, "unknown option %s\n",
							p);
					return;
					}
				}
			}

		if (argc != 2)
			{
			printf("usage: mount [-r] something somewhere\n");
			return 1;
			}

		if (mount(argv[0], argv[1], i) < 0)
			{
			perror("mount failed");
			return 1;
			}

		fdo = fopen("/etc/mtab", "a");
		if (fdo == NULL)
			{
			goto error0;
			}
		sprintf(buf,"%s\t%s\tr%c\n", argv[0], argv[1], i ? 'o' : 'w');
		if (fputs(buf, fdo) == EOF)
			{
		error0:
			perror("can't update /etc/mtab");
			return 1;
			}
		fclose(fdo);
		}
	else
		{
		if (argc != 2)
			{
			printf("usage: umount something\n");
			return 1;
			}

		if (umount(argv[1]) < 0)
			{
			perror(argv[1]);
			return 1;
			}

		fdi = fopen("/etc/mtab", "r");
		if (fdi == NULL)
			{
			perror("can't open /etc/mtab");
			return 2;
			}
		fdo = fopen("/etc/mtab.tmp", "w");
		if (fdo == NULL)
			{
			perror("can't backup /etc/mtab");
			return 2;
			}
		fnd = 0;
		for (i = 0; !feof(fdi); i++)
			{
			fgets(buf, sizeof(buf)-1, fdi);
			p = strchr(buf, '\t');
			if (p == NULL)
				{
				continue;
				}
			c = *p;
			*p = 0;
			if (strcmp(buf,argv[1]) == 0)
				{
				++fnd;
				continue;
				}
			*p = c;
			fputs(buf, fdo);
			fflush(fdo);
			}
		fclose(fdi);
		fclose(fdo);
		if (i < 1)
			{
			errno = EFAULT;
			perror("can't backup /etc/mtab");
			goto error2;
			}
		if (fnd == 0)
			{
			perror("/etc/mtab not correctly updated");
			goto error2;
			}
		if (unlink("/etc/mtab"))
			{
			perror("can't replace /etc/mtab");
		error2:
			unlink("/etc/mtab.tmp");
			return 2;
			}
		if (rename("/etc/mtab.tmp", "/etc/mtab"))
			{
			perror("can't rename /etc/mtab.tmp to /etc/mtab");
			return 3;
			}
		}
/* printf("mount exiting\n"); */
	return 0;
}

