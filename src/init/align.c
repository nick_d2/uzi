/* align.c
 * descripton:	 change files position on disk
 * author:	 Archi Schekochikhin
 *		 Adriano Cunha <adrcunha@dcc.unicamp.br>
 * license:	 GNU Public License version 2
 */

#include <stdio.h> /* for stdout */
#include <unistd.h>
#include <fcntl.h> /* for O_RDONLY */
#include <sys/stat.h> /* for XIP_ALIGN */
#include <utime.h>
#include <time.h>
#include <string.h>
#include <errno.h>

#ifndef DEBUG
#define DEBUG 0
#endif

char *prog_name;		/* Call name of this program. */

typedef enum identity { ALIGN, UALIGN } identity_t;

identity_t identity;		/* How did the user call me? */

char *basename(const char *path)
/* Return the last component of a pathname.  (Note: declassifies a const
 * char * just like strchr.
 */
{
	const char *p= path;

	for (;;) {
		while (*p == '/') p++;		/* Trailing slashes? */

		if (*p == 0) break;

		path= p;
		while (*p != 0 && *p != '/') p++;	/* Skip component. */
	}
	return (char *) path;
}

int main(argc, argv)
	int argc;
	char **argv;
	{
	int i, fd, er = 0, /*ncreate = 0,*/ nsymlink = 0, verbose = 0;
	struct utimbuf utim;
	struct stat statbuf;
	char *p;
	time_t now, mytimet;
	int errs = 0;

	/* Call name of this program. */
	prog_name= basename(argv[0]);

	/* Required action. */
	if (strcmp(prog_name, "align") == 0) {
		identity= ALIGN;
	} else
	if (strcmp(prog_name, "ualign") == 0) {
		identity= UALIGN;
	} else {
		fprintf(stderr,
	"%s: Identity crisis, not called align or ualign\n", prog_name);
		exit(1);
	}

	argc--;
	argv++;
	while ((argc > 0) && (**argv == '-'))
		{
		argc--;
		p = *argv++;
		while (*++p)
			switch (*p)
				{
#if 0
			case 'c':
				ncreate = 1;
				break;
#endif
			case 's':
				nsymlink = 1;
				break;
			case 'v':
				verbose = 1;
				break;
			default:
				printf("unknown option %s\n", p);
				return 1;
				}
		}

	if (argc < 1)
		{
		switch (identity)
			{
		case ALIGN:
			printf("usage: align " /*"[-c] "*/ "[-s] [-v] filename ...\n");
			break;
		case UALIGN:
			printf("usage: ualign " /*"[-c] "*/ "[-s] [-v] filename ...\n");
			break;
			}
		return 1;
		}

	for (i = 0; i < argc; i++)
		{
		er = 0;
		p = argv[i];
		if (nsymlink)
			{
			fd = open(p, O_SYMLINK);
			if (fd >= 0)
				{
				close(fd);
				if (verbose)
					{
					printf("skipping symlink %s\n", p);
					}
				continue;
				}
			}
		fd = open(p, O_RDONLY);
#if 0
		if (fd < 0)
			{
			if (errno == ENOENT && ncreate == 0)
				{
				fd = creat(p, 0666);
				}
			}
#endif
		if (fd < 0)
			{
			er = -1;
			}
		else
			{
#if DEBUG > 0
			sysdebug(DEBUG);
#endif
			er = falign(fd, (identity == ALIGN) ?
					XIP_ALIGN : XIP_UALIGN);
#if DEBUG > 0
			sysdebug(0);
#endif
			close(fd);
			}
		if (er)
			{
			fprintf(stderr, (identity == ALIGN) ?
					"can't align " : "can't ualign");
			perror(p);
			errs++;
			}
		else if (verbose)
			{
			printf((identity == ALIGN) ?
					"align %s\n" : "ualign %s\n", p);
			}
		}

	return errs;
}

