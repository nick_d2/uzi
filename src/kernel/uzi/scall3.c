/*
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 Implementation of system calls
**********************************************************/

#define NEED__DEVIO
#define NEED__FILESYS
#define NEED__MACHDEP
#define NEED__SCALL

#if 1 /* Nick */
#include "vendor.h" /* Nick, must come first */
#include <unix.h>
#include <config.h>
#include <extern.h>
#include "devio.h" /* prototypes added by Nick */
#include "filesys.h" /* prototypes added by Nick */
#include "xip.h" /* prototypes added by Nick */
#include "scall3.h" /* prototypes added by Nick */
#define __KERNEL__ /* temporary */
#else
#include "uzix.h"
#ifdef SEPH
#include "types.h"
#include "signal.h"
#include "errno.h"
#include "sys\stat.h"
#endif
#include "unix.h"
#include "extern.h"
#endif

/* Implementation of system calls */

#define ISIZE(ino)	((ino)->c_node.i_size)

/* shortcuts for 'udata' access */
#define UWHERE		(udata.u_offset)
#define UBAS		(udata.u_base)
#define UCNT		(udata.u_count)
#define UERR		(udata.u_error)

#ifdef __KERNEL__

#if 1 /* Nick please look into this */
/* User's execve() call. All other flavors are library routines. */
/*******************************************
execve (name, argv, envp)        Function 23
char *name;
char *argv[];
char *envp[];
********************************************/
#define name (char *)udata.u_argn0
#define argv (char **)udata.u_argn1
#define envp (char **)udata.u_argn2

int sys_execve(void)
	{
	int argc;
	register inoptr ino;
	char *buf;
	blkno_t bmap();
	blkno_t blk;
	char **nargv, **nenvp;		/* In user space */
	struct s_argblk *abuf, *ebuf;
	int (**sigp)();
	char *progptr;			/* Original UZI */
#if 1 /* Nick EXE format */
	uint entry, count, remain, offset;
#if 1 /* Nick banked EXE format */
#if 1
	fsptr dev;
	dev_t devno;
	blkno_t i, pos, *region = NULL, regions, blocks;
#else
	blkno_t i, pos, regions;
#endif
#endif
#endif

	ino = namei(name, NULLINOPTR, 1);
	if (ino == NULL)
		{
		return -1;
		}

	if ((getperm(ino) & S_IOEXEC) == 0 ||
#if 0 /* Nick */
			(ino->c_node.i_mode & S_IFREG) == 0 ||
#endif
			(ino->c_node.i_mode &
				(S_IEXEC | S_IOEXEC | S_IGEXEC)) == 0)
		{
		udata.u_error = EACCES;
		goto nogood;
		}

#if 1 /* Nick */
	if (S_ISREGALIGN(ino->c_node.i_mode) == 0)
		{
		udata.u_error = ENOREGALIGN; /* file not regular or aligned */
		goto nogood;
		}
#endif

/* abyte('a'); */

	setftim (ino, A_TIME);
/* abyte('b'); */

	/* Read in the first block of the new program */
	buf = bread(ino->c_dev, bmap(ino, 0, 1), 0);
/* abyte('c'); */

#if 1 /* Nick EXE format */
	switch (((exefile_t *)buf)->e_magic)
		{
	case E_MAGIC:
		break;
	case 0x2123: /* '#!' */
		udata.u_error = ESHELL;
		goto nogood2;
	default:
		udata.u_error = ENOEXEC;
		goto nogood2;
		}

#if 1 /* Nick banked EXE format */
	entry = ((exefile_t *)buf)->e_format;
	switch (entry) /* temporary multiple use of variable */
		{
	case E_FORMAT_LARGE:
		break;
	case E_FORMAT_BANKED:
#if 1
		/* prepare to access the given inode */
		devno = ino->c_dev;
		dev = getfs(devno);

		/* construct region list using shared subroutine */
		region = bitmap_examine(dev, ino, ino->c_node.i_size,
					     &regions, &blocks);
		if (region == NULL)
			{
			goto nogood2; /* error code has been setup */
			}
#else
		if (ino->c_dev)
			{
			udata.u_error = ENOLCKFS;
			goto nogood2;
			}
		if (getmode(ino) != S_IFALIGN)
			{
			udata.u_error = ENOALIGN;
			goto nogood2;
			}
#endif
		break;
	default:
		udata.u_error = ENOEXEC;
		goto nogood2;
		}
#endif

	/* check the file's reported length against header */
	/* note: holes in the executable will cause a crash!! */
	if (((exefile_t *)buf)->e_size > ino->c_node.i_size)
		{
		udata.u_error = ETOOSHORT; /* file is too short, or hole */
		goto nogood2;
		}

	/* can't skip more than one block for header */
	if (((exefile_t *)buf)->e_hsize >= BUFSIZE)
		{
		udata.u_error = ENOEXEC; /* executable format error */
		goto nogood2;
		}
#else
	/****************************************
	 * Get magic number into var magic
	 * C3	: executable file no C/D sep.
	 * 00FF  :	 "		"  with C/D sep. (not supported in UZI180)
	 * other : maybe shell script (nogood2)
	 ****************************************/
	if ((*buf & 0xff) != EMAGIC)
		{
		if (((uchar *)buf)[0] == '#' && ((uchar *)buf)[1] == '!')
			{
			udata.u_error = ESHELL;
			}
		else
			{
			udata.u_error = ENOEXEC;
			}
		goto nogood2;
		}
#endif

#if 0 /* Nick has temporarily removed CP/M support */
		   /* Set Flag to indicate type of Executable (UZI-CP/M) */
	if ((*(buf+3)=='U') && (*(buf+4)=='Z') && (*(buf+5)=='I'))
		uzicom = 1;
	else
		uzicom = 0;
#endif
/* abyte('d'); */

	/* Gather the arguments, and put them in temporary buffers. */
	/* Put environment in another buffer. */
	abuf = (struct s_argblk *)tmpbuf();
	ebuf = (struct s_argblk *)tmpbuf();
/* abyte('e'); */

	if (wargs(argv, abuf) || wargs(envp, ebuf))
		{
		goto nogood3;	 /* SN */
		}
/* abyte('f'); */

	di(); /* not sure if this is really necessary */

	/* Check setuid stuff here. No other changes needed in user data */
	if (ino->c_node.i_mode & S_ISUID)
		{
		udata.u_euid = ino->c_node.i_uid;
		}

	if (ino->c_node.i_mode & S_ISGID)
		{
		udata.u_egid = ino->c_node.i_gid;
		}

	/* Reset child of fork */
	udata.u_ptab->p_fork_inf = 0;
/* abyte('g'); */

	/* We are definitely going to succeed with the exec,
	 * so we can start writing over the old program
	 */

#if 1 /* Nick EXE format */
	/* get break for program (not including argument data) */
	nargv = (char **)((exefile_t *)buf)->e_break;

	/* read back arguments, determine position of environment */
	nenvp = (char **)rargs((char *)nargv, abuf, &argc);
	brelse((bufptr)abuf);

	/* read back environment, determine initial break address */
	udata.u_ptab->p_break = (char **)rargs((char *)nenvp, ebuf, NULL);
	udata.u_break = (uint)udata.u_ptab->p_break; /* phase this out */

	/* zero out the program's udata and stack segments */
	progptr = (char *)((exefile_t *)buf)->e_udata;
	remain = (uint)nargv - ((exefile_t *)buf)->e_udata;

#if 1 /* Nick banked EXE format */
	bzero(ebuf, BUFSIZE); /* optimised for size, not speed */
	uput(ebuf, 0x8000, 0x100); /* zero out virtual memory table */
#else
	count = int_min(remain, BUFSIZE);
	bzero(ebuf, count); /* optimised for speed, not size */
#endif

	while (remain)
		{
		count = int_min(remain, BUFSIZE);
		uput(ebuf, progptr, count);

		progptr += count;
		remain -= count;
		}

	brelse((bufptr)ebuf);

	/* push the arguments to main(), just below the argument data */
	uputw(argc, nargv - 3);
	uputw(nargv, nargv - 2);
	uputw(nenvp, nargv - 1);

	/* set initial stack for program, just below parameters pushed */
	udata.u_isp = (char *)(nargv - 3);

#if 1 /* Nick banked EXE format */
	/* if executable is banked, map it into virtual memory space */
	if (entry == E_FORMAT_BANKED) /* entry = e_format field from header */
		{
		/* find initial location of program, in virtual memory */
		progptr = (char *)0x8000; /* hard coded for now */

#if 1
		for (i = 0; i < regions; i++)
			{
			/* convert to page number, put in virt memory table */
			uputc(0x40 + (region[i] >> (PAGE_LOG-BUFSIZELOG)) - 4,
				progptr++);
			}

		if (brelse((bufptr)region) < 0)
			{
			/* udata.u_error = something; */
			}
		/* region = NULL; */ /* don't bother, we can't fail now */
#else
		/* find count of regions, using the file's reported size */
		regions = (ino->c_node.i_size +
				REGION_BYTES - 1) >> REGION_LOG;

		/* find each region's existing physical memory address */
		pos = 0;
		for (i = 0; i < regions; i++)
			{
			/* first block must be aligned, find its position */
			blk = bmap(ino, pos, 1);

			/* convert to page number, put in virt memory table */
			uputc(0x40 + (blk >> (PAGE_LOG-BUFSIZELOG)) - 4,
				progptr++);

			pos += REGION_BLOCKS;
			}
#endif
		}
#endif

	/* find out program's entry address, while buf is still valid */
	entry = ((exefile_t *)buf)->e_entry;
#else
	/* Move 1st Block to user bank */
	uput(buf, PROGBASE, BUFSIZE);
	brelse((bufptr)buf);
/* abyte('h'); */
#endif

#if 0 /* Nick redundant */
	/* At this point, we are committed to reading in and
	 * executing the program. We switch to a local stack,
	 * and pass to it the necessary parameter: ino
	 */

	udata.u_ino = ino;	 /* Temporarily stash these here */

/* I think this must be a mistake... not necessary and can't use automatic */
/* variables after this point, because SP needs a known relationship to IX */
/*	tempstack(); */
/* abyte('i'); */
#endif

#if 1 /* Nick EXE format */
	/* Read in the rest of the program */
	/* we have already checked that offset is < BUFSIZE */
	progptr = (char *)((exefile_t *)buf)->e_idata;
	offset = ((exefile_t *)buf)->e_hsize;
	remain = ((exefile_t *)buf)->e_udata - (uint)progptr;

	if (remain) /* first block is treated specially */
		{
		count = int_min(remain, BUFSIZE - offset);
		uput(buf + offset, progptr, count);

		progptr += count;
		remain -= count;
		}
	brelse((bufptr)buf);

	blk = 1;
	while (remain)
		{
		buf = bread(ino->c_dev, bmap(ino, blk++, 1), 0);

		count = int_min(remain, BUFSIZE);
		uput(buf, progptr, count);

		progptr += count;
		remain -= count;

		brelse((bufptr)buf);
		}
#else
	/* Read in the rest of the program */
	progptr = PROGBASE + BUFSIZE;
	for (blk = 1; blk <= (ino->c_node.i_size >> BUFSIZELOG); ++blk)
		{
		buf = bread(ino->c_dev, bmap(ino, blk, 1), 0);
		uput(buf, progptr, BUFSIZE);
		brelse((bufptr)buf);
		progptr += BUFSIZE;
		}
#endif
/* abyte('j'); */
	i_deref(ino);

#if 0 /* Nick EXE format */
	/* Set Break for program */
	udata.u_ptab->p_break = (void *)progptr;
	udata.u_break = (int)progptr; /* this needs to be phased out */
/* abyte('k'); */
#endif

	/* Turn off caught signals */
	for (sigp = udata.u_sigvec; sigp < (udata.u_sigvec + NSIGS); sigp++)
		{
		if (((long)*sigp) != SIG_IGN)
			{
			*sigp = (void (*)(signal_t))SIG_DFL;
			}
		}

#if 0 /* Nick EXE format */
	/* Read back the arguments and the environment */
	nargv = (char **)rargs(PROGTOP-2, abuf, &argc);
	nenvp = (char **)rargs((char *)nargv, ebuf, NULL);
#endif

	/* Fill in udata.u_name with Program invocation name */
	uget(ugetw(nargv), udata.u_name, 8);
/* abyte('l'); */

#if 0 /* Nick EXE format */
	brelse((bufptr)abuf);
	brelse((bufptr)ebuf);
#endif

#if 0 /* Nick */
	/* Shove argc and the address of argv just below envp */
	uputw(argc, nenvp - 3);
	uputw(nargv, nenvp - 2);
	uputw(nenvp, nenvp - 1);
#endif

	/* Jump into the program, first setting the stack
	 * and copying the udata block
	 */

	ei(); /* not sure if this is really necessary */
/* abyte('m'); */
#if 0 /* Nick EXE format */
	udata.u_isp = (char *)(nenvp - 3);
	doexec((int16 *)udata.u_isp);
#else
	doexec((int *)udata.u_isp, entry);
#endif

nogood3:
	brelse((bufptr)abuf);
	brelse((bufptr)ebuf);
nogood2:
#if 1
	if (region)
		{
		brelse((bufptr)region);
		}
#endif
	brelse((bufptr)buf);
nogood:
	i_deref(ino);
	return -1;
	}

#undef name
#undef argv
#undef envp

/* SN    TODO      max (1024) 512 bytes for argv
               and max  512 bytes for environ
*/


int wargs(char **argv, struct s_argblk *argbuf)       /* argv in user space */
{
    register char *ptr;    /* Address of base of arg strings in user space */
    int      c;
    register char *bufp;
    /* Nick unsigned ugetw(); due to IAR stricter checking */
    char     ugetc();
/* kprintf("wargs 0x%x\n", argv); */

    argbuf->a_argc = 0;              /* Store argc in argbuf */
    bufp = argbuf->a_buf;

    while (ptr = (char *)ugetw (argv++))
    {
/* kprintf("0x%x\n", ptr); */
        ++(argbuf->a_argc);          /* Store argc in argbuf. */
        do
        {
            *bufp++ = c = ugetc (ptr++);
            if (bufp > argbuf->a_buf+500)
            {
                udata.u_error = E2BIG;
                return (1);
            }
        }
        while (c);
    }
    argbuf->a_arglen = bufp - argbuf->a_buf;    /*Store total string size. */
    return (0);
}



char *rargs(char *ptr, struct s_argblk *argbuf, int  *cnt)
{
    int  argc;
    char **argv;         /* Address of users argv[], just below ptr */
#if 0 /* Nick EXE format */
    int arglen;
    char *argbase;
#endif
    char *sptr;

    sptr = argbuf->a_buf;

#if 1 /* Nick EXE format */
    /* Move them into the users address space, from the bottom */
    argc = argbuf->a_argc;
    argv = (char **)ptr;
    ptr += (argc + 1) * sizeof(char *);

/* abyte('a'); */
/* ahexw((int)sptr); */
/* abyte(' '); */
/* ahexw((int)ptr); */
/* abyte(' '); */
/* ahexw((int)argbuf->a_arglen); */
    if (argbuf->a_arglen)
        uput (sptr, ptr, argbuf->a_arglen);
/* abyte('b'); */
#else
    /* Move them into the users address space, at the very top */
    ptr -= (arglen = argbuf->a_arglen);

/* abyte('a'); */
/* ahexw((int)sptr); */
/* abyte(' '); */
/* ahexw((int)ptr); */
/* abyte(' '); */
/* ahexw((int)arglen); */
    if (arglen)
        uput (sptr, ptr, arglen);
/* abyte('b'); */

    /* Set argv to point below the argument strings */
    argc = argbuf->a_argc;
    argbase = argv = (char **)ptr - (argc + 1);
#endif

    if (cnt)
/* { */
/* kprintf("(argc = %d)\n", argc); */
        *cnt = argc;
/* } */

    /* Set each element of argv[] to point to its argument string */
    while (argc--)
    {
/* abyte('c'); */
/* ahexw((int)ptr); */
/* abyte(' '); */
/* ahexw((int)argv); */
        uputw (ptr, argv++);
/* abyte('d'); */
#if 1 /* Nick EXE format */
        do
            ++ptr;
        while (*sptr++);
#else
        if (argc)
        {
        }
#endif
    }
/* abyte('e'); */
/* ahexw(0); */
/* abyte(' '); */
/* ahexw((int)argv); */
    /* add Null Pointer to end of array */
    uputw ((char *)0, argv);
/* abyte('f'); */
#if 1 /* Nick EXE format */
    return ptr;
#else
    return argbase;
#endif
}
#else
int repack(char **argp, char **envp, uchar check));
void *scop(char **argv, uchar cnt, char *to, char *real);
uint ssiz(char **argv, uchar *cnt, char **smin);
void exec2(void);

static uint __argc;
static char **__argv;
static char **__envp;

/* check for arguments size */
uint ssiz(char **argv, uchar *cnt, char **smin)
{
	char *p, *mi = *smin;
	uint n = 0;

	*cnt = 1;
	while ((p = *argv++) != NULL) {
		if (p < mi)	mi = p;
		while (++n, *p++ != '\0')
			;
		++(*cnt);
	}
	*smin = mi;
	return n;
}

/* copy arguments vector */
void *scop(char **argv, uchar cnt, char *to, char *real)
{
	char *s, **a = (char **)to, *q = to + sizeof(char *) * cnt;

	while ((s = *argv++) != NULL) {
		*a++ = real;
		while (++real, (*q++ = *s++) != '\0')
			;
	}
	*a = NULL;
	return q;
}

/* move all arguments to top of program memory or check their size */
/* for move, stack can't be near the top of program memory and */
/* PROGBASE area is used as draft area */
int repack(char **argp, char **envp, uchar check)
{
	uint tot, et, asiz, esiz;
	uchar acnt, ecnt;
	register char *p = (void *)PROGBASE;
	char *amin = (void *)UZIXBASE;

	asiz = ssiz(argp, &acnt, &amin);
	esiz = ssiz(envp, &ecnt, &amin);

	tot = (asiz + sizeof(char *) * acnt) +
	      (et = esiz + sizeof(char *) * ecnt);
	if ((uint)p + tot > (uint)amin) {	/* p == PROGBASE! */
		UERR = ENOMEM;
Err:		return 1;	/* no room for args */
	}
	if (check) {
		if ((uint)UBAS > (uint)(UZIXBASE - 1 - tot)) {
			UERR = E2BIG;
			goto Err;	/* no room for args */
		}
		goto Ok;
	}
	p = scop(argp, acnt, p, (char *)(UZIXBASE - 1 - et - asiz));
	scop(envp, ecnt, p, (char *)(UZIXBASE - 1 - esiz));
	__envp = (char **)(UZIXBASE - 1 - et);
	__argv = (char **)(UZIXBASE - 1 - tot);
	__argc = acnt-1;
	bcopy((void *)PROGBASE, __argv, tot);
Ok:	return 0;
}

/* exec2() second step of execve - executed on system stack! */
void exec2(void)
{
	register char **p, *progptr;
	register blkno_t pblk;
	uchar blk = 0;
	char *buf;

	/* Read in the rest of the program */
	progptr = (char *)PROGBASE;
	while (blk <= UCNT) {
		if ((pblk = bmap(udata.u_ino, blk, 1)) != NULLBLK) {
			buf = bread(udata.u_ino->c_dev, pblk, 0);
			if (buf != NULL) {
				bcopy(buf, progptr, BUFSIZE);
				brelse((bufptr)buf);
			}
		}
		progptr += BUFSIZE;
		++blk;
	}
	i_deref(udata.u_ino);
	/* Zero out the free memory */
	bzero(progptr, (uint) ((char *)__argv - progptr));
	udata.u_ptab->p_break = (void *)(((exeptr)PROGBASE)->e_bss);
	udata.u_break = (void *)(((exeptr)PROGBASE)->e_bss);
#if DEBUG > 1
	udata.u_traceme = (DEBUG > 10); /* Nick 0; */
#endif
	/* Fill in udata.u_name */
	bcopy(*__argv, udata.u_name, DIRNAMELEN);
	/* Shove argc and the address of argv just below argv */
	p = __argv;
	*--__argv = (char *) __envp;
	*--__argv = (char *) p;
	*--__argv = (char *) __argc;
#ifdef UZIX_MODULE
	RESET_MODULE();
#endif
	/* Machine dependant jump into the program, first setting the stack */
	doexec((uint)__argv);	/* NORETURN */
}

/*********************************************************
 SYSCALL execve(char *name, char *argv[], char *envp[]);
*********************************************************/
#define name (char *)udata.u_argn0
#define argv (char **)udata.u_argn1
#define envp (char **)udata.u_argn2
int sys_execve(void)
{
	register inoptr ino;
	register uchar *buf;
	register sig_t *sp;
	uchar magic;
	uint mblk;

	if ((ino = namei(name, NULL, 1)) == 0) {
		UERR = ENOENT;
		goto Err1;
	}
	mblk = (uint)ISIZE(ino);	/* image length in bytes */
	if (PROGBASE + ISIZE(ino) >= UZIXBASE) { /* long operation! */
		UERR = ENOMEM;
		goto Ret;
	}
	if ((getperm(ino) & S_IOEXEC) == 0 ||
	      getmode(ino) != S_IFREG ||
	      (ino->c_node.i_mode & (S_IEXEC | S_IGEXEC | S_IOEXEC)) == 0) {
		UERR = EACCES;
		goto Ret;
	}
	/* save information about program size in blocks/top */
	UCNT = (mblk + BUFSIZE - 1) >> BUFSIZELOG;
	UBAS = (void *)(PROGBASE + mblk + 1024); /* min +1K stack */
	/* Read in the first block of the new program */
	buf = bread(ino->c_dev, bmap(ino, 0, 1), 0);
	if (buf == NULL) goto Ret;
	magic = ((exeptr)buf)->e_magic;
	if (brelse((bufptr)buf) != 0) goto Ret;
	if (magic != EMAGIC) {
		if (((uchar *)buf)[0] == '#' && ((uchar *)buf)[1] == '!')
			UERR = ESHELL;
		else	UERR = ENOEXEC;
		goto Ret;
	}
	setftim(ino, A_TIME);	/* set access time */
	/* Turn off caught signals */
	sp = udata.u_sigvec;
	while (sp < (udata.u_sigvec + NSIGS)) {
		if ((sig_t)(*sp) != SIG_IGN)
			*(sig_t *)sp = SIG_DFL;
		++sp;
	}
	/* Here, check the setuid stuff. No other changes need be
	 * made in the user data
	 */
	if (ino->c_node.i_mode & S_ISUID) udata.u_euid = ino->c_node.i_uid;
	if (ino->c_node.i_mode & S_ISGID) udata.u_egid = ino->c_node.i_gid;
	/*
	 * at this point, we are committed to reading in and executing
	 * the program. We switch to a local stack, and pass to it the
	 * necessary parameter: ino
	 */
	udata.u_ino = ino;	/* Temporarly stash these here */
	if (repack(argv, envp, 1))	/* Check args size */
		goto Ret;
	tempstack();
	repack(argv, envp, 0);	/* Move arguments */
	exec2();

Ret:	i_deref(ino);
Err1:	return (-1);
}
#undef name
#undef argv
#undef envp
#endif

#if 0 /* def UZIX_MODULE */

#define proc_page0 udata.u_ptab->p_swap[0]
#define proc_page1 udata.u_ptab->p_swap[1]
static void *stkbkp = NULL;

modtabptr searchmodule (int sig) {
	modtabptr mtable=&modtab;

	while  (mtable != modtab + MTABSIZE) {
		if (mtable->sig==sig) 
			return mtable;
		++mtable;
	}
	UERR=ESRCH;
	return NULL;
}

/* call module handler */
int callmodu2(uint sign, uint fcn, char *args, int argsize, int pid) {
#ifdef PC_UZIX_TARGET
	uchar TEMPDBUF[512];		/* possible problem: stack too deep */
#else
/* MSX already defines TEMPDBUF as a 512bytes buffer */
#endif
	static modtabptr mtable;
	static uint _fcn;
	static uint _argsiz;
	static int _ppid;
	static ptptr _p;

	if (sign == NULL) goto Err;
	if ((mtable = searchmodule(sign)) == NULL) goto Err;
	_di();
	_p = udata.u_ptab;
	_ppid = pid == 0 ? _p->p_pid : pid;
	_fcn=fcn;
	_argsiz=argsize;
	if (argsize > 0) bcopy(args, (uchar *)TEMPDBUF, argsize);
	NOTUSED(fcn);		/* this is a trick for HTC. if you remove this,
				   if the above IF is false, HTC will jump the
				   _asm below */
#define	__SAVESTACK	callmodu2_SAVESTACK
#ifdef MSX_UZIX_TARGET
#include "process.msx"
#endif
#ifdef PC_UZIX_TARGET
#include "process.mtc"
#endif
	tempstack();
	swap_read(mtable->page);	/* swap_read can't do ei()! */
	_fcn=mtable->fcn_hnd(_fcn, _ppid, (uchar *)TEMPDBUF, _argsiz);
	_di();
	swap_read(_p->p_swap);
#define	__RESTSTACK	callmodu2_RESTSTACK
#ifdef MSX_UZIX_TARGET
#include "process.msx"
#endif
#ifdef PC_UZIX_TARGET
#include "process.mtc"
#endif
	_ei();
	if (_fcn!=0) {
		UERR=_fcn;
Err:		return -1;
	}
	return 0;
}

/* clear waiting replies from module to the exited process */
void clear_module_reqs(int pid) {
	modreplyptr mreply;
	modtabptr mtable=&modtab;

	for (mreply=modreply; mreply < modreply + RTABSIZE; ++mreply) {
		if (mreply->pid==pid) mreply->sig = 0;
	}
	/* tell modules that the queries/replies for this
	 * process can be erased
	 */
	while  (mtable != modtab + MTABSIZE) {
		callmodu2(mtable->sig, 0, NULL, -1, pid);
		if (mtable->page[0]==proc_page0 && mtable->page[1]==proc_page1) {
			mtable->sig=0;
			RESET_MODULE();
		}
		++mtable;
	}
}

/*********************************************************
 SYSCALL reg(uint sig, int (*func)());
*********************************************************/
#define sign (uint)udata.u_argn0
#define func (void (*))udata.u_argn1
int sys_reg(void)
{
	modtabptr mtable=&modtab;
	
	while  (mtable != modtab + MTABSIZE) {
		if (mtable->sig==0) goto fnd; 
		++mtable;
	}
	UERR=ENOMEM;
	return -1;
fnd:	mtable->sig = sign;
	mtable->fcn_hnd = func;
	mtable->page[0] = proc_page0;
	mtable->page[1] = proc_page1;
	SET_MODULE();
	return 0;
}
#undef func
#undef sign

/*********************************************************
 SYSCALL dereg(uint sig);
*********************************************************/
#define sign (uint)udata.u_argn0
int sys_dereg(void)
{
	modtabptr mtable;
	
	if ((mtable = searchmodule(sign)) == NULL) goto Err;
	if (!super() && 
		(mtable->page[0]!=proc_page0 || mtable->page[1]!=proc_page1)) {
		UERR=EPERM;
Err:		return -1;
	}
	/* we should search for a waiting reply on reply table and return
	   EBUSY if the table is not clear... */
	mtable->sig=0;
	RESET_MODULE();
	return 0;
}
#undef func
#undef sign

/*********************************************************
 SYSCALL callmodu(uint sig, uint fcn, char *args, int argsize);
*********************************************************/
#define sign (uint)udata.u_argn0
#define fcn (uint)udata.u_argn1
#define args (char *)udata.u_argn2
#define argsize (int)udata.u_argn3
int sys_callmodu(void)
{
	if (argsize < 0 || argsize > BUFSIZE) {
		UERR=EINVAL;
		return -1;
	}
	return callmodu2(sign, fcn, args, argsize, 0);
}
#undef sign
#undef fcn
#undef args
#undef argsize

/*********************************************************
 SYSCALL modreply(int sig, int fcn, char *repladdr);
*********************************************************/
#define sign (uint)udata.u_argn0
#define fctn (uint)udata.u_argn1
#define repladdr (uint)udata.u_argn2
/* send module reply to the process that requested module function */
int sys_modreply(void)
{
	modtabptr mtable;
	modreplyptr mreply;
	struct swap_mmread pp;
	int p_id;
	
	if ((mtable = searchmodule(sign)) == NULL) {
		goto Err;
	}
	p_id = (udata.u_ptab)->p_pid;
	pp.mm[0]=mtable->page[0];
	pp.mm[1]=mtable->page[1];
	for (mreply=modreply; mreply < modreply + RTABSIZE; ++mreply) {
		if (mreply->sig==sign && mreply->pid==p_id && mreply->fcn==fctn) {
			pp.buf = (void *)repladdr;
			pp.size = mreply->replysize;
			pp.offset = (uint)(mreply->replyaddr);
			if (swap_mmread(&pp) < 0) {
reperr:				UERR=EIO;
				goto Err1;
			}
			if (callmodu2(sign, fctn, NULL, -1, 0) < 0) goto reperr;	/* clear module reply table */
			mreply->sig=0;
			return 0;
		}
	}
	/* since this syscall is called essentialy by a library, the common
	   structure is "while (modreply()<0)". so, the module must be executed
	   to answer the call. so, let's not waste CPU calling this syscall
	   over and over again and returning -1 until process timeslice expires.
	   let's give other processes their bit of CPU */
	/* no reply found for this query */
	UERR=ENXIO;
Err1:	runticks = udata.u_ptab->p_cprio;
Err:	return -1;
}
#undef sign
#undef fctn
#undef repladdr

/*********************************************************
 SYSCALL repfmodu(int pid, int fcn, char *repladdr, int replysize);
*********************************************************/
#define p_id (uint)udata.u_argn0
#define fcnt (uint)udata.u_argn1
#define repladdr (char *)udata.u_argn2
#define reply_size (uint)udata.u_argn3
/* put module reply in the reply table to the waiting process */
int sys_repfmodu(void)
{
	modtabptr mtable=&modtab;
	modreplyptr mreply;

	while  (mtable != modtab + MTABSIZE) {
		if (mtable->page[0]==proc_page0 && mtable->page[1]==proc_page1)
			goto fnd;
		++mtable;
	}
	UERR=ESRCH;
	goto Err;
fnd:
	for (mreply=modreply; mreply < modreply + RTABSIZE; ++mreply) {
		if (mreply->sig==0) {
			mreply->sig=mtable->sig;
			mreply->fcn=fcnt;
			mreply->pid=p_id;
			mreply->replyaddr=repladdr;
			mreply->replysize=reply_size;
			return 0;
		}
	}
	UERR=ENXIO;
Err:	return -1;
}
#undef p_id
#undef fcnt
#undef repladdr
#undef reply_size

#endif  /* UZIX_MODULE */

#endif	/* __KERNEL__ */
