;/***************************************************************
;   UZI (Unix Z80 Implementation) Kernel:  machasm.asz
;----------------------------------------------------------------
; Adapted from UZI By Doug Braun, and UZI280 by Stefan Nitschke
;            Copyright (C) 1998 by Harold F. Bower
;       Portions Copyright (C) 1995 by Stefan Nitschke
;****************************************************************/
;/* Revisions:
; * 22.12.97 - extracted from several modules.		      HFB
; */

; These routines reflect settings for the YASBEC SBC.
; Capabilities of the YASBEC are:

;  SCSI   - National DP5380/8490 Controller with or
;             without Interrupt-driven DMA transfer.
;  Floppy - Western Digital FD1772.  Modifications
;             for National 8473 also available.
;             Polled operation only.
;  RTC    - Dallas DS-1216E installed between ROM and socket.
;  Printer- Octal latch with Interrupt *ACK (limited Centronics).
;  Memory - 128k to 1 MB available.
;  RS-232 - Two Channels w/partial handshaking.
;             Channel 0 - Auxiliary
;             Channel 1 - Console TTY

; Resources used and chip set are:

;  Z180 ASCI0  - Aux Serial port (unused here)
;  Z180 ASCI1  - TTY Console (ASCI1 Interrupt)
;  Z180 Timer0 - 50/100 mS Interrupts (ASCI1 Int)
;                Provides ticks for time slicing
;  Z180 Int0   - SCSI (5380) (optional Interrupt)
;  Z180 Int1   - Parallel printer ACK Latch
;  Dallas DS-1216 Clock/Calendar (for TOD)
;****************************************************************/

	public	_initsys
 .if 0 ; Nick
	public	_rdtod
	public	__putc
 .endif
	public	_lpout, _ei, _di				; PUBlic
 .if 1 ; Nick
	public	_di_absolute, _ei_absolute
 .endif
	public	_IRET, _tempstack, _uget, _ugets, _uput		;   "
	public	_ugetw, _uputw, _ugetc, _uputc, _doexec, _unix	;   "
	public	_osBank
 .if 0 ; Nick has temporarily removed CP/M support
	public	_uzicom
 .endif
 .if 0 ; Nick, not public anymore, now defined by extern.h (can be anywhere)
	extern	_ub
 .else
	public	_ub
 .endif

	extern	_fs_init, _tty_inproc, _panic, _inint, _kprintf	; in machdep.c
	extern	_calltrap					;   "
	extern	_FdcCk						; flopasm.asz
	extern	_clkint2, _unix2				; in process.c
	extern	_tod						; in data.c
 .if 0 ; IAR
	extern	__Lbss, __Hdata					; from linker
 .endif
 .if 0 ; Nick has temporarily removed CP/M support
	extern	__Lcpm22, __Hcpm22, __Bcpm22			; from linker
	extern	EStart						; in emu.asz
 .endif
 .if 0 ; Nick
	extern	_bcopy						; runtime.lib
 .endif

	extern	copyr
	extern	abyte, ahexw, acrlf
 .if 1 ; Nick
	extern	_unix_locked_out
 .endif
	extern	_exit ; for trap errors

 .if 1 ; IAR
	extern	?BANK_CALL_DIRECT_L08
	extern	?BANK_FAST_LEAVE_L08
 .endif

;#include <unix.h>
;#include <config.h>
;#include <extern.h>

$ asmdef.inc		; Common Definitions, Ascii and CP/M
$ z180.inc		; Z180 Register Mnemonics

	rseg	CODE
;/*---------------------------------------------------------------*/
;/* Perform initial system setup from cold start.  Begin by       */
;/* clearing all of the uninitialized RAM to 0, then Initialize   */
;/* the Z180 Interrupt Vector Table.  Interrupt Handler addresses */
;/* are installed at the appropriate locations in Init, and the   */
;/* base address of the table is set in Z180 registers.           */
;/*    This interrupt table configuration is YASBEC-specific!     */
;/*---------------------------------------------------------------*/

;initsys()
;{
;#asm

_initsys:
 .if 1 ; IAR
	push	bc
	push	de

 .if 0
	ld	a,'!'
	call	abyte
 .endif
 .else
	ld	hl,__Hdata	; Pt to common image in low mem (Top of Data)
	ld	de,0F000H	;  Common Area destination
	ld	bc,endcom-_ub	;   and length
	ldir			;  ..move into position

		;    /* Initialize the internal Z180 interrupt table address */
		; NOTE: Insure that it begins on an even 32-byte boundary!

	ld	hl,itbl		; Get Address of Interrupt Table (Common Mem)
	OUT0	(IL),L		;    Set Z180 Low Int Adr
	ld	a,h		; Get Page
	ld	i,a		;  to Interrupt Register
	ld	a,00010001B	; Turn Ints On and enable downcounting
	OUT0	(TCR),A
	ld	a,00000111B	; Activate INT0 and Interrupts
	OUT0	(ITC),A
	di			;   <immediately disable ints>

  	ld	de,__Lbss	; Clear all bss Memory
	ld	hl,0F000H
	or	a
	sbc	hl,de		; Compute bss size
	ld	c,l
	ld	b,h		; Set count
	ld	l,e
	ld	h,d		;  Copy to source Reg
	inc	de		;   Dest = Source + 1
	dec	bc		;    deduct one from count for init byte
	ld	(hl),0		; Zero the first Byte
	ldir			;   and move it along
 .endif

;    /* Establish base of Common Memory which is always resident */
; This *MUST* be done here since CBR might be different if read in high mem

	IN0	A,(CBR)		; Get Current Bank
	LD	(_osBank),A	;  Save for later kernel use

 .if 1 ; IAR
	ld	hl,LWRD _fs_init
	ld	a,BYTE3 _fs_init
	call	?BANK_CALL_DIRECT_L08

	pop	de
	pop	bc
	jp	?BANK_FAST_LEAVE_L08
 .else
	jp	_fs_init	; ..chain to more setup in MACHDEP.C
 .endif

;=============== Section relocated to Common Memory (F000H) =============
; This portion of code is always in context in both User and System modes.
; Critical items are:
;    User Data Block (udata)
;    User Stack (from end of udata thru top of _ub)
;    Z180 Interrupt Table (because we don't know where we are when interrupted)
;    Interrupt Handlers (or at least vectors) which are "bank aware" if they
;		chain to code in the system area
;    Temporary Stack for use in process swapping or other temporary purposes

 .if 1 ; Nick, not needed anymore, now defined by extern.h (can be anywhere)
	rseg	RCODE
			; udata block padded up to 512 bytes (256 Wds).
			; Data is in the low part, with the Kernel (System)
			; Stack at _ub+512 and the Timer Interrupt Stack
			; 256 bytes above that.
_ub:
 .if 1
 defs 1000h
 .else
	defw	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	defw	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	defw	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	defw	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	defw	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	defw	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	defw	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	defw	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
			; Kernel stack ends here
	defw	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	defw	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	defw	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	defw	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
			; Timer Interrupt stack ends here
 .endif
 .endif

 .if 0 ; Nick
; NOTE: The interrupt table must start on a 32-byte boundary

itbl:			; INT0 - Uncomment Only one of these first two
	defw	_badint		;;-- For Polled SCSI
;;--	defw	_scsint		;;-- For Interrupt/DMA SCSI

	defw	_intpio		; INT1    - Printer ACK
	defw	_timer		; Timer0  - Process Tick
	defw	_badint		; Timer1  - (unused)
	defw	_badint		; DMA Ch0 - (unused)
	defw	_badint		; DMA Ch1 - (unused)
	defw	_badint		; CSIO    - (unused)
	defw	_badint		; ASCI0   - (unused)
	defw	_tty_int	; ASCI1   - Console TTY
itblen	 equ  $-itbl
;#endasm
;}
 .endif


;/*--------------- Interrupt Handler Routines --------------*/

TrapEr:	ld	hl,0		; Get Addr where Trap Occured
	add	hl,sp
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	ld	a,(_osBank)
	OUT0	(CBR),A		;  Get System Bank into Context
	ld	sp,0000
	push	de		; Trap Addr to stack
	ld	hl,trpmsg	;  Print message
	push	hl
 .if 1 ; IAR
	ld	hl,LWRD _kprintf
	ld	a,BYTE3 _kprintf
	call	?BANK_CALL_DIRECT_L08
 .else
	call	_kprintf
 .endif
 .if 1
	ld	de,1
	ld	hl,LWRD _exit
	ld	a,BYTE3 _exit
	jp	?BANK_CALL_DIRECT_L08
 .else
TrapL:	di
	jr	TrapL		; ..should never get here..but...
 .endif

trpmsg:	defm	'Trap @ %x!'
	defb	0AH,0

 .if 0 ; Nick
;.....
;badint()
;{
;    panic ("Bad Int");

_badint: ld	a,(_osBank)
	OUT0	(CBR),A		; Get System Bank into Context
	ld	sp,0000		; Set Error Stk
	ld	hl,bimsg
	push	hl
 .if 1 ; IAR
	ld	hl,LWRD _panic
	ld	a,BYTE3 _panic
	call	?BANK_CALL_DIRECT_L08
 .else
	call	_panic
 .endif
	ret			; ..should never get here

bimsg:	defm	'Bad Int'
	defb	0
;}


;static char ptrrdy;     /* Printer Ready Flag */


;/*---- Parallel Printer Interrupt Handler ----*/
;/* In the YASBEC, data is latched to an octal buffer, and the Printer
;   strobe activated by writing to different addresses.  When the
;   printer acknowledged the character by toggling its *ACK line low,
;   an interrupt in generated (INT 1) to indicate that the printer is
;   ready for another character.  This handler clears the flag and int.
; */

;intpio()
;{
;#asm

_intpio: di				; Clear Ints
	push	af
	ld	a,0ffh
	ld	(ptrrdy),a		; Set Ready Flag
	out	(42h),a			; ..and clear interrupt
	pop	af
	ei				; Enable Ints
	ret
;#endasm
;}


;/*--------------- Interrupt-Driven Console Handler ---------------*/
;/* This tty interrupt routine is activated in response to a received
;   character on ASCI1 of the Z180.  It reads the character from the
;   port then passed it to code in "devtty.c" to add it to the tty
;   input queue, echoing and processing backspace and carriage return.
;   If the queue contains a full line, it wakes up anything waiting
;   on it.  If it is totally full, it beeps at the user.
; */

;tty_int()
;{
;    register char c;
;#asm

_tty_int:
	di
	push	af		; Save machine state
	push	bc
	push	de
	push	hl
	push	ix
	push	iy
	IN0	L,(RDR1)	; Read char from ASCI1
	ld	h,0		;  make into word
	push	hl
	ld	l,1		; Set for minor = 1
	push	hl
 .if 1 ; IAR
	ld	hl,LWRD _tty_inproc
	ld	a,BYTE3 _tty_inproc
	call	?BANK_CALL_DIRECT_L08
 .else
	call	_tty_inproc	;   massage character in C (devtty.c)
 .endif
	pop	bc		;    (clr stk on ret)
	pop	bc		;     (both words)
	pop	iy
	pop	ix
	pop	hl
	pop	de
	pop	bc
	pop	af		; Restore state
	ei			;  Ints Ok now
	ret
;}


;/*---------------------- Timer Interrupt Handler ----------------------*/
;/* This is activated roughly every 50 mS (depending on the timer value
;   set on initialization) to control timeouts.  It only runs when
;   interrupts are enabled, so is subject to some error.  Set the correct
;   value in "config.h" for the desired rate depending on processor clock
;   speed for your configuration.

;   NOTE: This routine also provides a timer function to turn floppy disk
;   motors off after a pre-determined time (for the P112 or DB8473 mod)
;   and can also provide periodic sampling of the _tty routines for
;   systems which do not have interrupts on the serial (tty) ports.
; */
;timer()
;{
;#asm

_timer:	di			; No ints here
	ex	af,af'
	push	af		; Save all regs
	ex	af,af'
	exx
	push	bc
	push	de
	push	hl
	exx
	push	af
	push	bc
	push	de
	push	hl
	push	ix
	push	iy

	IN0	A,(TCR)
	IN0	A,(TMDR0L)
	IN0	A,(TMDR0H)	; Clear the Interrupt

	LD	(ISTACK-2),SP		; Save Entry Stack Ptr
	LD	SP,ISTACK-2		;   set New Stack

	IN0	A,(CBR)		; Read entry Bank Buffer Reg
	push	af		;   (save for exit)

	ld	a,(_osBank)
	OUT0	(CBR),A		;  Get System Bank into Context

 .if 1 ; IAR
	ld	hl,LWRD _FdcCk
	ld	a,BYTE3 _FdcCk
	call	?BANK_CALL_DIRECT_L08
 .else
	CALL	_FdcCk		; Perform needed Floppy Timeout functions
 .endif

 .if 0 ; Nick
	call	__cin		; Check polled tty ports
				;   process any waiting chars
 .endif

; We call the tick service routine in "process.c" in lieu of a real int.

 .if 1 ; IAR
	ld	hl,LWRD _clkint2
	ld	a,BYTE3 _clkint2
	call	?BANK_CALL_DIRECT_L08
 .else
	call	_clkint2;
 .endif

; Subroutine service() incorporated within this portion of the handler.
; It is sampled every 1/TICKSPERSECOND Secs, with calltrap() called as needed.

;    /* Deal with a pending caught signal, if any */
;    if (!udata.u_insys)

	ld	a,(_ub+OSYS)
	or	a		; In Kernel Mode?
	jr	nz,tdone	; ..jump if Not..no trap check

;    {
;        inint = 0;

	ld	(_inint),a	; (A still 0 from above)

;        calltrap();

 .if 1 ; IAR
	ld	hl,LWRD _calltrap
	ld	a,BYTE3 _calltrap
	call	?BANK_CALL_DIRECT_L08
 .else
	call	_calltrap	; Handle signal
 .endif
;    }

tdone:	pop	af		; Get entry Bank
	OUT0	(CBR),A		;  bring back into Context
	LD	SP,(ISTACK-2)	;   restore Entry Stack Ptr
	JP	_IRET		; ..exit, restoring all regs, enable Ints
;#endasm
;}
 .endif ; Nick


;/*----------------------------------------------------------*/
;/* Send a single character to Console, waiting if not ready.
;   This routine does not mask the MSB before sending, so it is
;   possible to send 8-bit characters to the console.  Output
;   handshaking is also not provided which may result in over-
;   driving the terminal at higher speeds.  In the Z180, output
;   flow control is automatic via rts/cts if so wired.
; */

; _putc (int minor, char c)

 .if 0 ; Nick
__putc:	POP	HL		; Return Address
	POP	DE		;  minor
	POP	BC		;   c
	PUSH	BC		; Keep Parms on stack
	PUSH	DE
; {
;     if (minor == 2) {

	DEC	E		; 2
	DEC	E		;   -> 0?
	JR	NZ,put0		; ..jump if Not 2 (assume 1)

;         /* tty1 (minor = 2) is desired, which is ASCI0 */
;         while (!(in (0x04)&02)) ;

put1:	IN0	A,(STAT0)	; ASCI0 status (Aux)
	AND	02H		;  Ready?
	JR	Z,put1		; ..loop til Ready

;         out (c, 0x06);

	OUT0	(TDR0),C	;  Send Char
	JP	(HL)		;   "Return"
;     }

;     else {

;         /* tty0 (minor = 0) is desired, which is ASCI1 */
;         while (!(in (0x05)&02)) ;

put0:	IN0	A,(STAT1)	; ASCI1 Status (Con)
	AND	02H		;  Ready?
	JR	Z,put0		; ..loop to wait if Not

;         out (c, 0x07);

	OUT0	(TDR1),C	; Send Char
	JP	(HL)		;   jump to Return addr
;     }
 .endif

;----------------------------------------------------------
; Check for waiting input characters from tty1 (ASCI0).
; ASCI1 is interrupt-driven and handled elsewhere.
; First check tty1 (ASCI0)

 .if 0 ; Nick
__cin:	IN0	A,(STAT0)	; Aux (Reader) Input Status
	AND	80H		;  keep only status bit
	LD	L,A
	LD	H,A		;   (prepare C-style return status)
	RET	Z

	IN0	A,(RDR0)	; Read ASCI0 Input Port
	LD	L,A
	LD	H,0		;  make into word
	PUSH	HL
	LD	L,2		;   minor = 2
	PUSH	HL
 .if 1 ; IAR
	ld	hl,LWRD _tty_inproc
	ld	a,BYTE3 _tty_inproc
	call	?BANK_CALL_DIRECT_L08
 .else
	CALL	_tty_inproc	; Process character
 .endif
	POP	BC		;  clean stack
	POP	BC
	JR	__cin		; ..loop to clean out waiting chars
 .endif

;/*---------------- Real Time Clock Interface -----------------*/
;/* Dallas DS-1216E Real-Time Clock/Calendar (aka No-Slot-Clock) Interface.
;   This functions by reading location 4 of the ROM after an initial setup
;   sequence to obtain date/time one bit at a time.  The bit appears in
;   the LSB of the byte, and each byte is read in MSB..LSB fashion.  The
;   entire Date/Time String is eight bytes read as:
;      10/100Sec   Sec   Min   Hour   DOW   Day   Mon   Year
;   This implementation ignores the tenths/hundredths and the Day-Of-Week
;   bytes (after using them to check validity) and converts the date/time
;   to the correct UZI (MS-DOS) format.
;*/

;/* Read Hardware Clock/Calendar into: struct  time_s	*/
;/*				     uint16  t_time;	*/
;/*				     uint16  t_date;	*/
;/* Update global time of day */
;rdtod()
;{

	rseg	CODE

 .if 1 ; Nick
;_rdtod:
;	ld	hl,0
;	ld	(_tod),hl
;	ld	(_tod+2),hl
;	jp	?BANK_FAST_LEAVE_L08
 .else
_rdtod:
	di			; NO INTERRUPTS in this routine
	ld	(_timstk+12),sp	; Store entry SP
	ld	sp,_timstk+12	;   set ours
			; Set up chip for reading
	IN0	A,(CBR)		; Read entry Bank Buffer Reg
	push	af		;  save for exit
	xor	a		; Set banked area for physical 0 (ROM)
	OUT0	(CBR),A
	IN0	A,(DCNTL)	; Read entry wait states
	push	af		;  save for exit
	or	0c0h		;   set to max memory waits
	OUT0	(DCNTL),A
	ld	hl,cstrng	; Point to control activation string
	ld	d,8		;  8 bytes
	ld	a,(0004h)	;   read clock control addr

nxbyt:	ld	b,8		; Gather 8 bits
	ld	c,(hl)		; Get a control byte
nxbit:	rr	c		; Is LSB "1"?
	jr	c,onebit	; ..jump if Yes
	ld	a,(0002h)	; Else read "0"
	jr	zerbit		; ..continue

onebit:	ld	a,(0003h)	; Read "1"
zerbit:	djnz	nxbit		; ..loop til all bits done
	inc	hl		; Advance to next byte
	dec	d		; more bytes to go?
	jr	nz,nxbyt	; ..loop if More
			; Else fall thru to Read Clock
	ld	hl,_lcltim+6	; Pt to tenths field (tenths field)
	ld	d,8		; Gather 8 bytes of info

rdbyt:	ld	b,8		; 8-bits per byte
rdbit:	ld	a,(0004h)	; Read a bit from the clock
	rra			;  put LSB in Carry
	rr	c		;   then to MSB of accumulated byte
	djnz	rdbit		;  ..loop til 8 bits read
	ld	a,d
	cp	4		; DOW field?
	jr	nz,rdbit0	; ..jump if No
	push	af		; else save for later Error checks
	jr	rdbit1		; ..bypass save/increment

rdbit0:	ld	(hl),c		; Save the byte
	dec	hl		;  backup to previous byte
rdbit1:	dec	d		; More bytes to read?
	jr	nz,rdbyt	; ..loop if Yes

	pop	af		; Retrieve DOW for Error check
	ld	c,a		;   save
	pop	af		; Restore wait settings
	OUT0	(DCNTL),A
	pop	af		; Restore Memory Bank
	OUT0	(CBR),A
	ld	a,c		; Get Error Flag
	or	a		; Error (0)?
	jp	z,noclock	; ..jump if Yes
	inc	a		; Error (FF)?
	jp	z,noclock	; ..jump if Yes

;    tod.t_date = tread(DAY) | (tread(MON)<<5) | (YEAR<<9);

	ld	hl,_lcltim	; Pt to years field of day/date
	ld	a,(hl)		;  fetch
	inc	hl		;   (advance to month)
	call	bcd2bin
	ADD	A,A
	LD	D,A
	LD	E,0		; (Year * 512) -or- (Year << 9)
	ld	a,(hl)		; Fetch month
	inc	hl		;  (advance to day)
	call	bcd2bin
	ld	c,a
	LD	B,32
	MLT	BC		; (Month * 32) -or- (Month << 5)
	ex	de,hl
	add	hl,bc		;    add month to shifted year
	LD	A,(DE)		; Fetch day
	INC	DE		;  (advance to hrs)
	call	bcd2bin
	OR	L		; Add Binary Day
	LD	L,A		;  to Yr,Mo
	ld	(_tod+2),hl	;   Save in _tod.t_date
	ex	de,hl		;    restore ptr

;    tod.t_time = (tread(SECS)>>1) | (tread(MINS)<<5) | (tread(HRS)<<11);

	ld	a,(hl)		; Fetch Hours
	inc	hl		;  (advance to mins)
	call	bcd2bin
	ADD	A,A		; (Hrs * 8)
	ADD	A,A		;  -or-
	ADD	A,A		;   (Hrs << 11-8)
	LD	D,A
	LD	E,0		;    = Hrs << 11 -or- Hrs * 2048
	ld	a,(hl)		; Fetch Minutes
	inc	hl		;  (advance to secs)
	call	bcd2bin
	ld	c,a
	LD	B,32
	MLT	BC		; (Mins * 32) -or- (Mins << 5)
	ex	de,hl
	add	hl,bc		;    add mins to shifted hrs
	LD	A,(DE)		; Fetch Secs
	call	bcd2bin
	srl	a		; Shift right one to strip LSB
	OR	L		; Add Binary (shifted) Sec
	LD	L,A		;  to Hr,Min
	ld	(_tod),hl	;  save in _tod.t_time

timex:	ld	sp,(_timstk+12)	; Restore entry Stack
 .if 1 ; IAR
	ld	hl,LWRD _ei
	ld	a,BYTE3 _ei
	call	?BANK_CALL_DIRECT_L08
 .else
	call	_ei		;  Ints Ok now
 .endif
	ret			;   return!


noclock: ld	hl,0
	ld	(_tod),hl
	ld	(_tod+2),hl
	jr	timex

cstrng:	defb	0c5h,3ah,0a3h,5ch,0c5h,3ah,0a3h,5ch
;}

; Helper file for converting Time/Date in above routine
; return ( 10*((n>>4)&0x0f) + (n&0x0f);

bcd2bin: push	de
	ld	d,a
	and	0f0h
	ld	e,a
	ld	a,d
	and	0fh
	srl	e
	add	a,e
	srl	e
	srl	e
	add	a,e
	pop	de
	ret
 .endif

;/*-----------------------------------------------------*/
;/* Send a single character to the Printer port, waiting if
;   it is not ready.  The output characters are not masked
;   by this routine so it is possible to send 8-bit data.
; */

	rseg	RCODE

;lpout(c)
;char c;
;{
;    while (!ptrrdy) ;

_lpout:	ld	a,(ptrrdy)	; get Parallel port status
	or	a		; Ready?
	jr	z,_lpout	; ..loop if Not

;    ptrrdy = 0;

	xor	a		; Else set busy
	ld	(ptrrdy),a

;    out (c,0x42);

	pop	hl		; Get Return Addr
	pop	de		;  minor #
	pop	bc		;   and character
	push	bc		;    (keeping stack balanced)
	push	de
	ld	a,c
	out	(42h),a		; send to data latch

;    out (c,0x43);

	out	(43h),a		;  toggle strobe

;    out (c,0x42);

	out	(42h),a		;   clear strobe
	jp	(hl)		;  ..return
;}

;/************** Miscellaneous Support Routines ***************/
;.....
; Set up Temporary System Stack to use during context switches
;  (Area reserved at end of this module)

_tempstack:
	pop	hl		; Fetch Return Address
	ld	sp,tstk+tmplen	;  Set temp Stack to top of scratch area
	jp	(hl)		; .."return"


;-------------------------------------------------------------
; Unix System CALL Trap Routine.  This entry point is reached when a
; process requests Unix Kernel services with a RST 30H instruction.

_unix:	di			; No Ints while we change Banks & Stack
	ex	af,af'
	push	af
 .if 0
 ld a,'%'
 call abyte
 .endif
	ex	af,af'
	exx
	push	bc
	push	de
	push	hl
	exx
	push	af
	push	bc
	push	de
	push	hl
	push	ix
	push	iy

 .if 1 ; Nick
	LD	(_ub+ORVAL1),bc	; in case we are NOT returning long in bc:hl
 .endif

;    udata.u_retloc = (char *)ugetw (udata.u_sp);        /* uret */

	ld	(_ub+ORET),sp	; Save return Stack Ptr for Task
	ld	(_ub+OSP),sp	; Nick... allows _brk() to check for error

 .if 1 ; latest unix() calling convention for better UZIX compatibility
	ld	a,e		; a = callno (1st argument)
 .else
 .if 0 ; for Nick's version the syscall index is passed in a (how convenient)
	LD	HL,+22		; (Fudged balue to index Formal Parms)
	ADD	HL,SP		; Pt to Return Address + 1 Word
	LD	A,(HL)		;  fetch Function CALL Number
 .endif
 .endif
	LD	(_ub+OCALL),A	;   save

;                        /* Copy args from user data space */
;    udata.u_argn3 = ugetw (udata.u_sp+4);        /* argn3 */
;    udata.u_argn2 = ugetw (udata.u_sp+3);        /* argn2 */
;    udata.u_argn1 = ugetw (udata.u_sp+2);        /* argn1 */
;    udata.u_argn  = ugetw (udata.u_sp+1);        /* argn  */

 .if 1 ; latest unix() calling convention for better UZIX compatibility
	ld	l,c
	ld	h,b		; hl -> stack frame (2nd argument)
 .else
 .if 1
	ld	hl,24		; points to return address + 2 word (pushed DE)
	add	hl,sp
 .else
	INC	HL		; Advance to argn
	INC	HL
 .endif
 .endif

	LD	BC,8		;  move 4 Words
	LD	DE,_ub+OARGN	;   to _ub+u_argn
	LDIR			;    (from unix2 in process.c)

	LD	SP,ISTACK ;KSTACK	; Set High Stack for Kernel Processing

	LD	A,(_osBank)	; Get OS Bank Number
	OUT0	(CBR),A		;  Bring OS bank into Context

 .if 1 ; IAR
	ld	hl,LWRD _unix2
	ld	a,BYTE3 _unix2
	call	?BANK_CALL_DIRECT_L08
 .else
	call	_unix2		; Continue processing
 .endif

	; Shove return values where they will eventually get popped to regs

	DI			; No Ints while we change Banks & Stack

	LD	A,(_ub+OPAGE)	; Get Process' Bank #

	ld	sp,(_ub+ORET)	; Restore Stack Ptr value for User Return
;	ld	sp,(_ub+OSP)	; Nick... as ORET was removed to save space

	OUT0	(CBR),A		;  Bring it into Context
 .if 0
 ld a,'#'
 call abyte
 .endif

	POP	IY		; Start to restore status
	POP	IX
	INC	SP		;  Skip HL restore
	INC	SP		;   (value already in HL)
	POP	DE
	LD	BC,(_ub+OERR)	; Get Error result
	LD	A,B
	OR	C		;  Set NZ if Error
 .if 1 ; Nick
	INC	SP
	INC	SP		; skip bc restoration (temporary !!)
	LD	BC,(_ub+ORVAL1)	; in case we are returning long in bc:hl
 .else
	POP	BC
 .endif
	INC	SP		;   (skip AF restoration)
	INC	SP
	JR	Z,_IRet0	;  ..jump if No Error, Carry Clear
	SCF			; Else set Carry for Error
	LD	HL,(_ub+OERR)	;  Return Error # instead of RetVal
	JR	_IRet0		; ..and jump to restore rest & Return

			;..fall thru to exit..
;-----------------------------------------------------------
; This is the common interrupt-scall-trap return routine.
; It restores all registers.

_IRET:	pop	iy
	pop	ix
	pop	hl
	pop	de
	pop	bc
	pop	af
_IRet0:	exx			; Enter here for Unix2 Return
	pop	hl
	pop	de
	pop	bc
	exx
	ex	af,af'
	pop	af
	ex	af,af'
; ex (sp),hl
; push af
; ld a,'['
; call abyte
; call ahexw
; ld a,']'
; call abyte
; pop af
; ex (sp),hl
	ei			; ..Return to User allowing Ints Now
	ret


;/*-----------------------------------------------------*/
; extern int unix();
; doexec ((int *)envp, int entry)
; {

	rseg	CODE

_doexec: di			; Stop Ints here
 .if 1 ; IAR
	;push	bc		; don't bother, we're throwing away our stack
	;push	de		; don't bother, we're throwing away our stack

	; careful - de and bc are used again below, don't corrupt them !!
 .endif

 .if 0 ; Nick has temporarily removed CP/M support
	LD	A,(_uzicom)
	OR	A		; Is this an UZI App?
	JR	NZ,ExeUZI	; ..jump if Yes

	LD	HL,__Hcpm22	; Else position Emulator
	LD	DE,__Lcpm22
	OR	A
	SBC	HL,DE		; Compute Length
	INC	HL		;  (+1 for safety)
	PUSH	HL		; Pass
	LD	HL,EStart	;   (EA00)
	PUSH	HL		;  Dest in proc
	LD	HL,__Bcpm22+100H ;    (correct for TPA offset)
	PUSH	HL		;   Src in System
	CALL	_uput		;    Get module
	POP	BC
	POP	BC
	POP	BC

ExeUZI:
 .endif
 .if 1 ; IAR
	ex	de,hl		; hl = 1st parameter (passed in de)
 .else
	pop	hl		;  Remove Return Address
	pop	hl		;   Ptr to Argv[] now on top-of-stack
 .endif

	LD	A,(_ub+OPAGE)	; Fetch Task bank #
	OUT0	(CBR),A		;  Bring task bank into Context

	LD	SP,HL		; Initialize User Stack below params

	xor	a		;  show that we are in User Mode
	ld	(_ub+OSYS),a	;   by clearing OS flag

	ld	a,0c3h		; Set Restart Vector for SYStem Calls
	ld	(0030h),a	;  (RST 30H is Unix Function call vector)
	ld	hl,_unix
	ld	(0031h),hl
	ld	(0000h),a	; Set vector for Illegal Instructions
	ld	hl,TrapEr	;   to Our Trap Handler
	ld	(0001h),hl

 .if 0 ; Nick has temporarily removed CP/M support
	; At this point, we check on the type of executable loaded.  If it
	; is a CP/M type (No "UZI" string detected), then load the CP/M
	; emulator module in the process' memory and execute there.

	LD	A,(_uzicom)
	OR	A		; Uzi Executable?
	JR	Z,ExeCPM	; ..jump if No
 .endif

 .if 1 ; Nick EXE format
; ld l,c
; ld h,b
; call ahexw
	push	bc
	ei			; Allow Ints again
	ret
 .else
	ei			; Allow Ints again
	jp	PROGBASE ;0100H	; ..begin UZI task execution
 .endif

 .if 0 ; Nick has temporarily removed CP/M support
ExeCPM:	EI
	JP	EStart		; Execute Bios Cold Boot
 .endif
; }

;/*-----------------------------------------------------*/
; /* Get a string from User to kernel Space up to count chars
;  * in length.  Addresses > Common base fetch from Common.
;  * This module MUST be in Common memory since it transfers
;  * a character-at-a-time by switching banks in context.
;  * Function called as: ugets (SourceAdr, DestAdr, MaxLength).
;  * Exit: HL = 0 if Ok, <>0 (hopefully) if string too long.
;  */
; ugets (char *uptr, char *sptr, unsigned count);

_ugets:
 .if 1 ; IAR
	ld	hl,4		; assumes banked memory model (bbr on stack)
	add	hl,sp

	push	bc
	push	de
	push	iy

	push	bc
	pop	iy		; destination to iy (2nd argument)

	ld	c,(hl)
	inc	hl
	ld	b,(hl)		; count to bc (3rd argument)

	ex	de,hl		; source to hl (1st argument)
 .else
	ld	hl,+7
	add	hl,sp		; Pt to Hi-byte of count
	push	iy
	ld	b,(hl)
	dec	hl
	ld	c,(hl)		; Get count to BC
	dec	hl
	ld	d,(hl)
	dec	hl
	ld	e,(hl)		;  Destination to DE (always in kernel space)
	push	de
	pop	iy		;    (transfer to iy)
	dec	hl
	ld	a,(hl)
	dec	hl
	ld	l,(hl)
	ld	h,a		;   Source to HL
 .endif
	ld	a,(_osBank)	;   (assume Yes by loading kernel base)
	ld	e,a		;     set dest bank to kernel
	ld	a,(_ub+OPAGE)	; Else load User Bank as source
	ld	d,a		;   save source bank
	LD	A,R
	PUSH	AF		;    (save Interrupt Flag as B2)
	IN0	A,(CBR)		; Get Current Bank
	push	af		;   save for exit

	di			; No Ints while we transfer the string
l0:	OUT0	(CBR),D		; Switch to User Bank
	ld	a,(hl)		;  Get a byte
	inc	hl		;   bump ptr
	OUT0	(CBR),E		; Switch to kernel Bank
	ld	(iy+0),a	;  save byte
	inc	iy		;   bump ptr
	or	a		; End?
	jr	z,l1 ;1f	; ..jump to exit if Yes

	dec	bc		; Dec max count
	ld	a,b
	or	c		; Destination full?
	jr	nz,l0 ;0b	; .loop if Not
	dec	iy		; Else back up destination
	ld	(iy+0),0	;  Terminate dest string
	jr	l2 ;2f		; ..exit w/hl hopefully non-zero

l1:	ld	hl,0		; Return 0 if Ok
l2:	pop	af
	OUT0	(CBR),A		; Bring Entry Bank back in context
	POP	AF		;  Restore Int Flag in B2
	POP	IY		;   and Regs
	AND	04H		; Were Ints ON at Entry?
 .if 1 ; IAR
	jr	z,$+3
	ei
;	push	hl
;	ld	hl,LWRD _ei
;	ld	a,BYTE3 _ei
;	call	nz,?BANK_CALL_DIRECT_L08
;	pop	hl

	pop	de
	pop	bc
	jp	?BANK_FAST_LEAVE_L08
 .else
	RET	Z		; ..exit if No, Ints stay Off
	EI			; Else Ints On
	ret
 .endif


;/*-----------------------------------------------------*/
; Move block of data from System to User Space
;   uput (char *sptr, char *uptr, unsigned nbytes)

_uput:
 .if 1 ; IAR
	ld	hl,4		; assumes banked memory model (bbr on stack)
	add	hl,sp

	push	bc
	push	de
	push	iy

	ld	a,(hl)
	inc	hl
	ld	h,(hl)
	ld	l,a
; call ahexw
 .else
	pop	hl		; Ret Addr
	pop	de		; Source (*sptr)
	pop	bc		;  Dest (*uptr)
	ex	(sp),hl		;   Count to HL (nbytes)
 .endif
	LD	A,R
	PUSH	AF		;    (Save Int Flag in B2)
	push	hl		;     Save Count
	ld	a,(_ub+OPAGE)	; Get Dest Bank
	ld	l,a		;  (dest = LBC)
	ld	a,(_osBank)	;   System Bank (Source)
	JR	_uget0		;  ..continue in common code below

;/*-----------------------------------------------------*/
; Move block of data from User to System space.
;   uget (char *uptr, char *sptr, unsigned nbytes);

_uget:
 .if 1 ; IAR
	ld	hl,4		; assumes banked memory model (bbr on stack)
	add	hl,sp

	push	bc
	push	de
	push	iy

	ld	a,(hl)
	inc	hl
	ld	h,(hl)
	ld	l,a
 .else
	pop	hl		; Ret Addr
	pop	de		; Source (*uptr)
	pop	bc		;  Dest (*sptr)
	ex	(sp),hl		;   Count to HL (nbytes)
 .endif
	LD	A,R
	PUSH	AF		;    (Save Int Flag in B2)
	push	hl		;     Save Count
	ld	a,(_osBank)	; Get Dest Bank
	ld	l,a		;  (dest = LBC)
	ld	a,(_ub+OPAGE)	;   User's Bank (Source)
_uget0:	ld	h,a		;    (source = HDE)
; call ahexw
 .if 1
; push hl
; ld l,c
; ld h,b
; call ahexw
; pop hl
; ld a,'x'
; call abyte
	bit	7,b
	jr	nz,$+4		; 0 <= bc < 8000h ?
	ld	l,0		; yes, it's relative to CA0 (0), not CA1 (CBR)

; ex de,hl
; call ahexw
; ex de,hl
; ld a,'y'
; call abyte
	bit	7,d
	jr	nz,$+4		; 0 <= de < 8000h ?
	ld	h,0		; yes, it's relative to CA0 (0), not CA1 (CBR)
; call ahexw
; ld a,'z'
; call abyte
 .endif
 .if 1
	ld	a,e
	ld	e,d		; Shuffle to deal with high 16-bits
	ld	d,0		;   and Addr to 16-bits

	push	hl
	ld	l,h		; so that l = source bank #

	ld	h,d ;0		; Expand Source Bank
	add	hl,hl		; Source Bank #
	add	hl,hl		;  * 16
	add	hl,hl
	add	hl,hl
	add	hl,de		;   + Hi 8-bits of addr

	ld	d,l
	ld	e,a		; h:de -> source data
	push	de		; save value for iy
	pop	iy		; h:iy -> source data
	ld	d,h		; d:iy -> source data

	ld	a,c
	ld	c,b		; Shuffle to deal with high 16-bits
	ld	b,0		;   and Addr to 16-bits

	pop	hl		; so that l = source bank #

	ld	h,b ;0		; Expand Dest Bank
	add	hl,hl		; Dest Bank #
	add	hl,hl		;  * 16
	add	hl,hl
	add	hl,hl
	add	hl,bc		;   + Hi 8-bits of Addr

	ld	e,h
	ld	h,l
	ld	l,a		; e:hl -> destination spot

	pop	bc		; bc = count
	call	copyr		; go and copy it, in small blocks !!
 .else
	di			; NO INTERRUPTS while we play around here!
	OUT0	(SAR0L),E	; Set Source LSB
	OUT0	(SAR0L+3),C	;   and Dest LSB
	PUSH	HL		; Save both bank CBR values
	LD	H,0
	ADD	HL,HL		;  while we massage Dest Bank #
	ADD	HL,HL
	ADD	HL,HL
	ADD	HL,HL
	LD	C,B		; Position Dest (MSB)
	LD	B,0
	ADD	HL,BC		;  Add processed Bank to Dest Addr MSB
	OUT0	(SAR0L+4),L	;   Set Dest MSB
	OUT0	(SAR0L+5),H	;    and Bank
	POP	HL		; Restore Bank CBR values
	LD	L,H
	LD	H,0		; position
	ADD	HL,HL		;  and massage Source Bank #
	ADD	HL,HL
	ADD	HL,HL
	ADD	HL,HL
	LD	E,D		; Position Source (MSB)
	LD	D,0
	ADD	HL,DE		;  Add processed Bank to Source Addr MSB
	OUT0	(SAR0L+1),L	;   Set Source MSB
	OUT0	(SAR0L+2),H	;    and Bank

	POP	HL		; Restore Count
	ld	a,h
	or	l		; Anything to move?
	jr	z,ugetX		; ..quit here if Not

	OUT0	(SAR0L+6),L	;   Set Count LSB
	OUT0	(SAR0L+7),H	;    and MSB

	LD	A,00000010B	; Set DMA Mode control to Burst Mode
	OUT0	(DMODE),A
	LD	A,40H		; Enable DMA0
	OUT0	(DSTAT),A	;   and move the block

ugetX:
 .endif
	POP	AF		; Restore Int Flag
 .if 1
	pop	iy
 .endif
 .if 0 ; IAR
	EX	(SP),HL		; Ret Addr to HL, junk to stack
	PUSH	HL		;  param
	PUSH	HL		;   space
 .endif
	AND	04H		; Were Ints ON at entry?
 .if 1 ; IAR
	jr	z,$+3
	ei
;	ld	hl,LWRD _ei
;	ld	a,BYTE3 _ei
;	call	nz,?BANK_CALL_DIRECT_L08

	pop	de
	pop	bc
	jp	?BANK_FAST_LEAVE_L08
 .else
	CALL	NZ,_ei		;  If so, activate if Not in intr svc
	JP	(HL)		;  "return"
 .endif


;/*-----------------------------------------------------*/
; Get a Word (16-bits) of data from Address in User Space
;   ugetw (char *uptr) {
;       int w;
;
;       uget (uptr, &w, sizeof (int));
;       return (w);
;   }

_ugetw:
 .if 1 ; IAR
	push	bc
	push	de

	ex	de,hl		; argument to hl
 .else
	pop	de		; Ret Addr
	pop	hl		;  *uptr
	push	hl		;   (keep Stack constant)
	push	de
 .endif
	IN0	A,(CBR)		; Get Current Bank
	ld	e,a		;  (save for exit)
	LD	A,R
	LD	D,A		;    (save Int flag as B2)
	di			;     No INTS while we switch banks
	ld	a,(_ub+OPAGE)
	OUT0	(CBR),A		; Bring User Bank in context
	ld	a,(hl)		;  fetch Word (lo)
	inc	hl
	ld	h,(hl)		;   fetch (hi)
	ld	l,a		; Position return value
	jr	gpXit		; ..exit thru common code


;/*-----------------------------------------------------*/
; Put a Word (16-bits) of data to Address in User Space
;   uputw (int w, char *uptr) {
;       uput (&w, uptr, sizeof (int));
;   }

_uputw:
 .if 1 ; IAR
; ld a,'x'
; call abyte
	push	bc
	push	de

	ld	l,c
	ld	h,b		; 2nd argument to hl (uptr)
	ld	c,e
	ld	b,d		; 1st argument to bc (w)
 .else
	pop	de		; Ret Addr
	pop	bc		;  w
	pop	hl		;   *uptr
	push	hl		;    (keep Stack constant)
	push	bc
	push	de
 .endif
	IN0	A,(CBR)		; Get Current Bank
	ld	e,a		;  (save for exit)
	LD	A,R
	LD	D,A		;    (save Int flag as B2)
	di			;     No INTS while we switch banks
	ld	a,(_ub+OPAGE)
	OUT0	(CBR),A		; Bring User Bank in context
	ld	(hl),c
	inc	hl
	ld	(hl),b		;   store the word
gpXit:	OUT0	(CBR),E		; Restore Entry Bank to context
	BIT	2,D		; Were Ints ON at entry?
 .if 1 ; IAR
; ld a,'y'
; call abyte
	jr	z,$+3
	ei
;	push	hl
;	ld	hl,LWRD _ei
;	ld	a,BYTE3 _ei
;	call	nz,?BANK_CALL_DIRECT_L08
;	pop	hl
; ld a,'z'
; call abyte

	pop	de
	pop	bc
	jp	?BANK_FAST_LEAVE_L08
 .else
	JR	NZ,_ei		; ..activate Ints if On and Not in intr svc
	ret			;   quit
 .endif


;/*-----------------------------------------------------*/
; Get a Byte of data from Address in User Space
;   ugetc (char *uptr) {
;       char c;
;
;       uget (uptr, &c, 1);
;       return (c);
;   }

_ugetc:
 .if 1 ; IAR
	push	bc
	push	de

	ex	de,hl		; argument to hl
 .else
	pop	de		; Ret Addr
	pop	hl		;  &w
	push	hl		;   (keep Stack constant)
	push	de
 .endif
	IN0	A,(CBR)		; Get Current Bank
	ld	e,a		;  (save for exit)
	LD	A,R
	LD	D,A		;    (save Int flag as B2)
	di			;     No INTS while we switch banks
	ld	a,(_ub+OPAGE)
	OUT0	(CBR),A		; Bring User Bank in context
	ld	l,(hl)
	ld	h,0
	jr	gpXit		; ..exit thru common code


;/*-----------------------------------------------------*/
; Put a Byte of data to Address in User Space
;   uputc (int c, char *uptr) {
;       uput (&c, uptr, 1);
;   }

_uputc:
 .if 1 ; IAR
	push	bc
	push	de

	ld	l,c
	ld	h,b		; 2nd argument to hl (uptr)
	ld	c,e
	ld	b,d		; 1st argument to bc (w)
 .else
	pop	de		; Ret Addr
	pop	bc		;  c
	pop	hl		;   *uptr
	push	hl		;    (keep Stack constant)
	push	bc
	push	de
 .endif
	IN0	A,(CBR)		; Get Current Bank
	ld	e,a		;  (save for exit)
	LD	A,R
	LD	D,A		;    (save Int flag as B2)
	di			;     No INTS while we switch banks
	ld	a,(_ub+OPAGE)
	OUT0	(CBR),A		; Bring User Bank in context
	ld	(hl),c		;  store the byte
	jr	gpXit		; ..exit thru common code


;;/*-----------------------------------------------------*/
;;/* Disable interrupts */
;;di()
;;{
;;#asm
;_di:
;; ld a,'-'
;; call abyte
;	di
;;#endasm
; .if 1 ; IAR
;	jp	?BANK_FAST_LEAVE_L08
; .else
;	ret
; .endif
;;}
;
;;/*-----------------------------------------------------*/
;;/* Enable interrupts if we are not in service routine */
;;ei()
;;{
;;    if (inint)
;
;_ei:
;; ld a,'+'
;; call abyte
;	ld	a,(_inint)
;	or	a
;
;;        return;
;
; .if 1 ; IAR
;	jp	z,?BANK_FAST_LEAVE_L08
; .else
;	ret	z		; ..in Service routine, leave disabled
; .endif
;_ei_absolute:			; another entry point to save space (see below)
;	ei			; else enable ints
;;}
; .if 1 ; IAR
;	jp	?BANK_FAST_LEAVE_L08
; .else
;	ret
; .endif

;/*--------------------------------------------------------------------*/
;/* Disable interrupts regardless of whether we are in service routine */
;di_absolute()
;{
;#asm
_di_absolute:
	di
;#endasm
 .if 1 ; IAR
	jp	?BANK_FAST_LEAVE_L08
 .else
	ret
 .endif
;}

;/* Enable interrupts regardless of whether we are in service routine */
;ei_absolute()
;{
;#asm
_ei_absolute:
	ei
;#endasm
 .if 1 ; IAR
	jp	?BANK_FAST_LEAVE_L08
 .else
	ret
 .endif
;}

;/* Placeholder routines to increment/decrement kernel locking count */

_di:
	ld	hl,_unix_locked_out
	inc	(hl)
 .if 1 ; IAR
	jp	?BANK_FAST_LEAVE_L08
 .else
	ret
 .endif

_ei:
	ld	hl,_unix_locked_out
	dec	(hl)

	; if it's zero, do something (wake all tasks waiting on lock??)

 .if 1 ; IAR
	jp	?BANK_FAST_LEAVE_L08
 .else
	ret
 .endif

;***************************************************************
;		     D A T A    A R E A
;***************************************************************

		rseg	RCODE

_osBank: 	defb	0		; CBR Register value for Kernel data

 .if 0 ; Nick has temporarily removed CP/M support
_uzicom: 	defw	1		; Executable Type (0 = CP/M, 1 = UZI)
 .endif

ptrrdy:		defb	0		; Ptr Ready Flag (for Int Ack)

_timstk:	defs	14		; static char *timstk[14];
					; /* Clock stack */
_lcltim:	defs	7		; static char *lcltim[7];
					; /* Raw Clock buff */

tmplen		equ	200h ; Nick ;100		; Length of Temp Stack
tstk:		defs	tmplen		; Temp Stack Space in Hi-Memory

 .if 0 ; Nick
endcom:
 .endif

; -----------------------------------------------------------------------------

	END
