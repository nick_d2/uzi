; Z180 Register Mnemonics

CNTLA0	EQU	00H		; ASCI Control Reg A Ch 0
CNTLA1	EQU	01H		; ASCI Control Reg A Ch 1
CNTLB0	EQU	02H		; ASCI Control Reg B Ch 0
CNTLB1	EQU	03H		; ASCI Control Reg B Ch 1
STAT0	EQU	04H		; ASCI Status Reg Ch 0
STAT1	EQU	05H		; ASCI Status Reg Ch 1
TDR0	EQU	06H		; ASCI Tx Data Reg Ch 0
TDR1	EQU	07H		; ASCI Tx Data Reg Ch 1
RDR0	EQU	08H		; ASCI Rx Data Reg Ch 0
RDR1	EQU	09H		; ASCI Rx Data Reg Ch 1

CNTR	EQU	0AH		; CSI/O Control Reg
TRDR	EQU	0DH		; CSI/O Tx/Rx Data Reg

TMDR0L	EQU	0CH		; Timer Data Reg Ch0-Low
TMDR0H	EQU	0DH		; Timer Data Reg Ch0-High
RLDR0L	EQU	0EH		; Timer Reload Reg Ch0-Low
RLDR0H	EQU	0FH		; Timer Reload Reg Ch0-High
TCR	EQU	10H		; Timer Control Reg
TMDR1L	EQU	14H		; Timer Data Reg Ch1-Low
TMDR1H	EQU	15H		; Timer Data Reg Ch1-High
RLDR1L	EQU	16H		; Timer Reload Reg Ch1-Low
RLDR1H	EQU	17H		; Timer Reload Reg Ch1-High
FRC	EQU	18H		; Free-Running Counter

CCR	EQU	1FH		; CPU Control Reg (Z8S180 & higher Only)

SAR0L	EQU	20H		; DMA Source Addr Reg Ch0-Low
SAR0H	EQU	21H		; DMA Source Addr Reg Ch0-High
SAR0B	EQU	22H		; DMA Source Addr Reg Ch0-Bank
DAR0L	EQU	23H		; DMA Dest Addr Reg Ch0-Low
DAR0H	EQU	24H		; DMA Dest Addr Reg Ch0-High
DAR0B	EQU	25H		; DMA Dest ADDR REG CH0-Bank
BCR0L	EQU	26H		; DMA Byte Count Reg Ch0-Low
BCR0H	EQU	27H		; DMA Byte Count Reg Ch0-High
MAR1L	EQU	28H		; DMA Memory Addr Reg Ch1-Low
MAR1H	EQU	29H		; DMA Memory Addr Reg Ch1-High
MAR1B	EQU	2AH		; DMA Memory Addr Reg Ch1-Bank
IAR1L	EQU	2BH		; DMA I/O Addr Reg Ch1-Low
IAR1H	EQU	2CH		; DMA I/O Addr Reg Ch2-High
BCR1L	EQU	2EH		; DMA Byte Count Reg Ch1-Low
BCR1H	EQU	2FH		; DMA Byte Count Reg Ch1-High
DSTAT	EQU	30H		; DMA Status Reg
DMODE	EQU	31H		; DMA Mode Reg
DCNTL	EQU	32H		; DMA/Wait Control Reg

IL	EQU	33H		; INT Vector Low Reg
ITC	EQU	34H		; INT/TRAP Control Reg
RCR	EQU	36H		; Refresh Control Reg
CBR	EQU	38H		; MMU Common Base Reg
BBR	EQU	39H		; MMU Bank Base Reg
CBAR	EQU	3AH		; MMU COmmon/Bank Area Reg
OMCR	EQU	3EH		; Operation Mode Control Reg
ICR	EQU	3FH		; I/O Control Reg

; Z182 Additional Registers (uncomment to define)

;BRK0	EQU	12H		; Break Control Reg Ch 0
;BRK1	EQU	13H		; Break Control Reg Ch 1

;INTTYPE EQU	0DFH		; Interrupt Edge/Pin MUX Reg
;WSGCS	EQU	0D8H		; Wait-State Generator Chip Select
;ENH182	EQU	0D9H		; Z80182 Enhancements Reg
;PINMUX	EQU	0DFH		; Interrupt Edge/Pin MUX Reg
;RAMUBR	EQU	0E6H		; RAM End Boundary
;RAMLBR	EQU	0E7H		; RAM Start Boundary
;ROMBR	EQU	0E8H		; ROM Boundary
;FIFOCTL EQU	0E9H		; FIFO Control Reg
;RTOTC	EQU	0EAH		; Rx Time-Out Time Constant
;TTOTC	EQU	0EBH		; Tx Time-Out Time Constant
;FCR	EQU	0ECH		; FIFO Register
;SCR	EQU	0EFH		; System Pin Control
; (MIMIC Registers occupy 0F0-0FFH if used)

;DDRA	EQU	0EDH		; PIO Direction Reg Port A
;DRA	EQU	0EEH		; PIO Data Port A
;DDRB	EQU	0E4H		; PIO Direction Reg Port B
;DRB	EQU	0E5H		; PIO Data Port B
;DDRC	EQU	0DDH		; PIO Direction Reg Port C
;DRC	EQU	0DEH		; PIO Data Port C

;SCCACNT EQU	0E0H		; ESCC Control Channel A
;SCCAD	EQU	0E1H		; ESCC Data Channel A
;SCCBCNT EQU	0E2H		; ESCC Control Channel B
;SCCBD	EQU	0E3H		; ESCC Data Channel B

