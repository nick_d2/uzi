/* scall2.h for UZI180 by Nick (we are converting everything to ANSI C) */

#ifndef __SCALL2_H
#define __SCALL2_H

ptptr findprocess(int pid);
void filldesc(uchar oft, uchar access, inoptr ino);
int sys_pipe(void);
int sys_stime(void);
int sys_times(void);
int sys_brk(void);
int sys_sbrk(void);
int sys_waitpid(void);
int sys__exit(void);
int sys_fork(void);
int sys_pause(void);
int sys_signal(void);
int sys_kill(void);
int sys_alarm(void);
int sys_reboot(void);
int sys_getset(void);
int sys_getset(void);

#endif /* __SCALL2_H */

