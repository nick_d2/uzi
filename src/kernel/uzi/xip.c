/*
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 Filesystem related routines for XIP system
**********************************************************/

#define NEED__DEVIO
#define NEED__FILESYS
#define NEED__MACHDEP
#define NEED__PROCESS
#define NEED__SCALL

#include "vendor.h" /* Nick, must come first */
#include <unix.h>
#include <config.h>
#include <extern.h>
#include "devio.h" /* prototypes added by Nick */
#include "filesys.h" /* prototypes added by Nick */
#include "xip.h" /* prototypes added by Nick */

#if 0
#undef DEBUG
#define DEBUG 3 /* TEMPORARY! */
#endif

#ifdef UTIL /* rather a hack indeed */
#define	EISALIGN	42		/* 42 File is already aligned */
#define	ENOALIGN	43		/* 43 File is not aligned */
#define ETOOSHORT	44		/* 44 File is too short, or hole */
#define ENOREGALIGN	45		/* 42 Not a regular or aligned file */
#define	ENOLCKFS	46		/* 45 Not a locking filesystem */
#define ECORRUPTFS	47		/* 46 Filesystem is corrupt */
#endif

int int_min(int, int);

/* Returns true if the magic number of a superblock is ok ??? not used */
#define gooddev(dev)		((dev)->s_mounted == SMOUNTED)

#define blockinodes(blk)	((blk) << DINODESPERBLOCKLOG)
#define devinodes(dev)		blockinodes((dev)->s_isize)
#define inodeblock(dev,ino)	(((ino) >> DINODESPERBLOCKLOG) + (dev)->s_reserv)
#define inodeoffset(ino)	((ino) & DINODESPERBLOCKMASK)

int bitmap_align(inoptr ino, off_t size)
	{
	blkno_t i, j, k;
	blkno_t blk, pos, /*inc,*/ newno;
	blkno_t *region, regions, blocks;
	fsptr dev;
	dev_t devno;
	inoptr other;
	int flag, exclude, indirection;

#if DEBUG >= 3
 dprintf(3, "bitmap_align(%u, %ld) starting\n", ino - i_tab, size);
#endif

/* return 0; */

	/* prepare to access the given inode */
	devno = ino->c_dev;
	dev = getfs(devno);
	if (dev->s_bitmap_immov >= dev->s_bitmap_final)
		{
		udata.u_error = ENOLCKFS; /* not a locking filesystem */
#if DEBUG >= 3
 dprintf(3, "bitmap_align() returning -1 a\n");
#endif
		return -1;
		}

	/* validate the filetype, it must be non-aligned */
	if ((ino->c_node.i_mode & S_IFMT) == S_IFALIGN)
		{
		udata.u_error = EISALIGN; /* file is already aligned */
		return -1;
		}
	if ((ino->c_node.i_mode & S_IFMT) != S_IFREG)
		{
		udata.u_error = ENOREGALIGN; /* file not regular or aligned */
#if DEBUG >= 3
 dprintf(3, "bitmap_ualign() returning -1 b\n");
#endif
		return -1;
		}

	/* ensure the region list will fit in 512 bytes */
	regions = (size + REGION_BYTES - 1) >> REGION_LOG;
	if (regions > (BUFSIZE / sizeof(blkno_t)))
		{
		udata.u_error = ENOSPC; /* size specified is too large */
#if DEBUG >= 3
 dprintf(3, "bitmap_align() returning -1 c\n");
#endif
		return -1;
		}

	/* ready to allocate the region list */
	region = (blkno_t *)tmpbuf();

	/* calculate size in blocks, to resolve size of last region */
	blocks = (size + BUFSIZE - 1) >> BUFSIZELOG;

	/* allocate each region its physical memory address */
	pos = 0;
	for (i = 0; i < regions; i++)
		{
		/* is this a full region, or the last partial region? */
		k = int_min(blocks - pos, REGION_BLOCKS);

		/* try to find contiguous aligned non-reserved blocks */
		j = bitmap_search(devno, k, dev->s_bitmap_immov,
					    dev->s_bitmap_final);
		if (j == (blkno_t)-1)
			{
			/* there was not enough aligned space available */
			udata.u_error = ENOSPC;

			regions = i;
			goto bitmap_align_error;
			}

		/* found a suitable spot for the region, snarf it */
		region[i] = j;
		if (bitmap_reserve(devno, j, k, 1, dev->s_bitmap_immov,
						   dev->s_bitmap_final))
			{
			/* disk i/o error, or corrupt filesystem */
			/* udata.u_error = ENXIO */;

			regions = i;
			goto bitmap_align_error;
			}

		/* calculate file position for start of next region */
		pos += REGION_BLOCKS;
		}

#if DEBUG > 1
 dprintf(2, "processing my inode %d\n", ino->c_num);
#endif

	/* move the file's data into unoccupied blocks of each region */
	pos = 0;
	for (i = 0; i < regions; i++)
		{
		/* is this a full region, or the last partial region? */
		k = int_min(blocks - pos, REGION_BLOCKS);

		/* loop through each block of the region individually */
		blk = region[i];
		for (j = 0; j < k; j++)
			{
			/* see if the target spot for this block is occupied */
			flag = bitmap_set(devno, blk, 1, dev->s_bitmap_block,
							 dev->s_bitmap_immov);
			if (flag == -1)
				{
				/* disk i/o error, or corrupt filesystem */
				/* udata.u_error = ENXIO; */

				goto bitmap_align_error;
				}

			if (flag == 0)
				{
				/* no, move something in and possibly repeat */
				newno = bitmap_align_chase(dev, ino, blk, pos,
					region, regions, blocks);
				if (newno == (blkno_t)-1)
					{
					/* i/o error, or corrupt filesystem */
					/* udata.u_error = ENXIO; */

					goto bitmap_align_error;
					}

				/* eventually a non-target block was freed */
				if (bitmap_set(devno, newno, 0,
						dev->s_bitmap_block,
						dev->s_bitmap_immov) != 1)
					{
					/* disk error, or corrupt filesystem */
					/* udata.u_error = ENXIO; */

					goto bitmap_align_error;
					}
				}

			blk++;
			pos++;
			}
		}

	/* now, if any blocks remain to be swapped, traverse the entire */
	/* filesystem to find them, updating the filesystem as we move them */
	k = devinodes(dev);
	for (j = ROOTINODE; j < k; j++)
		{
		/* scan inode bitmap via utility routine, looking for '1' */
		j = bitmap_find(devno, j, 1, 0, dev->s_bitmap_inode,
						dev->s_bitmap_block);
		if (j == (blkno_t)-1)
			{
			/* disk i/o error, or corrupt filesystem */
			/* udata.u_error = ENXIO; */

			goto bitmap_align_error;
			}

		if (j < ROOTINODE || j >= k)
			{
			break; /* not found, means no more inodes to search */
			}

		other = i_open(devno, j);
		if (other == NULLINO)
			{
			/* disk i/o error, or corrupt filesystem */
			/* udata.u_error = ENXIO; */

			goto bitmap_align_error;
			}

#if DEBUG > 1
 dprintf(2, "processing other inode %d\n", other->c_num);
#endif
#if 0 /* don't bother, since the S_IFMT bits will be invalid anyway */
		if (other->c_node.i_mode == 0)
			{
			i_deref(other);
			continue; /* inode bitmap is bad, try to continue */
			}
#endif

		/* check mode */
		flag = other->c_node.i_mode & S_IFMT;
		if (flag != S_IFALIGN &&
		    flag != S_IFREG &&
		    flag != S_IFDIR &&
		    flag != S_IFLNK &&
		    flag != S_IFBLK &&
		    flag != S_IFCHR)
			{
			i_deref(other);
			continue; /* inode's mode is bad, skip it, continue */
			}

		/* check blocks */
		if (flag == S_IFALIGN || flag == S_IFREG ||
				flag == S_IFDIR || flag == S_IFLNK)
			{
			/*pos = 0;*/
			/*inc = 1;*/
			indirection = 0;
			exclude = (ino == other) ? 2 : (flag == S_IFALIGN);
#if 1 /* under construction */
			for (i = ((exclude == 1) ? DIRECTBLOCKS : 0);
#else
			for (i = (exclude ? DIRECTBLOCKS : 0);
#endif
					i <= DIRECTBLOCKS+INDIRECTBLOCKS; i++)
				{
				if (i == DIRECTBLOCKS)
					{
					/*inc = BUFSIZE / sizeof(blkno_t);*/
					indirection = 1;
					}
				else if (i == DIRECTBLOCKS+INDIRECTBLOCKS)
					{
					/* inc doesn't matter in this case */
					indirection = 2;
					}
				blk = other->c_node.i_addr[i];
/* if (blk == 3840) dprintf(3, "bad @ %d,%d 0x%04x\n", other->c_num, i, &other->c_node.i_addr[i]); */
				if (blk)
					{
					pos = bitmap_align_recurse(dev, /*pos,*/
						ino, exclude,
						&other->c_node.i_addr[i],
						&other->c_dirty, indirection,
						region, regions, blocks);

					/* move something into the freed spot, and possibly repeat */
					if (pos != (blkno_t)-1)
						{
						blk = bitmap_align_chase(
							dev, ino,
							blk, pos,
							region, regions, blocks);

						if (blk == (blkno_t)-1)
							{
							/* disk i/o error, or corrupt filesystem */
							/* udata.u_error = ENXIO; */

							break; /*return (blkno_t)-1;*/
							}

						/* eventually a non-target block was freed, indicate this */
						if (bitmap_set(devno, blk, 0,
							dev->s_bitmap_block,
							dev->s_bitmap_immov)
								!= 1)
							{
							/* disk i/o error, or corrupt filesystem */
							/* udata.u_error = ENXIO; */

							break; /*return (blkno_t)-1;*/ /* corrupt filesystem */
							}
						}
					}
				/*pos += inc;*/
				}
			}

		i_deref(other);
		}

	if (brelse((bufptr)region) < 0)
		{
		/* udata.u_error = something */

bitmap_align_error:
		/* politely unreserve all regions reserved so far */
		for (i = 0; i < regions; i++)
			{
			/* in this case always unreserve a full sized region */
			bitmap_reserve(devno, region[i],
				       REGION_BLOCKS, 0,
				       dev->s_bitmap_immov,
				       dev->s_bitmap_final);
			}

#if DEBUG >= 3
 dprintf(3, "bitmap_align() returning -1 d\n");
#endif
		return -1;
		}

	ino->c_node.i_mode = (ino->c_node.i_mode & ~S_IFMT) | S_IFALIGN;
	ino->c_dirty = 1;
	i_sync();	/* flushing inodes */

#if DEBUG >= 3
 dprintf(3, "bitmap_align() returning 0, success\n");
#endif
	return 0; /* successfully aligned the file */
	}

int bitmap_ualign(inoptr ino, off_t size)
	{
	blkno_t i, j, pos;
	blkno_t *region, regions, blocks;
	fsptr dev;
	dev_t devno;

#if DEBUG >= 3
 dprintf(3, "bitmap_ualign(%u, %ld) starting\n", ino - i_tab, size);
#endif

	/* prepare to access the given inode */
	devno = ino->c_dev;
	dev = getfs(devno);

	/* construct region list using subroutine shared with execve() */
	region = bitmap_examine(dev, ino, size, &regions, &blocks);
	if (region == NULL)
		{
#if DEBUG >= 3
 dprintf(3, "bitmap_ualign() returning -1 a\n");
#endif
		return -1; /* error code has been setup by bitmap_examine() */
		}

	/* unreserve all regions using the list */
	pos = 0;
	for (i = 0; i < regions; i++)
		{
		/* is this a full region, or the last partial region? */
		j = int_min(blocks - pos, REGION_BLOCKS);

		/* unreserve the required number of blocks for region */
		if (bitmap_reserve(devno, region[i], j, 0,
					  dev->s_bitmap_immov,
					  dev->s_bitmap_final))
			{
			/* disk i/o error, or corrupt filesystem */
			/* udata.u_error = ENXIO */;
#if DEBUG >= 3
 dprintf(3, "bitmap_ualign() returning -1 f\n");
#endif
			return -1;
			}

		pos += REGION_BLOCKS;
		}

	if (brelse((bufptr)region) < 0)
		{
		/* udata.u_error = something */
		}

	ino->c_node.i_mode = (ino->c_node.i_mode & ~S_IFMT) | S_IFREG;
	ino->c_dirty = 1;
	i_sync();	/* flushing inodes */

#if DEBUG >= 3
 dprintf(3, "bitmap_ualign() returning 0\n");
#endif
	return 0;
	}

blkno_t *bitmap_examine(fsptr dev, inoptr ino, off_t size,
				   blkno_t *regioncount, blkno_t *blockcount)
	{
	blkno_t i, j, k;
	blkno_t blk, pos, /*inc,*/ newno;
	blkno_t *region, regions, blocks;

#if DEBUG >= 3
 dprintf(3, "bitmap_examine(%d, %d, %ld, 0x%x, 0x%x) starting\n",
         dev - fs_tab, ino - i_tab, size, regioncount, blockcount);
#endif

	if (dev->s_bitmap_immov >= dev->s_bitmap_final)
		{
		udata.u_error = ENOLCKFS; /* not a locking filesystem */
#if DEBUG >= 3
 dprintf(3, "bitmap_examine() returning NULL, error %u\n", udata.u_error);
#endif
		return NULL;
		}

	/* validate the filetype, it must be aligned */
	if ((ino->c_node.i_mode & S_IFMT) == S_IFREG)
		{
		udata.u_error = ENOALIGN; /* file is not aligned */
#if DEBUG >= 3
 dprintf(3, "bitmap_examine() returning NULL, error %u\n", udata.u_error);
#endif
		return NULL;
		}
	if ((ino->c_node.i_mode & S_IFMT) != S_IFALIGN)
		{
		udata.u_error = ENOREGALIGN; /* file not regular or aligned */
#if DEBUG >= 3
 dprintf(3, "bitmap_examine() returning NULL, error %u\n", udata.u_error);
#endif
		return NULL;
		}

	/* ensure the region list will fit in 512 bytes */
	regions = (size + REGION_BYTES - 1) >> REGION_LOG;
	if (regions > (BUFSIZE / sizeof(blkno_t)))
		{
		udata.u_error = ENOSPC; /* size specified is too large */
#if DEBUG >= 3
 dprintf(3, "bitmap_examine() returning NULL, error %u\n", udata.u_error);
#endif
		return NULL;
		}

	/* ready to allocate the region list */
	region = (blkno_t *)tmpbuf();

	/* calculate size in blocks, to resolve size of last region */
	blocks = (size + BUFSIZE - 1) >> BUFSIZELOG;

	/* find each region's existing physical memory address */
	pos = 0;
	for (i = 0; i < regions; i++)
		{
		/* is this a full region, or the last partial region? */
		k = int_min(blocks - pos, REGION_BLOCKS);

		/* first block must be aligned, save its position in list */
		blk = bmap(ino, pos, 1);
		if ((blk & (PAGE_BLOCKS-1)) || blk == NULLBLK)
			{
			udata.u_error = ENOALIGN; /* alignment bad */
#if DEBUG >= 3
 dprintf(3, "bitmap_examine() returning NULL, error %u a\n", udata.u_error);
#endif
			return NULL;
			}
		region[i] = blk;
		pos++;

		/* try to find and skip contiguous aligned blocks */
		for (j = 1; j < k; j++)
			{
			blk++;
			newno = bmap(ino, pos, 1);
			if (newno != blk) /*|| newno == NULLBLK)*/
				{
				udata.u_error = ENOALIGN; /* alignment bad */
#if DEBUG >= 3
 dprintf(3, "bitmap_examine() returning NULL, error %u b\n", udata.u_error);
#endif
				return NULL;
				}
			pos++;
			}
		}

	*regioncount = regions;
	*blockcount = blocks;
#if DEBUG >= 3
 dprintf(3, "bitmap_examine() returning 0x%x (%d, %d)\n", region, regions, blocks);
#endif
	return region;
	}

blkno_t bitmap_align_chase(fsptr dev, inoptr ino, blkno_t blk, blkno_t pos,
				      blkno_t *region, blkno_t regions,
				      blkno_t blocks)
	{
	int i, occupied;
	bufptr buf, other;

#if DEBUG >= 3
 dprintf(3, "bitmap_align_chase(%d, %d, %d, %d, 0x%x, %d, %d) starting\n",
         dev - fs_tab, ino - i_tab, blk, pos, region, regions, blocks);
#endif

	/* check we have been passed a valid block number */
	if (blk < (dev->s_reserv + dev->s_isize) || blk >= dev->s_fsize)
		{
		udata.u_error = ECORRUPTFS;
#if DEBUG
 dprintf(1, "bitmap_align_chase() 1 ino = %d, blk = %d, pos = %d\n",
         ino->c_num, blk, pos);
#endif
		return (blkno_t)-1; /* filesystem is corrupt */
		}

	/* infinite loop to chase the just-freed block around the file */
	while (1)
		{
		/* find corresponding block in allocation table for ino */
		blk = bitmap_align_bmap(ino, blk, pos);
		if (blk == (blkno_t)-1)
			{
			/* udata.u_error = ETOOSHORT; */
			return (blkno_t)-1; /* file is too short, or hole */
			}

		/* check we have been passed a valid block number */
		if (blk < (dev->s_reserv + dev->s_isize) ||
				blk >= dev->s_fsize)
			{
			udata.u_error = ECORRUPTFS;
#if DEBUG
 dprintf(1, "bitmap_align_chase() 2 ino = %d, blk = %d, pos = %d\n",
         ino->c_num, blk, pos);
#endif
#if DEBUG >= 3
 dprintf(3, "bitmap_align_chase() returning -1, error %u\n", udata.u_error);
#endif
			return (blkno_t)-1; /* filesystem is corrupt */
			}

		/* see if the just-freed block was part of the target file */
		pos = bitmap_align_reverse(blk, region, regions, blocks);
		if (pos == (blkno_t)-1)
			{
#if DEBUG >= 3
 dprintf(3, "bitmap_align_chase() returning %d, ok\n", blk);
#endif
			return blk; /* success, return the just-freed block */
			}

		/* yes, we can move something into the just-freed block */
		}
	}

blkno_t bitmap_align_bmap(inoptr ino, blkno_t newno, blkno_t pos)
	{
	dev_t devno;
	int i, j, sh;
	blkno_t blk;
	bufptr buf, other;

#if DEBUG >= 3
 dprintf(3, "bitmap_align_bmap(%u, %u) starting\n", ino - i_tab, pos);
#endif

	devno = ino->c_dev;
	/* blocks 0..DIRECTBLOCKS-1 are direct blocks */
	if (pos < DIRECTBLOCKS)
		{
		blk = ino->c_node.i_addr[pos];
		if (blk == 0)
			{
			goto Err;
			}
		ino->c_node.i_addr[pos] = newno; /* move the block! */
		ino->c_dirty = 1;
		goto Ok;
		}
	/* addresses DIRECTBLOCKS and DIRECTBLOCKS+1 have single and double
	 * indirect blocks.
	 * The first step is to determine how many levels of indirection.
	 */
	pos -= DIRECTBLOCKS;
	sh = 0;
	j = INDIRECTBLOCKS+DINDIRECTBLOCKS;
	if (pos >= BUFSIZE / sizeof(blkno_t))	/* double indirect */
		{
		sh = 8; 			/* shift by 8 */
		pos -= BUFSIZE / sizeof(blkno_t);
		j -= INDIRECTBLOCKS;
		}
	/* fetch the address from the inode.
	 * Create the first indirect block if needed.
	 */
	blk = ino->c_node.i_addr[TOTALREFBLOCKS - j];
	if (blk == 0)
		{
		udata.u_error = ETOOSHORT; /* hole in file */
		goto Err;
		}
	/* fetch through the indirect blocks */
	while (j < INDIRECTBLOCKS+DINDIRECTBLOCKS) /* Nick <= */
		{
		buf = (bufptr)bread(devno, blk, 0);
		if (buf == NULL)
			{
			goto Err;
			}
		i = (pos >> sh) & (BUFSIZE / sizeof(blkno_t) - 1);
		blk = ((blkno_t *)buf)[i];
		if (brelse(buf) < 0)
			{
			goto Err;
			}
		if (blk == 0)
			{
			udata.u_error = ETOOSHORT; /* hole in file */
			goto Err;
			}
		sh -= 8;
		++j;
		}

	buf = (bufptr)bread(devno, blk, 0);
	if (buf == NULL)
		{
Err:
#if DEBUG >= 3
 dprintf(3, "bitmap_align_bmap() returning -1, error %u\n", udata.u_error);
#endif
		return (blkno_t)-1;
		}
	i = (pos >> sh) & (BUFSIZE / sizeof(blkno_t) - 1);
	blk = ((blkno_t *)buf)[i];
	if (blk == 0)
		{
		udata.u_error = ETOOSHORT; /* hole in file */
		brelse(buf);
		goto Err;
		}
	((blkno_t *)buf)[i] = newno; /* move the block! */
	if (bawrite(buf) < 0)
		{
		goto Err;
		}
Ok:
	/* read the block, as we have to move the data also */
	buf = (bufptr)bread(devno, blk, 0);
	if (buf == NULL)
		{
		goto Err;
		}

#if DEBUG >= 2
 dprintf(2, "getting block %d -> %d, errno = %d\n", blk, newno, udata.u_error);
#endif

	/* really move the block by manipulating cache entries */
	other = (bufptr)bfind(devno, newno); /* hanging around in cache? */
	if (other)
		{
/* dprintf(3, "bufpool entry %d = block %d, dirty 0\n", other - bufpool, blk); */
		other->bf_blk = blk; /* swap, for minimal overhead */
		other->bf_dirty = 0; /* try to avoid writing back */
		}

	/* revise the original cache entry to show the new block */
	buf->bf_blk = newno; /* original value is in variable blk */
/* dprintf(3, "bufpool entry %d = block %d, dirty 1\n", buf - bufpool, newno); */
	if (bawrite(buf) < 0) /* dirty, but delay writing for the moment */
		{
		goto Err;
		}

#if DEBUG >= 3
 dprintf(3, "bitmap_align_bmap() returning %u, success\n", blk);
#endif
	return blk;
}

blkno_t bitmap_align_reverse(blkno_t blk,
			     blkno_t *region, blkno_t regions, blkno_t blocks)
	{
	blkno_t i, k;
	blkno_t pos;

#if DEBUG > 3
 dprintf(4, "bitmap_align_reverse(%u) starting\n", blk);
#endif

	/* perform a reverse lookup of blk via the region list */
	pos = 0;
	for (i = 0; i < regions; i++)
		{
		/* is this a full region, or the last partial region? */
		k = int_min(blocks - pos, REGION_BLOCKS);

		/* see if the given blk falls within this region */
		if (blk >= region[i] && blk < (region[i] + k))
			{
			/* yes, return the computed logical pos in file */
			pos += blk - region[i];
#if DEBUG > 3
 dprintf(4, "bitmap_align_reverse() returning %u, found\n", pos);
#endif
			return pos;
			}

		/* calculate file position for start of next region */
		pos += REGION_BLOCKS;
		}

	/* the given blk was not found in any region of the list */
#if DEBUG > 3
 dprintf(4, "bitmap_align_reverse() returning -1, not found\n");
#endif
	return -1;
	}

blkno_t bitmap_align_recurse(fsptr dev, inoptr ino, int exclude,
				    blkno_t *parent, char *dirty,
				    int indirection, blkno_t *region,
				    blkno_t regions, blkno_t blocks)
	{
	dev_t devno;
	int i, reserved;
	blkno_t blk, newno;
	bufptr buf, other;
	int moveable;
	blkno_t tempblk;
	char tempdirty;
	blkno_t pos, chase;

#if DEBUG > 3
 dprintf(4, "bitmap_align_recurse"
         "(%d, %d, %d, 0x%x, 0x%x, %d, 0x%x, %d, %d) starting\n",
         dev - fs_tab, ino - i_tab, exclude, parent, dirty, indirection,
         region, regions, blocks);
#endif

	/* check we have been passed a valid block number */
	blk = *parent;
	if (blk < (dev->s_reserv + dev->s_isize) || blk >= dev->s_fsize)
		{
		/* filesystem is corrupt, invalid block */
		udata.u_error = ECORRUPTFS;
#if DEBUG
 dprintf(1, "bitmap_align_recurse() ino = %d, blk = %d, ind = %d, par = 0x%04x\n",
         ino->c_num, blk, indirection, parent);
#endif
#if DEBUG >= 3
 dprintf(3, "bitmap_align_recurse() returning -1, error %u\n", udata.u_error);
#endif
		return (blkno_t)-1;
		}

	/* see if this block is intended for the target file */
#if 0 /* not possible, see "exclude ? DIRECTBLOCKS : 0" above */
	if (exclude && indirection == 0)
		{
		/* it's some other aligned file on the drive */
		moveable = 0;
		}
	else
#endif
#if 0 /* under construction */
	if (exclude == 2 && indirection == 0)
		{
		/* it's the target file */
		chase = bitmap_align_reverse(blk, region, regions, blocks);
		moveable = (chase != (blkno_t)-1);
		}
	else
#endif
		{
		chase = bitmap_align_reverse(blk, region, regions, blocks);
		moveable = (chase != (blkno_t)-1);
		}

	/* early out, if the block doesn't need to be accessed at all */
	if (moveable == 0 && indirection == 0)
		{
		/* no action is needed (no move and not indirect) */
#if DEBUG > 3
 dprintf(4, "bitmap_align_recurse() returning -1, no action, ok\n");
#endif
		return (blkno_t)-1;
		}

	/* need to read the block, we might modify it, and/or relocate it */
	devno = dev->s_dev; /* don't forget! */
	buf = (bufptr)bread(devno, blk, 0);
	if (buf == NULL)
		{
#if DEBUG >= 3
 dprintf(3, "bitmap_align_recurse() returning -1, error %u\n", udata.u_error);
#endif
		return (blkno_t)-1; /* disk i/o error reading the block itself */
		}

	/* if it's a single or double indirect block, traverse recursively */
#if 1 /* under construction */
	if (indirection && (exclude != 1 || indirection > 1))
#else
	if (indirection && (exclude == 0 || indirection > 1))
#endif
		{
#if 0 /* silly silly */
		if (indirection > 1)
			{
			newno = BUFSIZE / sizeof(blkno_t); /* double ind. */
			}
		else
			{
			newno = 1; /* single indirect */
			}
#endif
		for (i = 0; i < BUFSIZE / sizeof(blkno_t); i++)
			{
			newno = ((blkno_t *)buf)[i];
/* if (newno == 3840) dprintf(3, "bad # %d,%d 0x%04x\n", buf->bf_blk, i, &((blkno_t *)buf)[i]); */
			if (newno)
				{
				if (exclude == 2) /* it's the target file */
					{
					tempblk = newno;
					tempdirty = 0;

					if (bfree(buf, buf->bf_dirty) < 0)
						{
#if DEBUG >= 3
 dprintf(3, "bitmap_align_recurse() returning -1, error %u\n", udata.u_error);
#endif
						return (blkno_t)-1; /* disk i/o error */
						}

					pos = bitmap_align_recurse(dev, /*pos,*/
						ino, exclude,
						&tempblk, &tempdirty,
						indirection-1,
						region, regions, blocks);

					buf = (bufptr)bread(devno, blk, 0);
					if (buf == NULL)
						{
#if DEBUG >= 3
 dprintf(3, "bitmap_align_recurse() returning -1, error %u\n", udata.u_error);
#endif
						return (blkno_t)-1; /* disk i/o error */
						}

					if (tempdirty)
						{
						((blkno_t *)buf)[i] = tempblk;
						buf->bf_dirty = tempdirty;
						}
					}
				else
					{
					pos = bitmap_align_recurse(dev, /*pos,*/
						ino, exclude,
						&((blkno_t *)buf)[i],
						&buf->bf_dirty, indirection-1,
						region, regions, blocks);
					}

				/* move something into the freed spot, and possibly repeat */
				if (pos != (blkno_t)-1)
					{
					if (exclude == 2)
						{
						if (bfree(buf, buf->bf_dirty) < 0)
							{
#if DEBUG >= 3
 dprintf(3, "bitmap_align_recurse() returning -1, error %u\n", udata.u_error);
#endif
							return (blkno_t)-1; /* disk i/o error */
							}
						}

					newno = bitmap_align_chase(dev, ino,
						newno, pos,
						region, regions, blocks);

					if (exclude == 2)
						{
						buf = (bufptr)bread(devno, blk, 0);
						if (buf == NULL)
							{
#if DEBUG >= 3
 dprintf(3, "bitmap_align_recurse() returning -1, error %u\n", udata.u_error);
#endif
							return (blkno_t)-1; /* disk i/o error */
							}
						}

					if (newno == (blkno_t)-1)
						{
#if DEBUG >= 3
 dprintf(3, "bitmap_align_recurse() returning -1, error %u\n", udata.u_error);
#endif
						return (blkno_t)-1;
						}

					/* eventually a non-target block was freed, indicate this */
					if (bitmap_set(devno, newno, 0,
						dev->s_bitmap_block,
						dev->s_bitmap_immov) != 1)
						{
						/* i/o error, or corrupt filesystem */
#if DEBUG >= 3
 dprintf(3, "bitmap_align_recurse() returning -1, error %u\n", udata.u_error);
#endif
						return (blkno_t)-1;
						}
					}
				}
			/*pos += newno;*/
			}
		}

	/* if this block is in the way of the target file, move it */
	if (moveable)
		{
		/* find an unoccupied block (fixme: can't be reserved) */
		newno = bitmap_find(devno, 0, 0, 1, dev->s_bitmap_block,
						    dev->s_bitmap_immov);
		if (newno < (dev->s_reserv + dev->s_isize) ||
				newno >= dev->s_fsize)
			{
			udata.u_error = ENOSPC; /* may overwrite ENXIO !! */
#if DEBUG >= 3
 dprintf(3, "bitmap_align_recurse() returning -1, error %u\n", udata.u_error);
#endif
			return (blkno_t)-1;
			}

		/* revise parent's allocation entry to show the new block */
		*parent = newno;

		/* inform parent that its allocation entries were changed */
		*dirty = 1;

#if DEBUG >= 2
 dprintf(2, "putting block %d -> %d, errno = %d\n", blk, newno, udata.u_error);
#endif

		/* really move the block by manipulating cache entries */
		other = (bufptr)bfind(devno, newno); /* hanging around in cache? */
		if (other)
			{
/* dprintf(3, "bufpool entry %d = block %d, dirty 0\n", other - bufpool, blk); */
			other->bf_blk = blk; /* swap, for minimal overhead */
			other->bf_dirty = 0; /* try to avoid writing back */
			}

		/* revise the original cache entry to show the new block */
/* dprintf(3, "bufpool entry %d = block %d, dirty 2\n", buf - bufpool, newno); */
		buf->bf_blk = newno; /* original value is in variable blk */
		if (bfree(buf, 2) < 0) /* very dirty, write back immediately */
			{
#if DEBUG >= 3
 dprintf(3, "bitmap_align_recurse() returning -1, error %u\n", udata.u_error);
#endif
			return (blkno_t)-1; /* disk i/o error writing back */
			}

#if DEBUG > 3
 dprintf(4, "bitmap_align_recurse() returning %d, moved, ok\n", chase);
#endif
		return chase; /* indicate which spot was just freed */
		}
	else
		{
		if (bfree(buf, buf->bf_dirty) < 0)
			{
#if DEBUG >= 3
 dprintf(3, "bitmap_align_recurse() returning -1, error %u\n", udata.u_error);
#endif
			return (blkno_t)-1; /* disk i/o error writing back */
			}
		}

#if DEBUG > 3
 dprintf(4, "bitmap_align_recurse() returning -1, ok\n");
#endif
	return (blkno_t)-1; /* successfully traversed the block and its children */
	}

blkno_t bitmap_search(dev_t devno, int size, blkno_t start, blkno_t final)
	{
	char *buf, *p;
	blkno_t i, j, lm, blk;
	unsigned long bits, mask; /* assumes REGION_BLOCKS <= 32 */

#if DEBUG >= 3
 dprintf(3, "bitmap_search(%u, %d, %u, %u) starting\n",
         devno, size, start, final);
#endif

#if REGION_BLOCKS < 32
	/* validate the size parameter for extra safety */
	if (size < 1 || size > REGION_BLOCKS)
		{
		return (blkno_t)-1; /* should indicate parameter error */
		}

	/* form a bitstring of the needed size, for testing bitmap */
	mask = (1L << size) - 1;
#else
	/* validate the size parameter for extra safety */
	if (size < 1 || size > 32) /* REGION_BLOCKS > 32 bombs at runtime */
		{
		return (blkno_t)-1; /* should indicate parameter error */
		}

	/* form a bitstring of the needed size, for testing bitmap */
	if (size < 32)
		{
		mask = (1L << size) - 1;
		}
	else
		{
		mask = (unsigned long)-1; /* size is maximum 32 */
		}
#endif

	/* initialise bitbuffer, we need it to span bitmap blocks */
	bits = (unsigned long)-1;

	/* read bitmap, skipping unused bytes of reserved blocks */
	j = start;
	while (j < final)
		{
		/* calculate ending position after reading block */
		lm = int_min((j + BUFSIZE) & ~(BUFSIZE - 1), final);

		/* read block and calculate starting relevant byte */
		buf = bread(devno, j >> BUFSIZELOG, 0);
		if (buf == NULL)
			{
#if DEBUG >= 3
 dprintf(3, "bitmap_search() returning -1, i/o error\n");
#endif
			return (blkno_t)-1; /* should indicate i/o error */
			}
		p = buf + (j & (BUFSIZE - 1));

		/* ready to scan each byte looking for contiguous '0' bits */
		for (i = j; i < lm; i += (PAGE_BLOCKS >> 3)) /* can't be 0 ! */
			{
			bits = (bits >> PAGE_BLOCKS) |
			       (((unsigned long)*p) << (32 - PAGE_BLOCKS));

			if ((bits & mask) == 0)
				{
				/* all of the required blocks are unlocked */

				/* calculate which disk block it refers to */
				blk = ((i - start) << 3) - (32 - PAGE_BLOCKS);

				/* don't write it back, our caller will */
				if (brelse((bufptr)buf) < 0)
					{
#if DEBUG >= 3
 dprintf(3, "bitmap_search() returning -1, i/o error\n");
#endif
					return (blkno_t)-1;
					}

#if DEBUG >= 3
 dprintf(3, "bitmap_search() returning %u, success\n", blk);
#endif
				return blk;
				}

			p += PAGE_BLOCKS >> 3; /* can't be 0 */
			}

		/* nothing found this time, release block and loop */
		if (brelse((bufptr)buf) < 0)
			{
#if DEBUG >= 3
 dprintf(3, "bitmap_search() returning -1, i/o error\n");
#endif
			return (blkno_t)-1;
			}

		/* bump counter to continue after data just read */
		j = lm;
		}

	/* no '0' string found within the bitmap limits */
#if DEBUG >= 3
 dprintf(3, "bitmap_search() returning -1, bitmap full\n");
#endif
	return (blkno_t)-1; /* not an error, bitmap was really full */
	}

int bitmap_reserve(dev_t devno, blkno_t blk, int size, int flag,
				blkno_t start, blkno_t final)
	{
	blkno_t i, j, lm;
	char *buf, *p, mask;

#if DEBUG >= 3
 dprintf(3, "bitmap_reserve(%u, %u, %d, %d, %u, %u) starting\n",
         devno, blk, size, flag, start, final);
#endif

	/* calculate byte position within bitmap for this block */
	j = start + (blk >> 3);
	if (j >= final)
		{
#if DEBUG >= 3
 dprintf(3, "bitmap_reserve() returning -1, out of range\n");
#endif
		return -1; /* block out of range, very bad */
		}

	/* read block and calculate starting relevant byte */
	buf = bread(devno, j >> BUFSIZELOG, 0);
	if (buf == NULL)
		{
#if DEBUG >= 3
 dprintf(3, "bitmap_reserve() returning -1, i/o error\n");
#endif
		return -1; /* should indicate i/o error */
		}
	p = buf + (j & (BUFSIZE - 1));

	/* calculate ending position after reading block */
	lm = int_min((j + BUFSIZE) & ~(BUFSIZE - 1), final);

	/* ready to modify the required bits and write back */
	mask = flag ? 0xff : 0;
	while (size > 8)
		{
		*p++ = mask;

		j++;
		if (j >= lm)
			{
			/* gone past the end of the current bitmap block */
			if (j >= final)
				{
#if DEBUG >= 3
 dprintf(3, "bitmap_reserve() returning -1, out of range\n");
#endif
			return -1; /* out of range */
				}

			/* need to flush the modifications done so far */
			if (bawrite((bufptr)buf) < 0)
				{
#if DEBUG >= 3
 dprintf(3, "bitmap_reserve() returning -1, i/o error\n");
#endif
				return -1; /* i/o error */
				}

			/* read next block, we always start at the beginning */
			buf = bread(devno, j >> BUFSIZELOG, 0);
			if (buf == NULL)
				{
#if DEBUG >= 3
 dprintf(3, "bitmap_reserve() returning -1, i/o error\n");
#endif
				return -1; /* i/o error */
				}
			p = buf;

			/* calculate ending position after reading block */
			lm = int_min((j + BUFSIZE) & ~(BUFSIZE - 1), final);
			}

		size -= 8;
		}

	/* done all bitmap bytes, except the last (maybe partial) byte */
	mask = (1 << size) - 1;
	if (flag)
		{
		*p |= mask;
		}
	else
		{
		*p &= ~mask;
		}

	/* ready to write back the modified bitmap block */
	if (bawrite((bufptr)buf) < 0)
		{
#if DEBUG >= 3
 dprintf(3, "bitmap_reserve() returning -1, i/o error\n");
#endif
		return -1; /* should indicate i/o error */
		}

#if DEBUG >= 3
 dprintf(3, "bitmap_reserve() returning 0, success\n");
#endif
	return 0;
	}

blkno_t bitmap_find(dev_t devno, blkno_t blk, int flag, int toggle,
				 blkno_t start, blkno_t final)
	{
	char c, first;
	char *buf, *p, mask;
	blkno_t i, j, lm;

#if DEBUG >= 3
 dprintf(3, "bitmap_find(%u, %u, %d, %d, %u, %u) starting\n",
		devno, blk, flag, toggle, start, final);
#endif

	/* set up to skip byte sized groups having opposite value */
	mask = flag ? 0 : 0xff;

	/* prepare to handle the first byte of the bitmap specially */
	first = ~((1 << (blk & 7)) - 1);

	/* read bitmap, skipping unused bytes of reserved blocks */
	j = start + (blk >> 3);
	while (j < final)
		{
		/* calculate ending position after reading block */
		lm = int_min((j + BUFSIZE) & ~(BUFSIZE - 1), final);

		/* read block and calculate starting relevant byte */
		buf = bread(devno, j >> BUFSIZELOG, 0);
		if (buf == NULL)
			{
#if DEBUG >= 3
 dprintf(3, "bitmap_find() returning -1, i/o error\n");
#endif
			return (blkno_t)-1; /* should indicate i/o error */
			}
		p = buf + (j & (BUFSIZE - 1));

		/* ready to scan each byte looking for a bit of 'flag' */
		for (i = j; i < lm; i++)
			{
			c = (*p ^ mask) & first; /* set bits being sought */
			first = 0xff; /* process full bytes from now on */

			if (c)
				{
				/* at least one of the 8 blocks is available */

				/* calculate which disk block it refers to */
				blk = (i - start) << 3;
				for (i = 0; i < 7; i++) /* don't check bit 7 */
					{
					if (c & (1 << i))
						{
						break;
						}
					}
				blk += i;

				if (toggle)
					{
					/* ready to toggle and write back */
					*p ^= 1 << i;
					if (bawrite((bufptr)buf) < 0)
						{
#if DEBUG >= 3
 dprintf(3, "bitmap_find() returning -1, i/o error\n");
#endif
						return (blkno_t)-1; /* i/o */
						}
					}
				else
					{
					if (brelse((bufptr)buf) < 0)
						{
#if DEBUG >= 3
 dprintf(3, "bitmap_find() returning -1, i/o error\n");
#endif
						return (blkno_t)-1; /* i/o */
						}
					}

#if DEBUG >= 3
 dprintf(3, "bitmap_find() returning %u, success\n", blk);
#endif
				return blk;
				}

			p++;
			}

		/* nothing found this time, release block and loop */
		if (brelse((bufptr)buf) < 0)
			{
#if DEBUG >= 3
 dprintf(3, "bitmap_find() returning -1, i/o error\n");
#endif
			return (blkno_t)-1;
			}

		/* bump counter to continue after data just read */
		j = lm;
		}

	/* no 'flag' bit found within the bitmap limits */
	blk = (j - start) << 3;
#if DEBUG >= 3
 dprintf(3, "bitmap_find() returning %u, bitmap full\n", blk);
#endif
	return blk; /* not an error, bitmap was really full */
	}

int bitmap_get(dev_t devno, blkno_t blk, blkno_t start, blkno_t final)
	{
	blkno_t i, j;
	char *buf, *p;

#if DEBUG >= 3
 dprintf(3, "bitmap_get(%u, %u, %u, %u) starting\n", devno, blk, start, final);
#endif

	/* calculate byte position within bitmap for this block */
	j = start + (blk >> 3);
	if (j >= final)
		{
#if DEBUG >= 3
 dprintf(3, "bitmap_get() returning -1, out of range\n");
#endif
		return -1; /* block out of range, very bad */
		}
	i = 1 << (blk & 7);

	/* read block and calculate starting relevant byte */
	buf = bread(devno, j >> BUFSIZELOG, 0);
	if (buf == NULL)
		{
#if DEBUG >= 3
 dprintf(3, "bitmap_get() returning -1, i/o error\n");
#endif
		return -1; /* should indicate i/o error */
		}
	p = buf + (j & (BUFSIZE - 1));

	/* check if the block is free or used (value of 0 or 1) */
	i = ((*p & i) != 0);

	if (brelse((bufptr)buf) < 0)
		{
#if DEBUG >= 3
 dprintf(3, "bitmap_get() returning -1, i/o error\n");
#endif
		return -1;
		}

#if DEBUG >= 3
 dprintf(3, "bitmap_get() returning %d, success\n", i);
#endif
	return i;
	}

int bitmap_set(dev_t devno, blkno_t blk, int flag,
			    blkno_t start, blkno_t final)
	{
	blkno_t i, j;
	char *buf, *p, mask;

#if DEBUG >= 3
 dprintf(3, "bitmap_set(%u, %u, %d, %u, %u) starting\n",
	 devno, blk, flag, start, final);
#endif

	/* calculate byte position within bitmap for this block */
	j = start + (blk >> 3);
	if (j >= final)
		{
#if DEBUG >= 3
 dprintf(3, "bitmap_set() returning -1, out of range\n");
#endif
		return -1; /* block out of range, very bad */
		}
	mask = 1 << (blk & 7);

	/* read block and calculate starting relevant byte */
	buf = bread(devno, j >> BUFSIZELOG, 0);
	if (buf == NULL)
		{
#if DEBUG >= 3
 dprintf(3, "bitmap_set() returning -1, i/o error\n");
#endif
		return -1; /* should indicate i/o error */
		}
	p = buf + (j & (BUFSIZE - 1));

	/* find the desired and the original values of the bit */
	i = (flag != 0);
	j = ((*p & mask) != 0);

	/* ready to modify the bit and write back (if necessary) */
	if (i != j)
		{
		*p ^= mask;
		if (bawrite((bufptr)buf) < 0)
			{
#if DEBUG >= 3
 dprintf(3, "bitmap_set() returning -1, i/o error\n");
#endif
			return -1; /* should indicate i/o error */
			}
		}
	else
		{
		if (brelse((bufptr)buf) < 0)
			{
#if DEBUG >= 3
 dprintf(3, "bitmap_set() returning -1, i/o error\n");
#endif
			return -1; /* should indicate i/o error */
			}
		}

#if DEBUG >= 3
 dprintf(3, "bitmap_set() returning %d, success\n", j);
#endif
	return j;
	}

