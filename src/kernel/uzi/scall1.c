/*
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 Implementation of system calls
**********************************************************/

#define NEED__DEVIO
#define NEED__FILESYS
#define NEED__MACHDEP
#define NEED__PROCESS
#define NEED__SCALL

#if 1 /* Nick */
#include "vendor.h" /* Nick, must come first */
#include <unix.h>
#include <config.h>
#include <extern.h>
#include "devio.h" /* prototypes added by Nick */
#include "filesys.h" /* prototypes added by Nick */
#include "scall1.h" /* prototypes added by Nick */
#define __KERNEL__ /* temporary */
static int chany(char *path, int val1, int val2, bool_t mode);
#else /* Nick */
#include "uzix.h"
#ifdef SEPH
#include "types.h"
#include "signal.h"
#include "errno.h"
#include "fcntl.h"
#include "sys\stat.h"
#include "sys\ioctl.h"
#endif
#include "unix.h"
#include "extern.h"
#endif /* Nick */

#define ISIZE(ino)	((ino)->c_node.i_size)

/* definitions for 'UDATA' access */
#define UOFFS		(udata.u_offset)
#define UBAS		(udata.u_base)
#define UCNT		(udata.u_count)
#define UERR		(udata.u_error)

inoptr rwsetup(uchar fd, void *base, uint cnt, uchar rwflag);
#define psize(x)	(uint)ISIZE(x)
#define addoff(x,y)	*(x)+=(y)
#define updoff(x)	of_tab[udata.u_files[x]].o_ptr = UOFFS
void stcpy(inoptr ino, struct stat *buf);
inoptr n_creat(char *name, bool_t new, mode_t mode);

#if 1 /* Nick UZIX compatible */
extern char UZIX[]; /* operating system name, defined in main.c */
extern char HOST[]; /* this terminal's hostname, defined in main.c */
#endif

#ifdef __KERNEL__
/*********************************************
 SYSCALL NONE();
**********************************************/
int sys_NONE(void)
{
	UERR = ENOSYS;
	return -1;
}
#endif	/* __KERNEL__ */

/****************************************
 SYSCALL sync(void);
***************************************/
int sys_sync(void)
{
	i_sync();	/* flushing inodes */
	fs_sync();	/* flushing superblocks */
			/* flushing buffers is already done by i_sync/fs_sync */
	return 0;
}

#ifdef NEED__SCALL

/********************************
 SYSCALL utime(char *path, struct utimbuf *buf);
********************************/
#define path (char *)udata.u_argn0
#define buf ((struct utimbuf *)udata.u_argn1)
int sys_utime(void)
{
	struct utimbuf lcl, *bufp = buf;
	register inoptr ino;

	if (path == NULL) {
		UERR = EFAULT;
Err:		return -1;
	}
	if (bufp == NULL) {
		bufp = &lcl;
		rdtime(&bufp->actime);
		bcopy(&bufp->actime, &bufp->modtime, sizeof(time_t));
	}
#if 1 /* Nick */
	else
		{
		uget((char *)bufp, (char *)&lcl, sizeof(struct utimbuf));
		bufp = &lcl;
		}
#endif
	if ((ino = namei(path, NULL, 1)) == NULL)
		goto Err;
	if (ino->c_node.i_uid != udata.u_euid && !super()) {
		UERR = EPERM;
		goto Err1;
	}
	if (ino->c_ro) {
		UERR = EROFS;
Err1:		i_deref(ino);
		goto Err;
	}
	bcopy(&bufp->actime, &ino->c_node.i_atime, sizeof(time_t));
	bcopy(&bufp->modtime, &ino->c_node.i_mtime, sizeof(time_t));
	ino->c_dirty=1;
	i_deref(ino);
	return 0;
}
#undef path
#undef buf

/*********************************************
 SYSCALL close(int uindex);
**********************************************/
#define uindex (uint)udata.u_argn0
int sys_close(void)
{
	return doclose(uindex);
}
#undef uindex

/* truncate the file, if a regular one, to zero length */
void truncateto0(inoptr ino)
	{
	oft_t *op;

#if 1
	if (S_ISREGALIGN(ino->c_node.i_mode)) /* regular file or XIP aligned */
#else
	if (getmode(ino) == S_IFREG)	/* regular file */
#endif
		{
		/* Truncate the regular file to zero length */
		ISIZE(ino) = 0; /* for future use */
		f_trunc(ino);
		/* Reset any oft pointers */
		op = of_tab;
		while (op < of_tab+OFTSIZE)
			{
			if (op->o_inode == ino)
				op->o_ptr = 0;
			++op;
			}
		}
	}

/*******************************************
 SYSCALL open(char *name, int flag, mode_t mode);
********************************************/
#define name (char *)udata.u_argn0
#define flag (int)udata.u_argn1
#define mode udata.u_argn2
int sys_open(void)
{
	ofptr of;
	off_t *op;
	register inoptr ino;
	register int spf = flag;
	uchar perm, oftindex, uindex;

	if ((flag &= 0xFF) > O_RDWR) {	/* special flags in high byte */
		UERR = EINVAL;
		goto Err1;
	}
	if (spf & O_SYMLINK)
		spf = O_SYMLINK;
	if ((uindex = uf_alloc()) == (uchar)-1 ||	/* user file descriptor */
	    (oftindex = oft_alloc()) == (uchar)-1)	/* system file descriptor */
		goto Err1;
	if ((spf & O_NEW) ||
	    (ino = namei(name, NULL, !(spf & O_SYMLINK))) == NULL) { /* open inode for name */
		if (!(spf & O_CREAT))
			goto Err;
		mode = (S_IFREG | (mode & S_UMASK & ~udata.u_mask));
		udata.u_error = 0;
		if ((ino = n_creat(name, (spf & O_NEW) != 0, mode)) == NULL)
#if 0 /* Nick's experimental addition */
			{
			UERR = 10000;
			goto Err;
			}
#else
			goto Err;
#endif
	}
	of = &of_tab[oftindex];
	of->o_inode = ino;			/* save inode ref */
	perm = getperm(ino);			/* check for permissions */
	if (((((uchar)flag == O_RDONLY) || (uchar)flag == O_RDWR) &&
			!((uchar)perm & S_IOREAD)) ||
	    (((uchar)flag == O_WRONLY || (uchar)flag == O_RDWR) &&
			!((uchar)perm & S_IOWRITE))) {
		UERR = EPERM;
		goto Err;
	}
	if (((uchar)flag == O_WRONLY || (uchar)flag == O_RDWR) &&
			(ino->c_ro)) {
		UERR = EROFS;
		goto Err;
	}
	if ((spf & O_SYMLINK) && getmode(ino) != S_IFLNK) {
		UERR = ESRCH;
		goto Err;
	}
	if (getmode(ino) == S_IFDIR &&		/* directories only for read */
	    flag != O_RDONLY) {
		UERR = EISDIR;
		goto Err;
	}
	if (isdevice(ino) &&
#if 1 /* Nick */
		ino->c_refs == 1 &&
		/*of_tab[oftindex].o_refs == 1 &&*/ /* always true */
#endif
			d_open(DEVNUM(ino)) != 0) {
		UERR = ENXIO;			/* can't access to device */
		goto Err;
	}
	udata.u_files[uindex] = oftindex;	/* user fd */
	op = &(of->o_ptr);
	if (spf & O_APPEND)			/* posit to end of file */
		*op = ino->c_node.i_size;
	else	*op = 0;		/* posit to start of file */
	/* creat must truncate file to 0 if it exists! */
	if ((spf & (O_CREAT|O_TRUNC)) == (O_CREAT|O_TRUNC)) truncateto0(ino);
	of->o_access = flag;
	if (getmode(ino) == S_IFPIPE && of->o_refs == 1)
		psleep(ino);	/* sleep process if no writer of reader */
	return uindex;

Err:	oft_deref(oftindex);	/* This will call i_deref() */
Err1:	return (-1);
}
#undef name
#undef flag
#undef mode

/********************************************
 SYSCALL link(char *oldname, char *newname);
*********************************************/
#define oldname (char *)udata.u_argn0
#define newname (char *)udata.u_argn1
int sys_link(void)
{
	register inoptr ino, ino2;
	inoptr parent2 = NULL;
	int ret = -1;
#if 1
	char fname[15];
#endif

	if ((ino = namei(oldname, NULL, 1)) == 0)
		goto Err1;
	if (!(getperm(ino) & S_IOWRITE)) {
		UERR = EPERM;
		goto Err;
	}
	/* Make sure newname doesn't exist, and get its parent */
	if ((ino2 = namei(newname, &parent2, 1)) != NULL) {
		i_deref(ino2);
		UERR = EEXIST;
		goto Err;
	}
	if (parent2 == NULL)	/* even no path */
		goto Err;
	if (ino->c_dev != parent2->c_dev) {
		UERR = EXDEV;	/* inter-device link not allowed */
		goto Err;
	}
#if 1 /* Nick */
	filename(newname, fname);
	if (ch_link(parent2, "", fname, ino) == 0)
#else
	if (ch_link(parent2, "", filename(newname), ino) == 0)
#endif
		goto Err;
	/* Update the link count */
	++ino->c_node.i_nlink;
	if (wr_inode(ino) < 0) goto Err;
	setftim(ino, C_TIME);
	ret = 0;
Err:	if (parent2 != NULL)
		i_deref(parent2);
	i_deref(ino);
Err1:	return ret;
}
#undef oldname
#undef newname

/********************************************
 SYSCALL symlink(char *oldname, char *newname);
*********************************************/
#define oldname (char *)udata.u_argn0
#define newname (char *)udata.u_argn1
int sys_symlink(void)
{
	register inoptr ino = NULL;
	int ret = -1;
	mode_t mode = S_IFREG | (0666 & ~udata.u_mask);
#if 1 /* Nick for IAR compiler */
	char *p;
#endif

	/* Create newname as new IFREG node */
	if ((ino = n_creat(newname, 1, mode)) == NULL)
		goto Ret;
	/* fill in new file */
	UOFFS = 0;	/* don't use a = b = c = 0 ! HTC doesn't like this */
	UCNT = 0;
	UBAS = (uchar *)oldname; /* buf */
#if 1 /* Nick for IAR compiler */
	p = oldname;
	while (ugetc(p)) /* (*p) */
		{
		p++;
		udata.u_count++;
		}
#else
	while (ugetc(oldname++)) /* (*oldname++ != 0) */
		UCNT = UCNT+1;	/* nbytes */
#endif
#if 1 /* Nick */
	udata.u_sysio = 0;	/* target of symlink is passed in user space */
#endif
	writei(ino);		/* write it */
	/* change mode to LNK */
	ino->c_node.i_mode = (mode & S_UMASK) | S_IFLNK;
	if (wr_inode(ino) < 0) goto Ret;
	ret = 0;
Ret:	if (ino != NULL)	i_deref(ino);
	return ret;
}
#undef oldname
#undef newname

/*******************************************
 SYSCALL unlink(char *path);
********************************************/
#define path (char *)udata.u_argn0
int sys_unlink(void)
{
	auto inoptr pino;
	register inoptr ino = namei(path, &pino, 0);
	int ret = -1;
#if 1
	char fname[15];
#endif

	if (pino == NULL || ino == NULL) {
		UERR = ENOENT;
		goto Ret;
	}
	if (!(getperm(ino) & S_IOWRITE)) {
		UERR = EPERM;
		goto Ret;
	}
	/* Remove the directory entry */
#if 1
	filename(path, fname);
	if (ch_link(pino, fname, "", NULL) == 0)
#else
	if (ch_link(pino, filename(path), "", NULL) == 0)
#endif
		goto Ret;
	/* Decrease the link count of the inode */
	if (ino->c_node.i_nlink-- == 0)
		warning("_unlink: bad nlink %d",ino->c_node.i_nlink += 2);
	setftim(ino, C_TIME);
	ret = 0;
Ret:	if (pino)	i_deref(pino);
	if (ino)	i_deref(ino);
	return ret;
}
#undef path

/**********************************************
 SYSCALL read(int d, char *buf, uint nbytes);
 SYSCALL write(int d, char *buf, uint nbytes);
**********************************************/
#define d (uint)udata.u_argn0
#define buf (void *)udata.u_argn1
#define nbytes (uint)udata.u_argn2
#define isread (udata.u_callno == 23)
int sys_readwrite(void)
{
	register inoptr ino;

	/* Set up u_base, u_offset, ino; check permissions, file num. */
	if ((ino = rwsetup(d, buf, nbytes, isread)) == NULL)
		return (-1);	/* bomb out if error */
	if (nbytes)
		if (isread) readi(ino); else writei(ino);
	updoff(d);
	return UCNT;
}
#undef d
#undef buf
#undef nbytes

/************************************************
 SYSCALL lseek(int file, long offset, int whence);
************************************************/
#define file (uchar)udata.u_argn0
#define offset *((long *)&udata.u_argn1)
#define flag (int)udata.u_argn3
int sys_lseek(void)
{
	register off_t* offp;
	register inoptr ino;

	if ((ino = getinode(file)) == NULL) {
Err1:		udata.u_retval1 = (-1);
		return (-1);
	}
	if (getmode(ino) == S_IFPIPE) {
		UERR = ESPIPE;
		goto Err1;
	}
	offp = &of_tab[udata.u_files[file]].o_ptr;
	switch (flag) {
	case SEEK_SET:	*offp = offset; 		break;
	case SEEK_CUR:	*offp += offset;		break;
	case SEEK_END:	*offp = ISIZE(ino) + offset;	break;
	default:
		UERR = EINVAL;
		goto Err1;
	}
	udata.u_retval1 = ((uint *)offp)[1];
	return *(uint *)offp;
}
#undef file
#undef offset
#undef flag

/************************************
 SYSCALL chdir(char *dir);
************************************/
#define dir (char *)udata.u_argn0
int sys_chdir(void)
{
	register inoptr newcwd;

	if ((newcwd = namei(dir, NULL, 1)) == 0)
Err1:		return (-1);
	if (getmode(newcwd) != S_IFDIR) {
		i_deref(newcwd);
		UERR = ENOTDIR;
		goto Err1;
	}
	if (!(getperm(newcwd) & S_IOEXEC)) {
		UERR = EPERM;
		goto Err1;
	}
	i_deref(udata.u_cwd);
	udata.u_cwd = newcwd;
	return 0;
}
#undef dir

/************************************
 SYSCALL chroot(char *dir);
************************************/
#define dir (char *)udata.u_argn0
int sys_chroot(void)
{
	if (sys_chdir() == 0) {
		i_deref(udata.u_root);
		udata.u_root = udata.u_cwd;
		i_ref(udata.u_root);
		return 0;
	}
	return -1;
}
#undef dir

/*********************************************
 SYSCALL mknod(char *name, mode_t mode, int dev);
*********************************************/
#define name (char *)udata.u_argn0
#define mode (int)udata.u_argn1
#define dev (dev_t)udata.u_argn2
int sys_mknod(void)
{
	register inoptr ino;

	mode &= ((~udata.u_mask & S_UMASK) | S_IFMT);
	if (S_ISDEV(mode) && !super()) { /* only super able to create devs */
		UERR = EPERM;
Err1:		return -1;
	}
	if ((ino = n_creat(name, 1, mode)) == NULL)
		goto Err1;
	/* Initialize device node */
	if (isdevice(ino)) {
		DEVNUM(ino) = dev;
		ino->c_dirty = 1;
	}
	if (wr_inode(ino) < 0) {
		i_deref(ino);
		goto Err1;
	}
	i_deref(ino);
	return 0;
}
#undef name
#undef mode
#undef dev

/****************************************
 SYSCALL access(char *path, int mode);
****************************************/
#define path (char *)udata.u_argn0
#define mode (int)udata.u_argn1
int sys_access(void)
{
	register inoptr ino;
	register int retval = -1;
	uchar euid, egid;
#if 1 /* Nick for IAR compiler */
	/* char *p; */

	/* p = path; */
	if ((mode &= 07) != 0 && ugetc(path) == 0) {
#else
	if ((mode &= 07) != 0 && *path == 0) {
#endif
		UERR = ENOENT;
		goto Err1;
	}
	/* Temporarily make eff. id real id. */
	euid = udata.u_euid;
	egid = udata.u_egid;
	udata.u_euid = udata.u_ptab->p_uid;
	udata.u_egid = udata.u_gid;

	if ((ino = namei(path, NULL, 1)) == NULL)
		goto Err;
	retval = 0;
	if ((~getperm(ino) & mode) != 0) {
		UERR = EPERM;
		retval = -1;
	}
	i_deref(ino);
Err:	udata.u_euid = euid;
	udata.u_egid = egid;
Err1:	return retval;
}
#undef path
#undef mode

static int chany(char *path, int val1, int val2, bool_t mode)
{
	register inoptr ino;

	if ((ino = namei(path, NULL, 1)) == NULL)
		goto Err1;
	if (ino->c_node.i_uid != udata.u_euid && !super()) {
		i_deref(ino);
		UERR = EPERM;
Err1:		return (-1);
	}
	if (mode) {
		/* file class can't be changed */
		ino->c_node.i_mode &= S_IFMT;
		ino->c_node.i_mode = (ino->c_node.i_mode |
				      ((mode_t)val1 & S_UMASK));
	}
	else {
		ino->c_node.i_uid = val1;
		ino->c_node.i_gid = val2;
	}
	setftim(ino, C_TIME);
	i_deref(ino);
	return 0;
}

/*******************************************
 SYSCALL chmod(char *path, mode_t mode);
*******************************************/
#define path (char *)udata.u_argn0
#define mode (mode_t)udata.u_argn1
int sys_chmod(void)
{
	return chany(path, (int)mode, 0, 1);
}
#undef path
#undef mode

/*************************************************
 SYSCALL chown(char *path, int owner, int group);
*************************************************/
#define path (char *)udata.u_argn0
#define owner (uchar)udata.u_argn1
#define group (uchar)udata.u_argn2
int sys_chown(void)
{
	return chany(path, owner, group, 0);
}
#undef path
#undef owner
#undef group

#if 1 /* Nick UZIX compatible */
/* stcpy() Utility for stat and fstat
 */
void stcpy(inoptr ino, struct stat *buf)
{
	/* don't bother making stat() and fstat() available to the kernel */
	/* but do ensure fields are copied individually, cinode_t may change */
	uputw(ino->c_dev, &buf->st_dev);
	uputw(ino->c_num, &buf->st_ino);
	uputw(ino->c_node.i_mode, &buf->st_mode);
	uputw(ino->c_node.i_nlink, &buf->st_nlink);
	uputw(ino->c_node.i_uid, &buf->st_uid);
	uputw(ino->c_node.i_gid, &buf->st_gid);
	uputw(DEVNUM(ino), &buf->st_rdev);
	uput(&ino->c_node.i_size, &buf->st_size, sizeof(off_t));
	uput(&ino->c_node.i_atime, &buf->st_atime, 3*sizeof(time_t));
}
#else
/* stcpy() Utility for stat and fstat
 */
void stcpy(inoptr ino, struct stat *buf)
{
	buf->st_dev = ino->c_dev;
	buf->st_ino = ino->c_num;
	buf->st_mode = ino->c_node.i_mode;
	buf->st_nlink = ino->c_node.i_nlink;
	buf->st_uid = ino->c_node.i_uid;
	buf->st_gid = ino->c_node.i_gid;
	buf->st_rdev = DEVNUM(ino);
	bcopy(&ino->c_node.i_size, &buf->st_size, sizeof(off_t));
	bcopy(&ino->c_node.i_atime, &buf->st_atime, 3*sizeof(time_t));
}
#endif

/**************************************
 SYSCALL stat(char *path, char *buf);
 SYSCALL fstat(int fd, char *buf);
 **************************************/
#define path (char *)udata.u_argn0
#define fd (uchar)udata.u_argn0
#define buf (char *)udata.u_argn1
#define isstat (udata.u_callno == 27)
int sys_statfstat(void)
{
	register inoptr ino;

	if (!valadr(buf, sizeof(struct stat))) goto Err;
	if ((ino = isstat ? namei(path, NULL, 1) : getinode(fd)) == 0) {
Err:		return -1;
	}
	stcpy(ino, (struct stat *)buf);
	if (isstat) i_deref(ino);
	return (0);
}
#undef path
#undef buf

#if 1 /* Nick free bitmap */
/**************************************
 SYSCALL falign(int fd, int parm);
 **************************************/
#define fd (uchar)udata.u_argn0
#define parm (int)udata.u_argn1
int sys_falign(void)
{
	inoptr ino;

	ino = getinode(fd);
	if (ino == 0)
		{
		return -1;
		}

	switch (parm)
		{
	case XIP_ALIGN:
		return bitmap_align(ino, ino->c_node.i_size);

	case XIP_UALIGN:
		return bitmap_ualign(ino, ino->c_node.i_size);
		}

	return -1;
}
#undef path
#undef buf
#endif /* Nick free bitmap */

/************************************
 SYSCALL dup(int oldd);
************************************/
#define oldd (uchar)udata.u_argn0
int sys_dup(void)
{
	register uchar newd, oftindex;

	if (getinode(oldd) == NULL ||
	    (newd = uf_alloc()) == (uchar)-1)
		return (-1);
	oftindex = udata.u_files[oldd];
	udata.u_files[newd] = oftindex;
	++of_tab[oftindex].o_refs;
	return newd;
}
#undef oldd

/****************************************
 SYSCALL dup2(int oldd, int newd);
****************************************/
#define oldd (uchar)udata.u_argn0
#define newd (uchar)udata.u_argn1
int sys_dup2(void)
{
	if (getinode(oldd) == NULL)
		goto Err1;
	if (newd >= UFTSIZE) {
		UERR = EBADF;
Err1:		return (-1);
	}
	if (!freefileentry(udata.u_files[newd]))
		doclose(newd);
	udata.u_files[newd] = udata.u_files[oldd];
	++of_tab[udata.u_files[oldd]].o_refs;
	return 0;
}
#undef oldd
#undef newd

#if 1 /* Nick, new getfsys() convention */
/* Special system call returns copies of UZIX
 * structures for system dependent utils like df, ps...
 */
/***********************************************
 SYSCALL getfsys(dev_t devno, char *ubuf);
***********************************************/
#define devno ((dev_t)udata.u_argn0)
#define ubuf ((char *)udata.u_argn1)
int sys_getfsys(void)
	{
	info_t buf;

	if (getfsys(devno, &buf))
		{
		return -1;
		}

	if (!valadr(ubuf, buf.size))
		{
		return -1;
		}

	uput((char *)buf.ptr, ubuf, buf.size);
	return 0;
	}
#undef devno
#undef ubuf
#else
/* Special system call returns pointers to UZIX
 * structures for system dependent utils like df, ps...
 */
/***********************************************
 SYSCALL getfsys(dev_t devno, char *buf); formerly info_t *buf);
***********************************************/
#define devno ((dev_t)udata.u_argn0)
#define buf ((info_t *)udata.u_argn1)
int sys_getfsys(void)
{
	return getfsys(devno, buf);
}
#undef devno
#undef buf
#endif

/**********************************************
 SYSCALL ioctl(int fd,int request, void *data);
**********************************************/
#define fd (uchar)udata.u_argn0
#define request (int)udata.u_argn1
#define data (void *)udata.u_argn2
int sys_ioctl(void)
{
	register inoptr ino;

	if ((ino = getinode(fd)) == NULL)
		UERR = EBADF;
	else if (!isdevice(ino))
		UERR = ENODEV;
	else if ((getperm(ino) & S_IOWRITE) == 0)
		UERR = EPERM;
	else if (!d_ioctl(DEVNUM(ino), request, data))
		return 0;
	return -1;
}
#undef fd
#undef request
#undef data

/**************************************************
 SYSCALL mount(char *spec, char *dir, int rwflag);
 SYSCALL umount(char *spec);
**************************************************/
#define spec (char *)udata.u_argn0
#define dir (char *)udata.u_argn1
#define rwflag (int)udata.u_argn2
#define ismount (udata.u_callno == 19)
int sys_mountumount(void)
{
	register inoptr sino, dino;
	register dev_t dev;
	int sts = -1;
	fsptr fp;

	if (!super()) {
		UERR = EPERM;
		goto Err1;
	}
	if ((sino = namei(spec, NULL, 1)) == NULL)
		goto Err1;
	if (ismount) if ((dino = namei(dir, NULL, 1)) == 0) {
		i_deref(sino);
		goto Err1;
	}
	if (getmode(sino) != S_IFBLK) {
		UERR = ENOTBLK;
		goto Err;
	}
	if (!ismount) {
#if 0 /* FIX THIS!... now fixed */
		if (!validdev(dev = DEVNUM(sino))) goto ErrIO;
#else
		if (!validdev(dev = DEVNUM(sino),NULL)) goto ErrIO;
#endif
	} else {
		if (getmode(dino) != S_IFDIR) {
			UERR = ENOTDIR;
			goto Err;
		}
		if (d_open(dev = DEVNUM(sino))) {
ErrIO:			UERR = ENXIO;
			goto Err;
		}
		d_close(dev);	/* fmount will open the device */
	}
	fp = findfs(dev);
	if (!ismount) {
		if (!fp || !fp->s_mounted) {
			UERR = EINVAL;
			goto Err;
		}
		dino = i_tab;
		while (dino < i_tab + ITABSIZE) {
			if (dino->c_refs > 0 && dino->c_dev == dev) {
				goto ErrBusy;
			}
			++dino;
		}
	} else {
		if (fp || dino->c_refs != 1 || dino->c_num == ROOTINODE) {
ErrBusy:		UERR = EBUSY;
			goto Err;
		}
	}
	sys_sync();
	if (!ismount) {
		fp->s_mounted = 0;
		i_deref(fp->s_mntpt);
		sts = 0;
	} else {
		if (!fmount(dev, dino, rwflag))
			sts = 0;
		else	goto ErrBusy;
	}
Err:	if (ismount) i_deref(dino);
	i_deref(sino);
Err1:	return sts;
}
#undef spec
#undef dir
#undef rwflag

/***********************************
 SYSCALL time(time_t *tvec);
***********************************/
#define tvec (time_t *)udata.u_argn0
int sys_time(void)
{
#if 1 /* Nick */
	time_t buf;

	rdtime(&buf);
	uputw(buf.t_time, &(tvec)->t_time);
	uputw(buf.t_date, &(tvec)->t_date);
#else
	rdtime(tvec);
#endif
	return 0;
}
#undef tvec

/*======================================================================*/

#ifdef __KERNEL__
/* exit0() - do exit through vector 0 (erroneous jmp to address 0)
 */
void exit0(void)
{
	doexit(-1, -1);
}
#endif	/* __KERNEL__ */

/* create any kind of node */
inoptr n_creat(char *name, bool_t new, mode_t mode)
{
	register inoptr ino;
	auto inoptr parent = NULL;
#if 1
	char fname[15];
#endif
#if DEBUG >= 3
 char *buf;

 buf = tmpbuf();
 ugets(name, buf, 512);
 dprintf(3, "n_creat(\"%s\", %d, 0%o)\n", buf, new, mode);
 brelse((bufptr)buf);
#endif

	if ((ino = namei(name, &parent, 1)) != NULL) {
		/* The file already exists */
		if (new) {
			UERR = EEXIST;		/* need new file! */
			goto Err1;
		}
		i_deref(parent);	/* parent not needed */
		if (getmode(ino) == S_IFDIR) {	/* can't rewrite directory */
			UERR = EISDIR;
			goto Err;
		}
		/* must have the write access */
		if ((getperm(ino) & S_IOWRITE) == 0) {
			UERR = EACCES;
			goto Err;
		}
		truncateto0(ino);
	}
	else {	/* must create the new file */
		if (parent == NULL)	/* missing path to file */
#if 0 /* Nick experimental */
			{
			UERR = 10003;
			goto Err;
			}
#else
			goto Err;
#endif
#if 1 /* Nick */
		filename(name, fname);
		if ((ino = newfile(parent, fname)) == NULL)
#else
		if ((ino = newfile(parent, name)) == NULL)
#endif
		/* Parent was derefed in newfile! */
#if 0 /* Nick experimental */
			{
			UERR = 10002;
			goto Err;
			}
#else
			goto Err;	/* Doesn't exist and can't make it */
#endif
		ino->c_node.i_mode = mode;
		setftim(ino, A_TIME | M_TIME | C_TIME);
		/* The rest of the inode is initialized in newfile() */
		if (_getmode(mode) == S_IFDIR) {
			if (ch_link(ino, "", ".", ino) == 0 ||
			    ch_link(ino, "", "..", parent) == 0)
				goto Err1;
			ino->c_node.i_size = 2*sizeof(direct_t);
			ino->c_node.i_nlink++;
			parent->c_node.i_nlink++;
			parent->c_dirty = 1;
		}
	}
	if (wr_inode(ino) < 0)
#if 0 /* Nick experimental */
		{
		UERR = 10001;
		goto Err;
		}
#else
		goto Err;
#endif
#if DEBUG >= 3
 dprintf(3, "n_creat() returning %u, success\n", ino - i_tab);
#endif
	return ino;

Err1:	i_deref(parent);
Err:	if (ino)
		i_deref(ino);
#if DEBUG >= 3
 dprintf(3, "n_creat() returning NULL, error %u\n", udata.u_error);
#endif
	return NULL;
}

/* readwritei() implements reading/writing from/to any kind of inode
 * udata prepared with parameters
 */

void readwritei(char write, inoptr ino)
{
	register uint amount, todo, ublk, uoff, tdo = 0;
	register char *bp;
	register blkno_t pblk;
	dev_t dev = ino->c_dev;
	bool_t ispipe = 0;

#if DEBUG >= 3
 dprintf(3, "readwritei(%u, %u) starting\n", (unsigned int)write, ino - i_tab);
#endif

	if (write && ino->c_ro) {
		UERR = EROFS;
#if DEBUG >= 3
 dprintf(3, "readwritei() returning, error %u\n", udata.u_error);
#endif
		return;
	}
	switch (getmode(ino)) {
	case S_IFCHR:	/* character device */
		UCNT = write ? cdwrite(DEVNUM(ino)) : cdread(DEVNUM(ino));
		if ((int)UCNT != -1)
			addoff(&UOFFS, UCNT);
#if DEBUG >= 3
 dprintf(3, "readwritei() returning, error %u\n", udata.u_error);
#endif
		return;
	case S_IFLNK:	/* sym link */
		if (write) goto error;
	case S_IFDIR:	/* directory */
	case S_IFREG:	/* regular file */
#if 1 /* Nick */
	case S_IFALIGN:	/* aligned regular file */
#endif
		todo = UCNT;
		if (!write) {
			/* See if offset is beyond end of file */
			if ((unsigned long)UOFFS >= (unsigned long)ISIZE(ino)) {
				UCNT = 0;
#if DEBUG >= 3
 dprintf(3, "readwritei() returning, error %u\n", udata.u_error);
#endif
				return;
			}
			/* See if end of file will limit read */
			if (UOFFS + todo > ISIZE(ino))
				todo = (uint)(ISIZE(ino) - UOFFS);
			UCNT = todo;
		}		
		goto loop;
#ifdef __KERNEL__
	case S_IFPIPE:	/* interprocess pipe */
		++ispipe;
		if (write) {	/* write side */
			while ((todo = UCNT) >
			       (DIRECTBLOCKS * BUFSIZE) - psize(ino)) {
				if (ino->c_refs == 1) { /* No readers - broken pipe */
					UCNT = -1;
					UERR = EPIPE;
					ssig(udata.u_ptab, SIGPIPE);
#if DEBUG >= 3
 dprintf(3, "readwritei() returning, error %u\n", udata.u_error);
#endif
					return;
				}
				/* Sleep if full pipe */
				psleep(ino);	/* P_SLEEP */
			}
			goto loop;
		} else {	/* read side */
			while (psize(ino) == 0) {
				if (ino->c_refs == 1)	/* No writers */
					break;
				/* Sleep if empty pipe */
				psleep(ino);		/* P_SLEEP */
			}
#if 1 /* Nick */
			/* this really should be revised to uint_min */
			todo = int_min(udata.u_count, psize(ino));
#else
			todo = Min(UCNT, psize(ino));
#endif
			UCNT = todo;
			goto loop;
		}
#endif	/* __KERNEL__ */
	case S_IFBLK:	/* block device */
		dev = DEVNUM(ino);
		todo = UCNT;
		/* blockdev/directory/regular/pipe */
loop:		while (todo) {
			ublk = (uint)(UOFFS >> BUFSIZELOG);
			uoff = (uint)UOFFS & (BUFSIZE - 1);
			pblk = bmap(ino, ublk, write ? 0 : 1);
			if (!write) {
				/* Nick added isdevice(ino) to allow access */
				/* to block 0 via open handle to /dev/hdX */
#if 0 /* FIX THIS!... now fixed */
				bp = (pblk == NULLBLK && isdevice(ino) == 0) ?
				     zerobuf() : bread(dev, pblk, 0);
#else
				bp = (pblk == NULLBLK && isdevice(ino) == 0) ?
				     zerobuf(1): bread(dev, pblk, 0);
#endif
				if (bp == NULL) {
					/* EFAULT: for FD, means, out of disk bounds!
					 * it's not really an error. it just means EOF... */
					if (UERR == EFAULT)
						UERR = 0;
#if DEBUG >= 3
 dprintf(3, "readwritei() returning, error %u\n", udata.u_error);
#endif
					return;
				}
			}
#if 1 /* Nick */
			/* this really should be revised to uint_min */
			amount = int_min(todo, BUFSIZE - uoff);
#else
			amount = Min(todo, BUFSIZE - uoff);
#endif
			if (write) {
				if (pblk == NULLBLK && isdevice(ino) == 0)
					return;	/* No space to make more blocks */
				/* If we are writing an entire block, we don't care
				 * about its previous contents
				 */
				bp = bread(dev, pblk, (amount == BUFSIZE));
				if (bp == NULL)
 {
#if DEBUG >= 3
 dprintf(3, "readwritei() returning, error %u\n", udata.u_error);
#endif
					return;
 }
#if 1 /* Nick */
				/* if System is in context or destination is in Common memory
				 * (F000-FFFF is always in context), simply copy the data.
				 * Otherwise perform Interbank move (uput) to User bank.
				 */
				if (udata.u_sysio || (udata.u_base < (char *)0x8000)) /* Nick >= (char *) 0xf000)) */
					bcopy (udata.u_base, bp + uoff, amount);
				else
					uget (udata.u_base, bp + uoff, amount);
#else
				bcopy(UBAS, bp + uoff, amount); 
#endif
				if (bfree((bufptr)bp, 2) < 0)
 {
#if DEBUG >= 3
 dprintf(3, "readwritei() returning, error %u\n", udata.u_error);
#endif
					return;
 }
			} else {
#if 1 /* Nick */
				/* if System is in context or destination is in Common memory
				 * (F000-FFFF is always in context), simply copy the data.
				 * Otherwise perform Interbank move (uput) to User bank.
				 */
				if (udata.u_sysio || (udata.u_base < (char *)0x8000)) /* Nick >= (char *)0xf000)) */
					bcopy (bp + uoff, udata.u_base, amount);
				else
					uput (bp + uoff, udata.u_base, amount);
#else
				bcopy(bp+uoff, UBAS, amount);
#endif
				if (brelse((bufptr)bp) < 0)
 {
#if DEBUG >= 3
 dprintf(3, "readwritei() returning, error %u\n", udata.u_error);
#endif
					return;
 }
			}
			UBAS = UBAS + amount;	/* HTC stuff */
			tdo = tdo + amount;
			addoff(&UOFFS, amount);
			todo = todo - amount;
#ifdef __KERNEL__
			if (ispipe) {
				if (ublk >= DIRECTBLOCKS)
					UOFFS &= (BUFSIZE - 1);
				addoff(&(ino->c_node.i_size),
				       write ? amount : -amount);
				/* Wake up any readers/writers */
				wakeup(ino);
			}
#endif	/* __KERNEL__ */
		}
		UCNT = tdo;
		/* Update size if file grew */
		if (write && !ispipe) {
			if (UOFFS > ISIZE(ino)) {
				ISIZE(ino) = UOFFS;
				ino->c_dirty = 1;
			}
		}
#if DEBUG >= 3
 dprintf(3, "readwritei() returning, success\n");
#endif
		return;
	default:
error:		UERR = ENODEV;
		if (!write)
			UCNT = -1;
#if DEBUG >= 3
 dprintf(3, "readwritei() returning, error %u\n", udata.u_error);
#endif
		return;
	}
}

/* rwsetup() prepare udata for read/write file */
inoptr rwsetup(uchar fd, void *base, uint cnt, uchar rdflag)
{
	register inoptr ino;
	register ofptr oftp;

#if DEBUG >= 3
 dprintf(3, "rwsetup(%u, 0x%x, %u, %u) starting\n",
         (unsigned int)fd, base, cnt, (unsigned int)rdflag);
#endif

	if ((ino = getinode(fd)) == NULL)	/* check fd */
 {
Err1:
#if DEBUG >= 3
 dprintf(3, "rwsetup() returning NULL, error %u\n", udata.u_error);
#endif
		return NULL;
 }
	oftp = of_tab + udata.u_files[fd];
	if (oftp->o_access == (rdflag ? O_WRONLY : O_RDONLY)) {
		UERR = EACCES;
		goto Err1;
	}
	setftim(ino, rdflag ? A_TIME : (A_TIME | M_TIME));
	/* Initialize u_offset from file pointer */
	UOFFS = oftp->o_ptr;
	UBAS = (uchar *)base;	/* buf */
	UCNT = cnt;		/* nbytes */
#if 1 /* Nick */
	udata.u_sysio = 0;	/* transfer is to be done on behalf of user */
#endif
#if DEBUG >= 3
 dprintf(3, "rwsetup() returning %u, success\n", ino - i_tab);
#endif
	return ino;
}

#if 0 /* FIX THIS! */
#ifdef __KERNEL__
#define tmpbuf ((udata_t *)TEMPDBUF)
int pdat(struct s_pdata *ubuf) {
	struct swap_mmread pp;
	register ptptr p = ptab;
#ifdef	PC_HOSTED
	udata_t TEMPDBUF[1];
#endif

	while (p < ptab+PTABSIZE) {
		if (p->p_pid == ubuf->u_pid && p->p_status != P_ZOMBIE) {
			if (p->p_status == P_RUNNING)
				bcopy(&ub, tmpbuf, sizeof(udata_t));
			else {
				pp.mm[0] = p->p_swap[0];
				pp.mm[1] = p->p_swap[1];
				pp.buf = (void *)tmpbuf;
				pp.size = sizeof(udata_t);
				pp.offset = (uint)p->p_udata;
				if (swap_mmread(&pp) < 0) {
					UERR = EIO;
					goto Err;
				}
			}
			ubuf->u_ptab = tmpbuf->u_ptab;
			bcopy(tmpbuf->u_name, ubuf->u_name, DIRNAMELEN);
			ubuf->u_insys = tmpbuf->u_insys;
			ubuf->u_callno = tmpbuf->u_callno;
#if DEBUG
			ubuf->u_traceme = tmpbuf->u_traceme;
#else
			ubuf->u_traceme = 0;
#endif
			ubuf->u_cursig = tmpbuf->u_cursig;
			ubuf->u_euid = tmpbuf->u_euid;
			ubuf->u_egid = tmpbuf->u_egid;
			ubuf->u_gid = tmpbuf->u_gid;
			ubuf->u_uid = p->p_uid;
			ubuf->u_time = tmpbuf->u_time;
			ubuf->u_utime = tmpbuf->u_utime;
			ubuf->u_stime = tmpbuf->u_stime;
			return 0;
		}
		++p;
	}
	UERR = ESRCH;
Err:	return -1;
}
#endif	/* __KERNEL__ */
#endif

/* This function is support for getfsys syscall and /dev/kmem */
#define ubuf ((struct s_pdata *)buf)
#if 1 /* Nick, new getfsys() convention */
info_t ibuf;
struct s_kdata kbuf;
int getfsys(dev_t devno, info_t *buf)
#else
#define kbuf ((struct s_kdata *)buf)
int getfsys(dev_t devno, void *buf)
#endif
	{
	fsptr dev = findfs(devno);
	register void *ptr = dev;
#if 1 /* Nick, new getfsys() convention */
	int cnt = sizeof(filesys_t);
#else
	uchar cnt = 1;
#endif

	if (dev == NULL)
		{
		switch (devno)
			{
		case GI_FTAB:	cnt = NFS;	ptr = fs_tab;		break;
		case GI_ITAB:	cnt = ITABSIZE; ptr = i_tab;		break;
		case GI_BTAB:	cnt = NBUFS;	ptr = bufpool;		break;
#ifdef __KERNEL__
		case GI_PTAB:	cnt = PTABSIZE; ptr = ptab;		break;
		case GI_UDAT:	cnt = 1;	ptr = &ub;		break;
		case GI_UTAB:	cnt = 1;	ptr = udata.u_ptab;	break;
#if 0 /* FIX THIS! */
		case GI_PDAT:	return pdat(ubuf);
#endif

#if 1 /* Nick, new getfsys() convention */
		case GI_KDAT:	bcopy(UZIX, kbuf.k_name, 14);
				bcopy(VERSION, kbuf.k_version, 8);
				bcopy(RELEASE, kbuf.k_release, 8);

				bcopy(HOST, kbuf.k_host, 14);
				bcopy(HOST_MACHINE, kbuf.k_machine, 8);

				kbuf.k_tmem = total * 16;
				kbuf.k_kmem = 32;	/* 32k kernel */

				cnt = sizeof(kbuf);
				ptr = &kbuf;
				goto getfsys_done;
#else
#if 1 /* Nick */
		case GI_KDAT:	uput(UZIX, kbuf->k_name, 14);
				uput(VERSION, kbuf->k_version, 8);
				uput(RELEASE, kbuf->k_release, 8);

				uput(HOST, kbuf->k_host, 14);
				uput(HOST_MACHINE, kbuf->k_machine, 8);

				uputw(total * 16, &kbuf->k_tmem);
				uputw(32, &kbuf->k_kmem); /* 32k kernel */
				goto Ok;
#else
		case GI_KDAT:	bcopy(UZIX, kbuf->k_name, 14);
				bcopy(VERSION, kbuf->k_version, 8);
				bcopy(RELEASE, kbuf->k_release, 8);

				bcopy(HOST, kbuf->k_host, 14);
				bcopy(HOST_MACHINE, kbuf->k_machine, 8);

				kbuf->k_tmem = total * 16;
				kbuf->k_kmem = 32;	/* 32k kernel */
				goto Ok;
#endif
#endif
#endif /* __KERNEL__ */
		default:
			UERR = ENXIO;
			return (-1);
			}

#if 1
		ibuf.size = cnt;
		ibuf.ptr = ptr;
		cnt = sizeof(ibuf);
		ptr = &ibuf;
#endif
		}

#if 1 /* Nick, new getfsys() convention */
getfsys_done:
	buf->size = cnt;
	buf->ptr = ptr;
#else
	((info_t *)buf)->size = cnt;
	((info_t *)buf)->ptr = ptr;
Ok:
#endif
	return 0;
	}
#undef tmpbuf
#if 0 /* Nick, new getfsys() convention */
#undef kbuf
#endif
#undef ubuf

#endif	/* NEED__SCALL */
