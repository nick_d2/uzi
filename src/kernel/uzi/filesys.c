/*
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 Filesystem related routines
**********************************************************/

#define NEED__DEVIO
#define NEED__FILESYS
#define NEED__MACHDEP
#define NEED__PROCESS
#define NEED__SCALL

#if 1 /* Nick */
#include "vendor.h" /* Nick, must come first */
#include <unix.h>
#include <config.h>
#include <extern.h>
#include "devio.h" /* prototypes added by Nick */
#include "filesys.h" /* prototypes added by Nick */
#include "xip.h" /* prototypes added by Nick */
#else /* Nick */
#include "uzix.h"
#ifdef SEPH
#include "types.h"
#include "signal.h"
#include "errno.h"
#include "sys\stat.h"
#endif
#include "unix.h"
#include "extern.h"
#endif /* Nick */

#if 1 /* Nick */
int int_min(int, int);
#endif

inoptr _namei(uchar *name, uchar **rest, inoptr strt, inoptr *parent);
fsptr getfs(dev_t devno);

/* Common messages. They are here to save memory */
static char *badinomsg = "%s: bad inode number %u";
char *baddevmsg = "%s: bad dev %u";
static char *badfsmsg = "%s: fs of dev %u marked as bad";
static char *refstoinode = "%s refs to inode %u";
static char *gtinobadoft = "getinode: bad oft %u%s %u";

/* Returns true if the magic number of a superblock is ok ??? not used */
#define gooddev(dev)		((dev)->s_mounted == SMOUNTED)

#define blockinodes(blk)	((blk) << DINODESPERBLOCKLOG)
#define devinodes(dev)		blockinodes((dev)->s_isize)
#define inodeblock(dev,ino)	(((ino) >> DINODESPERBLOCKLOG) + (dev)->s_reserv)
#define inodeoffset(ino)	((ino) & DINODESPERBLOCKMASK)

/* Check the given device number, and return its address in the mount table.
 */
fsptr findfs(dev_t devno)
{
	register fsptr dev = fs_tab;

	while (dev != fs_tab + NFS) {
		if (dev->s_mounted != 0 && dev->s_dev == devno)
			return dev;
		++dev;
	}
	return NULL;
}

/* Check the given device number, and return its address in the mount table.
 * Also time-stamp the superblock of dev, and mark it modified.
 * Used when freeing and allocating blocks and inodes.
 */
fsptr getfs(dev_t devno)
{
	fsptr dev = findfs(devno);

	if (dev == NULL)
		panic(baddevmsg, "getfs", devno);
	rdtime(&dev->s_time);	/* timestamp */
	dev->s_fmod = 1;	/* mark modified */
	return dev;
}

/* Wr_inode() writes out the given inode in the inode table out to disk,
 * and resets its dirty bit.
 * Attention! Immediate writing may be delayed by dirty_mask!
 */
int wr_inode(inoptr ino)
{
	dinode_t *buf;

#if DEBUG >= 3
 dprintf(3, "wr_inode(%u) starting\n", ino - i_tab);
#endif

	magic(ino, "wr_inode");
	if (ino->c_dirty && !ino->c_ro) {
		if ((buf = (dinode_t *)bread(ino->c_dev,
			inodeblock(findfs(ino->c_dev),
			ino->c_num), 0)) == NULL)
 {
Err:
#if DEBUG >= 3
 dprintf(3, "wr_inode() returning -1, error %u\n", udata.u_error);
#endif
			return -1;
 }
		bcopy(&ino->c_node, &buf[inodeoffset(ino->c_num)],
		      sizeof(dinode_t));
#if 1 /* Nick */
		/* write immediately, unless dirty_mask set before call */
		if (bfree((bufptr)buf, dirty_mask ? 1 : 2) < 0)
#else
		if (bfree((bufptr)buf, 2) < 0)	/* write immediately ! */
#endif
			goto Err;
	}
	ino->c_dirty = 0;	/* unmark modif flag */
#if DEBUG >= 3
 dprintf(3, "wr_inode() returning 0, success\n", udata.u_error);
#endif
	return 0;
}

/* I_ref() increases the reference count of the given inode table entry.
 */
void i_ref(inoptr ino)
{
#if DEBUG >= 4
 dprintf(4, "i_ref(%u) starting\n", ino - i_tab);
#endif

	magic(ino, "i_ref");
	if (++ino->c_refs >= 12 * ITABSIZE) /* Arbitrary limit */
		panic(refstoinode, "too many", ino - i_tab);
#if DEBUG >= 4
 dprintf(4, "i_ref() returning, refs %u\n", ino->c_refs);
#endif
}

/* I_deref() decreases the reference count of an inode, and frees it
 * from the table if there are no more references to it.  If it also
 * has no links, the inode itself and its blocks (if not a device) are freed.
 */
void i_deref(inoptr ino)
{
#if DEBUG >= 4
 dprintf(4, "i_deref(%u) starting\n", ino - i_tab);
#endif

	magic(ino, "i_deref");
	if (ino->c_refs == 0)
		panic(refstoinode, "no", ino);
	if (getmode(ino) == S_IFPIPE)
		wakeup(ino);
	/* If the inode has no links and no refs,
	 * it must have its blocks freed.
	 */
	if (--ino->c_refs == 0) {
		if (ino->c_node.i_nlink == 0) {
			ino->c_node.i_size = 0;
			f_trunc(ino);
			/* and also freeing this inode */
			ino->c_node.i_mode = 0;
			i_free(ino->c_dev, ino->c_num);
		}
		/* If the inode was modified, we must write it back to disk. */
		if (ino->c_dirty)
			wr_inode(ino);
	}
#if DEBUG >= 4
 dprintf(4, "i_deref() returning, refs %u\n", ino->c_refs);
#endif
}

/* I_free() is given a device and inode number, and frees the inode.
 * It is assumed that there are no references to the inode in the
 * inode table or in the filesystem.
 */
void i_free(dev_t devno, ino_t ino)
{
	register fsptr dev = getfs(devno);

#if DEBUG >= 4
 dprintf(4, "i_free(%u, %u) starting\n", devno, ino);
#endif

	if (ino <= ROOTINODE || ino >= devinodes(dev))
		panic(badinomsg, "i_free", ino);
#if 1 /* Nick free bitmap */
#if 0 /* remove support for the original UZI filesystem */
	if (dev->s_bitmap_inode < dev->s_bitmap_block)
		{
#endif
		if (bitmap_set(devno, ino, 0, dev->s_bitmap_inode,
				dev->s_bitmap_block) != 1)
			{
#if DEBUG >= 4
 dprintf(4, "i_free() returning, error\n");
#endif
			return; /* always an i/o error (or very bad !!) */
			}

		++dev->s_tinode;
#if 0 /* remove support for the original UZI filesystem */
#if DEBUG >= 4
 dprintf(4, "i_free() returning, success\n");
#endif
		return; /* crude way to skip inode list processing */
		}
#endif
#endif
#if 0 /* remove support for the original UZI filesystem */
	++dev->s_tinode;	/* total free inodes */
	/* account to free inodes list if it's possible */
	if (dev->s_ninode < FSFREEINODES)
		dev->s_inode[dev->s_ninode++] = ino;
#endif
#if DEBUG >= 4
 dprintf(4, "i_free() returning, success\n");
#endif
}

/* _namei() is given a string containing a path name,
 * and returns an inode table pointer.
 * If it returns NULL, the file did not exist.
 * If the parent existed, 'parent' will be filled in with the
 * parents inoptr. Otherwise, 'parent' will be set to NULL.
 */
#if 0 /* Nick */
inoptr _namei(uchar *uname, uchar **rest, inoptr strt, inoptr *parent)
#else
inoptr _namei(uchar *name, uchar **rest, inoptr strt, inoptr *parent)
#endif
{
	inoptr temp;
	register inoptr ninode; /* next dir */
	register inoptr wd;	/* the directory we are currently searching. */
#if 0 /* Nick */
	char *tb;
	char *name;        /* Grab a block to copy the name from user space */

	name = tb = (char *)tmpbuf();
#if 0
	if (ugets (uname, name, 512))
	{
		brelse((bufptr)tb);
		return NULLINODE;   /* Source String exceeded available length */
	}
#else
	ugets (uname, name, 512);		/*--*--*/
#endif
#endif

	ninode = wd = strt;
	i_ref(wd);
	i_ref(ninode);
	for (;;) {
		if (ninode != NULL) {
			magic(ninode, "_namei");
			/* See if we are at a mount point */
			ninode = srch_mt(ninode);
		}
		while (*name == '/')	/* Skip (possibly repeated) slashes */
			++name;
		if (*name == 0) 	/* No more components of path? */
			break;
		if (ninode == NULL) {	/* path not found */
			udata.u_error = ENOENT;
			goto Err;
		}
		if (getmode(ninode) == S_IFLNK)
			break;		/* symlinks processed separately */
		i_deref(wd);		/* drop current dir */
		wd = ninode;		/* switch to next inode */
		/* this node must be directory ... */
		if (getmode(wd) != S_IFDIR) {
			udata.u_error = ENOTDIR;
			goto Err;
		}
		/* ... enabled for searching */
		if ((getperm(wd) & S_IOEXEC) == 0) {
			udata.u_error = EPERM;
			goto Err;
		}
		/* See if we are going up through a mount point */
		if (wd->c_num == ROOTINODE &&
		    wd->c_dev != root_dev &&
		    name[1] == '.') {
			/* switch to other fsys */
			temp = findfs(wd->c_dev)->s_mntpt;
			i_deref(wd);
			i_ref(wd = temp);
		}
		/* search for next part of path */
		if ((ninode = srch_dir(wd, (char *)name)) == (inoptr)-1)
			goto Err;
		while (*name != 0 && *name != '/')
			++name; 	/* skip this part */
	}
	*parent = wd;			/* store ref to parent */
#if 0 /* Nick */
	*rest = uname + (name - tb);
	brelse((bufptr)tb);
#else
	*rest = name;
#endif
	return ninode;

Err:	*parent = NULL;
	i_deref(wd);
#if 0 /* Nick */
	brelse((bufptr)tb);
#endif
	return NULL;
}

/* namei() is given a string containing a path name,
 * and returns an inode table pointer.
 * If it returns NULL, the file did not exist.
 * If the parent existed, and 'parent' is not null, 'parent'
 * will be filled in with the parents inoptr.
 * Otherwise, 'parent' will be set to NULL.
 * If 'follow' is false then tail symlink is not opened
 */
#if 1 /* Nick */
inoptr namei(char *uname, inoptr *parent, uchar follow)
#else
inoptr namei(char *name, inoptr *parent, uchar follow)
#endif
{
	register inoptr ino;
	inoptr parent1, strt;
	uchar *buf = NULL, *buf1 = NULL, *rest, *s;
	uchar cnt = 0;
#if 1 /* Nick */
	char *tb;
	char *name;        /* Grab a block to copy the name from user space */

#if 0
	if (uname == NULL) {
		udata.u_error = ENOENT;
		return NULL;
	}
#endif
	name = tb = (char *)tmpbuf();
#if 0
	if (ugets(uname, name, 512))
		{
		brelse((bufptr)tb);
		return NULLINODE;   /* String exceeded available length */
		}
#else
	ugets(uname, name, 512);
#endif
#endif
	udata.u_error = 0;
#if 0 /* Nick */
	if (name == NULL) {
		udata.u_error = ENOENT;
		ino = NULL;
		goto Ret;
	}
#endif
	strt = (*name == '/') ? udata.u_root : udata.u_cwd;
	while ((ino = _namei((uchar *)name, &rest, strt, &parent1)) != NULL) {
		if (getmode(ino) != S_IFLNK || (*rest == 0 && !follow)) {
			if (parent != NULL)
				*parent = parent1;
			else	i_deref(parent1);
			goto Ret;
		}
		/* check symlink is readable */
		if (!(getperm(ino) & S_IOREAD)) {
			udata.u_error = EPERM;
Err:			i_deref(parent1);
			i_deref(ino);
			ino = NULL;
			goto Ret;
		}
#if 0 /* NICK TEMPORARY! PLEASE CHANGE THIS BACK! */
		if (!buf) if ((buf = (uchar *)zerobuf()) == NULL) goto Err1;
#else
		if (!buf) if ((buf = (uchar *)zerobuf(1)) == NULL) goto Err1;
#endif
		if (*rest) {	/* need the temporary buffer */
#if 0 /* NICK TEMPORARY! PLEASE CHANGE THIS BACK! */
			if (!buf1)	buf1 = (uchar *)zerobuf();
#else
			if (!buf1)	buf1 = (uchar *)zerobuf(0);
#endif
			if (!buf1) {
Err1:				udata.u_error = ENOMEM;
				goto Ret;
			}
			s = buf1;
			while ('\0' != (*s++ = *rest++))
				;
			rest = buf1;
		}
		else	rest = (uchar *)"";	/* ! It's important - really "" ! */
		/* readin symlink contents */
		udata.u_count = BUFSIZE;
		udata.u_base = (uchar *)buf;
		udata.u_offset = 0;
#if 1 /* Nick */
		udata.u_sysio = 1;
#endif
		readi(ino);
		/* check for buffer size */
		s = rest;
		while (*s++ != '\0')
			;
		if (udata.u_count + (s - rest) >= BUFSIZE-1) {
			udata.u_error = ENOMEM;
			goto Err;
		}
		/* create substituted name */
		s = buf+udata.u_count;
		if (*rest)
			*s++ = '/';
		while ((*s++ = *rest++) != '\0')
			;
		name = (char *)buf;
		strt = (*name == '/') ? root_ino : parent1;
		if (++cnt == 13) {	/* recursion too deep */
			udata.u_error = ELOOP;
			goto Err;
		}
		i_deref(parent1);
		i_deref(ino);
	}
	/* name not found (ino == NULL) */
	if (parent != NULL)
		*parent = parent1;	/* store ref to parent */
	else {	/* we want existent file! */
		if (parent1)
			i_deref(parent1); /* deref node */
		if (udata.u_error == 0)
			udata.u_error = ENOENT;
	}
Ret:	if (buf) if (brelse((bufptr)buf) < 0) ino=NULL;
	if (buf1) if (brelse((bufptr)buf1) < 0) ino=NULL;
#if 1 /* Nick */
	brelse((bufptr)tb);
#endif
	return ino;
}

/* Srch_dir() is given a inode pointer of an open directory and a string
 * containing a filename, and searches the directory for the file.
 * If it exists, it opens it and returns the inode pointer, otherwise NULL.
 * If an error occur, Srch_dir() returns -1.
 *
 * This depends on the fact that bread will return unallocated blocks as
 * zero-filled, and a partially allocated block will be padded with zeroes.
 */
inoptr srch_dir(inoptr wd, char *name)
{
	ino_t ino;
	blkno_t db;
	uint cb, ce, nblocks;
	register direct_t *bp, *buf;

	nblocks = (uint)(wd->c_node.i_size >> BUFSIZELOG); /* # of full blocks */
	if ((uint)wd->c_node.i_size & (BUFSIZE-1))
		++nblocks;			/* and part of last block */
	cb = 0;
	while (cb != nblocks) {
		/* map file block to fs */
		if ((db = bmap(wd, cb, 1)) == NULLBLK)
			break;
		if ((buf = bp = (direct_t *)bread(wd->c_dev, db, 0)) == NULL)
Err1:			return (inoptr)-1;
		ce = 0;
		while (ce < DIRECTPERBLOCK) {
			if (namecomp((uchar *)name, bp->d_name)) {
				/* file name found */
				ino = bp->d_ino; /* inode of file */
				if (brelse((bufptr)buf) < 0) goto Err1;
				/* open the found file */
				return i_open(wd->c_dev, ino);
			}
			++ce;
			++bp;
		}
		if (brelse((bufptr)buf) < 0)	/* need next block */
			goto Err1;
		++cb;
	}
	/* file not found in this directory */
	return NULL;
}

/* Srch_mt() sees if the given inode is a mount point.
 * If so it dereferences it, and references and returns a pointer
 * to the root of the mounted filesystem.
 */
inoptr srch_mt(inoptr ino)
{
	register fsptr dev = fs_tab;

	while (dev != fs_tab + NFS) {
		if (dev->s_mounted != 0 && dev->s_mntpt == ino) {
			i_deref(ino);
			ino = i_open(dev->s_dev, ROOTINODE);
			/* return replaced inode */
			break;
		}
		++dev;
	}
	return ino;
}

/* I_open() is given an inode number and a device number, and makes an entry
 * in the inode table for them, or increases it reference count if it is
 * already there.
 *
 * An inode == NULLINO means a newly allocated inode.
 */
inoptr i_open(dev_t devno, ino_t ino)
{
	fsptr dev;
	dinode_t *buf;
	inoptr ip, nindex;
	uchar i, newn, mode0;
#if 1 /* Nick free bitmap */
	uchar refs0;
#endif
	static inoptr nexti = i_tab;	/* rover ptr */

#if DEBUG >= 3
 dprintf(3, "i_open(%u, %u) starting\n", devno, ino);
#endif

	if ((dev = findfs(devno)) == NULL)
		panic(baddevmsg, "i_open", devno);
	newn = 0;
	if (ino == NULLINO) {		/* Wanted a new one */
		if ((ino = i_alloc(devno)) == NULLINO)
			goto Err;	/* errno is already set by i_alloc */
		++newn;
	}
/* dprintf(2, "ino = %u\n", ino); */
/* dprintf(2, "dev -> %u\n", dev - fs_tab); */
/* dprintf(2, "devinodes(dev) = %u\n", devinodes(dev)); */
	if (ino < ROOTINODE || ino >= devinodes(dev)) {
		warning(badinomsg, "i_open", ino);
		goto Err;
	}
	/* search inode table for available entry */
	nindex = NULL;
	i = 0;
	ip = nexti;
/* dprintf(2, "{ "); */
	while (i != ITABSIZE) {
		nexti = ip;
		if (++ip >= i_tab + ITABSIZE)
			ip = i_tab;
/* dprintf(2, "%u ", ip - i_tab); */
		if (ip->c_refs == 0)
			nindex = ip;	/* candidate for discarding */
		if (ip->c_dev == devno && ip->c_num == ino) {
			nindex = ip;
/* dprintf(2, "} "); */
			goto found;	/* really found */
		}
		++i;
	}
/* dprintf(2, "} "); */
	/* Not already in table - take last candidate */
	if (nindex == NULL) {	/* No unrefed slots in inode table */
		udata.u_error = ENFILE;
		goto Err;
	}
/* dprintf(2, "reading(%u, %u) ", inodeblock(dev, ino), inodeoffset(ino)); */
	/* discard oldest? inode from table and read the inode from disk */
	buf = (dinode_t *)bread(devno, inodeblock(dev, ino), 0);
	if (buf == NULL) goto Err;
	bcopy(&buf[inodeoffset(ino)], &nindex->c_node, sizeof(dinode_t));
	if (brelse((bufptr)buf) < 0) goto Err;
	/* fill-in in-core parts of inode */
	nindex->c_magic = CMAGIC;
	nindex->c_dev = devno;
	nindex->c_num = ino;
	nindex->c_ro = dev->s_ronly;
found:
/* dprintf(2, "found(%u, 0%o) ", nindex->c_node.i_nlink, nindex->c_node.i_mode); */
	mode0 = (getmode(nindex) == 0);
#if 1 /* Nick free bitmap */
	/* need to check for the special case of working on an erased file */
	refs0 = (nindex->c_refs == 0) && (nindex->c_node.i_nlink == 0);
#endif
	if (newn) {	/* RO fs can't do i_alloc()! */
		/* newly allocated disk inode must be clean */
#if 1 /* Nick free bitmap */
		if (!refs0 || !mode0)
#else
		if (nindex->c_node.i_nlink || !mode0)
#endif
			goto badino;
	}	/* and vice versa */
#if 1 /* Nick free bitmap */
	else if (refs0 || mode0)
#else
	else if (nindex->c_node.i_nlink == 0 || mode0)
#endif
		goto badino;
	i_ref(nindex);		/* yet one ref */
#if DEBUG >= 3
 dprintf(3, "i_open() returning %u, success\n", nindex - i_tab);
#endif
	return nindex;

badino: warning(badinomsg, "i_open (disk)", ino); /* nindex - i_tab); */
Err:
#if DEBUG >= 3
 dprintf(3, "i_open() returning NULL, error %u\n", udata.u_error);
#endif
	return NULL;
}

/* Ch_link() modifies or makes a new entry in the directory for the name
 * and inode pointer given. The directory is searched for oldname.
 * When found, it is changed to newname, and it inode # is that of *nindex.
 *
 * An empty oldname ("") matches a unused slot.
 * A nindex NULL means an inode #0.
 *
 * A return status of 0 means there was no space left in the filesystem,
 * or a non-empty oldname was not found, or the user did not have write
 * permission.
 */
int ch_link(inoptr wd, char *oldname, char *newname, inoptr nindex)
{
	direct_t curentry;
	int i;

#if DEBUG >= 3
 dprintf(3, "ch_link(%u, \"%s\", \"%s\", %u) starting\n",
         wd - i_tab, oldname, newname, nindex - i_tab);
#endif

	/* Need the write permissions */
	if ((getperm(wd) & S_IOWRITE) == 0) {
		udata.u_error = EPERM;
		goto Err;
	}
	/* Search the directory for the desired slot */
	udata.u_offset = 0;	/* from beginning */
	for (;;) {
		udata.u_base = (uchar *)&curentry;
		udata.u_count = sizeof(curentry);
#if 1 /* Nick */
		udata.u_sysio = 1;
#endif
		readi(wd);
		/* Read until EOF or name is found */
		/* readi() advances udata.u_offset */
		if (udata.u_count == 0 ||
		    namecomp((uchar *)oldname, curentry.d_name))
			break;
	}
	if (udata.u_count == 0 && *oldname)
#if 1 /* Nick's addition due to a newfile() path which didn't set u_error */
		{
		udata.u_error = ENOENT;
		goto Err;	/* Rename and old entry not found */
		}
#else
		goto Err;	/* Rename and old entry not found */
#endif
	oldname = (char *)curentry.d_name;
	i = sizeof(curentry.d_name);
	while (--i >= 0) {
		if ('\0' != (*oldname++ = *newname))
			++newname;
	}
	curentry.d_ino = nindex ? nindex->c_num : 0;
	/* If an existing slot is being used,
	 * we must back up the file offset
	 */
	if (udata.u_count > 0)
		udata.u_offset -= udata.u_count;
	udata.u_base = (uchar *)&curentry;
	udata.u_count = sizeof(curentry);
	udata.u_error = 0;
#if 1 /* Nick */
	udata.u_sysio = 1;
#endif
	writei(wd);	/* write back updated directory */
	if (udata.u_error)
 {
Err:
#if DEBUG >= 3
 dprintf(3, "ch_link() returning 0, error %u\n", udata.u_error);
#endif
		return 0;
 }
	/* Update directory length to next block - simple way to extend dir */
	if ((uint)wd->c_node.i_size & (BUFSIZE-1)) {
		wd->c_node.i_size &= ~(BUFSIZE-1);
		wd->c_node.i_size += BUFSIZE;
	}
	setftim(wd, A_TIME | M_TIME | C_TIME); /* And sets c_dirty */
#if DEBUG >= 3
 dprintf(3, "ch_link() returning 1, success\n");
#endif
	return 1;
}

#if 1 /* Nick */
/* Filename is given a path name in user space, and copies
 * the final component of it to name (in system space).
 */

void filename(char *upath, char *name)
{
    char *buf;
    register char *ptr;
/* Nick    char *tmpbuf(); */
/* Nick    int  ugets(); */

    buf = (char *)tmpbuf();
    if (ugets(upath, buf, 512))
    {
        brelse((bufptr)buf);
        *name = '\0';
        return;          /* An access violation reading the name */
    }
    ptr = buf;
    while (*ptr)
        ++ptr;
    /* Special case for "...name.../" */
    while (*ptr != '/' && ptr-- > buf)
        ;
    ptr++;
    bcopy(ptr, name, DIRNAMELEN);
    brelse((bufptr)buf);
}
#else
/* Filename() is given a path name, and returns a pointer
 * to the final component of it.
 */
char *filename(char *path)
{
	register char *ptr = path;

	while (*ptr)		/* find end of pathname */
		++ptr;
	while (*ptr != '/' && ptr >= path)
		--ptr;		/* traverse back */
	return (ptr + 1);
}
#endif

/* Namecomp() compares two strings to see if they are the same file name.
 * It stops at DIRNAMELEN chars or a null or a slash.
 * It returns 0 for difference.
 */
int namecomp(uchar *n1, uchar *n2)
{
	uchar n = DIRNAMELEN;

#if DEBUG >= 5
 dprintf(5, "namecomp(\"%s\", \"%s\") starting\n", n1, n2);
#endif

	while (*n1 && *n1 != '/') {
		if (*n1++ != *n2++)
			goto NotEq;
		if (--n == 0)
 {
#if DEBUG >= 5
 dprintf(5, "namecomp() returning -1, error\n");
#endif
			return (-1);	/* first name too long - ignore this */
 }
	}
	if (*n2 == '\0' || *n2 == '/')
 {
#if DEBUG >= 5
 dprintf(5, "namecomp() returning -1, match\n");
#endif
		return 1;		/* names matched */
 }
NotEq:
#if DEBUG >= 5
 dprintf(5, "namecomp() returning 0, no match\n");
#endif
	return 0;
}

/* Newfile() is given a pointer to a directory and a name, and
 * creates an entry in the directory for the name, dereferences
 * the parent, and returns a pointer to the new inode.
 *
 * It allocates an inode number, and creates a new entry in the inode
 * table for the new file, and initializes the inode table entry for
 * the new file.  The new file will have one reference, and 0 links to it.
 *
 * Better make sure there isn't already an entry with the same name.
 */
inoptr newfile(inoptr pino, char *name)
{
	register inoptr nindex;

#if DEBUG >= 3
 dprintf(3, "newfile(%u, \"%s\") starting\n", pino - i_tab, name);
#endif

	if ((getperm(pino) & S_IOWRITE) == 0) {
		udata.u_error = EPERM;
#if DEBUG >= 3
 dprintf(3, "newfile() returning NULL, error %u\n", udata.u_error);
#endif
		return 0;
	}
	if ((nindex = i_open(pino->c_dev, NULLINO)) == NULL)
 {
#if DEBUG >= 3
 dprintf(3, "newfile() returning NULL, error %u\n", udata.u_error);
#endif
		goto Ret;			/* can't create new inode */
 }
	/* fill-in new inode */
	bzero(&nindex->c_node,sizeof(dinode_t));
	nindex->c_node.i_mode = S_IFREG;	/* For the time being */
	nindex->c_node.i_nlink = 1;
	nindex->c_node.i_uid = udata.u_euid;	/* File owner */
	nindex->c_node.i_gid = udata.u_egid;
	nindex->c_dirty = 1;
	wr_inode(nindex);
#if 1 /* Nick now done by caller */
	if (ch_link(pino, "", name, nindex) == 0) {
#else
	if (ch_link(pino, "", filename(name), nindex) == 0) {
#endif
		i_deref(nindex);	/* can't enter new file to directory */
		nindex = NULL;
#if DEBUG >= 3
 dprintf(3, "newfile() returning NULL, error %u\n", udata.u_error);
#endif
	}
#if DEBUG >= 3
 else
  {
  dprintf(3, "newfile() returning %u, success\n", nindex - i_tab);
  }
#endif
Ret:	i_deref(pino);
	return nindex;
}

/* doclose() given the file descriptor and close them */
int doclose(uchar fd)
{
	register inoptr ino = getinode(fd);
	uchar oftindex;

#if DEBUG >= 3
 dprintf(3, "doclose(%u) starting\n", (unsigned int)fd);
#endif

	if (ino == NULL)
 {
#if DEBUG >= 3
 dprintf(3, "doclose() returning -1, error\n");
#endif
		return (-1);
 }
	oftindex = udata.u_files[fd];
	if (isdevice(ino)
	    && ino->c_refs == 1
	    && of_tab[oftindex].o_refs == 1)
		d_close(DEVNUM(ino));
	udata.u_files[fd] = -1;
	oft_deref(oftindex);
#if DEBUG >= 3
 dprintf(3, "doclose() returning 0, success\n");
#endif
	return 0;
}

/* I_alloc() finds an unused inode number, and returns it,
 * or NULLINO if there are no more inodes available.
 */
ino_t i_alloc(dev_t devno)
{
	ino_t ino;
	blkno_t blk;
	register uchar j, k;
	dinode_t *buf, *bp;
	fsptr dev = getfs(devno);

#if DEBUG >= 4
 dprintf(4, "i_alloc(%u) starting\n", devno);
#endif

	if (dev->s_ronly) {
		udata.u_error = EROFS;
		goto Err1;
	}
#if 1 /* Nick free bitmap */
#if 0 /* remove support for the original UZI filesystem */
	if (dev->s_bitmap_inode < dev->s_bitmap_block)
		{
#endif
		ino = bitmap_find(devno, 0, 0, 1, dev->s_bitmap_inode,
						  dev->s_bitmap_block);
		if (ino <= ROOTINODE || ino >= devinodes(dev))
			{
			goto Err; /* should distinguish disk i/o errors !! */
			}

		if (dev->s_tinode == 0)
			{
			goto Corrupt; /* fs is corrupt, not just full! */
			}

#if 0 /* remove support for the original UZI filesystem */
		goto found_inode; /* crude way to skip inode list processing */
		}
#endif
#endif
#if 0 /* remove support for the original UZI filesystem */
Again:	if (dev->s_ninode) {	/* there are available inodes in list */
		if (dev->s_tinode == 0)
			goto Corrupt; /* fs is corrupt, not just full! */
		/* get free inode from list */
		ino = dev->s_inode[--dev->s_ninode];
#if 1 /* Nick free bitmap */
found_inode:
#endif
#endif
		if (ino <= ROOTINODE || ino >= devinodes(dev))
			goto Corrupt;
		--dev->s_tinode;
#if DEBUG >= 4
 dprintf(4, "i_alloc() returning %u, success\n", ino);
#endif
		return ino;
#if 0 /* remove support for the original UZI filesystem */
	}
	/* There are no avilable inodes in list - we must scan the
	 * disk inodes, and fill up the table
	 */
	sys_sync();	/* Make on-disk inodes consistent */
	k = 0;
	blk = 0;
/* dprintf(2, "[ "); */
	while (blk != dev->s_isize) {
		if ((bp = buf = (dinode_t *)bread(devno, blk+dev->s_reserv, 0)) == NULL)
			goto Err;
		j = 0;
		while (j != DINODESPERBLOCK) {
			if (bp->i_mode == 0 && bp->i_nlink == 0)
/* { */
/* dprintf(2, "%d ", blockinodes(blk) + j); */
				dev->s_inode[k++] = blockinodes(blk) + j;
/* } */
			if (k == FSFREEINODES) {
				if (brelse((bufptr)buf) < 0) goto Err;
				goto Done;
			}
			++bp;
			++j;
		}
		++blk;
		if (brelse((bufptr)buf) < 0) goto Err;
		if (k >= DINODESPERBLOCK-2)		/* ??? */
			break;
	}
Done:
/* dprintf(2, "] "); */
	if (k == 0) {	/* no free inodes on disk */
		if (dev->s_tinode)
			goto Corrupt;
		goto Err; /* not a fatal error, the table is full */
	}
	dev->s_ninode = k;
	goto Again;
#endif

Corrupt:warning(badfsmsg, "i_alloc", devno);
	dev->s_mounted = ~SMOUNTED;	/* mark device as bad */
Err:	udata.u_error = ENOSPC;
Err1:
#if DEBUG >= 4
 dprintf(4, "i_alloc() returning NULLINO, error %u\n", udata.u_error);
#endif
	return NULLINO;
}

/* Blk_alloc() is given a device number, and allocates an unused block
 * from it. A returned block number of zero means no more blocks.
 * new: if the caller passes dirty = 1, don't write back immediately
 * new: if the caller passes dirty = 2, write back the zeros immediately
 * (the latter mode is used when allocating new indirect/dindirect blocks)
 */
blkno_t blk_alloc(dev_t devno, uchar dirty) /* Nick dirty */
{
#if 1 /* Nick, don't use nf variable unnecessarily */
	char *buf;
	blkno_t newno;
#else
	count_t nf;
	blkno_t *buf, newno;
#endif
	register fsptr dev = getfs(devno);
/* int i; */

#if DEBUG >= 4
 dprintf(4, "blk_alloc(%u, %u) starting\n", devno, (unsigned)dirty);
#endif

	if (dev->s_ronly) {
		udata.u_error = EROFS;
		goto Err1;
	}
#if 1 /* Nick free bitmap */
#if 0 /* remove support for the original UZI filesystem */
	if (dev->s_bitmap_block < dev->s_bitmap_immov)
		{
#endif
		newno = bitmap_find(devno, 0, 0, 1, dev->s_bitmap_block,
						    dev->s_bitmap_immov);
		if (newno < (dev->s_reserv + dev->s_isize) ||
				newno >= dev->s_fsize)
			{
			goto Err; /* should distinguish disk i/o errors !! */
			}

#if 0 /* remove support for the original UZI filesystem */
		goto found_block; /* crude way to skip free list processing */
		}
#endif
#endif
#if 0 /* remove support for the original UZI filesystem */
#if 1 /* Nick, don't use nf variable unnecessarily */
	if (dev->s_nfree <= 0 || dev->s_nfree > FSFREEBLOCKS)
		goto Corrupt;
#else
	nf = dev->s_nfree;
/* dprintf(2, "nf = %u\n", nf); */
	if (nf <= 0 || nf > FSFREEBLOCKS)
/* { */
/* dprintf(2, "nf out of range\n"); */
		goto Corrupt;
/* } */
#endif
	if (0 == (newno = dev->s_free[--dev->s_nfree])) {
		if (dev->s_tfree != 0)
/* { */
/* dprintf(2, "dev->s_tfree != 0\n"); */
			goto Corrupt;
/* } */
		++dev->s_nfree;
		goto Err;
	}
	/* See if we must refill the s_free array */
	if (dev->s_nfree == 0) {
		if ((buf = bread(devno, newno, 0)) == NULL)
			goto Err;
/* dprintf(2, "buf[0] = %u\n", ((blkno_t *)buf)[0]); */
		dev->s_nfree = ((blkno_t *)buf)[0];
		bcopy(((blkno_t *)buf)+1,dev->s_free,sizeof(dev->s_free));
/* dprintf(2, "< "); */
/* for (i = 0; i < dev->s_nfree; i++) */
/*  { */
/*  dprintf(2, "%u ", dev->s_free[i]); */
/*  } */
/* dprintf(2, "> "); */
		if (brelse((bufptr)buf) < 0)
			goto Err;
	}
#if 1 /* Nick free bitmap */
found_block:
#endif
#endif
	validblk(devno, newno);
	if (dev->s_tfree == 0)
/* { */
/* dprintf(2, "dev->s_tfree == 0\n"); */
		goto Corrupt;
/* } */
	--dev->s_tfree;
	/* Zero out the new block */
	if ((buf = bread(devno, newno, 2)) == NULL) 	/* 2 for zeroing */
		goto Err;
#if 1
	if (bfree((bufptr)buf, dirty) < 0)		/* write back */
#else
	if (bawrite((bufptr)buf) < 0)			/* write back */
#endif
		goto Err;
#if DEBUG >= 4
 dprintf(4, "blk_alloc() returning %u, success\n", newno);
#endif
	return newno;

Corrupt:warning(badfsmsg, "blk_alloc", devno);
	dev->s_mounted = ~SMOUNTED;
Err:	udata.u_error = ENOSPC;
Err1:
#if DEBUG >= 3
 dprintf(3, "blk_alloc() returning 0, error %u\n", udata.u_error);
#endif
	return 0;
}

/* Blk_free() is given a device number and a block number,
 * and frees the block.
 */
void blk_free(dev_t devno, blkno_t blk)
{
#if 1 /* Nick free bitmap */
	blkno_t j;
	char *buf, *p;
#else
	blkno_t *buf;
#endif
	register fsptr dev = getfs(devno);

#if DEBUG >= 4
 dprintf(4, "blk_free(%u, %u) starting\n", devno, blk);
#endif

	if (dev->s_ronly || blk == 0)
 {
End:
#if DEBUG >= 4
 dprintf(4, "blk_free() returning, error\n");
#endif
		return;
 }
	validblk(devno, blk);
#if 1 /* Nick free bitmap */
#if 0 /* remove support for the original UZI filesystem */
	if (dev->s_bitmap_block < dev->s_bitmap_immov)
		{
#endif
		if (bitmap_set(devno, blk, 0, dev->s_bitmap_block,
					      dev->s_bitmap_immov) != 1)
			{
			goto End; /* always an i/o error (or very bad !!) */
			}

#if 1 /* temporary only... also unreserve the block if it was reserved */
		if (bitmap_set(devno, blk, 0, dev->s_bitmap_immov,
					      dev->s_bitmap_final) == -1)
			{
			goto End; /* always an i/o error (or very bad !!) */
			}
#endif

		++dev->s_tfree;
#if 0 /* remove support for the original UZI filesystem */
#if DEBUG >= 4
 dprintf(4, "blk_free() returning, success\n");
#endif
		return; /* crude way to skip free list processing */
		}
#endif
#endif
#if 0 /* remove support for the original UZI filesystem */
	/* flushing overflowed free blocks list  */
	if (dev->s_nfree == FSFREEBLOCKS) {
		buf = bread(devno, blk, 1);
		if (buf == NULL)
			goto End;
		((blkno_t *)buf)[0] = dev->s_nfree;
		bcopy(dev->s_free, ((blkno_t *)buf)+1, sizeof(dev->s_free));
		if (bawrite((bufptr)buf) < 0)
			goto End;
		dev->s_nfree = 0;
	}
	++dev->s_tfree;
	dev->s_free[dev->s_nfree++] = blk;
#endif
#if DEBUG >= 4
 dprintf(4, "blk_free() returning, success\n");
#endif
}

/* Oft_alloc() allocate entries in the open file table.
 */
uchar oft_alloc(void)
{
	register ofptr op = of_tab;
	uchar j = 0;

#if DEBUG >= 4
 dprintf(4, "oft_alloc() starting\n");
#endif

	while (j != OFTSIZE) {
		if (op->o_refs == 0) {
			bzero(op, sizeof(oft_t)); /* zero out all fields */
			op->o_refs++;
#if DEBUG >= 4
 dprintf(4, "oft_alloc() returning %u, success\n", (unsigned int)j);
#endif
			return j;
		}
		++op;
		++j;
	}
	udata.u_error = ENFILE;
#if DEBUG >= 4
 dprintf(4, "oft_alloc() returning -1, error %u\n", udata.u_error);
#endif
	return (-1);
}

/* Oft_deref() dereference (and possibly free attached inode)
 * entries in the open file table.
 */
void oft_deref(uchar of)
{
	register ofptr op = of_tab + of;

#if DEBUG >= 4
 dprintf(4, "oft_deref(%u) starting\n", (unsigned int)of);
#endif

	if (--op->o_refs == 0 && op->o_inode != NULL) {
		i_deref(op->o_inode);
		op->o_inode = NULL;
	}
#if DEBUG >= 4
 dprintf(4, "oft_deref() returning, refs %u\n", op->o_refs);
#endif
}

/* Uf_alloc() finds an unused slot in the user file table.
 */
uchar uf_alloc(void)
{
	register uchar j = 0;
	register uchar *p = udata.u_files;

#if DEBUG >= 4
 dprintf(4, "uf_alloc() starting\n");
#endif

	while (j != UFTSIZE) {
		if (freefileentry(*p))
 {
#if DEBUG >= 4
 dprintf(4, "uf_alloc() returning %u, success\n", (unsigned int)j);
#endif
			return j;
 }
		++p;
		++j;
	}
	udata.u_error = ENFILE;
#if DEBUG >= 4
 dprintf(4, "uf_alloc() returning -1, error %u\n", udata.u_error);
#endif
	return (-1);
}

/* isdevice() returns true if ino points to a device
 * old: !!! assume S_IFBLK == S_IFCHR|any
 * new: checks for S_IFBLK and S_IFCHR separately
 */
uchar isdevice(inoptr ino)
{
#if 1 /* Nick, to avoid conflict with S_IFALIGN and other new filetypes */
	return S_ISDEV(ino->c_node.i_mode);
#else
	return (ino->c_node.i_mode & S_IFCHR) != 0;
#endif
}

/* Freeblk() - free (possible recursive for (double)inderect) block
 */
void freeblk(dev_t dev, blkno_t blk, uchar level)
{
#if DEBUG >= 3
 dprintf(3, "freeblk(%u, %u, %u) starting\n", dev, blk, (unsigned int)level);
#endif

	if (blk != 0) {
		if (level != 0) {	/* INDERECT/DINDERECT block */
			register blkno_t *buf = (blkno_t *)bread(dev, blk, 0);
			uint j;

			--level;
			if ((buf) != NULL) {
				j = BUFSIZE/sizeof(blkno_t);
				while (j != 0)
					freeblk(dev, buf[--j], level);
				brelse((bufptr)buf);
			}
		}
		blk_free(dev, blk);
#if DEBUG >= 3
 dprintf(3, "freeblk() returning, success\n");
#endif
	}
#if DEBUG >= 3
 else
  {
  dprintf(3, "freeblk() returning, error\n");
  }
#endif
}

/* F_trunc() frees all the blocks associated with the file,
 * if it is a disk file.
 * need enhancement for truncating to given size
 */
void f_trunc(inoptr ino)
{
	dinode_t *ip = &ino->c_node;
	blkno_t *blk = ip->i_addr;
	register uchar j;

#if DEBUG >= 3
 dprintf(3, "f_trunc(%u) starting\n", ino - i_tab);
#endif

	/* First deallocate the double indirect blocks */
	freeblk(ino->c_dev, blk[TOTALREFBLOCKS-1], 2);
	/* Also deallocate the indirect blocks */
	freeblk(ino->c_dev, blk[TOTALREFBLOCKS-1-DINDIRECTBLOCKS], 1);
	/* Finally, free the direct blocks */
	j = 0;
	while (j != DIRECTBLOCKS) {
		freeblk(ino->c_dev, *blk++, 0);
		++j;
	}
	bzero(ip->i_addr, sizeof(blkno_t)*TOTALREFBLOCKS);
	ino->c_dirty = 1;
	ip->i_size = 0;
#if DEBUG >= 3
 dprintf(3, "f_trunc() returning\n");
#endif
}

/* Bmap() defines the structure of file system storage by returning the
 * physical block number on a device given the inode and the logical
 * block number in a file.
 *
 * The block is zeroed if created.
 */
blkno_t bmap(inoptr ip, blkno_t bn, uchar rdflg)
{
	dev_t dev;
	register uint i, j, sh;
	register blkno_t *bp, nb;

#if DEBUG >= 3
 dprintf(3, "bmap(%u, %u, %u) starting\n", ip - i_tab, bn, (unsigned int)rdflg);
/* dprintf(2, "xx %u %u\n", i_tab[38].c_node.i_addr[3], udata.u_page); */
#endif

	if (isdevice(ip))	/* block devices */
 {
#if DEBUG >= 3
 dprintf(3, "bmap() returning %u, direct\n", bn);
#endif
		return bn;	/* map directly */
 }
	dev = ip->c_dev;
	/* blocks 0..DIRECTBLOCKS-1 are direct blocks */
	if (bn < DIRECTBLOCKS) {
		bp = &ip->c_node.i_addr[bn];
		if (0 == (nb = *bp)) {
			/* block not allocated yet */
			if (rdflg || (nb = blk_alloc(dev, 1)) == 0) /* write back later */
				goto Err;
			*bp = nb;
			ip->c_dirty = 1;
		}
		goto Ok;
	}
	/* addresses DIRECTBLOCKS and DIRECTBLOCKS+1 have single and double
	 * indirect blocks.
	 * The first step is to determine how many levels of indirection.
	 */
	bn -= DIRECTBLOCKS;
	sh = 0;
	j = INDIRECTBLOCKS+DINDIRECTBLOCKS;
	if (bn >= BUFSIZE/sizeof(blkno_t)) {	/* double indirect */
		sh = 8; 			/* shift by 8 */
		bn -= BUFSIZE/sizeof(blkno_t);
		j -= INDIRECTBLOCKS;
	}
	/* fetch the address from the inode.
	 * Create the first indirect block if needed.
	 */
	bp = &ip->c_node.i_addr[TOTALREFBLOCKS - j];
	if ((nb = *bp) == 0) {
		if (rdflg || 0 == (nb = blk_alloc(dev, 2))) /* write back now */
			goto Err;
		*bp = nb;
		ip->c_dirty = 1;
	}
	/* fetch through the indirect blocks */
	while (j <= INDIRECTBLOCKS+DINDIRECTBLOCKS) {
		if ((bp = (blkno_t *)bread(dev, nb, 0)) == NULL)
 {
Err:
#if DEBUG >= 3
 dprintf(3, "bmap() returning NULLBLK, error %u\n", udata.u_error);
#endif
			return NULLBLK;
 }
		i = (bn >> sh) & (BUFSIZE/sizeof(blkno_t)-1);
		if ((nb = bp[i]) != 0) {
			if (brelse((bufptr)bp) < 0)
				goto Err;
		} else {
			if (rdflg || 0 == (nb = blk_alloc(dev,
#if 1
					(j == INDIRECTBLOCKS+DINDIRECTBLOCKS) ?
					1 : 2 /* if indirect, write back now */
#else
					1 /* write back later */
#endif
					))) {
				brelse((bufptr)bp);
				goto Err;
			}
			bp[i] = nb;
			if (bawrite((bufptr)bp) < 0)
				goto Err;
		}
		sh -= 8;
		++j;
	}
Ok:
#if DEBUG >= 3
 dprintf(3, "bmap() returning %u, success\n", nb);
#endif
	return nb;
}

/* Validblk() panics if the given block number is not a valid data block
 * for the given device.
 */
void validblk(dev_t devno, blkno_t num)
{
	register fsptr dev = findfs(devno);

	if (dev == NULL)
		panic("validblk: dev %p not mounted",dev);
	if (num < dev->s_isize + dev->s_reserv || num >= dev->s_fsize)
		panic("validblk: invalid block %u", num);
}

/* Getinode() returns the inode pointer associated with a user's
 * file descriptor, checking for valid data structures
 */
inoptr getinode(uchar uindex)
{
	register inoptr inoindex;
	uchar oftindex = udata.u_files[uindex];

	if (uindex >= UFTSIZE || freefileentry(oftindex)) {
		udata.u_error = EBADF;
		return (NULL);
	}
	if (oftindex >= OFTSIZE)
		panic(gtinobadoft, oftindex, " for", uindex);
	if ((inoindex = of_tab[oftindex].o_inode) < i_tab ||
	    inoindex >= i_tab + ITABSIZE)
		panic(gtinobadoft, oftindex, ", inode", inoindex);
	magic(inoindex, "getinode");
	return inoindex;
}

/* Getperm() looks at the given inode and the effective user/group ids, and
 * returns the effective permissions in the low-order 3 bits.
 */
int getperm(inoptr ino)
{
	mode_t mode = ino->c_node.i_mode;

	if (super())
		mode |= S_ISDEV(mode) ? (mode >> 3) | (mode >> 6) : 07;
	else {
		if (ino->c_node.i_uid == udata.u_euid)	mode >>= 6; /* owner */
		else if (ino->c_node.i_gid == udata.u_egid) mode >>= 3; /* group */
	}
	return (mode & 07);
}

/* Setftim() sets the times of the given inode, according to the flags
 */
void setftim(inoptr ino, uchar flag)
{
	static time_t t;

	rdtime(&t);
	if (flag & A_TIME)	ino->c_node.i_atime = t;
	if (flag & M_TIME)	ino->c_node.i_mtime = t;
	if (flag & C_TIME)	ino->c_node.i_ctime = t;
	ino->c_dirty = 1;
}

/* Fmount() places the given device in the mount table with mount point ino
 */
int fmount(dev_t dev, inoptr ino, bool_t roflag)
{
	char *buf;
	register fsptr fp = fs_tab;

#if 0 /* NICK TEMPORARY! PLEASE CHANGE THIS BACK! */
	if (!validdev(dev)) {
#else
	if (!validdev(dev,NULL)) {
#endif
		udata.u_error = ENODEV;
		goto Err1;	/* ENODEV */
	}
	if (MAJOR(dev) >= BLK_DEVS)
		goto Err;
	/* Lookup free slot in mounted fs table */
	while (fp != fs_tab+NFS) {
		if (fp->s_dev == dev && fp->s_mounted != 0) {
			udata.u_error = EBUSY;
			goto Err1;
		}
		++fp;
	}
	while (fp != fs_tab) {
		--fp;
		if (fp->s_mounted == 0)
			goto Ok;
	}
	udata.u_error = ENOMEM;
	goto Err1;
	/* temporarly lock the slot */
Ok:	fp->s_mounted = ~SMOUNTED;
	fp->s_dev = dev;
	if (d_open(dev) != 0)
		panic("fmount: can't open fs on %p", dev);
#if 0 /* routine to search for the starting position within a FAT/FAT32 fs */
 {
 register int i;
 for (i = SUPERBLOCK; i < 0x400; ++i)
  {
  dprintf(2, "reading sector 0x%x\n", i);
  buf = bread(dev, i, 0);
  if (*(int *)buf == SMOUNTED)
   {
   dprintf(2, "formatted at sector 0x%x\n", i - SUPERBLOCK);
   panic("stop");
   }
  brelse((bufptr)buf);
  }
 }
#endif
	/* read in superblock */
	buf = (char *)bread(dev, SUPERBLOCK, 0);
	if (buf == NULL) goto Err1;
	bcopy(buf, fp, sizeof(filesys_t));
	/* fill-in in-core fields */
	fp->s_ronly = roflag;
	fp->s_dev = dev;
	fp->s_fmod = 0;
	if (brelse((bufptr)buf) < 0) goto Err1;
	/* See if there really is a filesystem on the device */
	if (fp->s_mounted != SMOUNTED || fp->s_isize >= fp->s_fsize) {
		fp->s_mounted = 0;	/* free fs table entry */
Err:		udata.u_error = ENOTBLK;
Err1:		return (-1);
	}
	if ((fp->s_mntpt = ino) != NULL)
		i_ref(ino);
/* fp->s_reserv = 2; */
	return 0;
}

/* magic() check inode against corruption
 */
void magic(inoptr ino, char *who) /* Nick added who */
{
	if (ino->c_magic != CMAGIC)
		panic("%s: corrupted inode %p", who, ino); /* Nick added who */
}

/* i_sync() syncing all inodes
 */
void i_sync(void)
{
	register inoptr ino = i_tab;

	/* Write out modified inodes */
	++dirty_mask;
	while (ino != i_tab + ITABSIZE) {
		if (ino->c_magic == CMAGIC &&
		    ino->c_dirty != 0 && 
		    ino->c_refs > 0)
			wr_inode(ino);
		++ino;
	}
#if 1 /* Nick */
	--dirty_mask;
#endif
	bufsync();
}

/* fs_sync() syncing all superblocks
 */
void fs_sync(void)
{
	register fsptr fp = fs_tab;
	register bufptr buf;

	/* Write out modified super blocks
	 * old: This fills the rest of the super block with zeros
	 * new: This preserves the remaining bytes of the superblock
	 */
	while (fp != fs_tab+NFS) {
		if (fp->s_mounted && fp->s_fmod) {
#if 1 /* Nick free bitmap */
			buf = (bufptr)bread(fp->s_dev, SUPERBLOCK, 0);
#else
			buf = (bufptr)bread(fp->s_dev, SUPERBLOCK, 2);
#endif
			if (buf != NULL) {
				fp->s_fmod = 0;
				bcopy(fp, buf, sizeof(*fp));
				if (bfree(buf, 1) < 0)
					fp->s_fmod = 1;
			}
		}
		++fp;
	}
	bufsync();
}

