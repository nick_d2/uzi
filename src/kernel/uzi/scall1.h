/* scall1.h for UZI180 by Nick (we are converting everything to ANSI C) */

#ifndef __SCALL1_H
#define __SCALL1_H

int sys_NONE(void);
int sys_sync(void);
int sys_utime(void);
int sys_close(void);
void truncateto0(inoptr ino);
int sys_open(void);
int sys_link(void);
int sys_symlink(void);
int sys_unlink(void);
int sys_readwrite(void);
int sys_lseek(void);
int sys_chdir(void);
int sys_chroot(void);
int sys_mknod(void);
int sys_access(void);
int sys_chmod(void);
int sys_chown(void);
void stcpy(inoptr ino, struct stat *buf);
int sys_statfstat(void);
#if 1 /* Nick free bitmap */
int sys_falign(void);
#endif
int sys_dup(void);
int sys_dup2(void);
int sys_getfsys(void);
int sys_ioctl(void);
int sys_mountumount(void);
int sys_time(void);
void exit0(void);
inoptr n_creat(char *name, bool_t new, mode_t mode);
void readwritei(char write, inoptr ino);
inoptr rwsetup(uchar fd, void *base, uint cnt, uchar rdflag);
int pdat(struct s_pdata *ubuf);
#if 1 /* Nick, new getfsys() convention */
int getfsys(dev_t devno, info_t *buf);
#else
int getfsys(dev_t devno, void *buf);
#endif

#endif /* __SCALL1_H */

