/* main.c for uzi180 by Nick - initialises CMX and then starts UZI as a task */

/* #define POLLED */ /* causes console I/O to be output via abyte() */

#include "vendor.h" /* Nick, must come first */
#include <cxfuncs.h>
#include <cxextern.h>
#include <string.h>
#include <unix.h>
#include <config.h>
#include <extern.h>
#include "devtty.h"
#include "devno.h"
#include "touchscreen.h"
#include "testsystem.h"

void asci0_setup(void);
void asci1_setup(void);
void escc0_setup(void);
void escc1_setup(void);
void apibus_setup(void);
void apibus_priority(char command);
void timer0_setup(void);	/* initializes timer 3, for CMX tick */

extern char osBank;				/* defined in machasm */
/* extern int root_dev; /* should be dev_t */	/* defined in extern.h */

/* extern char silly; */ /* Nick, very silly !! */
extern char rom_serial_no[]; /* 5 digits (leading space padded) plus null */

#if 1 /* Nick UZIX compatible */
char UZIX[14]="UZI180"; /* Nick "UZIX"; should be 13 chars or fewer */
char HOST[14]="sn00000"; /* Nick kdata.k_host should be 13 chars or fewer */
#endif

byte uzi_slot;  /* UZI task global slot number */
extern void initsys(void); /* start of the UZI task's code */

extern char cmdblk[8]; /* for scsiop(), defined in devhd.c */
extern long hd_offset;
extern long hd_sector;
extern char *dptr;
extern int dlen;
extern char *cptr;
extern int busid;

void main(int argc, char **argv);
void timer0_handler(void);
#if 0 /* Nick, see kprintf.c */
void kprintf(char *fmt, ...);
#endif
void kputchar(char c);
void puts(char *s);
void _putc(int minor, char c);
void tty_inproc(int minor, char c); /* in devtty.c */
void tty_rx_handler(SDEV *device, char c);
#if 0
void my_tty_inproc5(SDEV *device, char c);
#endif
#if 0 /* Nick, see kprintf.c */
char *itob(int n, char *s, int base);
#endif
void bzero(char *ptr, int count);
void bcopy(char *src, char *dest, int count);
int int_min(int a, int b);
int int_max(int a, int b);
int scsiop(void);
void compactflash_rx_handler(SDEV *device, char c);
void hostname_setup(void);

#if 0
tcbpointer compactflash_tcbptr; /* must be initialised to NULL by runtime!! */
#else
char compactflash_enable; /* must be initialised to 0 by runtime!! */
#endif

#if 0
byte t0_cnt;      /* used by interrupt, for counter */
byte go_ahead;    /* used by interrupt, to determine when to send messages */

char int_mesg[] = {"interrupt mesg"};  /* message that interrupt will send */
#endif

#if 0
void initsysx(void)
	{
	while (1)
		{
		abyte ('#');
		}
	}
#endif

/* Admin. wrapper for sdevs */
wsdevsT wsdevs[PORTS];


void main(int argc, char **argv)
	{
#if 1
	int i;
#endif
	byte status;

	/* abyte('C'); */

#if 1
	amess("kernel: parameters:");
	if (argc < 2)
		{
		amess(" none");
		}
	else
		{
		for (i = 1; i < argc; i++)
			{
			abyte(' ');
			amess(argv[i]);
			}
		}
	acrlf();

	root_dev = -1;
	for (i = 1; i < argc; i++)
		{
		if (strlen(argv[i]) == 8 &&
				strncmp(argv[i], "root=hd", 7) == 0 &&
				(status = argv[i][7]) >= '0' && status <= '9')
			{
			root_dev = status - '0';
			}
		}
/* abyte('a'); */
#endif

	K_OS_Init();  /* initialize ram and things */
/* abyte('b'); */
	K_Init_Sdevs(); /* set up serial port buffers */
/* abyte('c'); */
	/* abyte('D'); */

	asci0_setup();
/* abyte('d'); */
	asci1_setup();
/* abyte('e'); */
	escc0_setup();
/* abyte('f'); */
	escc1_setup();
/* abyte('g'); */
	apibus_setup();
/* abyte('h'); */
	/* abyte('E'); */

	/* initialise compactflash card (and synchronise WPO chip tx count) */
	K_Put_Str(10, "\x1bI", 2);
/* abyte('i'); */

#if 1
	K_Put_Str(1, "\r\nWelcome to Hytech CMX version 1.0\r\n", 37);
/* abyte('j'); */
	/* abyte('F'); */
#endif

	K_Task_Create(2, &uzi_slot, initsys, 128); /* create UZI task */
	K_Task_Start(uzi_slot); /* trigger UZI task */

	timer0_setup();	/* initialize and start timer, to produce CMX TICK */
	/* abyte('0'); */

#if 0
	K_Put_Str(0, "\r\nHello from Hytech CMX, serial port 0\r\n", 40);
/*	K_Put_Str(1, "\r\nHello from Hytech CMX, serial port 1\r\n", 40); */
	K_Put_Str(2, "\r\nHello from Hytech CMX, serial port 2\r\n", 40);
	K_Put_Str(3, "\r\nHello from Hytech CMX, serial port 3\r\n", 40);
	K_Put_Str(5, "\x1bKHello from Hytech CMX, apibus port 5\r\n", 40);
	K_Put_Str(6, "\x1bKHello from Hytech CMX, apibus port 6\r\n", 40);
#endif

#if 0
	K_Put_Str(1, "\r\nPress any key to start the scheduler... ", 42);

	while (!K_Get_Char(1, &status))
	   /* abyte('.') */; /* can't use CMX task blocking functions just yet */
 /* abyte('>'); */

	K_Put_Char(1, &status);

	K_Put_Str(1, "\r\nStarting the RTOS scheduler\r\n", 31);
#endif

#if 1
	hostname_setup();

	K_OS_Disable_Interrupts();

	/* redirect incoming characters on SERIAL 0 to the uzi tty handler */
	sdevs[0].rx_vector = tty_rx_handler;
	sdevs[1].rx_vector = tty_rx_handler;
	sdevs[2].rx_vector = tty_rx_handler;
	sdevs[3].rx_vector = tty_rx_handler;
	sdevs[4].rx_vector = tty_rx_handler;
	sdevs[5].rx_vector = tty_rx_handler;
	sdevs[6].rx_vector = tty_rx_handler;
	sdevs[7].rx_vector = tty_rx_handler;
	sdevs[8].rx_vector = tty_rx_handler;

	/* redirect incoming data from CompactFlash to the uzi scsi handler */
	sdevs[10].rx_vector = compactflash_rx_handler;

	K_OS_Enable_Interrupts();

	/* initialise device extra administration */
	memset(&wsdevs, 0, sizeof(wsdevsT));

	/* initialise test system */
	testsystem_init();

	/* initialise touchscreen hooks */
	touchscreen_init();

#endif

	K_OS_Start();         /* enter CMX RTOS */
	}

void timer0_handler(void)
	{
#if 0 /* Nick, very temporary */
	K_OS_Tick_Update();
#endif
#if 0
	if (go_ahead)	/* see if task has enable message sending */
		{
		if (++t0_cnt == 2)	/* see if count = 2. */
			{
			t0_cnt = 0;			/* reset count */

			/* The following allows an interrupt to use a CMX function, but
				it must use it indirectly. Thus the interrupt must call
				the respective CMX function with a leading 'int_' to the
				function. */
			K_Intrp_Mesg_Send(0,int_mesg);
			}
		}
#endif
	}

#if 0 /* Nick, see kprintf.c */
void kprintf (char *fmt, ...)
{
    register char *arg;
    register char c;
    int  base;
    char s[7], *itob();

/* abyte('{'); */
    arg = (char *)(&fmt+1);        /* NOTE: Assumes Pointer to 2-byte Word */
#if 0
 while (c = *fmt++)
  {
  kputchar(c);
  }
 ahexw(*(int *)arg);
 abyte('}');
 return;
#endif
    while (c = *fmt++) {
        if (c != '%') {
            kputchar (c);
            continue;
        }
        switch (c = *fmt++) {
            case 'c': kputchar (*arg++); arg++; /* for IAR compiler */
                      continue;
            case 'd': base = -10;
                      goto prt;
            case 'o': base = 8;
                      goto prt;
            case 'u': base = 10;
                      goto prt;
            case 'p': /* Nick */
            case 'x': base = 16;
   prt:
                      puts (itob (*(int *)arg, s, base));
                      arg++;  arg++;	/* NOTE: for Size of Word Ptr */
                      continue;
            case 's': puts (*(char **)arg);
                      arg++;  arg++;    /* NOTE: for Size of Word Ptr */
                     continue;
            default: kputchar (c);
                     continue;
        }
    }
/* abyte('}'); */
}
#endif

void kputchar(char c)
	{
	if (c == '\n')
#if 0
		{
		abyte('\r');
		}
	abyte(c);
#else
		{
		_putc(0, '\r');	/* Use default TTY, minor=0 */
		}
	_putc(0, c);
#endif
	}

void puts(char *s)
	{
	while (*s)
		{
		kputchar(*s++);
		}
	}

void _putc(int minor, char c)
	{
#ifdef POLLED
	if (minor == 0 || minor == 2)
		{
		abyte(c);
#else
	if (minor == 0)
		{
		K_Put_Char_Wait(1, &c, 0x1000);
#endif
		}
	else
		{
		K_Put_Char_Wait(minor - 1, &c, 0x1000);
		}
	}

void tty_rx_handler(SDEV *device, char c)
	{
#if 1
	register int minor;

	minor = device->port + 1;
	(*tty_vector[minor])(minor, c);
#else
	tty_inproc(1, c);
#endif
	}

#if 0
void my_tty_inproc5(SDEV *device, char c)
	{
	tty_inproc(2, c);
	}
#endif

#if 0 /* Nick, see kprintf.c */
#define TRUE 1
#define FALSE 0

/* convert an integer to a string in any base (2-36) */
char *itob (int n, char *s, int base)
    {
    register unsigned int u;
    register char *p, *q;
    register negative, c;
    if ((n < 0) && (base == -10)) {
        negative = TRUE;
        u = -n;
        }
    else {
        negative = FALSE;
        u = n;
        }
    if (base == -10)            /* Signals signed conversion */
         base = 10;
    p = q = s;
    do {                        /* Generate digits in reverse order */
        if ((*p = u % base + '0') > '9')
            *p += ('A' - ('9' + 1));
        ++p;
        u = u / base;
        } while (u > 0);
    if (negative)
        *p++ = '-';
    *p = '\0';                  /* Terminate the string */
    while (q < --p) {           /* Reverse the digits */
        c = *q;
        *q++ = *p;
        *p = c;
        }
    return s;
    }
#endif

void bzero(ptr, count)
char *ptr;
int count;
	{
	while (count--)
		{
		*ptr++ = 0;
		}
	}

void bcopy(src, dest, count)
char *src, *dest;
int count;
	{
	while (count--)
		{
		*dest++ = *src++;
		}
	}
int int_min(int a, int b)
{
	return (b < a ? b : a);
}

int int_max(int a, int b)
{
	return (b > a ? b : a);
}

int scsiop(void)
	{
	register char i, k;
	register unsigned int j; /* counts up to n*0x200 */
	char checksum; /* can't be register */
	int timecnt = 0x1000;

#if 0
 if (silly == 0)
  while (1)
   ;
#endif

/** abyte(':'); **/
#if 0 /* Nick very temporary */
 if (cmdblk[1] == 'W')
  return 0;
#endif

#if DEBUG >= 2
 dprintf(2, "scsiop: %c %u+%u,%u -> 0x%x\n", cmdblk[1],
					     (unsigned int)hd_sector,
					     (unsigned int)hd_offset,
					     (unsigned int)cmdblk[7],
					     (unsigned int)dptr);
#endif

	/* check for special hd_offset indicating the ramdrive is wanted */
	if (hd_offset == 0)
		{
		j = ((unsigned int)cmdblk[7]) << 9;
		hd_sector = 0x40000L + (hd_sector << 9);
		hd_offset = (((long)osBank) << 12) + (long)dptr;

		switch(cmdblk[1])
			{
		case 'R':
/* dprintf(2, "R %08lx -> %08lx, %04x\n", hd_sector, hd_offset, j); */
			copyr(hd_sector, hd_offset, j);
			break;
		case 'W':
/* dprintf(2, "W %08lx -> %08lx, %04x\n", hd_offset, hd_sector, j); */
			copyr(hd_offset, hd_sector, j);
			break;
			}

		return 0;
		}

	/* at this point partitioning doesn't matter, so clobber hd_sector */
	hd_sector += hd_offset;
/** abyte('a'); **/

	apibus_priority(0x17); /* set device 7 = priority 1 (maximum) */

	for (i = 0; i < cmdblk[7]; i++)
		{
/** abyte('b'); **/
		cmdblk[2] = hd_sector;
		cmdblk[3] = hd_sector >> 8;
		cmdblk[4] = hd_sector >> 16;
		cmdblk[5] = hd_sector >> 24;
/** abyte('c'); **/
		cmdblk[6] = 0xff - cmdblk[5] - cmdblk[4]
		                 - cmdblk[3] - cmdblk[2];
/** abyte('d'); **/

#if 0
		/* tell transmitter task waiting. */
		compactflash_tcbptr = activetcb;
#else
		compactflash_enable = 1; /* prepare to receive esc sequence */
#endif

/* abyte('&'); */
		switch(cmdblk[1])
			{

		case 'R':
/** abyte('e'); **/
			if (K_Put_Str_Wait(10, cmdblk, 7, timecnt) != K_OK)
				{
/** abyte('f'); **/
				/* set device 7 = priority 7 (minimum) */
				apibus_priority(0x77);

				return 1;
				}
/** abyte('g'); **/
			break;

		case 'W':
			if (K_Put_Str_Wait(10, cmdblk, 6, timecnt) != K_OK)
				{
				/* set device 7 = priority 7 (minimum) */
				apibus_priority(0x77);

				return 1;
				}

			checksum = cmdblk[6];
			for (j = 0; j < 0x200; j += 0x20)
				{
				if (K_Put_Str_Wait(10, dptr + j, 0x20, timecnt)
								       != K_OK)
					{
					/* set device 7 = priority 7 (min) */
					apibus_priority(0x77);

					return 1;
					}
				for (k = 0; k < 0x20; k++)
					{
					checksum -= dptr[j + k];
					}
/* abyte('.'); */
				}

			if (K_Put_Char_Wait(10, &checksum, timecnt) != K_OK)
				{
				/* set device 7 = priority 7 (minimum) */
				apibus_priority(0x77);

				return 1;
				}
			break;

			}

/* abyte('*'); */
#if 0
		K_I_Disable_Sched();	/* set task block. */

		/* see if still waiting for a response */
		K_OS_Disable_Interrupts();	/* DISABLE INTERRUPTS. */
		if (compactflash_tcbptr != NULL)
			{
			if (timecnt)	/* wait on time too? */
				{
				/* put task to sleep, indicating why */
				activetcb->tcbstate = WAIT | TIME;
				}
			else
				{
				/* put task to sleep, indicating why */
				activetcb->tcbstate = WAIT;
				}
			K_OS_Enable_Interrupts();	/* RE-ENABLE INTERRUPTS */

			K_I_Insert_Link(timecnt);	/* insert it into time link. */
			compactflash_tcbptr = NULL;	/* task will return to here. */

			K_I_Disable_Sched();	/* set task block. */
			if(K_I_Remove_Link())	/* remove from link. */
				{
				/* set device 7 = priority 7 (minimum) */
				apibus_priority(0x77);

				K_I_Func_Return();	/* time out occurred. */
				return(K_TIMEOUT);
				}
			}
		else
			{
			K_OS_Enable_Interrupts();	/* RE-ENABLE INTERRUPTS */
			}
		K_I_Func_Return();
#else
		while (compactflash_enable)
			; /* rather primitive for the moment */
#endif

		dptr += 0x200;
		hd_sector++;
		}
/** abyte(';'); **/

	apibus_priority(0x77); /* set device 7 = priority 7 (minimum) */

	return 0;
	}

void compactflash_rx_handler(SDEV *device, char c)
	{
	static char state = 0;
	static char *dest;
	static int count;

#if 0
	if (compactflash_tcbptr == NULL)
#else
	if (compactflash_enable == 0)
#endif
		{
#if 0
 if (c == '0' || c == '1')
  {
  abyte(c + 6);
  }
#endif
		return;
		}

	switch (state)
		{

	case 0:
		switch (c)
			{

		case '0':
		case '1':
#if 0
 abyte(c + 8);
#endif
			break;

		case 0x1b:
			state++;
			break;

			}
		break;

	case 1:
	case 2:
	case 3:
	case 4:
#if 0
 if (state == 1)
  {
  abyte(c);
  }
#endif
		if (c == cmdblk[state])
			{
			state++;
			break;
			}
		state = 0;
		break;

	case 5:
		if (c == cmdblk[5])
			{
			switch (cmdblk[1])
				{

			case 'R':
				state = 6; /* reading, prepare for data xfer */
				dest = dptr;
				count = 0x200;
				break;

			case 'W':
				state = 7; /* writing, skip data xfer step */
				break;

			default:
				state = 0; /* should never get here */
				break;

				}
			break;
			}
		state = 0;
		break;

	case 6:
		cmdblk[6] -= c;
		*dest++ = c;
#if 0
 if ((count & 0x1f) == 0)
  {
  abyte('.');
  }
#endif
		if (--count)
			{
			break;
			}
		state = 7;
		break;

	case 7:
		if (c == cmdblk[6])
			{
#if 0
 abyte('$');
#endif
#if 0
			/* see if task waiting. */
			if (compactflash_tcbptr->tcbstate & WAIT)
				{
				/* yes, wake task. */
				device->xmit.tcbptr->tcbstate = RESUME;
				if (compactflash_tcbptr->priority <
						active_priority)
					{
					PREEMPTED; /*cmx_flag1 |= preempted;*/	/* yes, set preempted K_I_Scheduler flag */
					}
				}

			compactflash_tcbptr = NULL;
#else
			compactflash_enable = 0; /* command done ok */
#endif
			}
#if 0
 else { abyte('%'); }
#endif
		/* we could also include retry stuff here */
		state = 0;
		break;

		}
	}

void hostname_setup(void)
	{
	register char i, j;

	for (i = 0; i < 5; i++)
		{
		if (rom_serial_no[i] != ' ')
			{
			break;
			}
		}

	j = 2;
	while (i < 5 && rom_serial_no[i])
		{
		HOST[j++] = rom_serial_no[i++];
		}

	HOST[j] = 0;
	}

