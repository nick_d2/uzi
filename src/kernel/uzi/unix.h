/****************************************************
UZI (Unix Z80 Implementation) Kernel:  unix.h
From UZI by Doug Braun and UZI280 by Stefan Nitschke.
*****************************************************/
/* History:
 *   21.12.97 - Removed leading ? from Per-Process equates.	HFB
 *   11.07.98 - Shortened Time Slices to 50 Ticks/Sec (20 mS).	HFB
 */

#ifndef __UNIX_H
#define __UNIX_H

#ifndef VAX /* Nick UZIX compatible */
#include <sys/types.h> /* for mode_t and others */
#include <fcntl.h> /* for O_RDONLY and others */
#include <signal.h> /* for signal_t and others */
#include <sys/ioctl.h> /* for info_t and others */
#include <sys/stat.h> /* for S_IFREG and others */
#include <sys/seek.h> /* for SEEK_END and others */
#include <errno.h> /* for EACCES and others */
#endif

#ifndef VAX
#define CPM
#endif

#define UFTSIZE 20 /*10*/       /* Number of user files */		/*280 = 22*/
#define OFTSIZE 30 /*15*/       /* Open file table size */		/*280 = 45*/
#define ITABSIZE 40 /*20*/      /* Inode table size */			/*280 = 45*/
#define PTABSIZE 5 /*6*/       /* Process table size in UZI180.  Size as:
                          *    1 MB RAM, 1st 32k Shadowed - 14
                          *    512k RAM, No Shadow ROM    -  7
                          *    512k RAM, 1st 32k Shadowed -  6
                          *    256k RAM, No Shadow ROM    -  3
                          */
#define NSIGS 16         /* Number of signals <= 16 */

#if 1 /* Nick UZIX compatible */
#define SUPERBLOCK	4 /*1*/	/* disk block of filesystem superblock */
#endif
#define ROOTINODE 1      /* Inode # of / for all mounted filesystems. */

#define TICKSPERSEC 50   /* Ticks per second */
#define MAXTICKS     4   /* Max ticks before swapping out (time slice)
                            default process time slice */
#define MAXBACK      3   /* Process time slice for tasks not connected
                            to the current tty */
#define MAXBACK2     2   /* Process time slice for background tasks */
#define MAXINTER     5   /* Process time slice for interactive tasks */

#define ARGBLK       0   /* Block number on SWAPDEV for arguments */
#define PROGBASE  ((char *)(0x8100))  /* also data base (Nick, prev 0x100) */
#define PROGTOP   ((char *)(0xfffe))  /* Top of program, base of U_DATA */
#define SYSCADDR  0x30   /* System call address, by Nick, UZIX compatible */

#define EMAGIC    0xc3   /* Header of executable */
#define CMAGIC   24721   /* Random number for cinode c_magic */
#define SMOUNTED 12742   /* Magic number to specify mounted filesystem */
#define NULL         0

#define Hi_TECH_C

#ifndef Hi_TECH_C
/* Speed and code length optimized for Q/C Compiler */
#define ifnull(e) if(e){}else
#define ifnot(e) if(e){}else
#define ifzero(e) if(e){}else
#else           /* Define for Hi-Tech Compiler */
#define FALSE 0
#define ifnull(e) if ((e)==0)
#define ifnot(e)  if ((e)==FALSE)
#define ifzero(e) if ((e)==NULL)
#endif


#ifdef CPM
    typedef unsigned uint16;
    typedef int int16;
#else
    typedef unsigned short uint16;
    typedef short int16;
#endif

/* USER! basic data types added by Nick, see ../include/types.h */
/* ! uchar & uint is counterparts and must be declared simultaneously */
#ifndef uchar_is_defined
#define uchar_is_defined
typedef unsigned char uchar;
typedef unsigned int uint;
#endif


typedef struct s_queue {
    char *q_base;    /* Pointer to data */
    char *q_head;    /* Pointer to addr of next char to read. */
    char *q_tail;    /* Pointer to where next char to insert goes. */
    int   q_size;    /* Max size of queue */
    int   q_count;   /* How many characters presently in queue */
    int   q_wakeup;  /* Threshold for waking up processes waiting on queue */
} queue_t;


#ifdef __TYPES_H /* Nick */
typedef time_t uzitime_t;
#else
typedef struct uzitime_s {
    uint16 t_time;
    uint16 t_date;
} uzitime_t;

#ifndef SKIP_TIME_T /* for utils target, skip when compiling ucp.c, ucpsub.c */
typedef uzitime_t time_t;
#endif

/* User's structure for times() system call */

struct tms {
	uzitime_t  tms_utime;
	uzitime_t  tms_stime;
	uzitime_t  tms_cutime;
	uzitime_t  tms_cstime;
	uzitime_t  tms_etime;      /* Elapsed real time */
};

#ifndef utimbuf_is_defined
#define utimbuf_is_defined
/* User's structure for utime() system call */
struct utimbuf {
	uzitime_t	actime;
	uzitime_t	modtime;
};
#endif

#endif /* Nick */

/* Flags for setftime() */
#define A_TIME 1
#define M_TIME 2
#define C_TIME 4

#ifndef __TYPES_H /* Nick */
/* extra typedefs added from types.h, needed for UTIL target */
typedef long off_t;
typedef uchar bool_t;	/* boolean value */
typedef uint count_t;	/* counter for anything */
typedef unsigned int mode_t;
#endif /* HACK! */
#ifndef BUFSIZE /* HACK! */
#define BUFSIZE 	512	/* uzix buffer/block size */
#define BUFSIZELOG	9	/* uzix buffer/block size log2 */
#endif /* __TYPES_H Nick */

#ifndef __SIGNAL_H /* Nick */
/* extra typedefs added from signal.h, needed for UTIL target */
typedef int signal_t;
#endif

#if 1 /* file-system related data types added by Nick from uzix1.0 */
typedef uint16 ino_t;	/* Can have 65536 inodes in fs */
typedef uint16 blkno_t;	/* Can have 65536 BUFSIZE-byte blocks in fs */
typedef uint16 dev_t;	/* Device number */

#define MINOR(dev)		((uchar)(dev))
#define MAJOR(dev)		((uchar)((dev) >> 8))
#define MKDEV(major, minor)	(((uint)(uchar)(major) << 8) | (uchar)(minor))

#define NULLINO ((ino_t)0)
#define NULLBLK ((blkno_t)-1)
#define NULLDEV ((dev_t)-1)
#else
typedef uint16 blkno_t;    /* Can have 65536 512-byte blocks in filesystem */
#define NULLBLK ((blkno_t)-1)
#endif

#if 1 /* Nick UZIX compatible */
typedef struct s_blkbuf {
	uchar	bf_data[BUFSIZE]; /* This MUST BE first ! */
	dev_t	bf_dev; 	/* device of this block */
	blkno_t bf_blk; 	/* and block number on device */
	uchar	bf_dirty;	/* buffer changed flag */
	uchar	bf_busy;	/* buffer processing in progress */
	uchar	bf_prio;	/* buffer must be in memory (for wargs) */
	uint	bf_time;	/* LRU time stamp */
/*	struct s_blkbuf *bf_next;  /* LRU free list pointer */
} blkbuf_t, *bufptr;
#else
typedef struct s_blkbuf {
    char        bf_data[512];    /* This MUST be first ! */
    char        bf_dev;
    blkno_t     bf_blk;
    char        bf_dirty;
    char        bf_busy;
    uint16      bf_time;         /* LRU time stamp */
} blkbuf_t, *bufptr;
#endif

#if 1 /* Nick UZIX compatible */
#define DIRECTBLOCKS	18
#define INDIRECTBLOCKS	1	/* MUST BE 1! */
#define DINDIRECTBLOCKS 1	/* MUST BE 1! */
#define TOTALREFBLOCKS	(DIRECTBLOCKS+2)
#endif

#if 1 /* Nick UZIX compatible */
typedef struct s_dinode {
#else
typedef struct dinode {
#endif
    uint16   i_mode;
    uint16   i_nlink;
    uint16   i_uid;
    uint16   i_gid;
    off_t    i_size; /* Nick blkoff_t i_size; */
    uzitime_t   i_atime;
    uzitime_t   i_mtime;
    uzitime_t   i_ctime;
    blkno_t  i_addr[20];
#if 1 /* Nick UZIX compatible */
} dinode_t;             /* Exactly 64 bytes long! */
#else
} dinode;               /* Exactly 64 bytes long! */
#endif

#if 1 /* Nick UZIX compatible */
#define DINODESPERBLOCK 	8	/* # of dinode_t per logical block */
#define DINODESPERBLOCKLOG	3	/* log2(DINODESPERBLOCK) */
#define DINODESPERBLOCKMASK	((1<<DINODESPERBLOCKLOG)-1)

#define DEVNUM(ino)	((dev_t)((ino)->c_node.i_addr[0]))

/* Getmode() returns the inode kind */
#define _getmode(mode)	((mode) & S_IFMT)
#define getmode(ino)	(_getmode((ino)->c_node.i_mode))
/* Super() returns true if we are the superuser */
#define super() 	(udata.u_euid == 0)
#endif

#ifndef __TYPES_H /* Nick */

struct stat	/* Really only used by users */
{
	int16   st_dev;
	uint16  st_ino;
	uint16  st_mode;
	uint16  st_nlink;
	uint16  st_uid;
	uint16  st_gid;
	uint16  st_rdev;
	off_t   st_size; /* blkoff_t st_size; */
	uzitime_t  st_atime;
	uzitime_t  st_mtime;
	uzitime_t  st_ctime;
};

#endif

#if 1 /* Nick UZIX compatible */
#ifndef _SYS_STAT_H

#define S_IFMT		0170000	/* file type mask */
#if 1 /* Nick */
#define S_IFALIGN	0120000 /* regular file, XIP aligned */
#endif
#define S_IFLNK		0110000	/* symbolic link */
#define S_IFREG		0100000	/* or just 000000, regular */
#define S_IFBLK 	0060000	/* block special */
#define S_IFDIR 	0040000	/* directory */
#define S_IFCHR 	0020000	/* character special */
#define S_IFPIPE 	0010000	/* pipe */

#define S_UMASK 	07777	/* bits modifiable by chmod */

#define S_ISUID 	04000	/* set euid to file uid */
#define S_ISGID 	02000 	/* set egid to file gid */
#define S_ISVTX 	01000	/* */

#define S_IREAD 	0400	/* owner may read */
#define S_IWRITE 	0200 	/* owner may write */
#define S_IEXEC 	0100	/* owner may execute <directory search> */

#define S_IGREAD 	0040	/* group may read */
#define S_IGWRITE 	0020 	/* group may write */
#define S_IGEXEC 	0010	/* group may execute <directory search> */

#define S_IOREAD 	0004	/* other may read */
#define S_IOWRITE  	0002	/* other may write */
#define S_IOEXEC 	0001	/* other may execute <directory search> */

#define S_IRWXU 	00700
#define S_IRUSR 	00400
#define S_IWUSR 	00200
#define S_IXUSR 	00100

#define S_IRWXG 	00070
#define S_IRGRP 	00040
#define S_IWGRP 	00020
#define S_IXGRP 	00010

#define S_IRWXO 	00007
#define S_IROTH 	00004
#define S_IWOTH 	00002
#define S_IXOTH 	00001

#ifdef __KERNEL__
#define S_IRWXUGO	(S_IRWXU|S_IRWXG|S_IRWXO)
#define S_IALLUGO	(S_ISUID|S_ISGID|S_ISVTX|S_IRWXUGO)
#define S_IRUGO 	(S_IRUSR|S_IRGRP|S_IROTH)
#define S_IWUGO 	(S_IWUSR|S_IWGRP|S_IWOTH)
#define S_IXUGO 	(S_IXUSR|S_IXGRP|S_IXOTH)
#endif

#define S_ISLNK(m)	(((m) & S_IFMT) == S_IFLNK)
#define S_ISREG(m)	(((m) & S_IFMT) == S_IFREG)
#define S_ISDIR(m)	(((m) & S_IFMT) == S_IFDIR)
#define S_ISCHR(m)	(((m) & S_IFMT) == S_IFCHR)
#define S_ISBLK(m)	(((m) & S_IFMT) == S_IFBLK)
#define S_ISPIPE(m)	(((m) & S_IFMT) == S_IFPIPE)

#if 1 /* Nick */
#define S_ISDEV(m)	(S_ISCHR(m) || S_ISBLK(m))
#else
#define S_ISDEV(m)	(((m) & S_IFMT) & S_IFCHR)
#endif

#if 1 /* Nick */
#define S_ISALIGN(m)	(((m) & S_IFMT) == S_IFALIGN)
#define S_ISREGALIGN(m)	(S_ISREG(m) || S_ISALIGN(m))
#endif

#if 1 /* Nick free bitmap */
#define XIP_ALIGN	0
#define XIP_UALIGN	1
#endif

#endif
#else
/* Bit masks for i_mode and st_mode */

#define OTH_EX  0001
#define OTH_WR  0002
#define OTH_RD  0004
#define GRP_EX  0010
#define GRP_WR  0020
#define GRP_RD  0040
#define OWN_EX  0100
#define OWN_WR  0200
#define OWN_RD  0400

#define SAV_TXT 01000
#define SET_GID 02000
#define SET_UID 04000

#define MODE_MASK 07777

#define F_REG   0100000
#define F_DIR   040000
#define F_PIPE  010000
#define F_BDEV  060000
#define F_CDEV  020000

#define F_MASK  0170000
#endif

#if 1 /* Nick UZIX compatible */
/* Getmode() returns the inode kind */
#define _getmode(mode)	((mode) & S_IFMT)
/* temporary #define getmode(ino)	(_getmode((ino)->c_node.i_mode)) */
/* Super() returns true if we are the superuser */
/* temporary #define super() 	(udata.u_euid == 0) */
#endif


#if 1 /* Nick UZIX compatible */
/* in-core inode structure */
typedef struct s_cinode {
	uint16	c_magic;	/* Used to check for corruption */
	uchar	c_dirty;	/* Modified flag */
	dev_t	c_dev;		/* Inode's device */
	ino_t	c_num;		/* Inode's number */
	uint16	c_refs; 	/* In-core reference count */
	bool_t	c_ro;		/* Read-only filesystem flag */
	dinode_t c_node;	/* disk inode copy */
} cinode_t, *inoptr;
#else
typedef struct cinode {
    int16      c_magic;           /* Used to check for corruption. */
    int16      c_dev;             /* Inode's device */
    uint16     c_num;             /* Inode # */
    dinode     c_node;
    char       c_refs;            /* In-core reference count */
    char       c_dirty;           /* Modified flag. */
} cinode, *inoptr;
#endif

#define NULLINODE ((inoptr)NULL)
#define NULLINOPTR ((inoptr*)NULL)


#ifndef __TYPES_H /* Nick */
#define DIRNAMELEN	14

/* device directory entry */
typedef struct s_direct {
	uint16	d_ino;			/* file's inode */
	uchar	d_name[DIRNAMELEN];	/* file name */
} direct_t;

#define BUFSIZE 	512	/* uzix buffer/block size */
#define BUFSIZELOG	9	/* uzix buffer/block size log2 */

#if 1 /* Nick free bitmap */
#define REGION_LOG 14
#define REGION_BYTES (1<<REGION_LOG)
#define REGION_BLOCKS (REGION_BYTES/BUFSIZE)

#define PAGE_LOG 12
#define PAGE_BYTES (1<<PAGE_LOG)
#define PAGE_BLOCKS (PAGE_BYTES/BUFSIZE)
#endif
#endif /* Nick */


#if 1 /* Nick UZIX compatible */
#define DIRECTPERBLOCK	(BUFSIZE/sizeof(direct_t))

#if 0 /* remove support for the original UZI filesystem */
#define FSFREEBLOCKS	50
#define FSFREEINODES	50
#endif

/* device-resident super-block */
typedef struct s_filesys {
	uint16	s_mounted;	/* signature */
	uint16	s_reserv;	/* # of first block of inodes */
	uint16	s_isize;	/* # of inode's blocks */
	uint16	s_fsize;	/* # of data's blocks */

	blkno_t s_tfree;	/* total free blocks */
#if 0 /* remove support for the original UZI filesystem */
	uint16	s_nfree;	/* # of free blocks in s_free */
	blkno_t s_free[FSFREEBLOCKS];	/* #s of free block's */
#endif

	ino_t	s_tinode;	/* total free inodes */
#if 0 /* remove support for the original UZI filesystem */
	uint16	s_ninode;	/* # of free inodes in s_inode */
	ino_t	s_inode[FSFREEINODES];	/* #s of free inodes */
#endif

	uzitime_t	s_time; 	/* last modification timestamp */

#if 1 /* Nick free bitmap */
	blkno_t	s_bitmap_inode;	/* disk byte position of free inode bitmap */
	blkno_t	s_bitmap_block;	/* disk byte position of free block bitmap */
	blkno_t s_bitmap_immov;	/* disk byte position of immoveable bitmap */
	blkno_t	s_bitmap_final;	/* byte position just after end of bitmaps */
#endif

/* Core-resident part */
	bool_t	s_fmod; 	/* filesystem modified */
	bool_t	s_ronly;	/* readonly filesystem */
	inoptr	s_mntpt;	/* Mount point inode */
	dev_t	s_dev;		/* Fs device */
} filesys_t, *fsptr;
#else
typedef struct filesys {
    int16       s_mounted;
    uint16      s_isize;
    uint16      s_fsize;
    int16       s_nfree;
    blkno_t     s_free[50];
    int16       s_ninode;
    uint16      s_inode[50];
    int16       s_fmod;
    uzitime_t      s_time;
    blkno_t     s_tfree;
    uint16      s_tinode;
    inoptr      s_mntpt;     /* Mount point */
} filesys, *fsptr;
#endif

#if 1 /* Nick UZIX compatible */
/* open file descriptor */
typedef struct oft {
	off_t   o_ptr; /* Nick blkoff_t o_ptr;		/* File position pointer */
	inoptr	o_inode;	/* Pointer into in-core inode table */
	uchar	o_access;	/* O_RDONLY, O_WRONLY, or O_RDWR */
	uchar	o_refs; 	/* Ref count: depends on # of active children */
} oft_t, *ofptr;
#else
typedef struct oft {
    blkoff_t  o_ptr;      /* File position point16er */
    inoptr    o_inode;    /* Pointer into in-core inode table */
    char      o_access;   /* O_RDONLY, O_WRONLY, or O_RDWR */
    char      o_refs;     /* Reference count: depends on # of active children */
} oft;
#endif


/* Process table p_status values */

#if 1 /* Nick UZIX compatible */
#ifndef _PSTATE_T
#define _PSTATE_T
/* Process table p_status values */
typedef enum {
	P_EMPTY = 0,	/* Unused slot */
	P_ZOMBIE,	/* 1. Exited. */
	P_FORKING,	/* 2. In process of forking; do not mess with */
	P_RUNNING,	/* 3. Currently running process */
	P_READY,	/* 4. Runnable process */
	P_SLEEP,	/* 5. Sleeping; can be awakened by signal */
	P_XSLEEP,	/* 6. Sleeping, don't wake up for signal */
	P_PAUSE,	/* 7. Sleeping for pause(); can wakeup for signal */
	P_WAIT		/* 8. Executed a wait() */
/* Nick	P_STOPED	/* 9. Process is stoped */
} pstate_t; /* pstate_t is needed for c:\uzi\simple\ps.c */
#endif
#else
#define P_EMPTY         0    /* Unused slot */
#define P_RUNNING       1    /* Currently running process */
#define P_READY         2    /* Runnable   */
#define P_SLEEP         3    /* Sleeping; can be awakened by signal */
#define P_XSLEEP        4    /* Sleeping, don't wake up for signal */
#define P_PAUSE         5    /* Sleeping for pause(); can wakeup for signal */
#define P_FORKING       6    /* In process of forking; do not mess with */
#define P_WAIT          7    /* Executed a wait() */
#define P_ZOMBIE2       8    /* Exited but code pages still valid. */
#define P_ZOMBIE        9    /* Exited. */
#endif


#if 1 /* Nick, see ../include/signal.h */
#ifndef __SIGNAL_H
#define __NOTASIGNAL 0
#define SIGHUP 1 		/* Hangup detected on controlling terminal
				   or death of a controlling process */
#define SIGINT 2 		/* Interrupt from keyboard */
#define SIGQUIT 3		/* Quit from keyboard */
#define SIGILL 4 		/* Illegal instruction */
#define SIGTRAP 5		/* Trace/breakpoint trap */
#define SIGIOT 6 		/* IOT trap. A synonym for SIGABRT */
#define SIGABRT SIGIOT		/* Abort signal from abort */
#define SIGUSR1 7		/* User's signal 1 */
#define SIGUSR2 8		/* User's signal 2 */
#define SIGKILL 9		/* Kill signal */
#define SIGPIPE 10		/* Broken pipe: write to pipe with no readers */
#define SIGALRM 11		/* Timer signal from alarm */
#define SIGTERM 12		/* Termination signal */
#define SIGURG 13			/* Urgent signal */
#define SIGCONT 14		/* Continue process */
#define SIGSTOP 15		/* Stop process */
#define __NUMOFSIGNALS SIGSTOP
/* this signals defined only for compatibility */
#define SIGBUS 16 		/* Bus error */
#define SIGFPE 17 		/* Floating point exception */
#define SIGSEGV 18		/* Invalid memory reference */
#define SIGSYS 19 		/* Bad argument to routine */
#define SIGTTIN 20
#define SIGTOUT 21
/* typedef int signal_t; this should be re-added later and used throughout */

typedef long sig_t;

#define SIG_DFL ((sig_t)0)	/* default signal handling */
#define SIG_IGN ((sig_t)1)	/* ignore signal */
#define SIG_ERR ((sig_t)-1)	/* error return from signal */
#endif
#else
#define SIGHUP  1       /* hangup */
#define SIGINT  2       /* interrupt */
#define SIGQUIT 3       /* quit */
#define SIGILL  4       /* illegal instruction (not reset when caught */
#define SIGTRAP 5       /* trace trap (not reset when caught) */
#define SIGIOT  6       /* IOT instruction */
#define SIGEMT  7       /* EMT instruction */
#define SIGFPE  8       /* floating point exception */
#define SIGKILL 9       /* kill */
#define SIGBUS  10      /* bus error */
#define SIGSEGV 11      /* segmentation violation */
#define SIGSYS  12      /* bad argument to system call */
#define SIGPIPE 13      /* write on a pipe with no one to read it */
#define SIGALRM 14      /* alarm clock */
#define SIGTERM 15      /* software termination signal from kill */

#define  SIG_DFL   (int (*)())0
#define  SIG_IGN   (int (*)())1
#endif

#define sigmask(sig)    (1<<(sig))

/* Process table entry */

#if 1 /* Nick UZIX compatible */
typedef struct s_ptab {
#else
typedef struct p_tab {
#endif
    char        p_status;       /* Process status */
    char        p_tty;          /* Process' controlling tty minor # */
    int         p_pid;          /* Process ID */
    int         p_uid;
#if 1 /* Nick UZIX compatible */
    struct s_ptab *p_pptr;      /* Process parent's table entry */
#else
    struct  p_tab *p_pptr;      /* Process parent's table entry */
#endif
    unsigned    p_alarm;        /* Seconds until alarm goes off */
    unsigned    p_exitval;      /* Exit value */
    char       *p_wait;         /* Address of thing waited for */
    /* Everything below here is overlaid by time info at exit */
    int         p_page;         /* Process BBR base value in UZI180 */
#if 1 /* Nick UZIX compatible */
    uchar	p_cprio;	/* Process current priority */
    signed char p_nice;		/* Process nice value (-20 to 19) */
#else
    int         p_priority;     /* Process priority */
#endif
    uint16      p_pending;      /* Pending signals */
    uint16      p_ignored;      /* Ignored signals */
#if 1 /* Nick */
    struct   s_ublock *p_ublk;  /* Pointer to udata block when not running */
#else
    struct   u_block *p_ublk;   /* Pointer to udata block when not running */
#endif
    uint16      p_waitno;       /* wait #; for finding longest waiting proc */
  /* Added for correct execve() of type 1 binaries */
    unsigned    p_fork_inf;     /* 0 = not child of fork
                                   1 = child of fork  */
#if 1 /* Nick UZIX compatible */
    void	*p_break;	/* process break level */
    uchar	p_intr; 	/* !0 if awakened by signal */
#endif
#if 1 /* Nick UZIX compatible */
} ptab_t, *ptptr;
#else
} p_tab, *ptptr;
#endif

/* Per-process data (Swapped with process) */
/*------ <use ASMDEF.I in Assembly Modules when needed>
_asm
OSYS	equ	2	; byte offsets of elements of u_data
OCALL	equ	3
ORET	equ	4	; Return Location
ORVAL	equ	6	; Return Value
OERR	equ	8	; Error Number
OSP	equ	10	; User's Stack Pointer
OBC	equ	12	; User's Frame Pointer
OPAGE	equ	121 + NSIGS ; Nick ; User's base BBR register value for Process
_endasm
------*/

#if 1 /* Nick UZIX compatible */
typedef struct s_udata {
#else
typedef struct u_data {
#endif
#if 1 /* Nick UZIX compatible */
    ptab_t      *u_ptab;        /* Process table pointer */
#else
    struct p_tab *u_ptab;       /* Process table pointer */
#endif
    char        u_insys;        /* True if in kernel */
    char        u_callno;       /* sys call being executed. */
    char        *u_retloc;      /* Return location from sys call */
    int         u_retval;       /* Return value from sys call */
    int         u_retval1;      /* Nick, for long return value from lseek() */
    int         u_error;        /* Last error number */
    int         *u_sp;          /* Used when process is swapped. */
    int         *u_bc;          /* Place for user's frame pointer */
    int         u_cursig;       /* Signal currently being caught */
#if 1 /* Nick */
    int         u_argn0;        /* Last system call arg */
#else
    int         u_argn;         /* Last system call arg */
#endif
    int         u_argn1;        /* This way because args on stack backwards */
    int         u_argn2;
    int         u_argn3;        /* args n-3, n-2, n-1, and n */

    char *      u_base;         /* Source or dest for I/O */
    unsigned    u_count;        /* Amount for I/O */
    off_t       u_offset; /* Nick blkoff_t    u_offset;       /* Place in file for I/O */
#if 1 /* Nick UZIX compatible */
    struct s_blkbuf *u_buf;
#else
    struct blkbuf *u_buf;
#endif
    char        u_sysio;        /* True if I/O to system space */

    int         u_gid;
    int         u_euid;
    int         u_egid;
    int         u_mask;         /* umask: file creation mode mask */
    uzitime_t      u_time;         /* Start time */
#if 0
    char        u_files[UFTSIZE];       /* Process file table:
	                           indices into open file table. */
#endif
    inoptr      u_cwd;          /* Index into inode table of cwd. */
    unsigned    u_break;        /* Top of data space */
    inoptr      u_ino;          /* Used during execve() */
    char        *u_isp;         /* Value of initial sp (argv) */
    int         (*u_sigvec[NSIGS])();   /* Array of signal vectors */
    char        u_name[8];      /* Name invoked with */
    uzitime_t      u_utime;        /* Elapsed ticks in user mode */
    uzitime_t      u_stime;        /* Ticks in system mode */
    uzitime_t      u_cutime;       /* Total childrens ticks */
    uzitime_t      u_cstime;
    char        u_page;         /* Process' MMU Base Address */
    inoptr      u_root;         /* Index into inode table of chroot target */
    char        u_traceme;	/* added by Nick, used only ifdef DEBUG */
    char        u_debugme;	/* added by Nick, used only ifdef DEBUG */
#if 1
    char        u_files[UFTSIZE];       /* Process file table:
	                           indices into open file table. */
#endif
#if 1 /* Nick UZIX compatible */
} udata_t;
#else
} u_data;
#endif


/* This is the user data structure, padded out to 512 bytes with the
 * System Stack.
 */
#if 1 /* Nick */
typedef struct s_ublock {
#else
typedef struct u_block {
#endif
#if 1 /* Nick UZIX compatible */
        udata_t	u_d;
        char	u_s [0x1000 /*512*/ - sizeof(struct s_udata)];
#else
        u_data u_d;
        char   u_s [512 - sizeof(struct u_data)];
#endif
#if 1 /* Nick */
} ublock_t;
#else
} u_block;
#endif


/* Struct to temporarily hold arguments in execve */
struct s_argblk {
    int a_argc;
    int a_arglen;
    int a_envc;
    char a_buf[512-3*sizeof(int)];
};


/* The device driver switch table */

#if 1 /* Nick UZIX compatible */
/* The device driver switch table */
typedef struct s_devsw {
	uchar	minors; 	/* # of minor device numbers */
	int	(*dev_init)(uchar minor);
	int	(*dev_open)(uchar minor);
	int	(*dev_close)(uchar minor);
	int	(*dev_read)(uchar minor, uchar rawflag);
	int	(*dev_write)(uchar minor, uchar rawflag);
	int	(*dev_ioctl)(uchar minor, int cmd, void *data);
} devsw_t;
#else
typedef struct devsw {
    int minor;          /* The minor device number (an argument to below) */
    int (*dev_open)();  /* The routines for reading, etc */
    int (*dev_close)(); /* Format: op(minor,blkno,offset,count,buf); */
    int (*dev_read)();  /* Offset would be ignored for block devices */
    int (*dev_write)(); /* Blkno and offset ignored for tty, etc. */
    int (*dev_ioctl)(); /* Count is rounded to 512 for block devices */
} devsw;
#endif

#if 1 /* Nick UZIX compatible */
#ifndef NATIVE
#ifndef __FCNTL_H
/* extra definitions added by Nick from sys/fcntl.h, needed for UTIL target */

#define O_RDONLY	0
#define O_WRONLY	1
#define O_RDWR		2

/* Flag values for open only */
#define O_CREAT 	0x0100	/* create and open file */
#define O_TRUNC 	0x0200	/* open with truncation */
#define O_NEW		0x0400	/* create only if not exist */
#define O_SYMLINK	0x0800	/* open symlink as file */

/* a file in append mode may be written to only at its end. */
#define O_APPEND	0x2000	/* to end of file */
#define O_EXCL		0x4000	/* exclusive open */
#define O_BINARY	0x8000	/* not used in unix */

#endif
#endif
#else
#ifndef _FCNTL_H
/* Open() parameters. */

#define O_RDONLY        0
#define O_WRONLY        1
#define O_RDWR          2
#endif
#endif

#if 1 /* Nick UZIX compatible */
#ifndef __ERRNO_H
/* Error codes */		/*- if not used */
#define EPERM		1		/* 1 Operation not permitted */
#define ENOENT		2		/* 2 No such file or directory */
#define ESRCH		3	/*-*/	/* 3 No such process */
#define EINTR		4		/* 4 Interrupted system call */
#define EIO		5		/* 5 I/O error */
#define ENXIO		6		/* 6 No such device or address */
#define E2BIG		7		/* 7 Arg list too long */
#define ENOEXEC 	8		/* 8 Exec format error */
#define EBADF		9		/* 9 Bad file number */
#define ECHILD		10		/* 10 No child processes */
#define EAGAIN		11		/* 11 Try again */
#define ENOMEM		12		/* 12 Out of memory */
#define EACCES		13		/* 13 Permission denied */
#define EFAULT		14		/* 14 Bad address */
#define ENOTBLK 	15		/* 15 Block device required */
#define EBUSY		16		/* 16 Device or resource busy */
#define EEXIST		17		/* 17 File exists */
#define EXDEV		18		/* 18 Cross-device link */
#define ENODEV		19		/* 19 No such device */
#define ENOTDIR 	20		/* 20 Not a directory */
#define EISDIR		21		/* 21 Is a directory */
#define EINVAL		22		/* 22 Invalid argument */
#define ENFILE		23		/* 23 File table overflow */
#define EMFILE		24	/*-*/	/* 24 Too many open files */
#define ENOTTY		25	/*-*/	/* 25 Not a typewriter */
#define ETXTBSY 	26	/*-*/	/* 26 Text file busy */
#define EFBIG		27	/*-*/	/* 27 File too large */
#define ENOSPC		28		/* 28 No space left on device */
#define ESPIPE		29		/* 29 Illegal seek */
#define EROFS		30	/*-*/	/* 30 Read-only file system */
#define EMLINK		31	/*-*/	/* 31 Too many links */
#define EPIPE		32		/* 32 Broken pipe */
#define EDOM		33	/*-*/	/* 33 Math argument out of domain of func */
#define ERANGE		34	/*-*/	/* 34 Math result not representable */
#define EDEADLK 	35	/*-*/	/* 35 Resource deadlock would occur */
#define ENAMETOOLONG	36	/*-*/	/* 36 File name too long */
#define ENOLCK		37	/*-*/	/* 37 No record locks available */
#define EINVFNC 	38		/* 38 Function not implemented */
#define ENOTEMPTY	39	/*-*/	/* 39 Directory not empty */
#define ELOOP		40	/*-*/	/* 40 Too many symbolic links encountered */
#define ESHELL		41	/*-*/	/* 41 It's a shell script */
#define ENOSYS		EINVFNC

#define __ERRORS	40
#endif
#else
#ifndef __ERRNO_H
/*
 * Error codes
 */
#define EPERM           1               /* Not owner */
#define ENOENT          2               /* No such file or directory */
#define ESRCH           3               /* No such process */
#define EINTR           4               /* Interrupted System Call */
#define EIO             5               /* I/O Error */
#define ENXIO           6               /* No such device or address */
#define E2BIG           7               /* Arg list too long */
#define ENOEXEC         8               /* Exec format error */
#define EBADF           9               /* Bad file number */
#define ECHILD          10              /* No children */
#define EAGAIN          11              /* No more processes */
#define ENOMEM          12              /* Not enough core */
#define EACCES          13              /* Permission denied */
#define EFAULT          14              /* Bad address */
#define ENOTBLK         15              /* Block device required */
#define EBUSY           16              /* Mount device busy */
#define EEXIST          17              /* File exists */
#define EXDEV           18              /* Cross-device link */
#define ENODEV          19              /* No such device */
#define ENOTDIR         20              /* Not a directory */
#define EISDIR          21              /* Is a directory */
#define EINVAL          22              /* Invalid argument */
#define ENFILE          23              /* File table overflow */
#define EMFILE          24              /* Too many open files */
#define ENOTTY          25              /* Not a typewriter */
#define ETXTBSY         26              /* Text file busy */
#define EFBIG           27              /* File too large */
#define ENOSPC          28              /* No space left on device */
#define ESPIPE          29              /* Illegal seek */
#define EROFS           30              /* Read-only file system */
#define EMLINK          31              /* Too many links */
#define EPIPE           32              /* Broken pipe */

/* math software */
#define EDOM            33              /* Argument too large */
#define ERANGE          34              /* Result too large */

#define ENAMETOOLONG    63              /* File name too long */
#endif
#endif

#if 1 /* Nick UZIX compatible */

#ifndef _SYS_IOCTL_H /* see change to "sys/ioctl.h" for utsname.c */

/* Info about a specific process, returned by sys_getfsys */
typedef struct s_pdata {
	int	u_pid;		/* Process PID */
	ptab_t	*u_ptab;	/* Process table pointer */
	uchar	u_name[DIRNAMELEN];	/* Name invoked with */

	/* syscall's interface */
	uchar	u_insys;	/* True if in kernel now */
	uchar	u_callno;	/* syscall being executed */
	uchar	u_traceme;	/* Process tracing flag */

	/* filesystem/user info */
	uchar	u_uid; 		/* user id */
	uchar	u_gid; 		/* group id */
	uchar	u_euid; 	/* effective user id */
	uchar	u_egid; 	/* group user id */
	uzitime_t	u_time; 	/* Start time */

	/* process flow info */
	signal_t u_cursig;	/* Signal currently being caught */

	/* time info */
	uzitime_t	u_utime;	/* Elapsed ticks in user mode */
	uzitime_t	u_stime;	/* Ticks in system mode */
};

/* Info about kernel, returned by sys_getfsys */
typedef struct s_kdata {
	uchar	k_name[14];	/* OS name */
	uchar	k_version[8];	/* OS version */
	uchar	k_release[8];	/* OS release */

	uchar	k_host[14];	/* Host name */
	uchar	k_machine[8];	/* Host machine */

	int	k_tmem; 	/* System memory, in kbytes */
	int	k_kmem; 	/* Kernel memory, in kbytes */
};

#endif

#define PRIO_MAX	19
#define PRIO_MIN	-20

#define WNOHANG		1
#define WUNTRACED	2

/* sys_getset() commands */
#define	GET_PID		0	/* get process id */
#define	GET_PPID	1	/* get parent process id */
#define	GET_UID		2	/* get user id */
#define	SET_UID		3	/* set user id */
#define	GET_EUID	4	/* get effective user id */
#define	GET_GID		5	/* get group id */
#define	SET_GID		6	/* set group id */
#define	GET_EGID	7	/* get effective group id */
#define	GET_PRIO	8	/* get process priority */
#define	SET_PRIO	9	/* set process priority */
#define	SET_UMASK	10	/* get/set umask */
#define	SET_TRACE	11	/* set trace flag */
#define	SET_DEBUG	12	/* set debug flag */

#endif

#ifndef SEEK_SET
/* extra definitions from sys/seek.h, it wasn't worth testing _SYS_SEEK_H */
#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2
#endif

#ifndef _SYS_IOCTL_H
/* extra definitions added by Nick from sys/ioctl.h, needed for UTIL target */
typedef enum {
	GI_PTAB = -1,	/* processes table */
	GI_ITAB = -2,	/* inodes table */
	GI_BTAB = -3,	/* buffers table */
	GI_FTAB = -4,	/* filesystems table */
	GI_UDAT = -5,	/* process user data */
	GI_UTAB = -6,	/* current process table */
	GI_PDAT = -7,	/* process info */
	GI_KDAT = -8	/* kernel info */
} getinfo_t;

typedef struct {
	getinfo_t req;
	int	size; /* Nick temporary size_t	size; */
	void	*ptr;
} info_t;
#endif

#if 1 /* Nick EXE format */
#define E_MAGIC 0xa6c9

#define E_FORMAT_LARGE 1
#define E_FORMAT_BANKED 2
#define E_FORMAT_KERNEL 3

typedef struct s_exefile
	{
	uint16	e_magic;
	uint16	e_format;
	off_t	e_size;
	uint16	e_hsize;
	uint16	e_idata;
	uint16	e_entry;
	uint16	e_udata;
	uint16	e_stack;
	uint16	e_break;
	} exefile_t;
#endif

#endif /* __UNIX_H */

