/* testsystem.c for uzi180 by Nick & Rob - initialises testsystem */

#include "vendor.h" /* Nick, must come first */
#include <cxfuncs.h>
#include <cxextern.h>
#include <string.h>
#include <unix.h>
#include <config.h>
#include <extern.h>
#include "devtty.h"
#include <stdio.h>
#include <syscalls.h>
#include "kprintf.h"
#include "extern.h"
#include "devno.h"
#include "touchscreen.h"
#include "testsystem.h"


extern void _putc(int minor, char c);

void testsystem_rx_hook(SDEV *device, char c);
void testsystem_rx_handler(SDEV *device, char c);

/* Testsystem defaults */
tsysT tsys = {
		0,	/* Testsystem not enabled */
		0	/* No recording of RTC data */
	     };


void testsystem_rx_handler(SDEV *device, char c)
{
	/*
	 * Receives and interprets the input stream from Serial 3,
	 * which is assumed to be simulated or recorded data, interleaved
	 * with special command sequences.
	 */
	static int state = 0, port = -1;

	if (tsys.enabled)
	{
		/* Hunt for a command or escaped sequence (starts with ESC) */
		if (c == 0x1b)
		{
			if (!state)
			{
				/* Start of command sequence */
				state = 1;
				return;
			}
			else
			{
				/* Escaped sequence */
				state = 0;
			}
		}
		else if (state)
		{
			switch (c)
			{
			case 'R': /* Toggle ext. RTC on/off */
				rtc.source = rtc.source == RTC_INT ?
							   RTC_EXT :
							   RTC_INT;
				break;
			case 'r': /* Toggle recording int. RTC on/off */
				tsys.rtc_rec = tsys.rtc_rec ? 0 : 1;
				break;
			default: /* 'Raw minor' device id */
				port = c;
			}
			
			state = 0;
			return;
		}


		/*
		 * First assume simulated INPUT from device
		 */
		switch (port)
		{
		case DEV_TS: /* Touchscreen */
			if (rtc.source == RTC_EXT && RTC_extract(c, RTC_EXT))
				break;
			/* Fall through */
		case DEV_TP: /* Thermal printer */
		case DEV_S1: /* Serial 1 - scanner */
		case DEV_LC: /* Load cell */
		case DEV_CD: /* Customer display */
			port = _2SDEV(port);
			sdevs[port].rx_vector(&sdevs[port], c);
			break;
		default:
			/*
			 * Next, assume OUTPUT to device;
			 * Bring 'raw minor' device id in normal range
			 */
			port -= DEVOUT_BASE;

			switch (port)
			{
			case DEV_TS:/* Touchscreen */
			case DEV_TP:/* Thermal printer */
			case DEV_S1:/* Serial 1 - scanner */
			case DEV_LC:/* Load cell */
			case DEV_CD:/* Customer display */
#ifdef RPB_DEBUG
				kprintf("p=%d, c=0x%x\n", port, c);
#endif
				_putc(port, c);
				break;
			default:
				/* No such device */
				kprintf("testsystem_rx_handler: ERROR port = %d, c = %d\n", port, c);
			}
		}
	}
}
	


void testsystem_rx_hook(SDEV *device, char c)
{
	/*
	 * Tags each received character with its source device id.
	 * Escapes special characters in the data stream.
	 */
	static char current_port = 0;
	char port;

	if (tsys.enabled)
	{
		port = _2MINOR(device->port);

		if (port != DEV_TS &&
		    port != DEV_TP &&
		    port != DEV_S1 &&
		    port != DEV_LC &&
		    port != DEV_CD) 
		{
			/* Not supported yet or error */
			kprintf("testsystem_rx_hook p=0x%x\n", (int)c);
			return;
		}

#if 0
		/*
		 * This part, remembering the current port, has problems
		 * with the testsystem_rx_handler() which keeps on
		 * interfering and overwrites 'current_port'.
		 * No solution to the problem yet (better understand it
		 * first), hence EACH received character output on S3
		 * is tagged with the device id.
		 */

		/*
		 * This may cause problems since all testsystem_rx_hooks
		 * access this same static current_port. Should be protected
		 * by a mutex => TODO
		 */
		if (port != current_port)
		{
			/* Insert minor device id before character received */
			_putc(DEV_S3, 0x1b);
			_putc(DEV_S3, port);
			current_port = port;
		}
#endif

		/* Insert minor device id before character received */
		_putc(DEV_S3, 0x1b);
		_putc(DEV_S3, port);

		/* Escape an ESC in the data stream */
		if (c == 0x1b)
			_putc(DEV_S3, 0x1b);

		_putc(DEV_S3, c);
	}

	/* Don't pass on RTC data */
	if (!rtc.seq)
		wsdevs[device->port].saved_rx_vector[1](device, c);
}

void testsystem_init(void)
{
	/*
	 * Initialises testsystem
	 */
	char p;

	/* Configures which devices get the testsystem hook installed */
#if 0 /* Serial 1 causes interrupt (re-entrance?) problems!! */
	wsdevs[_2SDEV(DEV_S1)].tsyshook = 1;
#endif
	wsdevs[_2SDEV(DEV_S1)].tsyshook = 0;
	wsdevs[_2SDEV(DEV_LC)].tsyshook = 0;
	wsdevs[_2SDEV(DEV_TS)].tsyshook = 1;
	wsdevs[_2SDEV(DEV_CD)].tsyshook = 1;
	wsdevs[_2SDEV(DEV_TP)].tsyshook = 1;

	K_OS_Disable_Interrupts();

	for (p = 0; p < PORTS; p++)
	{
		/* Skip Serial 3 itself */
		if (p != _2SDEV(DEV_S3))
		{
			/* Install testsystem hook on selected ports */
			if (wsdevs[p].tsyshook)
			{
				if (wsdevs[p].saved_rx_vector[0])
				{
					/* Install behind device special handler */
					wsdevs[p].saved_rx_vector[1] = wsdevs[p].saved_rx_vector[0];
					wsdevs[p].saved_rx_vector[0] = testsystem_rx_hook;
				}
				else
				{
					/* No device special handler */
					wsdevs[p].saved_rx_vector[1] = sdevs[p].rx_vector;
					sdevs[p].rx_vector = testsystem_rx_hook;
				}
			}
		}
		else
		{
			/* Install testsystem RX handler on serial 3 */
			if (wsdevs[p].saved_rx_vector[0])
			{
				/* Install behind device special handler */
				wsdevs[p].saved_rx_vector[1] = wsdevs[p].saved_rx_vector[0];
				wsdevs[p].saved_rx_vector[0] = testsystem_rx_handler;
			}
			else
			{
				/* No device special handler */
				wsdevs[p].saved_rx_vector[1] = sdevs[p].rx_vector;
				sdevs[p].rx_vector = testsystem_rx_handler;
			}
		}
	}

	K_OS_Enable_Interrupts();
	tsys.enabled = 1;

	kprintf("Test system enabled\n");
}

void testsystem_exit(void)
{
	/*
	 * Disables the testsystem
	 */
	char p;

	K_OS_Disable_Interrupts();

	for (p = 0; p < PORTS; p++)
	{
		/* Skip Serial 3 itself */
		if (p != _2SDEV(DEV_S3))
		{
			/* De-install testsystem hook on selected ports */
			if (wsdevs[p].tsyshook)
			{
				if (wsdevs[p].saved_rx_vector[0])
				{
					/* Remove behind device special handler */
					wsdevs[p].saved_rx_vector[0] = wsdevs[p].saved_rx_vector[1];
				}
				else
				{
					/* No device special handler */
					sdevs[p].rx_vector = wsdevs[p].saved_rx_vector[1];
				}
			}
		}
		else
		{
			/* Remove testsystem RX handler from Serial 3 */
			sdevs[p].rx_vector = wsdevs[p].saved_rx_vector[1];
		}
	}

	K_OS_Enable_Interrupts();
	tsys.enabled = 0;

	kprintf("Test system disabled\n");
}

