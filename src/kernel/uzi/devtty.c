/***************************************************************
   UZI (Unix Z80 Implementation) Kernel:  devtty.c
----------------------------------------------------------------
 Adapted from UZI By Doug Braun, and UZI280 by Stefan Nitschke
            Copyright (C) 1998 by Harold F. Bower
       Portions Copyright (C) 1995 by Stefan Nitschke
****************************************************************/
/* Revisions:
 *  22.12.97 - Moved __putc & tty_int to MACHASM.ASZ.	     HFB
 *  24.05.98 - Restructured tty_inproc, parse CTRL chars
 *	based on flag set bit (deletes CPM_CTRL in 280), add
 *	equate for Number of TTY terminals.		     HFB
 */

#if 0 /* Nick */
#define DEBUG            /* UNdefine to delete debug code sequences */

#define NTTYS  9 /* 2 */         /* Number of TTY ports defined */
#endif

#include "vendor.h" /* Nick, must come first */
#include <unix.h>
#include <config.h>
#include <extern.h>
#include "devtty.h" /* prototypes added by Nick */

struct s_tty_data tty_data[NTTYS+1];        /* tty_data[0] is not used */

struct s_tty_data tty_default =
	{
	0,         0,         '\b',       CTRL('x'), DFLT_MODE,
	CTRL('c'), CTRL('\\'), CTRL('q'), CTRL('s'), CTRL('d'), 0
	};

#if 1 /* Nick */
void (*tty_vector[NTTYS+1])(int minor, char c) =
	{
	tty_inproc_dummy, /* this entry is never used */
	tty_inproc_dummy,
	tty_inproc_dummy,
	tty_inproc_dummy,
	tty_inproc_dummy,
	tty_inproc_dummy,
	tty_inproc_dummy,
	tty_inproc_dummy,
	tty_inproc_dummy,
	tty_inproc_dummy
	};

extern char xmit_bufs[PORTS][XMIT_SIZE];
extern char recv_bufs[PORTS][RECV_SIZE];

struct s_queue ttyinq[NTTYS+1] =
	{
	{ NULL,	NULL, NULL, 0, 0, 0 },	/* this entry is never used */
	{ recv_bufs[0], recv_bufs[0], recv_bufs[0], TTYSIZ, 0, TTYSIZ/2 },
	{ recv_bufs[1], recv_bufs[1], recv_bufs[1], TTYSIZ, 0, TTYSIZ/2 },
	{ recv_bufs[2], recv_bufs[2], recv_bufs[2], TTYSIZ, 0, TTYSIZ/2 },
	{ recv_bufs[3], recv_bufs[3], recv_bufs[3], TTYSIZ, 0, TTYSIZ/2 },
	{ recv_bufs[4], recv_bufs[4], recv_bufs[4], TTYSIZ, 0, TTYSIZ/2 },
	{ recv_bufs[5], recv_bufs[5], recv_bufs[5], TTYSIZ, 0, TTYSIZ/2 },
	{ recv_bufs[6], recv_bufs[6], recv_bufs[6], TTYSIZ, 0, TTYSIZ/2 },
	{ recv_bufs[7], recv_bufs[7], recv_bufs[7], TTYSIZ, 0, TTYSIZ/2 },
	{ recv_bufs[8], recv_bufs[8], recv_bufs[8], TTYSIZ, 0, TTYSIZ/2 }
	};
#else
/* Character Input Queue size */
#define TTYSIZ 132

char tbuf1[TTYSIZ];
char tbuf2[TTYSIZ];

struct  s_queue  ttyinq[NTTYS+1] = {       /* ttyinq[0] is never used */
{   NULL,    NULL,    NULL,    0,        0,       0    },
{   tbuf1,   tbuf1,   tbuf1,   TTYSIZ,   0,   TTYSIZ/2 },
{   tbuf2,   tbuf2,   tbuf2,   TTYSIZ,   0,   TTYSIZ/2 }
};
#endif

int stopflag[NTTYS+1];   /* Flag for ^S/^Q */
int flshflag[NTTYS+1];   /* Flag for ^O */

int tty_read(uchar minor, uchar rawflag)
{
    int  nread;
    char c;
    int  remq();

#if DEBUG >= 2
 dprintf(2, "tty_read(%d, %d) starting\n", minor, rawflag);
#endif

       /* Minor == 0 means that it is the controlling tty of the process */
    ifnot (minor)
        minor = udata.u_ptab->p_tty;
    ifnot (udata.u_ptab->p_tty)
        udata.u_ptab->p_tty = minor;

    if ((minor < 1) || (minor > NTTYS+1)) {
        udata.u_error = ENODEV;
#if DEBUG >= 2
 dprintf(2, "tty_read() returning -1, error %d\n", udata.u_error);
#endif
        return (-1);
    }

    nread = 0;
    while (nread < udata.u_count)
    {
        for (;;)
        {
            di_absolute(); /* Nick */
            if (remq (&ttyinq[minor], &c)) {
#if 1 /* Nick */
                ei_absolute(); /* Nick */
#endif
                if (udata.u_sysio)
                    *udata.u_base = c;
                else
                    uputc (c, udata.u_base);
                break;
            }
#if 1 /* Nick */
            ei_absolute();
#endif
#if 1 /* Nick */
            /* In unbuffered mode, don't wait when no input */
            if (tty_data[minor].t_flags & UNBUFF)
 {
#if DEBUG >= 2
 dprintf(2, "tty_read() returning %d, success\n", nread);
#endif
                return(nread);
 }
#endif
#if 1 /* Nick, original uzi code, it can be handy to disable this sometimes */
            psleep (&ttyinq[minor]);
            if (udata.u_cursig || udata.u_ptab->p_pending) {     /* messy */
                udata.u_error = EINTR;
#if DEBUG >= 2
 dprintf(2, "tty_read() returning -1, error %d\n", udata.u_error);
#endif
                return (-1);
            }
#endif
        }
#if 0 /* Nick, removed as this is now done immediately after calling remq */
        ei_absolute(); /* Nick */
#endif
        if ((nread++ == 0) && (c == tty_data[minor].t_eof))   /* ^D */
 {
#if DEBUG >= 2
 dprintf(2, "tty_read() returning 0, eof\n");
#endif
            return(0);
 }

        /* In raw or cbreak mode, return after one char */
#if 1 /* Nick */
        if ((tty_data[minor].t_flags & UNBUFF) == 0 &&
		(c == '\n' || tty_data[minor].t_flags & (RAW|CBREAK)))
		{
		break;
		}
#else
        if (tty_data[minor].t_flags & (RAW|CBREAK))
            break;
        if (c == '\n')
            break;
#endif
        ++udata.u_base;
    } 
#if DEBUG >= 2
 dprintf(2, "tty_read() returning %d, success\n", nread);
#endif
    return(nread);
}



int tty_write(uchar minor, uchar rawflag)
{
    int towrite, c;
/* Nick    int ugetc(); */

       /* Minor == 0 means that it is the controlling tty of the process */
    ifnot (minor)
        minor = udata.u_ptab->p_tty;
    ifnot (udata.u_ptab->p_tty)
        udata.u_ptab->p_tty = minor;

    if ((minor < 1) || (minor > NTTYS+1)) {
        udata.u_error = ENODEV;
        return (-1);
    }

    towrite = udata.u_count;

    while (udata.u_count-- != 0)
    {
        for (;;)        /* Wait on the ^S/^Q flag */
        {
            di_absolute(); /* Nick */
            ifnot (stopflag[minor])
                break;
            psleep (&stopflag[minor]);
            if (udata.u_cursig || udata.u_ptab->p_pending)  /* messy */
            {
                udata.u_error = EINTR;
                return (-1);
            }
        }
        ei_absolute(); /* Nick */

        ifnot (flshflag[minor])
        {
            if (udata.u_sysio)
                c = *udata.u_base;
            else
                c = ugetc (udata.u_base);

            if (c == '\n' && (tty_data[minor].t_flags & CRMOD))
                _putc (minor, '\r');
            _putc (minor, c);
        }
        ++udata.u_base;
    }
    return (towrite);
}



int tty_open(uchar minor)
{
       /* Minor == 0 means that it is the controlling tty of the process */
    ifnot (minor)
        minor = udata.u_ptab->p_tty;

#if 1 /* Nick */
    if ((minor < 1) || (minor > NTTYS+1))
	{
	udata.u_error = ENODEV;
	return (-1);
	}
#endif

       /* If there is no controlling tty for the process, establish it */
    ifnot (udata.u_ptab->p_tty)
        udata.u_ptab->p_tty = minor;

       /* Initialize the tty_data */
    bcopy (&tty_default, &tty_data[minor], sizeof (struct s_tty_data));

#if 1 /* Nick */
    di_absolute();
    tty_vector[minor] = tty_inproc;
    ei_absolute();
#endif

    return (0);
}


int tty_close(uchar minor)
{
       /* If we are closing the controlling tty, make note */
    if (minor == udata.u_ptab->p_tty)
        udata.u_ptab->p_tty = 0;

#if 1 /* Nick */
    if ((minor < 1) || (minor > NTTYS+1))
	{
	udata.u_error = ENODEV;
	return (-1);
	}
#endif

#if 1 /* Nick */
    di_absolute();
    tty_vector[minor] = tty_inproc_dummy;
    ei_absolute();
#endif

    return (0);
}


/* Data in User Space */
int tty_ioctl(uchar minor, int request, char *data)
{
       /* Minor == 0 means that it is the controlling tty of the process */
    ifnot (minor)
        minor = udata.u_ptab->p_tty;
    if ((minor < 1) || (minor > NTTYS+1)) {
        udata.u_error = ENODEV;
        return (-1);
    }
    switch (request)
    {
        case TIOCGETP:
            uput (&tty_data[minor], data, 6);
            break;
        case TIOCSETP:
            uget (data, &tty_data[minor], 6);
            break;
        case TIOCGETC:
            uput (&tty_data[minor].t_intr, data, 5);
            break;
        case TIOCSETC:
            uget (data, &tty_data[minor].t_intr, 5);
            break;
        case TIOCSETN:
            uput (&ttyinq[minor].q_count, data, 2);
            break;
        case TIOCFLUSH:
            clrq (&tty_data[minor]);
            break;
        case TIOCTLSET:
            tty_data[minor].ctl_char = 1;
            break;
        case TIOCTLRES:
            tty_data[minor].ctl_char = 0;
            break;
        default:
            udata.u_error = EINVAL;
            return (-1);
    }
    return (0);
}


/* This routine processes a character in response to an interrupt.  It
 * adds the character to the tty input queue, echoing and processing
 * backspace and carriage return.  If the queue contains a full line,
 * it wakes up anything waiting on it.  If it is totally full, it beeps
 * at the user.
 * UZI180 - This routine is called from the raw Hardware read routine,
 * either interrupt or polled, to process the input character.  HFB
 */

void tty_inproc(int minor, char c)
{
    char  oc;
    struct s_tty_data *td;
    int   mode;
    int   insq(), uninsq();

    td = &tty_data[minor];
#if 1 /* Nick */
    if ((td->t_flags & (RAW|CBREAK|COOKED|UNBUFF)) == (RAW|UNBUFF))
	{
	insq (&ttyinq[minor], c);
	return; /* don't bother checking for error or waking task */
	}
#endif
    mode = td->t_flags & (RAW|CBREAK|COOKED);

/***    if (c == 0x1b && td->t_flags == DFLT_MODE)
/***            return;                   /* my terminal hates it SN */

    if (mode != RAW)
 { /* Nick */
        c &= 0x7f;                /* Strip off parity */
    if (!c)
        return;                   /* Simply quit if Null character */
 } /* Nick */

    if (td->ctl_char == 0)         /* Don't parse ctl chars if Non-0 */
    {
        if ((mode & RAW) == 0)       /* if mode == COOKED or CBREAK */
        {
#if DEBUG
          if (c == 0x1a)                /* ^Z */
              {
              idump();                  /*   (For debugging) */
              return;
              }
          else if (c == 0x0e)           /* ^N Nick */
              {
              traceon = !traceon;
              return;
              }
#endif

            if (c == '\r' && (td->t_flags & CRMOD))
                c = '\n';

            if (c == td->t_intr) {                   /* ^C */
                sgrpsig (minor, SIGQUIT);
                clrq (&ttyinq[minor]);
                stopflag[minor] = flshflag[minor] = 0;
                return;
            }
            else if (c == td->t_quit) {              /* ^\ */
                sgrpsig (minor, SIGINT);
                clrq (&ttyinq[minor]);
                stopflag[minor] = flshflag[minor] = 0;
                return;
            }
            else if (c == '\017') {                  /* ^O */
                flshflag[minor] = !flshflag[minor];
                return;
            }
            else if (c == td->t_stop) {              /* ^S */
                stopflag[minor] = 1;
                return;
            }
            else if (c == td->t_start) {             /* ^Q */
                stopflag[minor] = 0;
                wakeup (&stopflag[minor]);
                return;
            }
        }

        if (mode == COOKED)
        {
            if (c == td->t_erase)
            {
                if (uninsq (&ttyinq[minor], &oc))
                {
                   if (oc == '\n')
                        insq (&ttyinq[minor], oc);  /* Don't erase past nl */
                    else
                    {
                        echo (minor, '\b');
                        echo (minor, ' ');
                        echo (minor, '\b');
                    }
                }
                return;
            }
            else if (c == td->t_kill)
            {
                while (uninsq (&ttyinq[minor], &oc))
                {
                    if (oc == '\n')
                    {
                        insq (&ttyinq[minor], oc);  /* Don't erase past nl */
                        break;
                    }
                    echo (minor, '\b');
                    echo (minor, ' ');
                    echo (minor, '\b');
                }
                return;
            }
        }
    }
       /* All modes come here */

    if (c == '\n' && (td->t_flags & CRMOD))
        echo (minor, '\r');

    if (insq (&ttyinq[minor], c))
        echo (minor, c);
    else
        _putc (minor, '\007');        /* Beep if no more room */

    if ((mode != COOKED || c == '\n' || c == td->t_eof)
       && (td->ctl_char == 0))                 /* ^D */
          wakeup (&ttyinq[minor]);
/* abyte('^'); */
/* abyte(c); */
/* while (1) */
/*  ; */
}


#if 1 /* Nick */
void tty_inproc_dummy(int minor, char c)
	{
	}
#endif


void echo(int minor, char c)
{
    if (tty_data[minor].t_flags & ECHO)
        _putc (minor, c);
}


#if 0
char silly_pause(void)
	{
	char c;

	for (;;)
		{
		di_absolute(); /* Nick */
		if (remq (&ttyinq[2], &c))
			{
			return c;
			}
#if 1 /* Nick... very temporary!! */
 ei_absolute();
#endif
		}
	}
#endif


