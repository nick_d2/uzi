;/***************************************************************
;   UZI (Unix Z80 Implementation) Kernel:  procasm.asz
;----------------------------------------------------------------
; Adapted from UZI By Doug Braun, and UZI280 by Stefan Nitschke
;            Copyright (C) 1998 by Harold F. Bower
;       Portions Copyright (C) 1995 by Stefan Nitschke
;****************************************************************/
;/* Revisions:
; */

; #include <unix.h>
; #include <config.h>
; #include <extern.h>


	public	_swapout, _swapin, _dofork, _newid		; PUBlic

	extern	_chksigs, _ptab_alloc, _getproc, _newproc	; in process.c
	extern	_panic						; in machdep.c
	extern	_tempstack, _ei, _osBank			; in machasm.asz
	extern	_ub, _runticks					; in data.c

	extern	copyr
	extern	abyte

 .if 1 ; IAR
	extern	?BANK_CALL_DIRECT_L08
	extern	?BANK_FAST_LEAVE_L08
 .endif

$ asmdef.inc
$ z180.inc

	rseg	UDATA0
F199:	 defs	2	; static ptptr newp (swapout)
F207:	 defs	2	; static ptptr newp (swapin)
F218:	 defs	2	; static ptptr p (dofork)
	; /* Temp storage for dofork */
_newid:	 defs	2	; int16 newid;
	rseg	CODE

;---------------------------------------------------------------
; /* Swapout swaps out the current process, finds another that is READY,
;  * possibly the same process, and swaps it in.  When a process is
;  * restarted after calling swapout, it thinks it has just returned
;  * from swapout().
;  */
; /* This function can have no arguments or auto variables */

; swapout()
; {

_swapout:
 .if 1 ; IAR
	push	bc
	push	de
 .endif
; this doesn't do anything - see chksigs
;	DI			; Don't allow Ints while we do this

;     static ptptr newp;
;     ptptr getproc();

;     /* See if any signals are pending */
;     chksigs();

 .if 1 ; IAR
	ld	hl,LWRD _chksigs
	ld	a,BYTE3 _chksigs
	call	?BANK_CALL_DIRECT_L08
 .else
	call	_chksigs
 .endif

;     /* Get a new process */
;     newp = getproc();

 .if 1 ; IAR
	ld	hl,LWRD _getproc
	ld	a,BYTE3 _getproc
	call	?BANK_CALL_DIRECT_L08
 .else
	call	_getproc
 .endif
	ld	(F199),hl

; added by Nick, see above
	DI			; Don't allow Ints while we do this

;     /* If there is nothing else to run, just return */
;     if (newp == udata.u_ptab)

	LD	DE,(_ub)
	or	a
	sbc	hl,de
	JR	nz,L19

;     {
;         udata.u_ptab->p_status = P_RUNNING;

	EX	DE,HL
	ld	(hl),P_RUNNING ;1

;         return (runticks = 0);

	ld	hl,0
	ld	(_runticks),hl

; added by Nick, see above
	EI

 .if 1 ; IAR
	pop	de
	pop	bc
	jp	?BANK_FAST_LEAVE_L08
 .else
	ret
 .endif

;     }
;     /* Save the stack pointer and critical registers */
;     /*  Returns 1 if swapped */
; #asm

L19:
; added by Nick, see above
	EI

	ld	hl,01		; This will return 1 if swapped
	push	hl		;  will be return value
	push	ix
	push	iy

; #endasm
;     udata.u_sp = stkptr;

	ld	(_ub+OSP),sp

; Write the User Data Block to storage in the respective Memory Bank
; UZI180 - replaces "swrite();" with "uput (0xf000, 0xf000, 768);"
;     swrite();
;	call	_swrite

	LD	HL,1000h ;768		; Save all of ub, KStack and IStack
	LD	(dmaLen),HL	;  Length
 .if 1 ; new register convention
	; new:	L:BC = destination address	(L = CBR value, BC = offset)
	;	H:DE = source address		(H = CBR value, DE = offset)

	ld	a,(_ub+OPAGE)
	ld	l,a
	ld	bc,7000h	; just before process data begins at 8000h

	sub	a ;ld a,(_osBank)
	ld	h,a
	ld	de,_ub		; processing area in kernel data segment
 .else
	; old:	  L = Destination Bank #
	;	  E = Source Bank #
	;	 BC = Start/Destination Address in Banks

 .if 1 ; Nick
	LD	BC,_ub		;   Start Address in both Src and Dst
 .else
	LD	BC,0F000H	;   Start Address in both Src and Dst
 .endif
	LD	A,(_osBank)
	LD	E,A		;    Source Bank is Common1
	LD	HL,(_ub+OPAGE)	;     Dest Bank if that of Current Process
 .endif
	CALL	LWRD MovDMA	; Save Common:udata to User:udata!

;     /* Read the new process in, and return into its context. */
;     swapin (newp);

 .if 1 ; IAR
	ld	de,(F199)	; 1st argument is passed in de

	ld	hl,LWRD _swapin
	ld	a,BYTE3 _swapin
	call	?BANK_CALL_DIRECT_L08
 .else
	ld	hl,(F199)
	push	hl
	call	_swapin
 .endif


;     /* We should never get here. */
;     panic ("swapin failed");

	ld	hl,msg19
	ex	(sp),hl
 .if 1 ; IAR
	ld	hl,LWRD _panic
	ld	a,BYTE3 _panic
	call	?BANK_CALL_DIRECT_L08
 .else
	call	_panic
 .endif
 .if 0 ; should never get here, because panic() doesn't return
	pop	bc
	ret
 .endif
; }


;---------------------------------------------------------------
; /* No automatics can be used past tempstack();
;  */
; swapin (pp)
; ptptr pp;
; {

_swapin:
 .if 1 ; IAR
;	push	bc		; don't bother, we're throwing away our stack
;	push	de		; don't bother, we're throwing away our stack
 .endif

;     static blkno_t blk;
;     static ptptr newp;

;     di();

	DI			; No Ints while we work here

 .if 1 ; IAR
	ex	de,hl		; hl = 1st argument (passed in de)
 .else
	pop	de		; Ret Addr
	pop	hl		;  newp
	push	hl
	push	de
 .endif
	ld	(F207),hl	; local newp

;     /* See if process has ignored signals.  If not ignored 0CH,
;      * increase process priority (maybe an interactive task)
;      */
;     if (newp->p_ignored != 0xC)

	ld	de,0CH
	ld	iy,(F207)
	ld	l,(iy+20)
	ld	h,(iy+21)
	or	a
	sbc	hl,de
	JR	z,L1

;         newp->p_priority = MAXINTER;

	ld	(iy+16),5
	JR	L22

;     else
;         newp->p_priority = MAXBACK2;

L1:	ld	(iy+16),2

;     tempstack();

L22:	ld	(iy+17),0
 .if 0 ; oopsy, don't do this IAR
	ld	hl,LWRD _tempstack
	ld	a,BYTE3 _tempstack
	call	?BANK_CALL_DIRECT_L08
 .else
	call	_tempstack
 .endif

;     /* Reload the user area */
; UZI180 - User Data Area restored from Banked memory, and User Bank restored
; to context at exit.  This replaces the reads/writes to/from swap device.

;     swapread (SWAPDEV, blk, 512, ((char *)(&udata+1))-512 );

;     /* The user address space is read in two i/o operations,
;      * one from 0x100 to the break, and then from the stack up.
;      * Notice that this might also include part or all of the
;      * user data, but never anything above it.
;      */
;     swapread (SWAPDEV,
;               blk+1,
;               (((char *)(&udata+1))-PROGBASE) & ~511,
;               PROGBASE);

;     udata.u_page = newp->p_page;	/* Set u_page so uget works */

	LD	IY,(F207)
	LD	A,(IY+14)
	ld	(_ub+OPAGE),a

;    uget (0xf000, 0xf000, 768);

	LD	HL,1000h ;768		; Get all of ub, KStack and IStack
	LD	(dmaLen),HL	;  Length
 .if 1 ; new register convention
	; new:	L:BC = destination address	(L = CBR value, BC = offset)
	;	H:DE = source address		(H = CBR value, DE = offset)

	ld	a,(_ub+OPAGE)
	ld	h,a
	ld	de,7000h	; just before process data begins at 8000h

	sub	a ;ld a,(_osBank)
	ld	l,a
	ld	bc,_ub		; processing area in kernel data segment
 .else
	; old:	  L = Destination Bank #
	;	  E = Source Bank #
	;	 BC = Start/Destination Address in Banks

 .if 1 ; Nick
	LD	BC,_ub		;   Start Address in both Src and Dst
 .else
	LD	BC,0F000H	;   Start Address in both Src and Dst
 .endif
	LD	A,(_ub+OPAGE)
	LD	E,A		;    Source Bank is that of Current Process
	LD	HL,(_osBank)	;     Dest Bank is Common1
 .endif
	CALL	LWRD MovDMA	; Move User:ubase to Common:ubase

;     if (newp != udata.u_ptab)

	ld	de,(F207)
	ld	hl,(_ub)
	or	a
	sbc	hl,de
	JR	z,L23

;         panic ("mangled swapin");

	ld	hl,msg29
	push	hl
 .if 1 ; IAR
	ld	hl,LWRD _panic
	ld	a,BYTE3 _panic
	call	?BANK_CALL_DIRECT_L08
 .else
	call	_panic
 .endif
	pop	bc

;     di();

L23:	DI

;     newp->p_status = P_RUNNING;

	EX	DE,HL
	ld	(hl),P_RUNNING ;1

;     runticks = 0;

	ld	hl,0
	ld	(_runticks),hl

;     /* Restore the registers */
;     stkptr = udata.u_sp;

	ld	sp,(_ub+OSP)

; #asm
	pop	iy
	pop	ix
 .if 1
	ld	hl,LWRD _ei
	ld	a,BYTE3 _ei
	call	?BANK_CALL_DIRECT_L08
	pop	hl

	pop	de
	pop	bc
	jp	?BANK_FAST_LEAVE_L08
 .else
	pop	hl
	JP	_ei		; ..Exit, possibly enabling Ints
 .endif

; #endasm          /* return into the context of the swapped-in process */
; }

;---------------------------------------------------------------
; /* Dofork implements forking.
;  * This function can have no arguments or auto variables.
;  */
; dofork()
; {

_dofork:
 .if 1 ; IAR
	push	bc
	push	de
 .endif

;     static ptptr p;
;     ptptr ptab_alloc();

;     ifnot (p = ptab_alloc())

 .if 1 ; IAR
	ld	hl,LWRD _ptab_alloc
	ld	a,BYTE3 _ptab_alloc
	call	?BANK_CALL_DIRECT_L08
 .else
	call	_ptab_alloc
 .endif
	ld	(F218),hl
	ld	a,l
	or	h
	JR	nz,L26

;     {
;         udata.u_error = EAGAIN;

	ld	hl,EAGAIN ;11
	ld	(_ub+OERR),hl

;         return(-1);

	ld	hl,-1
 .if 1 ; IAR
	pop	de
	pop	bc
	jp	?BANK_FAST_LEAVE_L08
 .else
	ret
 .endif

;     }
;     di();

L26:	DI

;     udata.u_ptab->p_status = P_READY;     /* Parent is READY */

	ld	hl,(_ub)
	ld	(hl),P_READY ;2

;     newid = p->p_pid;

	ld	HL,(F218)
	INC	HL
	INC	HL
	LD	A,(HL)
	INC	HL
	LD	H,(HL)
	LD	L,A
	ld	(_newid),hl

;     /* Save the stack pointer and critical registers.
;      * When the process is swapped back in, it will be as if
;      * it returns with the value of the childs pid.
;      */
; #asm
	push	hl		; HL still has _newid from above
	push	ix
	push	iy
; #endasm

;     udata.u_sp = stkptr;

	ld	(_ub+OSP),sp

;     uput (0xf000, 0xf000, 768);	/* in lieu of call to swrite() */

	LD	HL,1000h ;768		; Save all of ub, KStack and IStack
	LD	(dmaLen),HL	;  Length
 .if 1 ; new register convention
	; new:	L:BC = destination address	(L = CBR value, BC = offset)
	;	H:DE = source address		(H = CBR value, DE = offset)

	ld	a,(_ub+OPAGE)
	ld	l,a
	ld	bc,7000h	; just before process data begins at 8000h

	sub	a ;ld a,(_osBank)
	ld	h,a
	ld	de,_ub		; processing area in kernel data segment
 .else
	; old:	  L = Destination Bank #
	;	  E = Source Bank #
	;	 BC = Start/Destination Address in Banks

 .if 1 ; Nick
	LD	BC,_ub		;   Start Address in both Src and Dst
 .else
	LD	BC,0F000H	;   Start Address in both Src and Dst
 .endif
	LD	A,(_osBank)
	LD	E,A		;    Source Bank is Common1
	LD	HL,(_ub+OPAGE)	;     Dest Bank if that of Current Process
 .endif
	CALL	LWRD MovDMA	; Save Common:ubase to User:ubase!

; #asm
	pop	hl		; reg set flag not needed
	pop	hl		; repair stack pointer
	pop	hl
; #endasm

	LD	A,(_ub+OPAGE)	; Get Parent's Page
	PUSH	AF		;   save for execve use

;     /* Make a new process table entry, etc. */
;     newproc (p);

 .if 1 ; IAR
	ld	de,(F218)		; de = p = 1st argument to call

	ld	hl,LWRD _newproc
	ld	a,BYTE3 _newproc
	call	?BANK_CALL_DIRECT_L08
 .else
	ld	hl,(F218)
	push	hl
	call	_newproc
	pop	bc
 .endif

;     di();

	DI			; (in case newproc enabled ints)

;     runticks = 0;

	ld	hl,0
	ld	(_runticks),hl

;     p->p_status = P_RUNNING;

	ld	hl,(F218)
	ld	(hl),P_RUNNING ;1

;     /* Added for correct execve() operation */
;     p->p_fork_inf = Parent_udata.u_page;      /* Mark with Parent's Page # */

	LD	DE,26		; Offset to p_fork_inf
	ADD	HL,DE
	POP	AF		; Restore Parent's Page #
	LD	(HL),A		;  save
	INC	HL
	LD	(HL),0		;   as Integer
	; a & hl will be used again below !!

; Copy entire 60k process into Child's address space

 .if 1 ; new register convention
	; new:	L:BC = destination address	(L = CBR value, BC = offset)
	;	H:DE = source address		(H = CBR value, DE = offset)

 .if 1 ; banked EXE format
	; don't corrupt a in here !!
	inc	hl
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	ex	de,hl			; hl = process's break level

	ld	de,8000h		; de = start of process, PROGBASE-100h
	or	a
	sbc	hl,de			; hl = bytes to copy from start
	ld	(dmaLen),hl
 .else
	ld	hl,PROGTOP-PROGBASE
	ld	(dmaLen),hl		; length to move

	ld	de,PROGBASE		; source address
 .endif
	ld	h,a			; source bank

	ld	a,(_ub+OPAGE)
	ld	l,a			; destination bank
	ld	c,e
	ld	b,d			; destination address is same as source
 .else
	; old:	  L = Destination Bank #
	;	  E = Source Bank #
	;	 BC = Start/Destination Address in Banks

	LD	E,A		; Source Bank is Parent's Bank (E)
 .if 1
	ld	hl,PROGTOP-PROGBASE
	ld	(dmaLen),hl	; length to move
	ld	bc,PROGBASE	; addr in both starts at PROGBASE
 .else
	LD	HL,0F000H
	LD	(dmaLen),HL	;  Length to move is 60k
	LD	C,L
	LD	B,L		;   Addr in both starts at 0000H
 .endif
	LD	HL,(_ub+OPAGE)	;    Dest is Child's Bank (L)
 .endif
	CALL	LWRD MovDMA	; Copy Parent's data to Child's Space

;     ei();

 .if 1 ; IAR
	ld	hl,LWRD _ei
	ld	a,BYTE3 _ei
	call	?BANK_CALL_DIRECT_L08
 .else
	call	_ei
 .endif

;     return (0);                 /* Return to child */

	ld	hl,0
; }
 .if 1 ; IAR
	pop	de
	pop	bc
	jp	?BANK_FAST_LEAVE_L08
 .else
	ret
 .endif


;================================================================
; Tailored generic DMA Move routine for Process Swaps used instead
; of calls to _uget and _uput to avoid all C-Procedure overhead.
; This code uses direct register setup for DMA Channel 0 instead
; of setting a data block and using the OTIMR block output instruc-
; tion.  This code is six bytes longer, but 74 T-states faster.
; Enter: Length to move in variable dmaLen
; new:	L:BC = destination address	(L = CBR value, BC = offset)
;	H:DE = source address		(H = CBR value, DE = offset)
; old:	  L = Destination Bank #
;	  E = Source Bank #
;	 BC = Start/Destination Address in Banks
; Exit : None
; Uses : AF,BC,DE,HL

MovDMA:
 .if 1 ; new register convention
	; new:	L:BC = destination address	(L = CBR value, BC = offset)
	;	H:DE = source address		(H = CBR value, DE = offset)

	ld	a,e
	ld	e,d		; Shuffle to deal with high 16-bits
	ld	d,0		;   and Addr to 16-bits

	push	hl
	ld	l,h		; so that l = source bank #

	ld	h,d ;0		; Expand Source Bank
	add	hl,hl		; Source Bank #
	add	hl,hl		;  * 16
	add	hl,hl
	add	hl,hl
	add	hl,de		;   + Hi 8-bits of addr

	ld	d,l
	ld	e,a		; h:de -> source data
	push	de		; save value for iy
	pop	iy		; h:iy -> source data
	ld	d,h		; d:iy -> source data

	ld	a,c
	ld	c,b		; Shuffle to deal with high 16-bits
	ld	b,0		;   and Addr to 16-bits

	pop	hl		; so that l = dest bank #

	ld	h,b ;0		; Expand Dest Bank
	add	hl,hl		; Dest Bank #
	add	hl,hl		;  * 16
	add	hl,hl
	add	hl,hl
	add	hl,bc		;   + Hi 8-bits of Addr

	ld	e,h
	ld	h,l
	ld	l,a		; e:hl -> destination spot

	ld	bc,(dmaLen)	; bc = count
	jp	copyr		; go and copy it, in small blocks !!
 .else
	; old:	  L = Destination Bank #
	;	  E = Source Bank #
	;	 BC = Start/Destination Address in Banks

 .if 1 ; Nick
	ld	a,c
	ld	c,b		; Shuffle to deal with high 16-bits
	ld	b,0		;   and Addr to 16-bits

	ex	de,hl		; so that l = source bank #

	ld	h,b ;0		; Expand Source Bank
	add	hl,hl		; Source Bank #
	add	hl,hl		;  * 16
	add	hl,hl
	add	hl,hl
	add	hl,bc		;   + Hi 8-bits of addr

	push	hl		; h will be restored into d, later on
	ld	h,l
	ld	l,a		; e:hl -> source data
	ex	(sp),hl		; save value for iy, get h = value for d
	pop	iy		; h:iy -> source data

	ex	de,hl		; so that l = destination bank #
				; d:iy -> source data

	ld	h,b ;0		; Expand Dest Bank
	add	hl,hl		; Dest Bank #
	add	hl,hl		;  * 16
	add	hl,hl
	add	hl,hl
	add	hl,bc		;   + Hi 8-bits of Addr

	ld	e,h
	ld	h,l
	ld	l,a		; e:hl -> destination spot

	ld	bc,(dmaLen)	; bc = count
	jp	copyr		; go and copy it, in small blocks !!
 .else
	OUT0	(SAR0L),C	; LSB Source Addr
	OUT0	(DAR0L),C	;  LSB Dest Addr
	LD	C,B		; Shuffle to deal with high 16-bits
	XOR	A
	LD	H,A		; Expand Dest Bank
	LD	D,A		;  Source Bank
	LD	B,A		;   and Addr to 16-bits
	ADD	HL,HL		; Dest Bank #
	ADD	HL,HL		;  * 16
	ADD	HL,HL
	ADD	HL,HL
	ADD	HL,BC		;   + Hi 8-bits of Addr
	OUT0	(DAR0L+1),L	;    Set Dest middle Byte
	OUT0	(DAR0B),H	;     and Dest Bank
	EX	DE,HL		; Place Source Bank for work
	ADD	HL,HL		; Source Bank #
	ADD	HL,HL		;  * 16
	ADD	HL,HL
	ADD	HL,HL
	ADD	HL,BC		;   + Hi 8-bits of Addr
	OUT0	(SAR0L+1),L	;    Set Source middle Byte
	OUT0	(SAR0B),H	;     and Source Bank
	LD	HL,(dmaLen)	; Get Length of move
	OUT0	(BCR0L),L	;  Set Low
	OUT0	(BCR0H),H	;   Set High
	LD	A,00000010B	; Set DMA Mode control to Burst Mode
	OUT0	(DMODE),A
	LD	A,40H		; Enable DMA0
	OUT0	(DSTAT),A	;   move the block
	RET			;     done.
 .endif
 .endif

;----------------------------------------------------------------
	rseg	IDATA0
msg19:	defs	14
	rseg	CDATA0
	DEFM	'swapin failed'
	DEFB	0
	rseg	IDATA0
msg29:	defs	15
	rseg	CDATA0
	DEFM	'mangled swapin'
	DEFB	0

	rseg	UDATA0
dmaLen:	DEFS	2		; Length of DMA Move

	end
