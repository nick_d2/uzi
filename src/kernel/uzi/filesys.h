/* filesys.h for UZI180 by Nick (we are converting everything to ANSI C) */

#ifndef __FILESYS_H
#define __FILESYS_H

fsptr findfs(dev_t devno);
fsptr getfs(dev_t devno);
int wr_inode(inoptr ino);
void i_ref(inoptr ino);
void i_deref(inoptr ino);
void i_free(dev_t devno, ino_t ino);
inoptr _namei(uchar *name, uchar **rest, inoptr strt, inoptr *parent);
inoptr namei(char *uname, inoptr *parent, uchar follow);
inoptr srch_dir(inoptr wd, char *name);
inoptr srch_mt(inoptr ino);
inoptr i_open(dev_t devno, ino_t ino);
int ch_link(inoptr wd, char *oldname, char *newname, inoptr nindex);
void filename(char *upath, char *name); /* char *filename(char *path); */
int namecomp(uchar *n1, uchar *n2);
inoptr newfile(inoptr pino, char *name);
int doclose(uchar fd);
ino_t i_alloc(dev_t devno);
blkno_t blk_alloc(dev_t devno, uchar dirty); /* Nick dirty */
void blk_free(dev_t devno, blkno_t blk);
uchar oft_alloc(void);
void oft_deref(uchar of);
uchar uf_alloc(void);
uchar isdevice(inoptr ino);
void freeblk(dev_t dev, blkno_t blk, uchar level);
void f_trunc(inoptr ino);
blkno_t bmap(inoptr ip, blkno_t bn, uchar rdflg);
void validblk(dev_t devno, blkno_t num);
inoptr getinode(uchar uindex);
int getperm(inoptr ino);
void setftim(inoptr ino, uchar flag);
int fmount(dev_t dev, inoptr ino, bool_t roflag);
void magic(inoptr ino, char *who);
void i_sync(void);
void fs_sync(void);

#endif /* __FILESYS_H */

