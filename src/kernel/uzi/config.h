/**************************************************
UZI (Unix Z80 Implementation) Kernel:  config.h
***************************************************/
/* ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! */
/* ! ! !  Remake devio.c when this file is changed ! ! ! */
/* ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! */

#ifdef DEVIO

#if 1 /* Nick UZIX compatible */
devsw_t dev_tab[] =  /* The device driver switch table */
{
/* minors init open   close      read      write       ioctl         */
/*-------------------------------------------------------------------*/
  {  2, ok, wd_open,  ok,        wd_read,  wd_write,   nogood_ioctl }, /* 0 hdX */
  {  1, ok, ok,       ok,        ok_rdwr,  null_write, nogood_ioctl }, /* 1 null */
  {  1, ok, ok,       ok,        ok_rdwr,  null_write, nogood_ioctl }, /* 2 zero */
  {  1, ok, ok,       ok,        mem_read, mem_write,  nogood_ioctl }, /* 3 kmem */
  { 10, ok, tty_open, tty_close, tty_read, tty_write,  tty_ioctl    }  /* 4 ttyX */
};
#define DEV_TAB (sizeof(dev_tab)/sizeof(dev_tab[0]))
#else
extern wd_open(), wd_read(), wd_write();
extern fd_open(), fd_read(), fd_write();
extern tty_open(), tty_close(), tty_read(), tty_write(), tty_ioctl();
extern lpr_open(), lpr_close(), lpr_write();
extern mem_read(), mem_write();
extern mt_read(), mt_write(), mt_open(), mt_close();
extern null_write();

/*static*/ struct devsw dev_tab[] =  /* The device driver switch table */
{
  /* minor  open     close     read      write      ioctl */
   /*-----------------------------------------------------*/
    { 0,  wd_open,     ok,    wd_read,  wd_write,   nogood },   /* 0 HD0 */
    { 1,  wd_open,     ok,    wd_read,  wd_write,   nogood },   /* 1 HD1 */
#if 1 /* def UTIL */
    { 0,     ok,       ok,      ok,     null_write, nogood },   /* 2 Swap */
    { 0,     ok,       ok,      ok,     null_write, nogood },   /* 3 floppy */
    { 0,     ok,       ok,      ok,     null_write, nogood },   /* 4 printer */
    { 0,     ok,       ok,      ok,     null_write, nogood },   /* 5 /dev/mt */
#else
    { 2,  wd_open,     ok,    wd_read,  wd_write,   nogood },   /* 2 Swap */
    { 0,  fd_open,     ok,    fd_read,  fd_write,   nogood },   /* 3 floppy */
    { 0, lpr_open, lpr_close, nogood,   lpr_write,  nogood },   /* 4 printer */
    { 0,  mt_open,  mt_close, mt_read,  mt_write,   nogood },   /* 5 /dev/mt */
#endif
    { 0,     ok,       ok,      ok,     null_write, nogood },   /* 6 /dev/null */
    { 0,     ok,       ok,    mem_read, mem_write,  nogood },   /* 7 /dev/kmem */
    { 0, tty_open, tty_close, tty_read, tty_write, tty_ioctl }, /* 8 tty */
    { 1, tty_open, tty_close, tty_read, tty_write, tty_ioctl }, /* 9 tty0 */
    { 2, tty_open, tty_close, tty_read, tty_write, tty_ioctl }, /* 10 tty1 */
    { 3, tty_open, tty_close, tty_read, tty_write, tty_ioctl }, /* 11 tty2 */
    { 4, tty_open, tty_close, tty_read, tty_write, tty_ioctl }, /* 12 tty3 */
    { 5, tty_open, tty_close, tty_read, tty_write, tty_ioctl }, /* 13 scale */
    { 6, tty_open, tty_close, tty_read, tty_write, tty_ioctl }, /* 14 lcd0 */
    { 7, tty_open, tty_close, tty_read, tty_write, tty_ioctl }, /* 15 lcd1 */
    { 0,     ok,       ok,      ok,     null_write, nogood },   /* 16 unused */
    { 9, tty_open, tty_close, tty_read, tty_write, tty_ioctl }  /* 17 lpr0 */
     /* Add more tty channels here if available, incrementing minor# */
};
#endif

#else /* Nick */

extern devsw_t dev_tab[]; /* Nick */

#endif

#define BOOT_TTY  0x402 /*10*/ /*9*/  /* Set this to default device for stdio, stderr */
                       /* In this case, the default is the first TTY device */

#define NDEVS   3         /* Devices 0..NDEVS-1 are capable of being mounted */
                          /*   (add new mountable devices to beginning area. */
#if 1 /* Nick UZIX compatible */
#define BLK_DEVS 1	/* # of block (mountable) devices in table */
#define NFS	NDEVS	/* max # of mounted filesystems */
#endif

#define TTYDEV  BOOT_TTY  /* Device used by kernel for messages, panics */
#define SWAPDEV 3         /* Device for swapping. */
#define NBUFS   10 /* DEBUGGING 20 */ /*10*/        /* Number of block buffers */

#if 1 /* Nick UZIX compatibility */

#define HOST_MACHINE	"WPO"	/* World Post Office HYT1000 / HYT1500 */
#define VERSION		"1.0"	/* UZIX version */
#define RELEASE		"0"	/* UZIX release */

#endif

