/* scall3.h for UZI180 by Nick (we are converting everything to ANSI C) */

#ifndef __SCALL3_H
#define __SCALL3_H

#if 1 /* Nick please look into this */
int sys_execve(void);
int wargs(char **argv, struct s_argblk *argbuf);
char *rargs(char *ptr, struct s_argblk *argbuf, int *cnt);
#else
uint ssiz(char **argv, uchar *cnt, char **smin);
void *scop(char **argv, uchar cnt, char *to, char *real);
int repack(char **argp, char **envp, uchar check);
void exec2(void);
int sys_execve(void);
#endif
#if 0 /* def UZIX_MODULE */
int sys_reg(void);
int sys_dereg(void);
int sys_callmodu(void);
int sys_modreply(void);
int sys_repfmodu(void);
#endif

#endif /* __SCALL3_H */

