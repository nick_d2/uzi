/* vendor.h for uzi180 by Nick - essential defines for the IAR compiler */

#ifndef UTIL /* ensures we don't do anything when compiling utils under MSVC */

#define _putc __putc
#define abyte _abyte
#define acrlf _acrlf
#define ahexw _ahexw
#define amess _amess
#define bcopy _bcopy
#define busid _busid
#define bzero _bzero
#define calltrap _calltrap
#define chksigs _chksigs
#define clkint2 _clkint2
#define cmdblk _cmdblk
#define di _di
#define di_absolute _di_absolute
#define doexec _doexec
#define dofork _dofork
#define dprintf _dprintf
#define dptr _dptr
#define dTbl _dTbl
#define ei _ei
#define ei_absolute _ei_absolute
#define fbuf _fbuf
#define fdInit _fdInit
#define fdread0 _fdread0
#define fdwrite0 _fdwrite0
#define ferror _ferror
#define fs_init _fs_init
#define fsector _fsector
#define ftrack _ftrack
#define getproc _getproc
#define hd_offset _hd_offset
#define hdhds _hdhds
#define hdspt _hdspt
#define inint _inint
#define initsys _initsys
#define kprintf _kprintf
#define lpout _lpout
#define newproc _newproc
#define osBank _osBank
#define panic _panic
#define ptab_alloc _ptab_alloc
/* #define rdtod _rdtod */
#define rom_serial_no _rom_serial_no
#define runticks _runticks
#define scsiop _scsiop
#define swapin _swapin
#define swapout _swapout
#define tempstack _tempstack
#define tod _tod
#define tty_inproc _tty_inproc
#define ub _ub
#define uget _uget
#define ugetc _ugetc
#define ugets _ugets
#define ugetw _ugetw
#define unix_locked_out _unix_locked_out
#define unix2 _unix2
#define uput _uput
#define uputc _uputc
#define uputw _uputw

#if 1 /* Nick, see kprintf.c */
int kprintf(char *, ...); /* ensures IAR won't try to pass register params */
int dprintf(char debuglevel, char *fmt, ...);
#else
void kprintf(char *, ...); /* ensures IAR won't try to pass register params */
#endif

/* diag.h for uzi180 by Nick - essential defines for the IAR compiler */

#pragma function = non_banked

/* void exit(int exitcode); */
/* void abort(int exitcode); */

void abyte(char data);
void acrlf(void);
void ahexw(int data);
void amess(char *ptr);
void tempstack(void); /* for machasm.s01 */

#pragma function = default

#endif /* UTIL */

