/* systrace.c by Nick for UZI180 */

#include "vendor.h" /* Nick, must come first */
#include <unix.h>
#include <config.h>
#include <extern.h>
#include "kprintf.h"

void kputchar(char c);
#if 0 /* Nick, see kprintf.c */
void kprintf(char *fmt, ...);
#endif
char ugetc(char *ptr);

void systrace_entry(void);
void systrace_exit(void);
void systrace_dump(unsigned int value, unsigned int count, unsigned char type);

#define TYPE_BIN 1
#define TYPE_STR 2
#define TYPE_OCT 3
#define TYPE_DEC 4
#define TYPE_HEX 5

struct systrace_entry
	{
	char return_type;
	char *function_name;
	char argument_type[4];
	};

struct systrace_entry systrace_table[] =
	{
#if 1 /* Nick UZIX compatibility */
	{ TYPE_DEC,	"access",	{ TYPE_STR, TYPE_OCT, 0, 0 } },
	{ TYPE_DEC,	"alarm",	{ TYPE_DEC, 0, 0, 0 } },
	{ TYPE_HEX,	"brk",		{ TYPE_HEX, 0, 0, 0 } },
	{ TYPE_DEC,	"chdir",	{ TYPE_STR, 0, 0, 0 } },
	{ TYPE_DEC,	"chmod",	{ TYPE_STR, TYPE_OCT, 0, 0 } },
	{ TYPE_DEC,	"chown",	{ TYPE_STR, TYPE_DEC, TYPE_DEC, 0 } },
	{ TYPE_DEC,	"close",	{ TYPE_DEC, 0, 0, 0 } },
	{ TYPE_DEC,	"getset",	{ TYPE_HEX, TYPE_HEX, TYPE_HEX, 0 } }, /*	{ TYPE_DEC,	"creat",	{ TYPE_STR, TYPE_HEX, 0, 0 } }, */
	{ TYPE_DEC,	"dup",		{ TYPE_DEC, 0, 0, 0 } },
	{ TYPE_DEC,	"dup2",		{ TYPE_DEC, TYPE_DEC, 0, 0 } },
	{ TYPE_DEC,	"execve",	{ TYPE_STR, TYPE_HEX, TYPE_HEX, 0 } },
	{ TYPE_DEC,	"_exit",	{ TYPE_DEC, 0, 0, 0 } },
	{ TYPE_DEC,	"fork",		{ 0, 0, 0, 0 } },
	{ TYPE_DEC,	"fstat",	{ TYPE_DEC, TYPE_HEX, 0, 0 } },
	{ TYPE_DEC,	"getfsys",	{ TYPE_HEX, TYPE_HEX, 0, 0 } },
	{ TYPE_HEX,	"ioctl",	{ TYPE_DEC, TYPE_HEX, TYPE_HEX, 0 } },
	{ TYPE_DEC,	"kill",		{ TYPE_DEC, TYPE_DEC, 0, 0 } },
	{ TYPE_DEC,	"link",		{ TYPE_STR, TYPE_STR, 0, 0 } },
	{ TYPE_DEC,	"mknod",	{ TYPE_STR, TYPE_OCT, 0, 0 } },
	{ TYPE_DEC,	"mount",	{ TYPE_STR, TYPE_STR, 0, 0 } },
	{ TYPE_DEC,	"open",		{ TYPE_STR, TYPE_HEX, TYPE_OCT, 0 } },
	{ TYPE_DEC,	"pause",	{ 0, 0, 0, 0 } },
	{ TYPE_DEC,	"pipe",		{ TYPE_HEX, 0, 0, 0 } },
	{ TYPE_HEX,	"read",		{ TYPE_DEC, TYPE_HEX, TYPE_HEX, 0 } },
	{ TYPE_HEX,	"sbrk",		{ TYPE_HEX, 0, 0, 0 } },
	{ TYPE_HEX,	"lseek",	{ TYPE_DEC, TYPE_HEX, TYPE_HEX, TYPE_DEC } },
	{ TYPE_DEC,	"signal",	{ TYPE_DEC, TYPE_HEX, TYPE_HEX, 0 } },
	{ TYPE_DEC,	"stat",		{ TYPE_STR, TYPE_HEX, 0, 0 } },
	{ TYPE_DEC,	"stime",	{ TYPE_HEX, 0, 0, 0 } },
	{ TYPE_DEC,	"sync",		{ 0, 0, 0, 0 } },
	{ TYPE_DEC,	"time",		{ TYPE_HEX, 0, 0, 0 } },
	{ TYPE_DEC,	"times",	{ TYPE_HEX, 0, 0, 0 } },
	{ TYPE_DEC,	"umount",	{ TYPE_STR, 0, 0, 0 } },
	{ TYPE_DEC,	"unlink",	{ TYPE_STR, 0, 0, 0 } },
	{ TYPE_DEC,	"utime",	{ TYPE_STR, TYPE_HEX, 0, 0 } },
	{ TYPE_DEC,	"waitpid",	{ TYPE_DEC, TYPE_HEX, TYPE_HEX, 0 } },
	{ TYPE_HEX,	"write",	{ TYPE_DEC, TYPE_BIN, TYPE_HEX, 0 } },
	{ TYPE_DEC,	"reboot",	{ TYPE_HEX, TYPE_HEX, 0, 0 } },
	{ TYPE_DEC,	"symlink",	{ TYPE_STR, TYPE_STR, 0, 0 } },
	{ TYPE_DEC,	"chroot",	{ TYPE_STR, 0, 0, 0 } },
	{ TYPE_DEC,	"falign",	{ TYPE_DEC, TYPE_DEC, 0, 0 } }
#else
	{ TYPE_DEC,	"_exit",	{ TYPE_DEC, 0, 0, 0 } },
	{ TYPE_DEC,	"open",		{ TYPE_STR, TYPE_HEX, 0, 0 } },
	{ TYPE_DEC,	"close",	{ TYPE_DEC, 0, 0, 0 } },
	{ TYPE_DEC,	"creat",	{ TYPE_STR, TYPE_HEX, 0, 0 } },
	{ TYPE_DEC,	"mknod",	{ TYPE_STR, TYPE_OCT, 0, 0 } },
	{ TYPE_DEC,	"link",		{ TYPE_STR, TYPE_STR, 0, 0 } },
	{ TYPE_DEC,	"unlink",	{ TYPE_STR, 0, 0, 0 } },
	{ TYPE_HEX,	"read",		{ TYPE_DEC, TYPE_HEX, TYPE_HEX, 0 } },
	{ TYPE_HEX,	"write",	{ TYPE_DEC, TYPE_BIN, TYPE_HEX, 0 } },
	{ TYPE_HEX,	"seek",		{ TYPE_DEC, TYPE_HEX, TYPE_DEC, 0 } },
	{ TYPE_DEC,	"chdir",	{ TYPE_STR, 0, 0, 0 } },
	{ TYPE_DEC,	"sync",		{ 0, 0, 0, 0 } },
	{ TYPE_DEC,	"access",	{ TYPE_STR, 0, 0, 0 } },
	{ TYPE_DEC,	"chmod",	{ TYPE_STR, TYPE_OCT, 0, 0 } },
	{ TYPE_DEC,	"chown",	{ TYPE_STR, TYPE_DEC, TYPE_DEC, 0 } },
	{ TYPE_DEC,	"stat",		{ TYPE_STR, TYPE_HEX, 0, 0 } },
	{ TYPE_DEC,	"fstat",	{ TYPE_DEC, TYPE_HEX, 0, 0 } },
	{ TYPE_DEC,	"dup",		{ TYPE_DEC, 0, 0, 0 } },
	{ TYPE_DEC,	"getpid",	{ 0, 0, 0, 0 } },
	{ TYPE_DEC,	"getppid",	{ 0, 0, 0, 0 } },
	{ TYPE_DEC,	"getuid",	{ 0, 0, 0, 0 } },
	{ TYPE_DEC,	"umask",	{ TYPE_OCT, 0, 0, 0 } },
	{ TYPE_DEC,	"getfsys",	{ TYPE_HEX, TYPE_HEX, 0, 0 } },
	{ TYPE_DEC,	"execve",	{ TYPE_STR, TYPE_HEX, TYPE_HEX, 0 } },
	{ TYPE_DEC,	"wait",		{ 0, 0, 0, 0 } },
	{ TYPE_DEC,	"setuid",	{ TYPE_DEC, 0, 0, 0 } },
	{ TYPE_DEC,	"setgid",	{ TYPE_DEC, 0, 0, 0 } },
	{ TYPE_DEC,	"time",		{ TYPE_HEX, 0, 0, 0 } },
	{ TYPE_DEC,	"stime",	{ TYPE_HEX, 0, 0, 0 } },
	{ TYPE_HEX,	"ioctl",	{ TYPE_DEC, TYPE_HEX, 0, 0 } },
	{ TYPE_HEX,	"brk",		{ TYPE_HEX, 0, 0, 0 } },
	{ TYPE_HEX,	"sbrk",		{ TYPE_HEX, 0, 0, 0 } },
	{ TYPE_DEC,	"fork",		{ 0, 0, 0, 0 } },
	{ TYPE_DEC,	"mount",	{ TYPE_STR, TYPE_STR, 0, 0 } },
	{ TYPE_DEC,	"umount",	{ TYPE_STR, 0, 0, 0 } },
	{ TYPE_DEC,	"signal",	{ TYPE_DEC, TYPE_HEX, 0, 0 } },
	{ TYPE_DEC,	"dup2",		{ TYPE_DEC, TYPE_DEC, 0, 0 } },
	{ TYPE_DEC,	"pause",	{ 0, 0, 0, 0 } },
	{ TYPE_DEC,	"alarm",	{ TYPE_DEC, 0, 0, 0 } },
	{ TYPE_DEC,	"kill",		{ TYPE_DEC, TYPE_DEC, 0, 0 } },
	{ TYPE_DEC,	"pipe",		{ TYPE_HEX, 0, 0, 0 } },
	{ TYPE_DEC,	"getgid",	{ 0, 0, 0, 0 } },
	{ TYPE_DEC,	"times",	{ TYPE_HEX, 0, 0, 0 } },
	{ TYPE_DEC,	"utime",	{ TYPE_HEX, 0, 0, 0 } },
	{ TYPE_DEC,	"reboot",	{ TYPE_HEX, TYPE_HEX, 0, 0 } },
	{ TYPE_DEC,	"mkfifo",	{ TYPE_STR, TYPE_OCT, 0, 0 } }
#endif
	};

#define SYSTRACE_LIMIT (sizeof(systrace_table) / sizeof(struct systrace_entry))

char *syserror_table[] =
	{
	"",
	"EPERM",
	"ENOENT",
	"ESRCH",
	"EINTR",
	"EIO",
	"ENXIO",
	"E2BIG",
	"ENOEXEC",
	"EBADF",
	"ECHILD",
	"EAGAIN",
	"ENOMEM",
	"EACCES",
	"EFAULT",
	"ENOTBLK",
	"EBUSY",
	"EEXIST",
	"EXDEV",
	"ENODEV",
	"ENOTDIR",
	"EISDIR",
	"EINVAL",
	"ENFILE",
	"EMFILE",
	"ENOTTY",
	"ETXTBSY",
	"EFBIG",
	"ENOSPC",
	"ESPIPE",
	"EROFS",
	"EMLINK",
	"EPIPE",
	"EDOM",
	"ERANGE",
	"EDEADLK",
	"ENAMETOOLONG",
	"ENOLCK",
	"EINVFNC",
	"ENOTEMPTY",
	"ELOOP",
	"ESHELL"
	};

#define SYSERROR_LIMIT (sizeof(syserror_table) / sizeof(char *))

void systrace_entry(void)
	{
	int *u_argn_ptr;
	register unsigned char i;
	unsigned int value;
	unsigned char type;

/* abyte('{'); */
/* ahexw(*(int *)(0xc931 + 0x9a)); */

/* kprintf("word at 0x83f8 = 0x%x\n", ugetw(0x83f8)); */

        kprintf ("\tpid %d, call %d",
		 udata.u_ptab->p_pid, udata.u_callno);

	if (udata.u_callno < 0 || udata.u_callno >= SYSTRACE_LIMIT)
		{
		panic("can't trace invalid syscall index");
		}

	kprintf (", %s (", systrace_table[udata.u_callno].function_name);

	u_argn_ptr = &udata.u_argn0;
	for (i = 0; i < 4; i++)
		{
		type = systrace_table[udata.u_callno].argument_type[i];
		if (type == 0)
			{
			break;
			}

		if (i)
			{
			kputchar(',');
			kputchar(' ');
			}

		value = *u_argn_ptr++;
		systrace_dump(value, udata.u_argn2, type);
		}

	kputchar(')');
	kputchar('\n');
	}

void systrace_exit(void)
	{
	register unsigned char type;

/* abyte('}'); */
/* ahexw(*(int *)(0xc931 + 0x9a)); */

        kprintf ("\t\tpid %d, call %d, ret ",
		 udata.u_ptab->p_pid, udata.u_callno);

	if (udata.u_callno == 23 /*7*/ && udata.u_error == 0) /* read() */
		{
		systrace_dump(udata.u_argn1, udata.u_retval, TYPE_BIN);
		kputchar(',');
		kputchar(' ');
		}

	type = systrace_table[udata.u_callno].return_type;
	systrace_dump(udata.u_retval, 0, type);

	if ((udata.u_callno == 25 || udata.u_callno == 26) &&
			udata.u_error == 0) /* lseek() or signal() */
		{
		kputchar(',');
		kputchar(' ');
		systrace_dump(udata.u_retval1, 0, type);
		}

	kprintf(", err %d", udata.u_error);
	if (udata.u_error > 0 && udata.u_error <= SYSERROR_LIMIT)
		{
		kprintf(" (%s)", syserror_table[udata.u_error]);
		}

	kputchar('\n');
/* kprintf("word at 0x83f8 = 0x%x\n", ugetw(0x83f8)); */
	}

void systrace_dump(unsigned int value, unsigned int count, unsigned char type)
	{
	register unsigned char j, c;
	unsigned char max, flag, *data;

	max = 32;
	flag = 1;

	switch (type)
		{

	case TYPE_BIN:
		max = 8;
		if (count <= 8)
			{
			max = count; /* size of buffer at (char *)value */
			flag = 0; /* whether to show ellipsis */
			}
		/* fall thru */

	case TYPE_STR:
		kputchar('"');

		data = (unsigned char *)value;
		for (j = 0; j < max; j++)
			{
			c = ugetc(data++);
			if (c == 0 && max > 16)
				{
				flag = 0; /* whether to show ellipsis */
				break;
				}
			if (c < 0x20 || c >= 0x80)
				{
				kputchar('\\');
				switch (c)
					{
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
					kputchar('0' + c);
					break;
				case 9:
					kputchar('t');
					break;
				case 0xa:
					kputchar('n');
					break;
				case 0xd:
					kputchar('r');
					break;
				default:
					kprintf("x%x", (int)c);
					break;
					}
				}
			else
				{
				if (c == '\\' || c == '"')
					{
					kputchar('\\');
					}
				kputchar(c);
				}
			}

		if (flag)
			{
			kputchar('.');
			kputchar('.');
			kputchar('.');
			}

		kputchar('"');
		break;

	case TYPE_OCT:
		if (value < 8)
			{
			kputchar('0' + value);
			}
		else
			{
			kprintf("0%o", value);
			}
		break;

	case TYPE_DEC:
		kprintf("%d", value);
		break;

	case TYPE_HEX:
		if (value < 10)
			{
			kputchar('0' + value);
			}
		else
			{
			kprintf("0x%x", value);
			}
		break;
		}
	}

