/***************************************************************
   UZI (Unix Z80 Implementation) Kernel:  process.c
----------------------------------------------------------------
 Adapted from UZI By Doug Braun, and UZI280 by Stefan Nitschke
            Copyright (C) 1998 by Harold F. Bower
       Portions Copyright (C) 1995 by Stefan Nitschke
****************************************************************/
/* Revisions:
 */

#if 0 /* Nick */
#undef DEBUG                   /* Define to activate Debug Code */
#endif

/* #include "io64180.h" /* silly silly */
#include "vendor.h" /* Nick, must come first */
#include <unix.h>
#include <config.h>
#include <extern.h>
#include "devio.h" /* prototypes added by Nick */
#include "devtty.h" /* prototypes added by Nick */
#include "filesys.h" /* prototypes added by Nick */

/* start of prototypes added by Nick */
void systrace_entry(void);
void systrace_exit(void);
int dowait(void);
/* end of prototypes added by Nick */

/* silly prototype added by Nick */
/* char silly_pause(void); */

#define NO_ASM      /* define this for pure c-coding */

extern char osBank;				/* defined in machasm */

#if 1 /* Nick UZIX compatible */
extern char UZIX[]; /* operating system name, defined in main.c */
#endif


init2()
{
    register char *j;
    static char bootline[2];
#if 1
    static char arg[] = { '/', 'b', 'i', 'n', '/', 'i', 'n', 'i', 't', 0, 0,
			  (int16)PROGBASE & 0xff, (int16)PROGBASE >> 8,
			  0, 0, 0, 0 };
#else
    static char arg[] = {'/','i','n','i','t',0,0,0x01,0x01,0,0,0,0};
#endif
    inoptr i_open();
    ptptr ptab_alloc();
/* Nick    int   d_open(),fmount(); */

#if 1 /* Nick, as udata is defined by defs statement, pad value 0xff */
    bzero (&udata, sizeof(udata_t));
#endif

    bufinit();
/* abyte('a'); */

       /* Create the context for the first process
        */
    newproc (udata.u_ptab = initproc = ptab_alloc());
/* ahexw(udata.u_page); */
/* abyte('b'); */
    initproc->p_status = P_RUNNING;
    initproc->p_fork_inf = udata.u_page;       /* set for use in execve */

       /* User's file table
        */
    for (j=udata.u_files; j < (udata.u_files+UFTSIZE); ++j)
        *j = -1; /* please see freefileentry() macro in unix.h */

/* abyte('c'); */
    ei_absolute(); /* Nick */
/* abyte('d'); */

#if 0 /* Nick */
       /* Set Time-Of-Day absolutely, instead of waiting for interrupt */
    rdtod();
#endif
/* abyte('e'); */

       /* Open the console tty device thereby establishing the init
        * process' controlling tty
        */
    if (d_open (TTYDEV) != 0)
        panic ("no tty");
#if 1 /* Nick */
    tty_data[MINOR(TTYDEV)].t_flags = XTABS | CRMOD | ECHO | COOKED;
#else
    tty_data[dev_tab[TTYDEV].minor].t_flags = XTABS | CRMOD | ECHO | COOKED;
#endif
/* abyte('f'); */

#if 1 /* Nick UZIX compatibility */
    kprintf("Welcome to Hytech %s version %s release %s\n\n",
	    UZIX, VERSION, RELEASE);
#else
    kprintf ("UZI180 (c) Copyright (1998) by H.F.Bower, D.Braun, S.Nitschke\n");
#endif
    if (root_dev == (dev_t)-1)
	{
#if 1 /* Nick */
        kprintf("root device? ");
#else
        kprintf ("boot: ");
#endif
        udata.u_base = bootline;
        udata.u_sysio = 1;
        udata.u_count = 2;
        udata.u_euid = 0;		/* Always begin as superuser */

        cdread (TTYDEV);
#if 1 /* Nick UZIX compatible */
        root_dev = *bootline - '0';
#else
        ROOTDEV = *bootline - '0';
#endif
	}

#if 1 /* Nick UZIX compatible */
    if (root_dev < 0 || root_dev >= NFS) /* number of mountable devs */
        {
        root_dev = 0;
        }
#else
    if (ROOTDEV < 0 || ROOTDEV > 1)
        {
        ROOTDEV = 0;
        }
#endif

       /* Mount the root device */
/* abyte('g'); */
#if 1 /* Nick UZIX compatible */
    if (fmount (root_dev, NULLINODE, 0))
#else
    if (fmount (ROOTDEV, NULLINODE))
#endif
        panic ("no filesys");
/* abyte('h'); */
#if 1 /* Nick UZIX compatible */
    if ((root_ino = i_open (root_dev, ROOTINODE)) == NULL)
#else
    ifnot (root = i_open (ROOTDEV, ROOTINODE))
#endif
        panic ("no root");
/* abyte('i'); */

#if 1 /* Nick UZIX compatible */
    udata.u_root = root_ino; /* already referenced by i_open */
    i_ref (udata.u_cwd = root_ino);
#else
    i_ref (udata.u_cwd = root);
#endif
/* abyte('j'); */

    rdtime (&udata.u_time);
/* abyte('k'); */

       /* Poke the execve arguments into user data space the UZI280 way */
    uput (arg, PROGBASE, sizeof (arg));
/* abyte('l'); */

#if 1
    udata.u_argn0  = (int16)PROGBASE;
    udata.u_argn1  = (int16)PROGBASE + 7 + 4;	/* Arguments (just "/init") */
    udata.u_argn2  = (int16)PROGBASE + 0xb + 4;	/* Environment (none) */
#else
    udata.u_argn   = (int16)PROGBASE;
    udata.u_argn1  = 0x107;       /* Arguments (just "/init") */
    udata.u_argn2  = 0x10b;       /* Environment (none) */
#endif

    sys_execve();

    panic ("no /bin/init, errno %d", udata.u_error); /* BIG Trouble */
}



/* psleep() puts a process to sleep on the given event.  If another
 * process is runnable, it swaps out the current one and starts the
 * new one.  Normally when psleep is called, the interrupts have
 * already been disabled.   An event of 0 means a pause(), while an
 * event equal to the process's own ptab address is a wait().
 */

psleep (event)
char *event;
{
    di_absolute(); /* Nick */
    if (udata.u_ptab->p_status != P_RUNNING)
        panic ("psleep: voodoo");
    if (!event)
        udata.u_ptab->p_status = P_PAUSE;
    else if (event == (char *)udata.u_ptab)
        udata.u_ptab->p_status = P_WAIT;
    else
        udata.u_ptab->p_status = P_SLEEP;

    udata.u_ptab->p_wait = event;
    udata.u_ptab->p_waitno = ++waitno;

    ei_absolute(); /* Nick */
    swapout();          /* Swap us out, and start another process */
            /* Swapout doesn't return until we have been swapped back in */
}



/* wakeup() looks for any process waiting on the event,
 * and makes it runnable
 */

wakeup (event)
char *event;
{
    register ptptr p;

    di_absolute(); /* Nick */
    for (p=ptab; p < ptab+PTABSIZE; ++p)
    {
        if (p->p_status > P_RUNNING && p->p_wait == event)
        {
            p->p_status = P_READY;
            p->p_wait = (char *)NULL;
        }
    }
    ei_absolute(); /* Nick */
}



/* Getproc returns the process table pointer of a runnable process.
 * It is actually the scheduler.  If there are none, it loops.
 * This is the only time-wasting loop in the system.
 */

ptptr
getproc()
{
    register status;
    static  ptptr pp = ptab;    /* Pointer for round-robin scheduling */

    for (;;)
    {
        if (++pp >= ptab + PTABSIZE)
            pp = ptab;
        di_absolute(); /* Nick */
        status = pp->p_status;
#ifdef NO_ASM
	ei_absolute();	/* Interrupts ON absolutely!  (instead of "ei()") */
#else
_asm
	ei		; Interrupts ON absolutely!  (instead of "ei()")
_endasm
#endif

        if (status == P_RUNNING)
            panic ("getproc: extra running");
        if (status == P_READY)
            return (pp);
    }
}



/* Newproc fixes up the tables for the child of a fork
 */
newproc (p)           /* Passed New process table entry */
ptptr p;
{
    register char *j;

/* abyte('['); */
/* ahexw(*(int *)(0xc931 + 0x9a)); */

    /* Note that ptab_alloc clears most of the entry */
    di_absolute(); /* Nick */
    /* p_swap is replaced by p_page for UZI180 Banked memory access.  It is
     * the BBR value for the base of the process area in memory (Byte value).
     * The same value is also set in udata.u_page for use by put/get routines.
     */
#if 1
    udata.u_page = p->p_page = osBank + (0x10-7) + (p-ptab) * 9;
#else
    udata.u_page = p->p_page = (p-ptab) * 0x10 + 0x10 + osBank;
#endif
    p->p_status = P_RUNNING;

    p->p_pptr = udata.u_ptab;
    p->p_ignored = udata.u_ptab->p_ignored;
    p->p_tty - udata.u_ptab->p_tty;
    ifnot (p->p_tty)                /* If no tty, try tty of parent's parent */
        p->p_tty = udata.u_ptab->p_pptr->p_tty;
    p->p_uid = udata.u_ptab->p_uid;
    udata.u_ptab = p;

    bzero (&udata.u_utime, 4 * sizeof (time_t)); /* Clear tick counters */

    rdtime (&udata.u_time);
#if 1 /* Nick UZIX compatible */
    if (udata.u_root)
        i_ref (udata.u_root);
#endif
    if (udata.u_cwd)
        i_ref (udata.u_cwd);
    udata.u_cursig = udata.u_error = 0;

#if 1 /* Nick UZIX compatible */
    /* Set default priority */
    p->p_cprio = MAXTICKS;
#else
    /* Set default priority */
    p->p_priority = MAXTICKS;
#endif

/* abyte('\\'); */
/* ahexw(*(int *)(0xc931 + 0x9a)); */
/* di_absolute(); */

    for (j=udata.u_files; j < (udata.u_files+UFTSIZE); ++j)
#if 1 /* Nick, to sidestep signed vs unsigned char issues */
	if (freefileentry(*j) == 0)
#else
        if (*j >= 0)
#endif
           ++of_tab[*j].o_refs;
    ei_absolute(); /* Nick */

/* abyte(']'); */
/* ahexw(*(int *)(0xc931 + 0x9a)); */

}



/* This allocates a new process table slot, and fills in its
 * p_pid field with a unique number.
 */
ptptr
ptab_alloc()
{
    register ptptr p;
    register ptptr pp;
    static int nextpid = 0;

/* abyte('('); */
/* ahexw(*(int *)(0xc931 + 0x9a)); */

    di_absolute(); /* Nick */
    for (p=ptab; p < ptab+PTABSIZE; ++p)
    {
        if (p->p_status == P_EMPTY)
            goto found;
    }
    ei_absolute(); /* Nick */

#if DEBUG >= 2 /* Nick */
    dprintf(2, "process table full\n");
#endif

/* abyte('*'); */
/* ahexw(*(int *)(0xc931 + 0x9a)); */

    return (NULL);

found:
       /* See if next pid number is unique */
nogood:
    if (nextpid++ > 32000)
        nextpid = 1;
    for (pp=ptab; pp < ptab+PTABSIZE; ++pp)
    {
        if (pp->p_status != P_EMPTY && pp->p_pid == nextpid)
            goto nogood;
    }

#if 1 /* Nick UZIX compatible */
    bzero (p, sizeof (struct s_ptab));
#else
    bzero (p, sizeof (struct p_tab));
#endif
    p->p_pid = nextpid;
    p->p_status = P_FORKING;

    ei_absolute(); /* Nick */

#if DEBUG >= 2 /* Nick */
    dprintf(2, "process %d starting\n", p->p_pid);
#endif

/* abyte(')'); */
/* ahexw(*(int *)(0xc931 + 0x9a)); */

    return (p);
}



/* This is the clock interrupt routine.   Its job is to increment the clock
 * counters, increment the tick count of the running process, and either
 * swap it out if it has been in long enough and is in user space or mark
 * it to be swapped out if in system space.  Also it decrements the alarm
 * clock of processes.
 * UZI180: This is CALLed from the actual Interrupting service
 *      routine (See MACHASM.ASZ).                             HFB
 * This must have no automatic or register variables.
 */
clkint2()
{
    static ptptr p;

    inint = 1;                    /* We arrived from H/W Interrupt */

       /* Increment processes and global tick counters */
    if (udata.u_ptab->p_status == P_RUNNING)
        incrtick (udata.u_insys ? &udata.u_stime : &udata.u_utime);

    incrtick (&ticks);

       /* Do once-per-second things */
    if (++sec == TICKSPERSEC)
    {
           /* Update global time counters */
        sec = 0;
#if 0 /* Nick */
        rdtod();          /* Update time-of-day */
#endif

           /* Update process alarm clocks */
        for (p=ptab; p < ptab+PTABSIZE; ++p)
        {
            if (p->p_alarm)
                ifnot (--p->p_alarm)
                    ssig (p, SIGALRM);
        }
    }
       /* Check run time of current process */
#if 1 /* Nick UZIX compatible */
    if ((++runticks >= udata.u_ptab->p_cprio)
#else
    if ((++runticks >= udata.u_ptab->p_priority)
#endif
               &&  !udata.u_insys  &&  (inint == 1))     /* Time to swap out */
    {
        di_absolute(); /* Nick, formerly just di; must have been a mistake */
        udata.u_insys = 1;
        inint = 0;
        udata.u_ptab->p_status = P_READY;
        swapout();
        udata.u_insys = 0;               /* We have swapped back in */
    }
    inint = 0;
}



extern int (*disp_tab[])();     /* in data.c */
extern int ncalls;

/* Unix System Call Trap Routine secondary processing.  The primary
 * Interrupt trap is in PROCASM.ASZ which manages state saving, then
 * vectors here for final activity.
 * No auto vars here, so flags will be preserved.
 */
unix2()
{
/* abyte('&'); */
/* ahexw((int16)osBank); */
/* abyte(' '); */
/* ahexw((int16)CBR); */
/* abyte(' '); */
/* ahexw((int16)udata.u_ptab); */
/* abyte(' '); */
/* ahexw(udata.u_ptab->p_pending); */
    udata.u_insys = 1;
    udata.u_error = 0;

       /* UZI180 saves the Stack Pointer and arguments in the
        * Assembly Language Function handler in MACHASM.ASZ
        */
    if (udata.u_callno >= ncalls)
        udata.u_error = EINVAL;
/* abyte('0'); */
    ei_absolute(); /* Nick */
/* abyte('1'); */

#if DEBUG && !defined(UTIL)
    if (traceon)
        {
#if 1
	systrace_entry();
#else
        kprintf ("\t\tcall %d (%x, %x, %x, %x)\n", udata.u_callno, /* Nick */
                 udata.u_argn, udata.u_argn1, udata.u_argn2, udata.u_argn3);
#endif
/* silly_pause(); */
        }
#endif
/* abyte('2'); */

       /* Branch to correct routine */
    ifnot (udata.u_error)           /* Could be set by ugetw() */
        udata.u_retval = (*disp_tab[udata.u_callno])();
/* abyte('3'); */

#if DEBUG && !defined(UTIL)
    if (traceon)
        {
#if 1
	systrace_exit();
#else
        kprintf ("\t\t\tcall %d, ret %x err %d\n",
                 udata.u_callno, udata.u_retval, udata.u_error);
#endif
        }
#endif
/* abyte('4'); */

    chksigs();
/* abyte('5'); */

    di_absolute(); /* Nick */
#if 1 /* Nick UZIX compatible */
    if (runticks >= udata.u_ptab->p_cprio)       /* Time to swap out */
#else
    if (runticks >= udata.u_ptab->p_priority)       /* Time to swap out */
#endif
    {
        udata.u_ptab->p_status = P_READY;
        swapout();
    }
    ei_absolute(); /* Nick */
    udata.u_insys = 0;
/*===    calltrap();            /* Call Trap Routine if necessary */
/* abyte('*'); */

    if (udata.u_error)
        return (-1);

    return (udata.u_retval);
}



/* This sees if the current process has any signals set, and handles them.
 */
chksigs()
{
    register j;

    di_absolute(); /* Nick */
    ifnot (udata.u_ptab->p_pending)
    {
        ei_absolute(); /* Nick */
        return;
    }

    for (j=1; j < NSIGS; ++j)
    {
        ifnot (sigmask(j) & udata.u_ptab->p_pending)
            continue;
        if (udata.u_sigvec[j] == SIG_DFL)
        {
            ei_absolute(); /* Nick */
#if DEBUG >= 2
            dprintf(2, "process terminated by signal %d: ", j); /* Nick %d */
#endif
            doexit (0, j);
        }

        if (((long)udata.u_sigvec[j]) != SIG_IGN)
        {
               /* Arrange to call the user routine at return */
            udata.u_ptab->p_pending &= ~sigmask(j);
            udata.u_cursig = j;
        }
    }
    ei_absolute(); /* Nick */
}



sgrpsig (tty, sig)
int tty;
int16 sig;
{
    register ptptr p;

    for (p=ptab; p < ptab+PTABSIZE; ++p) {
        if (p->p_status && p->p_tty == tty)
            ssig (p, sig);
    }
}



/*-- sendsig (ptptr proc, int16 sig)
 *-- {
 *--     register ptptr p;

 *--     if (proc)
 *--         ssig (proc, sig);
 *--     else
 *--         for (p=ptab; p < ptab+PTABSIZE; ++p)
 *--             if (p->p_status)
 *--                 ssig (p, sig);
 *-- }--*/



ssig (proc, sig)
ptptr proc;
int16 sig;
{
    register stat;

    di_absolute();
    ifnot (proc->p_status)
        goto done;              /* Presumably was killed just now */

    if (proc->p_ignored & sigmask (sig))
        goto done;

    stat = proc->p_status;
    if (stat == P_PAUSE || stat == P_WAIT || stat == P_SLEEP)
        proc->p_status = P_READY;

    proc->p_wait = (char *)NULL;
    proc->p_pending |= sigmask (sig);
done:
    ei_absolute();
}



#if 1 /* Nick UZIX compatible */
/* Sendsig() send signal sig to process p
 * (or to all processes if p == NULL)
 */
void sendsig(ptptr p, signal_t sig)
{
	if (p)
		ssig(p, sig);
	else {
		p = ptab;
		while (p < ptab+PTABSIZE) {
			if ((uchar)(p->p_status) > P_ZOMBIE)
				ssig(p, sig);
			++p;
		}
	}
}
#endif



#if 1 /* Nick UZIX compatible */
/* dowait()
 */
#define pid (int)udata.u_argn0
#define statloc (int *)udata.u_argn1
#define options (int)udata.u_argn2

int dowait(void)
{
	register ptptr p;
	int retval;

	for (;;) {
		/* Search for an exited child */
		di_absolute();
		p = ptab;
		while (p < ptab+PTABSIZE) {
			if (p->p_pptr == udata.u_ptab && /* my own child! */
			   (uchar)(p->p_status) == P_ZOMBIE) {
				retval = p->p_pid;
			    	/* if (pid < -1)...
			    	   wait for any child whose group ID is equal
			    	   to the absolute value of PID
			    	   - NOT IMPLEMENTED -

			    	   if (pid == 0)...
			    	   wait for any child whose group ID is equal
			    	   to the group of the calling process
			    	   - NOT IMPLEMENTED -

			    	   if option == WUNTRACED...
			    	   - NOT IMPLEMENTED -
			    	*/
			    	if (pid > 0) if (retval != pid) continue;
				if (statloc != NULL)
#if 1 /* Nick */
					uputw(p->p_exitval, statloc);
#else
					*statloc = p->p_exitval;
#endif
				p->p_status = P_EMPTY;	/* free ptab now! */
				/* Add in child's time info.
				 * It was stored on top of p_alarm in the childs
				 * process table entry
				 */
				addtick(&udata.u_cutime, (time_t *)&(p->p_break)+0);
				addtick(&udata.u_cstime, (time_t *)&(p->p_break)+1);
				ei_absolute();
				return retval;
			}
			++p;
		}
		if (options & WNOHANG != 0) {
			udata.u_error = ECHILD;
			return 0;
		}
		/* Nothing yet, so wait */
		psleep(udata.u_ptab);	/* P_WAIT */
		if (udata.u_ptab->p_intr) {
			udata.u_error = EINTR;
			return -1;
		}
	}
}

#undef options
#undef statloc
#undef pid
#endif



