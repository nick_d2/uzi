/* devhd.h for UZI180 by Nick (we are converting everything to ANSI C) */

#ifndef __DEVHD_H
#define __DEVHD_H

int wd_read(uchar minor, uchar rawflag);
int wd_write(uchar minor, uchar rawflag);
int setup(uchar minor, uchar rawflag);
void chkstat(int stat, int rdflag);
int wd_open(uchar minor);

#endif /* __DEVHD_H */

