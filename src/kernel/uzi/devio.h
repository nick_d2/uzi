/* devio.h for UZI180 by Nick (we are converting everything to ANSI C) */

#ifndef __DEVIO_H
#define __DEVIO_H

void *bread(dev_t dev, blkno_t blk, uchar rewrite);
int bfree(bufptr bp, uchar dirty);
void *zerobuf(uchar waitfor);
void bufsync(void);
#if 1 /* Nick */
bufptr bfind(int dev, blkno_t blk);
#endif
bufptr freebuf(uchar waitfor);
#if DEBUG > 0
void bufdump(void);
#endif
devsw_t *validdev(dev_t dev, char *msg);
int bdreadwrite(bufptr bp, uchar write);
int cdreadwrite(dev_t dev, uchar write);
int d_openclose(dev_t dev, uchar open);
int d_ioctl(dev_t dev, int request, void *data);
int d_init(void);
int ok(uchar minor);
int ok_rdwr(uchar minor, uchar rawflag);
int nogood(uchar minor);
int nogood_ioctl(uchar minor, int req, void *data);
#if 1 /* def __KERNEL__ */
int insq(queue_t *q, uchar c);
int remq(queue_t *q, uchar *cp);
int uninsq(queue_t *q, uchar *cp);
void clrq(queue_t *q);
#if 0
int fullq(queue_t *q);
#endif
#endif

#endif /* __DEVIO_H */

