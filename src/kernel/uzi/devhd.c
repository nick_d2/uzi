/***************************************************************
UZI (Unix Z80 Implementation) Kernel:  devhd.c
   Unique Hardware driver contained in hdasm.asz
----------------------------------------------------------------
 Adapted from UZI By Doug Braun, and UZI280 by Stefan Nitschke
            Copyright (C) 1998 by Harold F. Bower
       Portions Copyright (C) 1995 by Stefan Nitschke
****************************************************************/
/* Revisions:
 * 1.1.98 - Added IDE and SCSI configurations.		HFB
 * 14.7.98 - Finalized config via HDCONF.H w/variables.	HFB
 */

#include "vendor.h" /* Nick, must come first */
#include <unix.h>
#include <config.h>
#include <extern.h>
#include <hdconf.h>      /* Add configuration-specific data */
#include <devhd.h> /* prototypes added by Nick */

#ifdef GIDE
extern unsigned char hdspt, hdhds;
#endif

    /* All Hard Disk Drivers pass Commands and Data via the cmdblk array.
     * The definition of each byte in the array varies as:
     *
     * SCSI:  Drivers use the short (6 byte) SCSI Read/Write Commands
     * Byte 0 - SCSI Command (08H = Read, 0AH = Write, 03H = Read Sense)
     *      1 - B7..5 = LUN, B4..0 = High 5 bits of Block Number
     *      2 -   Middle 8 bits of Block Number
     *      3 -     Low 8 bits of Block Number
     *      4 - Number of 512-byte Blocks to Read
     *      5 - Control Flags (unused, set to 0)
     * IDE:  Drivers use data in similar manner as SCSI as:
     * Byte 0 - IDE Command (20H = Read, 30H = Write)
     *      1 - High bits of Block Number
     *      2 -   Middle 8 bits of Block Number
     *      3 -     Low 8 bits of Block Number
     *      4 - Number of 512-byte Blocks to Read
     *      5 -  (unused, set to 0)
     * APIBUS:  Things are nudged around to make an 'esc' sequence:
     * Byte 0 - ESC
     *      1 - CompactFlash command ('R' = Read, 'W' = Write)
     *      2 - Bits  0.. 7 of LBA index for block
     *      3 - Bits  8..15 of LBA index for block
     *      4 - Bits 16..23 of LBA index for block
     *      5 - Bits 24..31 of LBA index for block
     *      6 - Checksum (0xff minus the sum of the preceding 4 bytes)
     *      7 - Number of 512-byte Blocks to read (done in software)
     */
#ifdef APIBUS
char cmdblk[8] = { 0x1b, 0, 0, 0, 0, 0, 0, 0 }; /* GLOBAL.  Used in main.c */
#else
char cmdblk[6] = { 0, LUN<<5, 0, 0, 0, 0 };    /* GLOBAL.  Used in hdasm.asz */
#endif
#ifdef APIBUS
long hd_offset = 0; /* Sector offset for selected partition, used in main.c */
long hd_sector = 0; /* Sector relative to offset given above, used in main.c */
#else
unsigned hd_offset = 0;      /* Track offset for Selected Unit, in hdasm.asz */
#endif

char  *dptr  = NULL;        /* Ptr to Data Buffer for I/O */
int    dlen  = 0;           /* Length of Data I/O Transfer */
char  *cptr  = NULL;        /* Ptr to Code Buffer for I/O */
int    busid = 1;           /* Hardware Unit (SCSI = 0,2,4,..64.  Unit Bit) */

extern int scsiop();

int wd_read(uchar minor, uchar rawflag)
{
    int setup();

/* kputchar(minor + '0'); */
#if DEBUG >= 2
 dprintf(2, "<\x08");
#endif

#ifdef APIBUS
    cmdblk[1] = RDCMD;
#else
    cmdblk[0] = RDCMD;
#endif
    if (setup (minor, rawflag))
        return -1; /* (0); */
/* abyte('~'); */

    chkstat (scsiop(), 1);

#if DEBUG >= 2
 dprintf(2, " \x08");
#endif

    return 0; /* success.. formerly count of bytes read */
}


int wd_write(uchar minor, uchar rawflag)
{
    int setup();

/* kputchar(minor + '0'); */
#if DEBUG >= 2
 dprintf(2, ">\x08");
#endif

#ifdef APIBUS
    cmdblk[1] = WRCMD;
#else
    cmdblk[0] = WRCMD;
#endif
    if (setup (minor, rawflag))
        return -1; /* (0); */

    chkstat (scsiop(), 0);

#if DEBUG >= 2
 dprintf(2, " \x08");
#endif

    return 0; /* success.. formerly count of bytes written */
}

int setup(uchar minor, uchar rawflag)
{
    register blkno_t block;

/* kprintf("setup(%u, %d)\n", minor, rawflag); */

    cptr = cmdblk;
    busid = 1;
#ifndef APIBUS
    cmdblk[5] = 0;       /* Clear Flags */
#endif
#ifdef GIDE
    hdspt = HD_Sector;           /* Set physical drive params if IDE */
    hdhds = HD_Heads;
#endif
    if (rawflag)
    {
        if (rawflag == 2)
        {
#ifdef APIBUS
            cmdblk[7] = swapcnt >> 9;
#else
            cmdblk[4] = swapcnt >> 9;
#endif
            dlen = swapcnt;
            dptr = swapbase;
            block = swapblk;
        }
        else
        {
#ifdef APIBUS
            cmdblk[7] = udata.u_count >> 9;
#else
            cmdblk[4] = udata.u_count >> 9;
#endif
            dlen = udata.u_count;
            dptr = udata.u_base;
            block = udata.u_offset >> BUFSIZELOG; /* Nick .o_blkno; */
        }
    }
    else
    {
#ifdef APIBUS
        cmdblk[7] = 1;
#else
        cmdblk[4] = 1;
#endif
        dlen = 512;
        dptr = udata.u_buf->bf_data;
        block = udata.u_buf->bf_blk;
    }

    switch (minor)			/* Also need to insure that ending
					   block # is valid on drive. HFB
					*/
    {
        case 0: hd_offset = HD0_Start;
#ifdef APIBUS
                if (block < HD0_Size)
#else
                if (block < (HD0_Size << 4))
#endif
                    goto setCDB;
                else break;
        case 1: hd_offset = HD1_Start;
#ifdef APIBUS
                if (block < HD1_Size)
#else
                if (block < (HD1_Size << 4))
#endif
                    goto setCDB;
                else break;
        case 2: hd_offset = SWAP_Start;
#ifdef APIBUS
                if (block < SWAP_Size)
#else
                if (block < (SWAP_Size << 4))
#endif
                    goto setCDB;
                else break;
        default: ;
    }
    udata.u_error = ENXIO;
    return (1);

setCDB:
#ifdef APIBUS
    hd_sector = block;
#else
    cmdblk[3] = block;
    cmdblk[2] = block >> 8;
    cmdblk[1] = LUN << 5;
#endif
    return (0);
}

/* Check results of operation returning (int)"stat".
 *  rdflag = 1 for Read, 0 for Write.
 */
void chkstat(int stat, int rdflag)
{
    if (stat)
    {
        kprintf ("HD %s failure stat: %x", rdflag ? "Read": "Write", stat);
        panic ("");
    }
}

int wd_open(uchar minor)
{
    return (0);
}

