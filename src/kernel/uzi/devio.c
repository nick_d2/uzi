/*
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 Buffer pool management and generic device I/O routines
**********************************************************/

#define __DEVIO__COMPILATION
#define NEED__DEVIO
#define NEED__MACHDEP
#define NEED__PROCESS

#if 1 /* Nick */
#define DEVIO /* creates the dev_tab array */
#include "vendor.h" /* Nick, must come first */
#include <unix.h>
#include <devhd.h> /* prototypes added by Nick (must come before config.h!) */
#include <devtty.h> /* prototypes added by Nick (must come before config.h!) */
#include <devmisc.h>/* prototypes added by Nick (must come before config.h!) */
#include <devio.h> /* prototypes added by Nick (must come before config.h!) */
#include <config.h>
#include <extern.h>
#define __KERNEL__ /* temporary */
#else
#include "uzix.h"
#ifdef SEPH
#include "types.h"
#include "signal.h"
#include "errno.h"
#include "sys\ioctl.h"
#endif
#include "unix.h"
#include "extern.h"
#include "config.h"
#endif

extern char *baddevmsg;

/* Buffer pool management and generic device I/O */

/* The high-level interface is through bread() and bfree().
 *
 * Bread() is given a device and block number, and a rewrite flag.
 * If rewrite is 0, the block is actually read if it is not already
 * in the buffer pool. If rewrite is set, it is assumed that the caller
 * plans to rewrite the entire contents of the block, and it will
 * not be read in, but only have a buffer named after it. Also if
 * rewrite great then 1 bread() zeroes allocated buffer.
 *
 * Bfree() is given a buffer pointer and a dirty flag.
 * The dirty flag ored with buffer bf_dirty and if result is 0, the buffer
 * is made available for further use, else the buffer is marked "dirty", and
 * it will eventually be written out to disk.  If the flag is 2, the buffer
 * will be immediately written out to disk.
 *
 * Zerobuf() returns a buffer of zeroes not belonging to any device.
 * It must be bfree'd after use, and must not be dirty.
 * It is used when a read() wants to read an unallocated block of a file.
 *
 * Bufsync() write outs all dirty blocks (exclude ones with bf_prio set!)
 *
 * Note that a pointer to a buffer structure is the same as a
 * pointer to the data.  This is very important.
 */

bufptr freebuf(uchar waitfor);

uint bufclock = 0;	/* Time-stamp counter for LRU */

#if DEBUG
uint buf_hits;	/* buffer pool hits */
uint buf_miss;	/* buffer pool misses */
uint buf_flsh;	/* buffer pool flushes */
#endif

/* read block blk from device dev, return buffer with this block */
void *bread(dev_t dev, blkno_t blk, uchar rewrite)
{
	register bufptr bp = bufpool;

#if DEBUG >= 3
 dprintf(3, "bread(%u, %u, %u) starting\n", dev, blk, rewrite);
#endif

#if 1 /* Nick */
	if (bp = bfind(dev, blk))
		{
		if (bp->bf_busy)
			panic("want busy block %d, device %d", blk, dev);
#if DEBUG
		++buf_hits;
#endif
#if DEBUG >= 5
 if (bp->bf_dev == 0 && bp->bf_blk == 222)
  {
  dprintf(5, ("chit 0x%04x 0x%02x\n", bp, bp->bf_data[0x3f]);
  }
#endif
		goto Done;
		}
#else
	/* find this block in buffer pool */
	while (bp < bufpool+NBUFS) {
		if (bp->bf_dev == dev && bp->bf_blk == blk) {
			if (bp->bf_busy)	/* ??? */
				panic("want busy block %d at dev %d",blk,dev);
#if DEBUG
			++buf_hits;
#endif
			goto Done;
		}
		++bp;
	}
#endif
#if DEBUG
	++buf_miss;
#endif
	/* block not found in pool - allocate free buffer for them */
	if ((bp = freebuf(1)) == NULL) goto Err;
	bp->bf_dev = dev;
	bp->bf_blk = blk;
	/* If rewrite is set, we are about to write over the entire block,
	 * so we don't need the previous contents
	 */
	if (rewrite == 0) {		/* we must read block data */
		if (bdread(bp) < 0) { /* device can define the error */
			if (udata.u_error == 0)
				udata.u_error = EIO;
Err:
#if DEBUG >= 3
 dprintf(3, "bread() returning NULL, error %u\n", udata.u_error);
#endif
			return NULL;
		}
#if DEBUG >= 5
 if (bp->bf_dev == 0 && bp->bf_blk == 222)
  {
  dprintf(5, ("read 0x%04x 0x%02x\n", bp, bp->bf_data[0x3f]);
  }
#endif
	}
Done:	if (rewrite > 1)		/* we need really zeroed block */
 {
		bzero(bp->bf_data, BUFSIZE);
#if DEBUG >= 5
 if (bp->bf_dev == 0 && bp->bf_blk == 222)
  {
  dprintf(5, ("zero 0x%04x 0x%02x\n", bp, bp->bf_data[0x3f]);
  }
#endif
 }
	bp->bf_busy++;			/* was always zero */
	bp->bf_time = ++bufclock;	/* Time stamp it */
#if DEBUG >= 3
 dprintf(3, "bread() returning %u, success\n", bp - bufpool);
#endif
	return (bp->bf_data);
}

/* free not needed now buffer */
int bfree(bufptr bp, uchar dirty)
{
#if DEBUG >= 3
 dprintf(3, "bfree(%u, %u) starting\n", bp - bufpool, dirty);
#endif

	bp->bf_dirty |= dirty;
	bp->bf_busy = 0;
	if (bp->bf_dirty && bp->bf_dev == NULLDEV)
		panic("attempt to write-back zerobuf");
#if 1 /* Nick */
	/* dirty_mask logic is now handled by i_sync() and wr_inode() */
	if (bp->bf_dirty >= 2) { /* Extra dirty */
#else
	if (bp->bf_dirty >= 2 && !dirty_mask) { /* Extra dirty */
#endif
#if DEBUG >= 5
 if (bp->bf_dev == 0 && bp->bf_blk == 222)
  {
  dprintf(5, ("wri2 0x%04x 0x%02x\n", bp, bp->bf_data[0x3f]);
  }
#endif
		if (bdwrite(bp) < 0) {
			udata.u_error = EIO;
#if DEBUG >= 3
 dprintf(3, "bfree() returning -1, error %u\n", udata.u_error);
#endif
			return (-1);
		}
		bp->bf_prio = bp->bf_dirty = 0;
	}
#if DEBUG >= 5
 else
  {
  if (bp->bf_dev == 0 && bp->bf_blk == 222)
   {
   dprintf(5, ("free 0x%04x 0x%02x\n", bp, bp->bf_data[0x3f]);
   }
  }
#endif
	if (!bp->bf_prio)
		wakeup(bufpool);
#if DEBUG >= 3
 dprintf(3, "bfree() returning 0, success\n");
#endif
	return 0;
}

/* Returns a free zeroed buffer with no associated device.
 * It is essentially a malloc for the kernel. Free it with brelse().
 * This procedure able to return NULL only if waitfor==0
 */
void *zerobuf(uchar waitfor)
{
	static int blk = 0;
	register bufptr bp; /* = freebuf(waitfor); Nick see below */

#if DEBUG >= 3
 dprintf(3, "zerobuf(%u) starting\n", waitfor);
#endif

	bp = freebuf(waitfor); /* Nick see above */

	if (bp) {
		bp->bf_dev = NULLDEV;
		bp->bf_blk = ++blk;
		bp->bf_busy = 1;
		bp->bf_time = ++bufclock;
		bzero(bp->bf_data, BUFSIZE);
#if DEBUG >= 3
 dprintf(3, "zerobuf() returning %d, found\n", bp - bufpool);
#endif
		return bp->bf_data;
	}
#if DEBUG >= 3
 dprintf(3, "zerobuf() returning NULL, not found\n", bp - bufpool);
#endif
	return NULL;
}

/* flush all dirty buffers (except ones with bf_prio set) */
void bufsync(void)
{
	register bufptr bp = bufpool;

#if DEBUG >= 3
 dprintf(3, "bufsync() starting\n");
#endif

#if 0
	/* dirty_mask logic is now handled by i_sync() and wr_inode() */
	dirty_mask = 0;
#endif
	while (bp < bufpool+NBUFS) {
		if (bp->bf_dev != NULLDEV &&
		    bp->bf_dirty &&
		    !bp->bf_prio) {
#if DEBUG >= 5
 if (bp->bf_dev == 0 && bp->bf_blk == 222)
  {
  dprintf(5, ("sync 0x%04x 0x%02x\n", bp, bp->bf_data[0x3f]);
  }
#endif
			if (!bdwrite(bp))
				bp->bf_dirty = 0;
		}
		++bp;
	}
#if DEBUG >= 3
 dprintf(3, "bufsync() returning\n");
#endif
}

#if 1 /* Nick */
bufptr bfind(int dev, blkno_t blk)
{
    register bufptr bp;

#if DEBUG >= 3
 dprintf(3, "bfind(%u, %u) starting\n", dev, blk);
#endif

    for (bp=bufpool; bp < bufpool+NBUFS; ++bp)
    {
        if (bp->bf_dev == dev && bp->bf_blk == blk)
 {
#if DEBUG >= 3
 dprintf(3, "bfind() returning %d, found\n", bp - bufpool);
#endif
            return (bp);
 }
    }
#if DEBUG >= 3
 dprintf(3, "bfind() returning NULL, not found\n");
#endif
    return (NULL);
}
#endif

/* find free buffer in pool or freeing oldest
 * This procedure able to return NULL only if waifor==0
 */
bufptr freebuf(uchar waitfor)
{
	uchar i;
	uint diff;
	register bufptr bp;
	register bufptr oldest = NULL;
	register uint oldtime = 0;

#if DEBUG >= 3
 dprintf(3, "freebuf(%u) starting\n", waitfor);
#endif

	/* Try to find a non-busy buffer and
	 * write out the data if it is dirty
	 */
	for (;;) {
		i = 1;
		while (oldest == NULL) {
			/* pass 1 - try to find non dirty buffer */
			/* pass 0 - try to find any buffer */
			bp = bufpool;
			while (bp < bufpool+NBUFS) {
				if (!bp->bf_busy && !bp->bf_prio &&
				    !(bp->bf_dirty & i)) {
					diff = bufclock - bp->bf_time;
					if (diff >= oldtime) {
						oldest = bp;
						oldtime = diff;
					}
				}
				++bp;
			}
			i ^= 1;
		}
		if (oldest)	/* buffer found */
			break;
		if (!waitfor)
			goto Err;
		psleep(bufpool); /* wait for buffer */
	}
	if (oldest->bf_dirty) {
#if 1 /* Nick */
		if (oldest->bf_dev == NULLDEV)
			panic("attempt to write-back zerobuf");
#endif
#if DEBUG
		++buf_flsh;
#endif
#if DEBUG >= 5
 if (bp->bf_dev == 0 && bp->bf_blk == 222)
  {
  dprintf(5, ("wri1 0x%04x 0x%02x\n", bp, bp->bf_data[0x3f]);
  }
#endif
		if (bdwrite(oldest) < 0) {
			udata.u_error = EIO;
Err:			return NULL;
		}
		oldest->bf_dirty = 0;
	}
#if DEBUG >= 5
 else
  {
  if (bp->bf_dev == 0 && bp->bf_blk == 222)
   {
   dprintf(5, ("toss 0x%04x 0x%02x\n", bp, bp->bf_data[0x3f]);
   }
  }
#endif
#if DEBUG >= 3
 dprintf(3, "freebuf() returning %d, found\n", oldest - bufpool);
#endif
	return oldest;
}

#if DEBUG
/* dump buffers info for debug */
void bufdump(void)
{
	register bufptr bp = bufpool;

	kprintf("Buf hits/miss/flsh: %u/%u/%u\nBuffs:\tdev\tblock\tDBP\ttime\t(clock=%d)\n",
		buf_hits, buf_miss, buf_flsh, bufclock);
	while (bp < bufpool+NBUFS) {
		kprintf("\t%p\t%u\t%c%c%c\t%u\n",
			bp->bf_dev, bp->bf_blk,
			bp->bf_dirty ? 'd' : '-',
			bp->bf_busy ? 'b' : '-',
			bp->bf_prio ? 'p' : '-',
			bp->bf_time);
		++bp;
	}
	buf_miss = buf_hits = buf_flsh = 0;
}
#endif

/* validate device number */
devsw_t *validdev(dev_t dev, char *msg)
{
	register devsw_t *t = &dev_tab[MAJOR(dev)];

	if (MAJOR(dev) < DEV_TAB && MINOR(dev) < t->minors)
		return t;
	if (msg != NULL)
		panic(baddevmsg, msg, MAJOR(dev));
	udata.u_error = ENODEV;
	return NULL;
}

/* Bdread() and bdwrite() are the block device interface routines.
 * They are given a buffer pointer, which contains the device,
 * block number, and data location.
 * They basically validate the device and vector the call.
 *
 * Cdread() and cdwrite() are the same for character (or "raw") devices,
 * and are handed a device number.
 *
 * Udata.u_base, count, and offset have the rest of the data.
 *
 * The device driver read and write routines now have only two arguments,
 * minor and rawflag. If rawflag is zero, a single block is desired, and
 * the necessary data can be found in udata.u_buf.
 * Otherwise, a "raw" or character read is desired, and udata.u_offset,
 * udata.u_count, and udata.u_base should be consulted instead.
 *
 * Any device other than a disk will have only raw access.
 */

/* block device read/write */
int bdreadwrite(bufptr bp, uchar write)
{
	register dev_t dev = bp->bf_dev;
	register devsw_t *dt = validdev(dev, !write ? "bdread" : "bdwrite");

	udata.u_buf = bp;
	if (write)
		return (dt->dev_write)(MINOR(dev), 0);
	return (dt->dev_read)(MINOR(dev), 0);
}

/* character device read */
int cdreadwrite(dev_t dev, uchar write)
{
	register devsw_t *dt = validdev(dev, !write ? "cdread" : "cdwrite");

	if (write)
		return (dt->dev_write)(MINOR(dev), 1);
	return (dt->dev_read)(MINOR(dev), 1);
}

int d_openclose(dev_t dev, uchar open)
{
	register devsw_t *dt = validdev(dev, open ? NULL : "d_close");

	if (dt == NULL)
		return (-1);
	if (open)
		return (dt->dev_open)(MINOR(dev));
	return (dt->dev_close)(MINOR(dev));
}

/* generic device ioctl */
int d_ioctl(dev_t dev, int request, void *data)
{
	register devsw_t *dt = validdev(dev, NULL);

	if (dt == NULL) {
		udata.u_error = ENXIO;
		goto Err;
	}
	if ((dt->dev_ioctl)(MINOR(dev), request, data)) {
		udata.u_error = EINVAL;
Err:		return (-1);
	}
	return 0;
}

/* all devices initialization */
int d_init(void)
{
	register devsw_t *dt = dev_tab;
	uchar minor;

	while (dt < dev_tab+DEV_TAB) {
		minor = 0;
		while (minor < dt->minors) {
			if ((dt->dev_init)(minor) != 0)
				return -1;
			++minor;
		}
		++dt;
	}
	return 0;
}

/* special filler for driver functions, which do nothing */
int ok(uchar minor)
{
	/* NOTUSED(minor); */
	return 0;
}

/* special filler for driver read/write functions, which do nothing */
int ok_rdwr(uchar minor, uchar rawflag)
{
	/* NOTUSED(minor); */
	/* NOTUSED(rawflag); */
	return 0;
}

/* special filler for driver functions, which return error */
int nogood(uchar minor)
{
	/* NOTUSED(minor); */
	return -1;
}

/* special filler for driver ioctl functions, which return error */
int nogood_ioctl(uchar minor, int req, void *data)
{
	/* NOTUSED(minor); */
	/* NOTUSED(req); */
	/* NOTUSED(data); */
	return -1;
}

#ifdef __KERNEL__
/*
 * Character queue management routines
 */

/* add something to the queue tail */
int insq(queue_t *q, uchar c)
{
	uchar retval = 0;

	di_absolute();
	if (q->q_count != q->q_size) {	/* queue not full */
		*(q->q_tail) = c;	/* save char */
		++q->q_count;		/* count them */
		if (++q->q_tail >= q->q_base + q->q_size)
			q->q_tail = q->q_base;	/* process pointer rollover */
		++retval;
	}
	ei_absolute();
	return retval;
}

/* Remove something from the queue head */
int remq(queue_t *q, uchar *cp)
{
	uchar retval = 0;

	di_absolute();
	if (q->q_count != 0) {
		*cp = *(q->q_head);	/* get char from queue */
		--q->q_count;		/* count them */
		if (++q->q_head >= q->q_base + q->q_size)
			q->q_head = q->q_base;	/* process pointer rollover */
		++retval;
	}
	ei_absolute();
	return retval;
}

/* Remove something from the tail; the most recently added char. */
int uninsq(queue_t *q, uchar *cp)
{
	uchar retval = 0;

	di_absolute();
	if (q->q_count != 0) {
		--q->q_count;		/* count removed char */
		if (--q->q_tail < q->q_base)
			q->q_tail = q->q_base + q->q_size - 1;
		*cp = *(q->q_tail);	/* save removed char */
		++retval;
	}
	ei_absolute();
	return retval;
}

/* Clear queue. */
void clrq(queue_t *q)
{
	di_absolute();
	q->q_count = 0;
	q->q_tail = q->q_base;
	q->q_head = q->q_base;
	ei_absolute();
}

#if 0
/* Returns true if the queue has more characters than its wakeup number */
/* not used now */
int fullq(queue_t *q)
{
	uchar s;

	di_absolute();
	s = q->q_count > q->q_wakeup;
	ei_absolute();
	return s;
}
#endif
#endif	/* __KERNEL__ */

