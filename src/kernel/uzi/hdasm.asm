
	public	_scsiop						; PUBlic

	extern	_dptr, _dlen, _cptr, _busid, _cmdblk, _hd_offset ; in devhd.c
 .if 1 ; IAR
	extern	?BANK_FAST_LEAVE_L08
 .endif

;/* This SCSI driver code section is a polled-IO driver for the YASBEC */

;  Equates for the National DP8490/NCR 5380 SCSI controller on YASBEC

NCR	EQU	058H		; Base of NCR 5380/DP5380/DP8490 in YASBEC

; 5380 Chip Registers

NCRDAT	EQU	NCR		; Current SCSI Data		(Read)
				; Output Data Register		(Write)
NCRCMD	EQU	NCR+1		; Initiator Command Register	(Read/Write)
NCRMOD	EQU	NCR+2		; Mode Register			(Read/Write)
NCRTGT	EQU	NCR+3		; Target Command Register	(Read/Write)
NCRBUS	EQU	NCR+4		; Current SCSI Bus Status	(Read)
NCRST	EQU	NCR+5		; Bus & Status Register		(Read)
				; Start DMA Send		(Write)
NCRINT	EQU	NCR+7		; Reset Parity/Interrupt	(Read)
				; Start DMA Initiator Receive	(Write)
DMAACK	EQU	NCR+8		; SCSI Dack IO Port		(Read/Write)

; Bit Assignments for 5380 Ports as indicated

B_AACK	EQU	00010000B	; Assert *ACK		(NCRCMD)
B_ASEL	EQU	00000100B	; Assert *SEL		(NCRCMD)
B_ABUS	EQU	00000001B	; Assert *Data Bus	(NCRCMD)

B_BSY	EQU	01000000B	; *Busy			(NCRBUS)
B_MSG	EQU	00010000B	; *Message		(NCRBUS)
B_CD	EQU	00001000B	; *Command/Data		(NCRBUS)
B_IO	EQU	00000100B	; *I/O			(NCRBUS)

B_PHAS	EQU	00001000B	; Phase Match		(NCRST)

	rseg	CODE
;---------------------------------------------------------------------
; Try the Command specified.  If errors returned (e.g. Attn assertion),
;  read the SCSI Sense status and try the command again.

_scsiop:
 .if 1 ; IAR
	push	bc
	push	de
	call	LWRD _scsiop_entry
	pop	de
	pop	bc
	jp	?BANK_FAST_LEAVE_L08
_scsiop_entry:
 .endif
	LD	HL,(_cmdblk+2)	; Get low 16-bits of Block #
	LD	E,H		;  in
	LD	D,L		;   Little-Endian order
	LD	HL,(_hd_offset)	; Fetch Track offset for this Logical Unit
	XOR	A
	LD	B,4
Mul16:	ADD	HL,HL		; Multiply by 16
	ADC	A,A		;   with 20-bit result
	DJNZ	Mul16
	ADD	HL,DE		; Add desired relative block #
	ADC	A,0		;   to 20-bit resulting block Number
	LD	C,A
	EX	DE,HL
	LD	HL,_cmdblk+1	; Pt to LUN/Hi-5 Block #
	LD	A,(HL)		; Get it
	AND	0E0H		;  strip block bits
	OR	C		;   add in ours
	LD	(HL),A		;    save
	INC	HL		; Advance to cmdblk+2
	LD	(HL),D		;  save middle 8-bits of block
	INC	HL
	LD	(HL),E		;   save low 8-bits

	CALL	LWRD HDRW0		; Try the Command
	RET	Z		; ..exit if Ok
	LD	HL,SENSE	; Else set for Sense Command
	LD	DE,snsDat	;  Pt to sense data
	CALL	LWRD HDRW1		;   and execute
HDRW0:	LD	DE,(_dptr)	; Get Ptr to Data Area
	LD	HL,_cmdblk	;   and Command Area
HDRW1:	LD	(hdCmd),HL	; Save Comnd Blk Ptr
	LD	(hdDat),DE	;   and Data Ptr
	CALL	LWRD SCSI		; Do the Work
	AND	00000010B	; Any errors?
	LD	L,A
	RET	NZ		;  return 0 if Ok, 02 if Errors
	LD	H,A
	RET

;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;  Raw SCSI Driver

SCSI:	XOR	A
	OUT	(NCRCMD),A	; Clear any previous Controller junk
	OUT	(NCRMOD),A
	OUT	(NCRTGT),A

	LD	A,(_busid)	; Get the Target Device address bit
	OR	10000000B	;   add Host initiator address bit
	OUT	(NCRDAT),A
	IN	A,(NCRCMD)	; Get Initiator Comnd Reg
	OR	B_ABUS		;  Get the Data Bus
	OUT	(NCRCMD),A
	LD	A,B_ASEL+B_ABUS	; Now Claim both Select and Data Bus bits
	OUT	(NCRCMD),A

; Wait for roughly 250 mS.  We might not have a valid clock so we just use
; a loop counter.  The specified SCSI duration is 250 mS.

	LD	DE,25000	; Set loop limit
	LD	HL,0FFFFH	; Preset Timeout Error Status

BSYWT:	DEC	DE
	LD	A,D
	OR	E		; Have we timed out?
	JR	Z,TimOut	; ..exit to Error if So
	IN	A,(NCRBUS)	; Get the Current Bus Status
	AND	B_BSY		; Is it BSY?
	JR	Z,BSYWT		; ..loop if Not
			;..else fall thru..
	LD	A,B_ABUS
	OUT	(NCRCMD),A	; Grab Bus w/o Select Command (or free)
	XOR	A		;  get a Zero without affecting Zero Flag
	OUT	(NCRCMD),A	;   then free the Data Bus
			;..fall thru to check for Phase Change..
PHASE:	IN	A,(NCRBUS)	; Read the Bus Status
	AND	B_MSG+B_CD+B_IO	;  keep the three phases we are interested in
	RRCA			; Rotate Phase status bits
	RRCA			;   into B0-2 position for testing
	OUT	(NCRTGT),A	; Check for phase match
	LD	C,NCRDAT	; Data goes to/from this port
	LD	HL,messge	;  (Ph 7 input goes here)
	CP	7		; Are we in Phase 7 ?
	JR	Z,HDIN		; ..jump if so to Message In Phase
	LD	HL,(hdDat)	;  (Ph 0/1 IO From/To here)
	OR	A		; Are we in Phase 0 ?
	JR	Z,HDOUT		; ..jump to if so to Data Out Phase
	DEC	A		; Are we in Phase 1 ?
	JR	Z,HDIN		; ..jump to if so to Data In Phase
	LD	HL,(hdCmd)	;  (Ph 2 output from here)
	DEC	A		; Are we in Phase 2 ?
	JR	Z,HDOUT		; ..jump to if so to Command Out Phase
	LD	HL,status	;  (Ph 3 Input to here)
	DEC	A		; Are we in Phase 3 ?
	JR	Z,HDIN		; ..jump to if so to Status In Phase
			;..else fall thru
; Phases 4, 5 and 6 wind up here in an Error

	LD	HL,0FEFFH	; Set Phase Error Return Value
	JR	TimOut		; ..continue to Exit


;.....
; SCSI Input routine.
; Polled-IO Input Routine
;  Enter with HL pointing to Buffer, C Addressing 5380 Data Port

HDIN:	IN	A,(NCRBUS)	; Check the Bus
	BIT	5,A		; Do we have a REQuest?
	JR	NZ,HDIN1	; ..jump if So to readint
	AND	B_BSY		; Is the Bus Busy?
	JR	NZ,HDIN		; ..loop if So

HDEXIT:	XOR	A		; Else we are finished.  Clean up & Quit
	OUT	(NCRCMD),A	;  clear Initiator Command Register
	OUT	(NCRTGT),A	;   and Target Command Register
	LD	HL,(status)	; Get Message (H) and Status (L) Bytes
TimOut:	LD	A,L		; Get Status Byte
	OR	A		;   set Return Status Ok if Status Byte = 0
	RET

HDIN1:	IN	A,(NCRST)	; Get SCSI Status
	AND	B_PHAS		; Do the Phases Match?
	JR	Z,PHASE		; ..quit here if Not and Clear
	INI			; Else Get byte from Port (C) to Memory at (HL)
	LD	A,B_AACK
	OUT	(NCRCMD),A	; ACKnowledge the Byte
	XOR	A
	OUT	(NCRCMD),A	;  clear *ACK bit
	JR	HDIN		;   and back for More

;.....
; SCSI Output Routine.
; Enter: HL = Address of Send Buffer start
; Polled-IO Output Routine
;  Enter with HL pointing to Buffer, C Addressing 5380 Data Port

HDOUT:	LD	A,B_ABUS	; Assert Data Bus
	OUT	(NCRCMD),A
	IN	A,(NCRBUS)	; Check the SCSI Bus
	BIT	5,A		; Do we have a REQuest?
	JR	NZ,HDOUT1	; ..jump if So to Send a Byte
	AND	B_BSY		; Else is the Bus Busy?
	JR	NZ,HDOUT	; ..loop if so because we have more to go
	JR	HDEXIT		; Else Quit

HDOUT1:	IN	A,(NCRST)	; Get Current Status
	AND	B_PHAS		; Do we have a Phase Match?
	JR	Z,PHASE		; ..quit here if Not and clear

	OUTI			; Send a byte from (HL) to Port (C)
	LD	A,B_AACK+B_ABUS
	OUT	(NCRCMD),A	; Set ACKnowledge and BUS bits
	XOR	A
	OUT	(NCRCMD),A	;  clear *ACK and *BUS
	JR	HDOUT		; ..and back for More

;.....
; SCSI Read Sense Command Data Block

	rseg	IDATA0
SENSE:	DEFS	6
	rseg	CDATA0
	DEFB	03H		; SCSI Sense Command
	DEFB	00,00,00,18,00	;   remainder of Sense Command Block
				; (data length set to available space)

	rseg	UDATA0
hdCmd:	DEFS	2		; Storage for Current Command Data Block
hdDat:	DEFS	2		; Storage for current Data Transfer Area
snsDat:	DEFS	18		; Storage for extended Sense Data Received

;<<--- Do not re-order the following Two Bytes --->>
status:	DEFS	1		; Ending Status Byte
messge:	DEFS	1		; Ending Message Byte
	rseg	CODE

	end
