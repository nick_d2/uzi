/* xip.h for UZI180 by Nick (we are converting everything to ANSI C) */

#ifndef __XIP_H
#define __XIP_H

int bitmap_align(inoptr ino, off_t size);
int bitmap_ualign(inoptr ino, off_t size);
blkno_t *bitmap_examine(fsptr dev, inoptr ino, off_t size,
				   blkno_t *regioncount, blkno_t *blockcount);
blkno_t bitmap_align_chase(fsptr dev, inoptr ino, blkno_t blk, blkno_t pos,
				      blkno_t *region, blkno_t regions,
				      blkno_t blocks);
blkno_t bitmap_align_bmap(inoptr ino, blkno_t newno, blkno_t pos);
blkno_t bitmap_align_reverse(blkno_t blk,
			     blkno_t *region, blkno_t regions, blkno_t blocks);
blkno_t bitmap_align_recurse(fsptr dev, inoptr ino, int exclude,
				    blkno_t *parent, char *dirty,
				    int indirection, blkno_t *region,
				    blkno_t regions, blkno_t blocks);
blkno_t bitmap_search(dev_t devno, int size, blkno_t start, blkno_t final);
int bitmap_reserve(dev_t devno, blkno_t blk, int size, int flag,
				blkno_t start, blkno_t final);
blkno_t bitmap_find(dev_t devno, blkno_t blk, int flag, int toggle,
				 blkno_t start, blkno_t final);
int bitmap_get(dev_t devno, blkno_t blk, blkno_t start, blkno_t final);
int bitmap_set(dev_t devno, blkno_t blk, int flag,
			    blkno_t start, blkno_t final);

#endif /* __XIP_H */

