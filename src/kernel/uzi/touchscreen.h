/* touchscreen.h for uzi180 - touchscreen definitions and declarations */

#define RTC_INT	0	/* internal RTC */
#define RTC_EXT	1	/* external RTC */

typedef struct
{
	unsigned char seq;	/* RTC sequence in progress */
	unsigned char source;	/* RTC source */
} rtcT;

extern rtcT rtc;

extern int RTC_extract(char c, char s);
extern void sttime(time_t *tvec);
extern void touchscreen_init(void);
extern void touchscreen_rx_hook(SDEV *device, char c);
