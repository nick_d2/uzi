/* devno.h for UZI180 by Rob */

#define DEVOUT_BASE	0x20	/* above this number means output to device */

#define _2SDEV(p)	((p)-1)	/* p is a minor device number */
#define _2MINOR(p)	((p)+1)	/* p is an SDEV device number */


/* Minor device numbers */
#define DEV_S0	1		/* Serial 0 */
#define DEV_S1	2		/* Serial 1 */
#define DEV_S2	3		/* Serial 2 */
#define DEV_S3	4		/* Serial 3 */
#define DEV_LC	5		/* Load cell */
#define DEV_TS	6		/* Touchscreen */
#define DEV_CD	7		/* Customer display */
#define DEV_TP	9		/* Thermal printer */


/*
 * Struct functioning as a wrapper to certain parts of SDEV,
 * in order to maintain some extra administration for devices
 */
typedef struct 
{
	char tsyshook;
	/*
	 * vector[0] occupied by test system RX hook
	 * vector[1] occupied by device original RX handler
	 */
	void (*saved_rx_vector[2])(SDEV *device, char c);
} wsdevsT;

extern wsdevsT wsdevs[PORTS];
