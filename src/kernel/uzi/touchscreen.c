/* touchscreen.c for uzi180 - touchscreen handlers */

#include "vendor.h" /* Nick, must come first */
#include <cxfuncs.h>
#include <cxextern.h>
#include <string.h>
#include <unix.h>
#include <config.h>
#include <extern.h>
#include "devtty.h"
#include "devno.h"
#include "touchscreen.h"
#include "testsystem.h"


/* RTC defaults */
rtcT rtc = {
		0,		/* No RTC sequence in progress */
		RTC_INT		/* RTC source is internal */
	   };

void touchscreen_init(void)
{
        K_OS_Disable_Interrupts();

	/* Install touchscreen RX hook */
	wsdevs[_2SDEV(DEV_TS)].saved_rx_vector[0] = sdevs[_2SDEV(DEV_TS)].rx_vector;
	sdevs[_2SDEV(DEV_TS)].rx_vector = touchscreen_rx_hook;

        K_OS_Enable_Interrupts();
}

void touchscreen_rx_hook(SDEV *device, char c)
{
	/* Extract RTC data from input stream */
	if (!RTC_extract(c, RTC_INT))
		(*wsdevs[_2SDEV(DEV_TS)].saved_rx_vector[0])(device, c);

	/* If the testsystem is recording, pass the RTC data on */
	else if (tsys.rtc_rec)
		(*wsdevs[_2SDEV(DEV_TS)].saved_rx_vector[0])(device, c);
}

/*
 * s: RTC source => s == RTC_INT : internal RTC
 *                  s == RTC_EXT : external RTC
*/
int RTC_extract(char c, char s)
{

	static unsigned char state[2] = {0, 0};
	static unsigned char message[2][8];

	message[s][state[s]] = c;

	switch (state[s])
	{

	case 0:
		switch (c)
		{

		case 0x1b:
			/*
			 * There's still a problem with this first ESC:
			 * it either IS or IS NOT given to the application
			 * depending on how the return code for this
			 * first ESC (1) is interpreted. 
			 */
			state[s]++;
			rtc.seq = 1;
			break;

		default:
			/* Encountered no sequence at all */
			return(0);

		}
		break;

	case 1:
		if (c == 'T')
		{
			state[s]++;
		}
		else
		{
			state[s] = 0;
			rtc.seq = 0;
			/* Unknown ESC sequence */
			return(0);
		}

		break;

	case 2: /* hour */
	case 3: /* mins */
	case 4: /* secs */
	case 5: /* month */
	case 6: /* day */
		state[s]++;
		break;

	case 7: /* year-1980 */
		if (rtc.source == s)
		{
			tod.t_time = ((int)message[s][4] >> 1) |
				     ((int)message[s][3] << 5) |
				     ((int)message[s][2] << 11);

			tod.t_date = (int)message[s][6] |
				     ((int)message[s][5] << 5) |
				     ((int)message[s][7] << 9);
		}

		state[s] = 0;
		rtc.seq = 0;
		/* Encountered complete RTC sequence */
		return(2);
	}

	/* Partial RTC sequence */
	return(1);
}

void sttime(time_t *tvec)
{
	char message[8];

	message[0] = 0x1b;
	message[1] = 'T';
	message[2] = (tvec->t_time >> 11) & 0x1f;	/* hour */
	message[3] = (tvec->t_time >> 5) & 0x3f;	/* mins */
	message[4] = (tvec->t_time << 1) & 0x3f;	/* secs */
	message[5] = (tvec->t_date >> 5) & 0xf;		/* month */
	message[6] = tvec->t_date & 0x1f;		/* day */
	message[7] = (tvec->t_date >> 9) & 0x3f;	/* year */

	if (K_Put_Str_Wait(_2SDEV(DEV_TS), message, 8, 0x1000) != K_OK)
	{
		panic("can't sttime");
	}
}

