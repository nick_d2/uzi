/* testsystem.h for uzi180 - testsystem definitions and declarations */

typedef struct
{
	unsigned char enabled;	/* status of test system */
	unsigned char rtc_rec;	/* Internal RTC being recorded */
} tsysT;

extern tsysT tsys;

extern void tsys_init();
extern void tsys_exit();

