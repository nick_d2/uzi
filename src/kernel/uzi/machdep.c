/***************************************************************
   UZI (Unix Z80 Implementation) Kernel:  machdep.c
----------------------------------------------------------------
 Adapted from UZI By Doug Braun, and UZI280 by Stefan Nitschke
            Copyright (C) 1998 by Harold F. Bower
       Portions Copyright (C) 1995 by Stefan Nitschke
****************************************************************/
/* Revisions:
 *  9.12.97 - modified _asm for only Z80, added include for
 *	config.h, deleted from unix.h.			     HFB
 */

#undef MEMDEBUG

#include "vendor.h" /* Nick, must come first */
#include <unix.h>
#include <config.h>
#include <extern.h>
#define __KERNEL__ /* Nick please look into this */
void panic(char *s, void *a1, void *a2, void *a3); /* Nick */
void warning(char *s, void *a1, void *a2, void *a3); /* Nick */

/* This is chained to from the initial hardware setup in MACHASM.ASZ which
 * performs initial required actions such as clearing uninitialized RAM,
 * setting up the interrupt vector table, and disabling interrupts.  Unique
 * hardware setups should also have been performed prior to entering here.
 * This is the equivalent of main()
 */

fs_init()
{
    /* abyte('@'); */

#if 1 /* Nick UZIX compatible */
    total = 16; /* total number of writeable 16 kbyte pages in system (256k) */
#endif

    inint = 0;
    udata.u_insys = 1;
    ei_absolute(); /* Nick */
    init2();                 /* in process.c, never returns here */
}


/* This checks to see if a user-suppled address is legitimate */

valadr (base, size)
char *base;
uint16 size;
{
#ifndef UTIL /* can't validate addresses under Windows */
    if (base < PROGBASE || base+size > PROGTOP) /* >= (char *)&udata) */
    {
        udata.u_error = EFAULT;
        return(0);
    }
#endif
    return(1);
}


/* This adds two tick counts together.  The t_time field holds up to
 *  one second of ticks, while the t_date field counts minutes.
 */

addtick (t1, t2)
time_t *t1, *t2;
{
    t1->t_time += t2->t_time;
    t1->t_date += t2->t_date;
    if (t1->t_time >= 60*TICKSPERSEC)
    {
        t1->t_time -= 60*TICKSPERSEC;
        ++t1->t_date;
    }
}


incrtick (t)
time_t *t;
{
    if (++t->t_time == 60*TICKSPERSEC)
    {
        t->t_time = 0;
        ++t->t_date;
    }
}


static int cursig;
static int (*curvec)();

/*----------------------------------------------------------*/
calltrap()
{
    /* Deal with a pending caught signal, if any. */
    /* udata.u_insys should be false, and interrupts enabled.
     * Remember, the user may never return from the trap routine.
     */

    if (udata.u_cursig)
    {
        cursig = udata.u_cursig;
        curvec = udata.u_sigvec[cursig];
        udata.u_cursig = 0;

	/* reset to default */
        udata.u_sigvec[cursig] = (void (*)(signal_t))SIG_DFL;

	/* call the user space handler */
        ei_absolute(); /* Nick */
 kprintf("calling pid %d signal %d handler at 0x%08lx\n",
         udata.u_ptab->p_pid, cursig, (long)curvec);
        (*curvec)(cursig);
 kprintf("done signal handler\n");
        di_absolute(); /* Nick */
    }
}

/*-----------------------------------------------------------*/
/* Read date/time to System location from global variable    */
#ifndef UTIL
rdtime (tloc)
time_t *tloc;
{
    di_absolute(); /* Nick */
    tloc->t_time = tod.t_time;
    tloc->t_date = tod.t_date;
    ei_absolute(); /* Nick */
}
#endif


/*--------------------------------------------------------*/
/* Set Clock Date/Time.  Error exit since Not implemented */

#ifdef UTIL
sttime()
{
    panic ("Calling sttime\n", 0, 0, 0); /* Nick added 0, 0, 0 */
}
#endif


#if 1 /* Nick UZIX compatible */
void panic(char *s, void *a1, void *a2, void *a3)
{
	/* what's the point di_absolute(); */
	++inint;
	kprintf("\nPANIC: ");
	kprintf(s,a1,a2,a3);
	kputchar('\n');
#if 0
	idump();
#endif
	abort(99);
}

/* warning() prints warning message to console */
void warning(char *s, void *a1, void *a2, void *a3)
{
	kprintf("\nWARNING: ");
	kprintf(s,a1,a2,a3);
	kputchar('\n');
}

#if 0 /* Nick see main.c */
#ifdef __KERNEL__
/* kputs() prints zero-terminated character string to console */
void kputs(s)
	register char *s;
{
	while (*s)
		kputchar((uchar)*(s++));
}
#endif
#endif

#if DEBUG >= 1
/* _idump() dumps state of all inodes
 */
static void _idump(void)
{
	register inoptr ip = i_tab;

	kprintf("Inodes:\tMAGIC\tDEV\tNUM\tMODE\tNLINK\t(DEV)\tREFS\tDIRTY\n");
	while (ip < i_tab + ITABSIZE) {
		kprintf("#%d\t%d\t%p\t%u\t%06o\t%d\t%p\t%d\t%d\n",
			ip - i_tab, ip->c_magic, ip->c_dev, ip->c_num,
			ip->c_node.i_mode, ip->c_node.i_nlink,
			DEVNUM(ip), ip->c_refs, ip->c_dirty);
		++ip;
	}
}

/* _pdump() dumps state of all processes
 */
static void _pdump(void)
{
	register ptptr pp = ptab;

	kprintf("Proc:\tSTATUS\tWAIT\tPID\tPPTR\tALARM\tPENDING\tIGNORED\tBREAK\n"); /*\tSP\n");*/
	while (pp < ptab + PTABSIZE) {
		kprintf("#%d\t%d\t%p\t%d\t%d\t%d\t%p\t%p\t%p\t%p\n",
			pp - ptab, pp->p_status, pp->p_wait, pp->p_pid,
			pp->p_pptr - ptab, pp->p_alarm, pp->p_pending,
			pp->p_ignored, pp->p_break/*, pp->p_sp*/);
		++pp;
	}
}

/* idump() dumps state of all system datas
 */
void idump(void)
{
	kprintf("%s: Errno %d, root #%d, Insys=%d, ptab #%d, callno=%d, cwd #%d, usp %p\n",
		udata.u_name,
		udata.u_error, root_ino - i_tab,
		udata.u_insys, udata.u_ptab - ptab,
		udata.u_callno, udata.u_cwd - i_tab,
		udata.u_sp /*udata.u_ptab->p_sp*/);
	_idump();
	_pdump();
	bufdump();
}
#endif

#else
/* This prints an error message and dies. */

panic (s)
char *s;
{
    di();
    inint = 1;
    ei_absolute(); /* added by Nick... has to come after the above */
#if 1 /* Nick */
    kprintf("\nPANIC: %s\n",s);
#else
    kprintf("PANIC: %s\n",s);
#endif
    idump();
    abort();
}


warning (char *s)
{
    kprintf("WARNING: %s\n",s);
}


idump()
{
    inoptr ip;
    ptptr pp;
#if 1 /* Nick UZIX compatible */
    extern cinode_t i_tab[];
#else
    extern struct cinode i_tab[];
#endif

#if 1 /* Nick UZIX compatible */
    kprintf("Err %d root %d\n", udata.u_error, root_ino - i_tab);
#else
    kprintf("Err %d root %d\n", udata.u_error, root - i_tab);
#endif
    kprintf ("\tMAGIC\tDEV\tNUM\tMODE\tNLINK\t(DEV)\tREFS\tDIRTY\n");

    for (ip=i_tab; ip < i_tab+ITABSIZE; ++ip)
    {
        kprintf ("%d\t%d\t%d\t%u\t0%o\t",
                ip-i_tab, ip->c_magic,ip->c_dev, ip->c_num,
                ip->c_node.i_mode);
        kprintf ("%d\t%d\t%d\t%d\n",             /* line split for compiler */
                ip->c_node.i_nlink,ip->c_node.i_addr[0],
                ip->c_refs,ip->c_dirty);
        ifnot (ip->c_magic)     
            break;
    }

    kprintf ("\tSTAT\tWAIT\tPID\tPPTR\tALARM\tPENDING\tIGNORED\tCHILD\n");
    for (pp=ptab; pp < ptab+PTABSIZE; ++pp)
    {
        kprintf ("%d\t%d\t0x%x\t%d\t",
                pp-ptab, pp->p_status, pp->p_wait,  pp->p_pid);
	kprintf ("%d\t%d\t0x%x\t0x%x\t%x\n",      /* line split for compiler */
                pp->p_pptr-ptab, pp->p_alarm, pp->p_pending,
                pp->p_ignored, pp->p_fork_inf);
        ifnot (pp->p_pptr)
            break;
    }

    bufdump();

    kprintf ("insys %d ptab %d call %d cwd %d sp 0x%x\n",
            udata.u_insys,udata.u_ptab-ptab, udata.u_callno, udata.u_cwd-i_tab,
            udata.u_sp);
}
#endif


#if 0 /* Nick UZIX compatible */
/* Min() calculate minimal of two numbers
 * Must be function not macro!
 */
int Min(int a, int b)
{
	return (b < a ? b : a);
}
#endif


