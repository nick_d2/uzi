/*
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 Implementation of system calls
**********************************************************/

#define NEED__FILESYS
#define NEED__MACHDEP
#define NEED__PROCESS
#define NEED__SCALL

#if 1 /* Nick */
#include "vendor.h" /* Nick, must come first */
#ifndef UTIL /* Nick */
#include <cxfuncs.h> /* because reboot() must wait on SERIAL 1 tx */
#endif
#include <unix.h>
#include <config.h>
#include <extern.h>
#include "devio.h" /* prototypes added by Nick */
#include "filesys.h" /* prototypes added by Nick */
#include "scall2.h" /* prototypes added by Nick */
#define __KERNEL__ /* temporary */
void sttime(time_t *tvec); /* temporary */
#else
#include "uzix.h"
#ifdef SEPH
#include "types.h"
#include "signal.h"
#include "errno.h"
#include "fcntl.h"
#include "sys\stat.h"
#endif
#include "unix.h"
#include "extern.h"
#endif

/* Implementation of system calls */

#define IBLK(ino)	((ino)->c_node.i_size.o_blkno)
#define IOFF(ino)	((ino)->c_node.i_size.o_offset)

/* definitions for 'udata' access */
#define UWHERE		(udata.u_offset)
#define UBLK		(UWHERE.o_blkno))
#define UOFF		(UWHERE.o_offset))
#define UBAS		(udata.u_base)
#define UCNT		(udata.u_count)
#define UERR		(udata.u_error)

#ifdef __KERNEL__

#if 1 /* Nick */
int uint_min(int, int);
#else
int Min(int, int);
#endif

/* return a pointer to p_tab of process pid only */
/* user must own process pid or be the superuser */
ptptr findprocess(int pid)
{
	register ptptr p = ptab;
	while (p < ptab + PTABSIZE) {
		if (p->p_pid == pid) {
			if (udata.u_ptab->p_uid == p->p_uid || super()) {
					return p;
			}
			else {
				UERR = EPERM;
				goto Err;
			}
		}
		++p;
	}
	UERR = EINVAL;
Err:	return NULL;
}

/********************************************
 SYSCALL pipe(int fildes[2]);
*******************************************/
#define fildes ((uint *)udata.u_argn0)

void filldesc(uchar oft, uchar access, inoptr ino)
{
	oft_t *op;

	op = &of_tab[oft];
	op->o_ptr = 0;
	op->o_inode = ino;
	op->o_access = access;
}

int sys_pipe(void)
{
	uchar u1, u2, oft1 = (uchar)-1, oft2 = (uchar)-1;
	inoptr ino;

	/* uf_alloc doesn't alloc an entry in user file table,
	   just returns the first free entry. so you must set it
	   before allocating another, or you'll get the same entry
	*/

	if ((ino = i_open(root_dev, NULLINO)) == NULL) {
		UERR = ENOMEM;
		goto Err;
	}
	if ((u1 = uf_alloc()) == (uchar)-1 ||
	    (oft1 = oft_alloc()) == (uchar)-1) {
		UERR = ENOENT;
		goto Err1;
	}
	udata.u_files[u1] = oft1;	/* read side */
	filldesc(oft1, O_RDONLY, ino);

	if ((u2 = uf_alloc()) == (uchar)-1 ||
	    (oft2 = oft_alloc()) == (uchar)-1) {
		udata.u_files[u1] = -1; 	/* free user file table entry */
		UERR = ENOENT;
		goto Err2;
	}
	/* fill in pipe descriptors */
	udata.u_files[u2] = oft2;	/* write side */
	filldesc(oft2, O_WRONLY, ino);

	/* No permissions necessary on pipes */
	ino->c_node.i_mode = S_IFPIPE | 0777;
	ino->c_node.i_nlink = 0;	/* a pipe is not in any directory */
	++ino->c_refs;			/* count two sides of pipe */

#if 1 /* Nick */
	uputw(u1, &fildes[0]);
	uputw(u2, &fildes[1]);
#else
	fildes[0] = u1;
	fildes[1] = u2;
#endif

	return 0;

Err2:	if (oft2 != (uchar)-1) oft_deref(oft2);
Err1:	if (oft1 != (uchar)-1) oft_deref(oft1);
Err:	return (-1);
}
#undef fildes

/**********************************
 SYSCALL stime(time_t *tvec)
**********************************/
#define tvec (time_t *)udata.u_argn0
int sys_stime(void)
{
#if 1 /* Nick */
	time_t buf;

	if (super() == 0)
		{
		udata.u_error = EPERM;
		return -1;
		}

	buf.t_time = ugetw(&(tvec)->t_time);
	buf.t_date = ugetw(&(tvec)->t_date);
	sttime(&buf);
	return 0;
#else
	if (super()) {
		sttime(tvec);
		return 0;
	}
	UERR = EPERM;
	return (-1);
#endif
}
#undef tvec

/*********************************
 SYSCALL times(struct tms *buf);
*********************************/
#define buf ((struct tms *)udata.u_argn0)
int sys_times(void)
{
	if (valadr(buf, sizeof(struct tms))) {
#if 1 /* Nick */
		uput(&udata.u_utime, buf, 4 * sizeof(time_t));
		uput(&ticks, &buf->tms_etime, sizeof(time_t));
#else
		di_absolute();
		bcopy(&udata.u_utime, buf, 4 * sizeof(time_t));
		bcopy(&ticks, &buf->tms_etime, sizeof(time_t));
		ei_absolute();
#endif
		return (0);
	}
	return (-1);
}
#undef buf

/**********************************
 SYSCALL brk(char *addr);
**********************************/
#define addr ((char *)udata.u_argn0)
int sys_brk(void)
{
#if 1 /* Nick */
	if (addr < PROGBASE /*|| (addr + 256) >= (char *)udata.u_sp*/) {
#else
	auto char dummy;	/* A thing to take address of */

	/* A hack to get approx val of stack ptr. */
	if ((uint)addr < PROGBASE || (addr + 256) >= &dummy) {
#endif
		UERR = ENOMEM;
		return (-1);
	}
	udata.u_ptab->p_break = addr;
	udata.u_break = (unsigned)addr; /* Nick added cast */
	return 0;
}
#undef addr

/************************************
 SYSCALL sbrk(uint incr);
************************************/
#define incr udata.u_argn0
int sys_sbrk(void)
{
#if 1 /* Nick */
	register char *oldbrk = udata.u_ptab->p_break;
#else
	register char *oldbrk = (char *)udata.u_break; /* Nick added cast */
#endif

	if (incr) {
		incr = incr + (uint)oldbrk;
		if ((uint)incr < (uint)oldbrk || sys_brk())
			/* argument already on place */
			return (-1);
	}
	return (int)oldbrk;
}
#undef incr

/**************************************
 SYSCALL waitpid(int pid, int *statloc, int options);
**************************************/
#define pid (int)udata.u_argn0
#define statloc (int *)udata.u_argn1
#define options (int)udata.u_argn0
int sys_waitpid(void)
{
	register ptptr p;

#if 1 /* Nick */
	if (!valadr((char *)statloc, sizeof(int))) {
#else
	if (statloc > (int *)UZIXBASE) {
#endif
		UERR = EFAULT;
		goto Err;
	}
	di_absolute();
	/* See if we have any children. */
	p = ptab;
	while (p < ptab + PTABSIZE) {
		if ((uchar)(p->p_status) != P_EMPTY &&
		    p->p_pptr == udata.u_ptab &&
		    p != udata.u_ptab) {
			ei_absolute();
			return dowait();
		}
		++p;
	}
	UERR = ECHILD;
	ei_absolute();
Err:	return -1;
}
#undef pid
#undef statloc
#undef options

/**************************************
 SYSCALL exit(int val);
**************************************/
#define val (int)udata.u_argn0
int sys__exit(void)
{
	doexit(val, 0);
	return -1;
}
#undef val



#if 1 /* Nick */
/* Subject of change in version 1.3h */
doexit (val, val2)
int16 val;
int16 val2;
{
    register int16 j;
    register ptptr p;

#if DEBUG >= 2
    dprintf(2, "process %d exiting\n", udata.u_ptab->p_pid);
#endif

    di();

    for (j=0; j < UFTSIZE; ++j)
    {
#if 1 /* Nick, to sidestep signed vs unsigned char issues */
	if (freefileentry(udata.u_files[j]) == 0)
#else
        if (udata.u_files[j] >= 0)      /* Portable equivalent of == -1 */
#endif
            doclose (j);
    }

    sys_sync();                         /* Not necessary, but a good idea. */

    udata.u_ptab->p_exitval = (val<<8) | (val2 & 0xff);

    i_deref (udata.u_root);
    i_deref (udata.u_cwd);

      /* Stash away child's execution tick counts in process table,
       * overlaying some no longer necessary stuff.
       */
    addtick (&udata.u_utime, &udata.u_cutime);
    addtick (&udata.u_stime, &udata.u_cstime);
    bcopy (&udata.u_utime, &(udata.u_ptab->p_wait), 2 * sizeof(time_t));

    /* See if we have any children. Set child's parents to our parent */
    for (p=ptab; p < ptab+PTABSIZE; ++p)
    {
        if (p->p_status && p->p_pptr == udata.u_ptab && p != udata.u_ptab)
            p->p_pptr = udata.u_ptab->p_pptr;
    }
    ei();

        /* Wake up a waiting parent, if any. */
    wakeup ((char *)udata.u_ptab->p_pptr);

    udata.u_ptab->p_status = P_ZOMBIE;

    swapin (getproc());
    panic ("doexit:won't exit");
}
#endif



/**************************************
 SYSCALL fork(void);
**************************************/
int sys_fork(void)
{
	return dofork();
}

/**************************************
 SYSCALL pause(void);
**************************************/
int sys_pause(void)
{
	psleep(NULL);	/* P_PAUSE */
	UERR = EINTR;
	return (-1);
}

#if 1 /* Nick */
/****************************************
 SYSCALL signal(int sig, void (*func)());
****************************************/
#define sig (signal_t)udata.u_argn0
#define func *((long *)&udata.u_argn1)
int sys_signal(void)
{
	long retval = -1;

	di_absolute();
	/* SIGKILL & SIGSTOP can't be caught !!!*/
	if (sig == __NOTASIGNAL ||
	   sig == SIGKILL ||
	   ((unsigned)sig) > NSIGS) {
		UERR = EINVAL;
		goto Err;
	}
	if (func == SIG_IGN)
		udata.u_ptab->p_ignored |= sigmask(sig);
	else {
#if 0 /* Nick removed this for IAR compiler... now back in again... OUT!! */
		if ((sig_t)func != SIG_DFL && ((char *)func) < PROGBASE) {
			UERR = EFAULT;
			goto Err;
		}
#endif
		udata.u_ptab->p_ignored = udata.u_ptab->p_ignored & ~(sigmask(sig));
	}
	retval = (long)udata.u_sigvec[sig-1];
	udata.u_sigvec[sig-1] = (void (*)(signal_t))func;
Err:	ei_absolute();
	udata.u_retval1 = ((int *)&retval)[1];
	return ((int *)&retval)[0];
}
#undef sig
#undef func
#else
/****************************************
 SYSCALL signal(int sig, void (*func)());
****************************************/
#define sig (signal_t)udata.u_argn0
#define func (void (*)(signal_t))udata.u_argn1
int sys_signal(void)
{
	register int retval = -1;
#if 1 /* Nick... totally temporary... due to internal error in IAR compiler */
	int (*p)();
#endif

	di_absolute();
	/* SIGKILL & SIGSTOP can't be caught !!!*/
	if (sig == __NOTASIGNAL ||
	   (uchar)sig == SIGKILL ||
	   (uchar)sig > NSIGS) {
		UERR = EINVAL;
		goto Err;
	}
#if 1 /* Nick... totally temporary... due to internal error in IAR compiler */
	p = func;
	if (p == SIG_IGN)
#else
	if (func == SIG_IGN)
#endif
		udata.u_ptab->p_ignored |= sigmask(sig);
	else {
#if 0 /* Nick removed this for IAR compiler... now back in again... OUT!! */
		if ((sig_t)func != SIG_DFL && ((char *)func) < PROGBASE) {
			UERR = EFAULT;
			goto Err;
		}
#endif
		udata.u_ptab->p_ignored = udata.u_ptab->p_ignored & ~(sigmask(sig));
	}
	retval = (int)udata.u_sigvec[sig-1];
	udata.u_sigvec[sig-1] = func;
Err:	ei_absolute();
	return (retval);
}
#undef sig
#undef func
#endif

/**************************************
 SYSCALL kill(int pid, int sig);
**************************************/
#define pid (int)udata.u_argn0
#define sig (int)udata.u_argn1
int sys_kill(void)
{
	register ptptr p;

	if (sig == __NOTASIGNAL || (uchar)sig >= NSIGS || pid <= 1) {
		UERR = EINVAL;
Err:		return -1;
	}
	if ((p = findprocess(pid)) == NULL) goto Err;
	sendsig(p, sig);
	return 0;
}
#undef pid
#undef sig

/********************************
 SYSCALL alarm(uint secs);
********************************/
#define secs (uint)udata.u_argn0
int sys_alarm(void)
{
	register int retval;

	di_absolute();
	retval = udata.u_ptab->p_alarm;
	udata.u_ptab->p_alarm = secs;
	ei_absolute();
	return retval;
}
#undef secs

/********************************
 SYSCALL reboot(char p1, char p2);
********************************/
#define p1 (char)udata.u_argn0
#define p2 (char)udata.u_argn1
int sys_reboot(void)
{
#if 1 /* Nick */
	if (super() == 0)
		{
		udata.u_error = EPERM;
		return -1;
		}
#endif

	if (p1 == 'm' && p2 == 'e')
 {
#ifndef UTIL /* Nick */
		while (sdevs[1].swr1 & 1)
			; /* wait for tx on SERIAL 1 to finish */
#endif
		abort(0); /* Nick _abort */
 }

#if 1 /* Nick */
	udata.u_error = EFAULT;
	return -1;
#else
	return EFAULT;
#endif
}
#undef p1
#undef p2

/***********************************
 SYSCALL getset(int what, int parm1 ...);
***********************************/
#define what (int)udata.u_argn0
#define parm1 (int)udata.u_argn1
#define parm2 (int)udata.u_argn2
int sys_getset(void)
{
	register int old;
	register ptptr p;
	register char nice;

	switch (what) {
	case GET_PID:
		return (udata.u_ptab->p_pid);
	case GET_PPID:
		return (udata.u_ptab->p_pptr->p_pid);
	case GET_UID:
		return (udata.u_ptab->p_uid);
	case SET_UID:
		if (super() || udata.u_ptab->p_uid == parm1) {
			udata.u_ptab->p_uid = parm1;
			udata.u_euid = parm1;
			goto Ok;
		}
		break;
	case GET_EUID:
		return (udata.u_euid);
	case GET_GID:
		return (udata.u_gid);
	case SET_PRIO:
		/* parm1 = PID, parm2 = nice value */
		if ((p = findprocess(parm1)) == NULL) goto Err;
#if 1 /* Nick */
		nice = int_min((char)parm2, PRIO_MAX);
#else
		nice = Min((char)parm2, PRIO_MAX);
#endif
		if (nice < PRIO_MIN) nice = PRIO_MIN;
		if (!super()) if (nice < 0) nice = 0;
		p->p_nice = nice;
		p->p_cprio = (nice < 1) ? 
			(((-nice)*(TICKSPERSEC - MAXTICKS)) / (- PRIO_MIN)) + MAXTICKS
			:
			1 + (((PRIO_MAX - nice) * MAXTICKS) / PRIO_MAX);
		goto Ok;
	case GET_EGID:
		return (udata.u_egid);
	case GET_PRIO:
		return (udata.u_ptab->p_cprio); /* must be p_prio */
	case SET_GID:
		if (super() || udata.u_gid == parm1) {
			udata.u_gid = parm1;
			udata.u_egid = parm1;
			goto Ok;
		}
		break;
	case SET_UMASK:
		old = udata.u_mask;
		udata.u_mask = parm1 & S_UMASK;
		return old;
	case SET_TRACE:
#if DEBUG
		old = udata.u_traceme;
		udata.u_traceme = parm1;
		return old;
#else
		goto Ok;
#endif
	case SET_DEBUG:
#if DEBUG
		old = udata.u_debugme;
		udata.u_debugme = parm1;
		return old;
#else
		goto Ok;
#endif
	}
	UERR = EPERM;
Err:	return (-1);
Ok:	return 0;
}
#undef what
#undef parm1
#undef parm2
#undef nice

#else
/***********************************
 SYSCALL getset(int what, int parm);
***********************************/
#define what (int)udata.u_argn0
#define parm (int)udata.u_argn1
int sys_getset(void)
{
	int old;

	switch (what) {
	case SET_UMASK:
		old = udata.u_mask;
		udata.u_mask = parm & S_UMASK;
		return old;
	}
	UERR = EPERM;
	return (-1);
}
#undef what
#undef parm

#endif	/* __KERNEL__ */
