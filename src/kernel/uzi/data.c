/***************************************************************
   UZI (Unix Z80 Implementation) Kernel:  data.c
----------------------------------------------------------------
 Adapted from UZI By Doug Braun, and UZI280 by Stefan Nitschke
            Copyright (C) 1998 by Harold F. Bower
       Portions Copyright (C) 1995 by Stefan Nitschke
****************************************************************/
/* Revisions:
 */

#include "vendor.h" /* Nick, must come first */
#include <unix.h>
#include <config.h>
#include <scall1.h> /* prototypes added by Nick */
#include <scall2.h> /* prototypes added by Nick */
#include <scall3.h> /* prototypes added by Nick */
#define MAIN

#include <extern.h>         /* This declares all the tables, etc */

int stdin, stdout, stderr;  /* Necessary for library, but never referenced */

/* Dispatch table for system calls */
#if 0 /* Nick UZIX compatible */
int sys_access(void);
int sys_alarm(void);
int sys_brk(void);
int sys_chdir(void);
int sys_chmod(void);
int sys_chown(void);
int sys_close(void);
int sys_getset(void);
int sys_dup(void);
int sys_dup2(void);
int sys_execve(void);
int sys__exit(void);
int sys_fork(void);
int sys_fstat(void);
int sys_getfsys(void);
int sys_ioctl(void);
int sys_kill(void);
int sys_link(void);
int sys_mknod(void);
int sys_mount(void);
int sys_open(void);
int sys_pause(void);
int sys_pipe(void);
int sys_read(void);
int sys_sbrk(void);
int sys_seek(void); /* int sys_lseek(void); */
int sys_signal(void);
int sys_stat(void);
int sys_stime(void);
int sys_sync(void);
int sys_time(void);
int sys_times(void);
int sys_umount(void);
int sys_unlink(void);
int sys_utime(void);
int sys_waitpid(void);
int sys_write(void);
int sys_reboot(void);
int sys_symlink(void);
int sys_chroot(void);
#endif

#if 1 /* Nick UZIX compatible */
int (*disp_tab[])() =
	{
	sys_access,
	sys_alarm,
	sys_brk,
	sys_chdir,
	sys_chmod,
	sys_chown,
	sys_close,
	sys_getset,
	sys_dup,
	sys_dup2,
	sys_execve,
	sys__exit,
	sys_fork,
	sys_statfstat, /* sys_fstat, */
	sys_getfsys,
	sys_ioctl,
	sys_kill,
	sys_link,
	sys_mknod,
	sys_mountumount, /* mount, */
	sys_open,
	sys_pause,
	sys_pipe,
	sys_readwrite, /* sys_read, */
	sys_sbrk,
	sys_lseek, /* sys_seek, */
	sys_signal,
	sys_statfstat, /* sys_stat, */
	sys_stime,
	sys_sync,
	sys_time,
	sys_times,
	sys_mountumount, /* sys_umount, */
	sys_unlink,
	sys_utime,
	sys_waitpid,
	sys_readwrite, /* sys_write, */
	sys_reboot,
	sys_symlink,
	sys_chroot
#if 1 /* Nick free bitmap */
	,sys_falign
#endif
	};
#else
extern int
        sys__exit(),
        sys_open(),
        sys_close(),
        sys_creat(),
        sys_mknod(),
        sys_link(),
        sys_unlink(),
        sys_read(),
        sys_write(),
        sys_seek(),
        sys_chdir(),
        sys_sync(),
        sys_access(),
        sys_chmod(),
        sys_chown(),
        sys_stat(),
        sys_fstat(),
        sys_dup(),
        sys_getpid(),
        sys_getppid(),
        sys_getuid(),
        sys_umask(),
        sys_getfsys(),
        sys_execve(),
        sys_wait(),
        sys_setuid(),
        sys_setgid(),
        sys_time(),
        sys_stime(),
        sys_ioctl(),
        sys_brk(),
        sys_sbrk(),
        sys_fork(),
        sys_mount(),
        sys_umount(),
        sys_signal(),
        sys_dup2(),
        sys_pause(),
        sys_alarm(),
        sys_kill(),
        sys_pipe(),
        sys_getgid(),
        sys_times(),
        sys_utime();
        sys_geteuid();
        sys_getegid();

int (*disp_tab[])() =
{       sys__exit,
        sys_open,
        sys_close,
        sys_creat,
        sys_mknod,
        sys_link,
        sys_unlink,
        sys_read,
        sys_write,
        sys_seek,
        sys_chdir,
        sys_sync,
        sys_access,
        sys_chmod,
        sys_chown,
        sys_stat,
        sys_fstat,
        sys_dup,
        sys_getpid,
        sys_getppid,
        sys_getuid,
        sys_umask,
        sys_getfsys,
        sys_execve,
        sys_wait,
        sys_setuid,
        sys_setgid,
        sys_time,
        sys_stime,
        sys_ioctl,
        sys_brk,
        sys_sbrk,
        sys_fork,
        sys_mount,
        sys_umount,
        sys_signal,
        sys_dup2,
        sys_pause,
        sys_alarm,
        sys_kill,
        sys_pipe,
        sys_getgid,
        sys_times,
        sys_utime,
        sys_geteuid,
        sys_getegid
 };
#endif

int ncalls = sizeof (disp_tab) / sizeof (int(*)());

