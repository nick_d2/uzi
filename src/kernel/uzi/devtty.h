/* devtty.h for UZI180 by Nick (we are converting everything to ANSI C) */

#ifndef __DEVTTY_H
#define __DEVTTY_H

#define NTTYS 9		/* Number of TTY ports defined */
#define TTYSIZ 132	/* Character Input Queue size */

#define	PORTS 13	/* total number of serial or apibus ports */
#define XMIT_SIZE 256	/* size of transmitter buffer */
#define RECV_SIZE 256	/* size of reciever buffer */

#define TIOCGETP  0
#define TIOCSETP  1
#define TIOCSETN  2
#define TIOCEXCL  3     /** currently not implemented  SN **/
#define UARTSLOW  4     /* Normal interrupt routine (UZI280) */
#define UARTFAST  5     /* Fast interrupt routine for modem usage (UZI280) */
#define TIOCFLUSH 6
#define TIOCGETC  7
#define TIOCSETC  8
              /* UZI280 extensions used by UZI180 in the CP/M 2.2 Emulator */
#define TIOCTLSET 9     /* Don't parse ctrl-chars */
#define TIOCTLRES 10    /* Normal Parse */

#define XTABS   0006000
#define UNBUFF  0000100 /* Nick */
#define RAW     0000040
#define CRMOD   0000020
#define ECHO    0000010
#define LCASE   0000004
#define CBREAK  0000002
#define COOKED  0000000

#define DFLT_MODE  RAW /* XTABS | CRMOD | ECHO | COOKED */

#define CTRL(c)  (c & 0x1f)

struct s_tty_data {
    char t_ispeed;
    char t_ospeed;
    char t_erase;
    char t_kill;
    int  t_flags;

    char t_intr;
    char t_quit;
    char t_start;
    char t_stop;
    char t_eof;

    char ctl_char;
};

int tty_read(uchar minor, uchar rawflag);
int tty_write(uchar minor, uchar rawflag);
int tty_open(uchar minor);
int tty_close(uchar minor);
int tty_ioctl(uchar minor, int request, char *data);
void tty_inproc(int minor, char c);
void tty_inproc_dummy(int minor, char c);
void echo(int minor, char c);

extern struct s_tty_data tty_data[NTTYS+1];	/* tty_data[0] is not used */
extern void (*tty_vector[NTTYS+1])(int minor, char c);	/* [0] is not used */

#endif /* __DEVTTY_H */

