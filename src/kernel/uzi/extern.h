/**************************************************
UZI (Unix Z80 Implementation) Kernel:  extern.h
***************************************************/
/* These are the global data structures */

#ifdef MAIN
#define extern
#endif

/* Nick #undef UTIL */	/* Change this to define UTIL if Not running kernel */

/* This is accessed by the macro udata which is really ub.u_d
 * for the running system, _ub is defined in MACHASM.ASZ, but
 * it needs to be here for the utilities that run under CP/M.
 */
/* Nick #ifndef UTIL */
/* Nick #ifndef MAIN */
#if defined(UTIL) || !defined(MAIN) /* Nick */
#if 1 /* Nick */
extern struct s_ublock ub;
#else
extern struct u_block ub;
#endif
#endif /* Nick */
/* Nick #endif */
/* Nick #endif */

#define udata ub.u_d

#if 1 /* Nick UZIX compatible */
extern struct s_ptab ptab[PTABSIZE];
#else
extern struct p_tab ptab[PTABSIZE];
#endif

#if 1 /* Nick UZIX compatible */
extern inoptr root_ino;		/* Address of root dir in inode table */
extern dev_t root_dev;		/* Device number of root filesystem. */
#else
extern inoptr root;     /* Address of root dir in inode table */
extern int16 ROOTDEV;   /* Device number of root filesystem. */
#endif

#if 1 /* Nick UZIX compatible */
extern struct s_cinode i_tab[ITABSIZE];  /* In-core inode table */
#else
extern struct cinode i_tab[ITABSIZE];    /* In-core inode table */
#endif
extern struct oft of_tab[OFTSIZE];       /* Open File Table */

#if 1 /* Nick UZIX compatible */
extern struct s_filesys fs_tab[NFS];     /* Table entry for each
                                            device with a filesystem. */
#else
extern struct filesys fs_tab[NDEVS];     /* Table entry for each
                                            device with a filesystem. */
#endif
#if 1 /* Nick UZIX compatible */
extern struct s_blkbuf bufpool[NBUFS];
extern uchar dirty_mask;	/* 1 if buffer writing delayed, else 0 */
#else
extern struct blkbuf bufpool[NBUFS];
#endif

extern ptptr initproc;  /* The process table address of the first process. */
extern int16 inint;     /* flag is set whenever interrupts are being serviced */
extern char unix_locked_out; /* how many kernel ei() / di() blocks entered */

extern int16 sec;       /* Tick counter for counting off one second */
extern int16 runticks;  /* Number of ticks current process has been
                           swapped in */

extern uzitime_t tod;      /* Time of day */
extern uzitime_t ticks;    /* Cumulative tick counter, in minutes and ticks  */

#if 1 /* Nick UZIX compatible */
extern uchar total;	/* Total available memory 16k pages */
#endif

extern char *swapbase;  /* Used by device driver for swapping */
extern unsigned swapcnt;
extern blkno_t swapblk;

extern uint16 waitno;   /* Serial number of processes entering wait state */

#ifdef DEBUG
extern char traceon;	/* Nick to enable/disable tracing via keyboard */
#endif

#if 1 /* Nick UZIX compatible */
/* release buffer */
#define brelse(bp)	bfree(bp, 0)

/* free buffer and mark them dirty for eventually writing */
#define bawrite(bp)	bfree(bp, 1)

/* do all buffers free without flushing */
#define	bufinit()	bzero(bufpool, NBUFS*sizeof(blkbuf_t))

/* shortcuts for block device read/write */
#define bdread(bp)	bdreadwrite(bp, 0)
#define bdwrite(bp)	bdreadwrite(bp, 1)

/* shortcuts for character device read/write */
#define cdread(dev)	cdreadwrite(dev, 0)
#define cdwrite(dev)	cdreadwrite(dev, 1)

/* shortcuts for device open/close */
#define d_open(dev)	d_openclose(dev, 1)
#define d_close(dev)	d_openclose(dev, 0)

/* shortcuts for access to buffer pool by clients */
#define tmpbuf()	zerobuf(1); /* not allowed to return NULL */

/* shortcuts for access to file data via an open inode */
#define readi(x) 	readwritei(0, x)
#define writei(x)	readwritei(1, x)

#define freefileentry(e)	((e) & 0x80)
#endif

#ifdef MAIN
#undef extern
#endif

#ifdef UTIL /* Nick */
#define kprintf printf
#define dprintf /* parameters will be evaluated, to no effect */
#define kputchar putchar
#define _putc putchar
#define uput(sptr, uptr, nbytes) (bcopy((sptr), (uptr), (nbytes)))
#define uget(uptr, sptr, nbytes) (bcopy((uptr), (sptr), (nbytes)))
#define uputw(w, uptr) (*(int *)(uptr) = (w))
#define ugetw(uptr) (*(int *)(uptr))
#define uputc(c, uptr) (*(char *)(uptr) = (c))
#define ugetc(uptr) (*(char *)(uptr))
#define ugets(uptr, sptr, count) (strcpy((sptr), (uptr)), 0) /* success */
#define ei()
#define di()
#define ei_absolute()
#define di_absolute()
#define abyte()
/* #define d_init() 0 */ /* pretends success (please look into this!!.. OK!) */
#define lpout()
/* #define rdtod() */
#define sys_execve()
#define swapout()
#define swapin()
#define dofork()
#endif

