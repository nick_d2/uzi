/***************************************************************
   UZI (Unix Z80 Implementation) Kernel:  devmisc.c
----------------------------------------------------------------
 Adapted from UZI By Doug Braun, and UZI280 by Stefan Nitschke
            Copyright (C) 1998 by Harold F. Bower
       Portions Copyright (C) 1995 by Stefan Nitschke
****************************************************************/
/* Revisions:
 * 25.12.97 - lpout moved to machine-dependent module. HFB
 */

#include "vendor.h" /* Nick, must come first */
#include <unix.h>
#include <config.h>
#include <extern.h>
#include <devmisc.h> /* prototypes added by Nick */

int mem_read(uchar minor, uchar rawflag)
{
    unsigned addr;

    addr = udata.u_offset; /* Nick 512*udata.u_offset.o_blkno+udata.u_offset.o_offset; */

    if (addr < 0x006C)
        return 1;

    if (udata.u_sysio)
        bcopy ((char *)addr, udata.u_base, udata.u_count);
    else
        uput ((char *)addr, udata.u_base, udata.u_count);

    return udata.u_count;
}


int mem_write(uchar minor, uchar rawflag)
{
    unsigned addr;

    addr = udata.u_offset; /* Nick 512*udata.u_offset.o_blkno+udata.u_offset.o_offset; */

    if (addr < 0x006C)
        return 1;

    if (udata.u_sysio)
        bcopy (udata.u_base, (char *)addr, udata.u_count);
    else
        uput (udata.u_base, (char *)addr, udata.u_count);

    return udata.u_count;
}



int null_write(uchar minor, uchar rawflag)
{
    return udata.u_count;
}



static char lop = 0;

int lpr_open(uchar minor)
{
    lop = 1;
    return 0;
}


int lpr_close(uchar minor)
{
    if (lop)
    {
        lop  = 0;
/*7Jul98        lpout('\f');**/
    }
    return 0;
}



int lpr_write(uchar minor, uchar rawflag)
{
    unsigned n;
    unsigned char  c;
/* Nick    int  ugetc(); */

    n = udata.u_count;
    while (n--) {
        if (udata.u_sysio)
            c = *udata.u_base++;
        else
            c = ugetc (udata.u_base++);
        lpout (c);
    }
    return udata.u_count;
}

/* NOTE: lpout moved to Machine-dependent Assembly Module - HFB */

/* #include "devmt.c" */

/***************************************************************
   UZI (Unix Z80 Implementation) Kernel:  devmisc.c
   Magnetic Tape, Unimplemented.  Currently a Null Device.
----------------------------------------------------------------
 Adapted from UZI By Doug Braun, and UZI280 by Stefan Nitschke
            Copyright (C) 1998 by Harold F. Bower
       Portions Copyright (C) 1995 by Stefan Nitschke
****************************************************************/
/* Revisions:
 */

int mt_read(uchar minor, uchar rawflag)
{
    return -1;
}

int mt_write(uchar minor, uchar rawflag)
{
    return -1;
}

int mt_open(uchar minor)
{
    return -1;
}

int mt_close(uchar minor)
{
    return 0;
}

