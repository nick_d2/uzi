/* devmisc.h for UZI180 by Nick (we are converting everything to ANSI C) */

#ifndef __DEVMISC_H
#define __DEVMISC_H

int mem_read(uchar minor, uchar rawflag);
int mem_write(uchar minor, uchar rawflag);
int null_write(uchar minor, uchar rawflag);
int lpr_open(uchar minor);
int lpr_close(uchar minor);
int lpr_write(uchar minor, uchar rawflag);
int mt_read(uchar minor, uchar rawflag);
int mt_write(uchar minor, uchar rawflag);
int mt_open(uchar minor);
int mt_close(uchar minor);

#endif /* __DEVMISC_H */

