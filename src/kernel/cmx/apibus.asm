; apibus.asm
; Interrupt driven driver for communications with/via the WPO controller

; -----------------------------------------------------------------------------

$ io64180.inc

	extern	_sdevs
	extern	_K_OS_Intrp_Entry
	extern	_K_OS_Intrp_Exit
	extern	?BANK_CALL_DIRECT_L08
	extern	?BANK_FAST_LEAVE_L08
	extern	abyte

ADIV	equ	2	; clocked serial i/o rate (power of 2, higher = slower)
ADVMAX	equ	9	; number of api bus devices (controls round robin alg.)

XMIT_SIZE equ	256	; must match cxfuncs.h, size of transmitter buffer
RECV_SIZE equ	256	; must match cxfuncs.h, size of reciever buffer

; must match cxfuncs.h, fields in SDEV structure:  ** ASSUMES 3 BYTE CODE PTRS!
axmit	equ	0			; offset of embedded CMX XMIT structure
atxcnt	equ	axmit+4			; CMX name is 'word16 bytes_in'
arecv	equ	13			; offset of embedded CMX RECV structure
arxcnt	equ	arecv+4			; CMX name is 'word16 bytes_out'
aport	equ	13+13
atxvec	equ	13+13+1			; called by interrupt, returns l=chr
arxvec	equ	13+13+4			; called by interrupt, passed c=chr
;smsvec	equ	13+13+7			; called by interrupt, passed c=bits
;sesvec	equ	13+13+10		; called by interrupt, passed c=bits
;stevec	equ	13+13+13		; vector to start asci/escc transmitter
;stdvec	equ	13+13+16		; vector to stop asci/escc transmitter
;sstat	equ	13+13+19		; address to read status (8530 02xxh)
;stxdr	equ	13+13+20		; address to write tx data (8530 02xxh)
;srxdr	equ	13+13+21		; address to read rx data (8530 02xxh)
;swr1	equ	13+13+22		; wr1 contents (interrupt enable bits)
;swr5	equ	13+13+23		; wr5 contents (modem control outputs)
;swr15	equ	13+13+24		; wr15 contents (modem control int ena)
;sinint	equ	13+13+25		; says whether wr1 really reflects wr1!
sdvlen	equ	13+13+26		; byte size of block per serial port

; -----------------------------------------------------------------------------

	rseg	CODE

	public	_apibus_setup

_apibus_setup::
	push	bc
	push	de

	ld	a,1
	di
	ld	(arobin),a
	ld	(arxovr),a

	in0	a,(ITC)
	or	INT1SW
	out0	(ITC),a
	ei

	pop	de
	pop	bc
	jp	?BANK_FAST_LEAVE_L08

; -----------------------------------------------------------------------------

	public	_apibus_priority

_apibus_priority::
if 0 ; 08apr03 patch to avoid BAD COMMAND error on TPG hardware
	; enter with e = priority value to be set
	; upper nibble is priority (1-7), lower nibble is device (1-8)

	; if ROM version of terminal is < 5.544, do nothing and return (todo)
	; if a priority command for the same device is waiting, override it
	; otherwise, wait for any previous priority command to be sent

	push	bc
	push	de

wait_for_tx:
	ei
	ld	hl,_acflag

;wait_for_tx_entry:
	di
	ld	a,(hl)			; see if any command is waiting
	or	a
	jr	z,set_priority		; no command is waiting
	jp	m,wait_for_tx		; some other command eg. reboot = 0aah

	xor	e			; priority command is waiting
	and	0fh			; check for matching lower nibble
	jr	nz,wait_for_tx		; it's for some other device
					; it's for the same device, override it
set_priority:
	ld	(hl),e
	ei

	pop	de
	pop	bc
endif
	jp	?BANK_FAST_LEAVE_L08

; -----------------------------------------------------------------------------

	rseg	RCODE

	public	_int1_vector

_int1_vector::
 .if 1
	call	_K_OS_Intrp_Entry
 .else
	push	af
	push	bc
	push	de
	push	hl
	push	iy
 .endif

 .if 1
	ld	hl,LWRD aint1
	ld	a,BYTE3 aint1
	call	?BANK_CALL_DIRECT_L08
 .else
	call	aint1
 .endif

 .if 1
	call	_K_OS_Intrp_Exit
 .else
	pop	iy
	pop	hl
	pop	de
	pop	bc
	pop	af
	ei
	ret
 .endif

	public	_csio_vector

_csio_vector::
 .if 1
	call	_K_OS_Intrp_Entry
 .else
	push	af
	push	bc
	push	de
	push	hl
	push	iy
 .endif

 .if 1
	ld	hl,LWRD acsint
	ld	a,BYTE3 acsint
	call	?BANK_CALL_DIRECT_L08
 .else
	call	acsint
 .endif

 .if 1
	call	_K_OS_Intrp_Exit
 .else
	pop	iy
	pop	hl
	pop	de
	pop	bc
	pop	af
	ei
	ret
 .endif

; -----------------------------------------------------------------------------

	rseg	CODE

public	aint1

aint1:	sub	a
	ld	(astate),a

public acsint

acsint:
 .if 1
	in0	a,(ITC)
	and	.binnot.INT1SW
	out0	(ITC),a			; disable further INT1 interrupts

	in0	a,(CNTR)
	and	10111111b
	out0	(CNTR),a		; disable further CSI/O interrupts

	ei
 .endif

 .if 0
	push	iy
 .endif

	ld	hl,astate
	ld	a,(hl)
	dec	a
	jp	z,LWRD astat1
	dec	a
	jr	z,astat2
	dec	a
	jr	z,astat3
	dec	a
	jp	z,LWRD astat4
	dec	a
	jp	z,LWRD astat5
	dec	a
	jp	z,LWRD astat6
	dec	a
	jp	z,LWRD astat7

astat0:	; start sending address command
 .if 1 ; 09mar03 can now send priority setting commands via this flag
	ld	hl,_acflag
	ld	a,(hl)			; get command to be sent (if any)
	ld	(hl),0			; clear the command for next time
	or	a
	jp	nz,LWRD acstx		; send command (INT1 is still on!!)
 .else
	ld	a,(_abflag)
	or	a
	jp	nz,LWRD acstx		; reboot command
 .endif

	ld	hl,astate
	ld	(hl),1			; set astate = 1

	ld	a,(adirn)
	rrca
	jr	c,aadrrx

aadrtx:
 ;ld a,2
 ;ld (arobin),a
	ld	a,(arobin)		; address select
; call abyte
	jp	LWRD acstx		; send address command

aadrrx:	sub	a			; address enquiry
; call abyte
	jp	LWRD acstx		; send address command

 .if 0 ; this should be reinstated when we desire reboot control
acmdtx:
 ; it's not working for some reason... but the abyte does come out!
 call abyte
	out	(TRDR),a		; send command byte provided by client

	ld	a,10h+ADIV		; csi/o master, divisor, te=1, eie=0
 jp LWRD acstmr ;	jr	acstmr
 .endif

;astat1: ; start receiving address response
;	inc	(hl)			; set astate = 2
;	jr	acsrx

astat2:	; received address response, inspect
	inc	(hl)			; set astate = 3

	in0	a,(TRDR)
; ld e,a
; or 80h
; call abyte
; ld a,e
	or	a			; selected some device?
	jp	z,LWRD acsnul		; no device available, go and flip dir

	ld	(adevok),a

astat3:	; start sending data size command
	inc	(hl)			; set astate = 4

	ld	a,(adevok)
	call	LWRD aindex		; iy -> _sdevs+(a+3)*sdvlen

	ld	a,(adirn)
	rrca
	jr	c,adirrx

adirtx:	; ask to transmit anything waiting
	ld	l,(iy+atxcnt)
	ld	h,(iy+atxcnt+1)		; hl = bytes waiting to be sent

	ld	a,l			; a = bytes waiting to be sent
	ld	de,80h
	or	a
	sbc	hl,de
	jr	c,$+4
	ld	a,7fh			; a = maximum possible transfer count

	ld	(acount),a		; save data size for transfer
; call abyte
	jp	LWRD acstx		; send data size command

adirrx:	; ask to receive anything waiting
	ld	e,(iy+arxcnt)
	ld	d,(iy+arxcnt+1)
 .if 1
	ld	hl,RECV_SIZE
 .else
	ld	l,(iy+arxsiz)
	ld	h,(iy+arxsiz+1)
 .endif
	or	a
	sbc	hl,de			; hl = available rx buffer bytes

	ld	a,l			; a = available rx buffer bytes
	ld	de,80h
	or	a
	sbc	hl,de
	jr	c,$+4
	ld	a,7fh			; a = maximum possible transfer count

	or	e			; say we want to receive
	ld	(acount),a		; save data size for transfer
; call abyte
	jp	LWRD acstx		; send data size command

astat1: ; start receiving address response
astat4:	; start receiving data size response
	inc	(hl)			; set astate = 5

 .if 1	; 01oct02 ensures ef gets reset to 0 (we don't really need to access
	; trdr when changing from sending to receiving, except to reset ef!!)
	in0	a,(TRDR)
 .endif
	jr	acsrx

astat5:	; received data size response, start transfer
	inc	(hl)			; set astate = 6

	in0	a,(TRDR)
; call abyte
;;; or a
;;; ld e,90h+1 ; because wpov1.asm, etc, have only $10 bytes receive buffer
;;; jp m,bumx
;;; ld e,40h+1 ; because wpov1.asm, etc, have only $40 bytes transmit buffer
;;;bumx:
;;; cp e
;;; jr c,bumxx ; our crude heuristic says it's a valid data size response
;;; ld e,a
;;; ld a,'$'
;;; call abyte
;;; ld a,e
;;;bumxx:
	ld	hl,acount
	xor	(hl)			; test for wrong direction of response
	jp	m,LWRD areset
	xor	(hl)			; restore original data size response
; ld e,a
; ld a,(adevok)
; cp 7
; ld a,e
; call z,abyte
	cp	(hl)
	jr	nc,$+3
	ld	(hl),a			; acount = lower of our max / slave max

	ld	a,(hl)
; call abyte
	and	7fh			; test for null transfer
	jr	z,acsnul		; yes, go and flip direction
	xor	(hl)			; test transfer direction
	jp	m,LWRD acstst		; we need to receive, start

	ld	hl,astate
	inc	(hl)
 jp LWRD astat7 ;	jr	astat7			; we need to transmit, start

astat6:	; received data byte
 .if 1
	ld	a,(adevok) ;2
	call	LWRD aindex		; iy -> _sdevs+(a+3)*sdvlen

	in0	c,(TRDR)		; c = 2nd parameter (byte)
	push	iy
	pop	de			; de = 1st parameter (word16)

	ld	l,(iy+arxvec)
	ld	h,(iy+arxvec+1)
	ld	a,(iy+arxvec+2)
	call	LWRD ?BANK_CALL_DIRECT_L08
 .else
	wcalld	ainput			; process received data byte
 .endif

acstst:	ld	hl,acount
	dec	(hl)			; say we received 1 byte
	jp	p,LWRD acsrdn		; if there is no more to receive

 .if 1 ; 09mar03 new sticky count at SYS level, similar to AVR sticky count
 ld a,3
 ld (_arxsti),a ; keep trying to receive for another 5 polls, we had something
 .endif

 .if 1 ; 09mar03 experiment to increase priority of API bus activity
 ld a,15 ; for the next 15 polls,
 ld (_aturbo),a ; don't pause in between each poll, just keep hammering
 .endif

acsrx:	; 01oct02 please ensure that ef has been reset to 0 before entering
 .if 0	; try this 21mar01
	ld	a,ADIV
	out0	(CNTR),a		; csi/o master, ef=0, eie=0

	in0	a,(ITC)
	and	.binnot.INT1SW		; maskoff INT1SW
	out0	(ITC),a			; disable int1, in case it is still on

	ei
	ld	b,40h
	djnz	$			; delay for 4433 to load 1 data byte
	di
 .endif
 .if 1	; 16oct02 now back in again, tests indicate we need it.  (latest wpo)
	; 16oct02 trying this out... fingers crossed... please test old wpo!!
	ld	b,40h ;10h
	djnz	$			; delay for 4433 to load 1 data byte
 .endif

	ld	a,60h+ADIV		; csi/o master, divisor, re=1, eie=1
 jp LWRD acstmr ;	jr	acstmr

acsrdn:	ld	hl,astate
	ld	(hl),3			; start new data size command
	jp	LWRD astat3		; immediately

acsnul:	sub	a
	ld	(adirn),a		; assume we will transmit

 .if 1 ; 09mar03 new sticky count at SYS level, similar to AVR sticky count
 ld hl,_arxsti
 ld a,(hl)
 or a ; countdown currently at zero?
 jr z,rx_not_sticky ; yes, transmit has priority as usual

 dec (hl) ; count down while nothing is actually received
 jr ACSFLP ; go and flip direction to rx

rx_not_sticky:
 .endif

	ld	a,(arobin)
	ld	c,a
	ld	b,ADVMAX ; 20mar02 ;8;ADVMAX
	; this will need to be revisited, and done based on rom version
	; (no, it's ok, as long as we don't send to the 9th [internal] device)

 .if 1 ; 09mar03 new sticky count at SYS level, similar to AVR sticky count
 ld hl,_atxsti
 ld a,(hl)
 or a ; countdown currently at zero?
 jr z,ASCAN ; yes, force the robin to proceed around

 dec (hl) ; count down while nothing is actually transmitted (eg. printer!)
 jr tx_sticky_entry ; go and try the same device
 .endif

ascan:	inc	c
	ld	a,c
	cp	ADVMAX+1 ; 20mar02 ;8+1;ADVMAX+1 ; gone past maximum device?
	jr	c,$+4
	ld	c,1			; wrap output device

 .if 1 ; 09mar03 new sticky count at SYS level, similar to AVR sticky count
tx_sticky_entry:
 .endif
	ld	a,c
	call	LWRD aindex		; iy -> _sdevs+(a+3)*sdvlen

	ld	a,(iy+atxcnt)
	or	(iy+atxcnt+1)		; more data to transmit?
	jr	nz,akick		; we will transmit, start immediately

	;inc	c
	;ld	a,c
	;cp	ADVMAX+1		; gone past maximum device?
	;jr	c,$+4
	;ld	c,1			; wrap output device

	djnz	ascan
; ld a,80h
; call abyte
	jr	acsflp			; nothing to output, go and receive

akick:

 .if 1 ; 09mar03 experiment to increase priority of API bus activity
 ld a,15 ; for the next 15 polls,
 ld (_aturbo),a ; don't pause in between each poll, just keep hammering
 .endif

	ld	a,c
	ld	(arobin),a		; set the found output device
; or 80h
; call abyte

 .if 0
	ld	l,16
	ld	de,500			; delay ??
	call	timer0			; request a kick up the bum
 .endif

	ld	hl,arxovr
	dec	(hl)
	jr	nz,areset		; forced receive every so often

acsflp:	ld	hl,arxovr
	ld	(hl),10h		; set time to next rx override

	ld	hl,adirn
	inc	(hl)			; no data to send, we will receive

areset:

 .if 1 ; 09mar03 experiment to increase priority of API bus activity
	sub	a
	ld	(ASTATE),a

	ld	hl,_aturbo
	or	(hl)			; had some recent activity?
	jr	z,reset_and_delay	; no, allow AVR to initiate next poll

	dec	(hl)			; yes, count down after activity dies
	jp	ASTAT0			; and start next poll immediately

reset_and_delay:
 .endif

	ld	a,00h+ADIV		; csi/o master, divisor
 .if 1
	di
 .endif
	out0	(CNTR),a		; no more csi/o needed for now

 .if 0 ; 09mar03 experiment to increase priority of API bus activity
	sub	a
	ld	(astate),a		; start with address command
 .endif

	in0	a,(ITC)
	or	INT1SW
	out0	(ITC),a			; enable int1 to start next poll
	jr	acsret

astat7:	; start sending data byte

 .if 1 ; 09mar03 new sticky count at SYS level, similar to AVR sticky count
 ld a,3
 ld (_atxsti),a ; keep trying the same device for another 3 polls, had something
 .endif

 .if 1 ; 09mar03 experiment to increase priority of API bus activity
 ld a,15 ; for the next 15 polls,
 ld (_aturbo),a ; don't pause in between each poll, just keep hammering
 .endif

	ld	a,(adevok)
	call	LWRD aindex		; iy -> _sdevs+(a+3)*sdvlen
 .if 1
	push	iy
	pop	de			; de = 1st parameter (word16)

	ld	l,(iy+atxvec)
	ld	h,(iy+atxvec+1)
	ld	a,(iy+atxvec+2)
	call	LWRD ?BANK_CALL_DIRECT_L08

	ld	a,l			; if h = 0ffh, should ignore, but don't
 .else
	call	atxget			; read a = 1 byte from tx queue
 .endif
; call abyte

; ld e,a
;; sub a
;; out (cntlb0),a ; 115200
; ld a,(adevok)
; cp 7 ;2
; ld a,e
; call z,abyte

	ld	hl,acount
	dec	(hl)			; say we transmitted 1 byte
	jr	nz,acstx		; if there is more to send

	ld	hl,astate
	ld	(hl),3			; say we will send new data size cmd

acstx:	; 01oct02 no special ef precautions, as this trdr access clears ef:
; ld e,a
; sub a
; out (cntlb0),a ; 115200
; ld a,e
; call abyte ; to see all transmissions, including data size response, etc
	out0	(TRDR),a

	ld	a,50h+ADIV		; csi/o master, divisor, te=1, eie=1
acstmr:
 .if 0	; 16oct02 trying this out... probably safe, no need to test old wpo!!
	ld	b,0 ; 03oct02 ;40h ;10h
	djnz	$			; delay for 4433 to load 1 data byte
 .endif
 .if 1
	di
 .endif
	out0	(CNTR),a

 .if 0 ; not necessary now that it's done at ACSINT
	in0	a,(ITC)
	and	.binnot.INT1SW		; maskoff INT1SW
	out0	(ITC),a			; disable int1, poll in progress
 .endif

acsret:
 .if 0
	pop	iy
 .endif
 .if 0	; try this 01aug01
	ld	bc,ac8530
	ld	a,3			; select rr3
	out0	(c),a
	nop
	in0	a,(c)
	ret	z			; 8530 not interrupting
	push	af
	push	bc
	jp	int2e
 .else
 .if 1
	jp	?BANK_FAST_LEAVE_L08
 .else
	ret
 .endif
 .endif

; -----------------------------------------------------------------------------

public	aindex

aindex:	ld	e,a
	ld	d,sdvlen
	mlt	de

	ld	iy,_sdevs+3*sdvlen
	add	iy,de			; iy -> _sdevs+(a+3)*sdvlen
	ret

; -----------------------------------------------------------------------------

		rseg	UDATA0

acount:		defs	1
astate:		defs	1
adirn:		defs	1
arobin:		defs	1
arxovr:		defs	1
adevok:		defs	1

		public	_acflag, _atxsti, _arxsti, _aturbo

_acflag:	defs	1		; 09mar03 apibus injected command byte
_atxsti:	defs	1		; 09mar03 apibus sticky counter for tx
_arxsti:	defs	1		; 09mar03 apibus sticky counter for rx
_aturbo:	defs	1		; 09mar03 counter for continuous polls

; -----------------------------------------------------------------------------

	END
