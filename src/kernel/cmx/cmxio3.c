/*********************************************************

Copyright (c) CMX Company. 1993-1995. All rights reserved

*********************************************************/
/* version 3.00 */

#include <cxfuncs.h>
#include <cxextern.h>	/* get cmx include header file */
/* Nick #include <io64180.h>		/* get 64180 io configuration file */

/* WARNING ---- UPDATE ---- WARNING

The uart_update() function has been broken into 2 (two) different
functions, which are the K_Update_Recv function and the
K_Update_Xmit function, call respectively by the reciever
and transmitter interrupt handlers

WARNING ---- UPDATE ---- WARNING */

/**************************************************************

						!!! CAUTION !!!

It is up to the interrupt handler that either calls the K_Update_Recv
and K_Update_Xmit functions to clear or set any needed bits in the UART
registers (IF NEED BE), so as to properly handle the sending and recieving
of characters thru the UART. This may include reloading a timer, clearing
the recieve char bit, error flags, etc, depending upon the processor and
mode that the UART is operating in.

CMX does NOT do anything to any UART register bits. It will just recieve
a character and transmit a character. However it will start the transmitter
when need be, according to the XMIT_INT_ON define set below by user.

**************************************************************/

/**************************************************************
  The user MAY have to tweak some of the following code, depending
upon the processor that they are working with. This is because
there are many different ways to configure a UART and many modes
that a UART can operate under.

 Probally the most the user will have to do is to properly enable the
UART transmitter and possibly properly clear the reciever ready flag
if needed to be done by software. Also the user must tell the "C"
functions where the UART registers are located. The user will
have to at the most, possibly manipulate the following functions:

K_Init_Xmit()	initialize transmitter
K_Init_Recv()	initialize reciever

 NOTE: in most cases the processor and "C" compiler will
allow you to access the IO registers by either including
the proper *.h file or by casting the register 
to an absolute address. However their are some exceptions
to this, and if so, the user must create their own
assembly routines to properly set, clear and test bits and
to get and send a byte or word to these register.
********************************************************/
							
/**************************************************************
 The user must set these to the CPU that they are working with

#define XMIT_INT_ON ???   enable the transmitter 
#define RECV_REG 		     reciever register 
#define XMIT_REG 			  transmit register
***************************************************************/

/******************************************************
	 The following is for the 68HC12 family

********************************************************/

#if 0
#define RECV_REG RDR0	/* reciever register */
#define XMIT_REG TDR0	/* transmit register */

#define TIE	0x01	/* transmit interrupt enable bit */

#define XMIT_INT_ON() { (STAT0 |= TIE); }  /* enable transmitter. */

#define XMIT_SIZE 256	/* size of transmitter buffer */
#define RECV_SIZE 256	/* size of reciever buffer */
#endif


word16 K_I_Get_Char_Common(byte port, byte *byte_ptr, word16 cnt,
                                      word16 timecnt, byte wait);
byte K_I_Put_Char_Common(byte port, byte *byte_ptr, word16 cnt,
                                    word16 timecnt, byte wait);
byte K_I_Remove_Link(void);
void K_I_Insert_Link(word16);

/* NOTES:
 The recieve interrupt could determine that the last character came in
 and signal an event indicating this, or wake a task, etc.
*/
/* THE FOLLOWING ARE TRANSMITTER FLAGS */
#define XMIT_BUSY	0x01

/* THE FOLLOWING ARE RECIEVER FLAGS */
#define OE		0x02
#define FE		0x04
#define PE		0x08
#define FULL 		0x20
#define RECV_BUFF_ERR	0x40

#define AER_UART_BUSY 0x20	/* error code pass back to calling task */

SDEV sdevs[PORTS]; /* Nick */
byte xmit_bufs[PORTS][XMIT_SIZE]; /* Nick */
byte recv_bufs[PORTS][RECV_SIZE]; /* Nick */

void K_Init_Sdevs(void)
{
	byte i;
	SDEV *device;

	device = &sdevs[0];
	for (i = 0; i < PORTS; i++)
		{
		device->port = i;
		device->tx_vector = K_Update_Xmit;

		/* for serial devices, set the usual incoming data handler */
		/* for API BUS devices, throw away incoming data by default */
		if (i < 4)
			{
			device->rx_vector = K_Update_Recv;
			}
		else
			{
			device->rx_vector = K_Update_Dummy;
			}

		device->modem_status_vector = K_Update_Dummy;
		device->error_status_vector = K_Update_Dummy;
		device->tx_enable_vector = K_Update_Dummy;
		device->tx_disable_vector = K_Update_Dummy;

		device->xmit.buff = xmit_bufs[i];
		device->recv.buff = recv_bufs[i];

		K_Init_Xmit(device);
		K_Init_Recv(device);

		device++;
		}
}

void K_Init_Xmit(SDEV *device)
{
	/* set up BAUD rate, parity, data bits, stop bits */
	device->xmit.flag = 0x00;	/* clear flags. */
	device->xmit.head = device->xmit.tail = &device->xmit.buff[0];
}

void K_Init_Recv(SDEV *device)
{
	/* set up BAUD rate, parity, data bits, stop bits */
	/* also enable reciever, if need be. */
	device->recv.flag = 0x00;	/* clear flags. */
	device->recv.head = device->recv.tail = &device->recv.buff[0];
}

/* The following can only be called by the interrupt. No
 Parameters are passed. */
#if 1
word16
#else
void
#endif
K_Update_Xmit(SDEV *device)
{
#if 1
	byte data;
#endif

#if 0
	if (device->xmit.flag & XMIT_BUSY)	/* see if transmitter should transmit. */
		{
#endif
		if (device->xmit.bytes_out)	/* decrement count and send. */
			{
#if 1
			if (device->xmit.bytes_wanted)	/* is task waiting for number of bytes. */
				{
				if (!(--device->xmit.bytes_wanted))	/* yes, decrement and test. */
					{
					if (device->xmit.tcbptr->tcbstate & WAIT)	/* see if task waiting. */
						{
						device->xmit.tcbptr->tcbstate = RESUME;	/* yes, wake task. */
						if (device->xmit.tcbptr->priority < active_priority)
							PREEMPTED; /*cmx_flag1 |= preempted;*/	/* yes, set preempted K_I_Scheduler flag */
						}
					}
				}
#endif
#if 1
			data = *device->xmit.tail;
			if (++device->xmit.tail == &device->xmit.buff[XMIT_SIZE]) /* see if wrap. */
				device->xmit.tail = &device->xmit.buff[0];
			device->xmit.bytes_out--;
#if 0
			_asm
				or	a	; cf=0 indicates char available
			_endasm;
#endif
			return data;
#else
			XMIT_REG = *device->xmit.tail++;	/* send character out. */
			device->xmit.bytes_out--;
#endif
			}
#if 0
		else
			{
#if 0
			device->xmit.flag &= ~XMIT_BUSY;	/* transmitter done. */
#endif
			if (device->xmit.tcbptr)	/* see if task needs to be notified. */
				{
				if (device->xmit.tcbptr->tcbstate & WAIT)
					{
					device->xmit.tcbptr->tcbstate = RESUME;	/* allow task to resume. */
					if (device->xmit.tcbptr->priority < active_priority) 
						PREEMPTED; /*cmx_flag1 |= preempted;*/	/* yes, set preempted K_I_Scheduler flag */
					}
				}
			}
		}
#endif
#if 1
	return -1;	/* h=0ffh says theres no char for tx */
#else
	_asm
	scf	; cf=1 says theres no char for tx
	_endasm;
#endif
}

word16 K_Update_Dummy(SDEV *device)
{
	return -1; /* if installed as tx handler, says tx should be stopped */
}

/* The following can only be called by the interrupt. No
 Parameters are passed. */
void K_Update_Recv(SDEV *device, byte data)
{
/* abyte('<'); */
/* ++device->recv.bytes_in; */
/* return; */
#if 1
	if (device->recv.bytes_in < RECV_SIZE)	/* is buffer full. */
#else
	if (!(device->recv.flag & FULL))	/* see if reciever NOT full. */
#endif
		{
#if 1
		*device->recv.head = data; /* store value from receiver int */
		if (++device->recv.head == &device->recv.buff[RECV_SIZE]) /* see if wrap. */
			device->recv.head = &device->recv.buff[0];
		device->recv.bytes_in++;
#else
		*device->recv.head = RECV_REG;	/* get character and store in buffer. */
		if (++device->recv.head == &device->recv.buff[RECV_SIZE]) /* see if wrap. */
			device->recv.head = &device->recv.buff[0];
#if 1
		if (++device->recv.bytes_in == RECV_SIZE)	/* is buffer full. */
#else
		if (device->recv.bytes_in++ == RECV_SIZE)	/* is buffer full. */
#endif
			{
			device->recv.flag |= FULL;	/* yes set flag. */
			}
#endif
		if (device->recv.bytes_wanted)	/* is task waiting for number of bytes. */
			{
			if (!(--device->recv.bytes_wanted))	/* yes, decrement and test. */
				{
				if (device->recv.tcbptr->tcbstate & WAIT)	/* see if task waiting. */
					{
/* abyte('!'); */
					device->recv.tcbptr->tcbstate = RESUME;	/* yes, wake task. */
					if (device->recv.tcbptr->priority < active_priority) 
						PREEMPTED; /*cmx_flag1 |= preempted;*/	/* yes, set preempted K_I_Scheduler flag */
					}
				}
			}
		}
	else
		{
		device->recv.flag |= RECV_BUFF_ERR;	/* flag that buffer overrun 
							occurred char lost. */
		}
/* abyte('|'); */
}

byte K_I_Put_Char_Common(byte port, byte *byte_ptr, word16 cnt,
                                    word16 timecnt, byte wait)
{
	SDEV *device;
#if 1
	word16 count, rem;
#endif

	if (port >= PORTS || cnt > XMIT_SIZE)
		{
		return K_ERROR;
		}
	device = &sdevs[port];

	K_I_Disable_Sched();	/* set task block. */
/* abyte ('<'); */
	K_OS_Disable_Interrupts();	/* DISABLE INTERRUPTS. */
#if 1
	rem = XMIT_SIZE - device->xmit.bytes_out;
	if (cnt > rem) /* is transmitter buffer too full? */
#else
	if (device->xmit.flag & XMIT_BUSY)	/* is transmitter busy. */
#endif
		{
		if (wait)	/* should task wait. */
			{
			if (timecnt)	/* wait on time too? */
				activetcb->tcbstate = WAIT | TIME;	 /* put task to sleep, idicating why */
			else
				activetcb->tcbstate = WAIT;	 /* put task to sleep, idicating why */
			device->xmit.tcbptr = activetcb;	/* tell transmitter task waiting. */
#if 1
			/* calculate number of bytes needed to satisfy. */
			device->xmit.bytes_wanted = cnt - rem;
#endif
			K_OS_Enable_Interrupts();	/* RE-ENABLE INTERRUPTS */
			K_I_Insert_Link(timecnt);	/* insert it into time link. */
			device->xmit.tcbptr = NULL;	/* task will return to here. */
			K_I_Disable_Sched();	/* set task block. */
			if(K_I_Remove_Link())	/* remove from link. */
				{
				K_I_Func_Return();	/* time out occurred. */
				return(K_TIMEOUT);
				}
			}
		else
			{
			K_OS_Enable_Interrupts();	/* RE-ENABLE INTERRUPTS */
			K_I_Func_Return();	/* release task lock. */
			return(AER_UART_BUSY);	/* return error that transmitter busy. */
			}
		}
	else
		{
/* abyte('u'); */
		K_OS_Enable_Interrupts();	/* RE-ENABLE INTERRUPTS */
/* abyte('v'); */
		}
	/* the following sets up the transmitter buffer, and starts transmission. */
#if 0
	device->xmit.flag |= XMIT_BUSY;
#endif
#if 1
	/* bytes to be given to the queue after copying into the queue */
	count = cnt;
/* abyte('w'); */

	/* calculate remaining bytes of contiguous buffer space after head */
	rem = &device->xmit.buff[XMIT_SIZE] - device->xmit.head;
/* abyte('x'); */

	/* we must ensure this gets executed if the queue is going to wrap! */
	if (cnt >= rem)
		{
		/* queue wrap is going to occur, fill queue up to wrap spot */
		cnt -= rem;
		while (rem--)
			{
			*device->xmit.head++ = *byte_ptr++;
			}
		device->xmit.head = &device->xmit.buff[0];
		}
/* abyte('y'); */

	/* if the queue wrapped, cnt may be zero in which case we do nothing */
	while(cnt--)
		{
		*device->xmit.head++ = *byte_ptr++;
		}
/* abyte('z'); */

	/* only now can we tell the interrupt that it has characters waiting */
	K_OS_Disable_Interrupts();	/* DISABLE INTERRUPTS. */
	device->xmit.bytes_out += count; /* number of characters to transmit */
	K_OS_Enable_Interrupts();	/* RE-ENABLE INTERRUPTS */

/* ahexw(count); */
	(*device->tx_enable_vector)(device); /* Nick */
/* abyte('>'); */
#else
	device->xmit.bytes_out = cnt;	/* number of characters to transmit. */
	device->xmit.head = &device->xmit.buff[0];	/* reset head. */
	device->xmit.tail = &device->xmit.buff[0];	/* reset tail. */
	while(cnt--)
		{
		*device->xmit.head++ = *byte_ptr++;	/* load bytes in. */
		}

	XMIT_INT_ON();	 /* enable transmitter. */
#endif
	K_I_Func_Return();
	return(K_OK);
}
/* for sending out characters to uart buffer*/
/* this works with uart buffer */
byte K_Put_Str(byte port, void *byte_ptr, word16 cnt)
{
	return(K_I_Put_Char_Common(port, (byte *)byte_ptr, cnt, 0, 0));
}

byte K_Put_Str_Wait(byte port, void *byte_ptr, word16 cnt, word16 timecnt)
{
	return(K_I_Put_Char_Common(port, (byte *)byte_ptr, cnt, timecnt, 1));
}

byte K_Put_Char(byte port, void *byte_ptr)
{
	return(K_I_Put_Char_Common(port, (byte *)byte_ptr, 1, 0, 0));
}

byte K_Put_Char_Wait(byte port, void *byte_ptr, word16 timecnt)
{
	return(K_I_Put_Char_Common(port, (byte *)byte_ptr, 1, timecnt, 1));
}



/* for recieving characters from uart buffer*/
/* this works with uart buffer */
word16 K_I_Get_Char_Common(byte port, byte *byte_ptr, word16 cnt,
                                      word16 timecnt, byte wait)
{
	SDEV *device;
	word16 count;
#if 1
	word16 rem;
#endif

	if (port >= PORTS || cnt > RECV_SIZE)
		{
		return 0; /* pretend UART busy, can't return K_ERROR; */
		}
	device = &sdevs[port];

	K_I_Disable_Sched();	/* set task lock. */
	K_OS_Disable_Interrupts();	/* DISABLE INTERRUPTS. */
	if (!cnt)	/* vaiable length command, get current contents */
		{
		cnt = device->recv.bytes_in;	/* get number of chars. already in. */
		K_OS_Enable_Interrupts();	/* RE-ENABLE INTERRUPTS */
		}
	else
		{
		if (cnt > device->recv.bytes_in)	/* see if in count > then requested. */
			{
			if (wait)	/* should task wait. */
				{
				if (timecnt)	/* yes, also time period. */
					activetcb->tcbstate = WAIT | TIME;	 /* put task to sleep, idicating why */
				else
					activetcb->tcbstate = WAIT;	 /* put task to sleep, idicating why */
				device->recv.tcbptr = activetcb;	/* tell reciever. */
				/* calculate number of bytes needed to satisfy. */
				device->recv.bytes_wanted = (cnt - device->recv.bytes_in);
				K_OS_Enable_Interrupts();	/* RE-ENABLE INTERRUPTS */
				K_I_Insert_Link(timecnt);	/* insert into time link. */
				K_I_Disable_Sched();	/* set task lock. */
				device->recv.bytes_wanted = 0;	/* reset wanted count. */
				if(K_I_Remove_Link())	/* remove from link if need be. */
					{
					if (wait == 0x02)	/* see if variable count mode. */
						{
						K_OS_Disable_Interrupts();	/* DISABLE INTERRUPTS. */
						cnt = device->recv.bytes_in;	/* set number of bytes to move. */
						K_OS_Enable_Interrupts();	/* RE-ENABLE INTERRUPTS */
						}
					else
						{
						K_I_Func_Return();	/* release task block. */
						return(0);	/* return error status. */
						}
					}
				}
			else
				{
				K_OS_Enable_Interrupts();	/* RE-ENABLE INTERRUPTS */
				K_I_Func_Return();	/* release task block. */
				return(0);	/* return uart busy status. */
				}
			}
		else
			{
			K_OS_Enable_Interrupts();	/* RE-ENABLE INTERRUPTS */
			}
		}
#if 1
/* abyte('a'); */
	/* bytes to be taken from the queue after copying out of the queue */
	count = cnt;

	/* calculate remaining bytes of contiguous buffer space after head */
	rem = &device->recv.buff[RECV_SIZE] - device->recv.tail;

	/* we must ensure this gets executed if the queue is going to wrap! */
	if (cnt >= rem)
		{
		/* queue wrap is going to occur, suck queue up to wrap spot */
		cnt -= rem;
		while (rem--)
			{
/* abyte('b'); */
			*byte_ptr++ = *device->recv.tail++;
			}
		device->recv.tail = &device->recv.buff[0];
		}

	/* if the queue wrapped, cnt may be zero in which case we do nothing */
	while(cnt--)
		{
/* abyte('c'); */
		*byte_ptr++ = *device->recv.tail++;
		}

	/* only now can we tell the interrupt that we freed some queue space */
	K_OS_Disable_Interrupts();	/* DISABLE INTERRUPTS. */
	device->recv.bytes_in -= count;	/* number of characters now freed. */
#if 0
	device->recv.flag &= ~FULL;	/* reset flag. (THIS IS SILLINESS!) */
#endif
	K_OS_Enable_Interrupts();	/* RE-ENABLE INTERRUPTS */
/* abyte('d'); */
#else
	count = 0;
	/* load callers pointer with bytes. */
	while (cnt-- && device->recv.bytes_in)
		{
		*byte_ptr++ = *device->recv.tail;
		/* see if wrap. */
		if (++device->recv.tail == &device->recv.buff[RECV_SIZE])
			device->recv.tail = &device->recv.buff[0];
		count++;	/* increment count. */
		K_OS_Disable_Interrupts();	/* disable interrupts. */
		--device->recv.bytes_in;	/* decrement count of number of bytes in reciever. */
#if 0
		device->recv.flag &= ~FULL;	/* reset flag. */
#endif
		K_OS_Enable_Interrupts();	/* re-enable interrupts. */
		}
#endif
	K_I_Func_Return();
/* abyte('e'); */
	return(count);
}

word16 K_Recv_Count(byte port)
{
	SDEV *device;

	if (port >= PORTS)
		{
		return K_ERROR;
		}
	device = &sdevs[port];

	return(device->recv.bytes_in);
}

/* get single character, no time out, no wait */
word16 K_Get_Char(byte port, void *byte_ptr)
{
	return(K_I_Get_Char_Common(port, (byte *)byte_ptr, 1, 0, 0));
}

/* get single character, wait, if after wait no character, return nothing */
word16 K_Get_Char_Wait(byte port, void *byte_ptr, word16 timecnt)
{
	return(K_I_Get_Char_Common(port, (byte *)byte_ptr, 1,
                                         timecnt, 1));
}

/* get requested number of char. , if number requested
not there, return nothing, NO wait */
word16 K_Get_Str(byte port, void *byte_ptr, word16 cnt)
{
	return(K_I_Get_Char_Common(port, (byte *)byte_ptr, cnt, 0, 0));
}

/* get requested number of char. , wait for timeout if number requested
not there, if after wait correct number NOT there , return nothing */
word16 K_Get_Str_Wait(byte port, void *byte_ptr, word16 cnt, word16 timecnt)
{
	return(K_I_Get_Char_Common(port, (byte *)byte_ptr, cnt,
                                         timecnt, 1));
}

/* get requested number of char. , wait for timeout if number requested
not there, if after wait correct number NOT there , return the
number that came in */
word16 K_Get_Str_Wait_Return(byte port, void *byte_ptr,
                                        word16 cnt, word16 timecnt)
{
	return(K_I_Get_Char_Common(port, (byte *)byte_ptr, cnt,
                                         timecnt, 2));
}

/* get ALL character in buffer, do not wait */
word16 K_Get_Str_Return(byte port, void *byte_ptr)
{
	return(K_I_Get_Char_Common(port, (byte *)byte_ptr, 0, 0, 0));
}

/* inset task into time wait link, if need be. */
void K_I_Insert_Link(word16 timecnt)
{
	activetcb->tcbtimer = timecnt;	/* load task time counter with proper time */
	if (timecnt)	/* put into timer chain ?? */
		{
		/* let task sleep, indicating why and also waiting on time */
		if (tsk_timer_lnk->ftlink != (tcbpointer)tsk_timer_lnk)	
			{
			activetcb->ftlink = tsk_timer_lnk->btlink->ftlink;
			tsk_timer_lnk->btlink->ftlink = activetcb;
			activetcb->btlink = tsk_timer_lnk->btlink;
			}
		else
			{
			activetcb->ftlink = activetcb->btlink = (tcbpointer)tsk_timer_lnk;
			tsk_timer_lnk->ftlink = activetcb;
			}
		tsk_timer_lnk->btlink = activetcb;
		}
	PREEMPTED; /*cmx_flag1 |= preempted;*/	/* set the K_I_Scheduler flag */
	K_I_Func_Return();	/* release task block */
}

/* remove from time link, if need be. */
byte K_I_Remove_Link()
{
	if (activetcb->tcbtimer)
		{
		activetcb->ftlink->btlink = activetcb->btlink;
		activetcb->btlink->ftlink = activetcb->ftlink;
		}
	/* see if task was woken, because the thing it was waiting for happened,
		or that the time period specified has elapsed */
	if (activetcb->tcbstate & TIME_EXPIRED)
		{
		return(K_TIMEOUT);	/* return the warning: that the time period expired */
		}
	else
		{
		return(K_OK);	/* return good, time period did NOT expire. */
		}
}

