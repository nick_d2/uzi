#include <cxfuncs.h>
#include <cxextern.h>	/* get cmx include header file */

#include <cmxtrack.h>	/* get cmx include header file */

#define BIG_ENDIAN 0
#define LARGE 0 /* Nick 1 */
#define STACK_MSB 0	/* set to 1, if stack pointer points to MSB of long, when pushed */
#define large_ptr void *

#define MAX_MEM_DECIMAL 0x05	/* max decimal size */
#define MAX_MEM_HEX 0x04		/* max hex size */
#define MAX_FIELD_LENGTH 12	/* the maximum field length of any field */
#define SCROLL_SIZE 18
#define HOME 71
#define PAGE_UP 73
#define PAGE_DOWN 81
#define END 79

#define LOG_DISPLAY_LEN 18

#define WHAT_FUNCTION 0
#define CMXTRACKER_VIEW 1
#define CMXTRACKER_RESET 2
#define DELAY_TICK 3
#define QUICK_TICK 4
#define EXIT_BUG 99

#define WHAT_FUNCTION_PROMPT 00 /* 1 */
#define CMXTRACKER_VIEW_PROMPT WHAT_FUNCTION_PROMPT + 1 /* 1 */
#define CMXTRACKER_RESET_PROMPT CMXTRACKER_VIEW_PROMPT + 1 /* 1 */
#define DELAY_TICK_PROMPT CMXTRACKER_RESET_PROMPT + 1 /* 1 */
struct prompt {
	char *word;
	byte in_length;
	};

/* prompt char., length of field */
static /* Nick */ const struct prompt prpt_array [] = {
			{ " Enter Function? \n",0x02},		
			{ "\r\nCMXTRACKER dump",0x01},
			{ "\r\nEnter Y/y to reset CMXTRACKER array or <return> to leave as is ",0x03},
			{ "\r\nEnter the number of ticks to wait or <return> to leave? ",0x05}
		};

/* keyboard flags */
struct track_keyflag {	 /* bit structure */
	bit_word16 f_key:1;
	bit_word16 e_key:1;
	bit_word16 non_zero:1;	
	bit_word16 cont:1;
	bit_word16 e_jmp:1;
	bit_word16 input_func:1;
	bit_word16 allow_hex:1;
	bit_word16 echo_off:1;
	bit_word16 short_str:1;
	bit_word16 int_action:1;
	bit_word16 timer_task:1;
	bit_word16 end_key:1;
	};

static struct track_keyflag keyflags;
static byte key_pressed;	/* byte for scan code of keypad 0x00 thru 0x0f */
static byte num_chars_in;
static byte * in_char_ptr;
static byte max_chars_in;
static byte current_func;
static byte keys_in_buff[MAX_FIELD_LENGTH];
static byte prompt_index;
static byte hex_byte;
static long hex_long;
static byte func_offset;		
static byte input_index;
static const struct prompt * prpt_ptr;
static word16 c5;
static byte * c6;

void delay_tick(void);
void quick_tick(void);
static void cmxtracker_view(void);
static void zerofill(byte *);
static void clear_chars_in(void);
static void conv_hex(void);
static void display_char(char);
static void putstg(const char *); 
static void putbyte(byte);
static void putword(word16);
static void puttime(void);
static void pichexlong(large_ptr,byte);
static void pichexword(void *,byte);
static void puthexbyte(byte);
static void puthexword(word16);
static void putname(char c2);

void cmxtracker_reset(void);
static void putcrlf(void);
static void prompt_out(void);
static byte cmxtracker_wake;
static byte cmxtracker_mode_byte;
static word16 cmxtracker_count;

static const char nibs[] = "0123456789ABCDEF";

/* non-statics */

byte * cmxtracker_in_ptr;
byte * cmxtracker_out_ptr;
word16 cmxtracker_ctr;
word16 num_records;
word16 record_cnt;
byte cmxtracker_slot;

extern byte cmxtracker_array[];
extern char * task_name[];
extern word16 rec_cnt[];

extern byte CMXTRACKER_ACTIVE;
extern byte BUG_WAIT_TICKS;
extern void init_cmxtracker(void);
extern byte K_Track_Getchr(byte *ptr);
extern void K_Track_Putchr(byte);

void bug_putstg(byte *);
void cmxtracker(void);

void cmxtracker(void)
{
	byte status;

	/* abyte('t'); */

		/* process */
	CMXTRACKER_ACTIVE = 0; /*1; /* Nick 0; */
	while(1)
		{
		/* abyte('u'); */

		if (!keyflags.cont) {
			while(1)
				{
				/* abyte('v'); */

				if (cmxtracker_wake)
					{
					key_pressed = '+';
					break;
					}
				status = K_Track_Getchr(&key_pressed);
				if (status)
					break;
				if (!CMXTRACKER_ACTIVE)
					{
					/* abyte('w'); */
					K_Task_Wait(BUG_WAIT_TICKS);
					/* abyte('x'); */
					}
				}
		cmxtracker_wake = 0;
		/* abyte('('); */
		/* abyte(key_pressed); */
		/* abyte(')'); */
		switch (key_pressed) {
			case '+':
				locked_out = 1;

				CMXTRACKER_ON = 0;
				CMXTRACKER_ACTIVE = 1;
				keyflags.f_key = TRUE;
				break;

			case 'p':
			case 'P':
				if (keyflags.echo_off)
					keyflags.echo_off = FALSE;
				else
					keyflags.echo_off = TRUE;
				break;

			case '\r':
				if (!keyflags.non_zero)
					{
					keyflags.f_key = TRUE;
					break;
					}

				if (num_chars_in > 0 || keyflags.input_func)
					{
					keyflags.e_key = TRUE;
					}
				break;

			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				if (num_chars_in < max_chars_in && !keyflags.input_func)
					{
					keyflags.non_zero = TRUE;
					display_char(key_pressed);
#if 1
					*in_char_ptr = key_pressed & 0x0f;
					abyte('I');
					ahexw(*in_char_ptr);
					abyte(' ');
					in_char_ptr++;
#else
					*in_char_ptr++ = key_pressed & 0x0f;
#endif
					num_chars_in++;
					}
				break;
			case '*':
				if (num_chars_in < max_chars_in &&!keyflags.input_func)
					{
					keyflags.non_zero = TRUE;
					display_char(key_pressed);
					hex_byte = key_pressed;
					keyflags.e_jmp = TRUE;
					func_offset |= 0x80;	/* set done entry flag */
					num_chars_in++;
					}
				break;

			case 'a':
			case 'A':
			case 'b':
			case 'B':
			case 'c':
			case 'C':
			case 'd':
			case 'D':
			case 'e':
			case 'E':
			case 'f':
			case 'F':
				if (!keyflags.allow_hex)
					break;
				if (num_chars_in < max_chars_in &&!keyflags.input_func)
					{
					keyflags.non_zero = TRUE;
					display_char(key_pressed);
					*in_char_ptr++ = ((key_pressed & 0x0f) + 0x09);
					num_chars_in++;
					}
				break;

			case 'X':
			case 'x':
				if (!keyflags.non_zero)
					{
					display_char('x');
					keyflags.allow_hex = TRUE;
					if (max_chars_in == MAX_MEM_DECIMAL)
						max_chars_in = MAX_MEM_HEX;
					}
				break;

			case 'Y':
			case 'y':
				hex_byte = 1;
				keyflags.e_jmp = TRUE;
				keyflags.e_key = TRUE;
				K_Track_Putchr('Y');
				func_offset |= 0x80;	/* set done entry flag */
				break;

			case 'N':
			case 'n':
				hex_byte = 0;
				keyflags.e_jmp = TRUE;
				keyflags.e_key = TRUE;
				K_Track_Putchr('N');
				func_offset |= 0x80;	/* set done entry flag */
				break;

			default:
				break; 
				} /* end of switch */
			} /* END OF KEYBOARD.CONT IF */

		if (keyflags.cont)
			{
			keyflags.cont = FALSE;
			keyflags.e_key = TRUE;
			keyflags.e_jmp = TRUE;
			func_offset = 0;
			}
			if (keyflags.f_key)
				{
				keyflags.f_key = FALSE;
				keyflags.e_key = FALSE;
				keyflags.e_jmp = FALSE;
				keyflags.allow_hex = FALSE;
				keyflags.cont = FALSE;
				keyflags.input_func = FALSE;
				func_offset = 0;
				current_func = WHAT_FUNCTION;
				prompt_index = WHAT_FUNCTION_PROMPT;
				/* clear all normal flags, etc. */
				clear_chars_in();
				max_chars_in = 2;
				putstg("\r\nEnter 1 to DISPLAY LOG\r\n");
				putstg("Enter 2 RESET LOG\r\n");
				putstg("Enter 3 GO and RESUME CMXTracker\r\n");
				putstg("Enter 4 Quick GO and RESUME CMXTracker\r\n");
				putstg("Enter 99 EXIT CMXTracker\r\n");
				putstg("Enter P/p to toggle ECHO mode, currently ");
				if (!keyflags.echo_off)
					{
					putstg("ON");
					}
				else
					{
					putstg("OFF");
					}
				putstg("\r\nYour choice? ");
				}
			if (keyflags.e_key && CMXTRACKER_ACTIVE)
				{
				keyflags.e_key = FALSE;
				if (!keyflags.e_jmp)
					{
					zerofill(&keys_in_buff[max_chars_in]);	/* go zero fill, for proper entry */
					conv_hex();	/* go put characters into hex form, just 2 lsd */
					func_offset |= 0x80;	/* set done entry flag */
					}
				else
					keyflags.e_jmp = FALSE;
				if (current_func == WHAT_FUNCTION)
					{
					func_offset = 0;
					current_func = hex_byte;
					abyte(' ');
					ahexw(current_func);
					abyte(' ');
					switch(current_func) {
						case WHAT_FUNCTION:
							break;
						case CMXTRACKER_VIEW:
							break;
						case CMXTRACKER_RESET:
							break;
						case QUICK_TICK:
							break;
						case DELAY_TICK:
							break;
						case EXIT_BUG:
							break;
						default:
							current_func = WHAT_FUNCTION;
							keyflags.f_key = TRUE;
							putstg("\r\n\r\nInvalid entry, press any key to continue\r\n");
							break;
						}
					}
				switch(current_func) {
					case CMXTRACKER_VIEW:
						cmxtracker_view();
						break;
					case CMXTRACKER_RESET:
						cmxtracker_reset();
						break;
					case QUICK_TICK:
						quick_tick();
						break;
					case DELAY_TICK:
						delay_tick();
						break;
					case EXIT_BUG:
						cmxtracker_mode_byte = 0x00;
						locked_out = 0;
						CMXTRACKER_ACTIVE = 0;
						CMXTRACKER_ON = 1;
						putstg("\r\n\r\n You have exited CMXTracker(TM), press <+> (plus key) to enter CMXTracker again!\r\n");
						break;
					default:
						break;
					} /* end of switch */
				} /* end of e key brace */
		}	/* end of endless while loop */
}

void cmxtracker_in_task(void)
{
	/* abyte('('); */
	cmxtracker_in(3,TASK_RUNNING,0,0,0);
	/* abyte(')'); */
}

/*
mode:
bit 0 = on
bit 1 = 
bit 2 = number of ticks
bit 3 = number of entries
bit 7 = autowake cmxtracker task
*/
void cmxtracker_mode(byte mode, word16 count)
{
	cmxtracker_mode_byte = mode;
	cmxtracker_count = count;
}

void cmxtracker_in(byte count,byte c1,byte c2,word16 c3,void *c4)
{
	byte *copy_in_ptr;

	/* abyte('&'); */
	/* ahexw(c1 | (word16)count << 8); */
	/* abyte(' '); */
	if (CMXTRACKER_ON)
		{
		PROC_SAVE_INT;		/* save interrupt state & disable interrupts */
		if (cmxtracker_ctr >= count)
			{
			cmxtracker_ctr -= count;
			copy_in_ptr = cmxtracker_in_ptr;
			cmxtracker_in_ptr += count;
			PROC_RESTORE_INT;

			*copy_in_ptr++ = count;
			*copy_in_ptr++ = activetcb - cmx_tcb;
			*copy_in_ptr++ = c1;
			if (count >= 4)
				*copy_in_ptr++ = c2;
			if (count == 5)
				*copy_in_ptr++ = (c3 & 0xff);
			if (count >= 6)
				{
				if (c1 & 0x80)
					{
					*copy_in_ptr++ = ((c3 >> 8) & 0xff);
					*copy_in_ptr++ = (c3 & 0xff);
					}
				else
					{
#if LARGE > 0
#if STACK_MSB > 0
					*copy_in_ptr++ = *(((byte *) &(c4)) + 0);
					*copy_in_ptr++ = *(((byte *) &(c4)) + 1);
					*copy_in_ptr++ = *(((byte *) &(c4)) + 2);
					*copy_in_ptr++ = *(((byte *) &(c4)) + 3);
#else
					*copy_in_ptr++ = *(((byte *) &(c4)) + 3);
					*copy_in_ptr++ = *(((byte *) &(c4)) + 2);
					*copy_in_ptr++ = *(((byte *) &(c4)) + 1);
					*copy_in_ptr++ = *(((byte *) &(c4)) + 0);
#endif
#else
#if STACK_MSB > 0
					*copy_in_ptr++ = *(((byte *) &(c4)) + 0);
					*copy_in_ptr++ = *(((byte *) &(c4)) + 1);
#else
					*copy_in_ptr++ = *(((byte *) &(c4)) + 1);
					*copy_in_ptr++ = *(((byte *) &(c4)) + 0);
#endif
#endif
					}
				}
			num_records++;
			if (cmxtracker_mode_byte & 0x0c)
				{
				if (cmxtracker_count)
					{
					if ((cmxtracker_mode_byte & 0x08) || c1 == CMX_TIC_CALL)
						if (!(--cmxtracker_count))
							if (cmxtracker_mode_byte & 0x80)
								{
								cmx_tcb[cmxtracker_slot].tcbstate = RESUME | TIME_EXPIRED;
								PREEMPTED;
								CMXTRACKER_ON = 0;
								cmxtracker_wake = 1;
								}
					}
				}
			}
		else
			{
			PROC_RESTORE_INT;
			}
		}
}

static void putstg(const char *s) /* display null-terminated ASCII string thru serial port */
{
	byte cnt;

	cnt = 0;

	while (*s)
		{
		if (keyflags.short_str)
			{
			if (cnt++ == 15)
				break;
			if (cnt > 12)
				{
				K_Track_Putchr('.');
				continue;
				}
			}
		K_Track_Putchr(*s++);
		}
	keyflags.short_str = FALSE;
}

static void cmxtracker_view(void)
{
	byte key;
	byte status;
	word16 record_cnt;
	byte page_ctrl = 0;
	byte update;
	word16 temp_cnt;
	byte out_cnt;
	byte task_num;
	byte cmx_key;
	word16 page;
	byte c2,c3,copy_c2 = 0; /* Nick has initialised copy_c2 = 0 for sdcc */
	byte parm1,parm2;

#if LARGE > 0
	byte parm3,parm4;
	word16 parm5,parm6;
#endif

	status = 1;
	key = HOME;
	page = 1;
	keyflags.int_action = FALSE;
	keyflags.timer_task = FALSE;
	keyflags.end_key = FALSE;

	while(1)
		{
		if (!status) {
			while(1)
				{
				if (keyflags.end_key)
					{
					key = PAGE_DOWN;
					break;
					}
				status = K_Track_Getchr(&key);
				if (status)
					break;
				}
			}
		status = 0;
		switch(key) {
				case 0x0D:
					keyflags.f_key = TRUE;
					keyflags.cont = TRUE;
	         	return;
					break;
				case HOME:
					keyflags.int_action = FALSE;
					keyflags.timer_task = FALSE;
					update = 1;
					page = 1;
					record_cnt = 0;
					break;
				case END:
					keyflags.int_action = FALSE;
					keyflags.timer_task = FALSE;
					putstg("\r\n\r\n Computing END ...\r\n");
					keyflags.end_key = TRUE;
					update = 1;
					page = 1;
					record_cnt = 0;
					break;
				case PAGE_DOWN:
					page_ctrl = 2;
					update = 1;
					break;
				case PAGE_UP:
					page_ctrl = 1;
					update = 1;
					break;
				default:
	         	break;
				}
		if (update)
			{
			if (page_ctrl == 1)
				{
				keyflags.int_action = FALSE;
				keyflags.timer_task = FALSE;
				/* page up */
				if (page >= 2)
					{
					page--;
					record_cnt = rec_cnt[page];
					}
				else
					{
					update = 0;
					page_ctrl = 0;
					continue;
					}
				}
			else if (page_ctrl == 2)
				{
				/* page down */
				if (record_cnt >= (num_records))
					{
					if (keyflags.end_key)
						{
						keyflags.end_key = FALSE;
						record_cnt = rec_cnt[page];
						}
					else
						{
						update = 0;
						page_ctrl = 0;
						continue;
						}
					}
				else
					{
					page++;
					}
				}
			update = 0;
			page_ctrl = 0;
			if (record_cnt < (num_records - 1))
				{
				cmxtracker_out_ptr = cmxtracker_array;
				for (temp_cnt = record_cnt; temp_cnt > 0; --temp_cnt)
					{
					cmxtracker_out_ptr += *cmxtracker_out_ptr;
					}
				}
			if (!keyflags.end_key)
				{
				putstg("\r\n\r\n View CMXTRACKER contents, <PG-DN>, <PG-UP>, <HOME>, <END> or <RETURN> to quit");
				putstg("\r\n\r\n PAGE Number ");
				putword(page);
				putstg("\r\n");
				}
			rec_cnt[page] = record_cnt;
 			for (temp_cnt = 0; (temp_cnt < LOG_DISPLAY_LEN) || (temp_cnt == 0xffff); temp_cnt++)
				{
				if (record_cnt < (num_records))
					{
					out_cnt = *cmxtracker_out_ptr++;
					task_num = *cmxtracker_out_ptr++;
					cmx_key = *cmxtracker_out_ptr++;
					abyte('^');
					ahexw(cmx_key | (word16)out_cnt << 8);
					abyte(' ');
					if (out_cnt > 3)
						{
						c2 = *cmxtracker_out_ptr++;
						}
					if (out_cnt == 5)
						{
						c3 = *cmxtracker_out_ptr++;
						}
					if (out_cnt >= 6)
						{
						if (cmx_key & 0x80)
							{
							cmx_key &= 0x7f;
							parm1 = *(byte *)cmxtracker_out_ptr++;
							parm2 = *(byte *)cmxtracker_out_ptr++;
							c5 = (parm1 * 256) + parm2;
							}
						else
							{
#if LARGE > 0
							parm1 = *(byte *)cmxtracker_out_ptr++;
							parm2 = *(byte *)cmxtracker_out_ptr++;
							parm5 = ((parm1 * 256) + parm2);
							parm3 = *(byte *)cmxtracker_out_ptr++;
							parm4 = *(byte *)cmxtracker_out_ptr++;
							parm6 = ((parm3 * 256) + parm4);
							c6 = (byte *)((parm5 * 65536) + parm6);
#else
							parm1 = *(byte *)cmxtracker_out_ptr++;
							parm2 = *(byte *)cmxtracker_out_ptr++;
							c6 = (byte *)((parm1 * 256) + parm2);
#endif
							}
						}
					if (cmx_key != CMX_TIC_CALL && cmx_key != INT_ACTION && cmx_key != TIMER_TASK_ACTION)
						{
						if (keyflags.end_key)
							{
							keyflags.timer_task = FALSE;
							keyflags.int_action = FALSE;
							record_cnt++;
							continue;
							}
						if (keyflags.timer_task)
							{
							keyflags.timer_task = FALSE;
							putstg("\r\n--CYCLIC ");
							putbyte(copy_c2);
							}
						else if (keyflags.int_action)
							{
							keyflags.int_action = FALSE;
							putstg("\r\n++INTERRUPT  ");
							}
						else
							{
							if (task_num)
								{
								if (task_name[task_num])
									{
									putstg("\r\n");
									keyflags.short_str = TRUE;
									putname(task_num);
									keyflags.short_str = FALSE;
									}
								else
									{
									putstg("\r\nSlot #  ");
									putbyte(task_num);
									}
								}
							else
								{
								putstg("\r\nUSER CODE    ");
								}
							}
						}
					else
						{
						if (keyflags.end_key)
							{
							if (cmx_key == INT_ACTION || cmx_key == TIMER_TASK_ACTION)
							temp_cnt--;
							record_cnt++;
							continue;
							}
						}
					switch(cmx_key) {
						case CMX_TIC_CALL:
							putstg("\r\n>>>> CMX Tick <<<<");
							break;
						case INT_ACTION:
							temp_cnt--;
							keyflags.int_action = TRUE;
							break;
						case TIMER_TASK_ACTION:
							temp_cnt--;
							copy_c2 = c2;
							keyflags.timer_task = TRUE;
							break;
						case ENABLE_SLICE_CALL:
							putstg("K_OS_Slice_On ");
							break;
						case DISABLE_SLICE_CALL:
							putstg("K_OS_Slice_Off ");
							break;
						case CXTEND_CALL:
							putstg("K_Task_End");
							break;
						case CXTCRE_K_ERROR:
							putstg("K_Task_Create, ERROR");
							break;
						case CXTCRE_K_OK:
							putstg("K_Task_Create, Successful");
							break;
						case CXTPRI_K_ERROR:
							putstg("K_Task_Priority, ERROR-> ");
							putname(c2);
							break;
						case CXTPRI_K_OK:
							putstg("K_Task_Priority, Successful, ");
							putname(c2);
							putstg(" ,NEW PRI. is");
							putbyte(c3);
							break;
						case CXTTRIG_K_ERROR:
							putstg("K_Task_Start, ERROR-> ");
							putname(c2);
							break;
						case CXTTRIG_K_OK:
							putstg("K_Task_Start, Successful, ");
							putname(c2);
							break;
						case CXTWATM_CALL:
							putstg("K_Task_Wait ");
							puttime();
							break;
						case CXTWATM_DELAY_K_ERROR:
							putstg("K_Task_Wait, Timed Out");
							break;
						case CXTWATM_DELAY_K_OK:
							putstg("K_Task_Wait, woken by K_Task_Wake");
							break;
						case CXTWAK_K_ERROR:
							putstg("K_Task_Wake, ERROR-> task ");
							putname(c2);
							break;
						case CXTWAK_K_OK:
							putstg("K_Task_Wake, Successful, ");
							putname(c2);
							break;
						case CXTWAK_K_NOT_WAITING:
							putstg("K_Task_Wake, ERROR-> not waiting, ");
							putname(c2);
							break;
						case CXPRVR_CALL:
							putstg("K_Task_Lock, Successful");
							break;
						case CXPRVL_CALL:
							putstg("K_Task_Unlock, Successful");
							break;
						case CXSCHED_CALL:
							putstg("K_Task_Coop_Sched");
							break;
						case CXTRMV_K_ERROR:
							putstg("K_Task_Delete, ERROR-> ");
							putname(c2);
							break;
						case CXTRMV_WAIT_K_ERROR:
							putstg("K_Task_Delete, ERROR-> BUSY, ");
							putname(c2);
							break;
						case CXTRMV_K_OK:
							putstg("K_Task_Delete, Successful, ");
							putname(c2);
							break;
						case CXERST_K_ERROR:
							putstg("K_Event_Reset, ERROR-> ");
							putname(c2);
							break;
						case CXERST_K_OK:
							putstg("K_Event_Reset, Successful, ");
							putname(c2);
							putstg(" Event bits reset = 0x");
							puthexword(c5);
							break;
						case CXEWATM_CALL:
							putstg("K_Event_Wait ");
							puttime();
							break;
						case CXEWATM_DELAY_K_TIMEOUT:
							putstg("K_Event_Wait, Timed Out");
							break;
						case CXEWATM_K_OK:
							putstg("K_Event_Wait, Successful, Event bits match = 0x");
							puthexword(c5);
							break;
						case CXESIG_MODE:
							putstg("K_Event_Signal, ERROR-> mode # ");
							putbyte(c2);
							break;
						case CXESIG_K_ERROR:
							putstg("K_Event_Signal, ERROR-> ");
							putname(c2);
							break;
						case CXESIG_K_OK:
							putstg("K_Event_Signal, Successful, Mode ");
							putbyte(c2);
							if (c2 < 3)
								{
								putname(c3);
								}
							else if (c2 == 3)
								{
								putstg("ALL Tasks");
								}
							else if (c2 == 4)
								{
								putstg("ALL Tasks Waiting");
								}
							else if (c2 == 5)
								{
								putstg("ALL Tasks same PRI.");
								}
							else if (c2 == 6)
								{
								putstg("ALL Tasks same PRI. Waiting");
								}
							break;
						case CXRSRSV_K_ERROR:
							if (c3)
								putstg("K_Resource_Get, ERROR-> resource # ");
							else
								putstg("K_Resource_Wait, ERROR-> resource # ");
							putbyte(c2);
							break;
						case CXRSRSV_K_RESOURCE_OWNED:
							putstg("K_Resource_Get, ERROR-> already owned, resource # ");
							putbyte(c2);
							break;
						case CXRSRSV_CALL:
							putstg("K_Resource_Wait, resource # ");
							putbyte(c2);
							puttime();
							break;
						case CXRSRSV_DELAY_K_TIMEOUT:
							putstg("K_Resource_Wait, Timed Out");
							break;
						case CXRSRSV_K_OK:
							if (c3)
								putstg("K_Resource_Get, Successful, resource # ");
							else
								putstg("K_Resource_Wait, Successful, resource # ");
							putbyte(c2);
							break;
						case CXRSREL_K_ERROR:
							putstg("K_Resource_Release, ERROR-> resource # ");
							putbyte(c2);
							break;
						case CXRSREL_K_RESOURCE_NOT_OWNED:
							putstg("K_Resource_Release, ERROR-> not owned, resource # ");
							putbyte(c2);
							break;
						case CXRSREL_K_OK:
							putstg("K_Resource_Release, Successful, resource # ");
							putbyte(c2);
							break;
						case CXQCRE_K_ERROR:
							putstg("K_Que_Create, ERROR-> queue # ");
							putbyte(c2);
							break;
						case CXQCRE_K_OK:
							putstg("K_Que_Create, Successful, queue # ");
							putbyte(c2);
							break;
						case CXQRST_K_ERROR:
							putstg("K_Que_Reset, ERROR-> queue # ");
							putbyte(c2);
							break;
						case CXQRST_K_OK:
							putstg("K_Que_Reset, Successful, queue # ");
							putbyte(c2);
							break;
						case CXQADD_K_ERROR:
							putstg("K_I_Que_Add_Common, ERROR-> Range: queue # ");
							putbyte(c2);
							break;
						case CXQADD_FULL:
							putstg("K_I_Que_Add_Common, ERROR-> Full: queue # ");
							putbyte(c2);
							break;
						case CXQADD_K_OK:
							putstg("K_I_Que_Add_Common, Successful, queue # ");
							putbyte(c2);
							break;
						case CXQRMV_K_ERROR:
							putstg("K_I_Que_Get_Common, ERROR-> Range: queue # ");
							putbyte(c2);
							break;
						case CXQRMV_EMPTY:
							putstg("K_I_Que_Get_Common, ERROR-> Empty: queue # ");
							putbyte(c2);
							break;
						case CXQRMV_K_OK:
							putstg("K_I_Que_Get_Common, Successful, queue # ");
							putbyte(c2);
							break;
						case CXMSWATM_K_ERROR:
							if (c3)
								putstg("K_Mesg_Get, ERROR-> range, MBOX # ");
							else
								putstg("K_Mesg_Wait, ERROR-> range, MBOX # ");
							putbyte(c2);
							break;
						case CXMSWATM_MAILBOX_K_ERROR:
							if (c3)
								putstg("K_Mesg_Get, ERROR-> MBOX owned, MBOX # ");
							else
								putstg("K_Mesg_Wait, ERROR-> MBOX owned, MBOX # ");
							putbyte(c2);
							break;
						case CXMSWATM_NOMESG_K_ERROR:
							putstg("K_Mesg_Wait, ERROR-> Timed Out, no messages, MBOX # ");
							putbyte(c2);
							break;
						case CXMSGET_NOMESG_K_ERROR:
							putstg("K_Mesg_Get, ERROR-> no messages, MBOX # ");
							putbyte(c2);
							break;
						case CXMSWATM_CALL:
							putstg("K_Mesg_Wait, MBOX # ");
							putbyte(c2);
							puttime();
							break;
						case CXMSGET_K_OK:
							putstg("K_Mesg_Get, Successful, MBOX # ");
							putbyte(c2);
							putstg(" Mesg. recv = ");
							keyflags.short_str = TRUE;
								putstg((char *)c6);
							break;
						case CXMSWATM_K_OK:
							putstg("K_Mesg_Wait, Successful, MBOX # ");
							putbyte(c2);
							putstg(" Mesg. recv = ");
							keyflags.short_str = TRUE;
								putstg((char *)c6);
							break;
						case CXMSSEND_K_ERROR:
							if (c3)
								putstg("K_Mesg_Send_Wait, ERROR-> Range/NO MSG slots, MBOX # ");
							else
								putstg("K_Mesg_Send, ERROR-> Range/NO MSG slots, MBOX # ");
							putbyte(c2);
							break;
						case CXMSSENW_K_OK:
							putstg("K_Mesg_Send_Wait, Successful, MBOX # ");
							putbyte(c2);
							putstg(" Mesg. sent = ");
							keyflags.short_str = TRUE;
								putstg((char *)c6);
							break;
						case CXMSSEND_CALL:
							putstg("K_Mesg_Send_Wait, MBOX # ");
							putbyte(c2);
							puttime();
							break;
						case CXMSSEND_K_TIMEOUT:
							putstg("K_Mesg_Send_Wait, ERROR-> Timed Out WAITING for ACK, MBOX # ");
							putbyte(c2);
							break;
						case CXMSSEND_K_OK:
							putstg("K_Mesg_Send_Wait, Successful, ACKED by receiving task, MBOX # ");
							putbyte(c2);
							break;
						case CXMSSEND_K_OK1:
							putstg("K_Mesg_Send, Successful, MBOX # ");
							putbyte(c2);
							putstg(" Mesg. sent = ");
							keyflags.short_str = TRUE;
								putstg((char *)c6);
							break;
						case CXMSBXEV_K_ERROR:
							putstg("K_Mbox_Event_Set, ERROR-> range, MBOX # ");
							putbyte(c2);
							break;
						case CXMSBXEV_K_OK:
							putstg("K_Mbox_Event_Set, Successful, MBOX # ");
							putbyte(c2);
							break;
						case CXMSACK_K_OK:
							putstg("K_Mesg_Ack_Sender, Successful, ");
							putname(c2);
							break;
						case CXCTCRE_K_ERROR:
							putstg("K_Timer_Create, ERROR-> Range: cyclic timer # ");
							putbyte(c2);
							break;
						case CXCTCRE_K_OK:
							putstg("K_Timer_Create, Successful, cyclic timer # ");
							putbyte(c2);
							break;
						case CXCTCOM_K_ERROR:
							putstg("K_I_Cyclic_Common, ERROR-> Range: cyclic timer # ");
							putbyte(c2);
							break;
						case CXCTSTT_K_OK:
							putstg("K_Timer_Start, Successful, cyclic timer # ");
							putbyte(c2);
							break;
						case CXCTRSTC_K_OK:
							putstg("K_Timer_Cyclic, Successful, cyclic timer # ");
							putbyte(c2);
							break;
						case CXCTRSTI_K_OK:
							putstg("K_Timer_Initial, Successful, cyclic timer # ");
							putbyte(c2);
							break;
						case CXCTRSTT_K_OK:
							putstg("K_Timer_Restart, Successful, cyclic timer # ");
							putbyte(c2);
							break;
						case CXCTSTP_K_ERROR:
							putstg("K_Timer_Stop, ERROR-> Range: cyclic timer # ");
							putbyte(c2);
							break;
						case CXCTSTP_K_OK:
							putstg("K_Timer_Stop, Successful, cyclic timer # ");
							putbyte(c2);
							break;
						case CXBFCRE_K_OK:
							putstg("K_Mem_FB_Create, Successful, Memory Block BASE address 0x");
#if LARGE > 0
							pichexlong(((large_ptr)(c6)),0);	/* address */
#else
							pichexword(((void *)(c6)),0);	/* address */
#endif
							break;
						case CXBFGET_K_OK:
							putstg("K_Mem_FB_Get, Successful, Memory address is 0x");
#if LARGE > 0
							pichexlong(((large_ptr)(c6)),0);	/* address */
#else
							pichexword(((void *)(c6)),0);	/* address */
#endif
							break;
						case CXBFGET_K_ERROR:
							putstg("K_Mem_FB_Get, ERROR-> none free, Memory Block base address 0x");
#if LARGE > 0
							pichexlong(((large_ptr)(c6)),0);	/* address */
#else
							pichexword(((void *)(c6)),0);	/* address */
#endif
							break;
						case CXBFREL_K_OK:
							putstg("K_Mem_FB_Release, Successful, Memory address returned 0x");
#if LARGE > 0
							pichexlong(((large_ptr)(c6)),0);	/* address */
#else
							pichexword(((void *)(c6)),0);	/* address */
#endif
							break;
						case TASK_RUNNING:
							putstg("EXECUTING");
							break;
						case INT_ENTRY:
							putstg("INTERRUPT # ");
							putbyte(c2);
							break;
						case USER_ENTRY:
							putstg("USER ENTRY, entry # ");
							putbyte(c2);
							break;
						default:
							putstg("Not valid command");
							break;
						}
					record_cnt++;
					}
				}
			if (!keyflags.end_key)
				putstg("\r\n");
			}
		}
}

void delay_tick(void)
{

	prompt_index = DELAY_TICK_PROMPT;

	switch (func_offset) {
		case 0x00:	/* display 1'st prompt */
			putcrlf();
			prompt_out();
			break;
		case 0x80:	/* process card number prompt */
			func_offset &= 0x7f;
			/* maybe change to decimal, later */
			if (hex_long < 0xffff && hex_long)
				{
				putstg("\r\nBUG task, waiting for the following number of TICKS ");
				putword((word16)hex_long);
				cmxtracker_mode_byte = 0x00;
				CMXTRACKER_ON = 1;
				CMXTRACKER_ACTIVE = 0;
				locked_out = 0;
				K_Task_Wait((word16)hex_long);
				locked_out = 1;
				CMXTRACKER_ACTIVE = 1;
				CMXTRACKER_ON = 0;
				keyflags.f_key = TRUE;
				keyflags.cont = TRUE;
				break;
				}
			else
				{
				putstg("\r\nMust be non zero number and less 65536!");
				func_offset = 0;
				keyflags.cont = TRUE;
				break;
				}
			}
}

void quick_tick(void)
{
		cmxtracker_mode_byte = 0x00;
		CMXTRACKER_ON = 1;
		CMXTRACKER_ACTIVE = 0;
		locked_out = 0;
		K_Task_Wait(1);
		locked_out = 1;
		CMXTRACKER_ACTIVE = 1;
		CMXTRACKER_ON = 0;
		keyflags.f_key = TRUE;
		keyflags.cont = TRUE;
}

static void clear_chars_in(void)
{
	in_char_ptr = keys_in_buff;
	do {
		*in_char_ptr++ = 0;
		} while (in_char_ptr != &keys_in_buff[sizeof keys_in_buff]);
	in_char_ptr = keys_in_buff;
	num_chars_in = 0;
}

static void conv_hex(void)
{

	byte *hex_char_ptr;
	word16 multiplier;

	abyte('!');

	multiplier = 1;
	hex_long = 0; 
	if (max_chars_in == 1)
		{
		hex_char_ptr = keys_in_buff;
		hex_byte = *hex_char_ptr;
		hex_long = (long)hex_byte;
		}
	else
		{
		hex_char_ptr = &keys_in_buff[max_chars_in-2];
		if (keyflags.allow_hex)
			{
			hex_byte = *hex_char_ptr * 16;
			hex_char_ptr++;
			hex_byte += *hex_char_ptr;
			}
		else
			{
#if 0
			abyte('H');
			ahexw(*hex_char_ptr);
			abyte(' ');
#endif
			hex_byte = *hex_char_ptr * 10;
			hex_char_ptr++;
#if 0
			abyte('L');
			ahexw(*hex_char_ptr);
			abyte(' ');
#endif
			hex_byte += *hex_char_ptr;
			}
		for (hex_char_ptr = &keys_in_buff[max_chars_in - 1];
				hex_char_ptr >= keys_in_buff;	--hex_char_ptr)
			{
			hex_long += *hex_char_ptr * multiplier;
			if (keyflags.allow_hex)
				multiplier *= 16;
			else
				multiplier *= 10;
			} 
		}

	abyte('@');
}

static void zerofill(byte *mov_char_ptr)
{
	byte count;
	byte *copy_in_char_ptr;

	if (num_chars_in != max_chars_in)
		{
		copy_in_char_ptr = in_char_ptr;
		for (count = 0; count < num_chars_in; count++)
			{
			*(--mov_char_ptr) = *(--in_char_ptr);
#if 0
			abyte('M');
			ahexw(*mov_char_ptr);
			abyte(' ');
#endif
			}
		for (count = num_chars_in;	count < max_chars_in; count++)
			{
			*(--mov_char_ptr) = 0x00;
			}
		in_char_ptr = copy_in_char_ptr;
		}
}
static void display_char(char x)
{
	if (!keyflags.echo_off)
		K_Track_Putchr(x);
}
void cmxtracker_reset(void)
{
	prompt_index = CMXTRACKER_RESET_PROMPT;

	switch (func_offset) {
		case 0x00:	/* display 1'st prompt */
			putcrlf();
			prompt_out();
			break;
		case 0x80:	/* process card number prompt */
			func_offset &= 0x7f;
			if (hex_byte)
				{
				init_cmxtracker();
				}
			keyflags.f_key = TRUE;
			keyflags.cont = TRUE;
			break;
			}
}
static void putcrlf(void)
{
	K_Track_Putchr(0x0d);
	K_Track_Putchr(0x0a);
}	/* putcrlf */
static void prompt_out(void)
{

	prpt_ptr = &prpt_array[prompt_index + func_offset];
	/* chars_in(); */
	keyflags.non_zero = FALSE;
	max_chars_in = prpt_ptr->in_length;
	clear_chars_in();
	putstg(prpt_ptr->word);
}

static void putbyte(byte num)		/* output a number (decimal) */
{

	byte digit,suppress;
	byte divisor;

    divisor = 100;
    suppress = 1;

    while( !( divisor == 1 )){
		digit = num / divisor;
		num = num - (digit * divisor);
		divisor = divisor / 10;
		if( suppress  &&  digit )
	   	suppress = 0;
		if( !suppress )
			digit |= '0';
		else
			digit = ' ';
		K_Track_Putchr(digit);
    	}
	K_Track_Putchr(num+'0');
	K_Track_Putchr(' ');
}

static void putword(word16 num)		/* output a number (decimal) */
{

	byte digit,suppress;
	word16 divisor;

    divisor = 10000;
    suppress = 1;

    while( !( divisor == 1 )){
		digit = num / divisor;
		num = num - (digit * divisor);
		divisor = divisor / 10;
		if( suppress  &&  digit )
	   	suppress = 0;
		if( !suppress )
			digit |= '0';
		else
			digit = ' ';
		K_Track_Putchr(digit);
    	}
	K_Track_Putchr((char)(num+'0'));
}

static void puttime(void)
{
	putstg(" TIME PERIOD = ");
	putword(c5);
}

static void pichexlong(large_ptr w,byte m)	/* puts out long word in hex */
{
#if BIG_ENDIAN > 0
	puthexbyte(*(((byte *) &(w)) + 0));
	puthexbyte(*(((byte *) &(w)) + 1));
	K_Track_Putchr(':');
	puthexbyte(*(((byte *) &(w)) + 2));
	if (m)
		puthexbyte((*(((byte *) &(w)) + 3)) & 0xf0);
	else
		puthexbyte(*(((byte *) &(w)) + 3));
	K_Track_Putchr(' ');
#else
	puthexbyte(*(((byte *) &(w)) + 3));
	puthexbyte(*(((byte *) &(w)) + 2));
	K_Track_Putchr(':');
	puthexbyte(*(((byte *) &(w)) + 1));
	if (m)
		puthexbyte((*(((byte *) &(w)) + 0)) & 0xf0);
	else
		puthexbyte(*(((byte *) &(w)) + 0));
	K_Track_Putchr(' ');
#endif
}	/* pichexlong */

static void pichexword(void *w,byte m)	/* puts out long word in hex */
{
#if BIG_ENDIAN > 0
	puthexbyte(*(((byte *) &(w)) + 0));
	if (m)
		puthexbyte((*(((byte *) &(w)) + 1)) & 0xf0);
	else
		puthexbyte(*(((byte *) &(w)) + 1));
	K_Track_Putchr(' ');
#else
	puthexbyte(*(((byte *) &(w)) + 1));
	if (m)
		puthexbyte((*(((byte *) &(w)) + 0)) & 0xf0);
	else
		puthexbyte(*(((byte *) &(w)) + 0));
	K_Track_Putchr(' ');
#endif
}	/* pichexword */

static void puthexbyte(byte b)	/* puts out byte in ASCII hex, to serial port */
{
	K_Track_Putchr(nibs[ b >> 4 ]);
	K_Track_Putchr(nibs[ b & 0x0F ]);
}	/* puthexbyte */

static void puthexword(word16 w)
/*	Put out word in hex, high byte first. */
{
	puthexbyte((byte)(w>>8));
	puthexbyte((byte)(w & 0xff));
}	/* puthexword */

static void putname(char c2) /* display null-terminated ASCII string thru serial port */
{
	byte ctr;
	char *s;

	if (!c2)
		return;
	if (!task_name[c2])
		{
		putstg("Slot # ");
		putbyte(c2);
		putstg("        ");
		}
	else
		{
		ctr = 0;
		if (!keyflags.short_str)
			putstg("Task ");
		s = task_name[c2];
		do {
			if (*s)
				K_Track_Putchr(*s++);
			else
				K_Track_Putchr(' ');
			} while (++ctr < 13 );
		}
}	

