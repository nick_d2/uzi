; bstartup.asm

;
;	$Id: bstartup.asm,v 1.1.1.1 2003/11/17 19:05:55 nick Exp $
;

;----------------------------------------------------------;
;						      	   ;
;		    CSTARTUP.S01		      	   ;
;						      	   ;
; This file contains the Z80/HD64180 C startup routine     ;
; and must usually be tailored to suit customer's hardware.;
;						      	   ;
; Version:  4.00 [ 28/Apr/94 IJAR]                         ;
;----------------------------------------------------------;

;#define banking 1

; Nick #define proc64180 ((__TID__&0x010)==0x010)

$ io64180.inc

	NAME	CSTARTUP

	extern	_main
	extern	_int1_vector
	extern	_int2_vector
	extern	_timer0_vector
	extern	_csio_vector
	extern	_asci0_vector
	extern	_asci1_vector
	extern	abyte
	extern	copyr

;	EXTERN	main		; where to begin execution
	EXTERN	?C_EXIT		; where to go when program is done

;#ifdef banking
;#if 0 ; Nick proc64180
;
;CBAR_addr EQU	3AH	       ; define I/O ports to MMU registers
;CBR_addr  EQU	38H	       ; (See also defines in debug.s01 and l08.s01)
;
;	EXTERN  CBAR_value
;	EXTERN  CBR_value
;#endif

	EXTERN	?BANK_CALL_DIRECT_L08
;#endif

;---------------------------------------------------------------;
;  Forward declarations of segment used during initialization	;
;---------------------------------------------------------------;

	COMMON	INTVEC
	RSEG	RCODE
	RSEG	CONST
	RSEG	CSTR
	RSEG	CDATA0
	RSEG	CCSTR
	RSEG	DATA0
	RSEG	IDATA0
	RSEG	UDATA0
	RSEG	ECSTR
	RSEG	TEMP
	RSEG	WCSTR

;---------------------------------------------------------------;
;  CSTACK - The C stack segment					;
;  								;
;  Please, see in the link file lnk*.xcl how to increment	;
;  the stack size without having to reassemble cstartup.s01 !	;
;---------------------------------------------------------------;

	RSEG	CSTACK
	DEFS	0			; a bare minimum !

; -----------------------------------------------------------------------------

;	ASEG
;	ORG	0
;
;init_A
;	JP	init_C

;---------------------------------------------------------------;
;  RCODE - where the execution actually begins			;
;---------------------------------------------------------------;
	RSEG	RCODE
;init_C
init::
 .if 1
	di				; for boot.bin this is redundant
 .endif

 .if 1
	ld	a,'A'
	call	abyte+4000h
 .endif

 .if 0 ; now done by boot.bin before entering
	ld	a,4-8 ;0 also works, see gboot.s01 at label virtual_init
	out0	(CBR),a			; CA1 = logical 8000 = abs 0:4000

	;; Stack at the top of logical memory.
	ld	sp,0			; abs 1:0000 is just beyond stack top
 .endif

	ld	de,0			; destination address
	ld	hl,.SFB.CDATA0+4000h	; ending source address
	ld	bc,4000h		; starting source address
	call	copy_mem+4000h		; copy RCODE, etc from bank area to CA0

	jp	silly
silly:					; and start executing in the new spot

	ld	a,.low.vector_table
	out0	(IL),a			; set interrupt vectors low byte
	ld	a,.high.vector_table
	ld	i,a			; set interrupt vectors high byte

	in0	a,(CBR)
	ld	(_os_bank),a		; for the CMX scheduler, and others

 .if 1
	ei				; now got a stack and interrupt vectors
 .endif

;	LD	SP,(.SFE.CSTACK)-1	; from high to low address

;---------------------------------------------------------------;
; If hardware must be initiated from assembly or if interrupts	;
; should be on when reaching main, this is the place to insert	;
; such code.							;
;---------------------------------------------------------------;

;#ifdef banking
;#if 0 ; Nick proc64180
;
;;---------------------------------------------------------------;
;; Setting of MMU registers - see chapter "Linking" of manual.	;
;;---------------------------------------------------------------;
;
;	LD	A,CBAR_value			; set CBAR value
;	OUT0	(CBAR_addr),A
;
;	LD	A,CBR_value			; set CBR value
;	OUT0	(CBR_addr),A
;
;#endif
;#endif

;---------------------------------------------------------------;
; If it is not a requirement that static/global data is set	;
; to zero or to some explicit value at startup, the following	;
; line refering to seg_init can be deleted, or commented.	;
;---------------------------------------------------------------;

	CALL	seg_init

 .if 1
	ld	e,.high._rom_serial_no
	ld	d,0

	ld	a,(_os_bank)		; CBR value for kernel data segment
	ld	l,a
	ld	h,d ;0

	add	hl,hl
	add	hl,hl
	add	hl,hl
	add	hl,hl			; convert to absolute base address
	add	hl,de			; adjust for dest, carry into segment

	ld	e,h			; e = segment no. for copy destination
	ld	h,l
	ld	l,.low._rom_serial_no	; e:hl -> copy dest (in kernel data)

	ld	iy,0fc81h
	;ld	d,0			; d:iy -> copy source (in EPROM)

	ld	bc,5
	call	copyr

; not necessary as this will have been zeroed by seg_init (it's in UDATA0):
;	sub	a
;	ld	(_rom_sn+5),a
 .endif

 .if 1
	ld	a,'B'
	call	abyte
 .endif

 .if 1
; now there are the next stack structure:
;	+4 envp
;	+2 argv
; sp->	+0 argc
	ld	ix, 0
	add	ix, sp
	ld	l, (ix+4)
	ld	h, (ix+5)
	ld	(_environ),hl
	ld	c, (ix+2)
	ld	b, (ix+3)
;	ld	(__argv),bc		; 1st argument to main
	ld	e, (ix+0)
	ld	d, (ix+1)
;	ld	(__argc),de		; 2nd argument to main
 .endif

;#ifdef banking
	LD	HL,LWRD(_main)		; banked call to _main()
	LD	A,BYTE3(_main)
	CALL	?BANK_CALL_DIRECT_L08
;#else
;	CALL	_main			; non-banked call to _main()
;#endif

;---------------------------------------------------------------;
; Now when we are ready with our C program we must perform a    ;
; system-dependent action.  In this case we just stop.		;
;---------------------------------------------------------------;
; DO NOT CHANGE THE NEXT LINE OF CSTARTUP IF YOU WANT TO RUN    ;
; YOUR SOFTWARE WITH THE HELP OF THE C-SPY HLL DEBUGGER.        ;
;---------------------------------------------------------------;

	JP	?C_EXIT

;---------------------------------------------------------------;
; Copy initialized PROMmed code to shadow RAM and clear		;
; uninitialized variables.					;
;---------------------------------------------------------------;

seg_init:

;---------------------------------------;
; Zero out UDATA0			;
;---------------------------------------;
	LD	HL,.SFE.UDATA0
	LD	DE,.SFB.UDATA0
	CALL	zero_mem

;---------------------------------------;
; Copy CDATA0 into IDATA0		;
;---------------------------------------;
	LD	DE,.SFB.IDATA0		; destination address
	LD	HL,.SFE.CDATA0+4000h	; really 8:0000 + .SFE.(CDATA0)
	LD	BC,.SFB.CDATA0+4000h	; really 8:0000 + .SFB.(CDATA0)
	CALL	copy_mem

;---------------------------------------;
; Copy CCSTR into ECSTR			;
;---------------------------------------;
	LD	DE,.SFB.ECSTR		; destination address
	LD	HL,.SFE.CCSTR+4000h	; really 8:0000 + .SFE.(CCSTR)
	LD	BC,.SFB.CCSTR+4000h	; really 8:0000 + .SFB.(CCSTR)

	; Just fall in to the copy_mem function

;---------------------------------------;
; Copy memory				;
;---------------------------------------;
copy_mem:
	XOR	A
	SBC	HL,BC
	PUSH	BC
	LD	C,L
	LD	B,H				; BC - that many bytes
	POP	HL				; source address
	RET	Z				; If block size = 0 return now
	LDIR
	RET

;---------------------------------------;
; Clear memory				;
;---------------------------------------;
zero_mem:
	XOR	A
again:
	PUSH	HL
	SBC	HL,DE
	POP	HL
	RET	Z
	LD	(DE),A
	INC	DE
	JR	again

; Nick has added read/write data here (must be in RCODE)

		public	_os_bank

_os_bank:	defb	0		; the CBR value for CMX/UZI data seg

; Nick has added read/write data here (doesn't need to be in RCODE)

		rseg	UDATA0

		public	_rom_serial_no

_rom_serial_no:	defs	6		; for c:\uzi\kernel\main.c (hostname)

;---------------------------------------------------------------;
; Interrupt vectors must be inserted here by the user.		;
;---------------------------------------------------------------;

	COMMON	INTVEC

common_start::
	jp	init+4000h		; +4000h because we're not loaded yet

	org	common_start+0x08
	ei
	ret
	org	common_start+0x10
	ei
	ret
	org	common_start+0x18
	ei
	ret
	org	common_start+0x20
	ei
	ret
	org	common_start+0x28
	ei
	ret
	org	common_start+0x30
	ei
	ret
	org	common_start+0x38
	ei
	ret

	org	common_start+0x40

	public	vector_table

vector_table::
	defw	_int1_vector
	defw	_int2_vector
	defw	_timer0_vector
	defw	_timer1_vector
	defw	_dma0_vector
	defw	_dma1_vector
	defw	_csio_vector
	defw	_asci0_vector
	defw	_asci1_vector

	public	_timer1_vector

_timer1_vector::

	public	_dma0_vector

_dma0_vector::

	public	_dma1_vector

_dma1_vector::
	ei
	ret

; -----------------------------------------------------------------------------

; variables added by Nick (see above in this file, and ..\libc\cstartup.s01)

		public	_environ
		;public	__argc, __argv, _environ, _errno, ___cleanup

		rseg	UDATA0
;__argc:	defs	2
;__argv:	defs	2
_environ:	defs	2
;errno:		defs	2
;___cleanup:	defs	2

	ENDMOD	;common_start ;init_A

;---------------------------------------------------------------;
; Function/module: exit (int code)				;
;								;
; When C-SPY is used this code will automatically be replaced   ;
; by a 'debug' version of exit().				;
;---------------------------------------------------------------;
	MODULE	exit

	extern	abyte, ahexw, amess, acrlf

	PUBLIC	abort
	PUBLIC	?C_ABORT

	PUBLIC	exit
	PUBLIC	?C_EXIT

	RSEG	RCODE

?C_ABORT:
abort	EQU	?C_ABORT

;	ld	de,1			; abort() is equivalent to exit(1);

?C_EXIT:
exit	EQU	?C_EXIT

;--------------------------------------------------------------;
; The next line can be replaced by user defined code.          ;
;--------------------------------------------------------------;
 .if 1 ; new interface to apibus.mac for reboot control
	extern	_abflag

	call	amess
	defb	'kernel: rebooting: exitcode ',0
	ex	de,hl
	call	ahexw
	call	acrlf

	ld	hl,0
	; wait for last character to be sent @ 9600
reboot_delay:
	dec	hl
	ld	a,l
	or	h
	jr	nz,reboot_delay

	ld	a,0aah
	ld	(_abflag),a

	ei
 .else
 .if 1 ; diagnostic indication that program was exited
	ld	a,'X'
	call	abyte
 .else
	NOP
 .endif
 .endif
	JR	$			; loop forever

; -----------------------------------------------------------------------------

	END
