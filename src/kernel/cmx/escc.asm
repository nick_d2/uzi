; escc.asm
; Interrupt driven serial driver for built in Z180 ports (Hytech CMX)

; -----------------------------------------------------------------------------

$ io64180.inc

	extern	_sdevs
	extern	_K_OS_Intrp_Entry
	extern	_K_OS_Intrp_Exit
	extern	?BANK_CALL_DIRECT_L08
	extern	?BANK_FAST_LEAVE_L08
	extern	abyte

XMIT_SIZE equ	256	; must match cxfuncs.h, size of transmitter buffer
RECV_SIZE equ	256	; must match cxfuncs.h, size of reciever buffer

; must match cxfuncs.h, fields in SDEV structure:  ** ASSUMES 3 BYTE CODE PTRS!
sxmit	equ	0			; offset of embedded CMX XMIT structure
srecv	equ	13			; offset of embedded CMX RECV structure
sport	equ	13+13
stxvec	equ	13+13+1			; called by interrupt, returns l=chr
srxvec	equ	13+13+4			; called by interrupt, passed c=chr
smsvec	equ	13+13+7			; called by interrupt, passed c=bits
sesvec	equ	13+13+10		; called by interrupt, passed c=bits
stevec	equ	13+13+13		; vector to start asci/escc transmitter
stdvec	equ	13+13+16		; vector to stop asci/escc transmitter
sstat	equ	13+13+19		; address to read status (8530 02xxh)
stxdr	equ	13+13+20		; address to write tx data (8530 02xxh)
srxdr	equ	13+13+21		; address to read rx data (8530 02xxh)
swr1	equ	13+13+22		; wr1 contents (interrupt enable bits)
swr5	equ	13+13+23		; wr5 contents (modem control outputs)
swr15	equ	13+13+24		; wr15 contents (modem control int ena)
sinint	equ	13+13+25		; says whether wr1 really reflects wr1!
sdvlen	equ	13+13+26		; byte size of block per serial port

; -----------------------------------------------------------------------------

	rseg	CODE

	public	_escc0_setup

_escc0_setup::
	push	bc
	push	de

	ld	iy,_sdevs+2*sdvlen
	ld	(iy+sstat),.low.AC8530
	ld	(iy+stxdr),.low.AD8530
	ld	(iy+srxdr),.low.AD8530
	ld	(iy+stevec),.low._escc_tx_enable
	ld	(iy+stevec+1),.high._escc_tx_enable
	ld	(iy+stevec+2),BYTE3 _escc_tx_enable

	ld	bc,AC8530
	di				; so int doesn't corrupt address reg
	in	(c) ;f,(c)		; reset pointer bits

	ld	d,9
	out	(c),d			; address WR9
	ld	a,00000010b
	out	(c),a			; mie off, status lo, no vector

	nop				; ld d,9
	out	(c),d			; address WR9
	ld	a,10000010b
	out	(c),a			; channel reset A, status lo, no vector

	; allow at least 4 clock cycles for reset

	ld	d,5
	out	(c),d			; address WR5
	ld	a,11101010b
	out	(c),a			; dtr on, 8 data, tx enable, rts on
	ld	(iy+swr5),a		; set up shadow for swr5sb, swr5rb

	ld	d,2
	out	(c),d			; address WR2
	sub	a
	out	(c),a			; redundantly set up interrupt vector

	ld	d,4
	out	(c),d			; address WR4
	ld	a,01000100b		; *16 with 1 stop bit, no parity
	out	(c),a

	ld	d,3
	out	(c),d			; address WR3
	ld	a,11000001b
	out	(c),a			; 8 data, auto disable, rx enable

	ld	d,15
	out	(c),d			; address WR15
	sub	a
	out	(c),a			; modem status interrupts disabled
	ld	(iy+swr15),a		; set up shadow for swr15s, swr15r

	dec	d			; ld d,14
	out	(c),d			; address WR14
	sub	a
	out	(c),a			; disable baud rate generator

	ld	d,12
	out	(c),d			; address WR12
	ld	a,6 ; 10=38400 6=57600 4=76800 2=115200 (RPB: added divisor 10)
	out	(c),a			; divisor 0002h indicates 115200 bps
	inc	d			; ld d,13
	out	(c),d			; address WR13
	sub	a
	out	(c),a			; divisor 0002h indicates 115200 bps
	inc	d			; ld d,14
	out	(c),d			; address WR14
	ld	a,00000010b
	out	(c),a			; set pclk input
	nop				; ld d,14
	out	(c),d			; address WR14
	or	1
	out	(c),a			; enable baud rate generator
	ld	d,11
	out	(c),d			; address WR11
	ld	a,01010010b		; use baud rate generator
	out	(c),a

	ld	d,1
	out	(c),d			; address WR1
	ld	a,00010001b
	out	(c),a			; rx int on all, no tx int, ext int
	ld	(iy+swr1),a		; set up shadow for swr1sb, swr1rb

	ld	a,00010000b		; reset ext status ints
	out	(c),a

	ld	d,9
	out	(c),d			; address WR9
	ld	a,00001010b		; mie on, status lo, no vector
	out	(c),a

; not yet... this will be done by the escc1 setup routine when both ports ok
;	in0	a,(ITC)
;	or	INT2SW
;	out0	(ITC),a

	ei

	pop	de
	pop	bc
	jp	?BANK_FAST_LEAVE_L08

	public	_escc1_setup

_escc1_setup::
	push	bc
	push	de

	ld	iy,_sdevs+3*sdvlen
	ld	(iy+sstat),.low.BC8530
	ld	(iy+stxdr),.low.BD8530
	ld	(iy+srxdr),.low.BD8530
	ld	(iy+stevec),.low._escc_tx_enable
	ld	(iy+stevec+1),.high._escc_tx_enable
	ld	(iy+stevec+2),BYTE3 _escc_tx_enable

	ld	bc,BC8530
	di				; so int doesn't corrupt address reg
	in	(c) ;f,(c)		; reset pointer bits

	ld	d,9
	out	(c),d			; address WR9
	ld	a,00000010b
	out	(c),a			; mie off, status lo, no vector

	nop				; ld d,9
	out	(c),d			; address WR9
	ld	a,01000010b
	out	(c),a			; channel reset B, status lo, no vector

	; allow at least 4 clock cycles for reset

	ld	d,5
	out	(c),d			; address WR5
	ld	a,11101010b
	out	(c),a			; dtr on, 8 data, tx enable, rts on
	ld	(iy+swr5),a		; set up shadow for swr5sb, swr5rb

	ld	d,2
	out	(c),d			; address WR2
	sub	a
	out	(c),a			; redundantly set up interrupt vector

	ld	d,4
	out	(c),d			; address WR4
	ld	a,01000100b		; *16 with 1 stop bit, no parity
	out	(c),a

	ld	d,3
	out	(c),d			; address WR3
	ld	a,11000001b
	out	(c),a			; 8 data, auto disable, rx enable

	ld	d,15
	out	(c),d			; address WR15
	sub	a
	out	(c),a			; modem status interrupts disabled
	ld	(iy+swr15),a		; set up shadow for swr15s, swr15r

	dec	d			; ld d,14
	out	(c),d			; address WR14
	sub	a
	out	(c),a			; disable baud rate generator

	ld	d,12
	out	(c),d			; address WR12
	ld	a,10 ; 10=38400 6=57600 4=76800 2=115200 (RPB: added divisor 10)
					; RPB: set speed to 38400 on escc2
	out	(c),a			; divisor 0002h indicates 115200 bps
	inc	d			; ld d,13
	out	(c),d			; address WR13
	sub	a
	out	(c),a			; divisor 0002h indicates 115200 bps
	inc	d			; ld d,14
	out	(c),d			; address WR14
	ld	a,00000010b
	out	(c),a			; set pclk input
	nop				; ld d,14
	out	(c),d			; address WR14
	inc	a			; or 1
	out	(c),a			; enable baud rate generator
	ld	d,11
	out	(c),d			; address WR11
	ld	a,01010010b		; use baud rate generator
	out	(c),a

	ld	d,1
	out	(c),d			; address WR1
	ld	a,00010001b
	out	(c),a			; rx int on all, no tx int, ext int
	ld	(iy+swr1),a		; set up shadow for swr1sb, swr1rb

; this is only done for S3DEV, and is intended to disconnect LAN optocoupler
	ld	d,5
	out	(c),d			; address WR5
	ld	a,11101000b
	out	(c),a			; dtr on, 8 data, tx enable, rts off
	ld	(iy+swr5),a		; update our knowledge of wr5 contents

	ld	a,00010000b		; reset ext status ints
	out	(c),a

	ld	d,9
	out	(c),d			; address WR9
	ld	a,00001010b		; mie on, status lo, no vector
	out	(c),a

	in0	a,(ITC)
	or	INT2SW
	out0	(ITC),a

	ei

	pop	de
	pop	bc
	jp	?BANK_FAST_LEAVE_L08

; -----------------------------------------------------------------------------

	public	_escc_tx_enable

_escc_tx_enable::
	push	bc
	push	de

;	ld	iy,_sdevs+0*sdvlen
	push	de
	pop	iy
	di
 .if 1
	ld	hl,LWRD swr1te
	ld	a,BYTE3 swr1te
	call	?BANK_CALL_DIRECT_L08
 .else
	call	swr1te
 .endif
	ei

	pop	de
	pop	bc
;	ret
	jp	?BANK_FAST_LEAVE_L08

	public	_escc_tx_disable

_escc_tx_disable::
	push	bc
	push	de

;	ld	iy,_sdevs+0*sdvlen
	push	de
	pop	iy
	di
 .if 1
	ld	hl,LWRD swr1td
	ld	a,BYTE3 swr1td
	call	?BANK_CALL_DIRECT_L08
 .else
	call	swr1td
 .endif
	ei

	pop	de
	pop	bc
;	ret
	jp	?BANK_FAST_LEAVE_L08

; -----------------------------------------------------------------------------

	rseg	RCODE

	public	_int2_vector

_int2_vector::
 .if 1
 ;ld a,'.'
 ;out0 (TDR1),a
	call	_K_OS_Intrp_Entry
 .else
	push	af
	push	bc
	push	de
	push	hl
	push	iy
 .endif

 .if 1
	ld	bc,AC8530
	in	(c) ;f,(c)		; reset pointer bits

	ld	d,9
	out	(c),d			; address WR9
	ld	a,00000010b
	out	(c),a			; mie off, status lo, no vector
	ei				; allow other interrupts to occur
 .endif

 .if 1
 .if 1 ; prevent overrun by polling the ports after each interrupt
	ld	hl,LWRD si23iv
	ld	a,BYTE3 si23iv
	call	?BANK_CALL_DIRECT_L08

	; routine to allow arbitrary tests for rx char (prevents overrun)
	ld	bc,AC8530		; enter with interrupts disabled!!
	in	a,(c)			; read rr0 (channel a = s2dev)
	rra				; test bit 0 = rx char available

	ld	hl,LWRD si2rx
	ld	a,BYTE3 si2rx
	call	c,?BANK_CALL_DIRECT_L08	; something ready, use the normal si2rx
					; (this clobbers bc if char received)
	ld	bc,BC8530
	in	a,(c)			; read rr0
	rra				; test bit 0 = rx char available

	ld	hl,LWRD si3rx
	ld	a,BYTE3 si3rx
	call	c,?BANK_CALL_DIRECT_L08	; something ready, use the normal si2rx
					; (this clobbers bc if char received)
 .else
	ld	hl,LWRD si23
	ld	a,BYTE3 si23
	call	?BANK_CALL_DIRECT_L08
 .endif
 .else
	call	si23
 .endif

 .if 1
	ld	bc,AC8530
	di				; prevent ints occurring immediately
	in	(c) ;f,(c)		; reset pointer bits

	ld	d,9
	out	(c),d			; address WR9
	ld	a,00001010b
	out	(c),a			; mie on, status lo, no vector
 .endif

 .if 1
	call	_K_OS_Intrp_Exit
 .else
	pop	iy
	pop	hl
	pop	de
	pop	bc
	pop	af
	ei
	ret
 .endif

; -----------------------------------------------------------------------------

	rseg	CODE

 .if 0 ; 17sep02 routine to allow arbitrary tests for rx char (prevents overrun)
 .if 1 ; 17sep02 enhancement for extra safety against overruns (esp. @ 6.144 mhz)
public	si23

si23:
 .if 1
	ld	hl,LWRD si23iv
	ld	a,BYTE3 si23iv
	call	?BANK_CALL_DIRECT_L08
 .else
	call	LWRD si23iv		; first process interrupt via vector
 .endif
	; fall into si23rx routine (it could also be useful during other ints!)
 .endif

public	si23rx

si23rx:	; routine to allow arbitrary tests for rx char (prevents overrun)
	ld	bc,AC8530		; enter with interrupts disabled!!
	in	a,(c)			; read rr0 (channel a = s2dev)
	rra				; test bit 0 = rx char available
 .if 1
	ld	hl,LWRD si2rx
	ld	a,BYTE3 si2rx
	call	c,?BANK_CALL_DIRECT_L08	; something ready, use the normal si2rx
 .else
	call	c,LWRD si2rx		; something ready, use the normal si2rx
 .endif
					; (this clobbers bc if char received)
	ld	bc,BC8530
	in	a,(c)			; read rr0
	rra				; test bit 0 = rx char available
 .if 1
	jp	nc,?BANK_FAST_LEAVE_L08	; nothing ready, no-op (ints disabled)
 .else
	ret	nc			; nothing ready, no-op (ints disabled)
 .endif
	; fall into the same si3rx routine as used by the interrupt system
	jp	LWRD si3rx
 .endif

 .if 0
silly: ; enter with special receive condition status byte in e
 ld a,e
 rrca
 rrca
 rrca
 rrca
 and 7
 add a,'0'
 rst 18h ;call abyte
 ret
 .endif

 .if 0
sillyx: ; enter with modem control input status byte in e
 ld a,e
 and 28h
 rrca ; 14h
 rrca ; 0ah
 rrca ; 05h
 add a,'0'
 rst 18h ;call abyte
 ret
 .endif

si3tx:
 .if 1
	ld	iy,_sdevs+3*sdvlen
 .endif

 .if 1
	ld	de,_sdevs+3*sdvlen	; de = 1st parameter (word16)

	ld	hl,(_sdevs+3*sdvlen+stxvec)
	ld	a,(_sdevs+3*sdvlen+stxvec+2)
	call	?BANK_CALL_DIRECT_L08

	inc	h			; hl = 0ffffh indicates no char for tx
	jp	z,LWRD swr1td

	; client has provided character for tx, send it
	ld	bc,BD8530
	out	(c),l			; send char on behalf of client
 .else
	ld	bc,(_sdevs+3*sdvlen+stxvec)
	call	0eb53h			; winved
 di ;;!! 24may02 for no good reason
	jp	c,swr1td ;jr c,si3td

	; client has provided character for tx, send it
	ld	bc,BD8530
	out	(c),e			; send char on behalf of client
 .endif
 .if 1
	jp	?BANK_FAST_LEAVE_L08
 .else
	ret ;jr	si23			; check for further interrupts pending
 .endif

;si3td:	call	swr1td			; serial write register 1 tx disable
;	ret ;jr si23			; check for further interrupts pending

 .if 1	; 16dec01 so that swint can be a regular modem status handler
si3ms:
 .if 0
	ld	iy,_sdevs+3*sdvlen
 .endif
	in	e,(c)			; read modem status inputs for client

 .if 0
 call sillyx
 .endif

	ld	a,00010000b		; reset ext status interrupts
	out	(c),a

 .if 1
	ld	c,e			; c = 2nd parameter (byte)
	ld	de,_sdevs+3*sdvlen	; de = 1st parameter (word16)

	ld	hl,(_sdevs+3*sdvlen+smsvec)
	ld	a,(_sdevs+3*sdvlen+smsvec+2)
 .if 1
	jp	LWRD ?BANK_JMP_DIRECT_L08
 .else
	jp	?BANK_CALL_DIRECT_L08
 .endif
 .else
	ld	bc,(_sdevs+3*sdvlen+smsvec)
;	call	0eb53h			; winved, ignore the cf return for now
;	ret ;jr	si23			; check for further interrupts pending
	jp	0eb53h			; winved, ignore the cf return for now
 .endif
 .else
si3ms:	; bc is already set up to read channel b modem status
;	call	swint
;	ret ;jr	si23			; check for further interrupts pending
	jp	swint
 .endif

si3rx:
 .if 0
	ld	iy,_sdevs+3*sdvlen
 .endif

	ld	c,low BD8530
 .if 1
	in	c,(c)			; c = 2nd parameter (byte)
	ld	de,_sdevs+3*sdvlen	; de = 1st parameter (word16)

	ld	hl,(_sdevs+3*sdvlen+srxvec)
	ld	a,(_sdevs+3*sdvlen+srxvec+2)
 .if 1
	jp	LWRD ?BANK_JMP_DIRECT_L08
 .else
	jp	?BANK_CALL_DIRECT_L08
 .endif
 .else
	in	e,(c)			; read character for client

	ld	bc,(_sdevs+3*sdvlen+srxvec)
;	call	0eb53h			; winved, ignore the cf return for now
;	ret ;jr	si23			; check for further interrupts pending
	jp	0eb53h			; winved, ignore the cf return for now
 .endif

si3es:	dec	d			; ld d,1
	out	(c),d			; select rr1
 .if 0
	ld	iy,_sdevs+3*sdvlen
 .else
	nop
 .endif
	in	e,(c)			; read special receive condition status

 .if 0
 call silly
 .endif

 .if 0	; 16dec01 when nothing pending, go to clkint via s3dev error status
 .if 1	; 16oct02 make sure we don't depend on ckimsr when the hardware is new
	ld	a,(hwtype)
	or	a
	jr	nz,skpclk
 .endif
	ld	a,e
	and	01110000b		; bit 6=framing, 5=overrun, 4=parity
	;jp	z,int2c			; in ser.mac for no good reason
	jr	z,int2c			; 14jun02 now here, & proper ret logic
 .if 1	; 16oct02 make sure we don't depend on ckimsr when the hardware is new
skpclk:
 .endif
 .endif

 .if 0
 ld a,'%'
 rst 18h ;call abyte
 .endif

	ld	a,00110000b		; error reset
	out	(c),a

 .if 1
	ld	c,e			; c = 2nd parameter (byte)
	ld	de,_sdevs+3*sdvlen	; de = 1st parameter (word16)

	ld	hl,(_sdevs+3*sdvlen+sesvec)
	ld	a,(_sdevs+3*sdvlen+sesvec+2)
 .if 1
	jp	LWRD ?BANK_JMP_DIRECT_L08
 .else
	jp	?BANK_CALL_DIRECT_L08
 .endif
 .else
	ld	bc,(_sdevs+3*sdvlen+sesvec)
;	call	0eb53h			; winved, ignore the cf return for now
;	ret ;jr	si23			; check for further interrupts pending
	jp	0eb53h			; winved, ignore the cf return for now
 .endif

 .if 0	; 14jun02 this is now moved to ser.inc as it was misleading
int2c:
 .if noints
	ld	bc,ckimsr
	in	a,(c)			; reset irq
	ret ;jr si23
 .else
	wjpd	clkint			; clock is interrupting
	; clkint should be modified,
	; as it no longer needs to preserve de/hl/iy
 .endif
 .endif

; -----------------------------------------------------------------------------

 .if 1 ; 17sep02 enhancement for extra safety against overruns (esp. 6.144 MHz)
si23iv: ; routine to perform interrupt vectoring... not the main int. handler!
 .else
public	si23

si23:	; without the enhancement, this routine is also the main int. handler!
 .endif
 .if 0 ; 20may02 hope this works (it does, but see below)
 ld a,'.'
 call abyte
 .endif
	ld	bc,BC8530
	ld	d,2
	out	(c),d			; select rr2
	ld	hl,LWRD si23jt-200h	; we'll also rely on d=2 later
	in	e,(c)			; read interrupt vector with status
 .if 0;xyz1	; 15dec01 diagnostic interrupt vector output, requires abfast
 ld a,e
 rrca
 add a,10h
 ;bit 1,e ; 0 indicates tx/rx int, 1 indicates modem/error int
 ;call nz,abyte
 out0 (TDR1),a
 .endif
	add	hl,de
	jp	(hl)

si23jt:	jr	si3tx
	jr	si3ms
	jr	si3rx
	jr	si3es
	jr	si2tx
	jr	si2ms
	jr	si2rx
	jr	si2es

; -----------------------------------------------------------------------------

si2tx:
 .if 1
	ld	iy,_sdevs+2*sdvlen
 .endif

 .if 1
	ld	de,_sdevs+2*sdvlen	; de = 1st parameter (word16)

	ld	hl,(_sdevs+2*sdvlen+stxvec)
	ld	a,(_sdevs+2*sdvlen+stxvec+2)
	call	?BANK_CALL_DIRECT_L08

	inc	h			; hl = 0ffffh indicates no char for tx
	jp	z,LWRD swr1td

	; client has provided character for tx, send it
	ld	bc,AD8530
	out	(c),l			; send char on behalf of client
 .else
	ld	bc,(_sdevs+2*sdvlen+stxvec)
	call	0eb53h			; winved
 di ;;!! 24may02 for no good reason
	jp	c,swr1td ;jr c,si2td

	; client has provided character for tx, send it
	ld	bc,AD8530
	out	(c),e			; send char on behalf of client
 .endif
; ld a,e
; jp abyte
 .if 1
	jp	?BANK_FAST_LEAVE_L08
 .else
	ret ;jr	si23			; check for further interrupts pending
 .endif

;si2td:	call	swr1td			; serial write register 1 tx disable
;	ret ;jr	si23			; check for further interrupts pending

si2ms:
 .if 0
	ld	iy,_sdevs+2*sdvlen
 .endif

	inc	c 			; ld c,low AC8530
	in	e,(c)			; read modem status inputs for client

	ld	a,00010000b		; reset ext status interrupts
	out	(c),a

 .if 1
	ld	c,e			; c = 2nd parameter (byte)
	ld	de,_sdevs+2*sdvlen	; de = 1st parameter (word16)

	ld	hl,(_sdevs+2*sdvlen+smsvec)
	ld	a,(_sdevs+2*sdvlen+smsvec+2)
 .if 1
	jp	LWRD ?BANK_JMP_DIRECT_L08
 .else
	jp	?BANK_CALL_DIRECT_L08
 .endif
 .else
	ld	bc,(_sdevs+2*sdvlen+smsvec)
;	call	0eb53h			; winved, ignore the cf return for now
;	ret ;jr	si23			; check for further interrupts pending
	jp	0eb53h			; winved, ignore the cf return for now
 .endif

si2rx:
 .if 0
	ld	iy,_sdevs+2*sdvlen
 .endif

	ld	c,low AD8530
 .if 1
	in	c,(c)			; c = 2nd parameter (byte)
	ld	de,_sdevs+2*sdvlen	; de = 1st parameter (word16)

	ld	hl,(_sdevs+2*sdvlen+srxvec)
	ld	a,(_sdevs+2*sdvlen+srxvec+2)
 .if 1
	jp	LWRD ?BANK_JMP_DIRECT_L08
 .else
	jp	?BANK_CALL_DIRECT_L08
 .endif
 .else
	in	e,(c)			; read character for client

	ld	bc,(_sdevs+2*sdvlen+srxvec)
;	call	0eb53h			; winved, ignore the cf return for now
;	ret ;jr	si23			; check for further interrupts pending
	jp	0eb53h			; winved, ignore the cf return for now
 .endif

si2es:	inc	c 			; ld c,low AC8530
	dec	d			; ld d,1
	out	(c),d			; select rr1
 .if 0
	ld	iy,_sdevs+2*sdvlen
 .else
	nop
 .endif
	in	e,(c)			; read special receive condition status

	ld	a,00110000b		; error reset
	out	(c),a

 .if 1
	ld	c,e			; c = 2nd parameter (byte)
	ld	de,_sdevs+2*sdvlen	; de = 1st parameter (word16)

	ld	hl,(_sdevs+2*sdvlen+sesvec)
	ld	a,(_sdevs+2*sdvlen+sesvec+2)
 .if 1
	jp	LWRD ?BANK_JMP_DIRECT_L08
 .else
	jp	?BANK_CALL_DIRECT_L08
 .endif
 .else
	ld	bc,(_sdevs+2*sdvlen+sesvec)
;	call	0eb53h			; winved, ignore the cf return for now
;	ret ;jr	si23			; check for further interrupts pending
	jp	0eb53h			; winved, ignore the cf return for now
 .endif

; rather silly routine provided to save code space in the handlers above:

?BANK_JMP_DIRECT_L08:
	call	?BANK_CALL_DIRECT_L08
	jp	?BANK_FAST_LEAVE_L08

; -----------------------------------------------------------------------------
; utility subroutines for use by our clients - 8530 ports (serial 2/3)

swr1te:	; serial write register 1 transmit enable - enter with di
 ;ld a,(iy+sdev)
 ;cp s2dev
 ;ld a,'e'
 ;call z,abyte

; ld a,(iy+sdev)
; cp s2dev
; jr nz,twatx
; call twatx
; jp abyte
;twatx:
	bit	1,(iy+swr1)
; ld a,'a'
 .if 1
	jp	nz,?BANK_FAST_LEAVE_L08	; transmit interrupt already enabled
 .else
	ret	nz			; transmit interrupt already enabled
 .endif

 .if 1
	push	iy
	pop	de			; de = 1st parameter (word16)

	ld	l,(iy+stxvec)
	ld	h,(iy+stxvec+1)
	ld	a,(iy+stxvec+2)
	call	?BANK_CALL_DIRECT_L08

	inc	h			; hl = 0ffffh indicates no char for tx
; ld a,'b'
 .if 1
	jp	z,?BANK_FAST_LEAVE_L08
 .else
	ret	z
 .endif
 .else
	ld	c,(iy+stxvec)
	ld	b,(iy+stxvec+1)
	call	0eb53h			; winved
; ld a,'b'
	ret	c			; there was really nothing to transmit
	ld	l,e
 .endif

	ld	e,2
 .if 1
 push hl ; oops !
	ld	hl,LWRD swr1sb
	ld	a,BYTE3 swr1sb
	call	?BANK_CALL_DIRECT_L08	; explicitly enable transmit interrupt
 pop hl ; oops !
 .else
	call	LWRD swr1sb		; explicitly enable transmit interrupt
 .endif

	ld	c,(iy+stxdr)
	out	(c),l			; send char on behalf of client
; ld a,(iy+sdev)
; cp s2dev
; ret nz
; ld a,55h
; call abyte
; ld a,l
; jp abyte
; ld a,'c'
 .if 1
	jp	?BANK_FAST_LEAVE_L08
 .else
	ret
 .endif

swr1sb:	; serial write register 1 set bits - enter with di
	ld	c,(iy+sstat)
	ld	b,2 ;(iy+sbase)
	ld	d,1
	out	(c),d
	ld	a,e
	or	(iy+swr1)
	out	(c),a
	ld	(iy+swr1),a
 .if 1
	jp	?BANK_FAST_LEAVE_L08
 .else
	ret
 .endif

swr1td:	; serial write register 1 transmit disable - enter with di
 ;ld a,(iy+sdev)
 ;cp s2dev
 ;ld a,'d'
 ;call z,abyte
	ld	e,0fdh
swr1rb:	; serial write register 1 reset bits - enter with di
	ld	c,(iy+sstat)
	ld	b,2 ;(iy+sbase)
	ld	d,1
	out	(c),d
	ld	a,e
	and	(iy+swr1)
	out	(c),a
	ld	(iy+swr1),a
 .if 1
	jp	?BANK_FAST_LEAVE_L08
 .else
	ret
 .endif

swr5sb:	; serial write register 5 set bits - enter with di
	ld	c,(iy+sstat)
	ld	b,2 ;(iy+sbase)
	ld	d,5
	out	(c),d
	ld	a,e
	or	(iy+swr5)
	out	(c),a
	ld	(iy+swr5),a
 .if 1
	jp	?BANK_FAST_LEAVE_L08
 .else
	ret
 .endif

swr5rb:	; serial write register 5 reset bits - enter with di
	ld	c,(iy+sstat)
	ld	b,2 ;(iy+sbase)
	ld	d,5
	out	(c),d
	ld	a,e
	and	(iy+swr5)
	out	(c),a
	ld	(iy+swr5),a
 .if 1
	jp	?BANK_FAST_LEAVE_L08
 .else
	ret
 .endif

swr15s:	; serial write register 15 set bits - enter with di
	ld	c,(iy+sstat)
	ld	b,2 ;(iy+sbase)
	ld	d,15
	out	(c),d
	ld	a,e
	or	(iy+swr15)
	out	(c),a
	ld	(iy+swr15),a
 .if 1
	jp	?BANK_FAST_LEAVE_L08
 .else
	ret
 .endif

swr15r:	; serial write register 15 reset bits - enter with di
	ld	c,(iy+sstat)
	ld	b,2 ;(iy+sbase)
	ld	d,15
	out	(c),d
	ld	a,e
	and	(iy+swr15)
	out	(c),a
	ld	(iy+swr15),a
 .if 1
	jp	?BANK_FAST_LEAVE_L08
 .else
	ret
 .endif

; -----------------------------------------------------------------------------

	END
