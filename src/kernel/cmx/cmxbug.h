#if (!defined(INC_CMXBUG))
#define INC_CMXBUG	1

/* include this file after including cxfuncs.h so cmx_reentrant is
	defined properly.
*/
#ifndef cmx_reentrant
#define cmx_reentrant
#endif
#ifndef cmx_xdata
#define cmx_xdata
#endif
#ifndef cmx_code 
#define cmx_code const
#endif

#if (defined(CMX_INIT_MODULE))
extern byte cmxbug_slot;
extern void cmxbug(void) cmx_reentrant;
byte CMXBUG_ACTIVE;
#if (defined(WINBUG))
extern byte cmx_xdata bug_step_one;
extern word16 cmx_xdata bug_step_count;
#endif	/* #if (defined(WINBUG)) */
#endif	/* #if (defined(CMX_INIT_MODULE)) */

#if (defined(WINBUG))
/* user-callable cmxbug functions */
void setbaud(void);
void cmxbug_activate(void) cmx_reentrant;
#endif	/* #if (defined(WINBUG)) */

#endif		/* #if (!defined(INC_CMXBUG)) */
