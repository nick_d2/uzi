; diag.asm
; Polled mode serial output for built in Z180 port SERIAL 0 (Hytech CMX)

; -----------------------------------------------------------------------------

$ io64180.inc

;	.area	_CODE
	rseg	RCODE

; -----------------------------------------------------------------------------

	public	_abyte

_abyte::
 .if 0 ; SDCC
	ld	hl,2
	add	hl,sp
	ld	a,(hl)
 .else ; IAR
	ld	a,e
 .endif

	public	abyte

abyte::
 .if 1
 ei ; a bit naughty but avoids inadvertent BAD COMMAND when line is busy
 .endif

 .if 0 ; 0=enable abytes, 1=disable abytes (doesn't affect the code size)
 ret
 .else
	push	af
 .endif

 .if 0 ; checking to see when memory corruption occurs
 extern _osBank
 ld a,(_osBank)
 cp 0
 jr z,ok
 pop af
 ld a,'_'
 push af
ok:
 .endif

L1$:	in0	a,(STAT1) ;(STAT0)
	and	10b
	jr	z,L1$

	pop	af
	out0	(TDR1),a ;(TDR0),a

 .if 0
 di
 .endif
 .if 0
 ei
 .endif
	ret

	public	_acrlf

_acrlf::

	public	acrlf

acrlf::
	ld	a,0dh
	call	abyte
	ld	a,0ah
	jr	abyte

	public	_ahexw

_ahexw::
 .if 0 ; SDCC
	ld	hl,2
	add	hl,sp
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
 .endif
	ex	de,hl

	public	ahexw

ahexw::
	ld	a,h
	call	ahexb
	ld	a,l

	public	ahexb

ahexb::
	push	af
	rrca
	rrca
	rrca
	rrca
	call	ahexn
	pop	af

	public	ahexn

ahexn::
	and	0fh
	add	a,90h
	daa
	adc	a,40h
	daa
	jr	abyte

	public	_amess

_amess::
 .if 0 ; SDCC
	ld	hl,2
	add	hl,sp
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
 .endif
	ex	de,hl

amess_loop:
	ld	a,(hl)
	inc	hl
	or	a
	ret	z

	call	abyte
	jr	amess_loop

	public	amess

amess::
	ex	(sp),hl
	push	af

 .if 1
	call	amess_loop
 .else
L01$:	ld	a,(hl)
	inc	hl
	or	a
	jr	z,L02$

	call	abyte
	jr	L01$

L02$:
 .endif
	pop	af
	ex	(sp),hl
	ret

; -----------------------------------------------------------------------------

	END
