; asci.asm
; Interrupt driven serial driver for built in Z180 ports (Hytech CMX)

; -----------------------------------------------------------------------------

$ io64180.inc

	extern	_sdevs
	extern	_K_OS_Intrp_Entry
	extern	_K_OS_Intrp_Exit
	extern	?BANK_CALL_DIRECT_L08
	extern	?BANK_FAST_LEAVE_L08
	extern	abyte

XMIT_SIZE equ	256	; must match cxfuncs.h, size of transmitter buffer
RECV_SIZE equ	256	; must match cxfuncs.h, size of reciever buffer

; must match cxfuncs.h, fields in SDEV structure:  ** ASSUMES 3 BYTE CODE PTRS!
sxmit	equ	0			; offset of embedded CMX XMIT structure
srecv	equ	13			; offset of embedded CMX RECV structure
sport	equ	13+13
stxvec	equ	13+13+1			; called by interrupt, returns l=chr
srxvec	equ	13+13+4			; called by interrupt, passed c=chr
smsvec	equ	13+13+7			; called by interrupt, passed c=bits
sesvec	equ	13+13+10		; called by interrupt, passed c=bits
stevec	equ	13+13+13		; vector to start asci/escc transmitter
stdvec	equ	13+13+16		; vector to stop asci/escc transmitter
sstat	equ	13+13+19		; address to read status (8530 02xxh)
stxdr	equ	13+13+20		; address to write tx data (8530 02xxh)
srxdr	equ	13+13+21		; address to read rx data (8530 02xxh)
swr1	equ	13+13+22		; wr1 contents (interrupt enable bits)
swr5	equ	13+13+23		; wr5 contents (modem control outputs)
swr15	equ	13+13+24		; wr15 contents (modem control int ena)
sinint	equ	13+13+25		; says whether wr1 really reflects wr1!
sdvlen	equ	13+13+26		; byte size of block per serial port

; -----------------------------------------------------------------------------

	rseg	CODE

	public	_asci0_setup

_asci0_setup::
;	push	bc
;	push	de

	ld	iy,_sdevs+0*sdvlen
	ld	(iy+sstat),STAT0
	ld	(iy+stxdr),TDR0
	ld	(iy+srxdr),RDR0
	ld	(iy+stevec),.low._asci_tx_enable
	ld	(iy+stevec+1),.high._asci_tx_enable
	ld	(iy+stevec+2),BYTE3 _asci_tx_enable

 .if 0 ; now done by gboot.s01
	ld	a,01110100b
	out0	(CNTLA0),a 		; enables, rts off, d8 pn s1, tend0
	;sub	a			; 115200 bps @ 18.432 MHz (ps = 0)
	;ld	a,20h			; 38400 bps @ 18.432 MHz (ps = 1)
	ld	a,22h			; 9600 bps @ 18.432 MHz (ps = 1)
	out0	(CNTLB0),a
 .endif

	ld	a,1000b
	out	(STAT0),a		; rx int enabled, tx int disabled
	ld	(iy+swr1),a		; set up shadow for ssrte, ssrtd

;	pop	de
;	pop	bc
	jp	?BANK_FAST_LEAVE_L08

	public	_asci1_setup

_asci1_setup::
;	push	bc
;	push	de

	ld	iy,_sdevs+1*sdvlen
	ld	(iy+sstat),STAT1
	ld	(iy+stxdr),TDR1
	ld	(iy+srxdr),RDR1
	ld	(iy+stevec),.low._asci_tx_enable
	ld	(iy+stevec+1),.high._asci_tx_enable
	ld	(iy+stevec+2),BYTE3 _asci_tx_enable

 .if 0 ; now done by gboot.s01
	ld	a,01110100b
	out0	(CNTLA1),a 		; enables, rts off, d8 pn s1, tend0
	;sub	a			; 115200 bps @ 18.432 MHz (ps = 0)
	;ld	a,20h			; 38400 bps @ 18.432 MHz (ps = 1)
	ld	a,22h			; 9600 bps @ 18.432 MHz (ps = 1)
	out0	(CNTLB1),a
 .endif

	ld	a,1000b
	out	(STAT1),a		; transmit interrupts disabled
	ld	(iy+swr1),a		; set up shadow for ssrte, ssrtd

;	pop	de
;	pop	bc
;	ret
	jp	?BANK_FAST_LEAVE_L08

; -----------------------------------------------------------------------------

	public	_asci_tx_enable

_asci_tx_enable::
	push	bc
	push	de

;	ld	iy,_sdevs+0*sdvlen
	push	de
	pop	iy
	di
 .if 1
	ld	hl,LWRD ssrte
	ld	a,BYTE3 ssrte
	call	?BANK_CALL_DIRECT_L08
 .else
	call	ssrte
 .endif
	ei

	pop	de
	pop	bc
;	ret
	jp	?BANK_FAST_LEAVE_L08

	public	_asci_tx_disable

_asci_tx_disable::
	push	bc
	push	de

;	ld	iy,_sdevs+0*sdvlen
	push	de
	pop	iy
	di
 .if 1
	ld	hl,LWRD ssrtd
	ld	a,BYTE3 ssrtd
	call	?BANK_CALL_DIRECT_L08
 .else
	call	ssrtd
 .endif
	ei

	pop	de
	pop	bc
;	ret
	jp	?BANK_FAST_LEAVE_L08

; -----------------------------------------------------------------------------

	rseg	RCODE

	public	_asci0_vector

_asci0_vector::
 .if 1
	call	_K_OS_Intrp_Entry
 .else
	push	af
	push	bc
	push	de
	push	hl
	push	iy
 .endif

	ld	iy,_sdevs+0*sdvlen

 .if 1
	inc	(iy+sinint)
	ld	a,(_sdevs+0*sdvlen+swr1) ;(iy+swr1)
	and	11110110b		; disable both tx and rx ints
	out0	(STAT0),a
	ei				; allow other interrupts to occur
 .endif

 .if 1
	ld	hl,LWRD si01
	ld	a,BYTE3 si01
	call	?BANK_CALL_DIRECT_L08
 .else
	call	si01
 .endif

 .if 1
	di				; prevent ints occurring immediately
	dec	(iy+sinint)
	ld	a,(_sdevs+0*sdvlen+swr1) ;(iy+swr1)
	out0	(STAT0),a		; re-enable tx and rx ints if needed
 .endif

 .if 1
	call	_K_OS_Intrp_Exit
 .else
	pop	iy
	pop	hl
	pop	de
	pop	bc
	pop	af
	ei
	ret
 .endif

	public	_asci1_vector

_asci1_vector::
 .if 1
	call	_K_OS_Intrp_Entry
 .else
	push	af
	push	bc
	push	de
	push	hl
	push	iy
 .endif

	ld	iy,_sdevs+1*sdvlen

 .if 1
	inc	(iy+sinint)
	ld	a,(_sdevs+1*sdvlen+swr1) ;(iy+swr1)
	and	11110110b		; disable both tx and rx ints
	out0	(STAT1),a
	ei				; allow other interrupts to occur
 .endif

 .if 1
	ld	hl,LWRD si01
	ld	a,BYTE3 si01
	call	?BANK_CALL_DIRECT_L08
 .else
	call	si01
 .endif

 .if 1
	di				; prevent ints occurring immediately
	dec	(iy+sinint)
	ld	a,(_sdevs+1*sdvlen+swr1) ;(iy+swr1)
	out0	(STAT1),a		; re-enable tx and rx ints if needed
 .endif

 .if 1
	call	_K_OS_Intrp_Exit
 .else
	pop	iy
	pop	hl
	pop	de
	pop	bc
	pop	af
	ei
	ret
 .endif

; -----------------------------------------------------------------------------

	rseg	CODE

si01:	; initial status read
	ld	c,(iy+sstat)
	ld	b,0 ;(iy+sbase)
	in	a,(c)

	; test for receiver error
	tst	01110000b		; ovrn, pe, fe
	jr	z,si01l0

	; receiver error, reset it
	res	2,c
	in	a,(c)
	and	11110111b		; reset efr
	out	(c),a

	set	2,c
	in	a,(c)			; repeat status read

si01l0:	; test for receiver ready
	tst	10000000b
	jr	z,si01l2

si01l1:	; receiver is ready, process 1 char
	ld	c,(iy+srxdr)
 .if 1
 .if 0 ; SDCC
	in	a,(c)

	push	af
	inc	sp
	push	iy
	call	_K_Update_Recv
	inc	sp
	inc	sp
	inc	sp
 .else ; IAR
	in	c,(c)			; c = 2nd parameter (byte)
	push	iy
	pop	de			; de = 1st parameter (word16)

;	call	_K_Update_Recv
;	ld	a,BYTE3 _K_Update_Recv
;	ld	hl,LWRD _K_Update_Recv
	ld	l,(iy+srxvec)
	ld	h,(iy+srxvec+1)
	ld	a,(iy+srxvec+2)
	call	LWRD ?BANK_CALL_DIRECT_L08
 .endif
 .else
	in	e,(c)

 .if 0
	ld	c,(iy+srxvec)
	ld	b,(iy+srxvec+1)
	call	0eb53h			; winved
 .else
	call	srxdft			; ignore the cf return for now
 .endif
 .endif

	ld	c,(iy+sstat)
	ld	b,0 ;(iy+sbase)
	in	a,(c)			; repeat status read
	tst	10000000b		; receiver ready again?
	jr	nz,si01l1		; go and receive a second char

si01l2:	; test for transmitter ready
	and	00000010b
 .if 1
	jp	z,?BANK_FAST_LEAVE_L08	; transmitter not ready
 .else
	ret	z			; transmitter not ready
 .endif

	bit	0,(iy+swr1)		; transmit interrupt enabled?
 .if 1
	jp	z,?BANK_FAST_LEAVE_L08	; transmitter not ready
 .else
	ret	z			; no, ignore transmitter ready status
 .endif

si01l3:	; transmitter is ready, process 1 char
 .if 1
 .if 0 ; SDCC
	push	iy
	call	_K_Update_Xmit
	inc	sp
	inc	sp
	jr	c,ssrtd			; serial status register tx disable
 .else ; IAR
	push	iy
	pop	de			; de = 1st parameter (word16)

;	call	_K_Update_Xmit
;	ld	a,BYTE3 _K_Update_Xmit
;	ld	hl,LWRD _K_Update_Xmit
	ld	l,(iy+stxvec)
	ld	h,(iy+stxvec+1)
	ld	a,(iy+stxvec+2)
	call	LWRD ?BANK_CALL_DIRECT_L08

	inc	h			; sets zf if hl was 0ffffh
	jr	z,ssrtd			; serial status register tx disable
 .endif

	ld	c,(iy+stxdr)
	ld	b,0 ;(iy+sbase)
	out	(c),l			; client has provided char for tx
 .else
 .if 0
	ld	c,(iy+stxvec)
	ld	b,(iy+stxvec+1)
	call	0eb53h			; winved
 .else
	call	stxdft
 .endif
	jr	c,ssrtd			; serial status register tx disable

	ld	c,(iy+stxdr)
	ld	b,0 ;(iy+sbase)
	out	(c),e			; client has provided char for tx
 .endif

	ld	c,(iy+sstat)
	in	a,(c)			; repeat status read
	and	00000010b		; transmitter ready again?
 .if 1
	jp	z,?BANK_FAST_LEAVE_L08	; transmitter not ready
 .else
	ret	z			; transmitter not ready
 .endif

	jr	si01l3			; go and transmit a second char

; -----------------------------------------------------------------------------
; utility subroutines for use by our clients - z180 ports (serial 0/1)

ssrte:	; serial status register transmit enable - enter with di
;if nick0 ; new addition 17sep02 to implement inter-byte delay on s0dev @ 2400 %
;	bit	2,(iy+sadlc)		; inter-byte delay enabled? (s0dev)
;	jr	nz,ssrtd		; yes, ensure tx int is always disabled
;endif	; %
	; new addition 07mar02 trying to fix double chars / corruption s0dev %
	bit	0,(iy+swr1)
 .if 1
	jp	nz,?BANK_FAST_LEAVE_L08	; transmit interrupt already enabled
 .else
	ret	nz			; transmit interrupt already enabled
 .endif
	; %
 ;ld a,(iy+sdev)
 ;cp s1dev
 ;ld a,'e'
 ;call z,abyte
	ld	e,1
ssrsb:	; serial status register set bits - enter with di
	ld	c,(iy+sstat)
	ld	b,0 ;(iy+sbase)
	ld	a,e
	or	(iy+swr1)
 .if 1
	ld	e,a
	ld	a,(iy+sinint)
	or	a
	jr	nz,ssrsb0
	out	(c),e
ssrsb0:	ld	(iy+swr1),e
 .else
	out	(c),a
	ld	(iy+swr1),a
 .endif
 .if 1
	jp	?BANK_FAST_LEAVE_L08
 .else
	ret
 .endif

ssrtd:	; serial status register transmit disable - enter with di
;	; new addition 07mar02 trying to fix double chars / corruption s0dev %
;	bit	0,(iy+swr1)
;	ret	z			; transmit interrupt already disabled
;	; %
 ;ld a,(iy+sdev)
 ;cp s1dev
 ;ld a,'d'
 ;call z,abyte
	ld	e,0feh
ssrrb:	; serial status register reset bits - enter with di
	ld	c,(iy+sstat)
	ld	b,0 ;(iy+sbase)
	ld	a,e
	and	(iy+swr1)
 .if 1
	ld	e,a
	ld	a,(iy+sinint)
	or	a
	jr	nz,ssrrb0
	out	(c),e
ssrrb0:	ld	(iy+swr1),e
 .else
	out	(c),a
	ld	(iy+swr1),a
 .endif
 .if 1
	jp	?BANK_FAST_LEAVE_L08
 .else
	ret
 .endif

; -----------------------------------------------------------------------------

	END
