; cxskv5b.asm

;******************************************************
;
;	Copyright (C) 2002
;	CMX Systems Inc.
;	12276 San Jose Blvd, #119
;	Jacksonville, FL 32223
;
;	All Rights Reserved
;
;*******************************************************
;
;		NOTE: for BANKED MEMORY MODELS
; Also this assembly file will work with the Archimedes l08.s01
; banked switching assembly module. This is for the 64180/Z180
; processors. For those if you that are trying to do bank switching
; with the Z80 MUST manipulate this code to match their bank switch
; code that they devise.
;
; NOTE: integers (words) are stored low byte first,high byte second
;

$ io64180.inc ; Nick

fwlink		equ	00h	;forward wait link
bwlink		equ	02h	;backward wait link
ftlink		equ	04h	;forward time link
btlink		equ	06h	;backward time link
tcbstate	equ	08h	;task state    
trig		equ 	0ah	;the number of triggers (starts) for task
priority	equ	0bh	;task priority, 0 is highest
tcbtimer	equ	0ch	;task countdown delay timer
nxttcb  	equ	0eh	;ptr to next TCB (task control block)
task_addr	equ	10h	;address of where task's code begins
stk_start	equ 	13h	;the tasks beginning stack's address
stk_save  	equ	15h	;the task's resume stack address
cbr_save	equ	1Dh	; Nick, used to get back to task's stack
bbr_save	equ	1Eh	; Nick, used to get back to task's code

; tcbstate low byte
RESOURCE	equ	01h	;waiting on resource
WAIT		equ	02h	;waiting
SEND_MESG	equ	08h	;waiting for task that recieved message,to wake me
WAIT_MESG	equ	10h	;waiting for message
FLAGS		equ	20h	;waiting on event
TIME_EXPIRED	equ	40h	;the time period specified has ellapsed

; tcbstate high byte
IDLE 		equ 	01h	;task not able to run, no triggers
READY 		equ	02h	;the task ready to run
RESUME		equ	04h	;the task ready to run, resume where it left off
RUNNING		equ	08h	;the task is the running task
TIME		equ	10h	;waiting on time

	extern	abyte
	extern	ahexw
	extern	acrlf
	extern	_stack_holder
	extern	_interrupt_stack
	extern	_activetcb
	extern	_active_priority
	extern	_cmx_tcb
	extern	_K_I_Intrp_Pipe_Out
	extern	_K_I_Timer_Task
	extern	_K_OS_Low_Power_Func
	extern  _TSLICE_SCALE
	extern  _SLICE_ON
	extern  _tslice_count

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	extern	_int_count		; now done here
	extern	_locked_out
	extern	_cmx_flag1
	extern	_ie_copy
	extern	_os_bank		; Nick... the CBR value for CMX data

preempted equ 01h
do_timer_tsk equ 02h
do_time_slice equ 04h
slice_enable equ 08h
do_coop_sched equ 10h
do_int_pipe equ 20h
idle_flag equ 40h
cmx_active equ 80h

bit_preempted equ 0
bit_do_timer_tsk equ 1
bit_do_time_slice equ 2
bit_slice_enable equ 3
bit_do_coop_sched equ 4
bit_do_int_pipe equ 5
bit_idle_flag equ 6
bit_cmx_active equ 7


SAVE_ALT_GR EQU 1	;Set to 1 if user or "C" compiler uses
			;alternate gr register set

LOW_POWER_ACTIVE EQU 0	;set to 1, to enable cmx_power function

SLICE_ENABLE EQU 1	;set to 1, to enable time slicing code testing

CMXTRACKER_ENABLE equ 0	;Set to 1, for CMXTracker capabilities

 .IF CMXTRACKER_ENABLE > 0
	extern	_previoustcb
	extern	_cmxtracker_in_task
	extern	_CMXTRACKER_ACTIVE
 .ENDIF

;#define proc64180 (__TID__&0x010=0x010)
;#if proc64180
; .if 0 ; Nick
;BBR	EQU	39H
; .endif
;#else
;	EXTERN	?BANK_SWITCH_PORT_L08
;#endif

	EXTERN	?BANK_CALL_DIRECT_L08
	EXTERN	?BANK_FAST_LEAVE_L08

; WARNING: Must be root code
	RSEG RCODE

	PUBLIC	_K_OS_Intrp_Entry
	PUBLIC	_K_OS_Intrp_Exit
	PUBLIC	_K_I_Sched
	PUBLIC  _K_I_Scheduler
	PUBLIC	_K_OS_Enable_Interrupts
	PUBLIC	_K_OS_Disable_Interrupts
	PUBLIC	_K_OS_Save_Interrupts
	PUBLIC  _K_OS_Restore_Interrupts

_K_I_Sched:
	push ix
	push af	;save all registers
	push bc
	push de
	push hl
	push iy
;if gr' regs used
 .IF SAVE_ALT_GR

	ex af,af'
	push af
	exx
	push bc
	push de
	push hl

 .ENDIF

;#if proc64180
 .if 1 ; Nick
	in0	e,(CBR)
	in0	d,(BBR)
	ld	a,(_os_bank)
	out0	(CBR),a
 .else
	in0	a,(BBR)	; get actual page number
	push	af ; save it
 .endif
;#else
;	IN	A,(?BANK_SWITCH_PORT_L08)
;	push	af ; save it
;#endif

sched_int:
; ld a,'\\'
; call abyte

	ld ix,(_activetcb)	;get current running task pointer
 .if 1 ; Nick
	ld	(ix+cbr_save),e
	ld	(ix+bbr_save),d
 .endif

 	ld a,(ix+tcbstate+1)	;get task states
 	and RUNNING		;see if running
 	jr nz,set_resume
	ld a,0ffh
	ld (_active_priority),a	;load it
	jp LWRD sched_cont
set_resume:
 	ld a,RESUME		;yes, identify that task can resume
 	ld (ix+tcbstate+1),a		;save task new state, if need be
sched_cont:
 	ld hl,0
 	add hl,sp		;save the current stack pointer of task
 	ld (ix+stk_save+1),h
 	ld (ix+stk_save),l
_K_I_Scheduler:	
 	di			;disable interrupts
 	ld hl,(_interrupt_stack)	;load in interrupt stack (also 
				;scheduler's and timer task stack 
 	ld sp,hl
	ld a,1			;set proper lock out states
 	ld (_int_count),a	;locked out interrupt stack swapping
	ld (_locked_out),a	;so int_action and timer task routine 
				;forced back to scheduler
sched_again:
 	ei			;enable interrupts
rescan1:
	ld hl,_cmx_flag1		;now test the scheduler flags
	bit bit_do_timer_tsk,(hl) ;should we do timer task
	jr z,rescan2		;no
	res bit_do_timer_tsk,(hl) ;yes, go do it
	push ix			;save ix reg
	LD A,BYTE3 _K_I_Timer_Task
	ld hl,LWRD _K_I_Timer_Task	;load hl with timer task address
	CALL	LWRD ?BANK_CALL_DIRECT_L08
	ld hl,_cmx_flag1		;restore hl to point to _cmx_flag1
	pop ix			;restore __activetcb
rescan2:
	bit bit_do_int_pipe,(hl) ;should we do interrupt pipe
	jr z,rescan4		;no
	push ix
	LD A,BYTE3 _K_I_Intrp_Pipe_Out
	ld hl,LWRD _K_I_Intrp_Pipe_Out	;go do pipe requests
	CALL	LWRD ?BANK_CALL_DIRECT_L08
	ld hl,_cmx_flag1
	pop ix			;restore __activetcb
rescan4:
	ld a,(hl)	;really _cmx_flag1
	bit bit_preempted,a	;is preempted flag set
	jr z,rescan5		;no, continue
	and .BINNOT.(preempted.BINOR.do_coop_sched.BINOR.do_time_slice.BINOR.slice_enable)
	or idle_flag		;set flag that we now test for idle condition
	ld (hl),a		;new _cmx_flag1 contents
 	ld ix,_cmx_tcb		;load the highest priority task 
				;(CMX timer task)
 	ld b,(ix+nxttcb+1)	;get next task in line, according to priority
 	ld c,(ix+nxttcb)
 	push bc
 	pop ix			;now pointing at highest puser priority task
	jp LWRD midpaus2		;now jump and test highest task
rescan5:
	ld a,(hl)		;get cmx_flags
	bit bit_do_coop_sched,a	;is cooperative scheduling flag set
	jr z,rescan6		;no, continue
	and .BINNOT.(do_coop_sched.BINOR.do_time_slice.BINOR.slice_enable)
	ld (hl),a		;load new _cmx_flag1 byte
	jp LWRD findready		;get next task able to run
rescan6:
 .IF SLICE_ENABLE > 0			
	bit bit_do_time_slice,(hl)	;see if time slice time up
	jr nz,findready		;yes, go see if another task with
				;same priority capable of running
 .ENDIF
midpaus2:
 	ld a,(ix+tcbstate+1)		;get task states
 	and RESUME.BINOR.READY	;see if task able to run or resume
 	jr nz,task_resume	;task is allowed to become the running task
				;task not able to run
findready:
 	ld b,(ix+nxttcb+1)	;get next task in line, according to priority
 	ld c,(ix+nxttcb)
 	push bc
 	pop ix
	ld a,.high._cmx_tcb	;test to see if we have reached end of TCB
	cp b
	jr nz,midpaus2		;no, we haven't
	ld a,.low._cmx_tcb
	cp c
	jr nz,midpaus2		;no, we haven't
; here means that we have gone to end of list
 .IF LOW_POWER_ACTIVE

; if idle flag set and list_complete flag set go to idle state
	bit bit_idle_flag,(hl)	;test to see if we have looped thru all tasks
	jr z,no_power_down	;no, test for more ready tasks
power_down:
	push ix		;yes, save ix reg
	LD A,BYTE3 _K_OS_Low_Power_Func
	ld hl,LWRD _K_OS_Low_Power_Func	;call reduced power function, by user
	CALL	LWRD ?BANK_CALL_DIRECT_L08
	pop ix		;restore ix

 .ENDIF

no_power_down:
 	ld ix,_cmx_tcb		;load the highest priority task 
				;(CMX timer task)
 	ld b,(ix+nxttcb+1)	;get next task in line, according to priority
 	ld c,(ix+nxttcb)
 	push bc
 	pop ix			;now pointing at highest puser priority task
 	jp LWRD rescan1		;go test next task
task_resume:	
; The following may look a little out of place, but the 
; reason behind all this is so interrupts will be disable 
; for the least amount of time possible
	ld c,a			;save a into c, (task state)
	ld a,(ix+priority)	;get priority of task
	ld (_active_priority),a	;load it
	ld (_activetcb),ix	;save current active task
	ld a,(hl)		;_cmx_flag1
	and .BINNOT.(idle_flag.BINOR.do_time_slice)	;mask out proper bits
	ld (hl),a		;store it
 	ld d,(ix+stk_start+1)	;get the beginning address of task's stack
 	ld e,(ix+stk_start)
 	bit 1,c		;READY, test to see if task just starting it's code
 	jr nz,task_res_cont	;yes, jump
 	ld d,(ix+stk_save+1)	;reload the stack pointer of task's save stack
	ld e,(ix+stk_save)
task_res_cont:			;NOTE: number of states in parenthesis
				; for 64180 processor

 	di			;disable interrupts 
	ld a,(hl)		;get the _cmx_flag1 byte
	and do_int_pipe.BINOR.do_timer_tsk	;(6)see if interrupt set flags
	jr z,res_cont1			;see if either flag became set
	jp LWRD sched_again		;(6 if no branch or 8) 
res_cont1:
	ld a,0			;(6) if no branch, then set interrupt count
	ld (_int_count),a	;(13) to zero
	ld (_locked_out),a	;(13) to zero
 .IF SLICE_ENABLE > 0			
	ld a,(_SLICE_ON)		;see if time slicing enabled
	or a			;set condition flag
	jr z,no_slice		;if 0, not set, so jump
	bit bit_slice_enable,(hl)	;see if slice enable set
	jr nz,no_slice		;yes, jump
	set bit_slice_enable,(hl)	;set flag
	ld a,(_TSLICE_SCALE)	;put slice count into counter
	ld (_tslice_count),a
no_slice:
 .ENDIF
 .IF CMXTRACKER_ENABLE > 0			
 	push ix
 	pop bc
	ld a,(_previoustcb+1)	;test to see if we have reached end of TCB
	cp b
	jr nz,not_same
	ld a,(_previoustcb)	;test to see if we have reached end of TCB
	cp c
	jr z,same
not_same:
	ld (_previoustcb),ix	;save current active task
	push ix		;yes, save ix reg
	LD A,BYTE3 _cmxtracker_in_task
	ld hl,LWRD _cmxtracker_in_task	;go log new task executing/resuming
	CALL	LWRD ?BANK_CALL_DIRECT_L08
	pop ix		;restore ix
same:
 	ld a,(ix+tcbstate+1)		;get task states
	ld c,a			;save a into c, (task state)
 .ENDIF
	ld (ix+tcbstate+1),RUNNING	;(14) load in RUNNING state into task's tcbstate
 	bit 1,c			;(6) see if task was READY, test to see  
				;if task just starting it's code
 	jr nz,task_ready	;(6 if no branch or 8) yes, jump
	ex de,hl		;(3) task is RESUMING
 	ld sp,hl		;(4) load stack pointer to proper address
 .if 1 ; Nick
	ld	e,(ix+cbr_save)
	ld	d,(ix+bbr_save)
 .endif

; ld a,'/'
; call abyte

done_scheduler:
; restore all register of either interrupt or task, then return

;#if proc64180
 .if 1 ; Nick
	out0	(CBR),e
	out0	(BBR),d
 .else
	POP	AF		;(9) pop page address
	OUT0	(BBR),A		;(13) set new page
 .endif
;#else
;	POP	AF		;(9) pop page address
;	OUT	(?BANK_SWITCH_PORT_L08),A
;#endif

;if gr' regs used
 .IF SAVE_ALT_GR

	pop hl			;(9)
	pop de			;(9)
	pop bc			;(9)
	exx			;(3)
	pop af			;(9)
	ex af,af'		;(4)

 .ENDIF

 	pop iy
 	pop hl
 	pop de
 	pop bc
 	pop af
 	pop ix
 	ei
 	ret
task_ready:
	ex de,hl		;(3) get task beginning address of stack
 	ld sp,hl		;(4) load it into stack pointer
	ld c,(ix+task_addr)	;(14) LOW BYTE of address
	ld b,(ix+task_addr+1)	;(14) MID BYTE of address
	ld a,(ix+task_addr+2)	;(14) HIGH BYTE of address
	push bc			;(11) the true address of code on stack

;#if proc64180
 .if 1 ; Nick
	; in this case, no need to mess with CBR, use the CMX data segment
 .endif
 .if 1 ; virtual memory
;	push	hl
	ld	h,80h
	ld	l,a			; hl -> 8000h + entry a value
	ld	a,(hl)			; translate bbr via virt memory table
;	pop	hl
;
;	or	a			; check for unused virt memory pages
;	ret	z			; just to be extra paranoid
 .endif
	OUT0	(BBR),A		;(13) set new page
;#else
;	OUT	(?BANK_SWITCH_PORT_L08),A
;#endif
; ld a,'|'
; call abyte

 	ei			;enable interrupts
 	ret			;will return to tasks beginning code address
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;	CAUTION to anyone writing interrupt service routines! ;;;;;;
;
;	The system tick timer interrupt MUST call these routines
;
;	Any interrupt that wants to use ANY of the CMX functions
;	MUST call these routines.
;
;	Here's an example of an interrupt service routine that uses
;	this method:
;
;	interrupt vector: interrupt vectors to here.
;		call	cxint_in
;		set interrupt masks if user wants
;		ei		;re-enable interrupts if user wants
;		...miscellaneous user ISR code, including...
;		call	cxint_ex	;replaces rti instruction
;
;	interrupt vector:
;		call 	cxint_in
;		set interrupt masks if user wants
;		ei	;re-enable interrupts is user wants
;		call 	C_WRITTEN_INTERRUPT_CODE
;		call	cxint_ex
;		
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; WARNING: for use with external chips that monitor
; the z80 (z180) RETI instruction
; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; the following is a problem, it does .BINNOT. distinquish
; between an internal interrupt or an external interrupt
; the internal interrupts should just use the RET instruction
; and the external interrupts the RETI instruction.
; Solution: change the code as follows in proper area
;no_stack_load:
;	inc a
;	ld (_int_count),a
;   ;;; ADD the following ;;;
;	ld bc,cxint_ex		
;	push bc
;   ;;;	back to regular instruction 
;	jp (ix)

;
; now interrupts will have to have the RET or RETI instruction at the
; end INSTEAD of the cxint_ex instruction.
;
; NOTE: also remove (comment out) the instructions "pop hl" and
; "call notify_int" which are no longer needed.
;
_K_OS_Intrp_Exit:
;	;CALL here instead of executing RETI at the end of an ISR.
;	;Only use this if cxint_in was called first.
;	;use a call cxint_ex instruction
 .if 0
 ex (sp),hl
 push af
 ld a,'j'
 call abyte
 call ahexw
 pop af
 ex (sp),hl
 .endif
 	di			;(3) disable interrupts
 	pop hl			;(9) add 2 to stack pointer because of call
 .if 0 ; Nick
	call LWRD notify_int		;(16) notify the hardware there were done with 
				;the interrupt (almost in reality)
 .endif
 .if 1 ; Nick
	pop	de
 .endif

	ld a,(_cmx_flag1)	;see if cmx has been entered
	bit bit_cmx_active,a
	jr z,done_scheduler	;no, exit out

 	ld hl,_int_count		;(9) load hl with interrupt count address
 	dec (hl)		;(10) decrement the interrupt count
 	jr nz,done_scheduler	;(6 or 8) if non zero (0), return to 
				;lower priority	interrupt
 	ld hl,(_stack_holder)	;(15) load in interrupted task stack 
 	ld sp,hl		;(4) restore stack pointer
	ld a,(_locked_out)	;(12) see if we are locked out
	or a			;(6) set or clear z flag
	jr nz,done_scheduler	;(6 or 8) if locked out, MUST finish 
				;what we were doing prior to interrupt
	ld a,(_cmx_flag1)	;(12) see if any scheduler flags set
	and do_int_pipe.BINOR.preempted.BINOR.do_timer_tsk.BINOR.do_time_slice.BINOR.do_coop_sched
	jr z,done_scheduler	;(6 or 8) resume task if no flags set
;goto_scheduler:
	ld hl,_locked_out	;(9) load hl with locked out address
	inc (hl)		;(10) increment locked out flag
	ei			;(3) re-enable interrupts
	jp LWRD sched_int		;(9) go save rest of interrupted task's state
 .if 0 ; Nick
notify_int:
 	reti
 .endif

_K_OS_Intrp_Entry:
 .if 0
 ex (sp),hl
 push af
 ld a,'i'
 call abyte
 call ahexw
 pop af
 ex (sp),hl
 .endif
	ex (sp),ix	;save ix on stack, get return address into ix
	push af	;save all registers
	push bc
	push de
	push hl
	push iy
;if gr' regs used
 .IF SAVE_ALT_GR

	ex af,af'
	push af
	exx
	push bc
	push de
	push hl

 .ENDIF

;#if proc64180
 .if 1 ; Nick
	in0	e,(CBR)
	in0	d,(BBR)
	ld	a,(_os_bank)
	out0	(CBR),a
 .else
	in0	a,(BBR)	; get actual page number
	push	af ; save it
 .endif
;#else
;	IN	A,(?BANK_SWITCH_PORT_L08)
;	push	af ; save it
;#endif

	ld a,(_cmx_flag1)	;see if cmx has been entered?
	bit bit_cmx_active,a
	jr z,cxint_in_exit	;no, get out

	ld a,(_int_count)	;see if interrupt nesting count 0
	or a
	jr nz,no_stack_load	;no, another interrupt was preempted (nesting) 
 	ld hl,0			;first interrupt
 	add hl,sp		;save the current stack pointer of task
 	ld (_stack_holder),hl
 	ld hl,(_interrupt_stack)	;load in interrupt stack (also 
				;scheduler's stack
 	ld sp,hl
no_stack_load:
	inc a			;increment interrupt nesting counter
	ld (_int_count),a
cxint_in_exit:
 .if 1 ; Nick
	push	de
 .endif
	jp (ix)			;go back to interrupt

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

_K_OS_Disable_Interrupts:
	di
	ret

_K_OS_Enable_Interrupts:
	ei
	ret

_K_OS_Save_Interrupts:
	ld a,i	;get iff2 flag, copy of iff1 
	jp pe,LWRD save_int1
	di
	ld a,0
	ld (_ie_copy),a	;save copy of interrupt enable flag
	ret
save_int1:
	di
	ld a,1
	ld (_ie_copy),a	;save copy of interrupt enable flag
	ret

_K_OS_Restore_Interrupts:
; restore interrupt enable flag
	ld a,(_ie_copy)		;see if interrupts were enable / disabled
	or a
	jr z,LWRD no1_ei_on
	ei
no1_ei_on:
	ret

; -----------------------------------------------------------------------------
; variables placed here by Nick, formerly defined via #ifdef CMX_INIT_MODULE
; these have to be in RCODE !! as we are now saving and restoring CBR in sched

;_locked_out:	defb	0   ; disable task switching
;_in_ctr:	defb	0   ; position within interrupt pipe to place bytes
;_out_ctr:	defb	0   ; position within interrupt pipe to remove bytes
;_cmx_flag1:	defb	0   ; contains flags, for CMX internal use
;_int_count:	defb	0   ; interrupt nested level count
;_pipe_slots_avail: defb 0
;_ie_copy:	defb	0

; -----------------------------------------------------------------------------

	END

