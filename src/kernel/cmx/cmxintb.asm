; cmxintb.asm
; Extremely basic interrupt system for Hytech CMX (banked)

$ io64180.inc

	extern	abyte
	extern	ahexw
	extern	acrlf
 	extern	_K_OS_Intrp_Entry
	extern	_K_OS_Intrp_Exit
	extern	_timer0_handler

	public	_timer0_setup
	public	_timer0_vector

	EXTERN	?BANK_CALL_DIRECT_L08
	EXTERN	?BANK_FAST_LEAVE_L08

; -----------------------------------------------------------------------------

;tmdr0l_r EQU 0ch
;tmdr0h_r EQU 0dh
;rldr0l_r EQU 0eh
;rldr0h_r EQU 0fh
;tcr_r EQU 10h

;	RSEG	RCODE
;
;	COMMON	INTVEC
;	org	$ + 4h
;
;VECT2:
;	DEFW	timer0_vect

; -----------------------------------------------------------------------------

	RSEG	CODE

_timer0_setup::
;	push	bc
;	push	de

;	ld	a,01
;	ld	i,a	;set i to 1, thus intenal interrupts start at 0x100

	ld	a,0bh
;	ld	a,20h
	out0	(TMDR0H),A
	out0	(RLDR0H),A
	ld	a,0b8h
	out0	(TMDR0L),a
	out0	(RLDR0L),a
	ld	a,11h
	out0	(TCR),a

;	pop	de
;	pop	bc
;	ret
	jp	LWRD ?BANK_FAST_LEAVE_L08

; -----------------------------------------------------------------------------

	RSEG	RCODE

_timer0_vector::
 .if 1
 push af
 in0 a,(TCR)
 in0 a,(TMDR0L)
 pop af
 ei
 ret
 .endif

 .if 0
 push af
 ld a,0
 inc a
 ld ($-2),a
 or a
 jr z,silly
 in0 a,(TCR)
 in0 a,(TMDR0L)
 pop af
 ei
 ret ;i
silly:
 pop af
 .endif
 .if 0
 push af
 ld a,'x'
 call abyte
 pop af
 .endif
	call	_K_OS_Intrp_Entry
 .if 0
 ld a,'y'
 call abyte
 .endif
	in0	a,(TCR)
	in0	a,(TMDR0L)
	ld	a,BYTE3 _timer0_handler
	ld	hl,LWRD _timer0_handler	;load hl with cmx_tic address
	call	LWRD ?BANK_CALL_DIRECT_L08
 .if 0
 ld a,'z'
 call abyte
 .endif
	call	_K_OS_Intrp_Exit

; -----------------------------------------------------------------------------

	END
