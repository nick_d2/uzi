/*********************************************************

Copyright (c) CMX Company. 1999. All rights reserved

*********************************************************/
/* version 5.30 */

#define CMXMODULE 1

#include <cxfuncs.h>	/* get cmx include header file */
#include <cxextern.h>	/* get cmx include header file */

#ifdef CMXTRACKER
#include <cmxtrack.h>	/* get cmx include header file */
#endif

void K_OS_Slice_On(void)
{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in1(ENABLE_SLICE_CALL);
			}
#endif
	SLICE_ON = 1;	/* enable time slicing */
	tslice_count = TSLICE_SCALE;	/* load the time slice counter,
			with the scale dictated by user in confiquration module. */
	SLICE_ENABLE;	/* inform CMX that time slicing is now active. */
}

void K_OS_Slice_Off(void)
{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in1(DISABLE_SLICE_CALL);
			}
#endif
	SLICE_ON = 0;	/* this will disable time slicing. */
}


/* K_I_Copy = dest,source,count */

void K_I_Copy(byte *dest,byte *src,byte count)
{
	/* this will move the source bytes to the destination */

	while (count--)	/* post decrement counter, and test */
		*dest++ = *src++;	/* increment the source and destination pointers */
}

/************************************************************
	task done routine, all task's that are finished their code,
	will MUST be returned to here. If a task is going to hit it's
	end brace, it MUST call the following function.
	
	This function will then decide if the task should be
	placed into the IDLE state or READY state because outstanding
	triggers	(caused by the K_Task_Start function) are present.
**************************************************************/
void K_Task_End(void)
{
	K_I_Disable_Sched();	/* set task block */
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in1(CXTEND_CALL);
			}
#endif
	/* now test to see if task has any outstanding start requests.
		If so, place task into READY state, if not IDLE state. */
	activetcb->tcbstate = (--activetcb->trig) ? READY : IDLE; 
	PREEMPTED;	/* set the preempted scheduling flag */
	K_I_Func_Return();	/* release task block */
}

/***************************************************************
	This function gets the address of a task's task control block.
	also tests to see if task was created and if so not removed.
	passes back requested task's address so the function that call
	this function will be able to work with the requested task's
	task control block.
*****************************************************************/

byte K_I_Get_Ptr(byte tskid,tcbpointer *tcbptr)
{
	if (tskid > MAX_TASKS || !tskid)	/* see if task exists */
		return(K_ERROR);	/* no, return error */
	*tcbptr = &cmx_tcb[tskid];	/* pass address of task TCB back */
	if (((tcbpointer)(*tcbptr))->nxttcb)	/* see if NOT NULL */
		return(K_OK);	/* return good status */
	else
		return(K_ERROR);	/* it was so return error */
}

/***********************************************************************
	This function will create a task. The task will be defined to the 
	task's control block array.
************************************************************************/

byte K_Task_Create(byte priority,byte *tskid,CMX_FP task_addr,word16 stack_size)
{
	tcbpointer tcbptr;
	byte i;

	/* abyte('A'); */

	K_I_Disable_Sched();	/* set task block */
	tcbptr = cmx_tcb;	/* start at beginning of linked list. */
	for (i = 0; ;i++)
		{
		/* abyte('B'); */

		if (tcbptr->nxttcb)	/* not NULL */
			{
			/* abyte('!'); */
			if ((++tcbptr) > &cmx_tcb[MAX_TASKS])	/* past MAXIMUM? */
				{
				/* abyte('@'); */
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in1(CXTCRE_K_ERROR);
			}
				/* abyte('#'); */
#endif
				K_I_Func_Return();	/* yes, return because no more room. */
				/* abyte('$'); */
				return(K_ERROR);	/* return ERROR. */
				}
			}
		else
			{
			/* abyte('%'); */
			break;	/* OK, exit out of for loop. */
			}
		}
	/* abyte('C'); */

	K_I_Priority_In(tcbptr,priority);	/* go load in task into priority chain */
	*tskid = i;	/* load in slot number */

	tcbptr->task_addr = task_addr;	/* load in task's CODE address */

	CXTCRE_SPECIFIC();
	/* abyte('D'); */

	tcbptr->tcbstate = IDLE;	/* put the task into the IDLE state */
	tcbptr->trig = 0; /* load the start counter with 0, no starts */
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in1(CXTCRE_K_OK);
			}
#endif
	/* abyte('E'); */

	K_I_Func_Return();	/* release task block */
	/* abyte('F'); */

	return(K_OK);	/* return good status */
}

/************************************************************
The following has been added, so user can pass the starting
address of a task, so as to allow multiple creations and
removing of tasks stacks, for when a task is removed after
using the 'normal' K_Task_Create function, the stack area is NOT
reclaimed.

This will allow stack area to be reclaimed if need be and if
user wants, also the ability to have different task use same
stack area, as long as they be CAREFUL to ensure that the tasks
sharing same stack do not run concurrently.
************************************************************/

byte K_Task_Create_Stack(byte priority,byte *tskid,CMX_FP task_addr,word16 *stack_start)
{
	tcbpointer tcbptr;
	byte i;

	K_I_Disable_Sched();	/* set task block */
	tcbptr = cmx_tcb;	/* start at beginning of linked list. */
	for (i = 0; ;i++)
		{
		if (tcbptr->nxttcb)	/* not NULL */
			{
			if ((++tcbptr) > &cmx_tcb[MAX_TASKS])	/* past MAXIMUM? */
				{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in1(CXTCRE_K_ERROR);
			}
#endif
				K_I_Func_Return();	/* yes, return because no more room. */
				return(K_ERROR);	/* return ERROR. */
				}
			}
		else
			{
			break;	/* OK, exit out of for loop. */
			}
		}
	K_I_Priority_In(tcbptr,priority);	/* go load in task into priority chain */
	*tskid = i;	/* load in slot number */

	tcbptr->task_addr = task_addr;	/* load in task's CODE address */
	tcbptr->stk_start = stack_start;
	tcbptr->tcbstate = IDLE;	/* put the task into the IDLE state */
	tcbptr->trig = 0; /* load the start counter with 0, no starts */
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in1(CXTCRE_K_OK);
			}
#endif
	K_I_Func_Return();	/* release task block */
	return(K_OK);	/* return good status */
}

/******************************************************
 This function will change the priority of a task, will possibly
 re-adjust the linked TCB chain.
******************************************************/
byte K_Task_Priority(byte tskid,byte new_priority)
{
	tcbpointer tcbptr;

	if(K_I_Get_Ptr(tskid,&tcbptr))	/* send address of pointer */
		{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXTPRI_K_ERROR,tskid);
			}
#endif
		return(K_ERROR);
		}
	K_I_Disable_Sched();	/* set task block */
	K_I_Unlink(tcbptr);	/* take task out of priority chain */
	K_I_Priority_In(tcbptr,new_priority);	/* now insert the task back into the
													priority chain link list */
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in3(CXTPRI_K_OK,tskid,new_priority);
			}
#endif
	K_I_Func_Return();	/* release task block */
	return(K_OK);	/* return good status */
}

/******************************************************
 This function will remove a task from the linked list of TCB's.
******************************************************/
void K_I_Unlink(tcbpointer tcbptr)
{
	tcbpointer prevtcb;

	/* remove the task from linked list */
	prevtcb = cmx_tcb;

	/* scan TCB's looking for TCB that points to this TCB */
	while (prevtcb->nxttcb != tcbptr)
		{
		prevtcb = prevtcb->nxttcb;
		}
	prevtcb->nxttcb = tcbptr->nxttcb;
	/* the previous tcb nxttcb points to the delink tcb nxttcb */
	tcbptr->nxttcb = 0;
	/* identify that this task no longers exist */
}

/*******************************************************
 This function will place the task into the proper linked list
 slot, according to priority.	Remember the lower the priority
 number, the higher the priority is for this task in relationship
 to others. 
*******************************************************/
void K_I_Priority_In(tcbpointer tcbptr,byte priority)
{
	tcbpointer prevtcb;
	tcbpointer tp;

	tp = cmx_tcb;	/* address of TCB link list. */
	do /* insert new tcb at appropriate priority slot in chain...*/
		{
		prevtcb = tp;
		tp = tp->nxttcb;
		} while ((tp != cmx_tcb) && (tp->priority <= priority));
	/* We should insert it just after prevtcb, just before tp. */

	tcbptr->nxttcb = tp;			/* adjust priority slot chain */
	prevtcb->nxttcb = tcbptr;
	tcbptr->priority = priority;	/* load in task's priority */
}
/****************************************************************
 This function will place a task into the READY state, if in the
 IDLE state. If the task is already triggered, then this will
 be queued up. The maximum number of starts for a task, can be
 255.
*****************************************************************/
byte K_Task_Start(byte tskid)
{
	tcbpointer tcbptr;

	if(K_I_Get_Ptr(tskid,&tcbptr))	/* send address of pointer */
		{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXTTRIG_K_ERROR,tskid);
			}
#endif
		return(K_ERROR);
		}
	K_I_Disable_Sched();	/* set task block */
	if (tcbptr->trig != 0xff) /* see if 0xFF, bypass if so */
		{
		if (++tcbptr->trig == 1)	/* increment the task start count byte */
			{
				/* if count = 1, then put task into READY state */
				tcbptr->tcbstate = READY;

			/* see if this task has a higher priority then current RUNNING task */
				if (tcbptr->priority < active_priority) 
					PREEMPTED;	/* yes, set preempted scheduling flag */
			}
		}
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXTTRIG_K_OK,tskid);
			}
#endif
	K_I_Func_Return();	/* release task block */
	return(K_OK);	/* return good status */
}

/**************************************************************
 The following will place a task into the doubly linked list of
 tasks that are waiting on time, if the CMX function was called
 specifying a time wait period of non 0 (zero). Also the task
 will be put to sleep, for the entity that the task was looking
 for, was not present at the time of the function call.
**************************************************************/
byte K_I_Time_Common(word16 timecnt,byte STATE)
{
	activetcb->tcbtimer = timecnt;	/* load task time counter with proper time */
	activetcb->tcbstate = STATE;	 /* put task to sleep, idicating why */
	if (timecnt)	/* put into timer chain ?? */
		{
		/* let task sleep, indicating why and also waiting on time */
		activetcb->tcbstate |= TIME;
		/* place into the doubly linked list. */
		if (tsk_timer_lnk->ftlink != (tcbpointer)tsk_timer_lnk)	
			{
			activetcb->ftlink = tsk_timer_lnk->btlink->ftlink;
			tsk_timer_lnk->btlink->ftlink = activetcb;
			activetcb->btlink = tsk_timer_lnk->btlink;
			}
		else
			{
			activetcb->ftlink = activetcb->btlink = (tcbpointer)tsk_timer_lnk;
			tsk_timer_lnk->ftlink = activetcb;
			}
		tsk_timer_lnk->btlink = activetcb;
		}
	PREEMPTED;	/* set the preempted scheduling flag */
	K_I_Func_Return();	/* release task block */

	/* the task WILL return to here, when it resumes execution. */
	K_I_Disable_Sched();	/* set lock out count. */
	if (activetcb->tcbtimer)	/* see if timer non 0. */
		{
		/* if so, remove from link list. */
		activetcb->ftlink->btlink = activetcb->btlink;
		activetcb->btlink->ftlink = activetcb->ftlink;
		}
	/* see if task was woken, because the thing it was waiting for happened,
		or that the time period specified has elapsed */
	if (activetcb->tcbstate & TIME_EXPIRED)
		{
		K_I_Func_Return();	/* release task block */
		return(K_TIMEOUT);	/* return the warning: that the time period expired */
		}
	else
		{
		K_I_Func_Return();	/* release task block */
		return(K_OK);	/* return good status */
		}
}
/************************************************************
 This function allows a task to wait for the desired time
 period to expire or indefinitely. The K_Task_Wake and K_Task_Wake_Force
 function may be used to wake this task, prior to the time
 period expiring.
************************************************************/
byte K_Task_Wait(word16 timecnt)
{
	K_I_Disable_Sched();	/* set task block */
	
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in4(CXTWATM_CALL,0,timecnt);
			}
#endif
	/* call the function that will suspend this task, indicate the
		reason to suspend this task */
	if (K_I_Time_Common(timecnt,WAIT))
		{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in1(CXTWATM_DELAY_K_ERROR);
			}
#endif
		return(K_TIMEOUT);
		}
	else
		{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in1(CXTWATM_DELAY_K_OK);
			}
#endif
		return(K_OK);
		}
}

/************************************************************
 The following function will be called by both the K_Task_Wake and
 K_Task_Wake_Force functions. This will determine whether a task will
 be forced to wake up, or to normally be woken up.

 if force = 0, then wake task only if task had called K_Task_Wait

 if force = 1, then wake task regardless of what function
               was called, that suspended task

************************************************************/
byte K_I_Wake_Common(byte tskid,byte force)
{
	tcbpointer tcbptr;

	if(K_I_Get_Ptr(tskid,&tcbptr))	/* send address of pointer */
		{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXTWAK_K_ERROR,tskid);
			}
#endif
		return(K_ERROR);
		}
	K_I_Disable_Sched();	/* set task block */

	/* if force non 0, then wake task, regardless of what it
		was waiting for. If force = 0, then only wake task
		if it was waiting on time or indefinitely. */
	if (tcbptr->tcbstate & (force ? ANY_WAIT : WAIT))
		{
		/* see if task was waiting for a resource */
		if (tcbptr->tcbstate & (RESOURCE | SEMAPHORE))
			{
			/* yes, remove task from resource wait linked list. */
			tcbptr->fwlink->bwlink = tcbptr->bwlink;
			tcbptr->bwlink->fwlink = tcbptr->fwlink;
			}
		/* put task into the RESUME state, also possibly indicate time
			expired, though it may not in the true sense */
		if (force)
			tcbptr->tcbstate = RESUME | TIME_EXPIRED;
		else
			tcbptr->tcbstate = RESUME;

		/* see if this task has a higher priority then the current RUNNING task */
		if (tcbptr->priority < active_priority)
			{
			PREEMPTED;	/* yes, set the preempted scheduling flag */
			}
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXTWAK_K_OK,tskid);
			}
#endif
		K_I_Func_Return();	/* release task block */
		return(K_OK);	/* return good status */
		}
	else
		{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in1(CXTWAK_K_NOT_WAITING);
			}
#endif
		K_I_Func_Return();	/* release task block */
		return(K_NOT_WAITING);	/* return error that task was not waiting */
		}
}

/*****************************************************************
		!!! WARNING: the CMX timer task will not run !!!!
 The CMX timer task will not be executed, nor the interrupt pipe
 if this function is called. The task that called this function should
 release it, by using the K_Task_Unlock function as soon as possible.
 In most cases this function should NOT be used. Improper use is
 abuse. Also the task that calls this function, MUST NOT call any
 function that may suspend this task
*****************************************************************/
void K_Task_Lock(void)
{
	/* set the task lock flag. This will stop the K_I_Scheduler
		from re-scheduling, until the K_Task_Unlock function is called */

	K_I_Disable_Sched();
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in1(CXPRVR_CALL);
			}
#endif
}
/****************************************************************
 Should be used ONLY if the task used the K_Task_Lock function.
*****************************************************************/
void K_Task_Unlock(void)
{
	/* lower the task privilege flag. See if the K_I_Scheduler
		should be invoked */

#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in1(CXPRVL_CALL);
			}
#endif
	K_I_Func_Return();
}
/***************************************************************
 The following function performs a cooperative scheduling.
***************************************************************/
void K_Task_Coop_Sched(void)
{
	K_I_Disable_Sched();	/* set task block */
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in1(CXSCHED_CALL);
			}
#endif
	DO_COOP_SCHED;	/* set the cooperative scheduling flag */
	K_I_Func_Return();	/* release task block */
}

/***********************************************************
 Remove a task. Once removed this task can not be used in anyway
 Also the task MUST NOT be waiting on anything, foe it will not
 be removed if so.
***********************************************************/
byte K_Task_Delete(byte tskid)
{
	tcbpointer tcbptr;

	if(K_I_Get_Ptr(tskid,&tcbptr))	/* send address of pointer */
		{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXTRMV_K_ERROR,tskid);
			}
#endif
		return(K_ERROR);
		}
	K_I_Disable_Sched();	/* set task block */
	if (tcbptr->tcbstate & ANY_WAIT)	/* see if task waiting */
		{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXTRMV_WAIT_K_ERROR,tskid);
			}
#endif
		K_I_Func_Return();	/* task was waiting, cannot terminate this task */
		return(K_ERROR);	/* return error status, task was not terminated */
		}
	else
		{
		K_I_Unlink(tcbptr);	/* no, task not waiting. Remove task from TCB */
		if (tcbptr == activetcb)	/* is task trying to commit suicide */
			{
			activetcb->tcbstate = 0;	/* load in highest priority task */
			PREEMPTED;	/* set the preempted scheduling flag */
			}
		}
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXTRMV_K_OK,tskid);
			}
#endif
	K_I_Func_Return();	/* release task block */
	return(K_OK);	/* return good status, if task not committing suicide */
}

/***********************************************************************

		TASK GENERAL PURPOSE FLAGS SYNCHRONIZATION

************************************************************************/
/***************************************************************
 This allows a task to reset a task's event flags that they
 would like to be cleared.
***************************************************************/
byte K_Event_Reset(byte tskid,word16 event)
{
	tcbpointer tcbptr;

	if(K_I_Get_Ptr(tskid,&tcbptr))	/* send address of pointer */
		{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXERST_K_ERROR,tskid);
			}
#endif
		return(K_ERROR);
		}
	K_I_Disable_Sched();	/* set task block */
	tcbptr->e_flags &= (~event);		/* place the event bit(s) into clear state, indicated by mask. */
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in4(CXERST_K_OK,tskid,event);
			}
#endif
	K_I_Func_Return();	/* release task block */
	return(K_OK);	/* return good status, if task not committing suicide */
}
/***************************************************************
 The following allows a task to wait for 1 or more events to be
 set, if they are not. Also a time period may be specified on how
 long the task will wait for at least 1 event to be set, in which
 the task wants.
MODE:
= 0 nothing
= 1 clear at beginning
= 2 auto clear on match
= 3 both 1 and 2
***************************************************************/
word16 K_Event_Wait(word16 match,word16 timecnt,byte mode)
{
	word16 good_bits;

	K_I_Disable_Sched();	/* set task block */
	if (mode & 0x01) 	/* see if we should clear specified events at beginning. */
		{
		activetcb->e_flags &= (~match);	/* yes, clear them. */
		}
		/* see if ANY task flags match */
	if (!(activetcb->e_flags & (activetcb->e_match = match)))
		{
		/* NO, so call CMX function that will suspend task. */
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in4(CXEWATM_CALL,0,timecnt);
			}
#endif
		if (K_I_Time_Common(timecnt,FLAGS))	
			{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in1(CXEWATM_DELAY_K_TIMEOUT);
			}
#endif
			return(0);	/* return the warning: that the time period expired */
			}
		K_I_Disable_Sched();	/* set task block. */
		}
	good_bits = activetcb->e_flags & match;	/* what event bits matched. */
	if (mode & 0x02)	/* should we clear the events now? */
		{
		activetcb->e_flags &= ~match;	/* yes, clear them. */
		}
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in4(CXEWATM_K_OK,0,good_bits);
			}
#endif
	K_I_Func_Return();	/* release task lock. */
	return(good_bits);	/* return events that were set according to mask. */
}

/**********************************************************************
 The following function will set possibly set an event.

byte mode: 	0 = specific task
				1 = highest priority (single task even if more then one of same)
				2 = highest priority waiting task on THIS EVENT
				3 = all tasks
				4 = all waiting tasks THIS EVENT
				5 = all with same priority (specify priority)
				6 = all waiting with same priority (specify priority) THIS EVENT
************************************************************************/
byte K_Event_Signal(byte mode, byte tskid_pri,word16 flag)
{
	tcbpointer tcbptr;
#ifdef CMXTRACKER
	byte cmxtracker_sent;
#endif

	if (mode > 0x06)	/* see if mode greater then maximum allowed. */
		{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXESIG_MODE,mode);
			}
#endif
		return(K_ERROR);
		}
	K_I_Disable_Sched();	/* set task block */
#ifdef CMXTRACKER
	cmxtracker_sent = 0;
#endif
	if (!mode)	/* is mode 0. */
		{
		if(K_I_Get_Ptr(tskid_pri,&tcbptr))	/* send address of pointer */
			{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXESIG_K_ERROR,tskid_pri);
			}
#endif
			K_I_Func_Return();
			return(K_ERROR);
			}
		goto test_event;
		}
	else
		{
		tcbptr = timertask->nxttcb;	/* start at head of linked list. */
		}
	for (; tcbptr != cmx_tcb; tcbptr = tcbptr->nxttcb) /* see if we tested all. */
		{
		if (mode > 0x04)	/* is mode greater then 4? */
			{
			if (tcbptr->priority != tskid_pri)	/* see if priority match. */
				continue;
			}
		if (!(mode & 0x01))	/* see if task must be waiting on this EVENT */
			{
			if (!((tcbptr->e_match & flag) &&
				(tcbptr->tcbstate & FLAGS)))
				{
				continue;	/* continue if task was not. */
				}
			}
test_event:
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			if (!cmxtracker_sent)
				{
				cmxtracker_sent = tcbptr - cmx_tcb;
				}
			}
#endif
		/* see if task event(s) match. */
		if (tcbptr->e_match & (tcbptr->e_flags |= flag))
			{
			if (tcbptr->tcbstate & FLAGS)	/* see if task was waiting on EVENT. */
				{
				tcbptr->tcbstate = RESUME; /* yes, the task may now resume. */
				
				if (TEST_NOT_PREEMPTED)	/* see if preempted flag not set yet. */
					{
					if (tcbptr->priority < active_priority)
						PREEMPTED;	/* yes, set the preempted scheduling flag */
					}
				if (mode < 0x03)	/* see if we should exit function. */
					break;
				}
			}
		if (mode < 0x02)	/* see if we should exit function. */
			break;
		}
#ifdef CMXTRACKER
 	if (CMXTRACKER_ON)
		{
		cmxtracker_in3(CXESIG_K_OK,mode,cmxtracker_sent);
		}
#endif
	K_I_Func_Return();	/* call CMX function, that will release task block */	
	return(K_OK);	/* return good status */
}

/*******************************************************************
		RESOURCE MANAGER
********************************************************************/

/*******************************************************************
 This function allows a task to reserve or get this resource.

 if func_mode = 1, then means get resource if free, otherwise return

 if func_mode = 0, then if the resource is "owned" by another task,
 then this task will be suspended until this resource is free or time
 period expires.
*******************************************************************/
byte K_I_Resource_Common(byte res_grp,word16 timecnt,byte func_mode)
{
	RESHDR *res_ptr;
	tcbpointer tp;

	if (res_grp >= MAX_RESOURCES)	/* see if resource number allowed */
		{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in3(CXRSRSV_K_ERROR,res_grp,func_mode);
			}
#endif
		return(K_ERROR);	/* no, return error status */
		}

	K_I_Disable_Sched();	/* set task block */

	res_ptr = &res_que[res_grp];	/* get address of resource */

	if (res_ptr->owner)	/* does a task own this resource? */
		{
		/* yes, what function was used. */
		if (func_mode)		/* did the K_Resource_Get function call this function */
			{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXRSRSV_K_RESOURCE_OWNED,res_grp);
			}
#endif
			 K_I_Func_Return();	/* release task block */
			 return(K_RESOURCE_OWNED);	/* return error that resource is already owned */
			}
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in4(CXRSRSV_CALL,res_grp,timecnt);
			}
#endif
		/* place the task into the resource doubly linked wait list. */

	tp = (tcbpointer)res_ptr;	/* address of resource link list. */
	do /* insert tcb at appropriate wait slot, based on priority. */
		{
#if 0
#if 1
		_asm
			nop
			nop
			nop
			nop
			nop
			nop
			nop
			nop
			nop
			nop
			nop
			nop
		_endasm;
#else
		abyte('.');
#endif
#endif

		tp = tp->fwlink;
		} while ((tp != (tcbpointer)res_ptr) && (tp->priority <= activetcb->priority));
	/* We should insert it just after prevtcb, just before tp. */

		activetcb->fwlink = tp->bwlink->fwlink;
		tp->bwlink->fwlink = activetcb;
		activetcb->bwlink = tp->bwlink;
		tp->bwlink = activetcb;

		if (res_ptr->fwlink->priority < res_ptr->owner->priority)
			{
			res_ptr->owner->priority = res_ptr->fwlink->priority;
			/* update linked list to put owner in correct spot */
			K_I_Unlink(res_ptr->owner);
			K_I_Priority_In(res_ptr->owner,res_ptr->owner->priority);
			}
		/*
			res_ptr->fwlink->priority is highest priority !!!
			which we will then raise owner to highest !!!
		*/
		/* go place task into suspended state. */
		if (K_I_Time_Common(timecnt,RESOURCE))
			{
#ifdef CMXTRACKER
			if (CMXTRACKER_ON)
				{
				cmxtracker_in1(CXRSRSV_DELAY_K_TIMEOUT);
				}
#endif
			return(K_TIMEOUT);	/* return the warning: that the time period expired */
			}
			else
			{
#ifdef CMXTRACKER
			if (CMXTRACKER_ON)
				{
				cmxtracker_in2(CXRSRSV_K_OK,res_grp);
				}
#endif
			return(K_OK);	
			}
		}
	/* NO, no task owned this resource. This task is now the owner of
		this resource */
	res_ptr->fwlink = res_ptr->bwlink = (tcbpointer)(res_ptr);
	res_ptr->owner = activetcb;
	res_ptr->owner_priority = activetcb->priority;
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in3(CXRSRSV_K_OK,res_grp,func_mode);
			}
#endif
	K_I_Func_Return();	/* release task block. */
	return(K_OK);	/* return good status. */
}

/*****************************************************************
 This lets a task that owns a resource, to release it. Once the resource
 is released, if another task is waiting for it, then that task will
 automatically become the owner.
******************************************************************/
byte K_Resource_Release(byte res_grp)
{
	RESHDR *res_ptr;

	if (res_grp >= MAX_RESOURCES)	/* see if resource group OK */
		{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXRSREL_K_ERROR,res_grp);
			}
#endif
		return(K_ERROR);	/* no, return error status */
		}

	res_ptr = &res_que[res_grp];	/* get address of resource */
	if (res_ptr->owner != activetcb)	/* see if task really "owns" this. */
		{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXRSREL_K_RESOURCE_NOT_OWNED,res_grp);
			}
#endif
		return(K_RESOURCE_NOT_OWNED);	/* resource not owned by this task */
		}
	K_I_Disable_Sched();	/* set task block */
	/* restore original task's priority */
	active_priority = res_ptr->owner_priority;
	if (res_ptr->owner->priority != active_priority)
		{
		/* update linked list to put owner in correct spot */
		K_I_Unlink(res_ptr->owner);
		K_I_Priority_In(res_ptr->owner,active_priority);
		}
	/* see if another task waiting for this resource. */
	if ((res_ptr->owner = res_ptr->fwlink) != (tcbpointer)res_ptr)
		{
		/* properly adjust the linked list. */
		res_ptr->fwlink = res_ptr->fwlink->fwlink;
		res_ptr->fwlink->bwlink = res_ptr->owner->bwlink;
		res_ptr->owner->tcbstate = RESUME;	/* wake task that was sleeping. */
		if (res_ptr->owner->priority < active_priority) 
			PREEMPTED;	/* yes, set the preempted scheduling flag */
		res_ptr->owner_priority = res_ptr->owner->priority;
		}
	else
		{
		res_ptr->owner = NULL;
		}
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXRSREL_K_OK,res_grp);
			}
#endif
	K_I_Func_Return();	/* release task block */
	return(K_OK);	/* return good status to caller */
}

/*****************************************************************

		QUEUE MANAGER

******************************************************************/
byte K_Que_Create(sign_word16 num_slots,byte slot_size,byte *queuearray,byte queuenum)
{
	QUEHDR *queue_ptr;

	if (queuenum >= MAX_QUEUES)	/* see if queue number OK. */
		{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXQCRE_K_ERROR,queuenum);
			}
#endif
		return(K_ERROR);
		}
	K_I_Disable_Sched();	/* set task block. */
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXQCRE_K_OK,queuenum);
			}
#endif
	queue_ptr = &queue[queuenum];	/* get address of proper queue handler. */
	queue_ptr->base_ptr = queuearray;	/* tell CMX where queue begins. */
	queue_ptr->num_slots = num_slots;	/* load number of slots */
	queue_ptr->size_slot = slot_size;	/* load in the size of slots */
	K_Que_Reset(queuenum);	/* reset the queue, all slots empty. */
	K_I_Func_Return();	/* release task block */
	return(K_OK);
}
byte K_Que_Reset(byte queuenum)
{
	QUEHDR *queue_ptr;

	if (queuenum >= MAX_QUEUES)	/* see if queue number within range. */
		{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXQRST_K_ERROR,queuenum);
			}
#endif
		return(K_ERROR);
		}
	K_I_Disable_Sched();	/* set task block. */
	queue_ptr = &queue[queuenum];	/* get address of proper queue handler. */
	/* reset the queue_ptr's variables, that maintain the queue_ptr */
	queue_ptr->queue_cnt = queue_ptr->head = queue_ptr->tail = 0;	
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXQRST_K_OK,queuenum);
			}
#endif
	K_I_Func_Return();	/* release task block */
	return(K_OK);	/* return good status. */
}


/************************************************************
	CMX general add to queue function.
	called by K_Que_Add_Top and K_Que_Add_Bottom functions.

	test: if testing for less then 0, then pointer = num_slots - 1
			if testing for equal to num_slots then pointer = 0

	when adding to top 
	add to where head points
	increment head and test.

	when adding to bottom.
	decrement tail and test
	add to where tail points

	when removing from top
	decrement head and test
	remove from where head points

	when removing from bottom
	remove from where tail points
	increment tail and test

 top = 0: add to bottom
 top = 1: add to top

*************************************************************/

byte K_I_Que_Add_Common(byte queuenum,void *byteptr,sign_word16 top)
{
	QUEHDR *queue_ptr;

	queue_ptr = &queue[queuenum];	/* get address of proper queue handler. */
	if (queuenum >= MAX_QUEUES || (!queue_ptr->size_slot))
		{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXQADD_K_ERROR,queuenum);
			}
#endif
		return(K_ERROR);
		}

	K_I_Disable_Sched();	/* set task block */

	/* see if queue is full already */
	if (queue_ptr->queue_cnt == queue_ptr->num_slots)
		{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXQADD_FULL,queuenum);
			}
#endif
		K_I_Func_Return();	/* release task block. */
		return(K_ERROR);	/* return proper status. */
		}
	if (top)	/* see if adding to top of queue */
		{
		top = queue_ptr->head;	/* top will now be our index into array. */

		/* see if head equals the max number slots, and
			if so, set pointer to bottom of queue */
		if (++queue_ptr->head == queue_ptr->num_slots)
			queue_ptr->head = 0;
		}
	else
		{
		/* were adding to bottom of queue, use tail pointer */
		if (--queue_ptr->tail < 0)	/* decrement tail pointer and test */
			queue_ptr->tail = (queue_ptr->num_slots - 1);

		top = queue_ptr->tail;	/* top will now be our index into array. */
		}
		/* K_I_Copy = dest,source,count */
		K_I_Copy((&(queue_ptr->base_ptr[top *
			queue_ptr->size_slot])),byteptr,queue_ptr->size_slot); 

#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXQADD_K_OK,queuenum);
			}
#endif
	/* see if queue now full */
	if (++queue_ptr->queue_cnt == queue_ptr->num_slots)
		{
		K_I_Func_Return();
		return(K_QUE_FULL);	/* return status indicating queue now full. */
		}
	K_I_Func_Return();	/* release task block */
	return(K_OK);	/* return good status */
}

/********************************************************
	test: if testing for less then 0 then pointer = num_slots - 1
			if testing for equal to num_slots then pointer = 0

	when removing from top
	decrement head and test
	remove from where head points

	when removing from bottom
	remove from where tail points
	increment tail and test

 top = 0: remove from bottom
 top = 1: remove from top

*********************************************************/

byte K_I_Que_Get_Common(byte queuenum,void *byteptr,sign_word16 top)
{
	QUEHDR *queue_ptr;

	queue_ptr = &queue[queuenum];	/* get address of proper queue handler. */
	if (queuenum >= MAX_QUEUES || (!queue_ptr->size_slot))
		{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXQRMV_K_ERROR,queuenum);
			}
#endif
		return(K_ERROR);
		}
	K_I_Disable_Sched();	/* set task block */
	/* see if queue_ptr has any slots filled */
	if (!(queue_ptr->queue_cnt))
		{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXQRMV_EMPTY,queuenum);
			}
#endif
		K_I_Func_Return();
		return(K_ERROR);	/* no, return error */
		}
	if (top)	/* see if request was to remove top slot contents */
		{
		/* decrement head and test to see if head less then 0 */
		if (--queue_ptr->head < 0)
			queue_ptr->head = (queue_ptr->num_slots - 1);

		top = queue_ptr->head;	/* top will now be our index into array. */
		}
	else
		{
		/* request was to remove from bottom slot */

		top = queue_ptr->tail;	/* top will now be our index into array. */

		/* see if tail pointer has reached it's maximum count and set
			to 0, if so */
		if (++queue_ptr->tail == queue_ptr->num_slots)
			queue_ptr->tail = 0;
		}
		/* K_I_Copy = dest,source,count */
		K_I_Copy(byteptr,(&(queue_ptr->base_ptr[top *
			queue_ptr->size_slot])),queue_ptr->size_slot);
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXQRMV_K_OK,queuenum);
			}
#endif
	/* see if queue_ptr now empty */
	if (--queue_ptr->queue_cnt)
		{
		K_I_Func_Return();	/* no, release task block */
		return(K_OK);	/* return good operation */
		}
	K_I_Func_Return();		/* yes, queue empty, release task block */
	return(K_QUE_EMPTY);	/* return warning that queue now empty */
}

/***********************************************************
		MESSAGE FUNCTIONS
**********************************************************/
/**********************************************************
 The following is the generalize message wait function

 is_get = 0, means to wait for message, with possible timeout

 is_get = 1, means don't wait for message, if none. 

**********************************************************/
void * K_I_Mesg_Wait_Common(byte mailbox,word16 timecnt,byte is_get)
{
	MAILBOX *mail_ptr;
	byte *copy_ptr;
	MSG *link;

	if (mailbox >= MAX_MAILBOXES)	/* see if mailbox number OK. */
		{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in3(CXMSWATM_K_ERROR,mailbox,is_get);
			}
#endif
		return(NULL);	/* no, return null idicating error. */
		}
	K_I_Disable_Sched();	/* set task block. */
	mail_ptr = &mail_box[mailbox];	/* address of the CMX mailbox handler. */
	if (mail_ptr->waiter)	/* see if another task already "owns" this mailbox. */
		{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in3(CXMSWATM_MAILBOX_K_ERROR,mailbox,is_get);
			}
#endif
		K_I_Func_Return();	/* release task block. */
		return(NULL);	/* return error, for only one task may wait on mailbox. */
		}
	if (!mail_ptr->first_lnk) 	/* any messages present in mailbox, null = no */
		{
		if (is_get)	/* K_Mesg_Get function used, do not wait */
			{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXMSGET_NOMESG_K_ERROR,mailbox);
			}
#endif
			K_I_Func_Return();	/* release task block. */
			return(NULL);	/* return error. */
			}
		mail_ptr->waiter = activetcb;	/* identify who "owns" mailbox. */
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in4(CXMSWATM_CALL,mailbox,timecnt);
			}
#endif
		K_I_Time_Common(timecnt,WAIT_MESG);	/* go suspend task. */

		/* task will be return to here by the K_I_Time_Common function. */
		mail_ptr->waiter = NULL;	/* now task will NOT own mailbox. */
		/* test to see if time expired or task forcefully woken, prior
			to a message being set to this mailbox. */
		if (activetcb->tcbstate & TIME_EXPIRED)
			{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXMSWATM_NOMESG_K_ERROR,mailbox);
			}
#endif
			return(NULL);	/* yes, pass back null, indicating no message. */
			}
		else
			{
			K_I_Disable_Sched();	/* set task lock */
			}
		}

	link = mail_ptr->first_lnk;	/* get first link. */
	link->env_link = message_ptr->env_link; /* re-arrange linked list. */
	message_ptr->env_link = link;
	message_free++;	/* increment message free counter. */

	copy_ptr = link->message;		/* address of message */
	activetcb->mesg_sender = link->sender;	/* what task sent this message. */

	/* update link to point to next message link, also test if more messages
		present. */
	if ((mail_ptr->first_lnk = link->link)) 
		{
		if (mail_ptr->task_num)	/* see if mailbox is suppose to signal event,
											that it has messages. */
			/* yes, then go set proper event bit. */
			K_Event_Signal(0,mail_ptr->task_num,mail_ptr->event_num);
		}
	link->link = NULL;	/* set message link to NULL. */
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			if (is_get)
				cmxtracker_in5(CXMSGET_K_OK,mailbox,copy_ptr);
			else
				cmxtracker_in5(CXMSWATM_K_OK,mailbox,copy_ptr);
			}
#endif
	K_I_Func_Return();	/* release task block */
	return((void *)copy_ptr);	/* return good status to caller */
}

/*******************************************************************
 General function for sending a message to a mailbox.
 
 wait = 0, do not wait after sending message

 wait = 1, wait after sending message, for receiver to wake us

*******************************************************************/
byte K_I_Mesg_Send_Common(byte mailbox,word16 timecnt,void *mesg,byte wait)
{
	MAILBOX *mail_ptr;
	MSG *link;	/* scratch pointer */

	K_I_Disable_Sched();
	/* see if mailbox within range and that there are
		message links available. */
	if ((mailbox >= MAX_MAILBOXES) || (!message_free))	
		{
		/* error */
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in3(CXMSSEND_K_ERROR,mailbox,wait);
			}
#endif
		K_I_Func_Return();	/* release task block */
		return(K_ERROR);	/* return good status to caller */
		}
	mail_ptr = &mail_box[mailbox];	/* CMX mailbox handler. */
	if (mail_ptr->waiter)	/* task waiting on this mailbox */
		{
		mail_ptr->waiter->tcbstate = RESUME;	/* allow task to run again */
		if (mail_ptr->waiter->priority < active_priority)
			PREEMPTED;	/* yes, so set the preempted scheduling flag */
		}

	link = message_ptr;	/* link now points to message header */
	message_ptr = link->env_link; /* update linked list */

	if (mail_ptr->first_lnk)	/* see if first link set */
		{
		mail_ptr->next_lnk->link = link;	/* yes, set next link. */
		}
	else
		{
		mail_ptr->first_lnk = link;	/* set first link. */
		/* because this is the first message into the mailbox
			see if mailbox is suppose to notify task that there
			are messages now residing in mailbox. */
		if (mail_ptr->task_num)	/* does task want to be notified? */
			/* yes, go set event. */
			{
#ifdef CMXTRACKER
			if (CMXTRACKER_ON)
				{
				if (locked_out == 2)
					{
					cmxtracker_in1(INT_ACTION);
					}
				}
#endif
			K_Event_Signal(0,mail_ptr->task_num,mail_ptr->event_num);
			}
		}
	mail_ptr->next_lnk = link;	/* set mailbox next link. */
	link->message = (byte *)mesg;	/* load address of message. */
	link->sender = NULL; /* set sender to NULL. This way interrupt will be covered */
	--message_free;	/* decrement message free */

	if (wait)	/* is this the send and wait call function */
		{
		link->sender = activetcb; /* load what task sent message. */
		/* this task will now be suspended. */
#ifdef CMXTRACKER
			if (CMXTRACKER_ON)
				{
				cmxtracker_in5(CXMSSENW_K_OK,mailbox,mesg);
				cmxtracker_in4(CXMSSEND_CALL,mailbox,timecnt);
				}
#endif
		if (K_I_Time_Common(timecnt,SEND_MESG))
			{
#ifdef CMXTRACKER
			if (CMXTRACKER_ON)
				{
				cmxtracker_in2(CXMSSEND_K_TIMEOUT,mailbox);
				}
#endif
			return(K_TIMEOUT);
			}
		else
			{
#ifdef CMXTRACKER
			if (CMXTRACKER_ON)
				{
				cmxtracker_in2(CXMSSEND_K_OK,mailbox);
				}
#endif
			return(K_OK);
			}
		}
	else
		{
		link->sender = NULL; /* set sender to NULL. */
#ifdef CMXTRACKER
			if (CMXTRACKER_ON)
				{
				cmxtracker_in5(CXMSSEND_K_OK1,mailbox,mesg);
				}
#endif
		K_I_Func_Return();	/* release task block */
		return(K_OK);	/* return good status to caller */
		}
}

/************************************************************
 the following function sets up a mailbox to automatically
 set an event of a particular task, when and if this mailbox
 has messages in it.
*************************************************************/
byte K_Mbox_Event_Set(byte mailbox,byte taskid,word16 event_num)
{
	MAILBOX *mail_ptr;

	if (mailbox >= MAX_MAILBOXES) /* see if mailbox number OK. */
		{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXMSBXEV_K_ERROR,mailbox);
			}
#endif
		return(K_ERROR);	/* NO, send error status. */
		}
	K_I_Disable_Sched();	/* set task block. */

	mail_ptr = &mail_box[mailbox];	/* get CMX mailbox handler */
	mail_ptr->task_num = taskid;	/* load task_num with task slot number. */
	mail_ptr->event_num = event_num;	/* what event bit will be set. */
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXMSBXEV_K_OK,mailbox);
			}
#endif
	K_I_Func_Return();	/* release task block. */
	return(K_OK);	/* return good status. */
}

/*****************************************************************
 This function allows a task that recieved a message to go wake
 the task that sent this message, if the task is waiting for an
 acknowledgement.
*****************************************************************/
byte K_Mesg_Ack_Sender(void)
{
	K_I_Disable_Sched();	/* set task block. */
	if (activetcb->mesg_sender)	/* see if message was sent by a task. */
		{
		/* see if the task that sent message waiting for acknowledgement. */
		if (activetcb->mesg_sender->tcbstate & SEND_MESG)	
			{
			activetcb->mesg_sender->tcbstate = RESUME; /* yes, wake task. */
			/* see if this task has a higher priority, then current running task. */
			if (activetcb->mesg_sender->priority < active_priority)
				PREEMPTED;	/* yes, so set the preempted scheduling flag */
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXMSACK_K_OK,(byte)(activetcb->mesg_sender - cmx_tcb));
			}
#endif
			}
		}
	K_I_Func_Return();	/* release task block. */
	return(K_OK);	/* always return good status */
}

/********************************************************************
		CYCLIC TIMER FUNCTIONS
*********************************************************************/
/* tproc_start states:
	0 = stopped
	1 = started
*/

/********************************************************************
 This function sets up the cyclic timer's event parameters.
********************************************************************/
byte K_Timer_Create(byte timed_num,byte mode,byte tskid_pri,word16 event_num)
{
	struct _tcproc *tpptr;

	if (timed_num >= MAX_CYCLIC_TIMERS)	/* have we exceeded the maximum number */
		{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXCTCRE_K_ERROR,timed_num);
			}
#endif
		return(K_ERROR);	/* yes, return error status */
		}
	K_I_Disable_Sched();	/* set task block */
	tpptr = &tcproc[timed_num];	/* get address of cyclic timer handler */
	tpptr->mode = mode;	/* load K_Event_Signal MODE */ 
	tpptr->tskid_pri = tskid_pri;	/* load in the tasks slot number
												or priority. May not be used. */
	tpptr->event_num = event_num;	/* load in the event to be set. */
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXCTCRE_K_OK,timed_num);
			}
#endif
	K_I_Func_Return();	/* release task block */
	return(K_OK);	/* return good status */
}

/******************************************************************
 This is the common routine that the most of the cyclic timer functions
 will call. This function starts, or restarts a cyclic timer and possibly
 also changes the cyclic timer's time parameters.

mode:
= 0: = just restart timer if not started 
= 1: = change only initial time and start 
= 2: = change only cyclic time and start 
= 3: = change both initial time and cyclic time and start
**************************************************************************/
byte K_I_Cyclic_Common(byte timed_num,word16 timecnt, word16 cyclic,byte mode)
{
	struct _tcproc *tpptr;

	if (timed_num >= MAX_CYCLIC_TIMERS)	/* have we exceeded the maximum number */
		{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXCTCOM_K_ERROR,timed_num);
			}
#endif
		return(K_ERROR);
		}
	K_I_Disable_Sched();	/* set task block */
	tpptr = &tcproc[timed_num];	/* get the CMX handler for this cyclic timer. */
	if (mode & 0x01)	/* see if mode bit 0 set. */
		{
		tpptr->tproc_timer = timecnt;	/* load time period into counter */
		}
	if (mode & 0x02)	/* see if mode bit 1 set. */
		{
		tpptr->reload_time = cyclic;	/* load cyclic time into counter */
		}
	if (!(tpptr->tproc_start))	/* see if 0 */
		{
		tpptr->tproc_start = 1;	/* start the timed procedure */
		/* add to the cyclic timers doubly linked list. */
		if (cyclic_lnk->ftlink != (struct _tcproc *)cyclic_lnk)	
			{
			tpptr->ftlink = cyclic_lnk->btlink->ftlink;
			cyclic_lnk->btlink->ftlink = tpptr;
			tpptr->btlink = cyclic_lnk->btlink;
			}
		else
			{
			tpptr->ftlink = tpptr->btlink = (struct _tcproc *)cyclic_lnk;
			cyclic_lnk->ftlink = tpptr;
			}
		cyclic_lnk->btlink = tpptr;
		}
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			if (mode == 3)
				{
				cmxtracker_in2(CXCTSTT_K_OK,timed_num);
				}
			else if (mode == 2)
				{
				cmxtracker_in2(CXCTRSTC_K_OK,timed_num);
				}
			else if (mode == 1)
				{
				cmxtracker_in2(CXCTRSTI_K_OK,timed_num);
				}
			else
				{
				cmxtracker_in2(CXCTRSTT_K_OK,timed_num);
				}
			}
#endif
	K_I_Func_Return();	/* release task block */
	return(K_OK);	/* return good status */
}

/******************************************************************
 stop a cyclic timer function
******************************************************************/
byte K_Timer_Stop(byte timed_num)
{
	struct _tcproc *tpptr;

	if (timed_num >= MAX_CYCLIC_TIMERS)	/* have we exceeded the maximum number */
		{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXCTSTP_K_ERROR,timed_num);
			}
#endif
		return(K_ERROR);
		}
	K_I_Disable_Sched();	/* set task block */
	tpptr = &tcproc[timed_num];	/* address of cyclic timer structure. */
	if (tpptr->tproc_start)	/* see if non 0 */
		{
		tpptr->tproc_start = 0;	/* stop cyclic timer. */
		/* the following removes the cyclic timer from
			the doubly linked list of started cyclic timers. */
		tpptr->ftlink->btlink = tpptr->btlink;
		tpptr->btlink->ftlink = tpptr->ftlink;
		}
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(CXCTSTP_K_OK,timed_num);
			}
#endif
	K_I_Func_Return();	/* release task block */
	return(K_OK);	/* return good status */
}


/*********************************************************************

		memory manager

**********************************************************************/
/* here's what a memory partition will look like
	ptr head;	 ptr to head of free list_ptr
	byte body;	 body of memory partition starts here
	does not test to ensure size of array is large enough for
	block size times the number of blocks */

/****************************************************************
 This function creates a fixed memory block according to the
 parameters passed. Make note, that the memory address supplied
 as the beginning of this fixed memory block is not tested as
 to legal CPU boundries and that the size of this memory block
 is free to be used.
****************************************************************/
void K_Mem_FB_Create(void *part,word16 bsize,word16 nblocks)
{
	byte *link;		/* scratch pointer */

#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in5(CXBFCRE_K_OK,0,part);
			}
#endif
	link = (byte *)part + sizeof(byte *); /* address of 1st block */
	*(byte **) part = link;	/* point head at 1st block */

	while (nblocks--)
	{
		part = link;		/* skip to next block */
		/* compute addr of next block */
		link += bsize;		/* compute addr of next block */
		*(byte **) part = link;	/* create link to it */
	}
	*(word16 **) part = 0;	/* last link in chain is null */
}

/****************************************************************
 this function passes a pointer of a free fixed memory block
 if one is available, to the caller.
****************************************************************/
byte K_Mem_FB_Get(void *part,byte **addr)
{
	byte *link;	/* scratch pointer */

	K_I_Disable_Sched();	/* prevent interruption by another task */
	link = *(byte **) part;	/* get head of free memory block */
	if (link != NULL)	/*  exhausted if link is null */
		{
		*(byte **) part = *(byte **) link; /* update head */
		*addr = link;	/* load address of memory block to user pointer */
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in5(CXBFGET_K_OK,0,link);
			}
#endif
		K_I_Func_Return();	/* release task block. */
		return(K_OK);	/* return good status. */
		}
	else
		{
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in5(CXBFGET_K_ERROR,0,part);
			}
#endif
		K_I_Func_Return();	/* release task block. */
		return(K_ERROR);		/* Error: no more memory blocks available */
		}
}	


/****************************************************************
 this function releases fixed memory block, back into the fixed block
 memory pool.
****************************************************************/
void K_Mem_FB_Release(void *part,byte *addr)
{
	byte *link;	/* scratch pointer */

	K_I_Disable_Sched();	/* set task block. */
	link = *(byte **) part;	/* save old head of free memory */
	*(byte **) addr = link;	/* point released block at old head */
	*(byte **) part = addr;	/* point head at released block */
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in5(CXBFREL_K_OK,0,addr);
			}
#endif
	K_I_Func_Return();	/* release task block. */

}

void K_OS_Start(void)
{
	PROC_DISABLE_INT;	/* disable interrupts. */
	PREEMPTED;	/* set preempted K_I_Scheduler flag. */
	CMX_ACTIVE;	/* identify that were CMX OS active. */
	K_I_Scheduler();			/* invoke K_I_Scheduler */
}

/*******************************************************************
 The following is the CMX timer task. This will see if any tasks
 need their time counters decremented (because they have specified
 a time period, and thier waiting). If so then this will use the doubly
 linked list of tasks that need their time counters decremented. Once a
 task has it's time counter decrement to 0, then the task will be place
 into the RESUME state, and remove from the linked list.

 Also the cyclic timers linked list will be tested to see if any cyclic
 timers need thier respective time counters decremented. If so then this
 will use the doubly	linked list of cyclic timers that need their time
 counters decremented. Once a cyclic timer has it's time counter decrement
 to 0, then the cyclic timer will have its K_Event_Signal parameters executed
 and also the cyclic time will be reloaded, if non zero.
****************************************************************/
 
void K_I_Timer_Task(void)
{
	tcbpointer x;
	struct _tcproc *tpptr;

	tpptr = cyclic_lnk->ftlink;	/* set pointer to cyclic linked list. */
	while (tpptr != (struct _tcproc *)cyclic_lnk) /* test for done of list. */
		{
		if (!(--tpptr->tproc_timer))	/* decrement time counter and test. */
			{
			/* if 0, then reload time counter, with cyclic time. */
			if ((tpptr->tproc_timer = tpptr->reload_time) == 0)
				{
				/* if cyclic time period 0, then stop cyclic timer
					and remove from linked list. */
				tpptr->tproc_start = 0;
				tpptr->ftlink->btlink = tpptr->btlink;
				tpptr->btlink->ftlink = tpptr->ftlink;
				}
			/* execute the cyclic timer's K_Event_Signal function parameters. */ 
#ifdef CMXTRACKER
		if (CMXTRACKER_ON)
			{
			cmxtracker_in2(TIMER_TASK_ACTION,(byte)(tpptr - tcproc));
			}
#endif
			K_Event_Signal(tpptr->mode,tpptr->tskid_pri,tpptr->event_num);
			}
		/* adjust pointer to next linked list member. */
		tpptr = tpptr->ftlink;
		} /* while loop */
	/* set pointer to beginning of task timer link list. */
	x = tsk_timer_lnk->ftlink;
	while (x != (tcbpointer)tsk_timer_lnk)	/* if not end of linked list. */
		{
		if (x->tcbstate & TIME)	/* see if task really waiting on time out. */
			{
			if (!(--x->tcbtimer))	/* yes, go decrement the task time counter. */
				{
				/* if time expired, test to see if task waiting on resource. */
				if (x->tcbstate & (RESOURCE | SEMAPHORE))
					{
					/* yes, remove task from resource wait linked list. */
					x->fwlink->bwlink = x->bwlink;
					x->bwlink->fwlink = x->fwlink;
					}
				/* set the task into the RESUME state. */
				x->tcbstate = RESUME | TIME_EXPIRED;
				/* indicate we should remove task from link list, this
					will be done by the function that place this task
					into the wait (sleep) state. This way we do NOT
					waste time in here performing this. */
				x->tcbtimer = 1;
				if (x->priority < active_priority) 
					PREEMPTED;	/* yes, set preempted scheduling flag */
				}
			}
		x = x->ftlink;	/* point to next link. */
		}
}	/* K_I_Timer_Task */

/*****************************************************************
 Semaphore stuff
******************************************************************/
/*
 Pend on semaphore, if count 0, otherwise return with semaphore

 mode 0 : wait if semaphore not present 
 mode 1 : do NOT wait if semaphore not present, return immediatedly

*/

byte K_I_Semaphore_Get_Common(byte sem_num, word16 timecnt, byte mode)	
{
	SEM *sem_ptr;

	if (sem_num >= MAX_SEMAPHORES)
		{
		return(K_ERROR);	/* return good status to caller */
		}
	K_I_Disable_Sched();
	sem_ptr = &sem_array[sem_num];	/* CMX semaphore handler. */
	if (sem_ptr->fwlink == NULL)
		{
		K_I_Func_Return();	/* release task block */
		return(K_ERROR);	/* return good status to caller */
		}
	if (!sem_ptr->sem_count)
		{
		if (mode)
			{
			K_I_Func_Return();	/* release task block */
			return(K_SEMAPHORE_NONE);	/* return error that semaphore
											had not been posted to */
			}
		if (sem_ptr->fwlink != (tcbpointer)sem_ptr)	/* is at least one task waiting
																		on is semaphore */
			{
			activetcb->fwlink = sem_ptr->bwlink->fwlink;
			sem_ptr->bwlink->fwlink = activetcb;
			activetcb->bwlink = sem_ptr->bwlink;
			sem_ptr->bwlink = activetcb;
			}
		else
			{
			sem_ptr->fwlink = sem_ptr->bwlink = activetcb;
			activetcb->fwlink = activetcb->bwlink = (tcbpointer)sem_ptr;
			}
		if (K_I_Time_Common(timecnt,SEMAPHORE))	/* go suspend task. */
			return(K_TIMEOUT);	/* return the warning: that the time period expired */
		else
			return(K_OK);
		}
	--sem_ptr->sem_count;
	K_I_Func_Return();
	return(K_OK);
}	/* K_I_Semaphore_Get_Common */

byte K_Semaphore_Post(byte sem_num)	
{
	SEM *sem_ptr;
	tcbpointer tp;

	if (sem_num >= MAX_SEMAPHORES)
		{
		return(K_ERROR);	/* return good status to caller */
		}
	K_I_Disable_Sched();
	sem_ptr = &sem_array[sem_num];	/* CMX semaphore handler. */
	if (sem_ptr->fwlink == NULL)
		{
		K_I_Func_Return();	/* release task block */
		return(K_ERROR);	/* return good status to caller */
		}
	++sem_ptr->sem_count;
	if (sem_ptr->fwlink != (tcbpointer)sem_ptr)	/* does a task own this semaphore */
		{
		sem_ptr->fwlink->tcbstate = RESUME;	/* allow task to run again */
		if (sem_ptr->fwlink->priority < active_priority)
			PREEMPTED;	/* yes, so set the preempted scheduling flag */
		tp = sem_ptr->fwlink;
		tp->fwlink->bwlink = tp->bwlink;
		sem_ptr->fwlink = tp->fwlink;
		--sem_ptr->sem_count; /* reduce count again, because this task now
										 has semaphore */
		}
	K_I_Func_Return();	/* release task block */
	return(K_OK);	/* return good status to caller */
}	/* K_Semaphore_Post */

byte K_Semaphore_Create(byte sem_num, word16 n)	
{
	SEM *sem_ptr;

	if (sem_num >= MAX_SEMAPHORES)
		{
		return(K_ERROR);	/* return good status to caller */
		}
	K_I_Disable_Sched();
	sem_ptr = &sem_array[sem_num];	/* CMX semaphore handler. */
	sem_ptr->fwlink = (tcbpointer)sem_ptr;
	sem_ptr->bwlink = (tcbpointer)sem_ptr;
	sem_ptr->sem_n = sem_ptr->sem_count = n;
	K_I_Func_Return();	/* release task block */
	return(K_OK);	/* return good status to caller */
}

/*
mode = 0 = do not flush if task owns semaphore
mode = 1 = do flush if task owns semaphore
also reset count to original value
*/
byte K_Semaphore_Reset(byte sem_num,byte mode)	
{
	SEM *sem_ptr;

	if (sem_num >= MAX_SEMAPHORES)
		{
		return(K_ERROR);	/* return good status to caller */
		}
	K_I_Disable_Sched();
	sem_ptr = &sem_array[sem_num];	/* CMX semaphore handler. */
	if (sem_ptr->fwlink == NULL)
		{
		K_I_Func_Return();	/* release task block */
		return(K_ERROR);	/* return good status to caller */
		}
	if (sem_ptr->fwlink != (tcbpointer)sem_ptr)	/* see if another task already "owns" this semaphore. */
		{
		if (!mode)
			{
			K_I_Func_Return();	/* release task block. */
			return(K_ERROR);
			}
		else
			{
			do {
				sem_ptr->fwlink->tcbstate = RESUME | TIME_EXPIRED;	/* allow task to run again */
				if (sem_ptr->fwlink->priority < active_priority)
					PREEMPTED;	/* yes, so set the preempted scheduling flag */
				if ((sem_ptr->fwlink = sem_ptr->fwlink->fwlink) == (tcbpointer)sem_ptr)
					sem_ptr->bwlink = (tcbpointer)sem_ptr;
				}
			while(sem_ptr->fwlink != (tcbpointer)sem_ptr);
			}
		}
	sem_ptr->sem_count = sem_ptr->sem_n;
	K_I_Func_Return();	/* release task block. */
	return(K_OK);
}



