extern byte MAX_TASKS;
extern byte MAX_RESOURCES;
extern byte MAX_CYCLIC_TIMERS;
extern byte MAX_MAILBOXES;
extern byte MAX_QUEUES;
extern byte RTC_SCALE;
extern byte MAX_SEMAPHORES;
extern byte TSLICE_SCALE;
extern byte SLICE_ON;

extern struct _tcb cmx_tcb[];

extern MAILBOX mail_box[];

extern MSG message_box[];

extern MSG *message_ptr;
extern word16 message_free;

extern QUEHDR queue[];

extern CYCLIC_TIMERS tcproc[];

extern CYCLIC_LNK *cyclic_lnk;

extern CYCLIC_LNK cyclic_buf;

extern TSK_TIMER_LNK *tsk_timer_lnk;

extern TSK_TIMER_LNK tsk_timer_buf;

extern RESHDR res_que[];

#ifdef CMXTRACKER
extern byte CMXTRACKER_ON;
#endif

extern word16 *stack_blk;

extern SEM sem_array[];

extern tcbpointer timertask;
extern byte rtc_count;

extern tcbpointer activetcb; 
extern byte active_priority;
extern byte tslice_count;

extern word32 cmx_tick_count;



