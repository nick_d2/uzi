/* strlen.h for uzi180 by Nick - subroutines based on /uzi/libc source */

#ifndef __STRLEN_H
#define __STRLEN_H

size_t strlen(char *str);

#endif /* __STRLEN_H */

