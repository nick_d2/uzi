; _exit.asm for UZI180 by Nick

; -----------------------------------------------------------------------------

	module	_exit

	public	_exit

 .if 1 ; send continuous reboot commands
	extern	_acflag
 .else
	extern	_abflag
 .endif
	extern	abyte, ahexw, amess, acrlf

$ io64180.inc

	extern	_sdevs

; must match cxfuncs.h, fields in SDEV structure:  ** ASSUMES 3 BYTE CODE PTRS!
sxmit	equ	0			; offset of embedded CMX XMIT structure
srecv	equ	13			; offset of embedded CMX RECV structure
sport	equ	13+13
stxvec	equ	13+13+1			; called by interrupt, returns l=chr
srxvec	equ	13+13+4			; called by interrupt, passed c=chr
smsvec	equ	13+13+7			; called by interrupt, passed c=bits
sesvec	equ	13+13+10		; called by interrupt, passed c=bits
stevec	equ	13+13+13		; vector to start asci/escc transmitter
stdvec	equ	13+13+16		; vector to stop asci/escc transmitter
sstat	equ	13+13+19		; address to read status (8530 02xxh)
stxdr	equ	13+13+20		; address to write tx data (8530 02xxh)
srxdr	equ	13+13+21		; address to read rx data (8530 02xxh)
swr1	equ	13+13+22		; wr1 contents (interrupt enable bits)
swr5	equ	13+13+23		; wr5 contents (modem control outputs)
swr15	equ	13+13+24		; wr15 contents (modem control int ena)
sinint	equ	13+13+25		; says whether wr1 really reflects wr1!
sdvlen	equ	13+13+26		; byte size of block per serial port

; -----------------------------------------------------------------------------

	rseg	CODE

_exit::
 .if 1
	ld	hl,(_sdevs+1*sdvlen+sxmit+4) ; bytes_out
	ld	a,l
	or	h
	jr	nz,_exit		; wait for outgoing traffic to be sent
 .endif

	call	amess
	defb	'kernel: rebooting: exitcode ',0
	ex	de,hl
	call	ahexw
	call	acrlf

	ld	hl,0
	; wait for last character to be sent @ 9600
reboot_delay:
	dec	hl
	ld	a,l
	or	h
	jr	nz,reboot_delay

	ld	a,0aah
 .if 1 ; send continuous reboot commands
	ei
reboot_loop:
	ld	(_acflag),a
	jr	reboot_loop
 .else
	ld	(_abflag),a

	ei
	jr	$			; loop forever
 .endif

; -----------------------------------------------------------------------------

	end
