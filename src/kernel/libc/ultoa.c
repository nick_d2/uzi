/* numeric/string conversions package
 */

/* #include "cvt.h" */
#include "vendor.h" /* Nick, must come first */

#include <stdlib.h>
#include "ultoa.h"
#include "itoa.h"

/**************************** ultoa.c ****************************/
/* #ifdef L_ultoa */

#if 1 /* Nick */
char *ultoa(unsigned long value, char *strP, int radix)
#else
char  *ultoa(value, strP, radix)
	unsigned long value;
	char *strP;
	int radix;
#endif
{
	char hex = 'A';

	if (radix < 0) {
		hex = 'a';
		radix = -radix;
	}
	return	__longtoa (value, strP, radix, 0, hex);
}

/* #endif L_ultoa */

