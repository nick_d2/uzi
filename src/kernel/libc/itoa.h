/* itoa.h for uzi180 by Nick - subroutines based on /uzi/libc source */

#ifndef __ITOA_H
#define __ITOA_H

char *__longtoa(unsigned long value, char *strP, int radix,
		char maybeSigned, char hexStyle);
char *itoa(int value, char *strP, int radix);

#endif /* __ITOA_H */

