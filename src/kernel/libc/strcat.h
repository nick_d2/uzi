/* strcat.h for uzi180 by Nick - subroutines based on /uzi/libc source */

#ifndef __STRCAT_H
#define __STRCAT_H

char *strcat(char *d, char *s);

#endif /* __STRCAT_H */

