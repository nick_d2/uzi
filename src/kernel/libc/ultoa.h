/* ultoa.h for uzi180 by Nick - subroutines based on /uzi/libc source */

#ifndef __ULTOA_H
#define __ULTOA_H

char *ultoa(unsigned long value, char *strP, int radix);

#endif /* __ULTOA_H */

