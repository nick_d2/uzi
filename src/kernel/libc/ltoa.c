/* numeric/string conversions package
 */

/* #include "cvt.h" */
#include "vendor.h" /* Nick, must come first */

#include <stdlib.h>
#include "ltoa.h"
#include "itoa.h"

/**************************** ltoa.c ****************************/
/* #ifdef L_ltoa */

#if 1 /* Nick */
char *ltoa(long value, char *strP, int radix)
#else
char *ltoa(value, strP, radix)
	long value;
	char *strP;
	int radix;
#endif
{
	char hex = 'A';

	if (radix < 0) {
		hex = 'a';
		radix = -radix;
	}
	return	__longtoa (value, strP, radix, (radix == 10), hex);
}

/* #endif L_ltoa */

