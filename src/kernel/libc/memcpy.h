/* memcpy.h for uzi180 by Nick - subroutines based on /uzi/libc source */

#ifndef __MEMCPY_H
#define __MEMCPY_H

void *memcpy(void *d, void *s, size_t l);

#endif /* __MEMCPY_H */

