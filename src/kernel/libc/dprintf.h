/* dprintf.h for uzi180 by Nick - subroutines based on /uzi/libc source */

#ifndef __DPRINTF_H
#define __DPRINTF_H

int dprintf(char *fmt, ...);

#endif /* __DPRINTF_H */

