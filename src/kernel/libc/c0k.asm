; c0k.asm by Nick for UZI180 banked memory model (creates a kernel executable)

; -----------------------------------------------------------------------------

 .if 1
$ io64180.inc
 .endif

	module	_c0k

	public	init			; vendor _init
	extern	_main
	extern	exit			; vendor _exit
 .if 1
	extern	abyte
 .endif
	extern	copyr

	extern	e_RCODE			; e_hsize
	extern	s_ICODE			; e_idata
	extern	s_UDATA0		; e_udata
	extern	s_CSTACK		; e_stack
	extern	e_CSTACK		; e_break

	extern	_int1_vector
	extern	_int2_vector
	extern	_timer0_vector
	extern	_csio_vector
	extern	_asci0_vector
	extern	_asci1_vector

	extern	?BANK_CALL_DIRECT_L08

E_MAGIC		equ	0a6c9h
E_FORMAT_KERNEL	equ	3
E_STACK_SIZE	equ	1000h

; -----------------------------------------------------------------------------
; forward definitions of segments, to set the linkage order (c0k must be first)

	rseg	RCODE
	rseg	ICODE
;	rseg	TEMP
;	rseg	DATA0
;	rseg	WCSTR
	rseg	CONST
	rseg	CSTR
	rseg	IDATA0(NUL)
	rseg	CDATA0
	rseg	ECSTR(NUL)
	rseg	CCSTR
	rseg	CODE(NUL)		; needs to be at end for banked model
	rseg	UDATA0
	rseg	CSTACK

; -----------------------------------------------------------------------------

	rseg	_DEFAULT		; means header is discarded at loading

	defw	E_MAGIC			; e_magic
	defw	E_FORMAT_KERNEL		; e_format
	defd	12345678h		; e_size
	defw	e_RCODE+14h		; e_hsize (14h = l__DEFAULT)
	defw	s_ICODE			; e_idata
	defw	init			; e_entry
	defw	s_UDATA0		; e_udata
	defw	s_CSTACK		; e_stack
	defw	e_CSTACK		; e_break

; -----------------------------------------------------------------------------

	rseg	ICODE

init::
 .if 0
 ld a,'A'
 call abyte
 .endif

 .if 1
	ld	a,.low.vector_table
	out0	(IL),a			; set interrupt vectors low byte
	ld	a,.high.vector_table
	ld	i,a			; set interrupt vectors high byte

	in0	a,(CBR)
	ld	(_os_bank),a		; for the CMX scheduler, and others

	ei				; now got a stack and interrupt vectors
 .endif

 .if 1
	ld	e,.high._rom_serial_no
	ld	d,0

	ld	a,(_os_bank)		; CBR value for kernel data segment
	ld	l,a
	ld	h,d ;0

	add	hl,hl
	add	hl,hl
	add	hl,hl
	add	hl,hl			; convert to absolute base address
	add	hl,de			; adjust for dest, carry into segment

	ld	e,h			; e = segment no. for copy destination
	ld	h,l
	ld	l,.low._rom_serial_no	; e:hl -> copy dest (in kernel data)

	ld	iy,0fc81h
	;ld	d,0			; d:iy -> copy source (in EPROM)

	ld	bc,5
	call	copyr

; not necessary as this will have been zeroed by seg_init (it's in UDATA0):
;	sub	a
;	ld	(_rom_sn+5),a
 .endif

 .if 0
	ld	a,'B'
	call	abyte
 .endif

; now there are the next stack structure:
;	+4 envp
;	+2 argv
; sp->	+0 argc
	pop	de
	ld	(_argc),de		; vendor __argc (2nd argument to main)
	pop	bc
	ld	(_argv),bc		; vendor __argv (1st argument to main)
	pop	hl
	ld	(environ),hl		; vendor _environ

	ld	HL,LWRD _main		; banked call to _main()
	ld	A,BYTE3 _main
	call	?BANK_CALL_DIRECT_L08

	ex	de,hl			; de = exitcode (1st argument to exit)

	ld	HL,LWRD exit		; banked call to _exit()
	ld	A,BYTE3 exit
	jp	?BANK_CALL_DIRECT_L08

; -----------------------------------------------------------------------------

	rseg	RCODE			; c0k must be the first module loaded !

common_start::
	jp	init+4000h		; +4000h because we're not loaded yet

	org	common_start+0x08
	ei
	ret
	org	common_start+0x10
	ei
	ret
	org	common_start+0x18
	ei
	ret
	org	common_start+0x20
	ei
	ret
	org	common_start+0x28
	ei
	ret
	org	common_start+0x30
	ei
	ret
	org	common_start+0x38
	ei
	ret

	org	common_start+0x40

	public	vector_table

vector_table::
	defw	_int1_vector
	defw	_int2_vector
	defw	_timer0_vector
	defw	_timer1_vector
	defw	_dma0_vector
	defw	_dma1_vector
	defw	_csio_vector
	defw	_asci0_vector
	defw	_asci1_vector

	public	_timer1_vector

_timer1_vector::

	public	_dma0_vector

_dma0_vector::

	public	_dma1_vector

_dma1_vector::
	ei
	ret

; -----------------------------------------------------------------------------

		rseg	RCODE

; Nick has added read/write data here (must be in RCODE)

		public	_os_bank

_os_bank:	defb	0		; the CBR value for CMX/UZI data seg

; Nick has added read/write data here (doesn't need to be in RCODE)

		rseg	UDATA0

		public	_rom_serial_no

_rom_serial_no:	defs	6		; for c:\uzi\kernel\main.c (hostname)

; -----------------------------------------------------------------------------

		public	_argc, _argv, environ, errno, __cleanup
		;public	__argc, __argv, _environ, _errno, ___cleanup

		rseg	UDATA0
_argc:		defs	2		; vendor __argc
_argv:		defs	2		; vendor __argv
environ:	defs	2		; vendor _environ
errno:		defs	2		; vendor _errno
__cleanup:	defs	3 ; oopsy 2	; vendor ___cleanup

		rseg	CSTACK
		defs	E_STACK_SIZE

; -----------------------------------------------------------------------------

	END
