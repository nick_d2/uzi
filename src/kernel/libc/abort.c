/* abort.c by Nick for UZI180 kernel */

#if 1 /* Nick, not sure what the other stuff is about */
void exit(int val); /* #include <stdlib.h> */ /* for exit() prototype */

void abort(int val)
	{
	exit(val);
	}
#else
/*
 */
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <syscalls.h>

void abort(void) {
	signal(SIGABRT, SIG_DFL);
	kill(SIGABRT, getpid());	/* Correct one */
	pause();			/* System may just schedule */
	signal(SIGKILL, SIG_DFL);
	kill(SIGKILL, getpid());	/* Can't trap this! */
	_exit(255);			/* WHAT!! */
}
#endif

