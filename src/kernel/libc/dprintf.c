/* dprintf.c
 *    Dale Schumacher			      399 Beacon Ave.
 *    (alias: Dalnefre')		      St. Paul, MN  55104
 *    dal@syntel.UUCP			      United States of America
 *
 * Altered to use stdarg, made the core function vfprintf.
 * Hooked into the stdio package using 'inside information'
 * Altered sizeof() assumptions, now assumes all integers except chars
 * will be either
 *  sizeof(xxx) == sizeof(long) or sizeof(xxx) == sizeof(short)
 *
 * -RDB
 */

/* #include "printf.h" */
#include "vendor.h" /* Nick, must come first */

#include <types.h>
#include <fcntl.h>
#if 1
#include <stdarg.h>
#define va_strt      va_start
#else
#include <varargs.h>
#define va_strt(p,i) va_start(p)
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "unix.h" /* Nick for udata.u_debugme */
#include "config.h" /* Nick for udata.u_debugme */
#include "extern.h" /* Nick for udata.u_debugme */

int dprintf(char debuglevel, char *fmt, ...)
{
#ifdef DEBUG
	va_list ptr;
	int rv;

	if (udata.u_debugme < debuglevel)
		{
		return 0;
		}

	va_strt(ptr, fmt);
#if 1 /* Nick */
	rv = vfprintf(NULL, fmt, ptr);
#else
	rv = vfprintf(stdout, fmt, ptr);
#endif
	va_end(ptr);
	return rv;
#else
	return 0;
#endif
}

