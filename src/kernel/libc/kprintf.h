/* kprintf.h for uzi180 by Nick - subroutines based on /uzi/libc source */

#ifndef __KPRINTF_H
#define __KPRINTF_H

int kprintf(char *fmt, ...);

#endif /* __KPRINTF_H */

