/* ltoa.h for uzi180 by Nick - subroutines based on /uzi/libc source */

#ifndef __LTOA_H
#define __LTOA_H

char *ltoa(long value, char *strP, int radix);

#endif /* __LTOA_H */

