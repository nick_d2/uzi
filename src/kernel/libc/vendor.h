/* vendor.h for uzi180 by Nick - essential defines for the IAR compiler */

#ifndef UTIL /* ensures we don't do anything when compiling utils under MSVC */

#define dprintf _dprintf
#define kprintf _kprintf
#define ub _ub

#if 1 /* Nick, see kprintf.c */
int kprintf(char *, ...); /* ensures IAR won't try to pass register params */
int dprintf(char debuglevel, char *fmt, ...);
#else
void kprintf(char *, ...); /* ensures IAR won't try to pass register params */
#endif

#endif /* UTIL */

