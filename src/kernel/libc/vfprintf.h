/* vfprintf.h for uzi180 by Nick - subroutines based on /uzi/libc source */

#ifndef __VFPRINTF_H
#define __VFPRINTF_H

int vfprintf(op, fmt, ap);

#endif /* __VFPRINTF_H */

