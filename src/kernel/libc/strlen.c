/* strlen.c
 * Copyright (C) 1995,1996 Robert de Bath <rdebath@cix.compulink.co.uk>
 * This file is part of the Linux-8086 C library and is distributed
 * under the GNU Library General Public License.
 */

/* #include "string-l.h" */
#include "vendor.h" /* Nick, must come first */

#include <string.h>
#include "strlen.h"

/********************** Function strlen ************************************/
/* #ifdef L_strlen */

#if 1 /* Nick */
size_t strlen(char *str)
#else
size_t strlen(str)
	register char *str;
#endif
{
#ifndef HI_TECH_C
	register char *p = str;

	while (*p != 0)
		++p;
	return (size_t)(p-str);
#else
_asm
	ld	h,d
	ld	l,e
	ld	bc,0ffffh
	xor	a
	cpir
	sbc	hl,de
	dec	hl
_endasm
#endif
}

/* #endif L_strlen */

