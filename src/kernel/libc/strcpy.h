/* strcpy.h for uzi180 by Nick - subroutines based on /uzi/libc source */

#ifndef __STRCPY_H
#define __STRCPY_H

char *strcpy(char *d, char *s);

#endif /* __STRCPY_H */

