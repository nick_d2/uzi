/* strcat.c
 * Copyright (C) 1995,1996 Robert de Bath <rdebath@cix.compulink.co.uk>
 * This file is part of the Linux-8086 C library and is distributed
 * under the GNU Library General Public License.
 */

/* #include "string-l.h" */
#include "vendor.h" /* Nick, must come first */

#include <string.h>
#include "strcat.h"

/********************** Function strcat ************************************/
/* #ifdef L_strcat */

#if 1 /* Nick */
char *strcat(char *d, char *s)
#else
char *strcat(d, s)
	char *d;
	char *s;
#endif
{
	strcpy(d + strlen(d), s);
	return d;
}

/* #endif L_strcat */

