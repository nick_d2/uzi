/* strncmp.h for uzi180 by Nick - subroutines based on /uzi/libc source */

#ifndef __STRNCMP_H
#define __STRNCMP_H

int strncmp(char *d, char *s, size_t l);

#endif /* __STRNCMP_H */

