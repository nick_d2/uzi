; LLSH.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?L_LSH_L03
		rseg	RCODE
rcode_base:
		public	?L_LSH_L03
?L_LSH_L03	equ	rcode_base+00000000h
		defb	0B7h,0C8h,0FEh,008h,038h,009h,041h
		defb	04Ch,065h,02Eh,000h,0D6h,008h,018h
		defb	0F2h,029h,0CBh,011h,0CBh,010h,03Dh
		defb	020h,0F8h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
