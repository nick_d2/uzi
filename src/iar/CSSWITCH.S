; CSSWITCH.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?C_S_SWITCH_L06
		rseg	RCODE
rcode_base:
		public	?C_S_SWITCH_L06
?C_S_SWITCH_L06	equ	rcode_base+00000000h
		defb	0E3h,0F5h,0D5h,016h,000h,07Bh,096h
		defb	023h,05Fh,096h,023h,07Ah,09Eh,023h
		defb	030h,004h,0EBh,023h,029h,019h,05Eh
		defb	023h,056h,0EBh,0D1h,0F1h,0E3h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
