; STRCMP.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?STRCMP_L11
		rseg	RCODE
rcode_base:
		public	?STRCMP_L11
?STRCMP_L11	equ	rcode_base+00000000h
		defb	0F5h,0D5h,01Ah,0BEh,020h,009h,013h
		defb	023h,0B7h,020h,0F7h,021h,000h,000h
		defb	011h,0CBh,01Ch,0D1h,0F1h,0C9h
		endmod

; -----------------------------------------------------------------------------

	end
