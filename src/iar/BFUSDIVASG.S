; BFUSDIVASG.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?BF_US_DIVASG_L10
		rseg	RCODE
rcode_base:
		public	?BF_US_DIVASG_L10
?BF_US_DIVASG_L10	equ	rcode_base+00000000h
		extern	?BF_US_LD_SHIFT_DOWN_L10
		extern	?US_DIV_L02
		extern	?BF_S_SHIFT_UP_L10
		extern	?BF_S_RET_VAL_L10
		defb	0EBh,0E3h,0DDh,0E5h,0D5h,0E5h,0DDh
		defb	0E1h,023h,023h,023h,0E3h,0E5h,0F5h
		defb	0C5h,0CDh
		defw	LWRD ?BF_US_LD_SHIFT_DOWN_L10
		defb	0CDh
		defw	LWRD ?US_DIV_L02
		defb	0CDh
		defw	LWRD ?BF_S_SHIFT_UP_L10
		defb	0C3h
		defw	LWRD ?BF_S_RET_VAL_L10
		endmod

; -----------------------------------------------------------------------------

	end
