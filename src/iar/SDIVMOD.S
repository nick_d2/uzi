; SDIVMOD.asm
; Nick's reverse engineered subset of the IAR banked 'C' library

; -----------------------------------------------------------------------------

		module	?S_DIVMOD_L02
		rseg	RCODE
rcode_base:
		public	?S_DIVMOD_L02
?S_DIVMOD_L02	equ	rcode_base+00000000h
		defb	021h,000h,000h,03Eh,011h,0CBh,013h
		defb	0CBh,012h,03Dh,0C8h,0CBh,015h,0CBh
		defb	014h,0EDh,042h,030h,0F2h,009h,018h
		defb	0EFh
		endmod

; -----------------------------------------------------------------------------

	end
