
/*
 * UNIX shell
 *
 * S. R. Bourne
 * Bell Telephone Laboratories
 *
 */

#include	"defs.h"
#include	"sym.h"

extern struct ionod *	inout();
extern int	chkword();
extern int	chksym();
extern struct trenod *	term();
extern struct trenod *	makelist();
extern struct trenod *	list();
extern struct regnod *	syncase();
extern struct trenod *	item();
extern int	skipnl();
extern int	prsym();
extern int	synbad();


/* ========	command line decoding	========*/




struct trenod *	makefork(flgs, i)
	int		flgs;
	struct trenod *		i;
{
	register struct trenod *	t;

	t=(struct trenod *)getstak(FORKTYPE);
	((struct forknod *)t)->forktyp=flgs|TFORK;
	((struct forknod *)t)->forktre=i;
	((struct forknod *)t)->forkio=0;
	return(t);
}

static struct trenod *	makelist(type,i,r)
	int		type;
	struct trenod *		i, * r;
{
	register struct trenod *	t;

	if ( i==0 || r==0
	) {	synbad();
	} else {
		t = (struct trenod *)getstak(LSTTYPE);
		((struct lstnod *)t)->lsttyp = type;
		((struct lstnod *)t)->lstlef = i;
		((struct lstnod *)t)->lstrit = r;
	; }
	return(t);
}

/*
 * cmd
 *	empty
 *	list
 *	list & [ cmd ]
 *	list [ ; cmd ]
 */

struct trenod *	cmd(sym,flg)
	register int		sym;
	int		flg;
{
	register struct trenod *	i, * e;

	i = list(flg);

	if ( wdval=='\n'
	) {	if ( flg&NLFLG
		) {	wdval=';'; chkpr('\n');
		; }
	} else if ( i==0 && (flg&MTFLG)==0
	) {	synbad();
	; }

	switch ( wdval ) {

	    case '&':
		if ( i
		) {	i = makefork(FINT|FPRS|FAMP, i);
		} else {	synbad();
		; }

	    case ';':
		if ( e=cmd(sym,flg|MTFLG)
		) {	i=makelist(TLST, i, e);
		; }
		break;

	    case EOFSYM:
		if ( sym=='\n'
		) {	break;
		; }

	    default:
		if ( sym
		) {	chksym(sym);
		; }

	}
	return(i);
}

/*
 * list
 *	term
 *	list && term
 *	list || term
 */

static struct trenod *	list(flg)
{
	register struct trenod *	r;
	register int		b;

	r = term(flg);
	while ( r && ((b=(wdval==ANDFSYM)) || wdval==ORFSYM)
	) {	r = makelist((b ? TAND : TORF), r, term(NLFLG));
	; }
	return(r);
}

/*
 * term
 *	item
 *	item |^ term
 */

static struct trenod *	term(flg)
{
	register struct trenod *	t;

	reserv++;
	if ( flg&NLFLG
	) {	skipnl();
	} else {	word();
	; }

	if ( (t=item((-1))) && (wdval=='^' || wdval=='|')
	) {	return(makelist(TFIL, makefork(FPOU,t), makefork(FPIN|FPCL,term(NLFLG))));
	} else {	return(t);
	; }
}

static struct regnod *	syncase(esym)
	register int	esym;
{
	skipnl();
	if ( wdval==esym
	) {	return(0);
	} else {
		register struct regnod *r = (struct regnod *)getstak(REGTYPE);
		r->regptr=0;
		for (;;) { wdarg->argnxt=r->regptr;
		     r->regptr=wdarg;
		     if ( wdval || ( word()!=')' && wdval!='|' )
		     ) { synbad();
		     ; }
		     if ( wdval=='|'
		     ) { word();
		     } else { break;
		     ; }
		}
		r->regcom=cmd(0,NLFLG|MTFLG);
		if ( wdval==ECSYM
		) {	r->regnxt=syncase(esym);
		} else {	chksym(esym);
			r->regnxt=0;
		; }
		return(r);
	; }
}

/*
 * item
 *
 *	( cmd ) [ < in  ] [ > out ]
 *	word word* [ < in ] [ > out ]
 *	if ... then ... else ... fi
 *	for ... while ... do ... done
 *	case ... in ... esac
 *	begin ... end
 */

static struct trenod *	item(flag)
	char		flag;
{
	register struct trenod *	t;
	register struct ionod *	io;

	if ( flag
	) {	io=inout((struct ionod *)0);
	} else {	io=0;
	; }

	switch ( wdval ) {

	    case CASYM:
		{
		   t=(struct trenod *)getstak(SWTYPE);
		   chkword();
		   ((struct swnod *)t)->swarg=wdarg->argval;
		   skipnl(); chksym(INSYM|BRSYM);
		   ((struct swnod *)t)->swlst=syncase(wdval==INSYM ?
								ESSYM : KTSYM);
		   ((struct swnod *)t)->swtyp=TSW;
		   break;
		}

	    case IFSYM:
		{
		   register int	w;
		   t=(struct trenod *)getstak(IFTYPE);
		   ((struct ifnod *)t)->iftyp=TIF;
		   ((struct ifnod *)t)->iftre=cmd(THSYM,NLFLG);
		   ((struct ifnod *)t)->thtre=cmd(ELSYM|FISYM|EFSYM,NLFLG);
		   ((struct ifnod *)t)->eltre=((w=wdval)==ELSYM ?
				cmd(FISYM,NLFLG) :
				(w==EFSYM ? (wdval=IFSYM, item(0)) : 0));
		   if ( w==EFSYM ) { return(t) ; }
		   break;
		}

	    case FORSYM:
		{
		   t=(struct trenod *)getstak(FORTYPE);
		   ((struct fornod *)t)->fortyp=TFOR;
		   ((struct fornod *)t)->forlst=0;
		   chkword();
		   ((struct fornod *)t)->fornam=wdarg->argval;
		   if ( skipnl()==INSYM
		   ) {	chkword();
			((struct fornod *)t)->forlst=(struct comnod *)item(0);
			if ( wdval!='\n' && wdval!=';'
			) {	synbad();
			; }
			chkpr(wdval); skipnl();
		   ; }
		   chksym(DOSYM|BRSYM);
		   ((struct fornod *)t)->fortre=cmd(wdval==DOSYM ?
							ODSYM : KTSYM, NLFLG);
		   break;
		}

	    case WHSYM:
	    case UNSYM:
		{
		   t=(struct trenod *)getstak(WHTYPE);
		   ((struct whnod *)t)->whtyp=(wdval==WHSYM ? TWH : TUN);
		   ((struct whnod *)t)->whtre = cmd(DOSYM,NLFLG);
		   ((struct whnod *)t)->dotre = cmd(ODSYM,NLFLG);
		   break;
		}

	    case BRSYM:
		t=cmd(KTSYM,NLFLG);
		break;

	    case '(':
		{
		   register struct parnod *	 p;
		   p=(struct parnod *)getstak(PARTYPE);
		   p->partre=cmd(')',NLFLG);
		   p->partyp=TPAR;
		   t=makefork(0,p);
		   break;
		}

	    default:
		if ( io==0
		) {	return(0);
		; }

	    case 0:
		{
		   register struct argnod *	argp;
		   register struct argnod *	*argtail;
		   register struct argnod *	*argset=0;
		   int		keywd=1;
		   t=(struct trenod *)getstak(COMTYPE);
		   ((struct comnod *)t)->comio=io; /*initial io chain*/
		   argtail = &(((struct comnod *)t)->comarg);
		   while ( wdval==0
		   ) {	argp = wdarg;
			if ( wdset && keywd
			) {
				argp->argnxt=(struct argnod *)argset;
				argset=(struct argnod **)argp;
			} else {
				*argtail=argp;
				argtail = &(argp->argnxt);
				keywd=flags&keyflg;
			; }
			word();
			if ( flag
			) { ((struct comnod *)t)->comio=
					inout(((struct comnod *)t)->comio);
			; }
		   ; }

		   ((struct comnod *)t)->comtyp=TCOM;
		   ((struct comnod *)t)->comset=(struct argnod *)argset;
		   *argtail=0;
		   return(t);
		}

	}
	reserv++; word();
	if ( io=inout(io)
	) {	t=makefork(0,t); t->treio=io;
	; }
	return(t);
}


static int	skipnl()
{
	while ( (reserv++, word()=='\n') ) { chkpr('\n') ; }
	return(wdval);
}

static struct ionod *	inout(lastio)
	struct ionod *		lastio;
{
	register int		iof;
	register struct ionod *	iop;
	register char	c;

	iof=wdnum;

	switch ( wdval ) {

	    case DOCSYM:
		iof |= IODOC; break;

	    case APPSYM:
	    case '>':
		if ( wdnum==0 ) { iof |= 1 ; }
		iof |= IOPUT;
		if ( wdval==APPSYM
		) {	iof |= IOAPP; break;
		; }

	    case '<':
		if ( (c=nextc(0))=='&'
		) {	iof |= IOMOV;
		} else if ( c=='>'
		) {	iof |= IORDW;
		} else {	peekc=c|MARK;
		; }
		break;

	    default:
		return(lastio);
	}

	chkword();
	iop=(struct ionod *)getstak(IOTYPE);
	iop->ioname=wdarg->argval;
	iop->iofile=iof;
	if ( iof&IODOC
	) { iop->iolst=iopend; iopend=iop;
	; }
	word(); iop->ionxt=inout(lastio);
	return(iop);
}

static int	chkword()
{
	if ( word()
	) {	synbad();
	; }
}

static int	chksym(sym)
{
	register int		x = sym&wdval;
	if ( ((x&SYMFLG) ? x : sym) != wdval
	) {	synbad();
	; }
}

static int	prsym(sym)
{
	if ( sym&SYMFLG
	) {	register struct sysnod *	sp=reserved;
		while ( sp->sysval
			&& sp->sysval!=sym
		) { sp++ ; }
		prs(sp->sysnam);
	} else if ( sym==EOFSYM
	) {	prs(endoffile);
	} else {	if ( sym&SYMREP ) { prc(sym) ; }
		if ( sym=='\n'
		) {	prs("newline");
		} else {	prc(sym);
		; }
	; }
}

static int	synbad()
{
	prp(); prs(synmsg);
	if ( (flags&ttyflg)==0
	) {	prs(atline); prn(standin->flin);
	; }
	prs(colon);
	prc('`');
	if ( wdval
	) {	prsym(wdval);
	} else {	prs(wdarg->argval);
	; }
	prc('\''); prs(unexpected);
	newline();
	exitsh(SYNBAD);
}
