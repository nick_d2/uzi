iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ args
@if errorlevel 1 goto failure
del args.r01
as-z80 -l -o args.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ blok
@if errorlevel 1 goto failure
del blok.r01
as-z80 -l -o blok.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ builtin
@if errorlevel 1 goto failure
del builtin.r01
as-z80 -l -o builtin.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ cmd
@if errorlevel 1 goto failure
del cmd.r01
as-z80 -l -o cmd.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ ctype
@if errorlevel 1 goto failure
del ctype.r01
as-z80 -l -o ctype.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ data
@if errorlevel 1 goto failure
del data.r01
as-z80 -l -o data.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ error
@if errorlevel 1 goto failure
del error.r01
as-z80 -l -o error.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ expand
@if errorlevel 1 goto failure
del expand.r01
as-z80 -l -o expand.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ fault
@if errorlevel 1 goto failure
del fault.r01
as-z80 -l -o fault.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ io
@if errorlevel 1 goto failure
del io.r01
as-z80 -l -o io.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ macro
@if errorlevel 1 goto failure
del macro.r01
as-z80 -l -o macro.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ main
@if errorlevel 1 goto failure
del main.r01
as-z80 -l -o main.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ msg
@if errorlevel 1 goto failure
del msg.r01
as-z80 -l -o msg.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ name
@if errorlevel 1 goto failure
del name.r01
as-z80 -l -o name.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ print
@if errorlevel 1 goto failure
del print.r01
as-z80 -l -o print.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ service
@if errorlevel 1 goto failure
del service.r01
as-z80 -l -o service.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ setbrk
@if errorlevel 1 goto failure
del setbrk.r01
as-z80 -l -o setbrk.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ stak
@if errorlevel 1 goto failure
del stak.r01
as-z80 -l -o stak.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ string
@if errorlevel 1 goto failure
del string.r01
as-z80 -l -o string.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ word
@if errorlevel 1 goto failure
del word.r01
as-z80 -l -o word.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ xec
@if errorlevel 1 goto failure
del xec.r01
as-z80 -l -o xec.s01
@if errorlevel 1 goto failure

link-z80 -f bsh
@if errorlevel 1 goto failure
ihex2bin -l bsh.i86 ..\..\..\bin\banked\bsh
@if errorlevel 1 goto failure

@echo SUCCESS
@goto done
:failure
@echo FAILURE
:done

