/* data.c by Nick for UNIX V7 Bourne shell */

#include	"defs.h"

/* temp files and io */
/* int		output; */	/* done in main.c and given value = 2 */
int		ioset;
struct ionod *	iotemp;		/* files to be deleted sometime */
struct ionod *	iopend;		/* documents waiting to be read at NL */

/* substitution */
int		dolc;
char *		*dolv;
struct dolnod *	argfor;
struct argnod *	gchain;

/* stak stuff */

/* A chain of ptrs of stack blocks that
 * have become covered by heap allocation.
 * `tdystak' will return them to the heap.
 */
struct blk *	stakbsy;

/* Base of the entire stack */
char *		stakbas;

/* Top of entire stack */
char *		brkend;

/* Base of current item */
/* char *		stakbot; */

/* Top of current item */
char *		staktop;

/* name tree and words */
/* struct sysnod	reserved[]; */
int		wdval;
int		wdnum;
struct argnod *	wdarg;
int		wdset; /* Nick */
int		dset;
char		reserv;

/* special names */
/* char		flagadr[]; */
char *		cmdadr;
char *		exitadr;
char *		dolladr;
char *		pcsadr;
char *		pidadr;

/* char		defpath[]; */

/* transput */
/* char		tmpout[]; */
char *		tmpnam;
int		serial;
/* struct fileblk *	standin; */
int		peekc;
char *		comdiv;
/* char		devnull[]; */

/* flags */
int		flags;

/* error exits from various parts of shell */
jmp_buf		subshell;
jmp_buf		errshell;

char		trapnote;
/* char *		trapcom[]; */
/* char		trapflg[]; */

/* execflgs */
int		exitval;
char		execbrk;
int		loopcnt;
int		breakcnt;

char		nosubst; /* Nick, set by trim() */

#if 0 /* Nick */
int		end;
#else
address		end[1];
#endif

