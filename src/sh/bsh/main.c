
/*
 * UNIX shell
 *
 * S. R. Bourne
 * Bell Telephone Laboratories
 *
 */

#include	"defs.h"
#include	"dup.h"
#include	"sym.h"
#include	"timeout.h"
#if 1 /* Nick */
#include	<types.h>
#else
#include	<sys/types.h>
#endif
#include	<sys/stat.h>
#include	<sgtty.h>

int		output = 2;
static char	beenhere = 0;
char		tmpout[20] = "/tmp/sh-";
struct fileblk		stdfile;
struct fileblk *		standin = &stdfile;
/* Nick #include	<execargs.h> */

extern int	exfile();

#if 0 /* Nick */
extern struct blk *bloktop; /*=BLK(end);*/	/*top of arena (last blok)*/
#endif


main(c, v)
	int		c;
	char *		v[];
{
	register int		rflag=ttyflg;

	/* initialise storage allocation */
	stdsigs();
#if 0 /* Nick */
	end = sbrk(0);
	bloktop = (struct blk *)end;
#endif
	setbrk(BRKINCR);
	addblok((unsigned)0);

	/* set names from userenv */
	getenv();

	/* look for restricted */
/*	IF c>0 ANDF any('r', *v) THEN rflag=0 FI */

	/* look for options */
	dolc=options(c,v);
	if ( dolc<2 ) { flags |= stdflg ; }
	if ( (flags&stdflg)==0
	) {	dolc--;
	; }
	dolv=v+c-dolc; dolc--;

	/* return here for shell file execution */
	setjmp(subshell);

	/* number of positional parameters */
	assnum(&dolladr,dolc);
	cmdadr=dolv[0];

	/* set pidname */
	assnum(&pidadr, getpid());

	/* set up temp file names */
	settmp();

	/* default ifs */
	dfault(&ifsnod, sptbnl);

	if ( (beenhere++)==0
	) {	/* ? profile */
		if ( *cmdadr=='-'
		    && (input=pathopen(nullstr, profile))>=0
		) {	exfile(rflag); flags &= ~ttyflg;
		; }
		if ( rflag==0 ) { flags |= rshflg ; }

		/* open input file if specified */
		if ( comdiv
		) {	estabf(comdiv); input = -1;
		} else {	input=((flags&stdflg) ? 0 : chkopen(cmdadr));
			comdiv--;
		; }
	} else {
#if 0 /* Nick... FIX THIS!! */
		*execargs=dolv;	/* for `ps' cmd */
#endif
	; }

	exfile(0);
	done();
}

static int	exfile(prof)
char		prof;
{
#if 1 /* Nick */
	time_t mailtime;
	time_t nulltime;
#else
	register long int	mailtime = 0;
#endif
	register int		userid;
	struct stat	statb;

#if 1 /* Nick */
	memset(&mailtime, 0, sizeof(time_t));
	memset(&nulltime, 0, sizeof(time_t));
#endif
	/* move input */
	if ( input>0
	) {	Ldup(input,INIO);
		input=INIO;
	; }

	/* move output to safe place */
	if ( output==2
	) {	Ldup(dup(2),OTIO);
		output=OTIO;
	; }

	userid=getuid();

	/* decide whether interactive */
	if ( (flags&intflg) || ((flags&oneflg)==0 &&
#if 1 /* Nick */
			isatty(output) && isatty(input))
#else
			gtty(output,&statb)==0 && gtty(input,&statb)==0)
#endif
	) {	dfault(&ps1nod, (userid?stdprompt:supprompt));
		dfault(&ps2nod, readmsg);
		flags |= ttyflg|prompt; ignsig(KILL);
	} else {	flags |= prof; flags &= ~prompt;
	; }

	if ( setjmp(errshell) && prof
	) {	close(input); return;
	; }

	/* error return here */
	loopcnt=breakcnt=peekc=0; iopend=0;
	if ( input>=0 ) { initf(input) ; }

	/* command loop */
	for (;;) {	tdystak(0);
		stakchk(); /* may reduce sbrk */
		exitset();
		if ( (flags&prompt) && standin->fstak==0 && !eof
		) {	if ( mailnod.namval
			    && stat(mailnod.namval,&statb)>=0 && statb.st_size
#if 1 /* Nick */
			    && memcmp(&statb.st_mtime, &mailtime, sizeof(time_t))
			    && memcmp(&statb.st_mtime, &nulltime, sizeof(time_t))
#else
			    && (statb.st_mtime != mailtime)
			    && mailtime
#endif
			) {	prs(mailmsg)
			; }
#if 1 /* Nick */
			memcpy(&mailtime, &statb.st_mtime, sizeof(time_t));
#else
			mailtime=statb.st_mtime;
#endif
			prs(ps1nod.namval); alarm(TIMEOUT); flags |= waiting;
		; }

		trapnote=0; peekc=readc();
		if ( eof
		) {	return;
		; }
		alarm(0); flags &= ~waiting;
		execute(cmd('\n',MTFLG),0);
		eof |= (flags&oneflg);
	}
}

chkpr(eor)
char eor;
{
	if ( (flags&prompt) && standin->fstak==0 && eor=='\n'
	) {	prs(ps2nod.namval);
	; }
}

settmp()
{
	itos(getpid()); serial=0;
	tmpnam=movstr(numbuf,&tmpout[TMPNAM]);
}

Ldup(fa, fb)
	register int		fa, fb;
{
#if 1 /* Nick */
	dup2(fa, fb);
#else
	dup(fa|DUPFLG, fb);
#endif
	close(fa);
#if 0 /* Nick... FIX THIS!! */
	ioctl(fb, FIOCLEX, 0);
#endif
}
