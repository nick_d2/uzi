
/*
 * UNIX shell
 *
 * S. R. Bourne
 * Bell Telephone Laboratories
 *
 */

#include	"defs.h"

extern char * *copyargs();
static struct dolnod *dolh;

char	flagadr[10];

char	flagchar[] = {
	'x',	'n',	'v',	't',	's',	'i',	'e',	'r',	'k',	'u',	0
};
int	flagval[]  = {
	execpr,	noexec,	readpr,	oneflg,	stdflg,	intflg,	errflg,	rshflg,	keyflg,	setflg,	0
};

/* ========	option handling	======== */


int	options(argc,argv)
	char *		*argv;
	int		argc;
{
	register char *	cp;
	register char *	*argp=argv;
	register char *	flagc;
	char *		flagp;

	if ( argc>1 && *argp[1]=='-'
	) {	cp=argp[1];
		flags &= ~(execpr|readpr);
		while ( *++cp
		) {	flagc=flagchar;

			while ( *flagc && *flagc != *cp ) { flagc++ ; }
			if ( *cp == *flagc
			) {	flags |= flagval[flagc-flagchar];
			} else if ( *cp=='c' && argc>2 && comdiv==0
			) {	comdiv=argp[2];
				argp[1]=argp[0]; argp++; argc--;
			} else {	failed(argv[1],badopt);
			; }
		; }
		argp[1]=argp[0]; argc--;
	; }

	/* set up $- */
	flagc=flagchar;
	flagp=flagadr;
	while ( *flagc
	) { if ( flags&flagval[flagc-flagchar]
	   ) { *flagp++ = *flagc;
	   ; }
	   flagc++;
	; }
	*flagp++=0;

	return(argc);
}

int	setargs(argi)
	char *		argi[];
{
	/* count args */
	register char *	*argp=argi;
	register int		argn=0;

	while ( Rcheat(*argp++)!=ENDARGS ) { argn++ ; }

	/* free old ones unless on for loop chain */
	freeargs(dolh);
	dolh=(struct dolnod *)copyargs(argi,argn);	/* sets dolv */
	assnum(&dolladr,dolc=argn-1);
}

int freeargs(blk)
	struct dolnod *		blk;
{
	register char *	*argp;
	register struct dolnod *	argr=0;
	register struct dolnod *	argblk;

	if ( argblk=blk
	) {	argr = argblk->dolnxt;
		if ( (--argblk->doluse)==0
		) {	for ( argp=(char **)argblk->dolarg; Rcheat(*argp)!=ENDARGS; argp++
			) { free(*argp) ; }
			free(argblk);
		; }
	; }
	return (int)argr;
}

static char * *	copyargs(from, n)
	char *		from[];
{
	register char * *	np=(char **)alloc(sizeof(char **)*n+3*BYTESPERWORD);
	register char * *	fp=from;
	register char * *	pp=np;

	((struct dolnod *)np)->doluse=1;	/* use count */
	np=(char **)((struct dolnod *)np)->dolarg;
	dolv=np;

	while ( n--
	) { *np++ = make(*fp++) ; }
	*np++ = ENDARGS;
	return(pp);
}

clearup()
{
	/* force `for' $* lists to go away */
	while ( argfor=(struct dolnod *)freeargs(argfor) );

	/* clean up io files */
	while ( pop() );
}

struct dolnod *	useargs()
{
	if ( dolh
	) {	dolh->doluse++;
		dolh->dolnxt=argfor;
		return(argfor=dolh);
	} else {	return(0);
	; }
}
