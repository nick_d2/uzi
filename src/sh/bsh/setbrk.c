

/*
 *	UNIX shell
 *
 *	S. R. Bourne
 *	Bell Telephone Laboratories
 *
 */

#include	"defs.h"

setbrk(incr)
{
#if 1 /* Nick, allows for signed increment */
	register char *a;

	if (brkend == NULL)
		{
		brkend = (char *)sbrk(0);
		}
	a = brkend;
	brkend += incr;
	brk(brkend);
#else
	register char *	a = (char *)sbrk(incr);
	brkend=a+incr;
#endif
	return (int)a;
}
