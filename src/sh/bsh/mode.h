
/*
 *	UNIX shell
 */


#define BYTESPERWORD	(sizeof(char *))

/* the following nonsense is required
 * because casts turn an Lvalue
 * into an Rvalue so two cheats
 * are necessary, one for each context.
 */
/* Nick union { int _cheat; } _cheatah; */
#define Lcheat(a)	(*(int *)&(a)) /* Nick (((_cheatah)a)._cheat) */
#define Rcheat(a)	((int)(a))


/* address puns for storage allocation */
typedef union {
	struct forknod *	_forkptr;
	struct comnod *	_comptr;
	struct parnod *	_parptr;
	struct ifnod *	_ifptr;
	struct whnod *	_whptr;
	struct fornod *	_forptr;
	struct lstnod *	_lstptr;
	struct blk *	_blkptr;
	struct namnod *	_namptr;
	char *	_bytptr;
	}	address;


/* for functions that do not return values */
/* Nick struct void {int vvvvvvvv;}; */


/* heap storage */
struct blk {
	struct blk *	word;
};

#define	BUFSIZ	64
struct fileblk {
	int	fdes;
	unsigned	flin;
	char	feof;
	char	fsiz;
	char *	fnxt;
	char *	fend;
	char *	*feval;
	struct fileblk *	fstak;
	char	fbuf[BUFSIZ];
};

/* for files not used with file descriptors */
struct filehdr {
	int	fdes;
	unsigned	flin;
	char	feof;
	char	fsiz;
	char *	fnxt;
	char *	fend;
	char *	*feval;
	struct fileblk *	fstak;
	char	_fbuf[1];
};

struct sysnod {
	char *	sysnam;
	int	sysval;
};

/* this node is a proforma for those that follow */
struct trenod {
	int	tretyp;
	struct ionod *	treio;
};

/* dummy for access only */
struct argnod {
	struct argnod *	argnxt;
	char	argval[1];
};

struct dolnod {
	struct dolnod *	dolnxt;
	int	doluse;
	char	dolarg[1];
};

struct forknod {
	int	forktyp;
	struct ionod *	forkio;
	struct trenod *	forktre;
};

struct comnod {
	int	comtyp;
	struct ionod *	comio;
	struct argnod *	comarg;
	struct argnod *	comset;
};

struct ifnod {
	int	iftyp;
	struct trenod *	iftre;
	struct trenod *	thtre;
	struct trenod *	eltre;
};

struct whnod {
	int	whtyp;
	struct trenod *	whtre;
	struct trenod *	dotre;
};

struct fornod {
	int	fortyp;
	struct trenod *	fortre;
	char *	fornam;
	struct comnod *	forlst;
};

struct swnod {
	int	swtyp;
	char *	swarg;
	struct regnod *	swlst;
};

struct regnod {
	struct argnod *	regptr;
	struct trenod *	regcom;
	struct regnod *	regnxt;
};

struct parnod {
	int	partyp;
	struct trenod *	partre;
};

struct lstnod {
	int	lsttyp;
	struct trenod *	lstlef;
	struct trenod *	lstrit;
};

struct ionod {
	int	iofile;
	char *	ioname;
	struct ionod *	ionxt;
	struct ionod *	iolst;
};

#define	FORKTYPE	(sizeof(struct forknod))
#define	COMTYPE		(sizeof(struct comnod))
#define	IFTYPE		(sizeof(struct ifnod))
#define	WHTYPE		(sizeof(struct whnod))
#define	FORTYPE		(sizeof(struct fornod))
#define	SWTYPE		(sizeof(struct swnod))
#define	REGTYPE		(sizeof(struct regnod))
#define	PARTYPE		(sizeof(struct parnod))
#define	LSTTYPE		(sizeof(struct lstnod))
#define	IOTYPE		(sizeof(struct ionod))
