

/*
 * UNIX shell
 *
 * S. R. Bourne
 * Bell Telephone Laboratories
 *
 */

#include	"defs.h"

char *		stakbot=nullstr;



/* ========	storage allocation	======== */

char *	getstak(asize)
	int		asize;
{	/* allocate requested stack */
	register char *	oldstak;
	register int		size;

	size=round(asize,BYTESPERWORD);
	oldstak=stakbot;
	staktop = stakbot += size;
	return(oldstak);
}

char *	locstak()
{	/* set up stack for local use
	 * should be followed by `endstak'
	 */
	if ( brkend-stakbot<BRKINCR
	) {	setbrk(brkincr);
		if ( brkincr < BRKMAX
		) {	brkincr += 256;
		; }
	; }
	return(stakbot);
}

char *	savstak()
{
	assert(staktop==stakbot);
	return(stakbot);
}

char *	endstak(argp)
	register char *	argp;
{	/* tidy up after `locstak' */
	register char *	oldstak;
	*argp++=0;
	oldstak=stakbot; stakbot=staktop=(char *)round(argp,BYTESPERWORD);
	return(oldstak);
}

int	tdystak(x)
	register char * 	x;
{
	/* try to bring stack back to x */
	while ( ADR(stakbsy)>ADR(x)
	) { free(stakbsy);
	   stakbsy = stakbsy->word;
	; }
	staktop=stakbot=max(ADR(x),ADR(stakbas));
	rmtemp(x);
}

stakchk()
{
	if ( (brkend-stakbas)>BRKINCR+BRKINCR
	) {	setbrk(-BRKINCR);
	; }
}

char *	cpystak(x)
	char *		x;
{
	return(endstak(movstr(x,locstak())));
}

