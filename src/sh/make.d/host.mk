# /uzi/src/sh/make.d/host.mk by Nick for Hytech NOS/UZI project

# Enter with TOPSRCDIR = relative path to current directory, which will be
# blank if the project is at the same level as this host.mk file, for example
# /uzi/src/nos/ppp has an "include ../make.d/host.mk" with TOPSRCDIR = blank

# -----------------------------------------------------------------------------

TOPSRCDIR:=	$(TOPSRCDIR)../
TOPSHDIR:=	$(TOPSRCDIR)

include $(TOPSHDIR)../make.d/host.mk

# -----------------------------------------------------------------------------

SHBINDIR:=	$(TOPSHDIR)bin/
SHINCDIR:=	$(TOPSHDIR)include/
SHLIBDIR:=	$(TOPSHDIR)lib/

INCDIRS:=	$(SHINCDIR) $(INCDIRS)
LIBDIRS:=	$(SHLIBDIR) $(LIBDIRS)

HDROUT=		$(SHINCDIR)
LIBOUT=		$(SHLIBDIR)

# -----------------------------------------------------------------------------

