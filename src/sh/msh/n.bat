iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ sh1
@if errorlevel 1 goto failure
del sh1.r01
as-z80 -l -o sh1.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ sh2
@if errorlevel 1 goto failure
del sh2.r01
as-z80 -l -o sh2.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ sh3
@if errorlevel 1 goto failure
del sh3.r01
as-z80 -l -o sh3.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ sh4
@if errorlevel 1 goto failure
del sh4.r01
as-z80 -l -o sh4.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ sh5
@if errorlevel 1 goto failure
del sh5.r01
as-z80 -l -o sh5.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ sh6
@if errorlevel 1 goto failure
del sh6.r01
as-z80 -l -o sh6.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ opendir
@if errorlevel 1 goto failure
del opendir.r01
as-z80 -l -o opendir.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ closedir
@if errorlevel 1 goto failure
del closedir.r01
as-z80 -l -o closedir.s01
@if errorlevel 1 goto failure

iccz80 -S -w -mb -v1 -z -A -I..\..\..\include\ readdir
@if errorlevel 1 goto failure
del readdir.r01
as-z80 -l -o readdir.s01
@if errorlevel 1 goto failure

link-z80 -f msh
@if errorlevel 1 goto failure
ihex2bin -l msh.i86 ..\..\..\bin\banked\msh
@if errorlevel 1 goto failure

copy sh.1 \uzi\bin\man

@echo SUCCESS
@goto done
:failure
@echo FAILURE
:done

