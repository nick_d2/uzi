#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h> /* Nick, for nick_opendir() prototype definition */

/* Need a way to have void used for ANSI, nothing for K&R. */
#ifndef _ANSI
#undef _VOID
#define _VOID
#endif

/* -------- sh.h -------- */
/*
 * shell
 */

#define	LINELIM	2100
#define	NPUSH	8	/* limit to input nesting */

#define	NOFILE	20	/* Number of open files */
#define	NUFILE	10	/* Number of user-accessible files */
#define	FDBASE	10	/* First file usable by Shell */

/*
 * values returned by wait
 */
#define	WAITSIG(s) ((s)&0177)
#define	WAITVAL(s) (((s)>>8)&0377)
#define	WAITCORE(s) (((s)&0200)!=0)

/*
 * library and system defintions
 */
#ifdef __STDC__
typedef void xint;	/* base type of jmp_buf, for not broken compilers */
#else
typedef char * xint;	/* base type of jmp_buf, for broken compilers */
#endif

/*
 * shell components
 */
/* #include "area.h" */
/* #include "word.h" */
/* #include "io.h" */
/* #include "var.h" */

#define	QUOTE	0200

#define	NOBLOCK	((struct op *)NULL)
#define	NOWORD	((char *)NULL)
#define	NOWORDS	((char **)NULL)
#define	NOPIPE	((int *)NULL)

/*
 * Description of a command or an operation on commands.
 * Might eventually use a union.
 */
struct op {
	int	type;	/* operation type, see below */
	char	**words;	/* arguments to a command */
	struct	ioword	**ioact;	/* IO actions (eg, < > >>) */
	struct op *left;
	struct op *right;
	char	*str;	/* identifier for case and for */
};

#define	TCOM	1	/* command */
#define	TPAREN	2	/* (c-list) */
#define	TPIPE	3	/* a | b */
#define	TLIST	4	/* a [&;] b */
#define	TOR	5	/* || */
#define	TAND	6	/* && */
#define	TFOR	7
#define	TDO	8
#define	TCASE	9
#define	TIF	10
#define	TWHILE	11
#define	TUNTIL	12
#define	TELIF	13
#define	TPAT	14	/* pattern in case */
#define	TBRACE	15	/* {c-list} */
#define	TASYNC	16	/* c & */

/*
 * actions determining the environment of a process
 */
#define	BIT(i)	(1<<(i))
#define	FEXEC	BIT(0)	/* execute without forking */

/*
 * flags to control evaluation of words
 */
#define	DOSUB	1	/* interpret $, `, and quotes */
#define	DOBLANK	2	/* perform blank interpretation */
#define	DOGLOB	4	/* interpret [?* */
#define	DOKEY	8	/* move words with `=' to 2nd arg. list */
#define	DOTRIM	16	/* trim resulting string */

#define	DOALL	(DOSUB|DOBLANK|DOGLOB|DOKEY|DOTRIM)

Extern	char	**dolv;
Extern	int	dolc;
Extern	int	exstat;
Extern  char	gflg;
Extern  int	talking;	/* interactive (talking-type wireless) */
Extern  int	execflg;
Extern  int	multiline;	/* \n changed to ; */
Extern  struct	op	*outtree;	/* result from parser */

Extern	xint	*failpt;
Extern	xint	*errpt;

struct	brkcon {
	jmp_buf	brkpt;
	struct	brkcon	*nextlev;
} ;
Extern	struct brkcon	*brklist;
Extern	int	isbreak;

/*
 * redirection
 */
struct ioword {
	short	io_unit;	/* unit affected */
	short	io_flag;	/* action (below) */
	char	*io_name;	/* file name */
};
#define	IOREAD	1	/* < */
#define	IOHERE	2	/* << (here file) */
#define	IOWRITE	4	/* > */
#define	IOCAT	8	/* >> */
#define	IOXHERE	16	/* ${}, ` in << */
#define	IODUP	32	/* >&digit */
#define	IOCLOSE	64	/* >&- */

#define	IODEFAULT (-1)	/* token for default IO unit */

Extern	struct	wdblock	*wdlist;
Extern	struct	wdblock	*iolist;

/*
 * parsing & execution environment
 */
#if 1 /* Nick */
extern struct s_env
	{
	char	*linep;
	struct	io	*iobase;
	struct	io	*iop;
	xint	*errpt;
	int	iofd;
	struct	s_env	*oenv;
	} env;
#else
extern struct	env {
	char	*linep;
	struct	io	*iobase;
	struct	io	*iop;
	xint	*errpt;
	int	iofd;
	struct	env	*oenv;
} e;
#endif

/*
 * flags:
 * -e: quit on error
 * -k: look for name=value everywhere on command line
 * -n: no execution
 * -t: exit after reading and executing one command
 * -v: echo as read
 * -x: trace
 * -u: unset variables net diagnostic
 */
extern	char	*flag;

extern	char	*null;	/* null value for variable */
extern	int	intr;	/* interrupt pending */

Extern	char	*trap[_NSIG+1];
Extern	char	ourtrap[_NSIG+1];
Extern	int	trapset;	/* trap pending */

extern	int	heedint;	/* heed interrupt signals */

Extern	int	yynerrs;	/* yacc */

Extern	char	line[LINELIM];
extern	char	*elinep;

/*
 * other functions
 */
#ifdef __STDC__
int (*inbuilt(char *s ))(void);
#else
int (*inbuilt())();
#endif

char *rexecve __P((char *c , char **v , char **envp ));
char *space __P((int n ));
char *strsave __P((char *s , int a ));
char *evalstr __P((char *cp , int f ));
char *putn __P((int n ));
char *sh_itoa __P((unsigned u , int n ));
char *unquote __P((char *as ));
struct var *lookup __P((char *n ));
int rlookup __P((char *n ));
struct wdblock *glob __P((char *cp , struct wdblock *wb ));
int subgetc __P((char ec , int quoted )); /* Nick, formerly int ec */
char **makenv __P((void));
char **eval __P((char **ap , int f ));
int setstatus __P((int s ));
int waitfor __P((int lastpid , int canintr ));

void onintr __P((int s )); /* SIGINT handler */

int newenv __P((int f ));
void quitenv __P((void));
void err __P((char *s ));
int anys __P((char *s1 , char *s2 ));
int any __P((int c , char *s ));
void next __P((int f ));
void setdash __P((void));
void onecommand __P((void));
void runtrap __P((int i ));
void xfree __P((char *s ));
int letter __P((int c ));
int digit __P((int c ));
int letnum __P((int c ));
int gmatch __P((char *s , char *p ));

/*
 * error handling
 */
void leave __P((void)); /* abort shell (or fail in subshell) */
void fail __P((void));	 /* fail but return to process next command */
void warn __P((char *s ));
void sig __P((int i ));	 /* default signal handler */

/* -------- var.h -------- */

struct	var {
	char	*value;
	char	*name;
	struct	var	*next;
	char	status;
};
#define	COPYV	1	/* flag to setval, suggesting copy */
#define	RONLY	01	/* variable is read-only */
#define	EXPORT	02	/* variable is to be exported */
#define	GETCELL	04	/* name & value space was got with getcell */

Extern	struct	var	*vlist;		/* dictionary */

Extern	struct	var	*homedir;	/* home directory */
Extern	struct	var	*prompt;	/* main prompt */
Extern	struct	var	*cprompt;	/* continuation prompt */
Extern	struct	var	*path;		/* search path for commands */
Extern	struct	var	*shell;		/* shell to interpret command files */
Extern	struct	var	*ifs;		/* field separators */

int yyparse __P((void));
struct var *lookup __P((char *n ));
void setval __P((struct var *vp , char *val ));
void nameval __P((struct var *vp , char *val , char *name ));
void export __P((struct var *vp ));
void ronly __P((struct var *vp ));
int isassign __P((char *s ));
int checkname __P((char *cp ));
int assign __P((char *s , int cf ));
void putvlist __P((int f , int out ));
int eqname __P((char *n1 , char *n2 ));

int execute __P((struct op *t , int *pin , int *pout , int act ));

/* -------- io.h -------- */
/* io buffer */
struct iobuf {
  unsigned id;				/* buffer id */
  char buf[512];			/* buffer */
  char *bufp;				/* pointer into buffer */
  char *ebufp;				/* pointer to end of buffer */
};

/* possible arguments to an IO function */
struct ioarg {
	char	*aword;
	char	**awordlist;
	int	afile;		/* file descriptor */
	unsigned afid;		/* buffer id */
	long	afpos;		/* file position */
	struct iobuf *afbuf;	/* buffer for this file */
};
Extern struct ioarg ioargstack[NPUSH];
#define AFID_NOBUF	(~0)
#define AFID_ID		0

/* an input generator's state */
struct	io {
	int	(*iofn)(_VOID);
	struct	ioarg	*argp;
	int	peekc;
	char	prev;		/* previous character read by readc() */
	char	nlcount;	/* for `'s */
	char	xchar;		/* for `'s */
	char	task;		/* reason for pushed IO */
};
Extern	struct	io	iostack[NPUSH];
#define	XOTHER	0	/* none of the below */
#define	XDOLL	1	/* expanding ${} */
#define	XGRAVE	2	/* expanding `'s */
#define	XIO	3	/* file IO */

/* in substitution */
#define	INSUB()	(env.iop->task == XGRAVE || env.iop->task == XDOLL)

/*
 * input generators for IO structure
 */
int nlchar __P((struct ioarg *ap ));
int strchar __P((struct ioarg *ap ));
int qstrchar __P((struct ioarg *ap ));
int filechar __P((struct ioarg *ap ));
int herechar __P((struct ioarg *ap ));
int linechar __P((struct ioarg *ap ));
int gravechar __P((struct ioarg *ap , struct io *iop ));
int qgravechar __P((struct ioarg *ap , struct io *iop ));
int dolchar __P((struct ioarg *ap ));
int wdchar __P((struct ioarg *ap ));
void scraphere __P((void));
void freehere __P((int area ));
void gethere __P((void));
void markhere __P((char *s , struct ioword *iop ));
int herein __P((char *hname , int xdoll ));
int run __P((struct ioarg *argp , int (*f)(_VOID)));

/*
 * IO functions
 */
int eofc __P((void));
int getc __P((int ec ));
int readc __P((void));
void unget __P((int c ));
void ioecho __P((char c )); /* Nick, formerly int c */
void prs __P((char *s ));
#if 1 /* Nick */
void prs_expand __P((int fd, unsigned char *p, unsigned char *nonl ));
#endif
void sh_putc __P((char c )); /* Nick, formerly int c */
void prn __P((unsigned u ));
void closef __P((int i ));
void closeall __P((void));

/*
 * IO control
 */
void pushio __P((struct ioarg *argp , int (*fn)(_VOID)));
int remap __P((int fd ));
int openpipe __P((int *pv ));
void closepipe __P((int *pv ));
struct io *setbase __P((struct io *ip ));

extern	struct	ioarg	temparg;	/* temporary for PUSHIO */
#define	PUSHIO(what,arg,gen) ((temparg.what = (arg)),pushio(&temparg,(gen)))
#define	RUN(what,arg,gen) ((temparg.what = (arg)), run(&temparg,(gen)))

/* -------- word.h -------- */
#ifndef WORD_H
#define	WORD_H	1
struct	wdblock {
	short	w_bsize;
	short	w_nword;
	/* bounds are arbitrary */
	char	*w_words[1];
};

struct wdblock *addword __P((char *wd , struct wdblock *wb ));
struct wdblock *newword __P((int nw ));
char **getwords __P((struct wdblock *wb ));
#endif

/* -------- area.h -------- */

/*
 * storage allocation
 */
char *getcell __P((unsigned nbytes ));
void garbage __P((void));
void setarea __P((char *cp , int a ));
int getarea __P((char *cp ));
void freearea __P((int a ));
void freecell __P((char *cp ));

Extern	int	areanum;	/* current allocation area */

#define	NEW(type) (type *)getcell(sizeof(type))
#define	DELETE(obj)	freecell((char *)obj)

/* modifications by Nick for compatibility with uzi libc library */

DIR *nick_opendir(char *path, DIR *dir, struct dirent *dd_buf);
int nick_closedir(DIR *dir);
struct dirent *nick_readdir(DIR *dir);

/* end of modifications by Nick */

void abyte(int value); /* see crt/diag.S */
void ahexw(int value); /* see crt/diag.S */

