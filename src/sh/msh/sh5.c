#define Extern extern
#include <sys/types.h>
#include <signal.h>
#define _NSIG NSIGS /* Nick NSIG */
#include <errno.h>
#include <setjmp.h>
#include <utsname.h> /* Nick see prs_expand() routine */
#include "sh.h"

/* Nick moved this "here" from below as part of the modulization */
struct	here {
	char	*h_tag;
	int	h_dosub;
	struct	ioword *h_iop;
	struct	here	*h_next;
};

#ifdef MODULE
#define STATIC
extern struct iobuf sharedbuf;
extern struct iobuf mainbuf;
extern unsigned bufid;
extern struct ioarg temparg;
extern struct here *inhere;		/* list of hear docs while parsing */
extern struct here *acthere;		/* list of active here documents */
#else
#define STATIC static
#define MODULE_sharedbuf
#define MODULE_mainbuf
#define MODULE_bufid
#define MODULE_temparg
#define MODULE_getc
#define MODULE_unget
#define MODULE_eofc
#define MODULE_readc
#define MODULE_ioecho
#define MODULE_pushio
#define MODULE_setbase
#define MODULE_nlchar
#define MODULE_wdchar
#define MODULE_dolchar
#define MODULE_xxchar
#define MODULE_strchar
#define MODULE_qstrchar
#define MODULE_filechar
#define MODULE_herechar
#define MODULE_gravechar
#define MODULE_qgravechar
#define MODULE_linechar
#define MODULE_prs
#define MODULE_prs_expand
#define MODULE_putc
#define MODULE_xprn
#define MODULE_closef
#define MODULE_closeall
#define MODULE_remap
#define MODULE_openpipe
#define MODULE_closepipe
#define MODULE_inhere
#define MODULE_acthere
#define MODULE_markhere
#define MODULE_gethere
#define MODULE_readhere
#define MODULE_herein
#define MODULE_scraphere
#define MODULE_freehere
#define MODULE_tempname
#endif

/* -------- io.c -------- */
/* #include "sh.h" */

/*
 * shell IO
 */

#ifdef MODULE_sharedbuf
STATIC struct iobuf sharedbuf = {AFID_NOBUF};
#endif
#ifdef MODULE_mainbuf
STATIC struct iobuf mainbuf = {AFID_NOBUF};
#endif
#ifdef MODULE_bufid
STATIC unsigned bufid = AFID_ID;	/* buffer id counter */
#endif

#ifdef MODULE_temparg
struct ioarg temparg = {0, 0, 0, AFID_NOBUF, 0};
#endif

STATIC void readhere __P((char **name, char *s, int ec ));
void pushio __P((struct ioarg *argp, int (*fn)()));
STATIC int xxchar __P((struct ioarg *ap ));
void tempname __P((char *tname ));


#ifdef MODULE_getc
int
getc(ec)
register int ec;
{
	register int c;

	if(env.linep > elinep) {
		while((c=readc()) != '\n' && c)
			;
		err("input line too long");
		gflg++;
		return(c);
	}
	c = readc();
 	if (ec != '\'' && env.iop->task != XGRAVE) {
		if(c == '\\') {
			c = readc();
			if (c == '\n' && ec != '\"')
				return(getc(ec));
			c |= QUOTE;
		}
	}
	return(c);
}
#endif


#ifdef MODULE_unget
void
unget(c)
int c;
{
	if (env.iop >= env.iobase)
		env.iop->peekc = c;
}
#endif


#ifdef MODULE_eofc
int
eofc()

{
  return env.iop < env.iobase || (env.iop->peekc == 0 && env.iop->prev == 0);
}
#endif


#ifdef MODULE_readc
int
readc()
{
	register c;

	for (; env.iop >= env.iobase; env.iop--)
		if ((c = env.iop->peekc) != '\0') {
			env.iop->peekc = 0;
			return(c);
		}
		else {
		    if (env.iop->prev != 0) {
		        if ((c = (*env.iop->iofn)(env.iop->argp, env.iop)) != '\0') {
			        if (c == -1) {
				        env.iop++;
				        continue;
			        }
			        if (env.iop == iostack)
				        ioecho(c);
			        return(env.iop->prev = c);
		        }
		        else if (env.iop->task == XIO && env.iop->prev != '\n') {
			        env.iop->prev = 0;
				if (env.iop == iostack)
					ioecho('\n');
			        return '\n';
		        }
		    }
		    if (env.iop->task == XIO) {
			if (multiline)
			    return env.iop->prev = 0;
			if (talking && env.iop == iostack+1)
#if 1 /* Nick */
			    prs_expand(2, prompt->value, NULL);
#else
			    prs(prompt->value);
#endif
		    }
		}
	if (env.iop >= iostack)
		return(0);
	leave();
	/* NOTREACHED */
}
#endif


#ifdef MODULE_ioecho
void
ioecho(c)
char c;
{
	if (flag['v'])
		write(2, &c, sizeof c);
}
#endif


#ifdef MODULE_pushio
void
pushio(argp, fn)
struct ioarg *argp;
int (*fn)();
{
	if (++env.iop >= &iostack[NPUSH]) {
		env.iop--;
		err("Shell input nested too deeply");
		gflg++;
		return;
	}
	env.iop->iofn = fn;

	if (argp->afid != AFID_NOBUF)
	  env.iop->argp = argp;
	else {
	  env.iop->argp  = ioargstack + (env.iop - iostack);
	  *env.iop->argp = *argp;
	  env.iop->argp->afbuf = env.iop == &iostack[0] ? &mainbuf : &sharedbuf;
	  if (isatty(env.iop->argp->afile) == 0 &&
	      (env.iop == &iostack[0] ||
	       lseek(env.iop->argp->afile, 0L, 1) != -1)) {
	    if (++bufid == AFID_NOBUF)
	      bufid = AFID_ID;
	    env.iop->argp->afid  = bufid;
	  }
	}

	env.iop->prev  = ~'\n';
	env.iop->peekc = 0;
	env.iop->xchar = 0;
	env.iop->nlcount = 0;
	if (fn == filechar || fn == linechar)
		env.iop->task = XIO;
	else if (fn == gravechar || fn == qgravechar)
		env.iop->task = XGRAVE;
	else
		env.iop->task = XOTHER;
}
#endif


#ifdef MODULE_setbase
struct io *
setbase(ip)
struct io *ip;
{
	register struct io *xp;

	xp = env.iobase;
	env.iobase = ip;
	return(xp);
}
#endif

/*
 * Input generating functions
 */

/*
 * Produce the characters of a string, then a newline, then EOF.
 */

#ifdef MODULE_nlchar
int
nlchar(ap)
register struct ioarg *ap;
{
	register int c;

	if (ap->aword == NULL)
		return(0);
	if ((c = *ap->aword++) == 0) {
		ap->aword = NULL;
		return('\n');
	}
	return(c);
}
#endif

/*
 * Given a list of words, produce the characters
 * in them, with a space after each word.
 */

#ifdef MODULE_wdchar
int
wdchar(ap)
register struct ioarg *ap;
{
	register char c;
	register char **wl;

	if ((wl = ap->awordlist) == NULL)
		return(0);
	if (*wl != NULL) {
		if ((c = *(*wl)++) != 0)
			return(c & 0177);
		ap->awordlist++;
		return(' ');
	}
	ap->awordlist = NULL;
	return('\n');
}
#endif

/*
 * Return the characters of a list of words,
 * producing a space between them.
 */

#ifdef MODULE_dolchar
int
dolchar(ap)
register struct ioarg *ap;
{
	register char *wp;

	if ((wp = *ap->awordlist++) != NULL) {
		PUSHIO(aword, wp, *ap->awordlist == NULL? strchar: xxchar);
		return(-1);
	}
	return(0);
}
#endif


#ifdef MODULE_xxchar
STATIC int
xxchar(ap)
register struct ioarg *ap;
{
	register int c;

	if (ap->aword == NULL)
		return(0);
	if ((c = *ap->aword++) == '\0') {
		ap->aword = NULL;
		return(' ');
	}
	return(c);
}
#endif

/*
 * Produce the characters from a single word (string).
 */

#ifdef MODULE_strchar
int
strchar(ap)
register struct ioarg *ap;
{
	register int c;

	if (ap->aword == NULL || (c = *ap->aword++) == 0)
		return(0);
	return(c);
}
#endif

/*
 * Produce quoted characters from a single word (string).
 */

#ifdef MODULE_qstrchar
int
qstrchar(ap)
register struct ioarg *ap;
{
	register int c;

	if (ap->aword == NULL || (c = *ap->aword++) == 0)
		return(0);
	return(c|QUOTE);
}
#endif

/*
 * Return the characters from a file.
 */

#ifdef MODULE_filechar
int
filechar(ap)
register struct ioarg *ap;
{
	register int i;
	char c;
	struct iobuf *bp = ap->afbuf;

	if (ap->afid != AFID_NOBUF) {
	  if ((i = ap->afid != bp->id) || bp->bufp == bp->ebufp) {
	    if (i)
	      lseek(ap->afile, ap->afpos, 0);
	    do {
	      i = read(ap->afile, bp->buf, sizeof(bp->buf));
	    } while (i < 0 && errno == EINTR);
	    if (i <= 0) {
	      closef(ap->afile);
	      return 0;
	    }
	    bp->id = ap->afid;
	    bp->ebufp = (bp->bufp  = bp->buf) + i;
	  }
	  ap->afpos++;
	  return *bp->bufp++ & 0177;
	}

	do {
		i = read(ap->afile, &c, sizeof(c));
	} while (i < 0 && errno == EINTR);
	return(i == sizeof(c)? c&0177: (closef(ap->afile), 0));
}
#endif

/*
 * Return the characters from a here temp file.
 */

#ifdef MODULE_herechar
int
herechar(ap)
register struct ioarg *ap;
{
	char c;


	if (read(ap->afile, &c, sizeof(c)) != sizeof(c)) {
		close(ap->afile);
		c = 0;
	}
	return (c);

}
#endif

/*
 * Return the characters produced by a process (`...`).
 * Quote them if required, and remove any trailing newline characters.
 */

#ifdef MODULE_gravechar
int
gravechar(ap, iop)
struct ioarg *ap;
struct io *iop;
{
	register int c;

	if ((c = qgravechar(ap, iop)&~QUOTE) == '\n')
		c = ' ';
	return(c);
}
#endif


#ifdef MODULE_qgravechar
int
qgravechar(ap, iop)
register struct ioarg *ap;
struct io *iop;
{
	register int c;

	if (iop->xchar) {
		if (iop->nlcount) {
			iop->nlcount--;
			return('\n'|QUOTE);
		}
		c = iop->xchar;
		iop->xchar = 0;
	} else if ((c = filechar(ap)) == '\n') {
		iop->nlcount = 1;
		while ((c = filechar(ap)) == '\n')
			iop->nlcount++;
		iop->xchar = c;
		if (c == 0)
			return(c);
		iop->nlcount--;
		c = '\n';
	}
	return(c!=0? c|QUOTE: 0);
}
#endif

/*
 * Return a single command (usually the first line) from a file.
 */

#ifdef MODULE_linechar
int
linechar(ap)
register struct ioarg *ap;
{
	register int c;

	if ((c = filechar(ap)) == '\n') {
		if (!multiline) {
			closef(ap->afile);
			ap->afile = -1;	/* illegal value */
		}
	}
	return(c);
}
#endif


#ifdef MODULE_prs
void
prs(s)
register char *s;
{
	if (*s)
		write(2, s, strlen(s));
}
#endif

#ifdef MODULE_prs_expand
void
prs_expand(int fd, unsigned char *p, unsigned char *nonl)
	{
	char *q, *r, i, n, ch;
	struct utsname name;
	char buf[PATHLEN];

	for (q = p; *q; q++)
		{
		if (*q == '\\')
			{
			write(fd, p, q - p);
			p = ++q;
			switch (*q)
				{
			case 'c':
				p++;
				if (nonl)
					{
					*nonl = 1;
					}
				break;
			case 'a':
				p++;
				write(fd, "\x07", 1);
			 	break;
			case 'b':
				p++;
				write(fd, "\b", 1);
				break;
			case 'f':
				p++;
				write(fd, "\f", 1);
				break;
			case 'n':
				p++;
				write(fd, "\n", 1);
				break;
			case 'r':
				p++;
				write(fd, "\r", 1);
				break;
			case 't':
				p++;
				write(fd, "\t", 1);
				break;
			case 'v':
				p++;
				write(fd, "\v", 1);
				break;
			case 'x':
				p++;
				n = 0;
				for (i = 0; i < 2; i++, p++, q++)
					{
					ch = *p - '0';
					if (ch > 'a'-'0')
						{
						ch = *p - 'a' + 10;
						}
					else if (ch > 'A'-'0')
						{
						ch = *p - 'A' + 10;
						}
					if (/*ch < 0 ||*/ ch > 0xf)
						{
						break;
						}
					n = (n << 4) + ch;
					}
				write(fd, &n, 1);
				break;
			case '0': case '1': case '2': case '3':
			case '4': case '5': case '6': case '7':
				n = *p++ - '0';
				for (i = 0; i < 2; i++, p++, q++)
					{
					ch = *p - '0';
					if (/*ch < 0 ||*/ ch > 7)
						{
						break;
						}
					n = (n << 3) + ch;
					}
				write(fd, &n, 1);
				break;
			case '#':
				p++;
				if (geteuid() == 0)
					{
					write(fd, "#", 1);
					}
				else
					{
					write(fd, "$", 1);
					}
				break;
			case 'u':
				p++;
				if ((r = getenv("USER")) == NULL)
					{
					write(fd, "someone", 7);
					}
				else
					{
					write(fd, r, strlen(r));
					}
				break;
			case 'h':
				p++;
				if (uname(&name) == -1)
					{
					write(fd, "somehost", 8);
					}
				else
					{
					write(fd, name.nodename,
							strlen(name.nodename));
					}
				break;
			case 'w':
				p++;
				if (getcwd(buf, PATHLEN) == NULL)
					{
					write(fd, "somewhere", 9);
					}
				else
					{
					write(fd, buf, strlen(buf));
					}
				break;
				}
			}
		}
	write(fd, p, q - p);
	}
#endif


#ifdef MODULE_putc
void
putc(c)
char c;
{
	write(2, &c, sizeof c);
}
#endif


#ifdef MODULE_xprn
void
prn(u)
unsigned u;
{
	prs(sh_itoa(u, 0));
}
#endif


#ifdef MODULE_closef
void
closef(i)
register int i;
{
	if (i > 2)
		close(i);
}
#endif


#ifdef MODULE_closeall
void
closeall()
{
	register u;

	for (u=NUFILE; u<NOFILE;)
		close(u++);
}
#endif

/*
 * remap fd into Shell's fd space
 */

#ifdef MODULE_remap
int
remap(fd)
register int fd;
{
	register int i;
	int map[NOFILE];

	if (fd < env.iofd) {
		for (i=0; i<NOFILE; i++)
			map[i] = 0;
		do {
			map[fd] = 1;
			fd = dup(fd);
		} while (fd >= 0 && fd < env.iofd);
		for (i=0; i<NOFILE; i++)
			if (map[i])
				close(i);
		if (fd < 0)
			err("too many files open in shell");
	}
	return(fd);
}
#endif


#ifdef MODULE_openpipe
int
openpipe(pv)
register int *pv;
{
	register int i;

	if ((i = pipe(pv)) < 0)
		err("can't create pipe - try again");
	return(i);
}
#endif


#ifdef MODULE_closepipe
void
closepipe(pv)
register int *pv;
{
	if (pv != NULL) {
		close(*pv++);
		close(*pv);
	}
}
#endif

/* -------- here.c -------- */
/* #include "sh.h" */

/*
 * here documents
 */

#ifdef MODULE_inhere
STATIC	struct here *inhere;		/* list of here docs while parsing */
#endif
#ifdef MODULE_acthere
STATIC	struct here *acthere;		/* list of active here documents */
#endif


#ifdef MODULE_markhere
void
markhere(s, iop)
register char *s;
struct ioword *iop;
{
	register struct here *h, *lh;

	h = (struct here *) space(sizeof(struct here));
	if (h == 0)
		return;
	h->h_tag = evalstr(s, DOSUB);
	if (h->h_tag == 0)
		return;
	h->h_iop = iop;
	iop->io_name = 0;
	h->h_next = NULL;
	if (inhere == 0)
		inhere = h;
	else
		for (lh = inhere; lh!=NULL; lh = lh->h_next)
			if (lh->h_next == 0) {
				lh->h_next = h;
				break;
			}
	iop->io_flag |= IOHERE|IOXHERE;
	for (s = h->h_tag; *s; s++)
		if (*s & QUOTE) {
			iop->io_flag &= ~ IOXHERE;
			*s &= ~ QUOTE;
		}
	h->h_dosub = iop->io_flag & IOXHERE;
}
#endif


#ifdef MODULE_gethere
void
gethere()
{
	register struct here *h, *hp;

	/* Scan here files first leaving inhere list in place */
	for (hp = h = inhere; h != NULL; hp = h, h = h->h_next)
	  readhere(&h->h_iop->io_name, h->h_tag, h->h_dosub? 0: '\'');

	/* Make inhere list active - keep list intact for scraphere */
	if (hp != NULL) {
	  hp->h_next = acthere;
	  acthere    = inhere;
	  inhere     = NULL;
	}
}
#endif


#ifdef MODULE_readhere
STATIC void
readhere(name, s, ec)
char **name;
register char *s;
int ec;
{
	int tf;
	char tname[30];
	register c;
	jmp_buf ev;
	char line [LINELIM+1];
	char *next;

	tempname(tname);
	*name = strsave(tname, areanum);
	tf = creat(tname, 0600);
	if (tf < 0)
		return;
	if (newenv(setjmp(errpt = ev)) != 0)
		unlink(tname);
	else {
		pushio(env.iop->argp, env.iop->iofn);
		env.iobase = env.iop;
		for (;;) {
			if (talking && env.iop <= iostack)
#if 1 /* Nick */
				prs_expand(2, cprompt->value, NULL);
#else
				prs(cprompt->value);
#endif
			next = line;
			while ((c = getc(ec)) != '\n' && c) {
				if (ec == '\'')
					c &= ~ QUOTE;
				if (next >= &line[LINELIM]) {
					c = 0;
					break;
				}
				*next++ = c;
			}
			*next = 0;
			if (strcmp(s, line) == 0 || c == 0)
				break;
			*next++ = '\n';
			write (tf, line, (int)(next-line));
		}
		if (c == 0) {
			prs("here document `"); prs(s); err("' unclosed");
		}
		quitenv();
	}
	close(tf);
}
#endif

/*
 * open here temp file.
 * if unquoted here, expand here temp file into second temp file.
 */

#ifdef MODULE_herein
int
herein(hname, xdoll)
char *hname;
int xdoll;
{
	register hf, tf;

	if (hname == 0)
		return(-1);
	hf = open(hname, 0);
	if (hf < 0)
		return (-1);
	if (xdoll) {
		char c;
		char tname[30];
		jmp_buf ev;

		tempname(tname);
		if ((tf = creat(tname, 0600)) < 0)
			return (-1);
		if (newenv(setjmp(errpt = ev)) == 0) {
			PUSHIO(afile, hf, herechar);
			setbase(env.iop);
			while ((c = subgetc(0, 0)) != 0) {
				c &= ~ QUOTE;
				write(tf, &c, sizeof c);
			}
			quitenv();
		} else
			unlink(tname);
		close(tf);
		tf = open(tname, 0);
		unlink(tname);
		return (tf);
	} else
		return (hf);
}
#endif


#ifdef MODULE_scraphere
void
scraphere()
{
	register struct here *h;

	for (h = inhere; h != NULL; h = h->h_next) {
		if (h->h_iop && h->h_iop->io_name)
		  unlink(h->h_iop->io_name);
	}
	inhere = NULL;
}
#endif

/* unlink here temp files before a freearea(area) */

#ifdef MODULE_freehere
void
freehere(area)
int area;
{
	register struct here *h, *hl;

	hl = NULL;
	for (h = acthere; h != NULL; h = h->h_next)
		if (getarea((char *) h) >= area) {
			if (h->h_iop->io_name != NULL)
				unlink(h->h_iop->io_name);
			if (hl == NULL)
				acthere = h->h_next;
			else
				hl->h_next = h->h_next;
		} else
			hl = h;
}
#endif


#ifdef MODULE_tempname
void
tempname(tname)
char *tname;
{
	static int inc;
	register char *cp, *lp;

	for (cp = tname, lp = "/tmp/shtm"; (*cp = *lp++) != '\0'; cp++)
		;
	lp = putn(getpid()*1000 + inc++);
	for (; (*cp = *lp++) != '\0'; cp++)
		;
}
#endif

