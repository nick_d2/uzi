#define Extern extern
#include <sys/types.h>
#include <signal.h>
#define _NSIG NSIGS /* Nick NSIG */
#include <errno.h>
#include <setjmp.h>
#include "sh.h"

#ifdef MODULE
#define STATIC
extern int intr;
extern int inparse;
extern char flags['z'-'a'+1];
extern char *flag;
extern char *elinep;
extern char *null;
extern int heedint;
#if 1 /* Nick */
extern struct s_env env;
#else
extern struct env e;
#endif
extern char **environ; /* environment pointer */
extern char shellname[];
extern char search[];
extern void (*qflag) __P((int));
extern struct region *areabot;		/* bottom of area */
extern struct region *areatop;		/* top of area */
extern struct region *areanxt;		/* starting point of scan */
#else
#define STATIC static
#define MODULE_intr
#define MODULE_inparse
#define MODULE_flags
#define MODULE_flag
#define MODULE_elinep
#define MODULE_null
#define MODULE_heedint
#define MODULE_env
#define MODULE_shellname
#define MODULE_search
#define MODULE_qflag
#define MODULE_main
#define MODULE_setdash
#define MODULE_newfile
#define MODULE_onecommand
#define MODULE_fail
#define MODULE_leave
#define MODULE_warn
#define MODULE_err
#define MODULE_newenv
#define MODULE_quitenv
#define MODULE_anys
#define MODULE_any
#define MODULE_putn
#define MODULE_sh_itoa
#define MODULE_next
#define MODULE_onintr
#define MODULE_letter
#define MODULE_digit
#define MODULE_letnum
#define MODULE_space
#define MODULE_strsave
#define MODULE_xfree
#define MODULE_sig
#define MODULE_runtrap
#define MODULE_lookup
#define MODULE_setval
#define MODULE_nameval
#define MODULE_export
#define MODULE_ronly
#define MODULE_isassign
#define MODULE_assign
#define MODULE_checkname
#define MODULE_putvlist
#define MODULE_eqname
#define MODULE_findeq
#define MODULE_gmatch
#define MODULE_cclass
#define MODULE_areabot
#define MODULE_areatop
#define MODULE_areanxt
#define MODULE_initarea
#define MODULE_getcell
#define MODULE_freecell
#define MODULE_freearea
#define MODULE_setarea
#define MODULE_getarea
#define MODULE_garbage
#endif

/* -------- sh.c -------- */
/*
 * shell
 */

/* #include "sh.h" */

#ifdef MODULE_intr
int	intr;
#endif
#ifdef MODULE_inparse
int	inparse;
#endif
#ifdef MODULE_flags
char	flags['z'-'a'+1];
#endif
#ifdef MODULE_flag
char	*flag = flags-'a';
#endif
#ifdef MODULE_elinep
char	*elinep = line+sizeof(line)-5;
#endif
#ifdef MODULE_null
char	*null	= "";
#endif
#ifdef MODULE_heedint
int	heedint =1;
#endif
#if 1 /* Nick */
#ifdef MODULE_env
struct	s_env	env = { line, iostack, iostack-1,
			(xint *)NULL, FDBASE, (struct s_env *)NULL };
#endif
#else
struct	env	e = { line, iostack, iostack-1,
		      (xint *)NULL, FDBASE, (struct env *)NULL };
#endif

extern	char	**environ;	/* environment pointer */

/*
 * default shell, search rules
 */
#ifdef MODULE_shellname
char	shellname[] = "/bin/sh";
#endif
#ifdef MODULE_search
char	search[] = ":/bin:/usr/bin";
#endif

#ifdef MODULE_qflag
void (*qflag) __P((int)) = (int (*)(int))SIG_IGN; /* Nick cast */
#endif

int main __P((int argc, char **argv ));
int newfile __P((char *s ));
STATIC char *findeq __P((char *cp ));
STATIC char *cclass __P((char *p, int sub ));
void initarea __P((void));

#ifdef MODULE_main
int
main(argc, argv)
int argc;
register char **argv;
{
	register int f;
	register char *s;
	int cflag;
	char *name, **ap;
	int (*iof)();

/* abyte('!'); */
	initarea();
	if ((ap = environ) != NULL) {
		while (*ap)
			assign(*ap++, !COPYV);
		for (ap = environ; *ap;)
			export(lookup(*ap++));
	}
	closeall();
	areanum = 1;

	shell = lookup("SHELL");
	if (shell->value == null)
		setval(shell, shellname);
	export(shell);

	homedir = lookup("HOME");
	if (homedir->value == null)
		setval(homedir, "/");
	export(homedir);

	setval(lookup("$"), sh_itoa(getpid(), 5));

	path = lookup("PATH");
	if (path->value == null)
		setval(path, search);
	export(path);

	ifs = lookup("IFS");
	if (ifs->value == null)
		setval(ifs, " \t\n");

	prompt = lookup("PS1");
	if (prompt->value == null)
#ifndef UNIXSHELL
		setval(prompt, "$ ");
#else
		setval(prompt, "% ");
#endif

/* abyte('@'); */
	if (geteuid() == 0) {
		setval(prompt, "# ");
		prompt->status &= ~EXPORT;
	}
/* abyte('#'); */
	cprompt = lookup("PS2");
	if (cprompt->value == null)
		setval(cprompt, "> ");

	iof = filechar;
	cflag = 0;
	name = *argv++;
	if (--argc >= 1) {
		if(argv[0][0] == '-' && argv[0][1] != '\0') {
			for (s = argv[0]+1; *s; s++)
				switch (*s) {
				case 'c':
					prompt->status &= ~EXPORT;
					cprompt->status &= ~EXPORT;
					setval(prompt, "");
					setval(cprompt, "");
					cflag = 1;
					if (--argc > 0)
						PUSHIO(aword, *++argv, iof = nlchar);
					break;
	
				case 'q':
					qflag = SIG_DFL;
					break;

				case 's':
					/* standard input */
					break;

				case 't':
					prompt->status &= ~EXPORT;
					setval(prompt, "");
					iof = linechar;
					break;
	
				case 'i':
					talking++;
				default:
					if (*s>='a' && *s<='z')
						flag[*s]++;
				}
		} else {
			argv--;
			argc++;
		}
		if (iof == filechar && --argc > 0) {
			setval(prompt, "");
			setval(cprompt, "");
			prompt->status &= ~EXPORT;
			cprompt->status &= ~EXPORT;
			if (newfile(name = *++argv))
				exit(1);
		}
	}
	setdash();
	if (env.iop < iostack) {
		PUSHIO(afile, 0, iof);
		if (isatty(0) && isatty(1) && !cflag)
			talking++;
	}
	signal(SIGQUIT, (sig_t)qflag); /* Nick cast */
	if (name && name[0] == '-') {
		talking++;
		if ((f = open(".profile", 0)) >= 0)
			next(remap(f));
		if ((f = open("/etc/profile", 0)) >= 0)
			next(remap(f));
	}
	if (talking)
		signal(SIGTERM, (sig_t)sig); /* Nick cast */
	if (signal(SIGINT, SIG_IGN) != SIG_IGN)
		signal(SIGINT, (sig_t)onintr); /* Nick cast */
	dolv = argv;
	dolc = argc;
	dolv[0] = name;
	if (dolc > 1)
		for (ap = ++argv; --argc > 0;)
			if (assign(*ap = *argv++, !COPYV))
				dolc--;	/* keyword */
			else
				ap++;
	setval(lookup("#"), putn((--dolc < 0) ? (dolc = 0) : dolc));

	for (;;) {
		if (talking && env.iop <= iostack)
#if 1 /* Nick */
			prs_expand(2, prompt->value, NULL);
#else
			prs(prompt->value);
#endif
		onecommand();
	}
}
#endif


#ifdef MODULE_setdash
void
setdash()
{
	register char *cp, c;
	char m['z'-'a'+1];

	cp = m;
	for (c='a'; c<='z'; c++)
		if (flag[c])
			*cp++ = c;
	*cp = 0;
	setval(lookup("-"), m);
}
#endif


#ifdef MODULE_newfile
int
newfile(s)
register char *s;
{
	register f;

	if (strcmp(s, "-") != 0) {
		f = open(s, 0);
		if (f < 0) {
			prs(s);
			err(": cannot open");
			return(1);
		}
	} else
		f = 0;
	next(remap(f));
	return(0);
}
#endif


#ifdef MODULE_onecommand
void
onecommand()
{
	register i;
	jmp_buf m1;

	while (env.oenv)
		quitenv();
	areanum = 1;
	freehere(areanum);
	freearea(areanum);
	garbage();
	wdlist = 0;
	iolist = 0;
	env.errpt = 0;
	env.linep = line;
	yynerrs = 0;
	multiline = 0;
	inparse = 1;
	intr = 0;
	execflg = 0;
	setjmp(failpt = m1);	/* Bruce Evans' fix */
	if (setjmp(failpt = m1) || yyparse() || intr) {
		while (env.oenv)
			quitenv();
		scraphere();
		if (!talking && intr)
			leave();
		inparse = 0;
		intr = 0;
		return;
	}
	inparse = 0;
	brklist = 0;
	intr = 0;
	execflg = 0;
	if (!flag['n'])
		execute(outtree, NOPIPE, NOPIPE, 0);
	if (!talking && intr) {
		execflg = 0;
		leave();
	}
	if ((i = trapset) != 0) {
		trapset = 0;
		runtrap(i);
	}
}
#endif


#ifdef MODULE_fail
void
fail()
{
	longjmp(failpt, 1);
	/* NOTREACHED */
}
#endif


#ifdef MODULE_leave
void
leave()
{
	if (execflg)
		fail();
	scraphere();
	freehere(1);
	runtrap(0);
	exit(exstat);
	/* NOTREACHED */
}
#endif


#ifdef MODULE_warn
void
warn(s)
register char *s;
{
	if(*s) {
		prs(s);
		exstat = -1;
	}
	prs("\n");
	if (flag['e'])
		leave();
}
#endif


#ifdef MODULE_err
void
err(s)
char *s;
{
	warn(s);
	if (flag['n'])
		return;
	if (!talking)
		leave();
	if (env.errpt)
		longjmp(env.errpt, 1);
	closeall();
	env.iop = env.iobase = iostack;
}
#endif


#ifdef MODULE_newenv
int
newenv(f)
int f;
{
#if 1 /* Nick */
	register struct s_env *ep;
#else
	register struct env *ep;
#endif

	if (f) {
		quitenv();
		return(1);
	}
#if 1 /* Nick */
	ep = (struct s_env *) space(sizeof(*ep));
#else
	ep = (struct env *) space(sizeof(*ep));
#endif
	if (ep == NULL) {
		while (env.oenv)
			quitenv();
		fail();
	}
	*ep = env;
	env.oenv = ep;
	env.errpt = errpt;
	return(0);
}
#endif


#ifdef MODULE_quitenv
void
quitenv()
{
#if 1 /* Nick */
	register struct s_env *ep;
#else
	register struct env *ep;
#endif
	register fd;

	if ((ep = env.oenv) != NULL) {
		fd = env.iofd;
		env = *ep;
		/* should close `'d files */
		DELETE(ep);
		while (--fd >= env.iofd)
			close(fd);
	}
}
#endif


#ifdef MODULE_anys
/*
 * Is any character from s1 in s2?
 */
int
anys(s1, s2)
register char *s1, *s2;
{
	while (*s1)
		if (any(*s1++, s2))
			return(1);
	return(0);
}
#endif


#ifdef MODULE_any
/*
 * Is character c in s?
 */
int
any(c, s)
register int c;
register char *s;
{
	while (*s)
		if (*s++ == c)
			return(1);
	return(0);
}
#endif


#ifdef MODULE_putn
char *
putn(n)
register int n;
{
	return(sh_itoa(n, -1));
}
#endif


#ifdef MODULE_sh_itoa
char *
sh_itoa(u, n)
register unsigned u;
int n;
{
	register char *cp;
	static char s[20];
	int m;

	m = 0;
	if (n < 0 && (int) u < 0) {
		m++;
		u = -u;
	}
	cp = s+sizeof(s);
	*--cp = 0;
	do {
		*--cp = u%10 + '0';
		u /= 10;
	} while (--n > 0 || u);
	if (m)
		*--cp = '-';
	return(cp);
}
#endif


#ifdef MODULE_next
void
next(f)
int f;
{
	PUSHIO(afile, f, filechar);
}
#endif


#ifdef MODULE_onintr
void
onintr(s)
int s;				/* ANSI C requires a parameter */
{
	signal(SIGINT, (sig_t)onintr); /* Nick cast */
	intr = 1;
	if (talking) {
		if (inparse) {
			prs("\n");
			fail();
		}
	}
	else if (heedint) {
		execflg = 0;
		leave();
	}
}
#endif


#ifdef MODULE_letter
int
letter(c)
register c;
{
	return((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_');
}
#endif


#ifdef MODULE_digit
int
digit(c)
register c;
{
	return(c >= '0' && c <= '9');
}
#endif


#ifdef MODULE_letnum
int
letnum(c)
register c;
{
	return(letter(c) || digit(c));
}
#endif


#ifdef MODULE_space
char *
space(n)
int n;
{
	register char *cp;

	if ((cp = getcell(n)) == 0)
		err("out of string space");
	return(cp);
}
#endif


#ifdef MODULE_strsave
char *
strsave(s, a)
register char *s;
int a;
{
	register char *cp, *xp;

	if ((cp = space(strlen(s)+1)) != NULL) {
		setarea((char *)cp, a);
		for (xp = cp; (*xp++ = *s++) != '\0';)
			;
		return(cp);
	}
	return("");
}
#endif


#ifdef MODULE_xfree
void
xfree(s)
register char *s;
{
	DELETE(s);
}
#endif

/*
 * trap handling
 */

#ifdef MODULE_sig
void
sig(i)
register int i;
{
	trapset = i;
	signal(i, (sig_t)sig); /* Nick cast */
}
#endif


#ifdef MODULE_runtrap
void
runtrap(i)
int i;
{
	char *trapstr;

	if ((trapstr = trap[i]) == NULL)
		return;
	if (i == 0)
		trap[i] = 0;
	RUN(aword, trapstr, nlchar);
}
#endif

/* -------- var.c -------- */
/* #include "sh.h" */

/*
 * Find the given name in the dictionary
 * and return its value.  If the name was
 * not previously there, enter it now and
 * return a null value.
 */

#ifdef MODULE_lookup
struct var *
lookup(n)
register char *n;
{
	register struct var *vp;
	register char *cp;
	register int c;
	static struct var dummy;

	if (digit(*n)) {
		dummy.name = n;
		for (c = 0; digit(*n) && c < 1000; n++)
			c = c*10 + *n-'0';
		dummy.status = RONLY;
		dummy.value = c <= dolc? dolv[c]: null;
		return(&dummy);
	}
	for (vp = vlist; vp; vp = vp->next)
		if (eqname(vp->name, n))
			return(vp);
	cp = findeq(n);
	vp = (struct var *)space(sizeof(*vp));
	if (vp == 0 || (vp->name = space((int)(cp-n)+2)) == 0) {
		dummy.name = dummy.value = "";
		return(&dummy);
	}
	for (cp = vp->name; (*cp = *n++) && *cp != '='; cp++)
		;
	if (*cp == 0)
		*cp = '=';
	*++cp = 0;
	setarea((char *)vp, 0);
	setarea((char *)vp->name, 0);
	vp->value = null;
	vp->next = vlist;
	vp->status = GETCELL;
	vlist = vp;
	return(vp);
}
#endif

/*
 * give variable at `vp' the value `val'.
 */

#ifdef MODULE_setval
void
setval(vp, val)
struct var *vp;
char *val;
{
	nameval(vp, val, (char *)NULL);
}
#endif

/*
 * if name is not NULL, it must be
 * a prefix of the space `val',
 * and end with `='.
 * this is all so that exporting
 * values is reasonably painless.
 */

#ifdef MODULE_nameval
void
nameval(vp, val, name)
register struct var *vp;
char *val, *name;
{
	register char *cp, *xp;
	char *nv;
	int fl;

	if (vp->status & RONLY) {
		for (xp = vp->name; *xp && *xp != '=';)
			putc(*xp++);
		err(" is read-only");
		return;
	}
	fl = 0;
	if (name == NULL) {
		xp = space(strlen(vp->name)+strlen(val)+2);
		if (xp == 0)
			return;
		/* make string:  name=value */
		setarea((char *)xp, 0);
		name = xp;
		for (cp = vp->name; (*xp = *cp++) && *xp!='='; xp++)
			;
		if (*xp++ == 0)
			xp[-1] = '=';
		nv = xp;
		for (cp = val; (*xp++ = *cp++) != '\0';)
			;
		val = nv;
		fl = GETCELL;
	}
	if (vp->status & GETCELL)
		xfree(vp->name);	/* form new string `name=value' */
	vp->name = name;
	vp->value = val;
	vp->status |= fl;
}
#endif


#ifdef MODULE_export
void
export(vp)
struct var *vp;
{
	vp->status |= EXPORT;
}
#endif


#ifdef MODULE_ronly
void
ronly(vp)
struct var *vp;
{
	if (letter(vp->name[0]))	/* not an internal symbol ($# etc) */
		vp->status |= RONLY;
}
#endif


#ifdef MODULE_isassign
int
isassign(s)
register char *s;
{
	if (!letter((int)*s))
		return(0);
	for (; *s != '='; s++)
		if (*s == 0 || !letnum(*s))
			return(0);
	return(1);
}
#endif


#ifdef MODULE_assign
int
assign(s, cf)
register char *s;
int cf;
{
	register char *cp;
	struct var *vp;

	if (!letter(*s))
		return(0);
	for (cp = s; *cp != '='; cp++)
		if (*cp == 0 || !letnum(*cp))
			return(0);
	vp = lookup(s);
	nameval(vp, ++cp, cf == COPYV? (char *)NULL: s);
	if (cf != COPYV)
		vp->status &= ~GETCELL;
	return(1);
}
#endif


#ifdef MODULE_checkname
int
checkname(cp)
register char *cp;
{
	if (!letter(*cp++))
		return(0);
	while (*cp)
		if (!letnum(*cp++))
			return(0);
	return(1);
}
#endif


#ifdef MODULE_putvlist
void
putvlist(f, out)
register int f, out;
{
	register struct var *vp;

	for (vp = vlist; vp; vp = vp->next)
		if (vp->status & f && letter(*vp->name)) {
			if (vp->status & EXPORT)
				write(out, "export ", 7);
			if (vp->status & RONLY)
				write(out, "readonly ", 9);
			write(out, vp->name, (int)(findeq(vp->name) - vp->name));
			write(out, "\n", 1);
		}
}
#endif


#ifdef MODULE_eqname
int
eqname(n1, n2)
register char *n1, *n2;
{
	for (; *n1 != '=' && *n1 != 0; n1++)
		if (*n2++ != *n1)
			return(0);
	return(*n2 == 0 || *n2 == '=');
}
#endif


#ifdef MODULE_findeq
STATIC char *
findeq(cp)
register char *cp;
{
	while (*cp != '\0' && *cp != '=')
		cp++;
	return(cp);
}
#endif

/* -------- gmatch.c -------- */
/*
 * int gmatch(string, pattern)
 * char *string, *pattern;
 *
 * Match a pattern as in sh(1).
 */

#define	CMASK	0377
#define	QUOTE	0200
#define	QMASK	(CMASK&~QUOTE)
#define	NOT	'!'	/* might use ^ */


#ifdef MODULE_gmatch
int
gmatch(s, p)
register char *s, *p;
{
	register int sc, pc;

	if (s == NULL || p == NULL)
		return(0);
	while ((pc = *p++ & CMASK) != '\0') {
		sc = *s++ & QMASK;
		switch (pc) {
		case '[':
			if ((p = cclass(p, sc)) == NULL)
				return(0);
			break;

		case '?':
			if (sc == 0)
				return(0);
			break;

		case '*':
			s--;
			do {
				if (*p == '\0' || gmatch(s, p))
					return(1);
			} while (*s++ != '\0');
			return(0);

		default:
			if (sc != (pc&~QUOTE))
				return(0);
		}
	}
	return(*s == 0);
}
#endif


#ifdef MODULE_cclass
STATIC char *
cclass(p, sub)
register char *p;
register int sub;
{
	register int c, d, not, found;

	if ((not = *p == not) != 0)
		p++;
	found = not;
	do {
		if (*p == '\0')
			return((char *)NULL);
		c = *p & CMASK;
		if (p[1] == '-' && p[2] != ']') {
			d = p[2] & CMASK;
			p++;
		} else
			d = c;
		if (c == sub || (c <= sub && sub <= d))
			found = !not;
	} while (*++p != ']');
	return(found? p+1: (char *)NULL);
}
#endif

/* -------- area.c -------- */
#define	REGSIZE		sizeof(struct region)
#define GROWBY		256
#undef	SHRINKBY	/* 64 */
#define FREE 32767
#define BUSY 0
#define	ALIGN (sizeof(int)-1)

/* #include "area.h" */

struct region {
	struct	region *next;
	int	area;
};

/*
 * All memory between (char *)areabot and (char *)(areatop+1) is
 * exclusively administered by the area management routines.
 * It is assumed that sbrk() and brk() manipulate the high end.
 */
#ifdef MODULE_areabot
STATIC struct region *areabot;		/* bottom of area */
#endif
#ifdef MODULE_areatop
STATIC struct region *areatop;		/* top of area */
#endif
#ifdef MODULE_areanxt
STATIC struct region *areanxt;		/* starting point of scan */
#endif


#ifdef MODULE_initarea
void
initarea()
{
	while ((int)sbrk(0) & ALIGN)
		sbrk(1);
	areabot = (struct region *)sbrk(REGSIZE);
	areabot->next = areabot;
	areabot->area = BUSY;
	areatop = areabot;
	areanxt = areabot;
}
#endif


#ifdef MODULE_getcell
char *
getcell(nbytes)
unsigned nbytes;
{
	register int nregio;
	register struct region *p, *q;
	register i;

	if (nbytes == 0)
 {
#if 1 /* Nick */
 prs("fatal: getcell() nbytes == 0\n");
 exit(1);
#else
		abort();	/* silly and defeats the algorithm */
#endif
 }
	/*
	 * round upwards and add administration area
	 */
	nregio = (nbytes+(REGSIZE-1))/REGSIZE + 1;
	for (p = areanxt;;) {
		if (p->area > areanum) {
			/*
			 * merge free cells
			 */
			while ((q = p->next)->area > areanum && q != areanxt)
				p->next = q->next;
			/*
			 * exit loop if cell big enough
			 */
			if (q >= p + nregio)
				goto found;
		}
		p = p->next;
		if (p == areanxt)
			break;
	}
	i = nregio >= GROWBY ? nregio : GROWBY;
	p = (struct region *)sbrk(i * REGSIZE);
	if (p == (struct region *)-1)
		return((char *)NULL);
	p--;
	if (p != areatop)
 {
#if 1 /* Nick */
 prs("fatal: getcell() p != areatop (");
 prn((int)p);
 prs(", ");
 prn((int)areatop);
 prs(")\n");
 exit(1);
#else
		abort();	/* allocated areas are contiguous */
#endif
 }
	q = p + i;
	p->next = q;
	p->area = FREE;
	q->next = areabot;
	q->area = BUSY;
	areatop = q;
found:
	/*
	 * we found a FREE area big enough, pointed to by 'p', and up to 'q'
	 */
	areanxt = p + nregio;
	if (areanxt < q) {
		/*
		 * split into requested area and rest
		 */
		if (areanxt+1 > q)
 {
#if 1 /* Nick */
 prs("fatal: getcell() areanxt+1 > q (");
 prn((int)areanxt+1);
 prs(", ");
 prn((int)q);
 prs(")\n");
 exit(1);
#else
			abort();	/* insufficient space left for admin */
#endif
 }
		areanxt->next = q;
		areanxt->area = FREE;
		p->next = areanxt;
	}
	p->area = areanum;
	return((char *)(p+1));
}
#endif


#ifdef MODULE_freecell
void
freecell(cp)
char *cp;
{
	register struct region *p;

	if ((p = (struct region *)cp) != NULL) {
		p--;
		if (p < areanxt)
			areanxt = p;
		p->area = FREE;
	}
}
#endif


#ifdef MODULE_freearea
void
freearea(a)
register int a;
{
	register struct region *p, *top;

	top = areatop;
	for (p = areabot; p != top; p = p->next)
		if (p->area >= a)
			p->area = FREE;
}
#endif


#ifdef MODULE_setarea
void
setarea(cp,a)
char *cp;
int a;
{
	register struct region *p;

	if ((p = (struct region *)cp) != NULL)
		(p-1)->area = a;
}
#endif


#ifdef MODULE_getarea
int
getarea(cp)
char *cp;
{
	return ((struct region*)cp-1)->area;
}
#endif


#ifdef MODULE_garbage
void
garbage()
{
	register struct region *p, *q, *top;

	top = areatop;
	for (p = areabot; p != top; p = p->next) {
		if (p->area > areanum) {
			while ((q = p->next)->area > areanum)
				p->next = q->next;
			areanxt = p;
		}
	}
#ifdef SHRINKBY
	if (areatop >= q + SHRINKBY && q->area > areanum) {
		brk((char *)(q+1));
		q->next = areabot;
		q->area = BUSY;
		areatop = q;
	}
#endif
}
#endif

