#define Extern extern
#include <sys/types.h>
#include <signal.h>
#define _NSIG NSIGS /* Nick NSIG */
#include <errno.h>
#include <setjmp.h>
#include <stddef.h>
#include <time.h>
/* Nick #include <sys/times.h> */
#include <sys/stat.h>
#include <sys/wait.h>
/* Nick #undef NULL */
#include "sh.h"

#ifdef MODULE
#define STATIC
#else
#define STATIC static
#define MODULE_execute
#define MODULE_forkexec
#define MODULE_parent
#define MODULE_iosetup
#define MODULE_echo
#define MODULE_find1case
#define MODULE_findcase
#define MODULE_brkset
#define MODULE_waitfor
#define MODULE_setstatus
#define MODULE_rexecve
#define MODULE_run
#define MODULE_dolabel
#define MODULE_dochdir
#define MODULE_doshift
#define MODULE_dologin
#define MODULE_doumask
#define MODULE_doexec
#define MODULE_dodot
#define MODULE_dowait
#define MODULE_doread
#define MODULE_doeval
#define MODULE_dotrap
#define MODULE_getsig
#define MODULE_getn
#define MODULE_dobreak
#define MODULE_docontinue
#define MODULE_brkcontin
#define MODULE_doexit
#define MODULE_doexport
#define MODULE_doreadonly
#define MODULE_rdexp
#define MODULE_badid
#define MODULE_doset
#define MODULE_varput
#define MODULE_dotimes
#define MODULE_inbuilt
#define MODULE_doecho
#define MODULE_dosync
#define MODULE_dosysdebug
#define MODULE_dosystrace
#endif

/* -------- exec.c -------- */
/* #include "sh.h" */

/*
 * execute tree
 */

#ifdef MODULE_waitfor
static	char	*signame[] = {
	"Signal 0",
	"Hangup",
	(char *)NULL,	/* interrupt */
	"Quit",
	"Illegal instruction",
	"Trace/BPT trap",
	"Abort",
	"EMT trap",
	"Floating exception",
	"Killed",
	"Bus error",
	"Memory fault",
	"Bad system call",
	(char *)NULL,	/* broken pipe */
	"Alarm clock",
	"Terminated",
};
#define	NSIGNAL (sizeof(signame)/sizeof(signame[0]))
#endif


STATIC int forkexec __P((struct op *t, int *pin, int *pout, int act, char **wp, int *pforked ));
STATIC int parent __P((void));
int iosetup __P((struct ioword *iop, int pipein, int pipeout ));
STATIC void echo __P((char **wp ));
STATIC struct op **find1case __P((struct op *t, char *w ));
STATIC struct op *findcase __P((struct op *t, char *w ));
STATIC void brkset __P((struct brkcon *bc ));
int dolabel __P((void));
int dochdir __P((struct op *t ));
int doshift __P((struct op *t ));
int dologin __P((struct op *t ));
int doumask __P((struct op *t ));
int doexec __P((struct op *t ));
int dodot __P((struct op *t ));
int dowait __P((struct op *t ));
int doread __P((struct op *t ));
int doeval __P((struct op *t ));
int dotrap __P((struct op *t ));
int getsig __P((char *s ));
#if 1 /* Nick */
void setsig __P((signal_t n, sig_t f));
#else
void setsig __P((int n, void (*f)()));
#endif
int getn __P((char *as ));
int dobreak __P((struct op *t ));
int docontinue __P((struct op *t ));
STATIC int brkcontin __P((char *cp, int val ));
int doexit __P((struct op *t ));
int doexport __P((struct op *t ));
int doreadonly __P((struct op *t ));
STATIC void rdexp __P((char **wp, void (*f)(), int key));
STATIC void badid __P((char *s ));
int doset __P((struct op *t ));
void varput __P((char *s, int out ));
int dotimes __P((void));
int doecho __P((struct op *t )); /* Nick */
int dosync __P((void)); /* Nick */
int dosysdebug __P((struct op *t )); /* Nick */
int dosystrace __P((struct op *t )); /* Nick */


#ifdef MODULE_execute
int
execute(t, pin, pout, act)
register struct op *t;
int *pin, *pout;
int act;
{
	register struct op *t1;
	int i, pv[2], rv, child, a;
	char *cp, **wp, **wp2;
	struct var *vp;
	struct brkcon bc;

	if (t == NULL)
		return(0);
	rv = 0;
	a = areanum++;
	wp = (wp2 = t->words) != NULL
	     ? eval(wp2, t->type == TCOM ? DOALL : DOALL & ~DOKEY)
	     : NULL;

	switch(t->type) {
	case TPAREN:
	case TCOM:
		rv = forkexec(t, pin, pout, act, wp, &child);
		if (child) {
			exstat = rv;
			leave();
		}
		break;

	case TPIPE:
		if ((rv = openpipe(pv)) < 0)
			break;
		pv[0] = remap(pv[0]);
		pv[1] = remap(pv[1]);
		(void) execute(t->left, pin, pv, 0);
		rv = execute(t->right, pv, pout, 0);
		break;

	case TLIST:
		(void) execute(t->left, pin, pout, 0);
		rv = execute(t->right, pin, pout, 0);
		break;

	case TASYNC:
		i = parent();
		if (i != 0) {
			if (i != -1) {
				setval(lookup("!"), putn(i));
				if (pin != NULL)
					closepipe(pin);
				if (talking) {
					prs(putn(i));
					prs("\n");
				}
			} else
				rv = -1;
			setstatus(rv);
		} else {
			signal(SIGINT, SIG_IGN);
			signal(SIGQUIT, SIG_IGN);
			if (talking)
				signal(SIGTERM, SIG_DFL);
			talking = 0;
			if (pin == NULL) {
				close(0);
				open("/dev/null", 0);
			}
			exit(execute(t->left, pin, pout, FEXEC));
		}
		break;

	case TOR:
	case TAND:
		rv = execute(t->left, pin, pout, 0);
		if ((t1 = t->right)!=NULL && (rv == 0) == (t->type == TAND))
			rv = execute(t1, pin, pout, 0);
		break;

	case TFOR:
		if (wp == NULL) {
			wp = dolv+1;
			if ((i = dolc) < 0)
				i = 0;
		} else {
			i = -1;
			while (*wp++ != NULL)
				;			
		}
		vp = lookup(t->str);
		while (setjmp(bc.brkpt))
			if (isbreak)
				goto broken;
		brkset(&bc);
		for (t1 = t->left; i-- && *wp != NULL;) {
			setval(vp, *wp++);
			rv = execute(t1, pin, pout, 0);
		}
		brklist = brklist->nextlev;
		break;

	case TWHILE:
	case TUNTIL:
		while (setjmp(bc.brkpt))
			if (isbreak)
				goto broken;
		brkset(&bc);
		t1 = t->left;
		while ((execute(t1, pin, pout, 0) == 0) == (t->type == TWHILE))
			rv = execute(t->right, pin, pout, 0);
		brklist = brklist->nextlev;
		break;

	case TIF:
	case TELIF:
	 	if (t->right != NULL) {
		rv = !execute(t->left, pin, pout, 0) ?
			execute(t->right->left, pin, pout, 0):
			execute(t->right->right, pin, pout, 0);
		}
		break;

	case TCASE:
		if ((cp = evalstr(t->str, DOSUB|DOTRIM)) == 0)
			cp = "";
		if ((t1 = findcase(t->left, cp)) != NULL)
			rv = execute(t1, pin, pout, 0);
		break;

	case TBRACE:
/*
		if (iopp = t->ioact)
			while (*iopp)
				if (iosetup(*iopp++, pin!=NULL, pout!=NULL)) {
					rv = -1;
					break;
				}
*/
		if (rv >= 0 && (t1 = t->left))
			rv = execute(t1, pin, pout, 0);
		break;
	}

broken:
	t->words = wp2;
	isbreak = 0;
	freehere(areanum);
	freearea(areanum);
	areanum = a;
	if (talking && intr) {
		closeall();
		fail();
	}
	if ((i = trapset) != 0) {
		trapset = 0;
		runtrap(i);
	}
	return(rv);
}
#endif


#ifdef MODULE_forkexec
STATIC int
forkexec(t, pin, pout, act, wp, pforked)
register struct op *t;
int *pin, *pout;
int act;
char **wp;
int *pforked;
{
	int i, rv, (*shcom)();
	register int f;
	char *cp;
	struct ioword **iopp;
	int resetsig;
	char **owp;

	owp = wp;
	resetsig = 0;
	*pforked = 0;
	shcom = NULL;
	rv = -1;	/* system-detected error */
	if (t->type == TCOM) {
		while ((cp = *wp++) != NULL)
			;
		cp = *wp;

		/* strip all initial assignments */
		/* not correct wrt PATH=yyy command  etc */
		if (flag['x'])
			echo (cp ? wp: owp);
		if (cp == NULL && t->ioact == NULL) {
			while ((cp = *owp++) != NULL && assign(cp, COPYV))
				;
			return(setstatus(0));
		}
		else if (cp != NULL)
			shcom = inbuilt(cp);
	}
	t->words = wp;
	f = act;
	if (shcom == NULL && (f & FEXEC) == 0) {
		i = parent();
		if (i != 0) {
			if (i == -1)
				return(rv);
			if (pin != NULL)
				closepipe(pin);
			return(pout==NULL? setstatus(waitfor(i,0)): 0);
		}
		if (talking) {
			signal(SIGINT, SIG_IGN);
			signal(SIGQUIT, SIG_IGN);
			resetsig = 1;
		}
		talking = 0;
		intr = 0;
		(*pforked)++;
		brklist = 0;
		execflg = 0;
	}
	if (owp != NULL)
		while ((cp = *owp++) != NULL && assign(cp, COPYV))
			if (shcom == NULL)
				export(lookup(cp));
#ifdef COMPIPE
	if ((pin != NULL || pout != NULL) && shcom != NULL && shcom != doexec) {
		err("piping to/from shell builtins not yet done");
		return(-1);
	}
#endif
	if (pin != NULL) {
		dup2(pin[0], 0);
		closepipe(pin);
	}
	if (pout != NULL) {
		dup2(pout[1], 1);
		closepipe(pout);
	}
	if ((iopp = t->ioact) != NULL) {
		if (shcom != NULL && shcom != doexec) {
			prs(cp);
			err(": cannot redirect shell command");
			return(-1);
		}
		while (*iopp)
			if (iosetup(*iopp++, pin!=NULL, pout!=NULL))
				return(rv);
	}
	if (shcom)
		return(setstatus((*shcom)(t)));
	/* should use FIOCEXCL */
	for (i=FDBASE; i<NOFILE; i++)
		close(i);
	if (resetsig) {
		signal(SIGINT, SIG_DFL);
		signal(SIGQUIT, SIG_DFL);
	}
	if (t->type == TPAREN)
		exit(execute(t->left, NOPIPE, NOPIPE, FEXEC));
	if (wp[0] == NULL)
		exit(0);
	cp = rexecve(wp[0], wp, makenv());
	prs(wp[0]); prs(": "); warn(cp);
	if (!execflg)
		trap[0] = NULL;
	leave();
	/* NOTREACHED */
}
#endif

/*
 * common actions when creating a new child
 */

#ifdef MODULE_parent
STATIC int
parent()
{
	register int i;

/* abyte('^'); */
	i = fork();
/* abyte('&'); */
/* ahexw(i); */
	if (i != 0) {
		if (i == -1)
#if 1 /* Nick */
			warn("can't fork - try again");
#else
			warn("try again");
#endif
	}
	return(i);
}
#endif

/*
 * 0< 1> are ignored as required
 * within pipelines.
 */

#ifdef MODULE_iosetup
int
iosetup(iop, pipein, pipeout)
register struct ioword *iop;
int pipein, pipeout;
{
	register u;
	char *cp, *msg;

	if (iop->io_unit == IODEFAULT)	/* take default */
		iop->io_unit = iop->io_flag&(IOREAD|IOHERE)? 0: 1;
	if (pipein && iop->io_unit == 0)
		return(0);
	if (pipeout && iop->io_unit == 1)
		return(0);
	msg = iop->io_flag&(IOREAD|IOHERE)? "open": "create";
	if ((iop->io_flag & IOHERE) == 0) {
		cp = iop->io_name;
		if ((cp = evalstr(cp, DOSUB|DOTRIM)) == NULL)
			return(1);
	}
	if (iop->io_flag & IODUP) {
		if (cp[1] || (!digit(*cp) && *cp != '-')) {
			prs(cp);
			err(": illegal >& argument");
			return(1);
		}
		if (*cp == '-')
			iop->io_flag = IOCLOSE;
		iop->io_flag &= ~(IOREAD|IOWRITE);
	}
	switch (iop->io_flag) {
	case IOREAD:
		u = open(cp, 0);
/* amess("IOREAD u = "); */
/* ahexw(u); */
/* acrlf(); */
		break;

	case IOHERE:
	case IOHERE|IOXHERE:
		u = herein(iop->io_name, iop->io_flag&IOXHERE);
		cp = "here file";
		break;

	case IOWRITE|IOCAT:
		if ((u = open(cp, 1)) >= 0) {
/* amess("IOWRITE|IOCAT u = "); */
/* ahexw(u); */
/* acrlf(); */
			lseek(u, (long)0, 2);
			break;
		}
	case IOWRITE:
		u = creat(cp, 0666);
/* amess("IOWRITE u = "); */
/* ahexw(u); */
/* acrlf(); */
		break;

	case IODUP:
		u = dup2(*cp-'0', iop->io_unit);
		break;

	case IOCLOSE:
		close(iop->io_unit);
		return(0);
	}
	if (u < 0) {
		prs(cp);
		prs(": cannot ");
		warn(msg);
		return(1);
	} else {
		if (u != iop->io_unit) {
			dup2(u, iop->io_unit);
			close(u);
		}
	}
	return(0);
}
#endif


#ifdef MODULE_echo
STATIC void
echo(wp)
register char **wp;
{
	register i;

	prs("+");
	for (i=0; wp[i]; i++) {
		if (i)
			prs(" ");
		prs(wp[i]);
	}
	prs("\n");
}
#endif


#ifdef MODULE_find1case
STATIC struct op **
find1case(t, w)
struct op *t;
char *w;
{
	register struct op *t1;
	struct op **tp;
	register char **wp, *cp;

	if (t == NULL)
		return((struct op **)NULL);
	if (t->type == TLIST) {
		if ((tp = find1case(t->left, w)) != NULL)
			return(tp);
		t1 = t->right;	/* TPAT */
	} else
		t1 = t;
	for (wp = t1->words; *wp;)
		if ((cp = evalstr(*wp++, DOSUB)) && gmatch(w, cp))
			return(&t1->left);
	return((struct op **)NULL);
}
#endif


#ifdef MODULE_findcase
STATIC struct op *
findcase(t, w)
struct op *t;
char *w;
{
	register struct op **tp;

	return((tp = find1case(t, w)) != NULL? *tp: (struct op *)NULL);
}
#endif

/*
 * Enter a new loop level (marked for break/continue).
 */

#ifdef MODULE_brkset
STATIC void
brkset(bc)
struct brkcon *bc;
{
	bc->nextlev = brklist;
	brklist = bc;
}
#endif

/*
 * Wait for the last process created.
 * Print a message for each process found
 * that was killed by a signal.
 * Ignore interrupt signals while waiting
 * unless `canintr' is true.
 */

#ifdef MODULE_waitfor
int
waitfor(lastpid, canintr)
register int lastpid;
int canintr;
{
	register int pid, rv;
	int s;
	int oheedint = heedint;

	heedint = 0;
	rv = 0;
	do {
		pid = wait(&s);
		if (pid == -1) {
			if (errno != EINTR || canintr)
				break;
		} else {
			if ((rv = WAITSIG(s)) != 0) {
				if (rv < NSIGNAL) {
					if (signame[rv] != NULL) {
						if (pid != lastpid) {
							prn(pid);
							prs(": ");
						}
						prs(signame[rv]);
					}
				} else {
					if (pid != lastpid) {
						prn(pid);
						prs(": ");
					}
					prs("Signal "); prn(rv); prs(" ");
				}
				if (WAITCORE(s))
					prs(" - core dumped");
				if (rv >= NSIGNAL || signame[rv])
					prs("\n");
				rv = -1;
			} else
				rv = WAITVAL(s);
		}
	} while (pid != lastpid);
	heedint = oheedint;
	if (intr)
		if (talking) {
			if (canintr)
				intr = 0;
		} else {
			if (exstat == 0) exstat = rv;
			onintr(0);
		}
	return(rv);
}
#endif


#ifdef MODULE_setstatus
int
setstatus(s)
register int s;
{
	exstat = s;
	setval(lookup("?"), putn(s));
	return(s);
}
#endif

/*
 * PATH-searching interface to execve.
 * If getenv("PATH") were kept up-to-date,
 * execvp might be used.
 */

#ifdef MODULE_rexecve
char *
rexecve(c, v, envp)
char *c, **v, **envp;
{
	register int i;
	register char *sp, *tp;
	int eacces = 0, asis = 0;

	sp = any('/', c)? "": path->value;
	asis = *sp == '\0';
	while (asis || *sp != '\0') {
		asis = 0;
		tp = env.linep;
		for (; *sp != '\0'; tp++)
			if ((*tp = *sp++) == ':') {
				asis = *sp == '\0';
				break;
			}
		if (tp != env.linep)
			*tp++ = '/';
		for (i = 0; (*tp++ = c[i++]) != '\0';)
			;
		execve(env.linep, v, envp);
		switch (errno) {
		case ENOEXEC:
#if 1 /* Nick */
			return "executable format error";
		case ENOALIGN:
			return "file is not aligned";
		case ETOOSHORT:
			return "file is too short, or hole";
		case ESHELL:
#endif
			*v = env.linep;
			tp = *--v;
			*v = env.linep;
			execve("/bin/sh", v, envp);
			*v = tp;
			return("no Shell");

		case ENOMEM:
			return("program too big");

		case E2BIG:
			return("argument list too long");

		case EACCES:
			eacces++;
			break;
		}
	}
	return(errno==ENOENT ? "not found" : "cannot execute");
}
#endif

/*
 * Run the command produced by generator `f'
 * applied to stream `arg'.
 */

#ifdef MODULE_run
int
run(argp, f)
struct ioarg *argp;
int (*f)();
{
	struct op *otree;
	struct wdblock *swdlist;
	struct wdblock *siolist;
	jmp_buf ev, rt;
	xint *ofail;
	int rv;

	areanum++;
	swdlist = wdlist;
	siolist = iolist;
	otree = outtree;
	ofail = failpt;
	rv = -1;
	if (newenv(setjmp(errpt = ev)) == 0) {
		wdlist = 0;
		iolist = 0;
		pushio(argp, f);
		env.iobase = env.iop;
		yynerrs = 0;
		if (setjmp(failpt = rt) == 0 && yyparse() == 0)
			rv = execute(outtree, NOPIPE, NOPIPE, 0);
		quitenv();
	}
	wdlist = swdlist;
	iolist = siolist;
	failpt = ofail;
	outtree = otree;
	freearea(areanum--);
	return(rv);
}
#endif

/* -------- do.c -------- */
/* #include "sh.h" */

/*
 * built-in commands: doX
 */


#ifdef MODULE_dolabel
int
dolabel()
{
	return(0);
}
#endif


#ifdef MODULE_dochdir
int
dochdir(t)
register struct op *t;
{
	register char *cp, *er;

	if ((cp = t->words[1]) == NULL && (cp = homedir->value) == NULL)
		er = ": no home directory";
	else if(chdir(cp) < 0)
		er = ": bad directory";
	else
		return(0);
	prs(cp != NULL? cp: "cd");
	err(er);
	return(1);
}
#endif


#ifdef MODULE_doshift
int
doshift(t)
register struct op *t;
{
	register n;

	n = t->words[1]? getn(t->words[1]): 1;
	if(dolc < n) {
		err("nothing to shift");
		return(1);
	}
	dolv[n] = dolv[0];
	dolv += n;
	dolc -= n;
	setval(lookup("#"), putn(dolc));
	return(0);
}
#endif

/*
 * execute login and newgrp directly
 */

#ifdef MODULE_dologin
int
dologin(t)
struct op *t;
{
	register char *cp;

	if (talking) {
		signal(SIGINT, SIG_DFL);
		signal(SIGQUIT, SIG_DFL);
	}
	cp = rexecve(t->words[0], t->words, makenv());
	prs(t->words[0]); prs(": "); err(cp);
	return(1);
}
#endif


#ifdef MODULE_doumask
int
doumask(t)
register struct op *t;
{
	register int i, n;
	register char *cp;

	if ((cp = t->words[1]) == NULL) {
		i = umask(0);
		umask(i);
		for (n=3*4; (n-=3) >= 0;)
			putc('0'+((i>>n)&07));
		putc('\n');
	} else {
		for (n=0; *cp>='0' && *cp<='9'; cp++)
			n = n*8 + (*cp-'0');
		umask(n);
	}
	return(0);
}
#endif


#ifdef MODULE_doexec
int
doexec(t)
register struct op *t;
{
	register i;
	jmp_buf ex;
	xint *ofail;

	t->ioact = NULL;
	for(i = 0; (t->words[i]=t->words[i+1]) != NULL; i++)
		;
	if (i == 0)
		return(1);
	execflg = 1;
	ofail = failpt;
	if (setjmp(failpt = ex) == 0)
		execute(t, NOPIPE, NOPIPE, FEXEC);
	failpt = ofail;
	execflg = 0;
	return(1);
}
#endif


#ifdef MODULE_dodot
int
dodot(t)
struct op *t;
{
	register i;
	register char *sp, *tp;
	char *cp;

	if ((cp = t->words[1]) == NULL)
		return(0);
	sp = any('/', cp)? ":": path->value;
	while (*sp) {
		tp = env.linep;
		while (*sp && (*tp = *sp++) != ':')
			tp++;
		if (tp != env.linep)
			*tp++ = '/';
		for (i = 0; (*tp++ = cp[i++]) != '\0';)
			;
		if ((i = open(env.linep, 0)) >= 0) {
			exstat = 0;
			next(remap(i));
			return(exstat);
		}
	}
	prs(cp);
	err(": not found");
	return(-1);
}
#endif


#ifdef MODULE_dowait
int
dowait(t)
struct op *t;
{
	register i;
	register char *cp;

	if ((cp = t->words[1]) != NULL) {
		i = getn(cp);
		if (i == 0)
			return(0);
	} else
		i = -1;
	setstatus(waitfor(i, 1));
	return(0);
}
#endif


#ifdef MODULE_doread
int
doread(t)
struct op *t;
{
	register char *cp, **wp;
	register nb;
	register int  nl = 0;

	if (t->words[1] == NULL) {
		err("Usage: read name ...");
		return(1);
	}
	for (wp = t->words+1; *wp; wp++) {
		for (cp = env.linep; !nl && cp < elinep-1; cp++)
			if ((nb = read(0, cp, sizeof(*cp))) != sizeof(*cp) ||
			    (nl = (*cp == '\n')) ||
			    (wp[1] && any(*cp, ifs->value)))
				break;
		*cp = 0;
		if (nb <= 0)
			break;
		setval(lookup(*wp), env.linep);
	}
	return(nb <= 0);
}
#endif


#ifdef MODULE_doeval
int
doeval(t)
register struct op *t;
{
	return(RUN(awordlist, t->words+1, wdchar));
}
#endif


#ifdef MODULE_dotrap
int
dotrap(t)
register struct op *t;
{
	register int  n, i;
	register int  resetsig;

	if (t->words[1] == NULL) {
		for (i=0; i<=_NSIG; i++)
			if (trap[i]) {
				prn(i);
				prs(": ");
				prs(trap[i]);
				prs("\n");
			}
		return(0);
	}
	resetsig = digit(*t->words[1]);
	for (i = resetsig ? 1 : 2; t->words[i] != NULL; ++i) {
		n = getsig(t->words[i]);
		xfree(trap[n]);
		trap[n] = 0;
		if (!resetsig) {
			if (*t->words[1] != '\0') {
				trap[n] = strsave(t->words[1], 0);
				setsig(n, (sig_t)sig); /* Nick cast */
			} else
				setsig(n, SIG_IGN);
		} else {
			if (talking)
				if (n == SIGINT)
					setsig(n, (sig_t)onintr); /* Nick cast */
				else
					setsig(n, n == SIGQUIT ? SIG_IGN 
							       : SIG_DFL);
			else
				setsig(n, SIG_DFL);
		}
	}
	return(0);
}
#endif


#ifdef MODULE_getsig
int
getsig(s)
char *s;
{
	register int n;

	if ((n = getn(s)) < 0 || n > _NSIG) {
		err("trap: bad signal number");
		n = 0;
	}
	return(n);
}

#if 1 /* Nick */
void setsig(signal_t n, sig_t f)
#else
void
setsig(n, f)
register n;
void (*f) __P((int));
#endif
{
	if (n == 0)
		return;
	if (signal(n, SIG_IGN) != SIG_IGN || ourtrap[n]) {
		ourtrap[n] = 1;
		signal(n, f);
	}
}
#endif


#ifdef MODULE_getn
int
getn(as)
char *as;
{
	register char *s;
	register n, m;

	s = as;
	m = 1;
	if (*s == '-') {
		m = -1;
		s++;
	}
	for (n = 0; digit(*s); s++)
		n = (n*10) + (*s-'0');
	if (*s) {
		prs(as);
		err(": bad number");
	}
	return(n*m);
}
#endif


#ifdef MODULE_dobreak
int
dobreak(t)
struct op *t;
{
	return(brkcontin(t->words[1], 1));
}
#endif


#ifdef MODULE_docontinue
int
docontinue(t)
struct op *t;
{
	return(brkcontin(t->words[1], 0));
}
#endif


#ifdef MODULE_brkcontin
STATIC int
brkcontin(cp, val)
register char *cp;
int val;
{
	register struct brkcon *bc;
	register nl;

	nl = cp == NULL? 1: getn(cp);
	if (nl <= 0)
		nl = 999;
	do {
		if ((bc = brklist) == NULL)
			break;
		brklist = bc->nextlev;
	} while (--nl);
	if (nl) {
		err("bad break/continue level");
		return(1);
	}
	isbreak = val;
	longjmp(bc->brkpt, 1);
	/* NOTREACHED */
}
#endif


#ifdef MODULE_doexit
int
doexit(t)
struct op *t;
{
	register char *cp;

	execflg = 0;
	if ((cp = t->words[1]) != NULL)
		setstatus(getn(cp));
	leave();
	/* NOTREACHED */
}
#endif


#ifdef MODULE_doexport
int
doexport(t)
struct op *t;
{
	rdexp(t->words+1, export, EXPORT);
	return(0);
}
#endif


#ifdef MODULE_doreadonly
int
doreadonly(t)
struct op *t;
{
	rdexp(t->words+1, ronly, RONLY);
	return(0);
}
#endif


#ifdef MODULE_rdexp
STATIC void
rdexp(wp, f, key)
register char **wp;
void (*f)();
int key;
{
	if (*wp != NULL) {
		for (; *wp != NULL; wp++)
			if (checkname(*wp))
				(*f)(lookup(*wp));
			else
				badid(*wp);
	} else
		putvlist(key, 1);
}
#endif


#ifdef MODULE_badid
STATIC void
badid(s)
register char *s;
{
	prs(s);
	err(": bad identifier");
}
#endif


#ifdef MODULE_doset
int
doset(t)
register struct op *t;
{
	register struct var *vp;
	register char *cp;
	register n;

	if ((cp = t->words[1]) == NULL) {
		for (vp = vlist; vp; vp = vp->next)
			varput(vp->name, 1);
		return(0);
	}
	if (*cp == '-') {
		/* bad: t->words++; */
		for(n = 0; (t->words[n]=t->words[n+1]) != NULL; n++)
			;
		if (*++cp == 0)
			flag['x'] = flag['v'] = 0;
		else
			for (; *cp; cp++)
				switch (*cp) {
				case 'e':
					if (!talking)
						flag['e']++;
					break;

				default:
					if (*cp>='a' && *cp<='z')
						flag[*cp]++;
					break;
				}
		setdash();
	}
	if (t->words[1]) {
		t->words[0] = dolv[0];
		for (n=1; t->words[n]; n++)
			setarea((char *)t->words[n], 0);
		dolc = n-1;
		dolv = t->words;
		setval(lookup("#"), putn(dolc));
		setarea((char *)(dolv-1), 0);
	}
	return(0);
}
#endif


#ifdef MODULE_varput
void
varput(s, out)
register char *s;
int out;
{
	if (letnum(*s)) {
		write(out, s, strlen(s));
		write(out, "\n", 1);
	}
}
#endif


#define	SECS	60L
#define	MINS	3600L


#ifdef MODULE_dotimes
int
dotimes()
{
#if 0 /* NICK VERY TEMPORARY PLEASE FIX THIS !! */
	struct tms tbuf;

	times(&tbuf);

	prn((int)(tbuf.tms_cutime / MINS));
	prs("m");
	prn((int)((tbuf.tms_cutime % MINS) / SECS));
	prs("s ");
	prn((int)(tbuf.tms_cstime / MINS));
	prs("m");
	prn((int)((tbuf.tms_cstime % MINS) / SECS));
	prs("s\n");
#endif
	return(0);
}
#endif


#ifdef MODULE_inbuilt
struct	builtin {
	char	*command;
	int	(*fn)();
};
static struct	builtin	builtin[] = {
	":",		dolabel,
	"cd",		dochdir,
	"shift",	doshift,
	"exec",		doexec,
	"wait",		dowait,
	"read",		doread,
	"eval",		doeval,
	"trap",		dotrap,
	"break",	dobreak,
	"continue",	docontinue,
	"exit",		doexit,
	"export",	doexport,
	"readonly",	doreadonly,
	"set",		doset,
	".",		dodot,
	"umask",	doumask,
	"login",	dologin,
	"newgrp",	dologin,
	"times",	dotimes,
	"echo",		doecho, /* Nick */
	"sync",		dosync, /* Nick */
	"sysdebug",	dosysdebug, /* Nick */
	"systrace",	dosystrace, /* Nick */
	0,
};

int (*inbuilt(s))()
register char *s;
{
	register struct builtin *bp;

	for (bp = builtin; bp->command != NULL; bp++)
		if (strcmp(bp->command, s) == 0)
			return(bp->fn);
	return((int(*)())NULL);
}
#endif


#ifdef MODULE_doecho
int
doecho(t)
register struct op *t;
	{
#if 1 /* see uzi/src/init/echo.c (slightly adapted for use within msh) */
	unsigned int i;
	unsigned char first = 1, esc = 0, nonl = 0, *p;

	if (t->words[1] == NULL) {
		prs("usage: echo [-ne] string ...\n");
		return 1;
	}
	for (i = 1; (p = t->words[i]); i++) {
		if (p[0] == '-') {
			for (++p; *p; ++p) {
				switch (*p) {
				case 'n':	nonl = 1; break;
				case 'e':	esc = 1; break;
				default:	goto Print;
				}
			}
		}
		else	break;
	}
Print:	for (; (p = t->words[i]); ++i) {
		if (!first)
			write(1, " ", 1);
		first = 0;
		if (esc)
			prs_expand(1, p, &nonl);
		else	write(1, p, strlen(p));
	}
	if (!nonl)
		write(1, "\n", 1);
#else
	register i, cr;

	i = 1;
	cr = 1;
	if (t->words[i])
		{
		if (strcmp(t->words[i], "-n") == 0)
			{
			i = 2;
			cr = 0;
			}
		if (t->words[i])
			{
			prs_expand(1, t->words[i++]);
			while (t->words[i])
				{
				write(1, " ", 1);
				prs_expand(1, t->words[i++]);
				}
			}
		}
	if (cr)
		{
		write(1, "\n", 1);
		}
#endif
	return 0;
	}
#endif


#ifdef MODULE_dosync
int
dosync()
	{
	sync();
	return 0;
	}
#endif


#ifdef MODULE_dosysdebug
int
dosysdebug(t)
register struct op *t;
	{
	if (t->words[1] == NULL)
		{
		prs("usage: sysdebug n\n");
		return 1;
		}
	sysdebug(atoi(t->words[1]));
	return 0;
	}
#endif


#ifdef MODULE_dosystrace
int
dosystrace(t)
register struct op *t;
	{
	if (t->words[1] == NULL)
		{
		prs("usage: systrace n\n");
		return 1;
		}
	systrace(atoi(t->words[1]));
	return 0;
	}
#endif


