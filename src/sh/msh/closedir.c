/* closedir.c	closedir implementation
 *
 */
#include <unistd.h>
#include <alloc.h>
#include <dirent.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>

/* don't bother freeing anything, since msh doesn't like sharing its heap */

int nick_closedir(DIR *dir)
{
	if (dir == NULL || dir->dd_buf == NULL || dir->dd_fd == 0) {
		errno = EFAULT;
		return -1;
	}
	close(dir->dd_fd);
#if 0
	free(dir->dd_buf);
#endif
	dir->dd_fd = 0;
	dir->dd_buf = NULL;
#if 0
	free(dir);
#endif
	return 0;
}

