# auto.mk by Nick for Hytech NOS/UZI project

# -----------------------------------------------------------------------------

SUBDIRS=

OUTPUTS=	msh$(EXEEXT)

msh$(call CANON, $(EXEEXT))_SOURCES= \
		sh1.c sh2.c sh3.c sh4.c sh5.c sh6.c opendir.c closedir.c \
		readdir.c

sh1_c_MODULES=	intr inparse flags flag elinep null heedint env shellname \
		search qflag main setdash newfile onecommand fail leave warn \
		err newenv quitenv anys any putn sh_itoa next onintr letter \
		digit letnum space strsave xfree sig runtrap lookup setval \
		nameval export ronly isassign assign checkname putvlist \
		eqname findeq gmatch cclass areabot areatop areanxt initarea \
		getcell freecell freearea setarea getarea garbage

sh2_c_MODULES=	startl peeksym nlseen iounit yylval yyparse pipeline andor \
		c_list synio musthave simple nested command dogroup thenpart \
		elsepart caselist casepart pattern wordlist list block restab \
		rlookup newtp namelist copyw word copyio io zzerr yyerror \
		yylex collect dual diag tree printf

sh3_c_MODULES=	execute forkexec parent iosetup echo find1case findcase \
		brkset waitfor setstatus rexecve run dolabel dochdir doshift \
		dologin doumask doexec dodot dowait doread doeval dotrap \
		getsig getn dobreak docontinue brkcontin doexit doexport \
		doreadonly rdexp badid doset varput dotimes inbuilt doecho \
		dosync dosysdebug dosystrace

sh4_c_MODULES=	eval makenv evalstr expand blank subgetc dollar grave unquote \
		glob cl nl spcl globname generate anyspcl xstrcmp newword \
		addword getwords func globv glob0 glob1 glob2 glob3 memcopy

sh5_c_MODULES=	sharedbuf mainbuf bufid temparg getc unget eofc readc ioecho \
		pushio setbase nlchar wdchar dolchar xxchar strchar qstrchar \
		filechar herechar gravechar qgravechar linechar prs \
		prs_expand putc xprn closef closeall remap openpipe closepipe \
		inhere acthere markhere gethere readhere herein scraphere \
		freehere tempname

# -----------------------------------------------------------------------------

