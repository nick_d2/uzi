copy catman.bat ..\..\bin
copy mkwhatis.bat ..\..\bin\man
copy mkwhatis.sed ..\..\bin\man
copy mkwhatis.sh ..\..\bin\man

md build-l
cd build-l
copy ..\apropo-l.lnk apropos.lnk
copy ..\catman-l.lnk catman.lnk
copy ..\man-l.lnk man.lnk
copy ..\build-l.ban n.bat
call n
cd..

md build-b
cd build-b
copy ..\apropo-b.lnk apropos.lnk
copy ..\catman-b.lnk catman.lnk
copy ..\man-b.lnk man.lnk
copy ..\build-b.ban n.bat
call n
cd ..

