/* utils.h for uzi180 utils by Nick - subroutines based on utils.asz source */

#ifndef __UTILS_H
#define __UTILS_H

/* char *itob(int n, char *s, int base); */
void bzero(char *ptr, int count);
void bfill(char *ptr, char val, int count);
void bcopy(char *src, char *dest, int count);
int int_min(int a, int b);
int int_max(int a, int b);

#endif /* __UTILS_H */

