HYTECH-INTERNATIONAL BV
CMX/UZI TESTRELEASE FOR SOFTWARE DEPARTMENT 030306SD

MANIFEST

-rw-r--r--    1 nick     users    10162351 Mar  6 22:08 uzi.zip

INTRODUCTION

Full release of the Hytech CMX/UZI kernel and associated utilities.  The zipped
release is at "http://www.hytechscales.com/release/nld/test/030306SD".

Note:  This doesn't contain Joost's userlevel work at the moment.

CHANGES IN THIS RELEASE

1.  New executable file format.  Executables can now be 'large' or 'banked'
    (see the IAR C compiler manuals for more information about memory models).

    The 'large' executables are similar to the older executables, but they now
    have a header which tells the kernel which segments must be loaded where.
    This cuts down the need for startup code to be compiled into every program.
    The handling of "IDATA" and "CDATA" is more efficient because they are now
    overlaid in logical memory, allowing full usage of the program's 32 kbyte
    address space for code and read/write data.  (In other words, read-only
    data is loaded once, into the read/write space, and is not kept during the
    execution of the program).  Large executables can be up to 32k in size.

    The 'banked' executables are almost the same as the new 'large' format,
    but the header of the file instructs the kernel's loader not to load any
    code beyond a certain point in the file.  After the initialisation code
    and data has been loaded, further code (banked code) is executed by using
    the Z180's "BBR" (Bank Base Register) to bring the code into the logical
    address space.  The code really lies in the terminal's 768 kbyte protected
    RAM-disk area, which is shared with the root filesystem.  So quite some
    contortions are needed to ensure that the remaining space, after software
    has been loaded for banked execution, can still be used as a RAM drive.
    There is no limit on the size of a banked executable, it can use all of
    available RAM except for that used by the kernel.  So approximately 512k.

    We also have a third executable type, 'kernel' which is very similar to a
    'banked' executable, but the header describes an extra code/data segment
    which gets loaded at address 0.  This provides the interrupt table and
    some other routines which need to be loaded all the time.  Previously this
    initialisation had been done by the kernel itself, but the new arrangement
    moves initialisation code out of the kernel and into the bootloader.  This
    is very desirable because the bootloader can be discarded after booting.
    'Kernel' executables are specially marked with a magic number, ensuring
    they can't be executed at user level by accident.  'Kernel' and 'banked'
    executables need to be aligned with the 'align' command before being run.

2.  Changes to the Hytech 'align' and 'ualign' utilities.  We now have a mode
    which prints which files are being aligned, and the programs can be told
    to skip symlinks as well.  This was necessary to get the bootup copying of
    files to look pretty.  It's also a good example of why we would want to
    add new switches and modes of operation to the standard Unix utilities.
    In general the standard Unix version 7 functionality is exactly what we
    want, but in some cases we will take out features which we don't need, and
    in some cases we will add specific functionality where there is a need.

3.  In the kernel, revised handling for block and character devices.  We now
    have an improved "devtab" (device table) which works more like 7th Edition
    Unix - the previous "uzi" device table was more limited because it couldn't
    handle "major" and "minor" device numbers - all devices had to be "major 0"
    and all the corresponding minor numbers had to be listed explicitly in the
    device table.  Now, the "major" number correctly indicates the family of
    devices, as it should, and each major number only takes one "devtab" entry.
    In our system, most of the devices are versions of the "tty" device (ie.
    serial ports and API BUS), so this was a substantial reduction in the size
    of the device table (from 18 entries to 5).  The changes to the device
    handling introduced some incompatiblity with existing device drivers, so
    they have also been modified for the new calling convention.  Please test!

4.  Fixed a bug in the TTY handling so that the TTY mode is remembered for as
    long as at least one handle remains open to the device.  This covers the
    situation of 'more' which opens its own TTY handle in order to save and
    restore the mode.  The default mode is binary since the last release.
    Binary is suitable for the Cognitive printer and other devices such as the
    touchscreen.  However the user's TTY is only set to cooked mode at bootup,
    so it should not be clobbered thereafter - hence the reference counting.

5.  Improvements to the 'signal' handling - not quite finished yet.  The idea
    is that you can catch various events, such as "control C" or "broken pipe"
    by registering a signal handler with the kernel (this is standard Unix
    functionality).  Unfortunately there are some bank-switching complications
    making it rather hard for the kernel to call out into the user program -
    the kernel must at least find out whether the user program is 'large' or
    'banked' and use the appropriate calling conventions.  This is mostly done
    but needs to be tested for each memory model - for now, don't use signals!

6.  Now got a choice of shells - SASH, Minix or Bourne.  At present the simple
    SASH shell is suitable for all our needs, but looking to the future we will
    probably want to run some shell scripts overnight, eg. to back up journal
    files into a special directory of the flash card, etc.  The shell scripts
    can get very complicated (see /uzi/src/man/makewhatis.sh for example), so
    we will probably want to use the features of the Minix or Bourne shell in
    our scripts eventually.  The Minix shell seems to be a little more reliable
    but the Bourne shell is the original and so it would potentially be more
    compatible with other people's shell scripts.  Note:  We need SED / AWK!!

7.  The unix 'man' command now works properly.  The manual pages themselves
    are copied from an early Unix version and are probably not legal, but good
    enough to test the "manpage" infrastructure.  The BSD utilities "catman"
    and so on, have been ported in order to create the /usr/man/* directories.

8.  The unix 'more' command now works properly.  Pipes now work properly, so
    you can give a command such as "ls | more".  Please test this feature, and
    other shell redirection or piping features in general!  Does it all work??

9.  Improvements to the compile system - we now have our own assembler (as-z80)
    and linker (link-z80).  This was necessary to get the banked executables
    to work, and also led to substantial reductions in the footprint of both
    large and kernel executables.  The output from the IAR 'C' compiler is
    automatically run through the IAR assembler (there seems to be no way of
    preventing this), but we ignore this and take the resulting 'S01' file,
    which contains assembler source in a fairly generic Z80 language.

    Our assembler had to be modified to handle the IAR compiler output, this is
    all working fine, except for a small problem handling symbol names which
    happen to conflict with Z80 registers.  So don't define global symbols of
    the names: a b c d e h l n r bc de hl ix iy, as this will abort assembly!

    Coming soon - faster linking - we have some code which needs to be added
    to the linker so that library searching will be faster.  This wasn't
    important enough to delay the release, so please put up with the compile
    times for now.  The new code for library indexing will reduce them a lot.

    Please note the new "rst" files (relocated listing) which are produced in
    your build directory after building.  This contains the real object code,
    AFTER linking, alongside the assembler source.  So you can easily check if
    your symbols ended up where you intended them to be.  Use the mapfile too!
    The "rst" files are much easier to read than the Intel-Hex format used for
    generating the binaries.  So there's no need for a Z180 disassembler when
    checking your output.  In future the "rst" files could form the basis for
    a source-level debugger if we decided to develop this feature.

    Note:  The Hytech assembler and linker are based on an open-source package
    called "asxxxx" which seems to be receiving regular updates.  See /uzi/doc.

10. In most of the build directories, temporary files are now gathered in a
    subdirectory named 'build-b' (banked), 'build-l' (large) or 'build' (if
    a choice is not available).  This means the /uzi folder can be archived
    with a command to exclude the build directories, which should reduce the
    download time via Internet, and would also make it possible to build the
    software using less disk space than before.  All the intermediate files
    can harmlessly be deleted after the build (eg. type "deltree build-l").

11. Improvements to the /uzi folder structure.  Source code for programs is
    placed one level deeper, for example "/uzi/src/hello" or several levels
    deeper, for example "/uzi/src/kernel/cmx".  Binaries are still kept under
    "/uzi/bin" as usual, but Windows binaries are now kept in "/uzi/src/bin"
    exclusively.  The folder "/uzi/include" is unchanged, but the new folder
    "/uzi/lib" contains all the pre-compiled library modules, including the
    'C' library, the 'system calls' library and the 'IAR internal' library.
    These are "libc.lib", "libsys.lib" and "libiar.lib" respectively.  Source
    is now under "/uzi/src/libc", "/uzi/src/libsys" and "/uzi/src/libiar"
    respectively, rather than the old system of mixing binary and source files.

12. Batch files to create 'n.bat' and 'xxxx.lnk' files for you.  This should
    make it easier to convert from the earlier compile system to the new.

    For a sample project you might create the folder "/uzi/src/sample" and
    create a single file called "/uzi/src/sample/sample.c".  To compile the
    program you would first type "mknbat-l sample" which creates "n.bat"
    containing the necessary commands to compile "sample.c" to an executable
    called "sample".  ('l' for large memory model, you could use 'b' instead).

    Before running this "n.bat" file, you must first create a corresponding
    linker script, done by typing "mklink-l sample" which creates "sample.lnk"
    having the commands to link "sample.rel" with the usual 'C' libraries to
    create the final executable, "sample".

    Note that once you've created the compile and link scripts, you shouldn't
    need to use "mk*.bat" any more.  You just continue developing, and run
    "n.bat" each time compilation is needed.  The generated files "n.bat" and
    "sample.lnk" can be edited if you need to add any further 'C' modules to
    the project.  If you do this, be careful not to run "mknbat-*.bat" or
    "mklink-*.bat" since you would clobber your changes.

    Note:  The generated files are geared for simplicity, so they don't support
    the "build", "build-b" or "build-l" directories.  For an example of how
    you would create both "large" and "banked" versions of a program, see the
    example folder "/uzi/src/hello".  Your build system should ideally look
    like this, although you might want to modify it to output your executables
    directly into "/uzi/bin/large" or "/uzi/bin/banked" for inclusion in a
    flash card image.  After this, add the needed commands to "/uzi/bin/n.ucp".

INSTALLATION PROCEDURE

In the Windows "System Properties" dialog, click "Advanced -> Environment
Variables".  Add the entry "c:\uzi\src\bin" into your path.  This is necessary
for the new compile system to work.  Note the distinction between "c:\uzi\bin",
which contains executables for the Hytech 1000 / 1500 terminal, and the folder
mentioned here, which contains Windows executables to use during building.

Due to the system-wide changes (executable formats and device numbering), it's
not possible to upgrade to this release by means of "newkrnl.sh" (although we
would like to test the "newkrnl.sh" functionality whenever possible).  Instead
you should use the CLR MEM switch of the terminal, although this will mean
having to reset the terminal's RTC manually to the current time and date.

As with the previous release, it's necessary to have a DOS or Windows 98 PC,
loaded with the Hytech POS software, to load the software from the CLR MEM
state.  Coming soon is a modification to the EPROM to avoid this inconvenience. 
Once the kernel has been loaded, further initialisation proceeds via flash
card.  So the flash card must have been initialised, with "hytdisk.dat" and
"uzidisk.dat" as usual, and inserted, before loading the kernel via POS.

Normally, you would build the kernel before doing the above.  This is because
only the kernel's build-script contains the commands to generate CHECKSUM for
the downloading via POS.  If you want to save time building the kernel, you
could just copy the distribution files CHECKSUM, KERNEL.BIN and BOOT.BIN from
the "/uzi/bin" directory to "/nlddl" on the machine which is running POS.

*** LATE BREAKING NOTE:

When using the banked executables, you will have to copy the executable onto
the root filesystem (ie. the RAMdrive) before executing each time.  Then the
file needs to be aligned with the "align" command.  But this can lead to a
problem:  If the file is modified after being aligned, the alignment is broken
and this can't easily be fixed at the moment.  You would need to delete the
file and start again.  So please use the following sequence to test your work:

	rm /bin/myprogram
	cp /usr/myfolder/myprogram /bin
	align /bin/myprogram
	chmod 755 myprogram
	myprogram

In this case I selected a folder called "myfolder" on the flash card for use in
transporting the banked program code onto the RAMdrive.  Note that the flash
card is mounted on /usr, so the folder "/usr/myfolder" is referred to as simply
"/myfolder" while constructing the flash card.  Use the "ucp" program for this.

