HYTECH-INTERNATIONAL BV
CMX/UZI TESTRELEASE FOR SOFTWARE DEPARTMENT 030217SD

MANIFEST

-rw-r--r--    1 nick     users     7808012 Feb 17 20:05 uzi.zip

INTRODUCTION

Full release of the Hytech CMX/UZI kernel and associated utilities.  The
release is at "www.hytechscales.com/release/nld/test/030217SD".

Note:  This doesn't contain Joost's userlevel work at the moment.

CHANGES IN THIS RELEASE

Contains an upgrade to the "teletype driver" so that now you have access to all
the serial ports, the scale, the internal printer, and both the LCDs.  I put
some testroutines in the directory /usr/root to give you a starting point.

The rough list of changes:
	- now got a ramdrive as drive a:
	- compactflash has changed to drive b:, not c:
	- now got a generic bootloader
	- now got a virtual memory table
	- can now run shell scripts
	- disk format has changed again
	- can now install a new kernel
	- can now access more devices
	- console is now serial 1 @ 9600
	- rtc is working, and prompts for initial set
	- revised malloc implementation
	- now got sample files for devices
	- now got ter program to test devices
	- can now run "passwd" for userfile maintenance
	- can now run "adduser" for userfile maintenance
	- now got a default group of "100" for users
	- ls program now displays in columns by default

Your installation procedure should be something like this, hopefully avoiding
the problems of last time:

	- rename /uzi to something else (as usual)
	- unpack uzi.zip to /uzi
	- reinstall the files making up your gui + application
	- cd /uzi/boot and run n.bat
	  (this will copy BOOT.BIN into \NLDDL on your Win98
	  machine, so that it can be loaded using POS)
	- cd /uzi/kernel and run n.bat
	  (this will copy KERNEL.BIN and CHECKSUM into \NLDDL
	  so that they can be loaded using POS)
	- insert the flash card into your drive (I assume E:)
	- cd /uzi/bin and run n.bat
	  (this will copy UZIDISK.DAT onto your flash card,
	  which must already have the HYTDISK.DAT file.  Note,
	  the size has changed to 4 Megabytes from 1.5 Meg, so
	  there's a strong likelihood you'll need to reformat
	  your flash card from scratch.  Be sure to try this!)
	- stop the flash card and insert it into the terminal.
	- run HyperTerminal using a serial lead plugged into
	  SERIAL 1 on the Hytech 1000/1500.  (The barcode
	  scanner port).  HyperTerminal must be set to use
	  9600 bps, because it's the barcode scanner port.
	- clear the terminal's RAM using the CLR MEM switch.
	- allow the terminal to load using POS.
	- the terminal gets CHECKSUM, KERNEL.BIN, BOOT.BIN
	  and then reboots.
	- follow the prompts on your HyperTerminal screen.
	  In particular, type the current date and time when
	  prompted.  After the RTC has been set, using this
	  method, try not to use the CLR MEM switch anymore.
	- the terminal will take several minutes to prepare
	  the RAMdrive for use.  This happens every time the
	  terminal is loaded using POS.  The download using
	  POS results in a very inefficient memory layout,
	  hence this automatic procedure which rearranges the
	  memory layout and makes it all accessible to you.
	- after this, you will install your software by using
	  the flash card.  Note that communications are not
	  working yet, so the flash card is the only way of
	  copying data into the terminal's memory.
	- for example, you could use the "ucp" program to put
	  your program into "/bin/myprogram" on the card, then
	  insert the flash card into the terminal, reboot by
	  using the on/off switch.  The flash card is mounted
	  on /usr, so your program is "/usr/bin/myprogram".
	  You can execute it directly from there, or else, you
	  can copy it eg. "cp /usr/bin/myprogram /bin".  This
	  means the program can be loaded very fast, because
	  the /bin directory is part of the RAMdrive, not the
	  flash card.  Similarly you can run my special script
	  "/boot/newkrnl.sh" and this will copy a new kernel
	  onto the system.  Previously I would have given you
	  "kernel.bin" and you would have copied it to
	  "/boot/kernel" on your flash card.  Although I think
	  it's better if we always build it from source..??

Good luck!  When you've got it working, try the following script:

	root@sn1234:/root$ cd /usr/root
	root@sn1234:/usr/root$ chset.sh

This will copy character sets into all system devices.
Then try the following script:

	root@sn1234:/usr/root$ demos.sh

This will copy demo data onto all system devices.
You can also do things like:

	root@sn1234:/root$ echo Hello world! > /dev/lcd0

which is useful for a quick test.  In your programs
you use open(), read(), write(), ioctl() and close()
just like you are now doing for "/dev/lcd0".

