UZI SYSTEM CALL INTERFACE SUMMARY

; -----------------------------------------------------------------------------

CONTENTS

int errno;
void _exit (int val);
int access (char *path, int mode);
int alarm (uint secs);
int brk (char *addr);
int chdir (char *dir);
int chroot (char *dir);
int chmod (char *path, mode_t mode);
int chown (char *path, int owner, int group);
int close (int uindex);
int creat (char *name, mode_t mode);
int dup (int oldd);
int dup2 (int oldd, int newd);
int execve (char *name, char *argv[], char *envp[]);
int fork (void);
int fstat (int fd, void *buf);
int getfsys (int dev, void *buf);
int getgid (void);
int getpid (void);
int getppid (void);
int getuid (void);
int geteuid (void);
int getegid (void);
int getprio (void);
int ioctl (int fd, int request, ...);
int kill (int pid, int sig);
int link (char *oldname, char *newname);
int symlink (char *oldname, char *newname);
int mknod (char *name, mode_t mode, int dev);
int mkfifo (char *name, mode_t mode);
int mount (char *spec, char *dir, int rwflag);
int open (char *name, uint flag, ...);
int pause (void);
int pipe (int fildes[2]);
int read (int fd, void *buf, uint nbytes);
int sbrk (uint incr);
int seek (int fd, int offset, int whence);
off_t lseek (int fd, off_t offset, int whence);
int setgid (int gid);
int setuid (int uid);
int setprio (int pid, char prio);
sig_t signal (signal_t sig, sig_t func);
int stat (char *path, void *buf);
int stime (time_t *tvec);
int sync (void);
time_t time (time_t *tvec);
int times (struct tms *buf);
int utime (char *path, struct utimbuf *buf);
int umask (int mask);
int umount (char *spec);
int unlink (char *path);
int wait (int *statloc);
int write (int fd, void *buf, uint nbytes);
int reboot (char p1, char p2);
int systrace (int onoff);

; -----------------------------------------------------------------------------

int errno;

If any system call fails, it returns -1 (this is why most of the calls below�
are listed as having an 'int' return value).  In this case the errno variable�
will give the reason for the error.  Possible error codes are as follows:

# Start /usr/lib/liberror.txt

1 Operation not permitted (EPERM)
2 No such file or directory (ENOENT)
3 No such process (ESRCH)
4 Interrupted system call (EINTR)
5 I/O error (EIO)
6 No such device or address (ENXIO)
7 Arg list too long (E2BIG)
8 Exec format error (ENOEXEC)
9 Bad file number (EBADF)
10 No child processes (ECHILD)
11 Try again (EAGAIN)
12 Out of memory (ENOMEM)
13 Permission denied (EACCES)
14 Bad address (EFAULT)
15 Block device required (ENOTBLK)
16 Device or resource busy (EBUSY)
17 File exists (EEXIST)
18 Cross-device link (EXDEV)
19 No such device (ENODEV)
20 Not a directory (ENOTDIR)
21 Is a directory (EISDIR)
22 Invalid argument (EINVAL)
23 File table overflow (ENFILE)
24 Too many open files (EMFILE)
25 Not a typewriter (ENOTTY)
26 Text file busy (ETXTBSY)
27 File too large (EFBIG)
28 No space left on device (ENOSPC)
29 Illegal seek (ESPIPE)
30 Read-only file system (EROFS)
31 Too many links (EMLINK)
32 Broken pipe (EPIPE)
33 Math argument out of domain of func (EDOM)
34 Math result not representable (ERANGE)
35 Resource deadlock would occur (EDEADLK)
36 File name too long (ENAMETOOLONG)
37 No record locks available (ENOLCK)
38 Function not implemented (EINVFNC)
39 Directory not empty (ENOTEMPTY)
40 Too many symbolic links encountered (ELOOP)
41 It's a shell script (ESHELL)

# End /usr/lib/liberror.txt

; -----------------------------------------------------------------------------

void _exit (int val);

Returns control to the system, terminating the current process.  Some other�
eligible process will then run instead.  The memory belonging to the current�
process is released and all files are closed.  The function call does not�
return, hence the void return value (normally all system calls return 'int').

Please note that _exit does not perform any user exit processing, hence the�
name '_exit' rather than 'exit'.  The standard C library provides the functions�
exit() and atexit() allowing any user hooks to execute before making the _exit�
system call.  For example, files opened by fopen() are flushed and closed at�
exit, by a special cleanup function that gets registered during 'C' startup.

; -----------------------------------------------------------------------------

int access (char *path, int mode);

Tests whether the current user (or effective user) is allowed to access the�
given file.  If so, returns 0.  If not, returns -1, and errno filled in.

Note:  The test is equivalent to opening the file with the specified mode, and�
then closing it again without doing anything.  Because the kernel uses this�
sequence of actions to determine whether access is granted, any existing file�
of the given name, will have its last-access time and date updated by the call.

; -----------------------------------------------------------------------------

int alarm (uint secs);

Requests that the current process receive an "alarm" signal after the given�
number of seconds has expired.  As there is only one "alarm" counter per�
process, any previous active "alarm" request will be overridden by the call.

; -----------------------------------------------------------------------------

int brk (char *addr);

Sets the top of the process's memory to the given address.  On entry to the�
program, "brk" is set to the top of the loaded program image, and therefore�
must be adjusted slightly higher if the program is going to use extra memory�
for data.  This is normally done at least once, by the 'C' startup code, to�
inform the kernel of how much uninitialised data the program wishes to use.

The kernel checks if the "brk" value is getting too close to the process's�
stack pointer (ie. if there would not be enough stack space left to process a�
system call or an interrupt).  In this case the call fails and returns -1.

The 'C' library also provides a malloc() and free() implementation, which is�
based on the brk() and sbrk() system calls.  Please see the sbrk() description.

; -----------------------------------------------------------------------------

int chdir (char *dir);

Sets the current process's directory to the given path.  This is equivalent to�
"opening" the directory (execute permission is required, and the file-time is�
updated), and so it is also useful to prevent any other processes from trying�
to remove the directory or dismount the drive which contains the directory.

Of course, subsequent system calls such as open() or access() or even chdir()�
will then use the selected directory as the starting point for relative path�
specification, for example if you are in directory "/usr/lib" and you create�
the file "foo.txt" this is the same as creating the file "/usr/lib/foo.txt".

; -----------------------------------------------------------------------------

int chroot (char *dir);

Sets the current process's root directory to the given path.  This is referred�
to as a "jail", because while the chroot is active, the current process cannot�
access any files or directories higher than the given root directory.

For example, after calling chroot("/usr"), if the user tries to create a file�
"/lib/foo.txt" this will really create a file called "/usr/lib/foo.txt".  This�
can be a useful security measure for programs such as email daemons, etc.

; -----------------------------------------------------------------------------

int chmod (char *path, mode_t mode);

Allows file permissions to be altered.  Mode is normally a 3-digit octal value,�
with the digits representing "user, group, other" respectively.  For example�
644 would mean permissions of '6' for the user who owns the file, '4' for users�
in the same group as the file, and '4' for everyone else.

The octal digits can be any of the following:  (1=Execute, 2=Write, 4=Read)

	EXECUTE		WRITE		READ
  0	No		No		No
  1	Yes		No		No
  2	No		Yes		No
  3	Yes		Yes		No
  4	No		No		Yes
  5	Yes		No		Yes
  6	No		Yes		Yes
  7	Yes		Yes		Yes

In addition a fourth octal digit can be prepended, interpreted as follows:

	VTX (??)	SETGID		SETUID
  0	No		No		No
  1	Yes		No		No
  2	No		Yes		No
  3	Yes		Yes		No
  4	No		No		Yes
  5	Yes		No		Yes
  6	No		Yes		Yes
  7	Yes		Yes		Yes

The SETGID and SETUID features are useful for allowing users to access certain�
superuser functions but not others.  In this case a file's owner would be set�
to "root" and the SETUID bit would be set.  The file is usually an executable.

For example the "login" program runs as SETUID 0, meaning anyone who can run�
"login" will temporarily become the superuser while this trusted program is�
executed.  It's up to the system administrator to make sure that the "login"�
program can't do anything malicious, before granting it SETUID permissions.

To make things easier, the following definitions are provided by sys/stat.h:

#define S_UMASK 	07777	/* bits modifiable by chmod */

#define S_ISUID 	04000	/* set euid to file uid */
#define S_ISGID 	02000 	/* set egid to file gid */
#define S_ISVTX 	01000	/* */

#define S_IREAD 	0400	/* owner may read */
#define S_IWRITE 	0200 	/* owner may write */
#define S_IEXEC 	0100	/* owner may execute <directory search> */

#define S_IGREAD 	0040	/* group may read */
#define S_IGWRITE 	0020 	/* group may write */
#define S_IGEXEC 	0010	/* group may execute <directory search> */

#define S_IOREAD 	0004	/* other may read */
#define S_IOWRITE  	0002	/* other may write */
#define S_IOEXEC 	0001	/* other may execute <directory search> */

See "c:\uzi\include\sys\stat.h" for further handy macros, for example the�
S_IRWXU macro returns a mask which contains all of the "read, write, execute"�
bits for "user" set.  This is handy for isolating a particular set of bits.

; -----------------------------------------------------------------------------

int chown (char *path, int owner, int group);

Changes the owner and group of the given file.  The owner and group are 16 bit�
integer numbers which correspond to entries in "/etc/passwd" and "/etc/group"�
respectively.  The user "root" is normally 0 and often is in group 0 as well.

Superuser permissions are required in order to make this call.  In other words�
it's not permitted for a user to create a file and then assign ownership to�
anyone else.  (Assigning ownership to "root" could cause a security problem).

; -----------------------------------------------------------------------------

int close (int uindex);

Closes the given file handle.  The file handle must previously have been opened�
with open() or creat(), or created using dup() or dup2().  If the caller is the�
last process with a handle to this file, the kernel will also close the file.

When the kernel really closes the file, the file's last-modification and/or�
last-access times are updated.  Any unwritten data is flushed, and the disk is�
synchronised.  The in-core inode table entry is marked as free for later use.

; -----------------------------------------------------------------------------

int creat (char *name, mode_t mode);

Creates a file of the given name, and opens it in the given mode.  Normally�
mode would be one of the write modes such as O_WRONLY or possibly O_RDWR.  Any�
existing file of the same name is deleted first, so the caller to creat() can�
be assured of getting a zero-length file before beginning to write data.

It's not possible to specify the access permissions for the file during this�
call.  Instead, the user must call the umask() function to set the default file�
creation permissions, then call creat() as described here.  The argument to�
umask() is specified in 'inverse', for example if the file was to be created�
with octal permissions '755' you would call umask(022) followed by creat().

If creat() is successful, a file handle is returned.  This opaque "token" is�
used for further access to the file, eg. by read(), write() or close().  Each�
process has its own file table, so handles are not unique across processes.

To make things easier, the following definitions are provided by fcntl.h:

#define O_RDONLY	0
#define O_WRONLY	1
#define O_RDWR		2

/* Flag values for open only */
#define O_CREAT 	0x0100	/* create and open file */
#define O_TRUNC 	0x0200	/* open with truncation */
#define O_NEW		0x0400	/* create only if not exist */
#define O_SYMLINK	0x0800	/* open symlink as file */

/* a file in append mode may be written to only at its end. */
#define O_APPEND	0x2000	/* to end of file */
#define O_EXCL		0x4000	/* exclusive open */
#define O_BINARY	0x8000	/* not used in unix */

; -----------------------------------------------------------------------------

int dup (int oldd);

Duplicates the given file handle.  The file handle must previously have been�
opened with open() or creat(), or created using dup() or dup2().  Any free�
location in the process's file table can be used to create the duplicate.

So "oldd" is copied into a new spot in order to create the duplicate reference. �
Both handles then refer to the same inode (each in-core inode has a reference�
count indicating how many duplicated or inherited handles refer to the file).

If dup() is successful, a file handle is returned.  This opaque "token" is used�
for further access to the file, eg. by read(), write() or close().

; -----------------------------------------------------------------------------

int dup2 (int oldd, int newd);

Duplicates the given file handle.  The file handle must previously have been�
opened with open() or creat(), or created using dup() or dup2().  The caller�
specifies a particular location in the process's file table, using "newd".

If "newd" was previously open, it is closed using the close() system call. �
Then "oldd" is copied into "newd" in order to create the duplicate reference. �
Both handles then refer to the same inode (each in-core inode has a reference�
count indicating how many duplicated or inherited handles refer to the file).

If dup2() is successful, a file handle is returned.  This opaque "token" is�
used for further access to the file, eg. by read(), write() or close().  Of�
course, this return value is always equal to the value of "newd" passed in.

; -----------------------------------------------------------------------------

int execve (char *name, char *argv[], char *envp[]);

Loads a program from the disk and executes it.  The current user must have�
execute permission for the file specified by "name".  Information is passed to�
the executed program using the argv[] and envp[] arrays.  The execve() call�
does not return, because the program is loaded into the same memory as the�
caller of execve().  Instead, the executed program will execute forever, or at�
least until it calls execve() again or exits with the _exit system call.

The kernel allocates some temporary storage in order to save your argv[] and�
envp[] arrays while the new program is being loaded.  They are compacted into�
kernel memory, and then copied back out into user memory after loading the�
program.  The 'C' startup saves the envp[] pointer for use by the getenv() and�
setenv() library calls, and then passes the argv[] and argc into your main().

Note that execve() can return in one situation:  If the file can't be executed. �
In this case execve() returns -1 and errno gives the reason why.

; -----------------------------------------------------------------------------

int fork (void);

Creates a new process, by duplicating the current process.  This is the only�
way to create a new process under Unix.  The created process will return from�
fork() a value of 0, which tells the subsequent code that it should behave as�
the child of the fork() rather than the parent.  Often it would then execve().

The originally existing process will return from fork() a value indicating how�
to find the new process just created.  So subsequent code knows that it should�
behave as the parent of the fork() rather than the child.  Using the PID value�
returned, the process which forked is able to keep track of the progress of the�
child, and obtain an exitcode when the child exits.  See the wait() call.

All memory belonging to the process is copied.  Of course there must be enough�
free memory for the new process's code and data (32 kbytes).  It's also quite a�
lengthy operation, since the entire 32 kbytes must be copied byte for byte.

All file handles belonging to the process are duplicated.  This doesn't require�
any files to be opened in reality, instead the in-core inodes simply have their�
reference count increased by one, to show that someone has inherited a handle.

Of course, if there is not enough memory or there is no free process table�
entry available, fork() will fail and return -1.  Errno gives the reason why.

; -----------------------------------------------------------------------------

int fstat (int fd, void *buf);

Checks whether the file exists, and if so, returns information about it.  Note�
that the user doesn't need to have any access to the file in order to make this�
call.  The user only needs execute permission on the directory containing it.

Calling fstat does not update the file's last accessed time.  It's completely�
benign and therefore would be used by a directory listing program (for example)�
in preference to the stat() system call.  (Both return the same information).

The structure of "buf" is defined in types.h and contains the following fields:

/* data structure for stat() */
struct stat {			/* USER! Really only used by users */
	uint	st_dev; 	/* device number */
	uint	st_ino; 	/* inode number */
	mode_t	st_mode;	/* file mode */
	uint	st_nlink;	/* number of links */
	uint	st_uid; 	/* owner id */
	uint	st_gid; 	/* owner group */
	uint	st_rdev;	/* */
	blkoff_t st_size;	/* Nick changed off_t to blkoff_t file size */
	time_t	st_atime;	/* last access time */
	time_t	st_mtime;	/* last modification time */
	time_t	st_ctime;	/* file creation time */
};

The relevant types "mode_t", "blkoff_t" and "time_t" are defined by types.h:

typedef unsigned int mode_t;

typedef struct blkoff_t {
    uint o_blkno;      /* Block number */
    int  o_offset;     /* Offset within block 0-511 */
} blkoff_t;

/* file's timestamp structure (non UNIX-standard) */
typedef struct s_time {
	uint	t_time;
	uint	t_date;
} time_t;

; -----------------------------------------------------------------------------

int getfsys (int dev, void *buf);

Returns information about the filesystem mounted on the given device.  This is�
a copy of the superblock for the filesystem, and thus can be used by a utility�
such as "df" to show information about the free space on the filesystem.

; -----------------------------------------------------------------------------

int getgid (void);

Returns the GID or "group ID" of the current process.  This is a unique integer�
referring to a line in the file "/etc/group" and is initialised at login time.

; -----------------------------------------------------------------------------

int getpid (void);

Returns the PID or "process ID" of the current process.  This is a unique�
integer generated when a process forks and is allocated a process table entry.

; -----------------------------------------------------------------------------

int getppid (void);

Returns the PID or "process ID" of the parent process.  This is a unique�
integer generated at the time the parent process was created.  The parent�
process is the process which forked to create this (the current) process.

; -----------------------------------------------------------------------------

int getuid (void);

Returns the UID or "user ID" of the current process.  This is a unique integer�
referring to a line in the file "/etc/passwd" and is initialised at login time.

; -----------------------------------------------------------------------------

int geteuid (void);

Returns the effective UID or "user ID" of the current process.  This would�
normally be equal to the value returned by getuid(), except that the user may�
have executed a binary program with the SETUID bit set.  If so, the effective�
user ID will be the user ID of the owner of that program.  For example, when�
executing the "login" utility, the effective user ID is always "root".

; -----------------------------------------------------------------------------

int getegid (void);

Returns the effective GID or "group ID" of the current process.  This would�
normally be equal to the value returned by getgid(), except that the user may�
have executed a binary program with the SETGID bit set.  If so, the effective�
user ID will be the user ID of the group of that program.  For example, when�
executing email daemons, the effective group ID is often "mail".

; -----------------------------------------------------------------------------

int getprio (void);

Returns the priority of the current process.  This is an integer indicating how�
many system "ticks" should be allocated to the process each time it runs.  When�
this number of "ticks" expires each time, some other process is allowed to run.

; -----------------------------------------------------------------------------

int ioctl (int fd, int request, ...);

Allows arbitrary control of devices.  The handle "fd" must have been opened�
with the open() system call or created with dup() or dup2().  Only special�
files referring to devices can be manipulated with the ioctl() system call.

The caller must know what kind of device the handle refers to, for example a�
TTY device will respond to certain kinds of ioctl() requests, whereas a block�
device will respond to different ones.  Unix doesn't inspect the "request" or�
subsequent parameters given.  These are simply passed on to the device driver.

; -----------------------------------------------------------------------------

int kill (int pid, int sig);

Sends a signal to a process.  If the process does not catch the signal, it will�
be killed, hence the name "kill".  The kill() system call can also be used to�
communicate with a child in a more benign way.  The "pid" is often obtained as�
the return value from fork(), or else by using the getpid() system call.

The "sig" argument can be one of the following values defined by signal.h:

#define SIGHUP 1 		/* Hangup detected on controlling terminal
				   or death of a controlling process */
#define SIGINT 2 		/* Interrupt from keyboard */
#define SIGQUIT 3		/* Quit from keyboard */
#define SIGILL 4 		/* Illegal instruction */
#define SIGTRAP 5		/* Trace/breakpoint trap */
#define SIGIOT 6 		/* IOT trap. A synonym for SIGABRT */
#define SIGABRT SIGIOT		/* Abort signal from abort */
#define SIGUSR1 7		/* User's signal 1 */
#define SIGUSR2 8		/* User's signal 2 */
#define SIGKILL 9		/* Kill signal */
#define SIGPIPE 10		/* Broken pipe: write to pipe with no reader */
#define SIGALRM 11		/* Timer signal from alarm */
#define SIGTERM 12		/* Termination signal */
#define SIGURG 13		/* Urgent signal */
#define SIGCONT 14		/* Continue process */
#define SIGSTOP 15		/* Stop process */

/* this signals defined only for compatibility */
#define SIGBUS 16 		/* Bus error */
#define SIGFPE 17 		/* Floating point exception */
#define SIGSEGV 18		/* Invalid memory reference */
#define SIGSYS 19 		/* Bad argument to routine */
#define SIGTTIN 20
#define SIGTOUT 21

When any process sends a signal to a sleeping process, its state will change to�
P_READY.  As the process is now eligible, it will be selected in due course by�
the scheduler, at which time the signal is processed (its handler is called). �
If the signal handler subsequently returns, normal execution resumes for the�
process, just after the point where it went to sleep.

; -----------------------------------------------------------------------------

int link (char *oldname, char *newname);

Creates a new file in the directory structure, named "newname", which refers to�
the same inode as the already existing file named "oldname".  Note that both�
aliases really refer to the same file, so writing or appending to the file, or�
truncating it, will affect both files identically.

Linking is achieved by making the new file refer to the same "inode" as the�
original file, and increasing that inode's "link count" by one.  Directory�
entries consist of "name" and "inode".  When files are deleted (unlinked),�
inodes will not be destroyed until all of their aliases have been deleted.

; -----------------------------------------------------------------------------

int symlink (char *oldname, char *newname);

Similar to the link() function call, except that symlinks are easily identified�
as such, whereas the file created by the link() system call cannot easily be�
distinguished from the original.  Symlinks are more manageable for this reason.

Symlinks also have the property that they can traverse mount points and so they�
are useful when aliasing files on some other media (for example a network or�
removeable drive).  This property is achieved because the symlink is just a�
small file containing a short string which is appended to the path.  For�
example I could symlink "/usr/etc" to "../etc" and the symlink, when accessed,�
would correctly find the "/usr/../etc" directory, even if the /usr directory�
had been mounted from a completely different drive than the /etc directory.

If the original file is deleted or moved somewhere temporarily, the symlink is�
"broken" and thus can't be accessed.  (ENOENT errors would result).  However�
this can be a valuable property, as symlinks can be archived and used again�
later, and/or the original files could be temporarily deleted, and restored�
again later.  Symlinks only need to be valid at the time of opening a file.

; -----------------------------------------------------------------------------

int mknod (char *name, mode_t mode, int dev);

Creates a special device file.  This would usually be placed in the "/dev"�
subdirectory, although it can be placed anywhere.  The file doesn't contain any�
data, but its inode is marked as "special" by setting the "block special" or�
"character special" bits in the "mode".  The "mode" is the same 16-bit integer�
which holds the access permissions for "user, group, other" as described for�
the chmod() system call, but only the lower 12 bits are modifiable by chmod. �
The upper 4 bits must be set when the file is created using the mknod() call.

To make things easier, the following definitions are provided by sys/stat.h:

#define S_IFMT		0170000	/* file type mask */
#define S_IFLNK		0110000	/* symbolic link */
#define S_IFREG		0100000	/* or just 000000, regular */
#define S_IFBLK 	0060000	/* block special */
#define S_IFDIR 	0040000	/* directory */
#define S_IFCHR 	0020000	/* character special */
#define S_IFPIPE 	0010000	/* pipe */

See "c:\uzi\include\sys\stat.h" for further handy macros, for example the�
S_ISLNK macro returns true if some file's mode indicates a symbolic link.

In addition to providing a "mode" parameter which has the "block special" or�
"character special" bits set, the user also provides a device number in "major,�
minor" format.  The "major" device number indicates which device driver will�
perform the work.  The "minor" device number is passed in to this device driver�
to do what it wants with.  For example a hard disk device might provide "minor"�
device numbers of 0 and 1 for two different hard disks attached to the system. �
A TTY device driver might provide "minor" device numbers of 0 thru 3 for four�
different serial ports attached to the system.  So the creator of the device�
special files is going to need to know something about the underlying driver!

The following major and minor device number combinations have been allocated:

  /* minor  open     close     read      write      ioctl */
   /*-----------------------------------------------------*/
    { 0,  wd_open,     ok,    wd_read,  wd_write,   nogood },   /* 0 HD0 */
    { 0,  fd_open,     ok,    fd_read,  fd_write,   nogood },   /* 1 floppy */
    { 1,  wd_open,     ok,    wd_read,  wd_write,   nogood },   /* 2 HD1 */
    { 2,  wd_open,     ok,    wd_read,  wd_write,   nogood },   /* 3 Swap */
    { 0, lpr_open, lpr_close, nogood,   lpr_write,  nogood },   /* 4 printer */
    { 1, tty_open, tty_close, tty_read, tty_write, tty_ioctl }, /* 5 tty */
    { 0,     ok,       ok,      ok,     null_write, nogood },   /* 6 null */
    { 0,     ok,       ok,    mem_read, mem_write,  nogood },   /* 7 mem */
    { 0,  mt_open,  mt_close, mt_read,  mt_write,   nogood },   /* 8 mt */
    { 2, tty_open, tty_close, tty_read, tty_write, tty_ioctl }  /* 9 tty1 */
     /* Add more tty channels here if available, incrementing minor# */

See the file "c:\uzi\kernel\config.h" for the most up to date version of this.

; -----------------------------------------------------------------------------

int mkfifo (char *name, mode_t mode);

Creates a special "pipe" file.  This can be placed anywhere in the directory�
structure of the drive which is to hold the piped data.  The file is like a�
regular file, except it is always written by appending to the file, and is read�
from the start.  Blocks are freed from the start of the file as they are read.

The file is marked as a "pipe" by setting the "pipe" bit in the file's "mode". �
The "mode" is the same 16-bit integer which holds the access permissions for�
"user, group, other" as described for the chmod() system call, but only the�
lower 12 bits are modifiable by chmod.  The upper 4 bits must be set when the�
file is created using the mkfifo() call.  You must be sure to set the S_IFPIPE�
bit in the passed mode, and also any necessary access permissions for the pipe.

If any process tries to write to a pipe which doesn't have any handles open to�
read it, it will receive a signal which would normally kill the program.  The�
signal can be caught if you need to write to broken pipes for some reason.

; -----------------------------------------------------------------------------

int mount (char *spec, char *dir, int rwflag);

Mounts the filesystem found on a given device, onto a given mount point.  The�
device referred to by "spec" must be a special device file with the "block�
special" bit set.  The mount point referred to by "dir" must be an already�
existing directory (usually empty).  "rwflag" indicates whether the mounted�
filesystem should be writeable.  (If the filesystem is mounted read-only, not�
even the superuser is allowed to make changes).

The filesystem on the device must be an UZI filesystem.  This is verified by�
reading the superblock of the filesystem and checking for the correct "magic�
number" in the header.  If this is found, the root directory of the filesystem�
is opened and remains open.  This is saved in the "mounts" table so that any�
further system calls such as open() can correctly traverse the mount point.

; -----------------------------------------------------------------------------

int open (char *name, uint flag, ...);

Searches for a file of the given name, and if found, opens it in the given�
mode.  Mode can be any of the read or write modes such as O_RDONLY or O_RDWR.

If open() is successful, a file handle is returned.  This opaque "token" is�
used for further access to the file, eg. by read(), write() or close().  Each�
process has its own file table, so handles are not unique across processes.

To make things easier, the following definitions are provided by fcntl.h:

#define O_RDONLY	0
#define O_WRONLY	1
#define O_RDWR		2

/* Flag values for open only */
#define O_CREAT 	0x0100	/* create and open file */
#define O_TRUNC 	0x0200	/* open with truncation */
#define O_NEW		0x0400	/* create only if not exist */
#define O_SYMLINK	0x0800	/* open symlink as file */

/* a file in append mode may be written to only at its end. */
#define O_APPEND	0x2000	/* to end of file */
#define O_EXCL		0x4000	/* exclusive open */
#define O_BINARY	0x8000	/* not used in unix */

; -----------------------------------------------------------------------------

int pause (void);

Pauses the current process until a signal is received (any signal).  The�
process must have installed a signal handler for the anticipated signal,�
because the default behaviour would be to terminate the process on receipt.

While the current process is paused, any other eligible processes will run. �
When one of them sends a signal to the paused process, its state will change�
from P_PAUSE to P_READY.  As the process is now eligible, it will be selected�
in due course by the scheduler, at which time the signal is processed (its�
handler is called).  If the signal handler returns, normal execution resumes.

; -----------------------------------------------------------------------------

int pipe (int fildes[2]);

Creates an unnamed pipe on the root filesystem.  The pipe is a temporary file�
which is not in any directory, similar to the result of creating a file using�
creat(), using unlink() to delete it, then continuing to access the file.

When temporary files are created like this, all is well, but as there is no�
reference to the file anywhere in the directory structure, its inode and data�
space will be freed immediately on closure.

Separate file handles are returned for the "read" and "write" sides of the�
pipe.  Assuming the pipe can be created, fildes[0] is given a handle opened in�
O_RDONLY mode, and fildes[1] is given a handle opened in O_WRONLY mode.

If the operation fails, fildes[0] and fildes[1] are both set to -1, the return�
value is -1 and errno says why the pipe could not be created.

; -----------------------------------------------------------------------------

int read (int fd, void *buf, uint nbytes);

Reads from the given file handle.  The file handle must previously have been�
opened with open() or creat(), or created using dup() or dup2().  The handle�
can refer to a file, in which case bytes will be read from the current file�
position, and the file position will be incremented.  Or "fd" can refer to a�
device, in which case the bytes are read from the associated device driver.

Whether the handle refers to a file or a device, read() will read as much as�
possible, to address "buf" onwards, up to the limit specified by "nbytes".  The�
return value from read() is the actual number of bytes read, or -1 if some kind�
of catastrophic error occurred (eg. disk I/O error).  EOF is not considered an�
error, and simply results in less than the expected number of bytes being read.

; -----------------------------------------------------------------------------

int sbrk (uint incr);

Increases the top of the process's memory by the given amount.  On entry to the�
program, "brk" is set to the top of the loaded program image, and therefore�
must be adjusted slightly higher if the program is going to use extra memory�
for data.  This is normally done at least once, by the 'C' startup code, to�
inform the kernel of how much uninitialised data the program wishes to use.

The kernel checks if the "brk" value is getting too close to the process's�
stack pointer (ie. if there would not be enough stack space left to process a�
system call or an interrupt).  In this case the call fails and returns -1.

The 'C' library also provides a malloc() and free() implementation, which is�
based on the brk() and sbrk() system calls.  Normally sbrk() is called from�
within malloc() each time malloc() wishes to extend the size of the pool.  This�
might not be very often since the user might be requesting small blocks and�
freeing them often.  An appropriate increment is usally determined in advance.

Or, sbrk() can be used as a direct replacement for malloc(), since it returns�
the previous brk address (ie. the start of the extra portion just allocated).

; -----------------------------------------------------------------------------

int seek (int fd, int offset, int whence);

Sets the file position for the given file handle.  Ordinary files and devices�
can be seeked, but seek() will return an error if the file handle refers to a�
pipe or a file opened in append mode.  On failure, errno indicates why.

The file position is set absolutely (SEEK_OSET/SEEK_BSET), or relative to the�
current file position (SEEK_OCUR/SEEK_BCUR), or relative to the end of the file�
(SEEK_OEND/SEEK_BEND).  Negative offsets for relative seek are not implemented.

The "whence" argument can be one of the following values from seek.h:

#define SEEK_OSET 0
#define SEEK_OCUR 1
#define SEEK_OEND 2
#define SEEK_BSET 3
#define SEEK_BCUR 4
#define SEEK_BEND 5

Since a file can be larger than the integer value represented in "offset", it's�
necessary to set the file position in two steps.  Firstly set the byte position�
in the current block, using one of the SEEK_Oxxx calls.  Then set the block�
number using one of the SEEK_Bxxx calls.  The resulting file position can be�
inferred by recombining the return values from each function.  When converting�
block/offset to "long" or vice versa, use a hard coded block size of 512 bytes.

; -----------------------------------------------------------------------------

off_t lseek (int fd, off_t offset, int whence);

Not yet implemented by the kernel.  Instead it's done as a library call, based�
on the seek() function call which is implemented by the kernel.  As we can only�
pass 16-bit parameters into the kernel for the moment, and the return value can�
only be 16-bit as well, it's quite convenient to provide an lseek() routine in�
the 'C' library, which decodes the "off_t" value into sector and byte offsets.

The "whence" argument can be one of the following values from seek.h:

#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2

; -----------------------------------------------------------------------------

int setgid (int gid);

Sets the GID or "group ID" of the current process.  This is a unique integer�
referring to a line in the file "/etc/group" and is initialised at login time.

Superuser permissions are required to make this call.  Therefore the "login"�
utility usually runs "setuid" so that you can temporarily become root to do it.

; -----------------------------------------------------------------------------

int setuid (int uid);

Sets the UID or "user ID" of the current process.  This is a unique integer�
referring to a line in the file "/etc/passwd" and is initialised at login time.

Superuser permissions are required to make this call.  Therefore the "login"�
utility usually runs "setuid" so that you can temporarily become root to do it.

; -----------------------------------------------------------------------------

int setprio (int pid, char prio);

Sets the priority of the current process.  This is an integer indicating how�
many system "ticks" should be allocated to the process each time it runs.  When�
this number of "ticks" expires each time, some other process is allowed to run.

; -----------------------------------------------------------------------------

sig_t signal (signal_t sig, sig_t func);

Install a signal handler to catch a particular signal.  Signals can be sent by�
any process, by device drivers, or by the kernel in response to certain events. �
The default behaviour on receipt of a signal is to kill the receiving process,�
but if the process has "caught" the signal by calling signal() to register a�
handler for the anticipated signal, the given signal handler executes instead.

The "sig" argument can be one of the following values defined by signal.h:

#define SIGHUP 1 		/* Hangup detected on controlling terminal
				   or death of a controlling process */
#define SIGINT 2 		/* Interrupt from keyboard */
#define SIGQUIT 3		/* Quit from keyboard */
#define SIGILL 4 		/* Illegal instruction */
#define SIGTRAP 5		/* Trace/breakpoint trap */
#define SIGIOT 6 		/* IOT trap. A synonym for SIGABRT */
#define SIGABRT SIGIOT		/* Abort signal from abort */
#define SIGUSR1 7		/* User's signal 1 */
#define SIGUSR2 8		/* User's signal 2 */
#define SIGKILL 9		/* Kill signal */
#define SIGPIPE 10		/* Broken pipe: write to pipe with no reader */
#define SIGALRM 11		/* Timer signal from alarm */
#define SIGTERM 12		/* Termination signal */
#define SIGURG 13		/* Urgent signal */
#define SIGCONT 14		/* Continue process */
#define SIGSTOP 15		/* Stop process */

/* this signals defined only for compatibility */
#define SIGBUS 16 		/* Bus error */
#define SIGFPE 17 		/* Floating point exception */
#define SIGSEGV 18		/* Invalid memory reference */
#define SIGSYS 19 		/* Bad argument to routine */
#define SIGTTIN 20
#define SIGTOUT 21

The "func" argument is a function pointer, but the sig_t type is provided by�
types.h for convenience.  The definition is:  typedef void (*sig_t) (signal_t); �
which means that the signal handler will be told which signal it's handling, as�
its first argument is a "signal_t", and the function does not return a value.

In fact the signal handler doesn't need to return at all, it may exit using the�
exit() call, or restore execution to an earlier saved state using the longjmp()�
library routine.  If the signal handler does return, then execution continues�
in the process which recieved the signal.  (If it was asleep, it will have been�
woken at the time the signal was originally sent, so it now executes again).

; -----------------------------------------------------------------------------

int stat (char *path, void *buf);

Checks whether the file exists, and if so, returns information about it.  Note�
that the user does need to have read access to the file in order to make this�
call.  The user also needs execute permission on the directory containing it.

Calling stat will update the file's last accessed time.  It should therefore be�
used with caution by directory listing or disk cataloguing programs.  For most�
purposes you would not want to corrupt the file's last-access time unless you�
were about to access the file yourself.  Use fstat() for a non-intrusive call.

; -----------------------------------------------------------------------------

int stime (time_t *tvec);

Not yet implemented.  Sets the system real time clock.  The buffer pointed to�
by "tvec" contains the time and date.  Superuser permission is required to set�
the system real time clock.

time_t is currently in the following format, defined by types.h:

/* file's timestamp structure (non UNIX-standard) */
typedef struct s_time {
	uint	t_time;
	uint	t_date;
} time_t;

; -----------------------------------------------------------------------------

int sync (void);

Synchronises the file system, possibly in preparation for a reboot or system�
shutdown (or just to ensure that an important file's data is really flushed).

The kernel writes any modified inodes to disk (for example, updating the length�
of any open files to the correct value), updates the superblock (this ensures�
the 'blocks free' and 'blocks used' counters are at their correct values), then�
finally any unwritten disk buffers are written out to disk before returning.

; -----------------------------------------------------------------------------

time_t time (time_t *tvec);

Returns the system real time clock.  The buffer pointed to by "tvec" receives�
the time.  No special permissions are required to do this.

time_t is currently in the following format, defined by types.h:

/* file's timestamp structure (non UNIX-standard) */
typedef struct s_time {
	uint	t_time;
	uint	t_date;
} time_t;

; -----------------------------------------------------------------------------

int times (struct tms *buf);

Returns information about the running time of the current process and its�
children.  The information is returned in a structure containing various�
fields.  The fields of the structure are as follows, defined by types.h:

/* User's structure for times() system call */
struct tms {
	time_t	tms_utime;      /* Elapsed ticks in user mode */
	time_t	tms_stime;      /* Elapsed ticks in system mode */
	time_t	tms_cutime;     /* Total childrens ticks (user mode) */
	time_t	tms_cstime;     /* Total childrens ticks (system mode) */
	time_t	tms_etime;	/* Elapsed real time */
};

time_t is currently in the following format, defined by types.h:

/* file's timestamp structure (non UNIX-standard) */
typedef struct s_time {
	uint	t_time;
	uint	t_date;
} time_t;

; -----------------------------------------------------------------------------

int utime (char *path, struct utimbuf *buf);

Checks whether the file exists, and if so, returns information about it.  Note�
that the user doesn't need to have any access to the file in order to make this�
call.  The user only needs execute permission on the directory containing it.

The structure of "buf" is defined in types.h and contains the following fields:

/* User's structure for utime() system call */
struct utimbuf {
	time_t	actime;			/* last accessed time */
	time_t	modtime;		/* last modified time */
};

time_t is currently in the following format, defined by types.h:

/* file's timestamp structure (non UNIX-standard) */
typedef struct s_time {
	uint	t_time;
	uint	t_date;
} time_t;

; -----------------------------------------------------------------------------

int umask (int mask);

Sets the default file creation permissions for the current process (this�
setting will also be inherited by any child processes).

The argument to umask() is specified in 'inverse', for example if some file was�
to be created with octal permissions '755' you would call umask(022) followed�
by creat().  Thus the argument to umask() tells the system which bits to turn�
off, hence the name "mask".  The usual value of 022 specifies that new files�
should not be writeable by "group" and "other" users, but only by the owner.

; -----------------------------------------------------------------------------

int umount (char *spec);

Unmounts the filesystem found on a given device, from its mount point.  Only�
the device containing the filesystem needs to be specified.  The device�
referred to by "spec" must be a special device file with the "block special"�
bit set.  The filesystem must earlier have been mounted using the mount()�
system call.  It is not possible to unmount the root filesystem.

The filesystem on the device must be an UZI filesystem.  The call will fail if�
any files on the device are still open.  The root directory of the filesystem,�
whose inode is obtained from the mounts table, is closed.  All unwritten data,�
including modified inodes and changes to the superblock, are written out.  The
mounts table is updated to show that the filesystem is no longer mounted.

; -----------------------------------------------------------------------------

int unlink (char *path);

Searches for the given path.  If the file is found, and the user has write�
permission for the directory containing the file, the found entry is removed�
from the directory structure.

In the process the directory entry is inspected.  Directory entries consist of�
"name" and "inode".  The associated inode is also inspected, and its "link�
count" is decreased by one.  The inode will also be destroyed, if its "link�
count" has reached zero, meaning all aliases to the file have been unlinked.

; -----------------------------------------------------------------------------

int wait (int *statloc);

Waits for a child process to die (any child of the current process).  If there�
are no children, wait() returns an error ECHILD.  If there are some children,�
the current process goes to sleep having indicated it wants to be notified when�
a child exits.  Some other eligible program runs, and eventually, if a child�
does exit, the sleeping process is woken up and wait() returns the child's PID. �
At the buffer pointed to by statloc, the process receives the child's exitcode.

; -----------------------------------------------------------------------------

int write (int fd, void *buf, uint nbytes);

Writes to the given file handle.  The file handle must previously have been�
opened with open() or creat(), or created using dup() or dup2().  The handle�
can refer to a file, in which case bytes will be written to the current file�
position, and the file position will be incremented.  Or "fd" can refer to a�
device, in which case the bytes are written to the associated device driver.

Whether the handle refers to a file or a device, write() will write as much as�
possible, using data from address "buf" onwards, up to the limit specified by�
"nbytes".  The return value from write() is the actual number of bytes written,�
or -1 if some kind of catastrophic error occurred (eg. disk I/O error).

; -----------------------------------------------------------------------------

int reboot (char p1, char p2);

Not yet implemented.  This call would be passed two special values, p1 = 'm'�
and p2 = 'e'.  The parameters are checked to guard against accidental reboots.

Only the superuser is allowed to reboot the system.  The reboot() system call�
would be used by a shutdown program, so that you could type "shutdown -r now".

; -----------------------------------------------------------------------------

int systrace (int onoff);

Enables or disables system call tracing.  When system call tracing is active�
for a process, the kernel will print diagnostic log information to the console,�
so that you can see exactly which system calls a program is making, and which�
ones were successful and which ones were not.  This system call might be used�
by a version of the "systrace" program, which turns on system tracing, executes�
a command line given by the user, and then turns off tracing before exiting.

; -----------------------------------------------------------------------------

