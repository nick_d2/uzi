HYTECH-INTERNATIONAL BV
CMX/UZI TESTRELEASE FOR SOFTWARE DEPARTMENT 030131SD

MANIFEST

-rw-r--r--    1 nick     users     6986817 Jan 31 23:06 uzi.zip

INTRODUCTION

Full release of the Hytech CMX/UZI kernel and associated utilities.

Note:  This doesn't contain Joost's userlevel work at the moment.  However we
do have a small "helloworld" program which compiles to /usr/bin/hello, Nick
uses this to test.

CHANGES IN THIS RELEASE

Contains a major filesystem upgrade - the superblock now begins at block 4,
rather than block 1.  This is partway towards implementing a RAMdrive in the
terminal's internal memory.  Utilities which construct and maintain the
filesystem are also completely new versions, with much better error checking
and help messages.  These utilities are now available at the terminal level
(also partway towards implementing the RAMdrive).

In the kernel itself, symbolic linking is now implemented.  The kernel also has
much better error checking and messages.  In the C library, a bug is fixed in
the creat() function (the bug caused it to pass garbage for the filename, so
that an EINVAL message was always returned).  This means shell redirections now
work again.

When building the filesystem, you should now use /uzi/bin/n.bat rather than the
former loadall.bat - the contents have been revised so that the filesystem is
now built from scratch every time, rather than relying on a pre-initialised
image in /uzi/utils.  The filesystem is now checked after copying the necessary
files into the uzidisk.dat image.

Note:  HD1_START has been set to 0x239.  For Joost's system this may need to be
changed to 0x259.  If possible, please advise the starting position for a brand
new flash card!

Note:  When prompted with "boot: " at startup, you must type 2.  The RAMdrive
is device 0 (but this won't boot until it's been formatted using mkfs and other
steps performed).  Similarly in the UCP program you need to type "root c:"
which translates to device 2.

