
This is a full uzi build.

It is basically a further development of the 030528 release
where the major differences are in changes and additions to the
testsystem.

/uzi/src/testsystem/tter

This program runs in a UNIX environment (Cygwin is OK too)
and controls the recording and playback of the terminal,
see tter -? for more. The baudrate is current default at 38400

/uzi/src/kernel/uzi

Here are the changes to the kernel regarding the testsystem.
Files:
testsystem.c
testsystem.h
touchscreen.c
touchscreen.h
devno.h


The testsystem is hardcoded to record/playback on Serial 3

The kernel currently comes alive with the testsystem enabled. This
is for now hardcoded and will be changed in the future to be enabled
or disabled at will.

This means that currently all device output is redirected to Serial 3
out and if "tter -r" is listening, is recorded. The recorded data is
tagged with the device id (UZI minor device number) of its source.
Format: ESC <minor dev id>

Playback with "tter -p" will redirect the input received on Serial 3
as if received from the devices concerned.

Commands currently supported via Serial 3 input:
ESC R	Toggles RTC of terminal between internal source (defaulr) or
	external source
ESC r	Toggles on/off internal source RTC to be routed to Serial 3 
	output in order to be recorded

Note that ESC in the to be recorded data stream is always escaped
(ESC becomes ESC ESC)

Known problems:
- <backspace> on the keyboard of the hyperterminal causes an interrupt
  interference with the testsystem_rx_handler on Serial 3 input. Symptom:
  hyperterminal shows: testsystem_rx_handler: ERROR port = .., c = ..
- The testsystem is quite slow during playback. One of the reasons is
  that each byte during recording is taggged with the device id, thus
  causes the amount of data to be processed to be trippled. The reason
  this is that "remembering" the currently active device and tag the
  data only when it changes, causes conflicts between multiple interrupts
  happening at the same time. In fact some mutex needs to be used
  to protect critical data in testsystem_rx_hook. This will be implemented
  in the next release.

