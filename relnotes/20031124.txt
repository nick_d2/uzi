HYTECH INTERNATIONAL BV
NOS/UZI RELEASE 031124SD

MANIFEST

-rw-r--r--   1406578 Nov 24 17:02 uzi031112-uzi031124-upd.zip*

INTRODUCTION

This is an update to the 031112SD release containing some more bugfixes:
- changed the memory threshold to 1024 bytes from 2048, see
"nos/include/nos/config.h"
- large mbuf size reduced to 512 from 2048, see "nos/net/mbuf.c"
- communication speed on SERIAL 2/3 changed to 57600 from 9600 bps, see
"nos/z80/escc.c" and "bin/root=hd0"
- fixed a memory leak on closing of sockets, see "nos/main/auto.mk"
- memory statistics changed so allocations don't include failed ones,
see "nos/net/alloc.c", this allows us to detect leaks via "memory stat"
- fixed a leak in the "ping" command that didn't close its IP socket if
the ping was aborted (perhaps due to a non-resolvable ping address).
See "nos/internet/ping.c"
- "nos/internet/icmp.c" changed slightly, in white space only
- "nos/internet/ip.c" has some abytes which have been commented again
- "nos/internet/tcpuser.c" has some abytes which have been commented again
- since the recent change to "nos/intl/intl.c" which allows _dprintf to work
on strings from the stringtable without a fatal infinite recursion, we found
a better solution:  references to _dprintf() in alloc.c have been changed to
dprintf(), allowing us to reverse (partially) the changes to "intl.c".
- because of the previous item, "nos/net/alloc.c" changed, in particular the
modules "alloc", "alloc_mbuf", "ambufw", "dup_p" and "free_mbuf".
- changes to comments in "nos/z80/esccvec.S"
- changes to the "nos/z80/escc.c" due to a redundant programming of the baud
rate that happened during a clock tick.  This initially caused the "root=hd0"
baudrate to be overridden after one clock tick.  The calculation is always
with reference to a *16 clock (fixed the erroneous selection of *1 or *64).
The change affects modules "escc_sioctl" N/A, "escc_txoff" and "escc_txon".
- changes to whitespace in "nos/net/socket.c" and "nos/net/timer.c".
- changes to whitespace in "nos/ppp/slip.c"
- in "nos/main/config.c" the garbage collection function (gcollect) is
removed temporarily.  Tests of transferring "kernel.bin" back and forth are
now working, with no garbage collection, so we left it out for the moment.

INSTALLATION INSTRUCTIONS

1.  In "c:/cygwin/uzi" unpack "uzi.zip" from the "030704SD" webfolder.
2.  In "c:/cygwin/uzi" unpack "uzipatch.zip" from the "030827SD" webfolder.
3.  In "c:/cygwin/uzi" unpack "uzipatch.zip" from the "031003SD" webfolder.
4.  In "c:/cygwin/uzi" execute "uzi031003-uzi031112-del.sh" from "031112SD".
    Note:  The first line of the script contains "#!/usr/bin/bash", this was
    necessary on Nick's system to get around a problem of mismatched Cygwin
    versions.  Your mileage may differ.  (It works OK for Rob Buijs though).
5.  In "c:/cygwin/uzi" unpack "uzi031003-uzi031112-upd.zip" from "031112SD".
6.  In "c:/cygwin/uzi" unpack "uzi031003-uzi031112-new.zip" from "031112SD".
7.  In "c:/cygwin/uzi" unpack "uzi031112-uzi031124-upd.zip" from this release.

SUMMARY OF SIGNIFICANT FILES

Existing files revised with this release:  (See "*-upd.zip")

bin/checksum
bin/kernel.bin
bin/root=hd0
bin/uzboot.bin
bin/uzboot.gz
bin/uzidisk.dat
src/nos/bin/net.bin
src/nos/include/nos/config.h
src/nos/internet/icmp.c
src/nos/internet/ip.c
src/nos/internet/ping.c
src/nos/internet/tcpuser.c
src/nos/intl/intl.c
src/nos/lib/internet_lib/ping.rel
src/nos/lib/intl_lib/_vdprintf.rel
src/nos/lib/net_lib/alloc.rel
src/nos/lib/net_lib/alloc_mbuf.rel
src/nos/lib/net_lib/ambufw.rel
src/nos/lib/net_lib/dup_p.rel
src/nos/lib/net_lib/free_mbuf.rel
src/nos/lib/z80_lib/escc_sioctl.rel
src/nos/lib/z80_lib/escc_txoff.rel
src/nos/lib/z80_lib/escc_txon.rel
src/nos/main/auto.mk
src/nos/main/config.c
src/nos/net/alloc.c
src/nos/net/mbuf.c
src/nos/net/socket.c
src/nos/net/timer.c
src/nos/ppp/slip.c
src/nos/z80/escc.c
src/nos/z80/esccvec.S

