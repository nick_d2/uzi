#ifndef __FCNTL_H
#define __FCNTL_H

#ifndef __TYPES_H /* Nick, for __P and uint definitions */
#include <sys/types.h>
#endif

#define O_RDONLY	0
#define O_WRONLY	1
#define O_RDWR		2

/* Flag values for open only */
#define O_CREAT 	0x0100	/* create and open file */
#define O_TRUNC 	0x0200	/* open with truncation */
#define O_NEW		0x0400	/* create only if not exist */
#define O_SYMLINK	0x0800	/* open symlink as file */

/* a file in append mode may be written to only at its end. */
#define O_APPEND	0x2000	/* to end of file */
#define O_EXCL		0x4000	/* exclusive open */
#define O_BINARY	0x8000	/* not used in unix */

#ifndef SEEK_SET
#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2
#endif

#ifndef SKIP_SYSCALLS
/* extra prototypes added by Nick */
/* just to make extra certain IAR doesn't use regparms */
extern int ioctl __P((int fd, int request, ...));
extern int open __P((const char *name, int flag, ...)); /* Nick uint */
#endif

#endif /* __FCNTL_H */
