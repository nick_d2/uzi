/*	setjmp.h

	Defines typedef and functions for setjmp/longjmp.

	Copyright (c) Borland International 1987,1988
	All Rights Reserved.
*/
#if 1 /* Nick __STDC__ */
#define _Cdecl
#else
#define _Cdecl	cdecl
#endif

#ifndef _SETJMP
#define _SETJMP
typedef struct {
	unsigned int	j_sp;
	/* unsigned int	j_ss; */ /* this will be ignored for large model */
	unsigned int	j_bc;
	unsigned int	j_ix;
	unsigned int	j_iy;
	unsigned int	j_ip;
	/* unsigned int	j_cs; */ /* this will be ignored for large model */
	unsigned char	j_cs; /* this will be ignored for large model */
}	jmp_buf[1];

void	_Cdecl longjmp(jmp_buf jmpb, int retval);
int	_Cdecl setjmp(jmp_buf jmpb);
#endif

