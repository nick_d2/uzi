#ifndef __TYPES_H
#define __TYPES_H

#ifndef __FEATURES_H
#include "features.h"
#endif

/* USER! basic data types */
/* ! uchar & uint is counterparts and must be declared simultaneously */
#ifndef uchar_is_defined
#define uchar_is_defined
typedef unsigned char uchar;
typedef unsigned int uint;
#endif

typedef uchar bool_t;	/* boolean value */
typedef uint count_t;	/* counter for anything */
#ifndef _SIZE_T
#define _SIZE_T
typedef uint size_t;
#endif
#ifndef _SSIZE_T
#define _SSIZE_T
typedef int ssize_t;
#endif

typedef unsigned int mode_t;
#if 0 /* Nick, see unix.h !! */
typedef struct blkoff_t {
    uint/*16*/ o_blkno;      /* Block number */
    int/*16*/  o_offset;     /* Offset within block 0-511 */
} blkoff_t;
#endif
typedef long off_t;

#if 1 /* halfway compatibility measure by Nick, the struct is the same size */
/* file's timestamp structure (non UNIX-standard) */
typedef struct s_time {
	uint	t_time;
	uint	t_date;
} dostime_t;
#endif
typedef unsigned long time_t;

/* User's structure for times() system call */
struct tms {
	time_t	tms_utime;
	time_t	tms_stime;
	time_t	tms_cutime;
	time_t	tms_cstime;
	time_t	tms_etime;	/* Elapsed real time */
};

#ifndef utimbuf_is_defined
#define utimbuf_is_defined
/* User's structure for utime() system call */
struct utimbuf {
	time_t	actime;
	time_t	modtime;
};
#endif

/* data structure for stat() */
struct stat {			/* USER! Really only used by users */
	uint	st_dev; 	/* device number */
	uint	st_ino; 	/* inode number */
	mode_t	st_mode;	/* file mode */
	uint	st_nlink;	/* number of links */
	uint	st_uid; 	/* owner id */
	uint	st_gid; 	/* owner group */
	uint	st_rdev;	/* */
	off_t	st_size;	/* Nick changed blkoff_t to off_t file size */
	time_t	st_atime;	/* last access time */
	time_t	st_mtime;	/* last modification time */
	time_t	st_ctime;	/* file creation time */
};

#define DIRNAMELEN	14

/* device directory entry */
typedef struct direct {
	unsigned int d_ino;		/* file's inode */
	uchar	d_name[DIRNAMELEN];	/* file name */
} direct_t;

#ifndef NULL
#define NULL		0
#endif

#if 0 /* Nick see z80/asm.h */
#define BUFSIZE 	512	/* uzix buffer/block size */
#define BUFSIZELOG	9	/* uzix buffer/block size log2 */

#if 1 /* Nick free bitmap */
#define REGION_LOG 14
#define REGION_BYTES (1<<REGION_LOG)
#define REGION_BLOCKS (REGION_BYTES/BUFSIZE)

#define PAGE_LOG 12
#define PAGE_BYTES (1<<PAGE_LOG)
#define PAGE_BLOCKS (PAGE_BYTES/BUFSIZE)
#endif
#endif

#ifndef BUFSIZ
#ifndef ZILOG /*PC_HOSTED*/
#define BUFSIZ		512	/* stdio buffer size */
#else
#define BUFSIZ		128	/* 512 is too much. MSX memory is little. */
#endif
#endif

#define PATHLEN 	256

#ifndef __BUFPOOL_H
/* extra hacks by Nick to get ls.c to compile, copied from "uzi/bufpool.h" */
typedef unsigned short ino_t;	/* Can have 65536 inodes in fs */
typedef unsigned short blkno_t;	/* Can have 65536 BUFSIZE-byte blocks in fs */
typedef unsigned short dev_t;	/* Device number */

#define MINOR(dev)		((uchar)(dev))
#define MAJOR(dev)		((uchar)((dev) >> 8))
#define MKDEV(major, minor)	(((uint)(uchar)(major) << 8) | (uchar)(minor))
#endif

#endif
