/* global include file for all .S files to specify model and define macros */

/* ------------------------------------------------------------------------- */
/* Z180 register definitions: */

#define CNTLA0		0x00		/* ASCI Control Reg A Ch 0 */
#define CNTLA1		0x01		/* ASCI Control Reg A Ch 1 */
#define CNTLB0		0x02		/* ASCI Control Reg B Ch 0 */
#define CNTLB1		0x03		/* ASCI Control Reg B Ch 1 */
#define STAT0		0x04		/* ASCI Status Reg Ch 0 */
#define STAT1		0x05		/* ASCI Status Reg Ch 1 */
#define TDR0		0x06		/* ASCI Tx Data Reg Ch 0 */
#define TDR1		0x07		/* ASCI Tx Data Reg Ch 1 */
#define RDR0		0x08		/* ASCI Rx Data Reg Ch 0 */
#define RDR1		0x09		/* ASCI Rx Data Reg Ch 1 */

#define CNTR		0x0a		/* CSI/O Control Reg */
#define TRDR		0x0b		/* CSI/O Tx/Rx Data Reg */

#define TMDR0L		0x0c		/* Timer Data Reg Ch0-Low */
#define TMDR0H		0x0d		/* Timer Data Reg Ch0-High */
#define RLDR0L		0x0e		/* Timer Reload Reg Ch0-Low */
#define RLDR0H		0x0f		/* Timer Reload Reg Ch0-High */
#define TCR		0x10		/* Timer Control Reg */
#define TMDR1L		0x14		/* Timer Data Reg Ch1-Low */
#define TMDR1H		0x15		/* Timer Data Reg Ch1-High */
#define RLDR1L		0x16		/* Timer Reload Reg Ch1-Low */
#define RLDR1H		0x17		/* Timer Reload Reg Ch1-High */
#define FRC		0x18		/* Free-Running Counter */

#define SAR0L		0x20		/* DMA Source Addr Reg Ch0-Low */
#define SAR0H		0x21		/* DMA Source Addr Reg Ch0-High */
#define SAR0B		0x22		/* DMA Source Addr Reg Ch0-Bank */
#define DAR0L		0x23		/* DMA Dest Addr Reg Ch0-Low */
#define DAR0H		0x24		/* DMA Dest Addr Reg Ch0-High */
#define DAR0B		0x25		/* DMA Dest ADDR REG CH0-Bank */
#define BCR0L		0x26		/* DMA Byte Count Reg Ch0-Low */
#define BCR0H		0x27		/* DMA Byte Count Reg Ch0-High */
#define MAR1L		0x28		/* DMA Memory Addr Reg Ch1-Low */
#define MAR1H		0x29		/* DMA Memory Addr Reg Ch1-High */
#define MAR1B		0x2a		/* DMA Memory Addr Reg Ch1-Bank */
#define IAR1L		0x2b		/* DMA I/O Addr Reg Ch1-Low */
#define IAR1H		0x2c		/* DMA I/O Addr Reg Ch2-High */
#define BCR1L		0x2e		/* DMA Byte Count Reg Ch1-Low */
#define BCR1H		0x2f		/* DMA Byte Count Reg Ch1-High */
#define DSTAT		0x30		/* DMA Status Reg */
#define DMODE		0x31		/* DMA Mode Reg */
#define DCNTL		0x32		/* DMA/Wait Control Reg */

#define IL		0x33			/* INT Vector Low Reg */
#define ITC		0x34		/* INT/TRAP Control Reg */
#define RCR		0x36		/* Refresh Control Reg */
#define CBR		0x38		/* MMU Common Base Reg */
#define BBR		0x39		/* MMU Bank Base Reg */
#define CBAR		0x3a		/* MMU COmmon/Bank Area Reg */
#define ICR		0x3f		/* I/O Control Reg */

/* Z180 register bit definitions: */

#define INT0SW		0x01		/* interrupt enable bits in ITC */
#define INT1SW		0x02		/* interrupt enable bits in ITC */
#define INT2SW		0x04		/* interrupt enable bits in ITC */

/* S180 additional registers: */

#define CCR		0x1f		/* CPU Control Reg */
#define OMCR		0x3e		/* Operation Mode Control Reg */

/* Z182 additional registers: */

#if 0
#define BRK0		0x12		/* Break Control Reg Ch 0 */
#define BRK1		0x13		/* Break Control Reg Ch 1 */

#define WSGCS		0xd8		/* Wait-State Generator Chip Select */
#define ENH182		0xd9		/* Z80182 Enhancements Reg */
#define INTTYPE		0xdf		/* Interrupt Edge/Pin MUX Reg */
#define PINMUX		0xdf		/* Interrupt Edge/Pin MUX Reg */
#define RAMUBR		0xe6		/* RAM End Boundary */
#define RAMLBR		0xe7		/* RAM Start Boundary */
#define ROMBR		0xe8		/* ROM Boundary */
#define FIFOCTL		0xe9		/* FIFO Control Reg */
#define RTOTC		0xea		/* Rx Time-Out Time Constant */
#define TTOTC		0xeb		/* Tx Time-Out Time Constant */
#define FCR		0xec		/* FIFO Register */
#define SCR		0xef		/* System Pin Control */
/* (MIMIC Registers occupy 0xf0-0xff if used) */

#define DDRA		0xed		/* PIO Direction Reg Port A */
#define DRA		0xee		/* PIO Data Port A */
#define DDRB		0xe4		/* PIO Direction Reg Port B */
#define DRB		0xe5		/* PIO Data Port B */
#define DDRC		0xdd		/* PIO Direction Reg Port C */
#define DRC		0xde		/* PIO Data Port C */

#define SCCACNT		0xe0		/* ESCC Control Channel A */
#define SCCAD		0xe1		/* ESCC Data Channel A */
#define SCCBCNT		0xe2		/* ESCC Control Channel B */
#define SCCBD		0xe3		/* ESCC Data Channel B */
#endif

/* ------------------------------------------------------------------------- */
/* Hytech specific hardware:  (For the WPO30-V5, Hytech 1000, Hytech 1500) */

#define BC8530		0x214		/* channel B control in 8530 */
#define AC8530		0x215		/* channel A control in 8530 */
#define BD8530		0x216		/* channel B data in 8530 */
#define AD8530		0x217		/* channel A data in 8530 */

/* ------------------------------------------------------------------------- */
/* Filesystem configuration:  (These constants are needed at boot time) */

#define LDFILE		0xf037		/* BOOTLDR.BIN entry point */

#define SUPERBLOCK	4		/* starting position of filesystem */
#define SMOUNTED	12742		/* random number to flag mounted fs */
#define ROOTINODE	1		/* root inode # for all mounted fs */

#define BUFSIZE		0x200		/* count of bytes per disk block */
#define BUFSIZELOG	9		/* shift count representing above */

#define SIZEOF_DINODE_T	64		/* count of bytes per disk inode */
#define DINODESPERBLOCK	8		/* count of disk inodes per block */
#define DINODESPERBLOCKLOG 3		/* shift count representing above */

#define DIRECTBLOCKS	18		/* count of direct block #s in inode */
#define INDIRECTBLOCKS	1		/* MUST BE 1! */
#define DINDIRECTBLOCKS 1		/* MUST BE 1! */
#define TOTALREFBLOCKS	(DIRECTBLOCKS+INDIRECTBLOCKS+DINDIRECTBLOCKS)

#define SIZEOF_DIRECT_T	16		/* count of bytes per dir entry */
#define DIRECTNAMELEN	14		/* how many of these are dir name */
#define DIRECTPERBLOCK	(BUFSIZE/SIZEOF_DIRECT_T)

#define REGION_LOG 	14		/* file alignment uses 16kbyte units */
#define REGION_BYTES	(1<<REGION_LOG)
#define REGION_BLOCKS	(REGION_BYTES/BUFSIZE)

#define WINDOW_LOG 	12		/* virtual memory uses 4kbyte units */
#define WINDOW_BYTES	(1<<WINDOW_LOG)
#define WINDOW_BLOCKS	(WINDOW_BYTES/BUFSIZE)

#define PAGE_LOG	12		/* units start on 4kbyte boundaries */
#define PAGE_BYTES	(1<<PAGE_LOG)
#define PAGE_BLOCKS	(PAGE_BYTES/BUFSIZE)

#define HD0_START	0x200		/* RAM volume starts at abs 4:0000 */
#define HD0_SIZE	0x600		/* RAM volume top location is F:FFFF */

#define HD1_START	0x59		/* CompactFlash volume starts at LBA */
#define HD1_SIZE	0x2000		/* CompactFlash volume size LBA sect */

#if 1 /* new kernel format */
#define E_MAGIC		0xa6c9		/* random number for executable file */

#define E_FORMAT_LARGE 1
#define E_FORMAT_BANKED 2
#define E_FORMAT_KERNEL	3		/* means the executable is a kernel */
#endif

/* ------------------------------------------------------------------------- */
/* Virtual memory configuration:  (These constants are needed at boot time) */

#define BA_START	0x1000
#define CA1_START	0xf000

/*#define DATA_START	BA_START*/
/*#define DATA_LIMIT	CA1_START*/

/*#define CODE_START	CA1_START*/
/*#define CODE_LIMIT	0x10000*/

#define KRNL_START	0
#define KRNL_LIMIT	CA1_START /*(BA_START-0x100)*/
#define KRNL_TABLE	0 /*KRNL_LIMIT*/
#define KRNL_THRESHOLD	0x80

#define	USER_TABLE	BA_START /*DATA_START*/
#define USER_START	(BA_START+0x100) /*(DATA_START+0x100)*/
#define USER_LIMIT	CA1_START /*DATA_LIMIT*/
#define USER_THRESHOLD	0x80

/*#define BBR_REFERENCE	(USER_TABLE+0xfc)*/
/*#define CBR_REFERENCE	(USER_TABLE+0xfe)*/

/* ------------------------------------------------------------------------- */

#define DEFAULT_STACK	0x1000		/* default stack size for exe header */
#define KERNEL_STACK	0x400 		/* minimal stack size for NOS main() */
#define MINIMAL_STACK	0x100		/* minimal stack size for bootstrap */

#define	STACKPAT	0x55aa		/* initial value for stack (words) */

/* ------------------------------------------------------------------------- */
/* definitions for use by interrupt handlers, must be up to date with nos!!! */

#define SIZEOF_STRUCT_DMA (2+2+1)
#define SIZEOF_STRUCT_FIFO (2+2+2+2+2+2+4)
#define SIZEOF_STRUCT_API \
	(2+SIZEOF_STRUCT_FIFO+2+SIZEOF_STRUCT_DMA+3+3+4+4+4+4+4+4+4+4+2+2)

#define STRUCT_API_fifo 2
#define STRUCT_API_dma (2+SIZEOF_STRUCT_FIFO+2)

#define STRUCT_DMA_cnt 2
#define STRUCT_DMA_busy (2+2)

#define STRUCT_FIFO_bufsize 2
#define STRUCT_FIFO_cnt (2+2+2+2)

#define API_DIV 2	/* power of 2, higher = slower (controls csi/o rate) */
#define API_MAX 9	/* number of apibus structures (controls addressing) */

/* ------------------------------------------------------------------------- */

