/* libintl.h by Nick for NOS/UZI project */

#ifndef __LIBINTL_H
#define	__LIBINTL_H

#ifdef ZILOG

#include <stdio.h>
#include <stdarg.h>

typedef void _char(void);
#define gettext _gettext
int _dprintf(unsigned char level, _char *format, ...);
int _vdprintf(unsigned char level, _char *format, va_list arguments);
int _fprintf(FILE *stream, _char *format, ...);
int _vfprintf(FILE *stream, _char *format, va_list arguments);
int _fputs(_char *string, FILE *stream);
int _printf(_char *format, ...);
int _vprintf(_char *format, va_list arguments);
int _puts(_char *string);
char *_strcpy(char *buffer, _char *string);
int _strlen(_char *string);
#define _strdup(string) ((char *)_gettext(string))
/*RPB*/
#define _strdupw(string) ((char *)_gettext(string)) /* Will do for now */
/*RPB*/
int _strncmp(const char *buffer, _char *string, size_t count);
char *_strncpy(char *buffer, _char *string, size_t count);
/*RPB*/
int _stricmp(const char *buffer, _char *string);
/*RPB*/

#else

#define _char const char
#define N_
#define _ gettext
#define _dprintf dprintf
#define _vdprintf vdprintf
#define _fprintf fprintf
#define _vfprintf vfprintf
#define _fputs fputs
#define _printf printf
#define _vprintf vprintf
#define _puts puts
#define _strcpy strcpy
#define _strdup strdup
#define _strlen strlen
#define _strncmp strncmp
#define _strncpy strncpy
#endif

const char *gettext(_char *string);

#endif /* __LIBINTL_H */

