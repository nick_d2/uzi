/*
 * UZIX - UNIX Implementation for MSX
 * (c) 1997-2001 Arcady Schekochikhin
 *		 Adriano C. R. da Cunha
 *
 * UZIX is based on UZI (UNIX Zilog Implementation)
 * UZI is a UNIX kernel clone written for Z-80 systems.
 * All code is public domain, not being based on any AT&T code.
 *
 * The author, Douglas Braun, can be reached at:
 *	7696 West Zayante Rd.
 *	Felton, CA 95018
 *	oliveb!intelca!mipos3!cadev4!dbraun
 *
 * This program is under GNU GPL, read COPYING for details
 *
 */

/**********************************************************
 UZIX system calls wrappers, used by UZIX utilities
**********************************************************/

#ifndef _XFS_H
#define _XFS_H

#include <sys/stat.h> /* for struct stat */
#include <sys/types.h> /* for mode_t, size_t, time_t */
#include "uzi/devio.h" /* for dev_t */

void xfs__exit(int retval);
int xfs_open(const char *name, int flag, ...);
int xfs_close(int uindex);
int xfs_creat(const char *name, mode_t mode);
int xfs_link(const char *name1, const char *name2);
int xfs_symlink(char *name1, char *name2);
int xfs_unlink(const char *path);
int xfs_read(int d, void *buf, size_t nbytes);
int xfs_write(int d, const void *buf, size_t nbytes);
int xfs_seek(int file, unsigned int offset, int flag);
int xfs_chdir(char *dir);
int xfs_mknod(char *name, mode_t mode, int dev);
void xfs_sync(void);
int xfs_access(char *path, int mode);
int xfs_chmod(char *path, mode_t mode);
int xfs_chown(char *path, int owner, int group);
int xfs_stat(char *path, struct stat *buf);
int xfs_fstat(int fd, struct stat *buf);
int xfs_falign(int fd, int parm);
int xfs_dup(int oldd);
int xfs_dup2(int oldd, int newd);
int xfs_umask(int mask);
int xfs_sysdebug(int level);
int xfs_systrace(int level);
int xfs_ioctl(int fd, int request, ...);
int xfs_mount(char *spec, char *dir, int rwflag);
int xfs_umount(char *spec);
time_t xfs_time(time_t *tvec);
int xfs_getfsys(int dev, void *buf);

#endif

