/* cinode.h by Nick for NOS/UZI project */

#ifndef __CINODE_H
#define __CINODE_H

#include <sys/stat.h> /* for S_IFMT */
#include <sys/types.h> /* for off_t */
#include "uzi/bufpool.h" /* for dev_t, blkno_t */
#include "uzi/rtc.h" /* for rtctime_t */

#define NINODES 16	/* maximum entries in inode cache */
#define CMAGIC 24721	/* random number for cinode c_magic (in-core only) */

/* Flags for setftime() */
#define A_TIME 1
#define M_TIME 2
#define C_TIME 4

/* note: SUPERBLOCK is provided by z80/asm.h */
/* note: (D)(IN)DIRECTBLOCKS and TOTALREFBLOCKS are provided by z80/asm.h */

/* structure of disk inode */
typedef struct
	{
	unsigned short i_mode;
	unsigned short i_nlink;
	unsigned short i_uid;
	unsigned short i_gid;
	off_t i_size;
	rtctime_t i_atime;
	rtctime_t i_mtime;
	rtctime_t i_ctime;
	blkno_t i_addr[TOTALREFBLOCKS];
	} dinode_t;		/* Exactly 64 bytes long!  (Don't break it!) */

/* note: DINODESPERBLOCK and DINODESPERBLOCKLOG are provided by z80/asm.h */

/* structure of in-core inode */
typedef struct
	{
	unsigned short c_magic;	/* Used to check for corruption */
	unsigned char c_dirty;	/* Modified flag */
	dev_t c_dev;		/* Inode's device */
	ino_t c_num;		/* Inode's number */
	unsigned short c_refs; 	/* In-core reference count */
	unsigned char c_ro;	/* Read-only filesystem flag */
	dinode_t c_node;	/* disk inode copy */
	unsigned char c_busy;	/* Acquired flag */
	} cinode_t;

#define DEVNUM(ino)	((dev_t)((ino)->c_node.i_addr[0]))

/* Getmode() returns the inode kind */
#define _getmode(mode)	((mode) & S_IFMT)

/* shortcuts for access to file data via an open inode */
#define readi(x) 	readwritei(0, x)
#define writei(x)	readwritei(1, x)

extern cinode_t *Inotab[NINODES];

int wr_inode(cinode_t *ino, unsigned char dirty_mask);
void i_ref(cinode_t *ino);
void i_deref(cinode_t *ino);
void i_acquire(cinode_t *ino);
void i_release(cinode_t *ino);
cinode_t *i_open(dev_t devno, ino_t ino);
void readwritei(char write, cinode_t *ino);
unsigned char isdevice(cinode_t *ino);
void f_trunc(cinode_t *ino);
blkno_t bmap(cinode_t *ip, blkno_t bn, unsigned char rdflg);
mode_t getmode(cinode_t *ino);
int getperm(cinode_t *ino);
void setftim(cinode_t *ino, unsigned char flag);
void magic(cinode_t *ino, char *who);
void i_sync(dev_t dev);
void inotab_garbage(int red);

#endif /* __INODE_H */

