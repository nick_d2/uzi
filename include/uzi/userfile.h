/* userfile.h by Nick for NOS/UZI project */

#ifndef __USERFILE_H
#define __USERFILE_H

#include "uzi/openfile.h" /* for oft_t */

#define NUSERFILES 16	/* maximum number of user files */

unsigned int uf_alloc(void);
int uf_close(unsigned int fd);
struct object_s *uf_acquire(unsigned int fd);
oft_t *uf_getoft(unsigned int fd);

#endif /* __USERFILE_H */

