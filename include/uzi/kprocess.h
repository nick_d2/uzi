/* kprocess.h by Nick for NOS/UZI project */

#ifndef __KPROCESS_H
#define __KPROCESS_H

#include <signal.h> /* for NSIGS */
#include "uzi/openfile.h" /* for oft_t, cinode_t, also <sys/types.h> size_t */
#include "uzi/karena.h"
#include "uzi/ksession.h" /* this does NOT include nos/session.h yet */
#include "uzi/userfile.h" /* for NUSERFILES */

#define N_KERNEL_PROCESS 20

#define PRIO_MAX	19
#define PRIO_MIN	-20

#define TICKSPERSEC 50   /* Ticks per second */
#define MAXTICKS     4   /* Max ticks before swapping out (time slice)
                            default process time slice */
#if 0 /* temporary */
#define MAXBACK      3   /* Process time slice for tasks not connected
                            to the current tty */
#define MAXBACK2     2   /* Process time slice for background tasks */
#define MAXINTER     5   /* Process time slice for interactive tasks */
#endif

/* note: WNOHANG / WUNTRACED are provided by sys/wait.h */

#define P_EMPTY 0	/* Unused slot */
#define P_ZOMBIE 1	/* Exited. */
#define P_FORKING 2	/* In process of forking; do not mess with */
#define P_RUNNING 3	/* Currently running process */
#define P_READY 4	/* Runnable process */
#define P_SLEEP	5	/* Sleeping; can be awakened by signal */
#define P_XSLEEP 6	/* Sleeping, don't wake up for signal */
#define P_PAUSE 7	/* Sleeping for pause(); can wakeup for signal */
#define P_WAIT 8	/* Executed a wait() */
/*#define P_STOPED 9*/	/* Process is stoped */

/* forward reference to help the compiler */
typedef struct kernel_process_s kernel_process_t;

/* structure of a unix process (nos processes are akin to threads) */
struct kernel_process_s
	{
	unsigned char refs;	/* how many threads share this process entry */
	unsigned char status;	/* Process status */

	kernel_arena_t *arena_p; /* normally one per process unless vforked */
	kernel_process_t *parent_p; /* Process parent's table entry */
#if 1 /* temporary */
	kernel_session_t *session_p; /* similar to notion of controlling tty */
#endif

	int p_cursig;		/* Signal currently being caught */
	char p_tty;		/* Process' controlling tty minor # */

	int p_pid;		/* Process ID */
	int p_uid;

	unsigned int p_alarm;	/* Seconds until alarm goes off */
	unsigned int p_exitval;	/* Exit value */

/* Everything below here is overlaid by time info at exit */
	unsigned long p_zombie[2]; /* Don't allow sharing of mem after all!! */

	unsigned char p_cprio;	/* Process current priority */
	signed char p_nice;	/* Process nice value (-20 to 19) */

	unsigned short p_pending; /* Pending signals */
	unsigned short p_ignored; /* Ignored signals */

	unsigned int p_break;	/* process break level */

/* everything below here has been moved from udata into the kernel_process */
	int p_gid;
	int p_euid;
	int p_egid;
	int p_umask;		/* umask: file creation mode mask */

	rtctime_t p_time;	/* Start time */

	cinode_t *p_cwd;	/* Index into inode table of cwd. */
	cinode_t *p_root;	/* Index into inode table of chroot target */

	int (*p_sigvec[NSIGS])(); /* Array of signal vectors */

	unsigned long p_utime;	/* Elapsed ticks in user mode */
	unsigned long p_stime;	/* Ticks in system mode */
	unsigned long p_cutime;	/* Total childrens ticks */
	unsigned long p_cstime;

	unsigned char p_traceme; /* added by Nick, see kernel/strace.c */

	struct object_s *object_p[NUSERFILES]; /* Process file table */
	};

/* structure of temporary per-call storage */
typedef struct
	{
	unsigned char u_insys;	/* True if in kernel */
	unsigned char u_callno;	/* sys call being executed. */
#if 0
	void *u_retloc;		/* Return location from sys call phase out!! */
#endif
	int u_retval;		/* Return value from sys call */
	int u_retval1;		/* Nick, for long return value from lseek() */
	int u_error;		/* Last error number */
#if 0
	int *u_sp;		/* Used when process is swapped. */
	int *u_bc;		/* Place for user's frame pointer */
	int u_cursig;		/* Signal currently being caught */
#endif
	int u_argn0;		/* Last system call arg */
	int u_argn1;		/* This way because args on stack backwards */
	int u_argn2;
	int u_argn3;		/* args n-3, n-2, n-1, and n */

	void *u_base;		/* Source or dest for I/O */
	unsigned int u_count;	/* Amount for I/O */
	off_t u_offset;		/* Place in file for I/O */
	blkbuf_t *u_buf;
	unsigned char u_sysio;	/* True if I/O to system space */
	} udata_t;

extern kernel_process_t *Kernel_process[N_KERNEL_PROCESS];

/* Super() returns true if we are the superuser */
#define super() (Kernel_thread_p->process_p->p_euid == 0)

kernel_process_t *kernel_process_new(void);
void kernel_process_delete(kernel_process_t *kp_p);
void kernel_process_ref(kernel_process_t *kp_p);
void kernel_process_deref(kernel_process_t *kp_p);
kernel_process_t *kernel_process_find(int pid);
kernel_process_t *kernel_process_fork(kernel_process_t *parent_kp_p);
void kernel_process_garbage(int red);

#if 0 /* SLIGHT MISMATCH BETWEEN PUBLIC AND KERNEL VERSION OF HEADER!! */
/* must be done here because it recursively includes proc.h -> kprocess.h */
#include "nos/session.h" /* temporary */
#endif

#endif /* __KPROCESS_H */

