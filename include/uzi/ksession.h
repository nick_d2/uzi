/* ksession.h by Nick for NOS/UZI project */

#ifndef __KSESSION_H
#define __KSESSION_H

/* note: user must include nos/session.h probably AFTER uzi/ksession.h */
/* this is due to a loop session.h -> proc.h -> kprocess.h -> ksession.h */

#define N_KERNEL_SESSION 20

/* forward reference to help the compiler */
typedef struct session kernel_session_t;

extern kernel_session_t *Kernel_session[N_KERNEL_SESSION];
extern kernel_session_t *Kernel_session_p;

kernel_session_t *kernel_session_new(void);
void kernel_session_delete(kernel_session_t *kp_p);
void kernel_session_ref(kernel_session_t *kp_p);
void kernel_session_deref(kernel_session_t *kp_p);
void kernel_session_garbage(int red);

#endif /* __KSESSION_H */

