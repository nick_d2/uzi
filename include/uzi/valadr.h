/* valadr.h by Nick for NOS/UZI project */

#ifndef __VALADR_H
#define __VALADR_H

int valadr(void *base, size_t size);

#endif /* __VALADR_H */

