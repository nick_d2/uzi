/* device.h by Nick for NOS/UZI project */

#ifndef __DEVICE_H
#define __DEVICE_H

#include <setjmp.h> /* for jmp_buf */
#include <stddef.h> /* for size_t */
#include "uzi/bufpool.h" /* for blkno_t, dev_t */

/* header for either kind of device driver switch table */
struct device_s
	{
	unsigned char minors;
	void (*init)(jmp_buf error, unsigned char minor);
	void (*open)(jmp_buf error, unsigned char minor);
	void (*close)(jmp_buf error, unsigned char minor);
	void (*ioctl)(jmp_buf error, unsigned char minor,
			unsigned int request, void *data);
	};

/* structure of a block device driver switch table entry */
struct device_block_s
	{
	struct device_s device;
	void (*read)(jmp_buf error, unsigned char minor,
			void *data, blkno_t block);
	void (*write)(jmp_buf error, unsigned char minor,
			void *data, blkno_t block);
	};

/* structure of a character device driver switch table entry */
struct device_char_s
	{
	struct device_s device;
	size_t (*read)(jmp_buf error, unsigned char minor,
			void *data, size_t count);
	size_t (*write)(jmp_buf error, unsigned char minor,
			void *data, size_t count);
	};

#define NDEVICES 7 /* number of device driver switch table entries (majors) */
#define DTHRESH 2 /* device driver switch table indices < DTHRESH are block */

extern struct device_s *Device[NDEVICES];

void device_init(void);
struct device_s *device_find(jmp_buf error, dev_t dev);

#endif /* __DEVICE_H */

