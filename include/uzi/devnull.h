/* devnull.h by Nick for NOS/UZI project */

#ifndef __DEVNULL_H
#define __DEVNULL_H

#include <setjmp.h> /* for jmp_buf */
#include <stddef.h> /* for size_t */

extern struct device_char_s Dev_null;

void dev_null_init(jmp_buf error, unsigned char minor);
size_t dev_null_read(jmp_buf error, unsigned char minor,
		void *data, size_t count);
size_t dev_null_write(jmp_buf error, unsigned char minor,
		void *data, size_t count);
void dev_null_ioctl(jmp_buf error, unsigned char minor,
		unsigned int request, void *data);

#endif /* __DEVNULL_H */

