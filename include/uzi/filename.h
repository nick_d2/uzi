/* filename.h by Nick for NOS/UZI project */

#ifndef __FILENAME_H
#define __FILENAME_H

#include "uzi/cinode.h" /* for cinode_t */

cinode_t *_namei(unsigned char *name, unsigned char **rest,
	      cinode_t *strt, cinode_t **parent);
cinode_t *namei(char *uname, cinode_t **parent, unsigned char follow);
cinode_t *srch_dir(cinode_t *wd, char *name);
cinode_t *srch_mt(cinode_t *ino);
int ch_link(cinode_t *wd, char *oldname, char *newname, cinode_t *nindex);
void filename(char *upath, char *name); /* char *filename(char *path); */
int namecomp(unsigned char *n1, unsigned char *n2);
cinode_t *newfile(cinode_t *pino, char *name);
int chany(char *path, int val1, int val2, unsigned char mode);
cinode_t *n_creat(char *name, unsigned char new, mode_t mode);

#endif /* __FILENAME_H */

