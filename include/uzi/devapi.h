/* devapi.h by Nick for NOS/UZI project */

#ifndef __DEVAPI_H
#define __DEVAPI_H

#include <setjmp.h> /* for jmp_buf */
#include <stddef.h> /* for size_t */
#include "nos/asci.h" /* for struct dma, struct fifo */
#include "uzi/kthread.h" /* for kernel_thread_t */

typedef struct {
	unsigned char refs;
	unsigned char minor;
	unsigned char nobl;
	kernel_thread_t *thread_p;
	struct mbuf *queue_p;
	} api_minor_t;

#define N_API_MINOR 9

extern api_minor_t *Api_minor[N_API_MINOR];
extern unsigned char Api_bufsize[N_API_MINOR];

extern struct device_char_s Dev_api;

void dev_api_open(jmp_buf error, unsigned char minor);
void dev_api_close(jmp_buf error, unsigned char minor);
size_t dev_api_read(jmp_buf error, unsigned char minor,
		void *data, size_t count);
size_t dev_api_write(jmp_buf error, unsigned char minor,
		void *data, size_t count);
void dev_api_ioctl(jmp_buf error, unsigned char minor,
		unsigned int request, void *data);
void api_minor_thread(int argc, void *argv, void *p);
void api_minor_thread_lcd0(int argc, void *argv, void *p);
void api_minor_thread_cfl0(int argc, void *argv, void *p);
void api_minor_garbage(int red);

#endif /* __DEVAPI_H */

