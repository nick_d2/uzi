/* openfile.h by Nick for NOS/UZI project */

#ifndef __OPENFILE_H
#define __OPENFILE_H

#include <sys/types.h> /* for off_t */
#include "uzi/cinode.h" /* for cinode_t */
#include "uzi/object.h" /* for struct object_s */

#define NOPENFILES 16	/* maximum number of open files */

/* structure of open file table entry */
typedef struct
	{
	struct object_s object;	/* header with type, refs, busy */
	unsigned char o_access;	/* O_RDONLY, O_WRONLY, or O_RDWR */
	off_t o_ptr;		/* File position pointer */
	cinode_t *o_inode;	/* Pointer into in-core inode table */
	} oft_t;

extern oft_t *Oftab[NOPENFILES];

oft_t *oft_alloc(void);
void oft_ref(oft_t *oft_p);
void oft_deref(oft_t *oft_p);
void oft_truncate(oft_t *oft_p);
void oftab_garbage(int red);

#endif /* __OPENFILE_H */

