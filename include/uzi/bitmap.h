/* bitmap.h by Nick for NOS/UZI project */

#ifndef __BITMAP_H
#define __BITMAP_H

#include <libintl.h> /* for _char */
#include "uzi/bufpool.h" /* for dev_t, blkno_t */

extern _char *Badinomsg;

/* note: SUPERBLOCK is provided by z80/asm.h */
/* note: (D)(IN)DIRECTBLOCKS and TOTALREFBLOCKS are provided by z80/asm.h */

#define blockinodes(blk)	((blk) << DINODESPERBLOCKLOG)
#define devinodes(dev)		blockinodes((dev)->s_isize)
#define inodeblock(dev,ino)	(((ino) >> DINODESPERBLOCKLOG) + \
					(dev)->s_reserv)
#define inodeoffset(ino)	((ino) & (DINODESPERBLOCK-1))

void i_free(dev_t devno, ino_t ino);
ino_t i_alloc(dev_t devno);
blkno_t blk_alloc(dev_t devno, unsigned char dirty); /* Nick dirty */
void blk_free(dev_t devno, blkno_t blk);
void validblk(dev_t devno, blkno_t num);
blkno_t bitmap_search(dev_t devno, int size, blkno_t start, blkno_t final);
int bitmap_reserve(dev_t devno, blkno_t blk, int size, int flag,
		blkno_t start, blkno_t final);
blkno_t bitmap_find(dev_t devno, blkno_t blk, int flag, int toggle,
		 blkno_t start, blkno_t final);
int bitmap_get(dev_t devno, blkno_t blk, blkno_t start, blkno_t final);
int bitmap_set(dev_t devno, blkno_t blk, int flag,
		blkno_t start, blkno_t final);

#endif /* __BITMAP_H */

