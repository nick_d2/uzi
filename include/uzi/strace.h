/* strace.h by Nick for NOS/UZI project */

#ifndef __STRACE_H
#define __STRACE_H

#define TYPE_BIN 1
#define TYPE_STR 2
#define TYPE_OCT 3
#define TYPE_DEC 4
#define TYPE_HEX 5

void system_entry(void);
void system_exit(void);
void system_dump(unsigned int value, unsigned int count, unsigned char type);

#endif

