/* maxmin.h by Nick for NOS/UZI project */

#ifndef __MAXMIN_H
#define __MAXMIN_H

int max_int(int a, int b);
long max_long(long a, long b);
unsigned int max_uint(unsigned int a, unsigned int b);
unsigned long max_ulong(unsigned long a, unsigned long b);
int min_int(int a, int b);
long min_long(long a, long b);
unsigned int min_uint(unsigned int a, unsigned int b);
unsigned long min_ulong(unsigned long a, unsigned long b);

#endif /* __MAXMIN_H */
