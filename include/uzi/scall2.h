/* scall2.h by Nick for NOS/UZI project */

#ifndef __SCALL2_H
#define __SCALL2_H

#include "uzi/cinode.h" /* for cinode_t */

/* sys_getset() commands */
#define	GET_PID		0	/* get process id */
#define	GET_PPID	1	/* get parent process id */
#define	GET_UID		2	/* get user id */
#define	SET_UID		3	/* set user id */
#define	GET_EUID	4	/* get effective user id */
#define	GET_GID		5	/* get group id */
#define	SET_GID		6	/* set group id */
#define	GET_EGID	7	/* get effective group id */
#define	GET_PRIO	8	/* get process priority */
#define	SET_PRIO	9	/* set process priority */
#define	SET_UMASK	10	/* get/set umask */
#define	SET_TRACE	11	/* set trace flag */
#define	SET_DEBUG	12	/* set debug flag */

int sys_pipe(void);
int sys_stime(void);
int sys_times(void);
int sys_brk(void);
int sys_sbrk(void);
int sys_waitpid(void);
int kernel_dowait(void);
int sys__exit(void);
void kernel_doexit(int val, int val2);
int sys_fork(unsigned int rq, unsigned int *ret_p); /* supposedly a va_list */
void sys_fork_thread(int argc, void *argv, void *p);
int sys_pause(void);
unsigned long sys_signal(void);
int sys_kill(void);
int sys_alarm(void);
int sys_reboot(void);
int sys_getset(void);
int sys_getset(void);

#endif /* __SCALL2_H */

