/* karena.h by Nick for NOS/UZI project */

#ifndef __KARENA_H
#define __KARENA_H

#define N_KERNEL_ARENA 20

/* forward reference to help the compiler */
typedef struct kernel_arena_s kernel_arena_t;

struct kernel_arena_s
	{
	unsigned char refs;	/* how many processes share this arena item */
	kernel_arena_t *next; /* to item with next higher a_page, or NULL */

	unsigned char page;	/* starting 4 kbyte page, 0..63 */
	unsigned char size;	/* count of 4 kbyte pages, 1..64 */
	};

extern kernel_arena_t *Kernel_arena[N_KERNEL_ARENA];
extern kernel_arena_t *Kernel_arena_p;

kernel_arena_t *kernel_arena_new(void);
void kernel_arena_delete(kernel_arena_t *ka_p);
void kernel_arena_ref(kernel_arena_t *ka_p);
void kernel_arena_deref(kernel_arena_t *ka_p);
kernel_arena_t *kernel_arena_allocate(unsigned char size);
int kernel_arena_resize(kernel_arena_t *ka_p, unsigned char size);
void kernel_arena_garbage(int red);

#endif /* __KARENA_H */

