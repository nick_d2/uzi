/* kvector.h by Nick for NOS/UZI project */

#ifndef __KVECTOR_H
#define __KVECTOR_H

#include <stdarg.h>

#define N_KERNEL_VECTOR 41

extern
unsigned int (*Kernel_vector[N_KERNEL_VECTOR])(unsigned int rq, va_list ap);

#endif /* __KVECTOR_H */

