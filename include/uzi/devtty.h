/* devtty.h by Nick for NOS/UZI project */

#ifndef __DEVTTY_H
#define __DEVTTY_H

#include <setjmp.h> /* for jmp_buf */
#include <sgtty.h> /* for struct sgtty */
#include <stddef.h> /* for size_t */
#include "nos/tty.h"
#include "uzi/ksession.h" /* for kernel_session_t */
#include "uzi/kthread.h" /* for kernel_thread_t */

typedef struct {
	unsigned char refs;
	unsigned char minor;

	struct sgttyb param;
	struct tchars chars;

	struct ttystate state;

	unsigned char type;	/* _FL_ASY, _FL_SOCK */
	void *type_p;		/* -> struct asy, struct usock */

	kernel_session_t *session_p;
	kernel_session_t *root_session_p;
	kernel_session_t *last_session_p;

	kernel_thread_t *thread_p;
	struct mbuf *queue_p;

	unsigned char *lp;	/* Pointer into same */
	unsigned char line[TTY_LINE_SIZE];
	} tty_minor_t;

#define N_TTY_MINOR 6 /*2*/

extern tty_minor_t *Tty_minor[N_TTY_MINOR];

extern struct device_char_s Dev_tty;

void dev_tty_open(jmp_buf error, unsigned char minor);
void dev_tty_close(jmp_buf error, unsigned char minor);
size_t dev_tty_read(jmp_buf error, unsigned char minor,
		void *data, size_t count);
size_t dev_tty_write(jmp_buf error, unsigned char minor,
		void *data, size_t count);
void dev_tty_ioctl(jmp_buf error, unsigned char minor,
		unsigned int request, void *data);
void tty_minor_thread(int argc, void *argv, void *p);
void tty_minor_garbage(int red);
int ttydriv(tty_minor_t *tm_p, unsigned char c);
int tty_read(tty_minor_t *tm_p, void *data, size_t count);
int tty_write(tty_minor_t *tm_p, void *data, size_t count);
int tty_fputs(char *string, tty_minor_t *tm_p);
int _tty_fputs(_char *string, tty_minor_t *tm_p);
int get_tty(tty_minor_t *tm_p);
int put_tty(unsigned char c, tty_minor_t *tm_p);
kernel_session_t *tty_focus(tty_minor_t *tm_p, kernel_session_t *kp_p);

#endif /* __DEVTTY_H */

