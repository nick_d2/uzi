/* systrace.h by Nick for NOS/UZI project */

#ifndef __SYSTRACE_H
#define __SYSTRACE_H

#define TYPE_BIN 1
#define TYPE_STR 2
#define TYPE_OCT 3
#define TYPE_DEC 4
#define TYPE_HEX 5

void systrace_entry(void);
void systrace_exit(void);
void systrace_dump(unsigned int value, unsigned int count, unsigned char type);

#endif

