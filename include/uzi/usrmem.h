/* usrmem.h by Nick for NOS/UZI project */

#ifndef __USRMEM_H
#define __USRMEM_H

#include <sys/types.h> /* for size_t */

#define bcopy(s, d, c) memcpy(d, s, c)
#define bfill(d, v, c) memset(d, v, c)
#define bzero(d, c) memset(d, 0, c)

/*RPB*/
#if 0

#define uget(s, d, c) usrget(d, s, c)
#define ugetc(s) usrget_unsigned_char(s)
#define ugets(s, d, c) usrget_string(d, s, c)
#define ugetw(s) usrget_unsigned_int(s)
#define uput(s, d, c) usrput(d, s, c)
#define uputw(v, d) usrput_unsigned_int(d, v)
#define uputc(v, d) usrput_unsigned_char(d, v)

#else

/* int uget(char *s, char *d, unsigned c) */
#define uget(s, d, c) ((void)usrget(d, s, c), 0)

/* int ugets(char *s, char *d, unsigned c)  rc==0 OK, rc<>0 NOK */
#define ugets(s, d, c) (usrget_string(d, s, c) == NULL ? 1 : 0)

/* unsigned int ugetw(char *s) */
#define ugetw(s) (usrget_unsigned_int(s))

/* unsigned char ugetc(char *s); Nick see execve.c */
#define ugetc(s) (usrget_unsigned_char(s))

/* int uput(char *s, char *d, unsigned c) */
#define uput(s, d, c) (usrput(d, s, c), 0)

/* int uputw(int v, char *d) */
#define uputw(v, d) (usrput_unsigned_int(d, v), 0)

/* int uputc(char v, char *d); Nick see execve.c */
#define uputc(v, d) (usrput_unsigned_char(d, v), 0)
#endif
/*RPB*/

void *usrget(void *dest, const void *src, size_t count);
unsigned int usrget_unsigned_int(const void *src);
unsigned char usrget_unsigned_char(const void *src);
void *usrget_string(void *dest, const void *src, size_t count);
void *usrput(void *dest, const void *src, size_t count);
void usrput_unsigned_int(void *dest, unsigned int value);
void usrput_unsigned_char(void *dest, unsigned char value);

#endif /* __USRMEM_H */
