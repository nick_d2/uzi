/* devio.h by Nick for NOS/UZI project */

#ifndef __DEVIO_H
#define __DEVIO_H

#include "uzi/bufpool.h" /* for dev_t, blkbuf_t */

/* shortcuts for device open/close */
#define d_open(dev)	d_openclose(dev, 1)
#define d_close(dev)	d_openclose(dev, 0)

/* shortcuts for block device read/write */
#define bdread(bp)	bdreadwrite(bp, 0)
#define bdwrite(bp)	bdreadwrite(bp, 1)

/* shortcuts for char device read/write */
#define cdread(dev, data, count)	cdreadwrite(dev, data, count, 0)
#define cdwrite(dev, data, count)	cdreadwrite(dev, data, count, 1)

void d_init(void);
unsigned char d_openclose(dev_t dev, unsigned char open);
unsigned char d_ioctl(dev_t dev, unsigned int request, void *data);
unsigned char bdreadwrite(blkbuf_t *bp, unsigned char write);
size_t cdreadwrite(dev_t dev, void *data, size_t count, unsigned char write);
struct device_s *validdev(dev_t dev, unsigned char *msg);
/*int nogood(unsigned char minor);*/
/*int nogood_rdwr(unsigned char minor, unsigned char rawflag);*/

#endif /* __DEVIO_H */

