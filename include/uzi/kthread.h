/* kthread.h by Nick for NOS/UZI project */

#ifndef __KTHREAD_H
#define __KTHREAD_H

#include <signal.h> /* for NSIGS */
#include "uzi/kprocess.h" /* also <sys/types.h> size_t and "uzi/karena.h" */
#include "nos/proc.h" /* defines struct proc (for now kernel_thread_t alias) */

#define N_KERNEL_THREAD 20

/* forward reference to help the compiler */
typedef struct proc kernel_thread_t;

extern kernel_thread_t *Kernel_thread[N_KERNEL_THREAD];

kernel_thread_t *kernel_thread_new(void);
void kernel_thread_delete(kernel_thread_t *kt_p);
void kernel_thread_garbage(int red);

#endif /* __KTHREAD_H */

