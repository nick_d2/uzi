/* bufpool.h by Nick for NOS/UZI project */

#ifndef __BUFPOOL_H
#define __BUFPOOL_H

#include "asm.h" /* "z80/asm.h" but we now rely on tradcpp -I../include/z80 */

#if 0 /* SLIGHT MISMATCH BETWEEN PUBLIC AND KERNEL VERSION OF HEADER!! */
/* BUFSIZE has been defined already by the C runtime library */
typedef unsigned short ino_t;	/* Can have 65536 inodes in fs */
typedef unsigned short blkno_t;	/* Can have 65536 BUFSIZE-byte blocks in fs */
typedef unsigned short dev_t;	/* Device number */
#endif

#define MINOR(dev)		((uchar)(dev))
#define MAJOR(dev)		((uchar)((dev) >> 8))
#define MKDEV(major, minor)	(((uint)(uchar)(major) << 8) | (uchar)(minor))

#define NULLINO ((ino_t)0)
#define NULLBLK ((blkno_t)-1)
#define NULLDEV ((dev_t)-1)

/* forward reference to help the compiler */
typedef struct blkbuf_s blkbuf_t;

/* structure of an in-core buffered disk block */
struct blkbuf_s
	{
	unsigned char bf_data[BUFSIZE]; /* This MUST BE first ! */
	dev_t bf_dev; 	/* device of this block */
	blkno_t bf_blk; 	/* and block number on device */
	unsigned char bf_dirty;	/* buffer changed flag */
	unsigned char bf_busy;	/* buffer processing in progress */
	/*unsigned char bf_prio;*/	/* buffer must be in memory (for wargs) */
	unsigned int bf_time;	/* LRU time stamp */
	/*struct s_blkbuf *bf_next;*/ /* LRU free list pointer */
	};

#define NBLKBUFS 12 /*8*/ /* max number of disk blocks in the buffer cache */

extern blkbuf_t *Bufpool[NBLKBUFS];

extern unsigned int bufclock;		/* time-stamp counter for LRU */

extern unsigned int buf_hits;		/* buffer pool hits */
extern unsigned int buf_miss;		/* buffer pool misses */
extern unsigned int buf_flsh;		/* buffer pool flushes */

/* release buffer */
#define brelse(bp)	bfree(bp, 0)

/* free buffer and mark them dirty for eventually writing */
#define bawrite(bp)	bfree(bp, 1)

void blk_acquire(blkbuf_t *blkbuf_p);
void blk_release(blkbuf_t *blkbuf_p);
void *bread(dev_t dev, blkno_t blk, unsigned char rewrite);
int bfree(blkbuf_t *bp, unsigned char dirty);
void *zerobuf(unsigned char waitfor);
void bufsync(dev_t dev, unsigned char drop);
blkbuf_t *bfind(dev_t dev, blkno_t blk);
int bufdump(int argc, char *argv[], void *p);
void bufpool_garbage(int red);

#endif /* __BUFPOOL_H */

