/* kserver.h by Nick for NOS/UZI project */

#ifndef __KSERVER_H
#define __KSERVER_H

#include "uzi/kprocess.h" /* also sys/types.h signal_t */

/* defined in z80/unixvec.S */
void unixret(int value);
void unixrun(void);

/* defined in kernel/kserver.c */
void clkint2(void);
unsigned int unix2(unsigned int rq, va_list ap);
void chksigs(void);
void sgrpsig(int tty, int sig);
void ssig(kernel_process_t *proc, int sig);
void sendsig(kernel_process_t *proc, signal_t sig);

#endif /* __KSERVER_H */

