/* scall1.h by Nick for NOS/UZI project */

#ifndef __SCALL1_H
#define __SCALL1_H

#include <sys/ioctl.h> /* for info_t */
#include "uzi/cinode.h" /* for cinode_t */
#include "nos/global.h" /* for Hostname and Version */

#define UZIX "NOS/UZI"
#define VERSION Version
#define RELEASE "RELEASE"

#define HOST Hostname
#define MACHINE "MACHINE"

/* Info about kernel, returned by sys_getfsys */
typedef struct
	{
	unsigned char k_name[14];	/* OS name */
	unsigned char k_version[8];	/* OS version */
	unsigned char k_release[8];	/* OS release */

	unsigned char k_host[14];	/* Host name */
	unsigned char k_machine[8];	/* Host machine */

	unsigned int k_tmem; 	/* System memory, in kbytes */
	unsigned int k_kmem; 	/* Kernel memory, in kbytes */
	} kdata_t;

int sys_sync(void);
int sys_utime(void);
int sys_close(void);
int sys_open(void);
int sys_link(void);
int sys_symlink(void);
int sys_unlink(void);
int sys_readwrite(void);
off_t sys_lseek(void);
int sys_chdir(void);
int sys_chroot(void);
int sys_mknod(void);
int sys_access(void);
int sys_chmod(void);
int sys_chown(void);
/*void stcpy(cinode_t *ino, struct stat *buf);*/
int sys_statfstat(void);
int sys_falign(void);
int sys_dup(void);
int sys_dup2(void);
int sys_getfsys(void);
int sys_ioctl(void);
int sys_mountumount(void);
time_t sys_time(void);
/*cinode_t *n_creat(char *name, bool_t new, mode_t mode);*/
cinode_t *rwsetup(unsigned char fd, void *base,
	       unsigned int cnt, unsigned char rdflag);
int pdat(struct s_pdata *ubuf);
int kernel_getfsys(dev_t devno, info_t *buf);

#endif /* __SCALL1_H */

