/* xip.h by Nick for NOS/UZI project */

#ifndef __XIP_H
#define __XIP_H

#include "uzi/filesys.h" /* for filesys_t, also "uzi/cinode.h" cinode_t */

int xip_align(cinode_t *ino, off_t size);
int xip_ualign(cinode_t *ino, off_t size);
blkno_t *xip_examine(filesys_t *dev, cinode_t *ino, off_t size,
		blkno_t *regioncount, blkno_t *blockcount);
blkno_t xip_align_chase(filesys_t *dev, cinode_t *ino, blkno_t blk,
		blkno_t pos, blkno_t *region, blkno_t regions, blkno_t blocks);
blkno_t xip_align_bmap(cinode_t *ino, blkno_t newno, blkno_t pos);
blkno_t xip_align_reverse(blkno_t blk,
		blkno_t *region, blkno_t regions, blkno_t blocks);
blkno_t xip_align_recurse(filesys_t *dev, cinode_t *ino, int exclude,
		blkno_t *parent, char *dirty,
		int indirection, blkno_t *region,
		blkno_t regions, blkno_t blocks);

#endif /* __XIP_H */

