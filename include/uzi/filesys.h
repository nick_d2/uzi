/* filesys.h by Nick for NOS/UZI project */

#ifndef __FILESYS_H
#define __FILESYS_H

#include <libintl.h> /* for _char */
#include "uzi/bufpool.h" /* for dev_t, blkno_t */
#include "uzi/cinode.h" /* for cinode_t */
#include "uzi/rtc.h" /* for rtctime_t */

#define NMOUNTS 2	/* maximum number of filesystems */
#define SMOUNTED 12742	/* magic number to flag mounted filesystem (on disk) */

/* note: SUPERBLOCK is provided by z80/asm.h */

#if 0 /* remove support for the original UZI filesystem */
#define FSFREEBLOCKS	50
#define FSFREEINODES	50
#endif

/* device-resident super-block */
typedef struct
	{
	unsigned short s_mounted; /* signature */
	unsigned short s_reserv; /* # of first block of inodes */
	unsigned short s_isize;	/* # of inode's blocks */
	unsigned short s_fsize;	/* # of data's blocks */

	blkno_t s_tfree;	/* total free blocks */
#if 0 /* remove support for the original UZI filesystem */
	unsigned short s_nfree;	/* # of free blocks in s_free */
	blkno_t s_free[FSFREEBLOCKS]; /* #s of free block's */
#endif

	ino_t	s_tinode;	/* total free inodes */
#if 0 /* remove support for the original UZI filesystem */
	unsigned short s_ninode; /* # of free inodes in s_inode */
	ino_t	s_inode[FSFREEINODES]; /* #s of free inodes */
#endif

	rtctime_t s_time; 	/* last modification timestamp */

#if 1 /* Nick free bitmap */
	blkno_t	s_bitmap_inode;	/* disk byte position of free inode bitmap */
	blkno_t	s_bitmap_block;	/* disk byte position of free block bitmap */
	blkno_t s_bitmap_immov;	/* disk byte position of immoveable bitmap */
	blkno_t	s_bitmap_final;	/* byte position just after end of bitmaps */
#endif

/* Core-resident part */
	unsigned char s_fmod; 	/* filesystem modified */
	unsigned char s_ronly;	/* readonly filesystem */
	cinode_t *s_mntpt;	/* mount point inode */
	dev_t s_dev;		/* device on which filesystem resides */
	} filesys_t;

extern dev_t Root_dev;		/* Device number of root filesystem. */
extern cinode_t *Root_ino;	/* Address of root dir in inode table */

extern filesys_t *Fstab[NMOUNTS];

extern _char *Baddevmsg;

void xfs_init(dev_t boot_dev);
void xfs_end(void);
void fs_init(void);
filesys_t *findfs(dev_t devno);
filesys_t *getfs(dev_t devno);
int fmount(dev_t dev, cinode_t *ino, unsigned char roflag);
void fs_sync(dev_t dev);

#endif /* __FILESYS_H */

