/* execve.h by Nick for NOS/UZI project */

#ifndef __EXECVE_H
#define __EXECVE_H

#include <sys/types.h> /* for off_t */

#if 1 /* Nick please look into this */

/* Struct to hold arguments temporarily in execve */
typedef struct
	{
	int a_argc;
	int a_arglen;
	int a_envc;
	char a_buf[512-3*sizeof(int)];
	} argblk_t;

#define E_MAGIC 0xa6c9

#define E_FORMAT_LARGE 1
#define E_FORMAT_BANKED 2
#define E_FORMAT_KERNEL 3

typedef struct
	{
	unsigned short e_magic;
	unsigned short e_format;
	off_t e_size;
	unsigned short e_hsize;
	unsigned short e_idata;
	unsigned short e_entry;
	unsigned short e_udata;
	unsigned short e_stack;
	unsigned short e_break;
	} exehdr_t;

int sys_execve(void);
int wargs(char **argv, argblk_t *argbuf);
char *rargs(char *ptr, argblk_t *argbuf, int *cnt);

#else

unsigned int ssiz(char **argv, unsigned char *cnt, char **smin);
void *scop(char **argv, unsigned char cnt, char *to, char *real);
int repack(char **argp, char **envp, unsigned char check);
void exec2(void);
int sys_execve(void);

#endif

#endif /* __EXECVE_H */

