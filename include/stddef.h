#ifndef __STDDEF_H
#define __STDDEF_H

#ifndef _SIZE_T
#define _SIZE_T
typedef unsigned int size_t;
#endif

#ifndef _PTRDIFF_T
#define _PTRDIFF_T
typedef int ptrdiff_t;
#endif

#ifndef _OFFSETOF
#define _OFFSETOF
#define offsetof(ty, mem) ((int)&(((ty *)0)->mem))
#endif

#ifndef NULL
#define NULL 0
#endif

#endif /* __STDDEF_H */
